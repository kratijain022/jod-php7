<?php
$base_url = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "https" : "http");
$base_url .= "://". @$_SERVER['HTTP_HOST'];
$base_url .= str_replace(basename($_SERVER['SCRIPT_NAME']),"",$_SERVER['SCRIPT_NAME']);

//send Complete Job Notification To Managers
$url= $base_url.'ws/sendpushandemail/runAcknowledgeNotificationToApplicantsCron';
//$url= "https://cdnsolutionsgroup.com/jobsondemand_qa/ws/sendpushandemail/runCompleteJobNotificationToManagersCron";
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_FAILONERROR, false);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
$output = curl_exec($ch);
$err = curl_error($ch);

echo $url."\n";

/********* Used for set up the log for service - start. ********/
function logSetup($output,$type,$error)
{
	//Start:maintain log file for testing purpose only.
		$file_name = "ws/logs/log.txt";
		$base_path = realpath(dirname(__FILE__)).'/';
		$file_path = $base_path.$file_name;
		if(file_exists($file_path) == false){
			$file_pointer = fopen($file_path, "a+");
		}
		$file_pointer = fopen($file_path, "a+");
		fwrite($file_pointer, "##########################################################################\n");
		fwrite($file_pointer, "Date:".date('d-m-Y h:i:s')."Log File executed \n ");
		fwrite($file_pointer, "output: ".$output."\n");
		if($type == 1){
			fwrite($file_pointer, "Email cron executed"."\n");
		}else{
			fwrite($file_pointer, "Push Notification cron executed"."\n");
		}
		if($output != Null){
				fwrite($file_pointer, "ERROR :".$error."\n");
		}else{
			fwrite($file_pointer, "NO ERROR SUCCESS IN CRON EXECUTION \n");
		}
		chmod($file_name, 0777);
		fclose($file_pointer);
	//End:maintain log file for testing purpose only.
}

?>
