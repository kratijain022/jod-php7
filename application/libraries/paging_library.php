<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Paging library
 */
class Paging_library
{
		/**
		* function for set paging config
		**/
		function get_config_paging($data)
		{
			
			$CI = & get_instance();
			$CI->load->library('pagination');
			
			$config['per_page'] = $data['per_page'];
			$config['num_links'] = 1;
			$config['base_url'] =  $data['url'];
			$config['total_rows'] = $data['count'];
			
			$config['use_page_numbers'] = TRUE;
			$config['page_query_string'] = TRUE;
			
			
			$config['full_tag_open'] = '<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 pull-right"><div class=" paging_simple_numbers" id="table3_paginate"><ul class="pagination">';
			$config['full_tag_close'] = '</<ul></div></div>';
			
			
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li class="paginate_button active"><a href="#">';
			$config['cur_tag_close'] = '</a></li>';
			
			$config['last_link'] = 'Last';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			
			
			$config['first_link'] = 'First';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			
			$config['next_link'] = '<i class="fa fa-angle-right"></i>';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			
			
			$config['prev_link'] = '<i class="fa fa-angle-left"></i>';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			
			
			$CI->pagination->initialize($config);
			//return $CI->pagination->create_links();
        
		}
		
}
?>
