<?php
//error_reporting(0);
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class Admin_template {
		var $template_data = array();

		function load($template = '', $view = '' , $view_data = array(), $return = FALSE){
			$CI = get_instance();

			//define constant
			define("BASEURL",base_url());
			define("IMAGEURL",BASEURL."uploads/");
			define("IMAGE",BASEURL.$CI->config->item('template_path')."img/");

			$data 				= 	$view_data;

			//side title
			$config['site_title'] 		= $CI->config->item('site_title');

			//add meta
			$CI->head->add_meta('viewport', 'width=device-width, initial-scale=1.0, maximum-scale=1');
			// css
			$CI->head->add_css($CI->config->item('bootstrap_min_css'));
			$CI->head->add_css($CI->config->item('animate_min_css'));
			$CI->head->add_css($CI->config->item('font-awesome_css'));
			$CI->head->add_css($CI->config->item('css_font_awesome_css'));
			$CI->head->add_css($CI->config->item('custom_css'));
			$CI->head->add_css($CI->config->item('green_css'));
			$CI->head->add_css($CI->config->item('chosen_css'));
			$CI->head->add_css($CI->config->item('parsley_css'));
			//$CI->head->add_css($CI->config->item('bootstrap_combined_min_css'));
			$CI->head->add_css($CI->config->item('datepicker_css'));
			$CI->head->add_css($CI->config->item('bootstrap_datetimepicker_min_css'));



			// js
			$CI->head->add_js($CI->config->item('ie-emulation-modes-warning_js'));
			$CI->head->add_js($CI->config->item('jquery_min_js'));
			$CI->head->add_js($CI->config->item('jquery-1.11.1.min_js'));
			$CI->head->add_js($CI->config->item('jquery-migrate_js'));
			$CI->head->add_js($CI->config->item('parsley_min_js'));
			$CI->head->add_js($CI->config->item('ie10-viewport-bug-workaround_js'));
			$CI->head->add_js($CI->config->item('bootstrap_min_js'));
			$CI->head->add_js($CI->config->item('jquery_nicescroll_min_js'));
			$CI->head->add_js($CI->config->item('icheck_min_js'));
			$CI->head->add_js($CI->config->item('custom_js'));
			$CI->head->add_js($CI->config->item('chosen_jquery_min_js'));
			$CI->head->add_js($CI->config->item('bootbox_min_js'));

			$CI->head->add_js($CI->config->item('jquery_dataTables'));
			$CI->head->add_js($CI->config->item('dataTables_bootstrap'));
			/* remove library of JS from here and put it at the top "jquery-1.11.1.min_js" */

			$CI->head->add_js($CI->config->item('bootstrap.min_js'));
			$CI->head->add_js($CI->config->item('modernizr_min_js'));
			$CI->head->add_js($CI->config->item('sparkline_js'));
			$CI->head->add_js($CI->config->item('toggles_js'));
			$CI->head->add_js($CI->config->item('retina_min_js'));
			$CI->head->add_js($CI->config->item('bootstrap_datetimepicker_min_js'));
			$CI->head->add_js($CI->config->item('bootstrap_datepicker_js'));
			$CI->head->add_js($CI->config->item('parsely_chinese_js'));

			// Added BY Kratika for the star rating
		//	$CI->head->add_js($CI->config->item('star_rating_js'));
		//	$CI->head->add_js($CI->config->item('star_rating_css'));

			//add inline js code
			$CI->head->add_inline_js("var baseUrl= '".base_url()."' ; ");

			$data['head'] = $CI->head->render_head($config);

			$data['content']	=	$CI->load->view($view, $data,true );

			$data['module']=$CI->router->fetch_class();
			$data['moduleMethod']=$CI->router->fetch_method();
 			$CI->load->view($template, $data);
		}
	}
?>
