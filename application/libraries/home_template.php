<?php 
//error_reporting(0); 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');	
	class Home_template {		
		var $template_data = array();		
	

		function load($template = '', $view = '' , $view_data = array(), $return = FALSE){           
			$CI = get_instance();
		
			//define constant
			define("BASEURL",base_url());
			define("IMAGE",BASEURL.$CI->config->item('template_path')."img/");
			define("IMAGEURL",BASEURL."uploads/");
			define("COS",BASEURL.$CI->config->item('template_path')."contract_of_service/");
			
			$data 				= 	$view_data;
			
			//side title
			$config['site_title'] 		= $CI->config->item('site_title');			
			
			//add meta
			$CI->head->add_meta('viewport', 'width=device-width, initial-scale=1');
			
			//add css
		 
			$CI->head->add_css($CI->config->item('bootstrap_css'));
			$CI->head->add_css($CI->config->item('animate_min_css'));
			$CI->head->add_css($CI->config->item('font-awesome_css'));
			$CI->head->add_css($CI->config->item('css_font_awesome_css'));
			$CI->head->add_css($CI->config->item('signin_css'));
			$CI->head->add_css($CI->config->item('parsley_css'));
			//css for show message
			
			$CI->head->add_css($CI->config->item('jquery_noty_css')); 
			$CI->head->add_css($CI->config->item('noty_theme_default_css')); 
			
			
			//add js
			$CI->head->add_js($CI->config->item('jquery_min_js'));
			$CI->head->add_js($CI->config->item('ie-emulation-modes-warning_js'));
			$CI->head->add_js($CI->config->item('ie10-viewport-bug-workaround_js'));
			$CI->head->add_js($CI->config->item('bootstrap_min_js'));
			$CI->head->add_js($CI->config->item('parsley_min_js'));
			
			//this is common file for all form
			$CI->head->add_js($CI->config->item('common_js'));
			
			
			//add inline js code
			$CI->head->add_inline_js("var baseUrl= '".base_url()."' ; ");

			$data['head'] = $CI->head->render_head($config);			
			$data['content']	=	$CI->load->view($view, $data,true );
			
			$data['module']=$CI->router->fetch_class();
			$data['moduleMethod']=$CI->router->fetch_method();
 			$CI->load->view($template, $data);				
		}
	}

?>
