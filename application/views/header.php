<!-- BEGIN HEADER -->

	<!-- BEGIN HEADER INNER -->
	<?php
	$userName=LoginUserDetails('email');
	$userType=LoginUserDetails('role_id');

?>
<link rel="icon" href="<?php echo base_url('templates/JOD/img/favicon.ico');?>" type="image/x-icon">

<script src= "<?php echo base_url('templates/JOD/javascript/star-rating.js');?>" type="text/javascript"></script>

<body>
<div class="loader" style="display:none" id="loader"><img src="<?php echo IMAGE ?>loader.gif" class="loader-img" ></div>
<?php  $current_page = basename($_SERVER['PHP_SELF']);
if($current_page == 'company_form' || $current_page == 'headquater_form') {
?>
<!--div class="outer-box back-image"-->
<div class="outer-box">
<?php
} else {
?>
<div class="outer-box">
<?php
}
?>
<div class="nav-md">
<nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <!--<button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>-->
          <a href="<?php echo base_url('admin'); ?>"><img class="logo" src="<?php echo IMAGE; ?>logo-small.png"></a>
          <?php if(LoginUserDetails('role_id') != 1){
					$url = base_url('jobs/job_section');
			}else{
					$url = "";
			} ?>
          <form action="<?php echo $url;  ?>" class="navbar-form navbar-right col-xs-9 pull-right" method="POST">
			<input type="text" placeholder="<?php echo $this->lang->line('job_title'); ?>..." class="form-control search-text" name="job_title" value="<?php if(isset($_REQUEST['job_title'])) { echo $_REQUEST['job_title']; }//echo set_value('search'); ?>" >
			<span id="job_title-error" class="help-block help-block-error"><?php echo form_error('job_title'); ?></span>
          </form>
        </div>
        <div class="navbar-collapse" >
          <ul class="nav navbar-nav navbar-right">
            <li class=""> <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
				<?php
					if($userType==1)
					{
						echo $this->lang->line('super_admin'); 
					}
					if($userType==2)
					{
						echo $this->lang->line('hq_manager'); 
					}
					if($userType==3)
					{
						echo $this->lang->line('outlet_manager'); 
					}
					if($userType==5)
					{
						echo $this->lang->line('area_manager'); 
					}
				?>
				<span class=" fa fa-angle-down"></span> </a>

              <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                <li><a href="<?php echo base_url().'admin/profile'; ?>"><i class="fa fa-user pull-right"></i><?php echo $this->lang->line('edit_profile'); ?></a> </li>
                <li><a href="<?php echo base_url().'home/logout'; ?>"><i class="fa fa-sign-out pull-right"></i> Log Out</a> </li>
              </ul>
            </li>
            <li><div class="nav toggle">
                                <a id="menu_toggles"><i class="fa fa-bars"></i></a>
                            </div></li>
          </ul>

        </div>
      </div>
    </nav>


<script>
var BASEURL= '<?php echo base_url(); ?>';
</script>
