<?PHP if ( ! defined('BASEPATH')) exit('No direct script access allowed');  ?>

<?php
//load header
	echo $head;
	 $this->load->view('header');
	$userType=LoginUserDetails('role_id');
$method = $this->router->fetch_method();

?>

<div class="container body">
      <div class="main_container top">
        <div class="col-md-3 left_col left-min">
          <div class="left_col">
            <!-- menu prile quick info -->
            <?php
			if($userType==1) {
			?>
            <div class="profile" >
				<div class="text-center">
					<a href="<?php echo base_url('admin'); ?>">
						<img src="<?php echo IMAGE ?>sponsor-item-1.png" class="profile_img">
					</a>
				</div>
              <h2> <?php echo $this->lang->line('super_admin'); ?> </h2>
            </div>
            <?php } else if($userType==2) {
			$records = getCompanydetails();

			?>
            <div class="profile" >
				<div class="text-center">
					<a href="<?php echo base_url('admin'); ?>">
					<img src="<?php echo checkImageFile(base_url('uploads/company_logo/thumb/'.$records->company_logo)) == true ?  base_url('uploads/company_logo/thumb/'.$records->company_logo) : IMAGE.'/no_image.png' ; ?>" class="profile_img">
                    </a>
				</div>
              <h2> <?php echo $records->company_name." HQ"?> </h2>
            </div>
			<?php
			} else if($userType==3) {
				$records = getCompanydetails();
				$user_account_id = LoginUserDetails('userid');
				$OManager = getOMManagerDetails($user_account_id);
				$outlet_id = $OManager[0]->outlet_id;
				$outlet_details	= getOutletDetail($outlet_id);
				$outlet_logo = $outlet_details->outlet_logo;
			?>
			<div class="profile-top profile" >
				<span class="top-text"><?php echo $records->company_name?> </span>
				<h2 class="area-new"><?php echo LoginUserDetails('user_outlet_name');?></h2>
				<div class="text-center">
					<a href="<?php echo base_url('admin'); ?>">
						<img src="<?php echo checkImageFile(base_url('uploads/outlet_logo/thumb/'.$outlet_logo)) == true ? base_url('uploads/outlet_logo/thumb/'.$outlet_logo) :  IMAGE.'/no_image.png' ; ?>" class="profile_img">
					</a>
				</div>
				<span class="user-name"><?php echo LoginUserDetails('user_first_name').' '.LoginUserDetails('user_last_name') ;?> </span>
              <h2 class="area-new"><?php echo $this->lang->line('fandb_outlet_manager'); ?></h2>
            </div>
			<?php
			} else if($userType==5) {
			$records = getCompanydetails();
			?>
			<div class="profile-top profile" >
				<span class="top-text"><?php echo $records->company_name?> </span>
				<div class="text-center">
					<a href="<?php echo base_url('admin'); ?>">
							<img src="<?php echo checkImageFile(base_url('uploads/company_logo/thumb/'.$records->company_logo)) == true ?  base_url('uploads/company_logo/thumb/'.$records->company_logo) : IMAGE.'/no_image.png' ; ?>" class="profile_img">
					</a>
				</div>
				<span class="user-name"><?php echo LoginUserDetails('user_first_name').' '.LoginUserDetails('user_last_name') ;?> </span>
              <h2 class="area-new"><?php echo $this->lang->line('area_manager'); ?> </h2>
            </div>
			<?php
			}
			?>
            <!-- /menu prile quick info -->

            <!-- sidebar menu -->
            <div class="clearfix"></div>


 <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
	<?php $current_page = basename($_SERVER['PHP_SELF']); ?>
<?php if($userType == 1){ ?>
  <div class="panel panel-default">
    <div class="" role="tab" id="headingOne">
      <ul class="nav side-menu">
		<li class="<?php if ($current_page == "admin"){ echo "active"; }?>">
		<a href="<?php echo base_url('admin'); ?>">
          <i class="fa fa-dashboard"></i> <?php echo $this->lang->line('dashboard') ?></a>
        </li>
      </ul>
    </div>
  </div>

  <div class="panel panel-default">
    <div class="" role="tab" id="headingOne">
      <ul class="nav side-menu">
		<li class="<?php $in1 = ""; if ($current_page == "company_section" || $current_page == "company_form"){ echo "active"; $in1 = "in"; }?>">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          <i class="fa fa-file"></i> <?php echo $this->lang->line('company') ?></a>
        </li>
      </ul>
    </div>
    <div id="collapseOne" class="panel-collapse collapse <?php echo $in1; ?>" role="tabpanel" aria-labelledby="headingOne">
      <div class="panel-body empty_padding">
	   <ul class="sub-postings">
      <li class="current-page <?php if($current_page == "company_section"){ echo "sub-postings-active"; } ?> "> <a href="<?php echo base_url('admin/company_section'); ?>"><?php echo $this->lang->line('company_list') ?></a> </li>
      <li class="current-page <?php if($current_page == "company_form"){ echo "sub-postings-active"; } ?> "> <a href="<?php echo base_url('admin/company_form'); ?>"><?php echo $this->lang->line('add_company') ?></a> </li>
      <li></li>
      </ul>
      </div>
    </div>
  </div>

  <div class="panel panel-default">
    <div class="" role="tab" id="headingTwo">
      <ul class="nav side-menu">
		<li class="<?php $in2 = ""; if ($current_page == "headquater_section" || $current_page == "headquater_form"){ echo "active"; $in2 = "in"; }?>">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
          <i class="fa fa-user"></i><?php echo $this->lang->line('hq_user') ?></a>
        </li>
      </ul>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse <?php echo $in2; ?>" role="tabpanel" aria-labelledby="headingTwo">
      <div class="panel-body empty_padding">
	   <ul class="sub-postings">
      <li class="current-page <?php if($current_page == "headquater_section"){ echo "sub-postings-active"; } ?> "> <a href="<?php echo base_url('admin/headquater_section'); ?>"> <?php echo $this->lang->line('hq_user_list') ?> </a> </li>
      <li class="current-page <?php if($current_page == "headquater_form"){ echo "sub-postings-active"; } ?> "> <a href="<?php echo base_url('admin/headquater_form'); ?>"> <?php echo $this->lang->line('add')." ".$this->lang->line('hq_user'); ?> </a> </li>
      <li></li>
      </ul>
      </div>
    </div>
  </div>

   <div class="panel panel-default">
    <div class="" role="tab" id="heading_job_type">
      <ul class="nav side-menu">
		<li class="<?php $in3 = ""; if ($current_page == "job_type_section" || $current_page == "job_type_form"){ echo "active"; $in3 = "in"; }?>">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_job_type" aria-expanded="true" aria-controls="collapse_job_type">
          <i class="fa fa-search"></i><?php echo $this->lang->line('job_type'); ?></a>
        </li>
      </ul>
    </div>
    <div id="collapse_job_type" class="panel-collapse collapse <?php echo $in3; ?>" role="tabpanel" aria-labelledby="heading_job_type">
      <div class="panel-body empty_padding">
	   <ul class="sub-postings">
      <li class="current-page <?php if($current_page == "job_type_section"){ echo "sub-postings-active"; } ?> "> <a href="<?php echo base_url('admin/job_type_section'); ?>"><?php echo $this->lang->line('job_type_list'); ?></a> </li>
      <li class="current-page <?php if($current_page == "job_type_form"){ echo "sub-postings-active"; } ?> "> <a href="<?php echo base_url('admin/job_type_form'); ?>"><?php echo $this->lang->line('add')." ".$this->lang->line('job_type'); ?></a> </li>
      </ul>
      </div>
    </div>
  </div>

  <div class="panel panel-default">
    <div class="" role="tab" id="heading_applicant_section">
      <ul class="nav side-menu">
		<li class="<?php $in6 = ""; if ($current_page == "applicant_section" || $current_page == "applicant_form" || $current_page == "applicant_profile"){ echo "active"; $in6 = "in"; }?>">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_applicant_section" aria-expanded="true" aria-controls="collapse_applicant_section">
          <i class="fa fa-bars"></i> <?php echo $this->lang->line('applicants');?></a>
        </li>
      </ul>
    </div>
	<div id="collapse_applicant_section" class="panel-collapse collapse <?php echo $in6; ?>" role="tabpanel" aria-labelledby="heading_applicant_section">
		<div class="panel-body empty_padding">
			<ul class="sub-postings">
				<li class="current-page <?php if($current_page == "applicant_section" || $current_page == "applicant_profile"){ echo "sub-postings-active"; } ?> "> <a href="<?php echo base_url('admin/applicant_section'); ?>"><?php echo $this->lang->line('applicants_list'); ?></a> </li>
				<li class="current-page <?php if($current_page == "applicant_form"){ echo "sub-postings-active"; } ?> "> <a href="<?php echo base_url('admin/applicant_form'); ?>"><?php echo $this->lang->line('add_applicant'); ?></a> </li>
			</ul>
		</div>
    </div>
  </div>

<div class="panel panel-default">
    <div class="" role="tab" id="heading_institute">
      <ul class="nav side-menu">
		<li class="<?php $in7= ""; if ($current_page == "institute_section" || $current_page == "institute_form"){ echo "active"; $in7 = "in"; }?>">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_institute" aria-expanded="true" aria-controls="collapse_institute">
          <i class="fa fa-bars"></i><?php echo $this->lang->line('educational_insti'); ?></a>
        </li>
      </ul>
    </div>
	<div id="collapse_institute" class="panel-collapse collapse <?php echo $in7; ?>" role="tabpanel" aria-labelledby="heading_institute">
		<div class="panel-body empty_padding">
			<ul class="sub-postings">
				<li class="current-page <?php if($current_page == "institute_section"){ echo "sub-postings-active"; } ?> "> <a href="<?php echo base_url('admin/institute_section'); ?>"><?php echo $this->lang->line('educational_institute_list'); ?></a> </li>
				<li class="current-page <?php if($current_page == "institute_form"){ echo "sub-postings-active"; } ?> "> <a href="<?php echo base_url('admin/institute_form'); ?>"><?php echo $this->lang->line('add')." ". $this->lang->line('educational_insti'); ?> </a> </li>
			</ul>
		</div>
    </div>
</div>

  <div class="panel panel-default">
    <div class="" role="tab" id="heading_outlet_section">
      <ul class="nav side-menu">
		<li class="<?php if ($current_page == "outlet_section"){ echo "active"; }?>">
		<a href="<?php echo base_url('admin/outlet_section'); ?>">
          <i class="fa fa-home"></i> <?php echo $this->lang->line('location_list'); ?></a>
        </li>
      </ul>
    </div>
  </div>

  <div class="panel panel-default">
    <div class="" role="tab" id="headingOne">
      <ul class="nav side-menu">
		<li class="<?php if ($current_page == "notification"){ echo "active"; }?>">
		<a href="<?php echo base_url('admin/notification'); ?>">
          <i class="fa fa-envelope"></i><?php echo $this->lang->line('notifications'); ?> </a>
        </li>
      </ul>
    </div>
  </div>

  <div class="panel panel-default">
    <div class="" role="tab" id="headingOne">
      <ul class="nav side-menu">
		<li class="<?php if ($current_page == "billing_records"){ echo "active"; }?>">
		<a href="<?php echo base_url('admin/billing_records'); ?>">
          <i class="fa fa-usd"></i><?php echo $this->lang->line('billing_record'); ?> </a>
        </li>
      </ul>
    </div>
  </div>

   <div class="panel panel-default">
    <div class="" role="tab" id="headingOne">
      <ul class="nav side-menu">
		<li class="<?php if ($current_page == "jod_credits"){ echo "active"; }?>">
		<a href="<?php echo base_url('admin/jod_credits'); ?>">
         <span><img class="credit_icon" src="<?php echo IMAGE; ?>jod-credit-icon-gray.png"></span><?php echo $this->lang->line('jod_credits'); ?> </a>
        </li>
      </ul>
    </div>
  </div>
  
   <div class="panel panel-default">
    <div class="" role="tab" id="heading_payment">
      <ul class="nav side-menu">
		<li class="<?php $in7= ""; if ($current_page == "job_summary" || $current_page == "job_payments"){ echo "active"; $in7 = "in"; }?>">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_payment" aria-expanded="true" aria-controls="collapse_payment">
          <i class="fa fa-money"></i><?php echo $this->lang->line('payments'); ?></a>
        </li>
      </ul>
    </div>
	<div id="collapse_payment" class="panel-collapse collapse <?php echo $in7; ?>" role="tabpanel" aria-labelledby="heading_payment">
		<div class="panel-body empty_padding">
			<ul class="sub-postings">
				<li class="current-page <?php if($current_page == "job_summary"){ echo "sub-postings-active"; } ?> "> <a href="<?php echo base_url('jobs/job_summary'); ?>"><?php echo $this->lang->line('job_summary'); ?></a> </li>
				<li class="current-page <?php if($current_page == "institute_form"){ echo "sub-postings-active"; } ?> "> <a href="<?php echo base_url('jobs/job_payments'); ?>"><?php echo $this->lang->line('job_payments'); ?></a> </li>
			</ul>
		</div>
    </div>
</div>


  <!--- usert type 2 : HQ Manager -->
 <?php } else if($userType == 2) { ?>
  <div class="panel panel-default">
    <div class="" role="tab" id="headingOne">
      <ul class="nav side-menu">
		<li class="<?php if ($current_page == "admin"){ echo "active"; }?>">
		<a href="<?php echo base_url('admin'); ?>">
          <i class="fa fa-dashboard"></i> <?php echo $this->lang->line('dashboard') ?></a>
        </li>
      </ul>
    </div>
  </div>

  <div class="panel panel-default">
    <div class="" role="tab" id="heading_outlet_section">
      <ul class="nav side-menu">
		<li class="<?php $in4 = ""; if ($current_page == "outlet_section" || $current_page == "outlet_form"){ echo "active"; $in4 = "in"; }?>">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_outlet_section" aria-expanded="true" aria-controls="collapse_outlet_section">
          <i class="fa fa-home"></i><?php echo $this->lang->line('job_locations') ?></a>
        </li>
      </ul>
    </div>
    <div id="collapse_outlet_section" class="panel-collapse collapse <?php echo $in4; ?>" role="tabpanel" aria-labelledby="heading_outlet_section">
      <div class="panel-body empty_padding">
	   <ul class="sub-postings">
      <li class="current-page <?php if($current_page == "outlet_section"){ echo "sub-postings-active"; } ?> "> <a href="<?php echo base_url('admin/outlet_section'); ?>"><?php echo $this->lang->line('location_list'); ?></a> </li>
      <li class="current-page <?php if($current_page == "outlet_form"){ echo "sub-postings-active"; } ?> "> <a href="<?php echo base_url('admin/outlet_form'); ?>"><?php echo $this->lang->line('add_location'); ?></a> </li>
      </ul>
      </div>
    </div>
  </div>

  <div class="panel panel-default">
    <div class="" role="tab" id="heading_managers">
      <ul class="nav side-menu">
		<li class="<?php $in5 = ""; if ($current_page == "area_manager_section" || $current_page == "area_manager_form" || $current_page == "outlet_manager_section" || $current_page == "outlet_manager_form" || $method == "area_manager_form"|| $method == "outlet_manager_form"){ echo "active"; $in5 = "in"; }?>">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_managers" aria-expanded="true" aria-controls="collapse_managers">
          <i class="fa fa-user"></i><?php echo $this->lang->line('managers'); ?></a>
        </li>
      </ul>
    </div>
    <div id="collapse_managers" class="panel-collapse collapse <?php echo $in5; ?>" role="tabpanel" aria-labelledby="heading_managers">
      <div class="panel-body empty_padding">
	   <ul class="sub-postings">
      <li class="current-page <?php if($current_page == "area_manager_section"){ echo "sub-postings-active"; } ?> "> <a href="<?php echo base_url('admin/area_manager_section'); ?>"><?php echo $this->lang->line('list_area_manager'); ?></a> </li>
      <li class="current-page <?php if($current_page == "area_manager_form" || $method == "area_manager_form"){ echo "sub-postings-active"; } ?> "> <a href="<?php echo base_url('admin/area_manager_form'); ?>"><?php echo $this->lang->line('add')." ".$this->lang->line('area_manager'); ?></a> </li>
      <li class="current-page <?php if($current_page == "outlet_manager_section"){ echo "sub-postings-active"; } ?> "> <a href="<?php echo base_url('admin/outlet_manager_section'); ?>"><?php echo $this->lang->line('list_location_manager'); ?></a> </li>
      <li class="current-page <?php if($current_page == "outlet_manager_form" || $method == "outlet_manager_form"){ echo "sub-postings-active"; } ?> "> <a href="<?php echo base_url('admin/outlet_manager_form'); ?>"><?php echo $this->lang->line('add')." ".$this->lang->line('location_manager'); ?></a> </li>
      </ul>
      </div>
    </div>
  </div>
<div class="panel panel-default">
    <div class="" role="tab" id="heading_template_section">
      <ul class="nav side-menu">
		<li class="<?php $in4 = ""; if ($current_page == "job_template" || $method == "job_template" || $current_page == "job_template_section"){ echo "active"; $in4 = "in"; }?>">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_template_section" aria-expanded="true" aria-controls="collapse_template_section">
          <i class="fa fa-file"></i><?php echo $this->lang->line('job_templates'); ?></a>
        </li>
      </ul>
    </div>
    <div id="collapse_template_section" class="panel-collapse collapse <?php echo $in4; ?>" role="tabpanel" aria-labelledby="heading_template_section">
      <div class="panel-body empty_padding">
	   <ul class="sub-postings">
      <li class="current-page <?php if($current_page == "job_template_section"){ echo "sub-postings-active"; } ?> "> <a href="<?php echo base_url('jobs/job_template_section'); ?>"><?php echo $this->lang->line('template_list'); ?></a> </li>
      <li class="current-page <?php if($current_page == "job_template" || $method == "job_template"){ echo "sub-postings-active"; } ?> "> <a href="<?php echo base_url('jobs/job_template'); ?>"><?php echo $this->lang->line('add_job_templates'); ?></a> </li>
      </ul>
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="" role="tab" id="headingOne">
      <ul class="nav side-menu">
		<li class="<?php if ($current_page == "notification"){ echo "active"; }?>">
		<a href="<?php echo base_url('jobs/notification'); ?>">
          <i class="fa fa-envelope"></i><?php echo $this->lang->line('notifications'); ?> </a>
        </li>
      </ul>
    </div>
  </div>

<!-- JOD credit HQ section added by krati -->
  <div class="panel panel-default">
    <div class="" role="tab" id="headingOne">
      <ul class="nav side-menu">
		<li class="<?php if ($current_page == "hq_jod_credits"){ echo "active"; }?>">
		<a href="<?php echo base_url('admin/hq_jod_credits'); ?>">
         <span><img class="credit_icon" src="<?php echo IMAGE; ?>jod-credit-icon-gray.png"></span><?php echo $this->lang->line('jod_credits'); ?> </a>
        </li>
      </ul>
    </div>
  </div>
  <!--- Added for Job Template section By Krati on date 28 Sept 2015 -->

  <!-- user type 3 : outlet manager and user type 5 : Area Manager  -->
 <?php } else if($userType == 3 || $userType == 5) { ?>
  <div class="panel panel-default">
    <div class="" role="tab" id="headingOne">
      <ul class="nav side-menu">
		<li class="<?php if ($current_page == "admin"){ echo "active"; }?>">
		<a href="<?php echo base_url('admin'); ?>">
          <i class="fa fa-dashboard"></i> <?php echo $this->lang->line('dashboard'); ?></a>
        </li>
      </ul>
    </div>
  </div>
 <?php  if($userType == 5 ) { ?>
  <div class="panel panel-default">
    <div class="" role="tab" id="heading_outlet_section">
      <ul class="nav side-menu">
		<li class="<?php if ($current_page == "outlet_section" || $method == "outlet_profile"){ echo "active"; }?>">
		<a href="<?php echo base_url('admin/outlet_section'); ?>">
          <i class="fa fa-home"></i> <?php echo $this->lang->line('location_list'); ?>
          </a>
        </li>
      </ul>
    </div>
  </div>
  <?php } ?>
 <div class="panel panel-default">
    <div class="" role="tab" id="heading_managers">
      <ul class="nav side-menu">
		<li class="<?php $in5 = ""; if ($current_page == "open_job_section" || $current_page == "active_job_section" || $current_page == "job_section" || $current_page == "complete_job_section" || $current_page == "job_form" || $current_page == "job_history_section" || $method == "job_detail" || $method =="job_form" || $method == "get_hired_applicants"){ echo "active"; $in5 = "in"; }?>">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_managers" aria-expanded="true" aria-controls="collapse_managers">
          <i class="fa fa-user"></i><?php echo $this->lang->line('job_posting'); ?></a>
        </li>
      </ul>
    </div>


    <div id="collapse_managers" class="panel-collapse collapse <?php echo $in5; ?>" role="tabpanel" aria-labelledby="heading_managers">
      <div class="panel-body empty_padding">
	  <ul class="sub-postings">
    <li class="current-page <?php if($current_page == "job_form"){ echo "sub-postings-active"; } ?> "> <a href="<?php echo base_url('jobs/job_form'); ?>"><?php echo $this->lang->line('create_new_job'); ?></a> </li>
		  <li class="current-page <?php if($current_page == "job_section"){ echo "sub-postings-active"; } ?> "> <a href="<?php echo base_url('jobs/job_section'); ?>"><?php echo $this->lang->line('all_job_posting'); ?></a> </li>
		  <li class="current-page <?php if($current_page == "active_job_section" || $method == "get_hired_applicants"){ echo "sub-postings-active"; } ?> "> <a href="<?php echo base_url('jobs/active_job_section'); ?>"><?php echo $this->lang->line('active_job_posting'); ?></a> </li>
		  <li class="current-page <?php if($current_page == "open_job_section"){ echo "sub-postings-active"; } ?> "> <a href="<?php echo base_url('jobs/open_job_section'); ?>"><?php echo $this->lang->line('open_job_posting'); ?></a> </li>
		<!--  <li class="current-page <?php if($current_page == "job_history_section"){ echo "sub-postings-active"; } ?> "> <a href="<?php echo base_url('jobs/job_history_section'); ?>"><?php echo $this->lang->line('history'); ?></a> </li> -->
		  <!--
			The area Manager will able to create jobs in all cases
			If area manger not exist and flag "job created by outlet manager" is true for the outlet then outlet manager will able to create jobs -->
		  <?php $outlet_id =  LoginUserDetails('user_outlet_id');
				$data = getOutletDetail($outlet_id);
				if(($userType == 3 && ($data->area_manager_id == 0 || $data->job_created_by_outlet_manager == 1)) || $userType == 5) {
		  ?>
			
		<?php } ?>
      </ul>
      </div>
    </div>
  </div>
<?php if($userType == 3) { ?>
 <div class="panel panel-default">
    <div class="" role="tab" id="headingOne">
      <ul class="nav side-menu">
		<li class="<?php if($method == "outlet_profile"){ echo "active"; } ?>">
			<?php $outlet_id =  LoginUserDetails('user_outlet_id');?>
		<a href="<?php echo base_url('admin/outlet_profile/'.$outlet_id);?>">
          <i class="fa fa-home"></i><?php echo $this->lang->line('outlet_profile'); ?></a>
        </li>
      </ul>
    </div>
  </div>
  <?php  } ?>

  <div class="panel panel-default">
    <div class="" role="tab" id="headingOne">
      <ul class="nav side-menu">
		<li class="<?php if ($current_page == "profile"){ echo "active"; }?>">
		<a href="<?php echo base_url('admin/profile');?>" >
          <i class="fa fa-user"></i><?php echo $this->lang->line('account_setting'); ?></a>
        </li>
      </ul>
    </div>
  </div>
<?php if($userType == 5) { ?>
    <div class="panel panel-default">
    <div class="" role="tab" id="headingOne">
      <ul class="nav side-menu">
		<li class="<?php if ($current_page == "outlet_level_jod_credits"){ echo "active"; }?>">
		<a href="<?php echo base_url('admin/outlet_level_jod_credits'); ?>">
         <span><img class="credit_icon" src="<?php echo IMAGE; ?>jod-credit-icon-gray.png"></span><?php echo $this->lang->line('jod_credits'); ?> </a>
        </li>
      </ul>
    </div>
  </div>
 <?php } ?>

 <?php if($userType == 3) { ?>
    <div class="panel panel-default">
    <div class="" role="tab" id="headingOne">
      <ul class="nav side-menu">
		<li class="<?php if ($current_page == "outlet_jod_credit"){ echo "active"; }?>">
		<?php $outlet_id =  LoginUserDetails('user_outlet_id');?>
		<a href="<?php echo base_url('admin/outlet_jod_credit/'.$outlet_id); ?>">
         <span><img class="credit_icon" src="<?php echo IMAGE; ?>jod-credit-icon-gray.png"></span><?php echo $this->lang->line('jod_credits'); ?></a>
        </li>
      </ul>
    </div>
  </div>
 <?php } ?>
 
  <?php } ?>
</div>
          </div>
        </div>

        <!-- page content -->
         <?php echo $content ?>
        <!-- /page content -->

  <?php
//load header
	 $this->load->view('footer');
?>
 </body>
</html>
