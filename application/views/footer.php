<!--div class="page-footer">
	<div class="page-footer-inner copyright">
		 2015 © On-Demand. All Rights Reserved.
	</div>
	<div class="scroll-to-top" style="display: block;">
		<i class="icon-arrow-up"></i>
	</div>
</div-->

<!-- to ser global message -->
<script>
$.listen('parsley:field:error', function(){
   $('#loader').fadeOut();
});
$(document).ready( function () {
$('div.dataTables_filter input').attr('placeholder', 'Search...');
	if ($('#table3 tr').length <= 2) {  // 2 becoz one is thead and other is no record tr
		if($('#table3 tr td').length <= 1) {
			$('.dataTables_paginate').hide();
			$('.dataTables_length').hide();
			$('.dataTables_filter').hide();
		}	
	}
	
	if (document.getElementsByTagName) {
		var inputElements = document.getElementsByTagName('input');
		for (i=0; inputElements[i]; i++) {
				inputElements[i].setAttribute('autocomplete','off');
			}
	}
} );
$(".alert").fadeOut( 5000 );
$('#contact_no').keyup(function () { 
    this.value = this.value.replace(/[^0-9\.]/g,'');
});
</script>
<script>
    jQuery(document).ready(function () {
        $("#input-21f").rating({
            starCaptions: function(val) {
                if (val < 3) {
                    return val;
                } else {
                    return 'high';
                }
            },
            starCaptionClasses: function(val) {
                if (val < 3) {
                    return 'label label-danger';
                } else {
                    return 'label label-success';
                }
            },
            hoverOnClear: false
        });
        
        $('#rating-input').rating({
              min: 0,
              max: 5,
              step: 1,
              size: 'lg',
              showClear: false
           });
           
        $('#btn-rating-input').on('click', function() {
            $('#rating-input').rating('refresh', {
                showClear:true, 
                disabled:true
            });
        });
        
        
        $('.btn-danger').on('click', function() {
            $("#kartik").rating('destroy');
        });
        
        $('.btn-success').on('click', function() {
            $("#kartik").rating('create');
        });
        
        $('#rating-input').on('rating.change', function() {
            alert($('#rating-input').val());
        });
        
        
        $('.rb-rating').rating({'showCaption':true, 'stars':'3', 'min':'0', 'max':'3', 'step':'1', 'size':'xs', 'starCaptions': {0:'status:nix', 1:'status:wackelt', 2:'status:geht', 3:'status:laeuft'}});
    });
</script>

