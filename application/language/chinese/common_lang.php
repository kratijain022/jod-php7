<?PHP if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* ------------------------ Super Admin dashboard -----*/
$lang['home'] 					= "家";
$lang['dashboard'] 				= "仪表板";
$lang['add_company'] 			= "添加公司";
$lang['add_company'] 			= "添加公司";
$lang['add_hq_manager'] 		= "添加HQ经理";
$lang['notifications'] 			= "通知";
$lang['reports_charts'] 		= "(報告/圖表)";

/* user prfile page */

$lang['working_history'] 		= "工作历史";
$lang['years_of_experience']	= "年工作经验";
$lang['hour_completed_with']	= "小时已完成";
$lang['average_rating']			= "平均评分";
$lang['total_votes']			= "总投票";
$lang['feedback']				= "反馈";
$lang['no_feedback_message']	= "没有反馈找到此用户";
$lang['your_open_job']			= "你的开放工作";
$lang['has_applied_for']		= "还申请";
$lang['not_applied_for_jobs']	= "目前不适用于您的工作";
$lang['your_active_job_that']	= "您的活动作业";
$lang['has_selected_for']		= "选择了";
$lang['not_selected_for_jobs']	= "目前未被选中为您的工作。";

/* Outlet Profile screen */
$lang['dollars_spent_on_wages_last_month'] = "美元花费上个月的工资";
$lang['hours_completed_With']			   = "小时已完成";
$lang['JODS']			   				   = "JODs";
$lang['staff_hired']			   		   = "工作人员聘用";
$lang['current_month']			   		   = "这个月";
$lang['view_all_rejected_members']		   = "查看全部被拒绝的会员申请人";
$lang['completed_job_button_msg']		   = "已完成的作业无法再次完成。";
$lang['completed_d_job_button_msg']		   = "已完成的作业无法再次删除。";
$lang['clock_in_clock_out_period_diff_msg'] = "时钟输入和输出时间超过18小时。你想继续吗?" ;
$lang['job_history_of'] 					= "的工作历史" ;
$lang['no_jobs_found']						= "找不到任务";

/* Job Feedbak Page */
$lang['job_feedback_page_message_1'] 	= "至少有一个申请人已经办理了手续，并且没有退出。请在完成工作之前，先将所有申请人都选出。";
$lang['job_feedback_page_message_2'] 	= "至少有一个申请人已经确认并没有入住，如果没有出现，你可以给申请人一个低评级。否则，请在完成工作之前取消申请人。";
$lang['job_feedback_page_message_3'] 	= "至少有一个申请人没有确认。完成作业将自动拒绝这些申请人。";
$lang['feedback'] 						= "反馈";
$lang['grade_raring'] 					= "请根据1到5星级评分您的申请人。 5是最好的。";
$lang['complete'] 						= "完成";
$lang['warning_msg1'] 					= "所有申请人都没有承认这份工作，所以你可以继续完成工作。完成工作将自动拒绝这些申请人。";
$lang['no_applicants_ack'] 				= "没有申请人已经承认这份工作。";
$lang['no_ack_all_cancel']				= "所有申请人都已取消工作或被系统或经理拒绝。所以你可以继续完成工作。";
$lang['additional_comments']			= "补充评论";
$lang['total_meal_hour']				= "总膳食小时";

/* ------------- ----------------- Data updation messages --------------------------------*/
$lang['job_added_successfully']						=  "作业成功添加";
$lang['job_updated_successfully']					=  "作业已成功更新";
$lang['job_copied_successfully']					=  "作业成功复制";
$lang['template_added_successfully']				=  "工作模板成功添加";
$lang['template_updated_successfully']				=  "工作模板已成功更新";
$lang['template_status_updated_successfully']		=  "作业模板状态已成功更新";
$lang['job_started_successfully']					=  "作业成功启动";
$lang['applicant_already_hired']					=  "申请人已经被聘用这项工作";
$lang['job_not_started']							=  "作业未启动";
$lang['select_atleast_one_candidate']				=  "请选择至少一个候选人";
$lang['job_already_completed']						=  "此工作已由另一位经理完成。";
$lang['job_cancel_msg']								=  "一个作业取消成功";
$lang['job_already_cancel_msg']						=  "此作业已被另一位管理员取消。";
$lang['job_completed_successfully']					=  "作业已成功完成";
$lang['max_length_msg']								=  "此值太长。它应该有8个字符。";
$lang['min_length_msg']								=  "此值太短。它应该有8个字符。";
$lang['super_admin']								=  "超级管理员";
$lang['hq_manager']									=  "总部经理";
$lang['outlet_manager']								=  "出口经理"; 
$lang['job_locations']								=  "工作地点";
$lang['add_location']								=  "添加位置";
$lang['managers']									=  "经理";
$lang['job_templates']								=  "工作模板";
$lang['add_job_templates']							=  "添加作业模板";
$lang['outlet_profile']								=  "出口配置文件";
$lang['fandb_outlet_manager']						=  "F＆B插座经理";
$lang['meal_break_time_msg']						=  "膳食休息时间不应大于总工作时间。";
$lang['required_clock_inout']						=  "时钟输入日期和时钟输出日期不应等于空白.";
$lang['clock_in_clock_out_req']						=  "请填写时钟和时钟输出日期。";
$lang['clock_out_grt_then_clock_in']				=  "时钟输出日期时间应大于时钟输入日期时间。";

