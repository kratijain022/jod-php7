<?PHP if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* ------------------------- Company List ---------------------------- */
$lang['company_list'] 					= "公司名单";
$lang['created_date'] 					= "创建日期";
$lang['company_name'] 					= "公司名";
$lang['address'] 						= "地址";
$lang['action'] 						= "行动";
$lang['hq_user'] 						= "HQ-用户";
$lang['credits'] 						= "学分";
$lang['edit_hq_user'] 					= "编辑HQ-用户";
$lang['edit'] 							= "编辑";
$lang['add'] 							= "加";
$lang['enable'] 						= "启用";
$lang['disable'] 						= "关闭";
$lang['log_out']						= "登出";

/*----------------------------- Company Form --------------*/
$lang['edit_company'] 					= "编辑公司"; 
$lang['company_registration'] 			= "公司注册"; 
$lang['create']							= "创建";
$lang['update']							= "更新";
$lang['basic_details']					= "更新";
$lang['company_logo']					= "公司徽标";
$lang['company_main_address']			= "公司主营地址";
$lang['contract_for_service']			= "劳务合同";
$lang['business_registration_no']		= "商业登记号码";
$lang['minimum_jod_credit_limit']		= "最小JOD信用额度";
$lang['image_not_supported_alert'] 		= "JPEG，JPG和PNG甲的支持";
$lang['pdf_not_supported_alert'] 		= "只有PDF文件的支持！";

/*---- HQ user List Form ----------------------- */
$lang['hq_user_list'] 					= "HQ用户列表"; 
$lang['name'] 							= "名称"; 
$lang['company'] 						= "公司"; 
$lang['contact'] 						= "联系";
$lang['edit_hq_manager']   				= "编辑HQ经理";
$lang['hq_manager_registartion']		= "HQ经理注册";
$lang['attach_to_company']				= "连接到公司";
$lang['hq_form_heading']				= "公司总部主要联系人用户详细信息";
$lang['first_name']						= "名字";
$lang['last_name']						= "姓";
$lang['job_title']						= "职称";
$lang['contact_no']						= "联系电话";
$lang['work_email']						= "工作电子邮件";

/* ---------------- Job Type List Form --------------- */
$lang['job_type_list']					=  "作业类型列表";
$lang['job_type_name']					=  "作业类型名称";
$lang['job_type']						=  "作业类型";
$lang['add_new']						=  "添新";

/* ----------------- Applicants List  ------------------- */
$lang['applicants_list'] 				= "申请人名单";
$lang['age'] 							= "年龄";
$lang['experience'] 					= "先进经验";
$lang['total_jobs'] 					= "就业总数";
$lang['sum_earned'] 					= "总和挣";
$lang['rating'] 						= "评分";
$lang['view'] 							= "视图";
$lang['update_applicant']				= "更新个人资料申请";
$lang['add_applicant']					= "申请加入";
$lang['applicant_details']				= "申请人详细";
$lang['unique_id']						= "唯一的ID";
$lang['display_name']					= "显示名称";
$lang['date_of_birth']					= "出生日期";
$lang['school_attend']					= "学校。参加";
$lang['region_number']					= "地区号码";
$lang['is_nea_certified']				= "是NEA认证";
$lang['referral_code']					= "推荐码";

/* ----------------------- Educational Institute List & Form----------------------- */ 
$lang['educational_institute_list']		= "教育机构名单";
$lang['institute_name']					= "学院名称";
$lang['educational_insti']				= "教育学院";

/* ------------------------- Outlets list and form -------------------------- */
$lang['location_list'] 					= "位置列表"; 
$lang['outlet_name'] 					= "餐饮业的出口名称"; 
$lang['new_outlet']						= "新的出路！";
$lang['outlet']							= "出口";

/*---------------------- Billing Record -------------------------------- */
$lang['billing_record']					= "计费记录";
$lang['month']							= "月";
$lang['jan']							= "一月";
$lang['feb']							= "二月";
$lang['mar']							= "游行";
$lang['apr']							= "四月";
$lang['may']							= "可能";
$lang['jun']							= "六月";
$lang['jul']							= "七月";
$lang['aug']							= "八月";
$lang['sept']							= "九月";
$lang['oct']							= "十月";
$lang['nov']							= "十一月";
$lang['dec']							= "十二月";
$lang['year']							= "年";
$lang['outlets']						= "奥特莱斯";
$lang['job']							= "工作";
$lang['time_in']						= "时间";
$lang['time_out']						= "暂停";
$lang['hours']							= "小时";
$lang['wages']							= "工资";
$lang['job_status']						= "作业状态";

/** -------------------  Profile page ------------------------------ */

$lang['account_setting']				= "帐号设定";
$lang['profile_management']				= "档案管理";
$lang['edit_profile']					= "编辑个人资料";
$lang['language']						= "语言";
$lang['password']						= "密码";
$lang['confirm_password']				= "确认密码";
$lang['leave_password_message']			= "保留密码字段为空，如果你不想更新";
$lang['status_change_msg']				= "您确定要更改状态吗";
$lang['active_jobs']					= "活动作业";

/* ------------------- Feedback detail page ------------------------- */

$lang['outlets_feedback'] 				= "Outlet的反馈";
$lang['applicants_feedback'] 			= "申请人的反馈";
$lang['managers_feedback'] 				= "经理的反馈";


/* ----------------------- Error and success messages --------------- */
$lang['company_suc_mesg']				= "公司成功添加";
$lang['company_already_exist']			= "公司已添加 ";
$lang['company_updated_success_msg']	= "公司更新成功";
$lang['company_deleted_success_msg']	= "公司已成功删除";
$lang['company_status_success_msg']		= "公司状态已成功更新";
$lang['hq_added_success_msg']			= "总部经理成功添加";
$lang['hq_updated_success_msg']			= "总部经理成功更新";
$lang['edit_hq_manager_title']			= "编辑总部经理";
$lang['add_hq_manager_title']			= "添加总部经理";
$lang['hq_deleted_success_msg']			= "总部成功删除";
$lang['hq_status_updated_success_msg']	= "总部状态已成功更新";
$lang['hq_list_page_title']				= "总部经理";
$lang['outlet_added_success_msg']		= "出口成功添加";
$lang['outlet_updated_success_msg']		= "插座更新成功";
$lang['outlet_already_added']			= "出口已添加";
$lang['outlet_deleted_successfully']	= "出口成功删除";
$lang['outlet_status_successfully']		= "出口状态已成功更新";
$lang['om_list_page_title']				= "出口经理";
$lang['om_added_successfully']			= "插座经理成功添加";
$lang['om_updated_successfully']		= "插座经理已成功更新";
$lang['om_status_updated_successfully']	= "出口管理器状态已成功更";
$lang['om_unassigned_successfully']		= "出口管理器成功取消分配";
$lang['am_added_successfully']			= "区域经理成功添加";
$lang['am_updated_successfully']		= "区域经理已成功更新";
$lang['am_status_updated_successfully']	= "区域经理状态已成功更新";
$lang['job_type_added_successfully']	= "作业类型已成功添加";
$lang['job_type_updated_successfully']	= "作业类型已成功更新";
$lang['job_type_already_added']			= "作业类型已添加";
$lang['job_type_status_updated_suc_msg'] = "作业类型状态已成功更新";
$lang['applicant_added_successfully'] 	= "申请人成功添加";
$lang['applicant_updated_successfully'] = "申请人已成功更新";
$lang['disable_user_popup_msg'] 		= "您确定要禁用用户?";
$lang['reason_to_disable_user'] 		= "禁用用户的原因";
$lang['Ok'] 							= "好";
$lang['reason_to_enable_user']	 		= "您确定要启用该用户?";
$lang['applicant_status_updated_mesg']	= "申请人状态已成功更新";
$lang['profile_updated_mesg']			= "个人资料已成功更新";
$lang['update_profile_page_title'] 		= "更新个人信息";
$lang['institute_added_successfully'] 	= "学院成功添加";
$lang['institute_updated_successfully'] = "学院更新成功";
$lang['edu_insti_already_exist'] 		= "教育学院已经添加";
$lang['edu_insti_status_updated'] 		= "教育学院状态更新成功";
$lang['jod_credit_error_msg'] 			= "请输入JOD信用和评论";
$lang['jod_credit_assign_suc_msg'] 		= "JOD学分已成功分配";
$lang['hq_manager_not_assigned_err'] 	= "HQ经理尚未分配给此公司";
$lang['no_transac_history_found'] 		= "未找到交易历史记录";
$lang['assign_jod_credit_err_msg'] 		= "你没有足够的JOD学分，你试图分配插座。";
$lang['deduct_jod_credit_err_msg'] 		= "无法扣除超过可用销售点信用额。";
$lang['oultlet_id_company_id_err'] 		= "未找到出口ID或公司ID。";
$lang['adjusted_on'] 					= "调整";
$lang['adjusted_credit'] 				= "调整信用";
$lang['jod_credit_difference'] 			= "信用差异";
$lang['no_adjustment'] 					= "无调整";
$lang['completed_by_jod_admin'] 		= "由JOD管理员完成";
$lang['job_start_date'] 				= "选择作业开始日期";
$lang['bank_name']						= "银行名";
$lang['bank_account_name']				= "银行帐户名称";
$lang['bank_account_number']			= "银行帐号";
$lang['status']							= "状态";
$lang['user_account_id']				= "用户帐户ID";