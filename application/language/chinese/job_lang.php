<?PHP if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* -------------------------------------------------- Notification HQ ----------------- */
$lang['job_completed'] 					= "工作已完成";
$lang['view_feedback']					= "查看反馈";
$lang['new_job_created']				= "新作业已创建";
$lang['approve']						= "批准";
$lang['reject']							= "拒绝";
$lang['no_notification_msg']			= "找不到通知";
$lang['view_open_job']					= "查看打开作业";
$lang['view_job_history']				= "查看工作记录";
$lang['view_active_job']				= "查看活动作业";
$lang['job_completed_u']				= "作业已完成";
$lang['job_cancelled_u']				= "作业已取消";
$lang['applied']						= "应用";
$lang['hired']							= "雇用";
$lang['selected']						= "选择";
$lang['rejected']						= "拒绝";

/* -------------------------- Open Job Posting page ------------------ */
$lang['job_posting'] 					= "招聘启事";
$lang['open_job_posting'] 				= "打开作业发布";
$lang['not_approved'] 					= "不批准";
$lang['edit_job_details'] 				= "编辑作业详细信息";
$lang['more_info'] 						= "更多信息";
$lang['no_open_job_found'] 				= "未找到打开的作业";

/*------------------- Active job Posting Page ------------- */
$lang['active_job_posting']				= "活动作业过帐";
$lang['no_active_job_found']			= "未找到活动作业";

/* -------------------------- All Job Posting page ------------------ */
$lang['all_job_posting'] 				= "所有工作过帐";
$lang['history'] 						= "历史";
$lang['duplicate_job']					= "重复作业";
$lang['job_cancelled'] 					= "作业已取消";

/* ------------------------ Job Template Form --------------------------- */
$lang['create_job_template'] 			= "创建新作业模板";
$lang['job_template_details'] 	 		= "工作模板细节";
$lang['edit_job_template'] 				= "编辑作业模板";
$lang['template_name'] 					= "模板名称";
$lang['select_job_type'] 				= "选择作业类型";
$lang['job_description'] 				= "职位描述";
$lang['start_date'] 					= "开始日期";
$lang['start_time'] 					= "开始时间";
$lang['end_date'] 						= "结束日期";
$lang['end_time'] 						= "时间结束";
$lang['earning_per_hour'] 				= "每小时收益";
$lang['cash'] 							= "现金";
$lang['bank_transfer'] 					= "银行转帐";
$lang['cheque'] 						= "支票";
$lang['special_instuctions'] 			= "特别说明";
$lang['nea_certificate_req'] 			= "NEA证书必需";
$lang['yes'] 							= "是";
$lang['no']								= "没有";
$lang['update_template']				= "更新模板";
$lang['save_template']					= "保存模板";
$lang['template_list']					= "作业模板列表";

/* ---------------------- Job creation Form --------------------- */
$lang['create_new_job']		 	 		= "创建新作业";
$lang['update_job']		 	 			= "更新作业";
$lang['select_job_template']		 	= "选择作业模板";
$lang['select_outlet']		 			= "选择出口";
$lang['select_outlet_manager']		 	= "選擇位置管理器";
$lang['create_job']		 				= "創建作業";
$lang['job_details']	 				= "工作詳細信息";
$lang['edit_job']		 				= "編輯作業";
$lang['date_alert_message']				= "开始日期应大于今天的日期";
$lang['end_date_alert_message']			= "结束日期应大于今天的日期";
$lang['start_time_alert_message']		= "开始时间应大于当前时间";
$lang['end_time_alert_message']			= "结束时间应大于开始时间。";
$lang['end_date_grea_start_date']		= "请确保结束日期大于或等于开始日期";
$lang['clock_out_successfully']			= "成功輸出";

/* ----------------------- Job Detail pages ------------------ */
$lang['applicants']		 	 			= "申请人";
$lang['copy_job']						= "复制作业";
$lang['delete_job']		 	 			= "删除作业";
$lang['payment_method']		 	 		= "付款方法";
$lang['start']		 	 				= "开始";
$lang['end']		 	 				= "结束";
$lang['copy_job_u']		 	 			= "复制作业";
$lang['job_time_overlap']		 	 	= "工作时间重叠";
$lang['self_cancelled']		 	 		= "自我取消";
$lang['view_profile']		 	 		= "查看资料";
$lang['years_old']		 	 			= "岁";
$lang['JOD_completed']		 			= "JOD完成";
$lang['view_all_applicants'] 			= "查看所有应用申请人";
$lang['delete_job_mesg'] 				= " 候选人已经申请这项工作。您是否仍要取消作业?";
$lang['confirm_delete_job_mesg'] 		= "您要取消作业吗？";
$lang['clock_out_u']					= "時鐘輸出";
$lang['clock_out_popup_message']		= "請驗證時鐘輸入和輸出時序，並輸入總中斷時間。";
$lang['job_start_time']					= "作業開始時間";
$lang['job_end_time']					= "作業結束時間";
$lang['total_break_time']				= "總斷點時間";
$lang['wages_per_hour']					= "每小時工資";
$lang['click_here_to_continue']			= "每小時工資";
$lang['view_all_rejected_applicants']	= "查看所有被拒絕的申請人";

/* ------------------------ Applicants list pages --------------------------------- */
$lang['applied_candidates'] 	 		= "应用候选人";
$lang['select'] 						= "选择";
$lang['confirm_selection'] 				= "确认选择";
$lang['back_to_job'] 					= "回到工作";
$lang['no_candidates_found'] 			= "找不到候选人。";
$lang['nea_food_handler'] 				= "NEA食品处理者";
$lang['years_of_exp'] 					= "多年经验";
$lang['job_roles'] 						= "工作角色";
$lang['past_employers'] 				= "过去的雇主";
$lang['no_experience'] 					= "没有经验";
$lang['hour'] 							= "小时";
$lang['active_job_posting'] 			= "活动作业过帐";
$lang['rejected_applicants'] 			= "被拒绝的申请人";
$lang['rejected_due_to_time_overlap']	= "由于时间重叠而被拒绝";
$lang['no_job_payment_summary_exist']	= "此日期没有付款摘要。";
$lang['old_clockin_clockout_details']	= "在时钟的老时钟细节";
$lang['new_clockin_clockout_details']	= "在时钟输出细节的新的时钟";
$lang['comment_field_required']			= "注释不能为空";
$lang['rating_required']				= "请为1至5颗星评分。";
$lang['applicant_done_acknowledgement']	= "申请人已完成确认";
$lang['applicant_not_ack']				= "申请人没有做出承认。";
$lang['job_id']							= "作业ID";
$lang['paid']							= "支付";
$lang['additional_jod_allowance']		= "JOD津贴";
$lang['view_more']						= "查看更多";

