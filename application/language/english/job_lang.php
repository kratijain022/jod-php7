<?PHP if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* -------------------------------------------------- Notification HQ ----------------- */
$lang['job_completed'] 					= "Job Completed";
$lang['view_feedback']					= "View Feedback";	
$lang['new_job_created']				= "New Job created";
$lang['approve']						= "Approve";
$lang['reject']							= "Reject";
$lang['no_notification_msg']			= "No Notifications Found";
$lang['view_open_job']					= "View Open Job";
$lang['view_job_history']				= "View Job History";
$lang['view_active_job']				= "View Active Job";
$lang['job_completed_u']				= "JOB COMPLETED";
$lang['job_cancelled_u']				= "JOB CANCELLED";
$lang['applied']						= "applied";
$lang['hired']							= "hired";
$lang['selected']						= "selected";
$lang['rejected']						= "Rejected";

/* -------------------------- Open Job Posting page ------------------ */
$lang['job_posting'] 					= "Job Posting";
$lang['open_job_posting'] 				= "Open Job Posting";
$lang['not_approved'] 					= "Not Approved";
$lang['edit_job_details'] 				= "Edit Job Details";
$lang['more_info'] 						= "more info";
$lang['no_open_job_found'] 				= "No open jobs are found";
/*------------------- Active job Posting Page ------------- */
$lang['active_job_posting']				= "Active Job Posting";
$lang['no_active_job_found']			= "No active jobs are found.";

/* -------------------------- All Job Posting page ------------------ */
$lang['all_job_posting'] 				= "All Job Posting";
$lang['history'] 						= "History";
$lang['duplicate_job']					= "Duplicate Job";
$lang['job_cancelled'] 					= "Job Cancelled";

/* ------------------------ Job Template Form --------------------------- */
$lang['create_job_template'] 			= "Create a New Job Template";
$lang['job_template_details'] 			= "Job Template Details";
$lang['edit_job_template'] 				= "Edit Job Template";
$lang['template_name'] 					= "Template name";
$lang['select_job_type'] 				= "Select Job Type";
$lang['job_description'] 				= "Job Description";
$lang['start_date'] 					= "Start Date";
$lang['start_time'] 					= "Start Time";
$lang['end_date'] 						= "End Date";
$lang['end_time'] 						= "End Time";
$lang['earning_per_hour'] 				= "Earning Per Hour";
$lang['cash'] 							= "Cash";
$lang['bank_transfer'] 					= "Bank Transfer";
$lang['cheque'] 						= "Cheque";
$lang['special_instuctions'] 			= "Special Instructions";
$lang['nea_certificate_req'] 			= "NEA Certificate Required";
$lang['yes'] 							= "Yes";
$lang['no']								= "No";
$lang['update_template']				= "Update Template";
$lang['save_template']					= "Save Template";
$lang['template_list']					= "List of Job Templates";
/* ---------------------- Job creation Form --------------------- */
$lang['create_new_job']		 	 		= "Create New Job";
$lang['update_job']		 	 			= "Update Job";
$lang['select_job_template']		 	= "Select Job Template";
$lang['select_outlet']		 			= "Select Outlet";
$lang['select_outlet_manager']		 	= "Select Outlet Manager";
$lang['create_job']		 				= "Create Job";
$lang['job_details']		 			= "Job Details";
$lang['edit_job']		 				= "Edit Job";
$lang['date_alert_message']				= "The start date should be greater than today date";
$lang['end_date_alert_message']			= "End date should be greater than Today date";
$lang['start_time_alert_message']		= "Start time Should be greater then Current time.";
$lang['end_time_alert_message']			= "End time Should be greater then Start time.";
$lang['end_date_grea_start_date']		= "Please ensure that the End Date is greater than or equal to the Start Date.";

/* ----------------------- Job Detail pages ------------------ */
$lang['applicants']		 	 			= "Applicants";
$lang['copy_job']		 	 			= "Copy Job";
$lang['delete_job']		 	 			= "Delete Job";
$lang['payment_method']		 	 		= "Payment Method";
$lang['start']		 	 				= "Start";
$lang['end']		 	 				= "End";
$lang['copy_job_u']		 	 			= "COPY JOB";
$lang['job_time_overlap']		 	 	= "Job Time Overlap";
$lang['self_cancelled']		 	 		= "Self Cancelled";
$lang['view_profile']		 	 		= "View Profile";
$lang['years_old']		 				= "years old";
$lang['JOD_completed']		 			= "JODs completed";
$lang['view_all_applicants'] 			= "View All Applied Applicants";
$lang['delete_job_mesg'] 				= " candidate has applied for the job. Do you want to cancel the assignment?";
$lang['confirm_delete_job_mesg'] 		= "Are you want to cancel the job?";
$lang['clock_out_u']					= "CLOCK OUT";
$lang['clock_out_popup_message']		= "Please verify the Clock In and Out timings and enter the Total Break Time.";
$lang['job_start_time']					= "Job Start Time";
$lang['job_end_time']					= "Job End Time";
$lang['total_break_time']				= "Total Break Time";
$lang['wages_per_hour']					= "Wages Per Hour";
$lang['click_here_to_continue']			= "Click here to Continue";
$lang['view_all_rejected_applicants']	= "View All Rejected Applicants";
$lang['clock_out_successfully']			= "Clock Out Successfully";


/* ------------------------ Applicants list pages --------------------------------- */
$lang['applied_candidates'] 			= "Applied Candidates";
$lang['select'] 						= "Select";
$lang['confirm_selection'] 				= "Confirm Selection";
$lang['back_to_job'] 					= "Back to Job";
$lang['no_candidates_found'] 			= "No Candidates Found.";
$lang['nea_food_handler'] 				= "NEA Food Handler";
$lang['years_of_exp'] 					= "Years of Experience";
$lang['job_roles'] 						= "Job Roles";
$lang['past_employers'] 				= "Past Employers";
$lang['no_experience'] 					= "No Experience";
$lang['hour'] 							= "hr";
$lang['active_job_posting'] 			= "Active Job Postings";
$lang['rejected_applicants'] 			= "Rejected Applicants";
$lang['rejected_due_to_time_overlap']	= "Rejected due to Time overlap";
$lang['average_rating']					= "Average Rating";
$lang['no_job_payment_summary_exist']	= "No Payment summary exist for this Date.";
$lang['old_clockin_clockout_details']	= "Old clock in clock out details";
$lang['new_clockin_clockout_details']	= "New clock in clock out details";
$lang['comment_field_required']			= "Comments should not be blank.";
$lang['rating_required']				= "Please rate 1 to 5 stars.";
$lang['applicant_done_acknowledgement']	= "Applicant has completed acknowledgment.";
$lang['applicant_not_ack']				= "Applicant has not done acknowledgment.";
$lang['job_id']							= "Job Id";
$lang['paid']							= "Paid";
$lang['additional_jod_allowance']		= "JOD Allowance";
$lang['view_more']						= "View More";
