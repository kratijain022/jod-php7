<?PHP if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* ------------------------ Super Admin dashboard -----*/

$lang['home'] 					= "Home";
$lang['dashboard'] 				= "Dashboard";
$lang['add_company'] 			= "Add Comapany";
$lang['add_hq_manager'] 		= "Add HQ Manager";
$lang['notifications'] 			= "Notifications";
$lang['reports_charts'] 		= "(Reports/Charts)";

/* user prfile page */
$lang['working_history'] 		= "Working History";
$lang['years_of_experience']	= "Yrs of working experience";
$lang['hour_completed_with']	= "Hours Completed With ";
$lang['average_rating']			= "Average Rating";
$lang['total_votes']			= "Total Votes";
$lang['feedback']				= "Feedback";
$lang['no_feedback_message']	= "No Feedback Found this User.";
$lang['your_open_job']			= "Your Open Job that";
$lang['has_applied_for']		= "has applied for";
$lang['not_applied_for_jobs']	= "is currently not applied for your jobs.";
$lang['your_active_job_that']	= "Your Active Job that";
$lang['has_selected_for']		= "has selected for";
$lang['not_selected_for_jobs']	= " is currently not selected for your jobs.";

/* Outlet Profile screen */
$lang['dollars_spent_on_wages_last_month'] = "dollars spent on <br> wages last month";
$lang['hours_completed_With']			   = "Hours Completed <br> With";
$lang['JODS']			   				   = "JODs";
$lang['staff_hired']			   		   = "Staff Hired";
$lang['current_month']					   = "Current Month";
$lang['view_all_rejected_members']		   = "View All Rejected Members Applicants";
$lang['completed_job_button_msg']		   = "Completed jobs can not be completed again.";
$lang['completed_d_job_button_msg']		   = "Completed jobs can not be deleted again.";
$lang['deleted_job_button_msg']		   	   = "Deleted jobs can not be completed.";
$lang['deleted_c_job_button_msg']		   = "Deleted jobs can not be deleted again." ;
$lang['clock_in_clock_out_period_diff_msg'] = "Clock in and out period is more than 18 hours. Do you want to continue?" ;
$lang['job_history_of'] 					= "Job History of";
$lang['no_jobs_found']						= "No Jobs found";
/* Job Feedbak Page */

$lang['job_feedback_page_message_1'] 	   	= " At least one applicant has clocked in and has not clocked out. Please clock out all applicants before completing the job.";
$lang['job_feedback_page_message_2'] 	   	= " At least one applicant has acknowledged and has not clock in, if it is a no-show, you can give a low rating to the applicant.  Otherwise please cancel the applicant before completing the job.";
$lang['job_feedback_page_message_3'] 	   	= " At least one applicant has not acknowledge.Completing the job will reject these applicants automatically.";
$lang['feedback'] 							= "Feedback";
$lang['grade_raring'] 						= "Please grade your applicants based on 1 to 5 stars. 5 being the best.";
$lang['complete'] 							= "Compelete";
$lang['warning_msg1'] 						= "All applicants has not acknowledged for the job, So you can proceed to complete the job.Completing the job will reject these applicants automatically.";
$lang['no_applicants_ack'] 					= "No Applicants has acknowledged for the job yet.";
$lang['no_ack_all_cancel']					= "All applicants either have cancelled the job or rejected by system or by Manager. So you can proceed to complete the job.";
$lang['additional_comments']				= "Additional Comments";
$lang['total_meal_hour']					= "Total Meal Hour";

/* ------------- ----------------- Data updation messages --------------------------------*/
$lang['job_added_successfully']						=  "Job Added Successfully";
$lang['job_updated_successfully']					=  "Job Updated Successfully";
$lang['job_copied_successfully']					=  "Job Copied Successfully";
$lang['template_added_successfully']				=  "Job Template Added Successfully";
$lang['template_updated_successfully']				=  "Job Template Updated Successfully";
$lang['template_status_updated_successfully']		=  "Job Template Status Updated Successfully";
$lang['job_started_successfully']					=  "Job started successfully";
$lang['applicant_already_hired']					=  "Applicants are already hired for this job";
$lang['job_not_started']							=  "Job not started";
$lang['select_atleast_one_candidate']				=  "Please select atleast one candidate";
$lang['job_already_completed']						=  "This job is already compelted by another manager.";
$lang['job_cancel_msg']								=  "One Job cancel Successfully";
$lang['job_already_cancel_msg']						=  "This job is already canceled by another manager.";
$lang['job_completed_successfully']					=  "Job comepleted successfully";
$lang['max_length_msg']								=  "This value is too long. It should have 8 characters.";
$lang['min_length_msg']								=  "This value is too short. It should have 8 characters.";
$lang['super_admin']								=  "Super Admin";
$lang['hq_manager']									=  "Headquarter Manager";
$lang['outlet_manager']								=  "Outlet Manager";
$lang['job_locations']								=  "Job Locations";
$lang['add_location']								=  "Add Location";
$lang['managers']									=  "Managers";
$lang['job_templates']								=  "Job Templates";
$lang['add_job_templates']							=  "Add Job Template";
$lang['outlet_profile']								=  "Outlet Profile";
$lang['fandb_outlet_manager']						=  "F&B Outlet Manager";
$lang['meal_break_time_msg']						=  "Meal Break Time should not be greater than Total working Hours.";
$lang['required_clock_inout']						=  "Clock In date and and Clock out date should not be equal to blank.";
$lang['clock_in_clock_out_req']						=  "Please fill clock in and clock out date.";
$lang['clock_out_grt_then_clock_in']				=  "Clock Out Date Time Should be greater than Clock In Date Time.";

