<?PHP if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* -------------------------------------------------- Company List ----------------- */
$lang['company_list'] 					= "List of Companies";
$lang['created_date'] 					= "Created Date";
$lang['company_name'] 					= "Company Name";
$lang['address'] 						= "Address";
$lang['action'] 						= "Action";
$lang['hq_user'] 						= "HQ-User";
$lang['credits'] 						= "Credits";
$lang['edit_hq_user'] 					= "Edit HQ-User";
$lang['edit'] 							= "Edit";
$lang['add'] 							= "Add";
$lang['enable'] 						= "Enable";
$lang['disable'] 						= "Disable";
$lang['log_out']						= "Log Out";

/*----------------------------- Company Form --------------*/
$lang['edit_company'] 					= "Edit Company"; 
$lang['company_registration']			= "Company Registration";
$lang['create']							= "Create";
$lang['update']							= "Update";
$lang['basic_details']					= "Basic Details";
$lang['company_logo']					= "Company Logo";
$lang['company_main_address']			= "Company Main Address";
$lang['contract_for_service']			= "Contract for Service";
$lang['business_registration_no']		= "Business Registration No.";
$lang['minimum_jod_credit_limit']		= "Minimum JOD credit Limit";
$lang['image_not_supported_alert'] 		= "JPEG, JPG and PNG formates are supprted.";
$lang['pdf_not_supported_alert'] 		= "Only pdf files are supported!";

/*---- HQ user List Form ----------------------- */

$lang['hq_user_list']					=  "List of HQ User";
$lang['name'] 							=  "Name";
$lang['company'] 						=  "Company";
$lang['contact'] 						=  "Contact";
$lang['edit_hq_manager']				=  "Edit HQ Manager";
$lang['hq_manager_registartion']		=  "HQ Manager Registration";
$lang['attach_to_company']				=  "Attach to Company";
$lang['hq_form_heading']				=  "Company HQ Main Contact User Details";
$lang['first_name']						=  "First Name";
$lang['last_name']						=  "Last Name";
$lang['job_title']						=  "Job Title";
$lang['contact_no']						=  "Contact Number";
$lang['work_email']						=  "Work Email";

/* ---------------- Job Type List Form--------------- */
$lang['job_type_list']					=  "List of Job Types";
$lang['job_type_name']					=  "Job Type Name";
$lang['job_type']						=  "Job Type";
$lang['add_new']						=  "Add New";

/* ----------------- Applicants List  ------------------- */
$lang['applicants_list'] 				= "List of Applicants";
$lang['age'] 							= "Age";
$lang['experience'] 					= "Exprience";
$lang['total_jobs'] 					= "Total Jobs";
$lang['sum_earned'] 					= "Sum Earned";
$lang['rating'] 						= "Rating";
$lang['view'] 							= "View";
$lang['update_applicant']				= "Update Applicant Profile";
$lang['add_applicant']					= "Add Applicant";
$lang['applicant_details']				= "Applicant Details";
$lang['unique_id']						= "Unique Id";
$lang['display_name']					= "Display Name";
$lang['date_of_birth']					= "Date of Birth";
$lang['school_attend']					= "School Attend";
$lang['region_number']					= "Region Number";
$lang['is_nea_certified']				= "Is NEA certified";
$lang['referral_code']					= "Referral Code";

/* ----------------------- Educational Institute List & Form----------------------- */ 
$lang['educational_institute_list']		= "List of Educational Institutes";
$lang['institute_name']					= "Institute Name";
$lang['educational_insti']				= "Educational Institute";

/* ------------------------- Outlets list and form -------------------------- */
$lang['location_list'] 					= "List of Locations"; 
$lang['outlet_name'] 					= "F&B Outlet Name"; 
$lang['new_outlet']						= "New Outlet!";
$lang['outlet']							= "outlet";

/*---------------------- Billing Record -------------------------------- */
$lang['billing_record']					= "Billing Records";
$lang['Month']							= "Month";
$lang['jan']							= "January";
$lang['feb']							= "February";
$lang['mar']							= "March";
$lang['apr']							= "April";
$lang['may']							= "May";
$lang['jun']							= "June";
$lang['jul']							= "July";
$lang['aug']							= "August";
$lang['sept']							= "September";
$lang['oct']							= "October";
$lang['nov']							= "November";
$lang['dec']							= "December";
$lang['year']							= "Year";
$lang['outlets']						= "Outlets";
$lang['job']							= "Jobs";
$lang['time_in']						= "Time In";
$lang['time_out']						= "Time Out";
$lang['hours']							= "Hours";
$lang['wages']							= "Wages";
$lang['job_status']						= "Job Status";

/** -------------------  Profile page ------------------------------ */

$lang['account_setting']				= "Account Settings";
$lang['profile_management']				= "Profile Management";
$lang['edit_profile']					= "Edit Profile";
$lang['language']						= "Language";
$lang['password']						= "Password";
$lang['confirm_password']				= "Confirm Password";
$lang['leave_password_message']			= "Leave password fields blank if you do not want to update.";
$lang['status_change_msg']  			= "Are you sure want to change the status";
$lang['active_jobs']					= "Active Jobs";

/* ------------------- Feedback detail page ------------------------- */

$lang['outlets_feedback'] 				= "Outlet's Feedback";
$lang['applicants_feedback'] 			= "Applicant's Feedback";
$lang['managers_feedback'] 				= "Manager's Feedback";

/* ----------------------- Error and success messages --------------- */
$lang['company_suc_mesg']				= "Company Added Successfully";
$lang['company_already_exist']			= "Company Already Added ";
$lang['company_updated_success_msg']	= "Company Updated Successfully";
$lang['company_deleted_success_msg']	= "Company Deleted Successfully";
$lang['company_status_success_msg']		= "Company Status Updated Successfully";
$lang['hq_added_success_msg']			= "Headquater Manager Added Successfully";
$lang['hq_updated_success_msg']			= "Headquater Manager Updated Successfully";
$lang['edit_hq_manager_title']			= "Edit Headquarters Manager";
$lang['add_hq_manager_title']			= "Add Headquarters Manager";
$lang['hq_deleted_success_msg']			= "Headquater Deleted Successfully";
$lang['hq_status_updated_success_msg']	= "Headquater Status Updated Successfully";
$lang['hq_list_page_title']				= "Headquarters Manager";
$lang['outlet_added_success_msg']		= "Outlet Added Successfully";
$lang['outlet_updated_success_msg']		= "Outlet Updated Successfully";
$lang['outlet_already_added']			= "Outlet Already Added";
$lang['outlet_deleted_successfully']	= "Outlet Deleted Successfully";
$lang['outlet_status_successfully']		= "Outlet Status Updated Successfully";
$lang['om_list_page_title']				= "Outlet Managers";
$lang['om_added_successfully']			= "Outlet Manager Added Successfully";
$lang['om_updated_successfully']		= "Outlet Manager Updated Successfully";
$lang['om_status_updated_successfully']	= "Outlet Manager Status Updated Successfully";
$lang['om_unassigned_successfully']		= "Outlet Manager Unassign Successfully";
$lang['am_added_successfully']			= "Area Manager Added Successfully";
$lang['am_updated_successfully']		= "Area Manager Updated Successfully";
$lang['am_status_updated_successfully']	= "Area Manager Status Updated Successfully";
$lang['job_type_added_successfully']	= "Job Type Added Successfully";
$lang['job_type_updated_successfully']	= "Job Type Updated Successfully";
$lang['job_type_already_added']			= "Job Type Already Added";
$lang['job_type_status_updated_suc_msg'] = "Job Type Status Updated Successfully";
$lang['applicant_added_successfully'] 	= "Applicant Added Successfully";
$lang['applicant_updated_successfully'] = "Applicant Updated Successfully";
$lang['disable_user_popup_msg'] 		= "Are you sure to disable the users?";
$lang['reason_to_disable_user'] 		= "Reason to disable user";
$lang['Ok'] 							= "Ok";
$lang['reason_to_enable_user']	 		= "Are you sure to enable the user?";
$lang['applicant_status_updated_mesg']	= "Applicant Status Updated Successfully";
$lang['profile_updated_mesg']			= "Profile Updated Successfully";
$lang['update_profile_page_title'] 		= "Update Profile";
$lang['institute_added_successfully'] 	= "Institute Added Successfully";
$lang['institute_updated_successfully'] = "Institute Updated Successfully";
$lang['edu_insti_already_exist'] 		= "Educational Institute Already Added";
$lang['edu_insti_status_updated'] 		= "Educational Institute Status Updated Successfully";
$lang['jod_credit_error_msg'] 			= "Please enter JOD credit and also Comments";
$lang['jod_credit_assign_suc_msg'] 		= "JOD credits assigned successfully";
$lang['hq_manager_not_assigned_err'] 	= "HQ Manager is not assigned yet for this company";
$lang['no_transac_history_found'] 		= "No transaction History found";
$lang['assign_jod_credit_err_msg'] 		= "You dont have enough JOD credits, you tried to assign the outlet.";
$lang['deduct_jod_credit_err_msg'] 		= "Cannot deduct more than available outlet credits.";
$lang['adjusted_on'] 					= "Adjusted on";
$lang['adjusted_credit'] 				= "Adjusted JOD Credit";
$lang['jod_credit_difference'] 			= "Credit Difference";
$lang['no_adjustment'] 					= "No adjustment";
$lang['completed_by_jod_admin'] 		= "Completed by JOD Admin";
$lang['job_start_date'] 				= "Select Job Start Date";
$lang['bank_name']						= "Bank Name";
$lang['bank_account_name']				= "Bank Account Name";
$lang['bank_account_number']			= "Bank Account Number";
$lang['status']							= "Status";
$lang['user_account_id']				= "User Account Id";
