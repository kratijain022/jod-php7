<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


	// ------------------------------------------------------------------------
	if ( ! function_exists('randomPassword')) {
		function randomPassword() {
			$alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
			$pass = array();
			$alphaLength = strlen($alphabet) - 1;
			for ($i = 0; $i < 6; $i++) {
				$n = rand(0, $alphaLength);
				$pass[] = $alphabet[$n];
			}
			return implode($pass);

		}
	}


	// ------------------------------------------------------------------------

	/*
	 * @descript: This function is used to get login user details
	 * @param1: $fieldname (string)
	 * @return enter user details
	 */


	if (!function_exists('LoginUserDetails')){

		function LoginUserDetails($fieldname='username'){
			$CI =& get_instance();

			return $CI->session->userdata($fieldname);
		}
	}

	// ------------------------------------------------------------------------

	/*
	 * @descript: This function is used convert date in any formate
	 * @param1: $date (date)
	 * @param2: $formate (string)
	 * @return converted date formate
	 */


	if ( ! function_exists('dateFormate')){

		function dateFormate($date='', $formate="d M Y"){
			$date =($date=='')?date('d M Y'):$date;
			return date($formate,strtotime($date));
		}
	}

	if ( ! function_exists('sendMail')){
		function sendMail($from,$to,$subject='',$content='')
		{
			//echo $from.' '.$to.' '.$subject.' '.$content;die;
			$CI =& get_instance();
			$fromName=$CI->config->item('site_name');
			$CI->load->library('email');
			$config['mailpath'] = '/usr/sbin/sendmail';
			$config['charset'] = 'iso-8859-1';
			$config['wordwrap'] = TRUE;
			$config['mailtype'] = 'html';

			$CI->email->initialize($config);
			$CI->email->from($from,$fromName);
			$CI->email->to($to);
			$CI->email->subject($subject);
			$CI->email->message($content);

			if (!$CI->email->send()) {
				show_error($CI->email->print_debugger());  die;
					return false;
			}
			else {
				return true;
			}
		}
	}

		/*
		* Descrition: Send Email
		* Date : 8th Jan 2014
		* Author :Gagan David
		*/
	if ( ! function_exists('send_email')){
		function send_email($data) {
			$CI =& get_instance();
			$email=$data['recieverId'];
			//$email = "pushprajsisodiya@cdnsol.com";
			//$email = "neerajshukla@cdnsol.com";
			$CI->load->library('email');
			//print_r($data);
			$config['protocol'] = "smtp";
			$config['smtp_host'] = "mail.aptus.com.sg";
			$config['smtp_port'] = "587";
			$config['smtp_user'] = "jobsondemand@aptus.com.sg";
			$config['smtp_pass'] = "1Abcdefg";
			$config['mailpath'] = '/var/qmail/bin/sendmail';
			$config['charset'] = 'iso-8859-1';
			$config['wordwrap'] = TRUE;
			$config['mailtype'] = 'html';

			$CI->email->initialize($config);
			$CI->email->from('jobsondemand@aptus.com.sg', 'Jobs On Demands');
			$CI->email->to($email);
			$CI->email->subject($data['subject']);

			//$mesg = $CI->load->view($data['template'],$data,true);

			$mesg = $data['message'];
			//echo $mesg; die;
			$CI->email->message($mesg);
			//$CI->email->send();
			 if ($CI->email->send())
			{
				$CI->email->print_debugger();
			}else{
				return true;
			}

		}
	}

	// ------------------------------------------------------------------------

	/*
	 * @descript: This function is used convert date in any formate
	 * @param1: $time (time)
	 * @param2: $formate (string)
	 * @return convert time formate
	 */

	if ( ! function_exists('timeFormate')){

		function timeFormate($time='', $formate="H:i:s"){

			$timeConvert = date($formate,strtotime($time));
			return $timeConvert;
		}
	}

	// ------------------------------------------------------------------------
	/**
	 * Get Global Messages
	 * Wriiten By Rajednra Patidar
	 * Date 28 Nov 2014
	 * @access	public
	 * @param	error msg
	 * @return	void
	 */
	if ( ! function_exists('set_global_messages'))
	{
		function set_global_messages($msg='', $type='error', $is_multiple=true)
		{

			$CI =&get_instance();

			$global_messages = array();
			foreach((array)$msg as $v) {
				$global_messages[$type.'Msg'][] = (string)$v;
			}
			$CI->session->set_userdata('global_messages', $global_messages);
		}
	}

	// ------------------------------------------------------------------------
	/**
	 * Get Global Messages
	 * Wriiten By Rajednra Patidar
	 * Date 28 Nov 2014
	 * @access	public
	 * @param	void
	 * @return	string
	 */
	if ( ! function_exists('get_global_messages'))
	{
		function get_global_messages()
		{

			$str = ''; //defined string message containg variable
			$CI =&get_instance(); // create ci instance object
			$k=''; // count containg varible
			$global_messages = (array)$CI->session->userdata('global_messages');

			if($global_messages && is_array($global_messages) &&count($global_messages) > 0) {
				foreach($global_messages as $k => $v) {

					foreach((array)$v as $w) {

						$str.=$w;
					}
				}
			}
			if($global_messages && !empty($global_messages)){
				$CI->session->unset_userdata('global_messages');
			}


			return array('msg'=>$str,'type'=>$k);
		}

	}

	/**
	 * Get Global Messages for all msg single and multiple
	 * Written  Rajenra Patidar
	 * @access	public
	 * @param	void
	 * @return	msg & msg status array
	 */
	//error_reporting(0);
	if ( ! function_exists('getGlobalMessage'))
	{
		function getGlobalMessage()
		{
			$showMSG='';
			$showType='';
			$CI =&get_instance();
			$msg=get_global_messages();
			 if(!empty($msg['msg']) && $msg['type']=="successMsg"){
				$showMSG=$msg['msg'];
				$showType='success';
			 }
			 elseif(!empty($msg['msg']) && $msg['type']=="errorMsg"){
				$showMSG=$msg['msg'];
				$showType='error';
			 }
			if($CI->session->flashdata('error')){
				$showMSG=$CI->session->flashdata('error');
				$showType='error';
			}
			if($CI->session->flashdata('success')){
				$showMSG=$CI->session->flashdata('success');
				$showType='success';
			}
			$allMSG= trim(strip_tags($showMSG));
			$msgArrays=explode(".",$allMSG);
			$showMSG='';
			if($allMSG!='' && !empty($msgArrays)){
				foreach($msgArrays as $value){
					if($value!=''){
						$showMSG.=trim($value).'.<br>';
					}
				}
			}

			$msgArray=array('msg'=>$showMSG,'type'=>$showType);
			return $msgArray;
		}
	}

	// ------------------------------------------------------------------------

	/*
	 * @description: This function is used get data form db table by passing necessary parameter
	 * @param1: $table  // table name
	 * @param2: $field  // necessary field name
	 * @param2: $whereField  // where field name
	 * @param2: $whereValue  // where field value
	 * @param2: $orderBy  // order by filed name
	 * @param2: $order  // order like asc/dsec
	 * @param2: $limit  // limit of record
	 * @param2: $offset // offset
	 * @param2: $resultInArray (boolean) // data return form array/object
	 * @return $res
	 *
	 **/

	if(! function_exists('getDataFromTabel')){
		function getDataFromTabel($table='', $field='*',  $whereField='', $whereValue='', $orderBy='', $order='ASC', $limit=0, $offset=0, $resultInArray=false ){
			$CI =& get_instance();
			$res =  $CI->common_model->getDataFromTabel($table, $field,  $whereField, $whereValue, $orderBy, $order, $limit, $offset, $resultInArray );
			return $res;
		}
	}

	// ------------------------------------------------------------------------

	/*
	 * @description: This function is used get data form db table by passing necessary parameter
	 * @param1: $table  // table name
	 * @param2: $field  // necessary field name
	 * @param2: $whereinField  // whereinField data
	 * @param2: $whereValue  // where field value
	 * @param2: $orderBy  // order by filed name
	 * @param2: $whereNotIn //pass not included id
	 * @return $result
	 *
	 **/
	if(! function_exists('getDataFromTabelWhereWhereIn')){
	function getDataFromTabelWhereWhereIn($table='', $field='*',  $where='',  $whereinField='', $whereinValue='', $orderBy='', $whereNotIn=0){
        $CI =& get_instance();
		$result =  $CI->common_model->getDataFromTabelWhereWhereIn($table, $field,  $whereField, $whereValue, $orderBy, $order, $limit, $offset, $resultInArray );
		return $result;
	    }
    }

	if(! function_exists('objecttoarray')) {
		function objecttoarray($object) {
			$array =  (array) $object;
			return $array;
		}
	}

	if(! function_exists('lang')) {
		function lang($langKey='') {
			$CI =& get_instance();
			return $CI->lang->line($langKey);
		}
	}

	if(! function_exists('set_upload_options')) {
		function set_upload_options($dirPath,$allowedTypes,$fileName="") {
			$config = array();
			$config['upload_path'] = $dirPath;
			$config['allowed_types'] = $allowedTypes;
			$config['overwrite'] = FALSE;
			if($fileName!=""){
				$config['file_name'] = $fileName;
			}
		return $config;
		}
	}

	if(! function_exists('check_email_exist')) {
		function check_email_exist($id,$email) {
			$CI =& get_instance();
			$prefix = $CI->db->dbprefix;
			$query = "SELECT `user_accounts_id` FROM `".$prefix."user_accounts` WHERE `is_delete` = '0' AND `email_id` LIKE '%$email%' AND user_accounts_id != '$id'";
			$result = $CI->db->query($query);
			if($result->num_rows() > 0) {
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}

	if(! function_exists('get_exprience')) {
		function get_exprience($userAccountId) {
			$CI =& get_instance();
			$prefix = $CI->db->dbprefix;
			$query = "SELECT SUM(`length_of_service`) As totalExprience FROM `".$prefix."employment_histories` WHERE `is_delete` = '0' AND `user_account_id` = '$userAccountId'";
			$result = $CI->db->query($query);
			if($result->num_rows()>0) {
				if($result->row()->totalExprience != '') {
					return $result->row()->totalExprience;
				} else {
					return '0';
				}
			} else {
				return '0';
			}
		}
	}

	if(! function_exists('get_total_jobs')) {
		function get_total_jobs($userAccountId) {
			$CI =& get_instance();
			$prefix = $CI->db->dbprefix;
			$query = "SELECT COUNT(`employment_history_id`) As totalJobs FROM `".$prefix."employment_histories` WHERE `is_delete` = '0' AND `user_account_id` = '$userAccountId'";
			$result = $CI->db->query($query);
			if($result->num_rows()>0) {
				return $result->row()->totalJobs;
			} else {
				return '0';
			}
		}
	}

	if(! function_exists('get_outlets_by_area_manager_id')) {
		function get_outlets_by_area_manager_id($areaManagerId) {
			$CI =& get_instance();
			$prefix = $CI->db->dbprefix;
			$query = "SELECT *  FROM `".$prefix."outlets` WHERE `is_delete` = '0' AND `area_manager_id` = '$areaManagerId'";

			$result = $CI->db->query($query);
			if($result->num_rows()>0) {
				return $result->result();
			} else {
				return '0';
			}
		}
	}


	if(! function_exists('get_current_jod_credits_outlets')) {
		function get_current_jod_credits_outlets($areaManagerId) {
			$CI =& get_instance();
			$prefix = $CI->db->dbprefix;
			$query = "SELECT SUM(current_jod_credit)  as total_current_jod_credit FROM `".$prefix."outlets` WHERE `is_delete` = '0' AND `area_manager_id` = '$areaManagerId'";
			$result = $CI->db->query($query);
			if($result->num_rows()>0) {
				return $result->result();
			} else {
				return '0';
			}
		}
	}

	if(! function_exists('get_email_template')) {
		function get_email_template($purpose) {
			$CI =& get_instance();
			$prefix = $CI->db->dbprefix;
			$query = "SELECT * FROM `".$prefix."email_template` WHERE `purpose` = '$purpose'";
			$result = $CI->db->query($query);
			if($result->num_rows()>0) {
				return $result->row();
			} else {
				return '0';
			}
		}
	}

	if(! function_exists('get_age_by_date_of_birth')) {
		function get_age_by_date_of_birth($birthday) {
			$age = strtotime($birthday);
			if($age === false){
				return 0;
			}
			list($y1,$m1,$d1) = explode("-",date("Y-m-d",$age));
            $now = strtotime("now");
			list($y2,$m2,$d2) = explode("-",date("Y-m-d",$now));
			$age = $y2 - $y1;
			if((int)($m2.$d2) < (int)($m1.$d1))
			$age -= 1;
			if($age == $y1 || $age == $y2){
				return 0;
			}
			return $age;
		}
	}

	if(!function_exists('loginCheck')) {
        function loginCheck() {
            if(loggedId()) {
                return TRUE;
            } else {
                redirect(BASEURL);
            }
        }
    }

    if(!function_exists('logedId')) {
         function loggedId() {
            $CI = get_instance();
            $userId  = $CI->session->userdata('userid');
            if($userId>0){
                return $userId;
            }
            else{
                return FALSE;
            }
         }
    }

    if(!function_exists('checkStatus')) {
         function checkStatus() {
            $CI = get_instance();
            $prefix = $CI->db->dbprefix;
            $userId  = $CI->session->userdata('userid');
            $query = "SELECT `status` FROM `".$prefix."user_accounts` WHERE `user_accounts_id` = '$userId'";
			$result = $CI->db->query($query);
            if($result->num_rows()>0){
				$userdata = $result->row();
				$status = $userdata->status;
				if($status == 0){
					$CI->session->unset_userdata('userid');
					$CI->session->unset_userdata('unique_id');
					$CI->session->unset_userdata('role_id');
					redirect('');
				}
			} else {
				$CI->session->unset_userdata('userid');
				$CI->session->unset_userdata('unique_id');
				$CI->session->unset_userdata('role_id');
				redirect('');
			}
         }
    }

	if(!function_exists('getCompanydetails')) {
		function getCompanydetails() {
			$CI = get_instance();
			$companyId  = $CI->session->userdata('company_id');
            $prefix = $CI->db->dbprefix;
            $query = "SELECT * FROM `".$prefix."companies` WHERE `company_id` = '$companyId'";
			$result = $CI->db->query($query);
			if($result->num_rows()>0){
				return $data = $result->row();
			}
		}

	}
	/* Function to get details from the user_accounts table on the basis of the id */
	if(!function_exists('getUserDetailsByID')) {
		function getUserDetailsByID($id) {
			$CI = get_instance();
			$prefix = $CI->db->dbprefix;
			$query = "SELECT * FROM `".$prefix."user_accounts` WHERE `user_accounts_id` = '$id'";
			$result = $CI->db->query($query);
			if($result->num_rows()>0){
			return $data = $result->row();
			}
		}
	}

	/* This function is to get the area manager id of the outlet on the basis of the outlet id */

	if(!function_exists('getOutletDetail')) {
		function getOutletDetail($outlet_id) {
			$CI = get_instance();
			$prefix = $CI->db->dbprefix;
			$query = "SELECT * FROM `".$prefix."outlets` WHERE `outlet_id` = '$outlet_id'";
			$result = $CI->db->query($query);
			if($result->num_rows()>0){
				return $data = $result->row();
			}else{
				return false;
			}
		}
	}


	if(!function_exists('getOutletManagerByOutlet')) {
		function getOutletManagerByOutlet($outlet_id) {
			$CI = get_instance();
			$prefix = $CI->db->dbprefix;
			$query = "SELECT * FROM `".$prefix."outlet_managers` WHERE `outlet_id` = '$outlet_id'";
			$result = $CI->db->query($query);
			if($result->num_rows()>0){
				return $data = $result->result();
			}else{
				return false;
			}
		}
	}

	if(!function_exists('getEmploymentHistory')) {
		function getEmploymentHistory($cid='') {
			$CI = get_instance();
			$query = "SELECT * FROM `jod_employment_histories` WHERE `user_account_id` = '$cid'";
			$result = $CI->db->query($query);
			if($result->num_rows()>0){
				return $data = $result->result();
			}else{
				return false;
			}
		}
	}

	if(!function_exists('getHiredCandidate')) {
		function getHiredCandidate($job_id='') {
			$CI = get_instance();
			//$count = $CI->jobs_model->get_hired_applicants_count($date='', $job_id);
			$query = "SELECT count(*) as candidate_count FROM `jod_job_applicants` WHERE job_id = '$job_id' AND status = '1' and is_delete = 0";
			$result = $CI->db->query($query);
			$data = $result->result();
			$data = $result->result();
			if(!empty($data[0]->candidate_count) && $data[0]->candidate_count != "" ){
				return $data[0]->candidate_count;
			}else{
				return 0;
			}
		}
	}

	if(!function_exists('getAppliedCandidate')) {
		function getAppliedCandidate($job_id='') {
			$CI = get_instance();
			$count = $CI->jobs_model->get_applied_applicants($job_id, 'count',$page="history_page");
			$query = "SELECT count(*) as candidate_count FROM `jod_job_applicants` WHERE job_id = '$job_id' AND (is_delete = 0 AND system_rejected = 0)";
			$result = $CI->db->query($query);
			$data = $result->result();
			if(!empty($data[0]->candidate_count) && $data[0]->candidate_count != "" ){
				return $data[0]->candidate_count;
			}else{
				return 0;
			}
		}
	}

	if(!function_exists('getoutletJObs')){
		function getoutletJObs($job_type="",$outlet_id=""){
				$CI = get_instance();
				$count = $CI->jobs_model->get_open_jobs_by_outletId($outlet_id,$job_type,'count');
				return $count;
		}
	}

	if(!function_exists('getAreaManagerId')){
		function getAreaManagerId($user_account_id){
			$CI = get_instance();
			$query = "SELECT * FROM `jod_area_manager` WHERE `user_accounts_id` = '$user_account_id'";
			$result = $CI->db->query($query);
			if($result->num_rows()>0){
				return $data = $result->result();
			}else{
				return false;
			}
		}
	}

	if(!function_exists('getHQManagerDetails')){
		function getHQManagerDetails($user_account_id){
			$CI = get_instance();
			$query = "SELECT * FROM `jod_headquater_manager` WHERE `user_accounts_id` = '$user_account_id'";
			$result = $CI->db->query($query);
			if($result->num_rows()>0){
				return $data = $result->result();
			}else{
				return false;
			}
		}
	}

	if(!function_exists('getHQManager')){
		function getHQManager(){
			$CI = get_instance();
			$companyId  = $CI->session->userdata('company_id');
			$query = "SELECT * FROM `jod_headquater_manager` WHERE `company_id` = '$companyId'";
			$result = $CI->db->query($query);
			if($result->num_rows()>0){
				return $data = $result->row();
			}else{
				return false;
			}
		}
	}

	if(!function_exists('getOMManagerDetails')){
		function getOMManagerDetails($user_account_id){
			$CI = get_instance();
			$query = "SELECT * FROM `jod_outlet_managers` WHERE `user_accounts_id` = '$user_account_id'";
			$result = $CI->db->query($query);
			if($result->num_rows()>0){
				return $data = $result->result();
			}else{
				return false;
			}
		}
	}

	if(!function_exists('getApplicantDetails')){
		function getApplicantDetails($user_account_id){
			$CI = get_instance();
			$query = "SELECT * FROM `jod_applicants` WHERE `user_account_id` = '$user_account_id'";
			$result = $CI->db->query($query);
			if($result->num_rows()>0){
				return $data = $result->result();
			}else{
				return false;
			}
		}
	}

	if(!function_exists('getTotalvoteCounts')){
		function getTotalvoteCounts($user_account_id){
			$CI = get_instance();
			$query = "SELECT count(feedback_id) as count FROM `jod_applicants_feedback` WHERE `applicant_user_account_id` = '$user_account_id'";
			$result = $CI->db->query($query);
			if($result->num_rows()>0){
				$data = $result->result();
				return $data[0]->count;
			}else{
				return 0;
			}
		}
	}

	if(!function_exists('getAverageRating')){
		function getAverageRating($user_account_id){
			$total_completed_jobs = getTotalvoteCounts($user_account_id);
			$CI = get_instance();
			$query = "SELECT SUM(rating) as total_rating FROM `jod_applicants_feedback` WHERE `applicant_user_account_id` = '$user_account_id'";
			$result = $CI->db->query($query);
			if($result->num_rows()>0){
				$data = $result->result();
				if($total_completed_jobs > 0){
					$avg_rating =  $data[0]->total_rating/$total_completed_jobs;
				}else{
						$avg_rating = 0;
				}
				return round($avg_rating, 2) ;
			}else{
				return 0;
			}
		}
	}

	if(!function_exists('getTotalCompletedHours')){
		function getTotalCompletedHours($user_account_id){
			$CI = get_instance();
			$query = "SELECT 	clock_in_time, clock_out_time FROM `jod_applicants_feedback` WHERE `applicant_user_account_id` = '$user_account_id'";
			$result = $CI->db->query($query);
			if($result->num_rows()>0){
				$data = $result->result();
				$final_hours = 0;
				foreach($data as $row){
					$hours = 0;
					$hours = round((strtotime($row->clock_out_time) - strtotime($row->clock_in_time))/(60*60));
					$final_hours  = $final_hours  + $hours;
				}
				return $final_hours;
			}else{
				 $final_hours = 0;
				 	return $final_hours;
				}
		}
	}

	if(!function_exists('getTotalCompletedHoursJOb')){
		function getTotalCompletedHoursJOb($job_id){
			$CI = get_instance();
			$query = "SELECT applicant_clockIn_dateTime, applicant_clockOut_dateTime FROM `jod_clockIn_clockOut` WHERE `job_id` = '$job_id'";
			$result = $CI->db->query($query);
			if($result->num_rows()>0){
				$data = $result->result();
				$final_hours = 0;
				foreach($data as $row){
					$hours = 0;
					$hours = round((strtotime($row->applicant_clockIn_dateTime) - strtotime($row->applicant_clockIn_dateTime))/(60*60));
					$final_hours  = $final_hours  + $hours;
				}
				return $final_hours;
			}else{
				 $final_hours = 0;
				 	return $final_hours;
				}
		}
	}


	if(!function_exists('applicant_feedback_data')){
		function applicant_feedback_data($user_account_id){
			$res = array();
			$CI = get_instance();
			$query = "SELECT * FROM `jod_applicants_feedback` WHERE `applicant_user_account_id` = '$user_account_id' ";
			$result = $CI->db->query($query);
			if($result->num_rows() > 0){
				$data = $result->result();
				$jobIds = array();
				foreach($data as $row){
					$query = "SELECT outlet_id,job_title FROM `jod_jobs` WHERE `job_id` = '$row->job_id'";
					$result = $CI->db->query($query);
					$outlet = $result->result();
					if($outlet){
						$outlet_data = getOutletDetail($outlet[0]->outlet_id);
						$row->outlet_logo = $outlet_data->outlet_logo;
						$row->job_title  =$outlet[0]->job_title;
						$res[] = $row;
					}
				}
				return $res;
			}else{
				return false;
			}
		}
	}

	if(!function_exists('pdf_link')){
		function pdf_link($company_id){
			$CI = get_instance();
			$query = "SELECT * FROM `jod_companies` WHERE 	company_id = '$company_id'";
			$result = $CI->db->query($query);
			if($result->num_rows() > 0){
				return $result->result();
			}else{
				return false;
			}
		}
	}

	if(!function_exists('areaManagerDetails')){
		function areaManagerDetails($area_managerId){
			$CI = get_instance();
			$query = "SELECT * FROM `jod_area_manager` WHERE area_manager_id = '$area_managerId'";
			$result = $CI->db->query($query);
			if($result->num_rows() > 0){
				return $result->result();
			}else{
				return false;
			}
		}
	}
	if(!function_exists('get_average_rating_outlet')){
		function get_average_rating_outlet($outlet_id){
			$CI = get_instance();
			$query = "SELECT *  FROM `jod_outlet_feedback` WHERE outlet_id = '$outlet_id'";
			$result = $CI->db->query($query);
			$final_rating  = 0;
			if($result->num_rows() > 0){
				$count = $result->num_rows();
				$rating = $result->result();
				$final_rating = 0 ;
				foreach($rating as $rate){
					$final_rating = $rate->rating + $final_rating ;
				}
				$avg_rate  = $final_rating /$count;
				return $avg_rate;
			}else{
				return 0;
			}

		}
	}

		if(!function_exists('get_total_dollar_outlet')){
		function get_total_dollar_outlet($outlet_id){
			$CI = get_instance();
			$query = "SELECT *  FROM `jod_jobs` WHERE outlet_id = '$outlet_id'";
			$result = $CI->db->query($query);
			if($result->num_rows() > 0){
				$jobs = $result->result();
				$total_amt = 0;
				$amount = 0;
				foreach($jobs as $job){
					$last_date_previous_month = date('Y-m-t', strtotime('-1 months'));
					$first_date_previous_month = date('Y-m-1', strtotime('-1 months'));
					$query = "SELECT SUM(total_wages) as total_amount FROM `jod_applicants_transactions` WHERE job_id = '$job->job_id' AND `created_at` > '$first_date_previous_month' AND `created_at` < '$last_date_previous_month'";
					$result = $CI->db->query($query);
					$amount_data  = $result->result();
					//echo $amount_data[0]->total_amount;
					if($amount_data[0]->total_amount != NULL){
						$total_amt = $amount_data[0]->total_amount + $total_amt;
					}
				}
				 return $total_amt;
			}else{
				return 0;
			}

		}
	}

	if(!function_exists('get_total_time_outlet')){
		function get_total_time_outlet($outlet_id){
			$response = array();
			$last_date_previous_month = date('Y-m-t', strtotime('-1 months'));
			$first_date_previous_month = date('Y-m-1', strtotime('-1 months'));
			$CI = get_instance();
			$query = "SELECT *  FROM `jod_jobs` WHERE outlet_id = '$outlet_id' AND is_approved = '1' AND is_applicants_hired = '1' AND is_delete = '0'" ;
			$result = $CI->db->query($query);
 			$job_count = 0;
			if($result->num_rows() > 0){
				$jobs = $result->result();
				$total_time = array();

				foreach($jobs as $job){
					$a_query = "select SUM(total_hour_worked) as total_hour from `jod_applicants_transactions` where outlet_id = '$outlet_id' AND job_id = '$job->job_id' AND `created_at` > '$first_date_previous_month' AND `created_at` < '$last_date_previous_month' ";
					$result = $CI->db->query($a_query);
					$res  = $result->result();
					if(!empty($res[0]->total_hour)){
						$job_count ++;
						$total_time [] = $res[0]->total_hour;
					}else{
						$total_time [] = 0;
					}
				}
				$response ['job_count'] = $job_count;
				$response ['total_time']  = array_sum($total_time);
			}else{
				$response ['total_time'] = 0;
				$response ['job_count'] = 0;
			}
			//print_r($response); die;
		return $response;
		}
	}

	if(!function_exists('get_hours')){
		function get_hours($data ){
			$final_hours = array();
			foreach ($data as $time){
					$hours = round((strtotime($time->clock_out_time) - strtotime($time->clock_in_time))/(60*60));
					array_push($final_hours,$hours);
			}
			return array_sum($final_hours) ;
		}
	}

	if(!function_exists('outlet_applicants')){
		function outlet_applicants($outlet_id){
			$CI = get_instance();
			$first_date = date('Y-m-01');
			$last_date  = date('Y-m-t');
			$query = "SELECT *  FROM `jod_jobs` WHERE outlet_id = '$outlet_id' AND is_applicants_hired = '1'";
			$result = $CI->db->query($query);
			$hired_applicant_count  = 0;
			if($result->num_rows() > 0){
				$jobs = $result->result();
				$total_applicant = array();
				foreach($jobs as $job){
					$applicant_query = "SELECT COUNT(*) as count FROM `jod_clockIn_clockOut` WHERE applicant_clockOut_dateTime  between '$first_date' AND '$last_date' AND job_id = '$job->job_id'";
					$result_applicant = $CI->db->query($applicant_query);
					$count_data  = $result_applicant->result();
					if(!empty($count_data[0]->count)){
						$hired_applicant_count = $count_data[0]->count + $hired_applicant_count;
					}
				}
				return $hired_applicant_count;
			}else{
				return 0;
			}
		}
	}

	if(!function_exists('get_today_hired_applicants_count')){
		function get_today_hired_applicants_count($date){
			$companyId = LoginUserDetails('company_id');
			$CI = get_instance();
			$query = "SELECT outlet_id  FROM `jod_outlets` WHERE company_id = '$companyId'";
			$result = $CI->db->query($query);
			if($result->num_rows() > 0){
				$outlet_ids = $result->result();
				$candidate_count = array();
				foreach ($outlet_ids as $outlet_id){
					$query1 = " SELECT job_id FROM `jod_jobs` WHERE jod_jobs.outlet_id = '$outlet_id->outlet_id' AND  jod_jobs.is_applicants_hired = 1; ";
					$result1 = $CI->db->query($query1);
					$Job_ids = $result1->result();
					$count	= get_hired_today($Job_ids,$date);
					array_push($candidate_count,$count);

				}
				return array_sum($candidate_count);

			}else{
				return 0;
			}
		}
	}

	if(!function_exists('checkImageFile')){
		function checkImageFile($url)
		{
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL,$url);
			// don't download content
			curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.52 Safari/537.17');
			curl_setopt($ch, CURLOPT_NOBODY, 1);
			curl_setopt($ch, CURLOPT_FAILONERROR, true);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
			////echo $url;
			//var_dump(curl_exec($ch));
			//echo $url;
			if(gettype(curl_exec($ch)) == "string"){
				if(curl_exec($ch) == 0)
				{
					$statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
					if ($statusCode == 200) {
						return true;
					}
				}
				else
				{
					return false;
				}
			}else{
				if(curl_exec($ch)!= FALSE)
				{
					$statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
					//echo $statusCode. "======". $url;

					if ($statusCode == 200) {
						return true;
					}
				}
				else
				{
					return false;
				}
			}
		}
	}

	if(!function_exists('get_hired_today')){
		function get_hired_today ($Job_ids,$date){
			$CI = get_instance();
			$arr = array();
			foreach ($Job_ids as $job){
				$start_date = $date. " 00:00:00";
				$end_date = $date." 23:59:00";
				$query = "SELECT count(*) as candidate_count FROM `jod_job_applicants` WHERE job_id = '$job->job_id' AND (hired_at BETWEEN '$start_date' AND '$end_date') AND  status = 1 AND is_delete=0 ";
				$result = $CI->db->query($query);
				$count = $result->result();
				array_push($arr,  $count[0]->candidate_count);

			}
			return array_sum($arr);
		}
	}

	if(!function_exists('getAllNotifications')){
		function getAllNotifications($user_id){
			$CI = get_instance();
			$prev_date = date('Y-m-d 00:00:00', strtotime(date('Y-m-d 00:00:00') .' -1 day'));
			$current_date = date('Y-m-d 23:59:00');

			$query = "SELECT jja.* FROM  jod_job_applicants as jja JOIN jod_jobs as jj ON jj.job_id = jja.job_id WHERE  jj.created_by =  '$user_id' AND jja.status='0' AND jja.created_at < '$current_date' AND jja.created_at > '$prev_date'";
			$result = $CI->db->query($query);
			$count = $result->result();
			return count($count);
		}
	}

	if(!function_exists('get_outlet')){
		function get_outlet() {
			$CI = get_instance();

			$query = "SELECT `jod_outlets`.*, `jod_companies`.`company_name` FROM (`jod_companies`) JOIN `jod_outlets` ON `jod_outlets`.`company_id` = `jod_companies`.`company_id` WHERE `jod_outlets`.`is_delete` = '0' ORDER BY `jod_outlets`.`created_at` desc";
			$result = $CI->db->query($query);
			$count = $result->result();
			return count($count);
		}
	}

	if(!function_exists('get_managers_feedback')){
		function get_managers_feedback($job_id,$user_id) {
			$CI = get_instance();
			$query = "SELECT * FROM (`jod_outlet_feedback`) WHERE `job_id` = '".$job_id."' AND `applicant_user_account_id`='".$user_id."'";
			$result = $CI->db->query($query);
			$dt = $result->row();
			return $dt;
		}
	}

	if(!function_exists('getWages')){
		function getWages($job_id) {
			$CI = get_instance();
			$query = "SELECT SUM(total_wages) as total_wages FROM (`jod_applicants_transactions`) WHERE `job_id` = '".$job_id."'";
			$result = $CI->db->query($query);
			$dt = $result->row();
			return $dt;
		}
	}


	if(!function_exists('get_hq_manager_notification')){
		function get_hq_manager_notification($label='',$pageNo='',$limit='') {
			if ($pageNo!="") {
				$offset = ($pageNo-1)*$limit;
			} else {
				$offset='0';
			}
			$dts2 = 0;
			$company_id = LoginUserDetails('company_id');
			$CI = get_instance();
			$query = "SELECT outlet_id FROM (`jod_outlets`) WHERE `company_id` = '".$company_id."'";
			$result = $CI->db->query($query);
			$dts = $result->result();
			//print_r($dt);die;
			if(!empty($dts)){
				$in_outlets = array();
				foreach($dts as $dt){
					$in_outlets[] = $dt->outlet_id;
				}
				$in_outlets = implode(',',$in_outlets);
				if ($pageNo!="" && $limit!=""){
					$query2 = "SELECT * FROM (`jod_jobs`) WHERE outlet_id IN (".$in_outlets.") AND is_delete='0' AND (is_approved='0' OR is_completed='1') ORDER BY created_at DESC  LIMIT ".$offset.",".$limit;
				}else{
					$query2 = "SELECT * FROM (`jod_jobs`) WHERE outlet_id IN (".$in_outlets.") AND is_delete='0' AND (is_approved='0' OR is_completed='1') ORDER BY created_at DESC ";
				}
				$result2 = $CI->db->query($query2);
				$dts2 = $result2->result();
				//print_r($result2); die;
				//print_r(count($dts2));die;
			}
			if(!empty($label) && $label=='count'){
				if(!empty($dts2)){
					return count($dts2);
				}else{ return 0; }
			}else{
				if($label=='result'){
					return $dts2;
				}else{
						return count($dts2);
				}
			}
		}
	}

	if(!function_exists('getJobRole')) {
		function getJobRole($job_role_id) {
			$CI = get_instance();
			$prefix = $CI->db->dbprefix;
			$query = "SELECT * FROM `".$prefix."job_roles` WHERE `job_role_id` = '$job_role_id'";
			$result = $CI->db->query($query);
			if($result->num_rows()>0){
				return $data = $result->row();
			}else{
				return false;
			}
		}
	}

	if(!function_exists('manage_job_access')){
		function manage_job_access($job_id){
			$flag = true;
			$userId = LoginUserDetails('userid');
			$roleId = LoginUserDetails('role_id');
			$CI = get_instance();
			$query = "SELECT *  FROM `jod_jobs` WHERE job_id = '$job_id'";
			$result = $CI->db->query($query);
			$job_details = $result->result();
			if($roleId == 5) { // area manager
				if($job_details[0]->job_created_by == 0){   // check job is created by area manager or outlet manager
					if($job_details[0]->created_by == $userId){  // if job is created by logged in area manager
						$flag = true;
					}else{
						$flag = false;
					}
				}else{
					// If job is created by OM
					$area_manager_id = getAreaManagerId(LoginUserDetails('userid'));
					$data = get_outlets_by_area_manager_id($area_manager_id[0]->area_manager_id);
					$outlet_ids = array();
					foreach($data as $outlet){
						array_push($outlet_ids,$outlet->outlet_id);
					}
					if(in_array($job_details[0]->outlet_id, $outlet_ids)){
						$flag = true;
					}else{
						$flag = false;
					}
				}
			}elseif($roleId == 3){ // logged in user is outlet manager
				if($job_details[0]->job_created_by == 0){  // if job is created by area manager
					if($job_details[0]->outlet_manager_id == $userId  ) { // check the outlet manager id
						$flag = true;
					}else{
						$flag = false;
					}
				}else{  // if job is created by outlet manager
					$outlet_id =  LoginUserDetails('user_outlet_id');
					if($outlet_id == $job_details[0]->outlet_id) { // if logged in outlet managers belongs to the same outlet
						$flag = true;
					}else{
						$flag = false;
					}
				}
			}else{   // other user
				$flag = false;
			}
			return $flag ;
		}
	}

	if(!function_exists("getOutletManagerById")){
		function getOutletManagerById($outlet_manager_id){
			$CI = get_instance();
			$query = "SELECT * FROM `jod_outlet_managers` WHERE `outlet_manager_id` = '$outlet_manager_id'";
			$result = $CI->db->query($query);
			if($result->num_rows()>0){
				return $data = $result->result();
			}else{
				return false;
			}

		}
	}


	if(!function_exists('check_all_outlet_job')){
		function check_all_outlet_job($outlet_id){
			$CI = get_instance();
			$query = "SELECT user_accounts_id  FROM `jod_outlet_managers` WHERE outlet_id = '$outlet_id'";
			$result = $CI->db->query($query);
			if($result->num_rows() > 0){
				$outlet_m_ids = $result->result();
				foreach ($outlet_m_ids as $outlet_m_id){
					$query1 = " SELECT job_id FROM `jod_jobs` WHERE jod_jobs.outlet_manager_id = '$outlet_m_id->user_accounts_id' AND jod_jobs.is_completed='0' AND jod_jobs.is_approved='1' ";
					$result1 = $CI->db->query($query1);
					$Job_ids = $result1->result();
					if(!empty($Job_ids)){
						return 'exist';
					}
				}

			}else{
				return 'not_exist';
			}
			return 'not_exist';
		}
	}


	if(!function_exists('get_company_feedback')){
		function get_company_feedback() {
			$return = array();
			$company_id = LoginUserDetails('company_id');
			$CI = get_instance();
			if(LoginUserDetails('role_id') == 2 ) { // HQ Manager
				$query = "SELECT * FROM (`jod_outlets`) WHERE `company_id` = '".$company_id."' AND `status`='1'";
				$result = $CI->db->query($query);
				$dts = $result->result();
			}elseif(LoginUserDetails('role_id') == 5) { // Area Manager
				$area_manager_id = getAreaManagerId(LoginUserDetails('userid'));
				$dts = get_outlets_by_area_manager_id($area_manager_id[0]->area_manager_id);
			}
			elseif(LoginUserDetails('role_id') == 3 )  {
				$outlet_id =  LoginUserDetails('user_outlet_id');
				$outlet_data = getOutletDetail($outlet_id );
				$dts[] = $outlet_data ;
			}
			//print_r($dts); die;
			if(!empty($dts)){
				$i=0;
				foreach($dts as $dt){
					$return[$i]['outlet_details'] = $dt;
					$query2 = 	"SELECT jof.*, ja.*, jj.* FROM jod_outlet_feedback AS jof
								JOIN jod_applicants as ja ON ja.user_account_id = jof.applicant_user_account_id
								JOIN jod_jobs as jj ON jj.job_id = jof.job_id
								WHERE jof.outlet_id  = '$dt->outlet_id' ORDER BY jof.job_id ASC ";
					$result2 = $CI->db->query($query2);
					$return[$i]['outlet_data'] = $result2->result();
					$i++;
				}
				return $return;
			}
			return $return;
		}
	}

	if(!function_exists('get_applicants_feedback')){
		function get_applicants_feedback($user_id,$job_id) {
			$CI = get_instance();
			$query = "SELECT * FROM (`jod_applicants_feedback`) WHERE `job_id` = '".$job_id."' AND `applicant_user_account_id`='".$user_id."'";
			$result = $CI->db->query($query);
			$data = $result->row();
			return $data;
		}
	}

	if(!function_exists('total_earned_money')){
		function total_earned_money($user_id) {
			$CI = get_instance();
			$query = "SELECT SUM(total_Amout) as total_money  FROM (`jod_applicants_feedback`) WHERE `applicant_user_account_id`='".$user_id."'";
			//echo $query;
			$result = $CI->db->query($query);
			$data = $result->row();
			return $data;
		}
	}


	if(!function_exists("get_total_count_company_feedback")){
		function get_total_count_company_feedback() {
			$return = array();
			$company_id = LoginUserDetails('company_id');
			$CI = get_instance();
			if(LoginUserDetails('role_id') == 2 ) { // HQ Manager
				$query = "SELECT * FROM (`jod_outlets`) WHERE `company_id` = '".$company_id."' AND `status`='1'";
				$result = $CI->db->query($query);
				$res_data = $result->result();
				$outlet_ids = array();
					foreach($res_data as $outlet){
						array_push($outlet_ids,$outlet->outlet_id);
					}

			}elseif(LoginUserDetails('role_id') == 5) { // Area Manager
				$area_manager_id = getAreaManagerId(LoginUserDetails('userid'));
				$res_data = get_outlets_by_area_manager_id($area_manager_id[0]->area_manager_id);
				$outlet_ids = array();
				foreach($res_data as $outlet){
					array_push($outlet_ids,$outlet->outlet_id);
				}
			}
			elseif(LoginUserDetails('role_id') == 3 )  {
				$outlet_ids =  array(LoginUserDetails('user_outlet_id'));
			}
			$outlet_str = implode(",",$outlet_ids);
			if($outlet_str == ""){
				$outlet_str = 0;
			}
			$CI = get_instance();
			$query = "SELECT SUM(rating) as total_rating, count(*) as total_counts  FROM (`jod_outlet_feedback`) WHERE outlet_id IN ($outlet_str)";
			$result = $CI->db->query($query);
			$data = $result->row();
			if($data->total_counts == 0){
					$data->total_counts = 1;
			}
		//	print_r($data); die;
			return $data;
		}
	}


	function round_up($value, $places)
	{
		$mult = pow(10, abs($places));
		 return $places < 0 ?
		ceil($value / $mult) * $mult :
			ceil($value * $mult) / $mult;
	}

	function get_HQMananger_companyId($companyId){
			$CI = get_instance();
			$query = "SELECT * FROM (`jod_headquater_manager`) WHERE `company_id` = '".$companyId."'";
			$result = $CI->db->query($query);
			$data = $result->row();
			return $data;
	}

	function get_total_assigned_JOD_credit_to_outlets($company_id){
		$CI = get_instance();
		$query = "SELECT SUM(current_jod_credit) as total_sum FROM (`jod_outlets`) WHERE `company_id` = '".$company_id."' AND `status`='1'";
		$result = $CI->db->query($query);
		$res_data = $result->result();
		return $res_data[0];
	}

	function get_job_history($flag,$outlet_id="",$search="",$length=0,$start=0,$orderBy="",$column_name=""){
		$result_arr = array();
		$company_id = LoginUserDetails('company_id');
		$CI = get_instance();
		if($flag != "" && $flag == "hq_level"){
			$CI = get_instance();
			$query_new  = "SELECT  jod_job_applicants.applicant_user_account_id, jod_job_applicants.meal_hours ,jod_job_applicants.job_id,jod_job_applicants.payment_amount_per_hour, ";
			$query_new .= "jod_clockIn_clockOut.applicant_clockIn_dateTime, jod_clockIn_clockOut.applicant_clockOut_dateTime, ";
			$query_new .= "jod_jobs.job_title,jod_jobs.payment_amount, ";
			$query_new .= "jod_outlets.outlet_id , jod_outlets.outlet_name, ";
			$query_new .= "jod_applicants.first_name , jod_applicants.last_name ";
			$query_new .= "FROM jod_job_applicants ";
			$query_new .= "JOIN jod_clockIn_clockOut ON jod_clockIn_clockOut.applicant_user_account_id = jod_job_applicants.applicant_user_account_id AND jod_clockIn_clockOut.job_id = jod_job_applicants.job_id ";
			$query_new .= "JOIN jod_jobs ON jod_job_applicants.job_id = jod_jobs.job_id ";
			$query_new .= "JOIN jod_outlets ON jod_jobs.outlet_id = jod_outlets.outlet_id ";
			$query_new .= "JOIN jod_applicants ON jod_job_applicants.applicant_user_account_id = jod_applicants.user_account_id ";
			$query_new .= "WHERE jod_job_applicants.clockInOutStatus = '2' AND jod_outlets.company_id  = '".$company_id."' AND jod_outlets.status ='1' ";
			if($search !=""){
				$query_new .= " AND (jod_clockIn_clockOut.applicant_clockIn_dateTime LIKE '%".$search."%' OR jod_clockIn_clockOut.applicant_clockOut_dateTime LIKE '%".$search."%' OR jod_outlets.outlet_name LIKE '%".$search."%' OR jod_applicants.first_name LIKE '%".$search."%' OR jod_applicants.last_name LIKE '%".$search."%') ";
			}
			if($column_name!=""){
				$query_new	.= " ORDER BY ".$column_name." ".$orderBy;
			}else{
				$query_new	.= " ORDER BY jod_clockIn_clockOut.applicant_clockIn_dateTime desc ";
			}
			if($length!= 0){
				$query_new .= " LIMIT ".$start .",".$length;
			}

			$result = $CI->db->query($query_new);

			$res_data_applicants = $result->result();
			if(!empty($res_data_applicants)){
				$data_applicant =  job_history_data($res_data_applicants);
					return $data_applicant;
			}else{
				return false;
			}
		}elseif($flag != "" && $flag == "area_manager_outlet_level"){
			$data_applicant = array();
			$user_id = LoginUserDetails('userid');
			if(LoginUserDetails('role_id') == 3 ){
				//echo "help"; die;
				$outlet_id =  LoginUserDetails('user_outlet_id');
				$outlet_data = getOutletDetail($outlet_id);
				if(!empty($outlet_data)){
					$select_job_query = "SELECT jod_jobs.job_id, jod_jobs.job_title, jod_jobs.outlet_id, jod_job_applicants.meal_hours, jod_job_applicants.applicant_user_account_id FROM jod_jobs JOIN jod_job_applicants ON jod_jobs.job_id = jod_job_applicants.job_id WHERE jod_jobs.outlet_id ='".$outlet_data->outlet_id."' ";
					$result_d = $CI->db->query($select_job_query);
					$job_id_data = $result_d->result();
					if(!empty($job_id_data)){
						foreach($job_id_data as $data){
						//	print_r($data); die;
							$data_arr = get_job_details_data($data->outlet_id,$data->job_id,$data->applicant_user_account_id);
							$data_arr["job_id"] = $data->job_id;
							$data_arr["outlet_id"] =$data->outlet_id;
							$data_arr["applicant_user_accounts_id"] = $data->applicant_user_account_id;
							$data_arr["clock_in_date"] = $data;
							$data_applicant[] = $data_arr;

						}
					}
				}
			}else{
				//echo "help"; die;
				// area manager
				$query_new  = "SELECT  jod_job_applicants.applicant_user_account_id, jod_job_applicants.meal_hours ,jod_job_applicants.job_id,jod_job_applicants.payment_amount_per_hour,  ";
				$query_new .= "jod_clockIn_clockOut.applicant_clockIn_dateTime, jod_clockIn_clockOut.applicant_clockOut_dateTime, ";
				$query_new .= "jod_jobs.job_title,jod_jobs.payment_amount, ";
				$query_new .= "jod_outlets.outlet_id , jod_outlets.outlet_name, ";
				$query_new .= "jod_applicants.first_name , jod_applicants.last_name ";
				$query_new .= "FROM jod_job_applicants ";
				$query_new .= "JOIN jod_clockIn_clockOut ON jod_clockIn_clockOut.applicant_user_account_id = jod_job_applicants.applicant_user_account_id AND jod_clockIn_clockOut.job_id = jod_job_applicants.job_id ";
				$query_new .= "JOIN jod_jobs ON jod_job_applicants.job_id = jod_jobs.job_id ";
				$query_new .= "JOIN jod_outlets ON jod_jobs.outlet_id = jod_outlets.outlet_id ";
				$query_new .= "JOIN jod_applicants ON jod_job_applicants.applicant_user_account_id = jod_applicants.user_account_id ";
				$query_new .= "JOIN jod_area_manager ON jod_area_manager.area_manager_id = jod_outlets.area_manager_id ";
				$query_new .= "WHERE jod_job_applicants.clockInOutStatus = '2' AND ";
				$query_new .= "jod_area_manager.user_accounts_id = '".$user_id."'";
				
				if($search !=""){
					$query_new .= " AND (jod_clockIn_clockOut.applicant_clockIn_dateTime LIKE '%".$search."%' OR jod_clockIn_clockOut.applicant_clockOut_dateTime LIKE '%".$search."%' OR jod_outlets.outlet_name LIKE '%".$search."%' OR jod_applicants.first_name LIKE '%".$search."%' OR jod_applicants.last_name LIKE '%".$search."%') ";
				}
				if($column_name!=""){
					$query_new	.= " ORDER BY ".$column_name." ".$orderBy;
				}else{
					$query_new	.= " ORDER BY jod_clockIn_clockOut.applicant_clockIn_dateTime desc ";
				}
				if($length!= 0){
					$query_new .= " LIMIT ".$start .",".$length;
				}
			//	echo $query_new; die;
			$msc = microtime(true);
				$result = $CI->db->query($query_new);
				mysql_query($query_new);
				$msc=microtime(true)-$msc;
				//echo $msc.' seconds'; // in seconds
				//echo 
				log_message("error", "Query in ".$query_new);
				log_message("error","execution time ". ($msc*1000).' milliseconds'); // in millseconds
				$res_data_applicants = $result->result();
					if(!empty($res_data_applicants)){
						$data_applicant =  job_history_data($res_data_applicants);
						return $data_applicant;
					}
					
				}
		}elseif($flag != "" && $flag == "outlet_profile"){
			$data_applicant = array();
			if($outlet_id != 0){
				$outlet_data = getOutletDetail($outlet_id);
				if(!empty($outlet_data)){
					$query_new  = "SELECT  jod_job_applicants.applicant_user_account_id, jod_job_applicants.meal_hours ,jod_job_applicants.job_id,jod_job_applicants.payment_amount_per_hour, ";
					$query_new .= "jod_clockIn_clockOut.applicant_clockIn_dateTime, jod_clockIn_clockOut.applicant_clockOut_dateTime, ";
					$query_new .= "jod_jobs.job_title,jod_jobs.payment_amount, ";
					$query_new .= "jod_outlets.outlet_id , jod_outlets.outlet_name, ";
					$query_new .= "jod_applicants.first_name , jod_applicants.last_name ";
					$query_new .= "FROM jod_job_applicants ";
					$query_new .= "JOIN jod_clockIn_clockOut ON jod_clockIn_clockOut.applicant_user_account_id = jod_job_applicants.applicant_user_account_id AND jod_clockIn_clockOut.job_id = jod_job_applicants.job_id ";
					$query_new .= "JOIN jod_jobs ON jod_job_applicants.job_id = jod_jobs.job_id ";
					$query_new .= "JOIN jod_outlets ON jod_jobs.outlet_id = jod_outlets.outlet_id ";
					$query_new .= "JOIN jod_applicants ON jod_job_applicants.applicant_user_account_id = jod_applicants.user_account_id ";
					$query_new .= "WHERE jod_job_applicants.clockInOutStatus = '2' AND ";
					$query_new .= "jod_outlets.outlet_id = '".$outlet_id."'";
					if($search !=""){
						$query_new .= " AND (jod_clockIn_clockOut.applicant_clockIn_dateTime LIKE '%".$search."%' OR jod_clockIn_clockOut.applicant_clockOut_dateTime LIKE '%".$search."%' OR jod_outlets.outlet_name LIKE '%".$search."%' OR jod_applicants.first_name LIKE '%".$search."%' OR jod_applicants.last_name LIKE '%".$search."%') ";
					}
					if($column_name!=""){
						$query_new	.= " ORDER BY ".$column_name." ".$orderBy;
					}else{
						$query_new	.= " ORDER BY jod_clockIn_clockOut.applicant_clockIn_dateTime desc ";
					}
					if($length!= 0){
						$query_new .= " LIMIT ".$start .",".$length;
					}
					$result = $CI->db->query($query_new);
					$res_data_applicants = $result->result();
					if(!empty($res_data_applicants)){
						$data_applicant =  job_history_data($res_data_applicants);
						return $data_applicant;
					}

				}

			}

		}
	}
	
	function get_job_history_count($flag,$outlet_id=""){
		$result_arr = array();
		$company_id = LoginUserDetails('company_id');
		$CI = get_instance();
		if($flag != "" && $flag == "hq_level"){
			$CI = get_instance();
			
			$oulet_id_query = "SELECT outlet_id from jod_outlets WHERE company_id = '".$company_id."'";
			$result_outlet = $CI->db->query($oulet_id_query);
			$outlet_data = $result_outlet->result();
			$outlet_ids = array();
			foreach($outlet_data as $outlet){
					array_push($outlet_ids,$outlet->outlet_id);
			}
			$outlet_ids_str = implode(',',$outlet_ids);
				
			$query_new = "SELECT count(jod_job_applicant_id) as job_history_count FROM jod_jobs JOIN jod_job_applicants ON jod_jobs.job_id = jod_job_applicants.job_id WHERE jod_jobs.outlet_id IN (".$outlet_ids_str.")  AND jod_job_applicants.completed_by_applicant = '1' AND jod_job_applicants.clockInOutStatus = '2'";
			
			$result = $CI->db->query($query_new);

			$res_data_applicants = $result->result();
			if(!empty($res_data_applicants)){
					return $res_data_applicants[0];
			}else{
				return false;
			}
		}elseif($flag != "" && $flag == "area_manager_outlet_level"){
			$data_applicant = array();
			$user_id = LoginUserDetails('userid');
			if(LoginUserDetails('role_id') == 3 ){
				// outlet Manager
			}else{
				// area manager
				$area_manager = getAreaManagerId($user_id);
				$outlets_data = get_outlets_by_area_manager_id($area_manager[0]->area_manager_id);
				$outlet_ids = array();
				foreach($outlets_data as $outlet){
						array_push($outlet_ids,$outlet->outlet_id);
				}
				$outlet_ids_str = implode(',',$outlet_ids);
					$query_new = "SELECT count(jod_job_applicant_id) as job_history_count FROM jod_jobs JOIN jod_job_applicants ON jod_jobs.job_id = jod_job_applicants.job_id WHERE jod_jobs.outlet_id IN (".$outlet_ids_str.")  AND jod_job_applicants.completed_by_applicant = '1' AND jod_job_applicants.clockInOutStatus = '2' ";
					
				$result = $CI->db->query($query_new);
				$res_data_applicants = $result->result();
					if(!empty($res_data_applicants)){
						return $res_data_applicants[0];
					//return $data_applicant;
					}else{
					 return false;	
					}
			}
			//return  $data_applicant;
		}elseif($flag != "" && $flag == "outlet_profile"){
			$data_applicant = array();
			if($outlet_id != 0){
				$outlet_data = getOutletDetail($outlet_id);
				if(!empty($outlet_data)){
					$query_new = "SELECT count(jod_job_applicant_id) as job_history_count FROM jod_jobs JOIN jod_job_applicants ON jod_jobs.job_id = jod_job_applicants.job_id WHERE jod_jobs.outlet_id IN (".$outlet_id.")  AND jod_job_applicants.completed_by_applicant = '1' AND jod_job_applicants.clockInOutStatus = '2'";
					$result = $CI->db->query($query_new);
					$res_data_applicants = $result->result();
					if(!empty($res_data_applicants)){
						return $res_data_applicants[0];
					}else{
						return false;
					}
				}
			}

		}
	}

	function get_job_details_data($outlet_id,$job_id,$applicant_user_account_id){
		$CI = get_instance();
		$select_query  = "SELECT jod_clockIn_clockOut.*, jod_job_applicants.meal_hours, jod_job_applicants.payment_amount_per_hour FROM jod_clockIn_clockOut JOIN jod_job_applicants ON jod_clockIn_clockOut.job_id = jod_job_applicants.job_id AND jod_clockIn_clockOut.applicant_user_account_id = jod_job_applicants.applicant_user_account_id WHERE jod_clockIn_clockOut.job_id = '".$job_id."' AND jod_clockIn_clockOut.applicant_user_account_id = '".$applicant_user_account_id."'";

		$applicant_result = $CI->db->query($select_query);
		$res_data_applicants = $applicant_result->result();
		//print_r($res_data_applicants);
		$data_array = array();
		if(!empty($res_data_applicants)){
			$data_array["date"]  = date('d M Y', strtotime($res_data_applicants[0]->applicant_clockIn_dateTime));
			$data_array["clock_out_date"]  = date('d M Y', strtotime($res_data_applicants[0]->applicant_clockOut_dateTime));
			$outlet_details = getOutletDetail($outlet_id);
			$data_array["outlet_name"]  = $outlet_details->outlet_name;
			$applicant_det = getApplicantDetails($applicant_user_account_id);
			$user_details = getUserDetailsByID($applicant_user_account_id);
			$data_array["applicant_fullname"] = $applicant_det[0]->first_name." ".$applicant_det[0]->last_name;
			$data_array["unique_id"] = $user_details->unique_id;
			$data_array["clock_in"] = date('H:i', strtotime($res_data_applicants[0]->applicant_clockIn_dateTime));
			$data_array["clock_out"] = date('H:i', strtotime($res_data_applicants[0]->applicant_clockOut_dateTime));
			$data_array["meal_break_time"] =  ($res_data_applicants[0]->meal_hours == null || $res_data_applicants[0]->meal_hours == "") ? "N/A"  : $res_data_applicants[0]->meal_hours ;
			$job_details = get_job_details($job_id);
			if(get_applicant_transaction_data($job_id,$applicant_user_account_id,$outlet_id)!= false){
				
				$meal_time = $res_data_applicants[0]->meal_hours;
				/* check meal break time */
				if($meal_time != "" && $meal_time !="00:00:00"){
					$seconds = strtotime("1970-01-01 $meal_time UTC");
					$meal_break_minutes =  $seconds/60;
				}else{
					$meal_break_minutes = 0;
				}
				$transaction_data = get_applicant_transaction_data($job_id,$applicant_user_account_id,$outlet_id);
				
				/* Total worked hours fetched from Db not having without dedcuted meal break time  */
				$total_hour_without_deduction_meal_break_time  = $transaction_data->total_hour_worked;
				/* Total Minutes after deduction meal break time in minutes */
				$total_minutes_after_deduction_meal_break_time  = ($transaction_data->total_hour_worked*60) - $meal_break_minutes;
				/* Total hours  after deduction meal break time in minutes */
				$total_hours_worked_after_deduction_meal_break_time = $total_minutes_after_deduction_meal_break_time/60.0;
				/* Total hours  worked after deduction meal break time in h:i formate */
				$data_array["total_hours_worked"] =  convert_seconds_to_time(round(($total_hours_worked_after_deduction_meal_break_time*3600))); 
				$data_array["wages_per_hour"] = ($res_data_applicants[0]->payment_amount_per_hour != 0) ? sprintf('%0.2f',$res_data_applicants[0]->payment_amount_per_hour) : sprintf('%0.2f',$job_details[0]->payment_amount) ;
				$data_array["total_jod_credit"] = sprintf('%0.2f',$transaction_data->total_wages);
				$data_array["credit_before_transacion"] = sprintf('%0.2f',$transaction_data->credit_before_transacion);
				$data_array["credit_after_transacion"] = sprintf('%0.2f',$transaction_data->credit_after_transacion);
			}else{
				$meal_time = $res_data_applicants[0]->meal_hours;
				/* check meal break time */
				if($meal_time != "" && $meal_time !="00:00:00"){
					$seconds = strtotime("1970-01-01 $meal_time UTC");
					$meal_break_minutes =  $seconds/60;
				}else{
					$meal_break_minutes = 0;
				}
				/* check difference between both time */
				if( strtotime($res_data_applicants[0]->applicant_clockOut_dateTime) > strtotime($res_data_applicants[0]->applicant_clockIn_dateTime) ){
					$total_minutes = (strtotime($res_data_applicants[0]->applicant_clockOut_dateTime) - strtotime($res_data_applicants[0]->applicant_clockIn_dateTime))/60;
					$hourdiff = $total_minutes / 60;
				}else{
					$hourdiff = 0;
					$total_minutes = 0;
				}
				$total_minutes_after_deduction = $total_minutes - $meal_break_minutes;
				$hours_after_deduction_meal_break_time = $total_minutes_after_deduction/60;
				$data_array["total_hours_worked"] = gmdate ('H:i', $total_minutes_after_deduction*60); 
				$data_array["wages_per_hour"] = ($res_data_applicants[0]->payment_amount_per_hour != 0) ? sprintf('%0.2f',$res_data_applicants[0]->payment_amount_per_hour) : sprintf('%0.2f',$job_details[0]->payment_amount) ;
			
				$data_array["total_jod_credit"] = ($res_data_applicants[0]->payment_amount_per_hour != 0) ? sprintf('%0.2f',$hours_after_deduction_meal_break_time * $res_data_applicants[0]->payment_amount_per_hour) : sprintf('%0.2f',$hours_after_deduction_meal_break_time * $res_data_applicants[0]->payment_amount);
				$data_array["credit_before_transacion"] = "N/A";
				$data_array["credit_after_transacion"] = "N/A";
			}
			
		}
		//print_r($data_array); die;
		return $data_array;
	}

	function get_job_details($job_id){
		$CI = get_instance();
		$query = "SELECT *  FROM `jod_jobs` WHERE job_id = '$job_id'";
		$result = $CI->db->query($query);
		$job_details = $result->result();
		return $job_details;
	}

	function current_month_consumed_jod_credit($outlet_id=0,$company_id=0,$is_area_manager=0){
		$CI = get_instance();
		$first_date = date('Y-m-01');
		$last_date  = date('Y-m-t');
		if($outlet_id!=0 && $company_id ==0){
			$a_query = "select SUM(total_wages) as consumed_credit from `jod_applicants_transactions` where outlet_id =".$outlet_id." AND created_at  between '$first_date' AND '$last_date'";
		}elseif($outlet_id==0 && $company_id !=0 && $is_area_manager== 0){
			$query = "SELECT * FROM (`jod_outlets`) WHERE `company_id` = '".$company_id."' AND `status`='1'";
			$result = $CI->db->query($query);
			$res_data = $result->result();
			if(!empty($res_data)){
				$outlet_ids = array();
				foreach($res_data as $outlet){
						array_push($outlet_ids,$outlet->outlet_id);
				}
				$outlet_ids_str = implode(',',$outlet_ids);
				$a_query = "select SUM(total_wages) as consumed_credit from `jod_applicants_transactions` where outlet_id IN (".$outlet_ids_str.") AND created_at  between '$first_date' AND '$last_date'";
			}else{
				return 0;
			}
		}elseif($is_area_manager == 1){
			$userId = LoginUserDetails('userid');
			$roleId = LoginUserDetails('role_id');
			if($roleId == 5){ // area Manager
				$area_manager = getAreaManagerId($userId);
				$outlets_data = get_outlets_by_area_manager_id($area_manager[0]->area_manager_id);
				$outlet_ids = array();
				foreach($outlets_data as $outlet){
						array_push($outlet_ids,$outlet->outlet_id);
				}
				$outlet_ids_str = implode(',',$outlet_ids);
				$a_query = "select SUM(total_wages) as consumed_credit from `jod_applicants_transactions` where outlet_id IN (".$outlet_ids_str.") AND created_at  between '$first_date' AND '$last_date'";
			}
		}
		$result_transaction = $CI->db->query($a_query);
		$transaction_data = $result_transaction->result();
		if(!empty($transaction_data)){
			return round_up($transaction_data[0]->consumed_credit,1, PHP_ROUND_HALF_DOWN);
		}else{
			return 0;
		}
	}

	function get_applicant_transaction_data($job_id,$user_accounts_id,$outlet_id){
		$CI = get_instance();
		$query = "select * from `jod_applicants_transactions` where outlet_id =".$outlet_id." AND job_id =".$job_id." AND applicants_user_account_id=".$user_accounts_id;
		//echo $query; die;
		$result_transaction = $CI->db->query($query);
		$transaction_data = $result_transaction->result();
		if(!empty($transaction_data)){
			return $transaction_data[0];
		}else{
			return false;
		}
	}

	function ConvertMinutes2Hours($Minutes)
	{
		if ($Minutes < 0)
		{
			$Min = Abs($Minutes);
		}
		else
		{
			$Min = $Minutes;
		}
		$iHours = Floor($Min / 60);
		$Minutes = ($Min - ($iHours * 60)) / 100;
		$tHours = $iHours + $Minutes;
		if ($Minutes < 0)
		{
			$tHours = $tHours * (-1);
		}
		$aHours = explode(".", $tHours);
		$iHours = $aHours[0];
		if (empty($aHours[1]))
		{
			$aHours[1] = "00";
		}
		$Minutes = $aHours[1];
		if (strlen($Minutes) < 2)
		{
			$Minutes = $Minutes ."0";
		}
		$tHours = $iHours .":". $Minutes;
		return $tHours;
	}

	function job_history_data($res_data_applicants){
		$data_applicant = array();
		foreach($res_data_applicants as $clock_in_out_data){
			$data_arr_job_history = array();
			$data_arr_job_history["date"] = date('d M Y', strtotime($clock_in_out_data->applicant_clockIn_dateTime));
			$data_arr_job_history["clock_out_date"] = date ('d M Y', strtotime($clock_in_out_data->applicant_clockOut_dateTime));
			$data_arr_job_history["outlet_name"] = $clock_in_out_data->outlet_name;
			$user_details = getUserDetailsByID($clock_in_out_data->applicant_user_account_id);
			$data_arr_job_history["applicant_fullname"] = $clock_in_out_data->first_name." ".$clock_in_out_data->last_name;
			$data_arr_job_history["clock_in"] = date('H:i', strtotime($clock_in_out_data->applicant_clockIn_dateTime));
			$data_arr_job_history["clock_in_date"] = $clock_in_out_data->applicant_clockIn_dateTime;
			$data_arr_job_history["clock_out"] = date('H:i', strtotime($clock_in_out_data->applicant_clockOut_dateTime));
			$data_arr_job_history["unique_id"] = $user_details->unique_id;
			$data_arr_job_history["job_id"] =  $clock_in_out_data->job_id;
			$data_arr_job_history["applicant_user_accounts_id"] = $clock_in_out_data->applicant_user_account_id;
			$data_arr_job_history["job_title"] = $clock_in_out_data->job_title;
			$data_arr_job_history["outlet_id"] = $clock_in_out_data->outlet_id;	
			$data_arr_job_history["wages_per_hour"] = ($clock_in_out_data->payment_amount_per_hour != 0) ? sprintf('%0.2f',$clock_in_out_data->payment_amount_per_hour) : sprintf('%0.2f',$clock_in_out_data->payment_amount) ;
			$meal_time = $clock_in_out_data->meal_hours;
			/* check meal break time */
			if($meal_time != "" && $meal_time !="00:00:00"){
				$seconds = strtotime("1970-01-01 $meal_time UTC");
				$meal_break_minutes =  $seconds/60;
			}else{
				$meal_break_minutes = 0;
			}
			if(get_applicant_transaction_data($clock_in_out_data->job_id,$clock_in_out_data->applicant_user_account_id,$clock_in_out_data->outlet_id)!= false){
				$transaction_data = get_applicant_transaction_data($clock_in_out_data->job_id,$clock_in_out_data->applicant_user_account_id,$clock_in_out_data->outlet_id);
				/* Total worked hours fetched from Db not having without dedcuted meal break time  */
				$total_hour_without_deduction_meal_break_time  = $transaction_data->total_hour_worked;
				/* Total Minutes after deduction meal break time in minutes */
				$total_minutes_after_deduction_meal_break_time  = ($transaction_data->total_hour_worked*60) - $meal_break_minutes;
				/* Total hours  after deduction meal break time in minutes */
				$total_hours_worked_after_deduction_meal_break_time = $total_minutes_after_deduction_meal_break_time/60.0;
				/* Total hours  worked after deduction meal break time in h:i formate */
				
				$total_hours_worked_formated = convert_seconds_to_time(round(($total_hours_worked_after_deduction_meal_break_time*3600)));
				
				
				
				$data_arr_job_history["total_hours_worked"] = $total_hours_worked_formated;
				
				$data_arr_job_history["credit_before_transacion"] = sprintf('%0.2f',$transaction_data->credit_before_transacion);
				$data_arr_job_history["credit_after_transacion"] = sprintf('%0.2f',$transaction_data->credit_after_transacion);
				$data_arr_job_history["total_jod_credit"] = sprintf('%0.2f',$transaction_data->total_wages);
			}else{
				$meal_time = $clock_in_out_data->meal_hours;
				/* check meal break time */
				if($meal_time != "" && $meal_time !="00:00:00"){
					$seconds = strtotime("1970-01-01 $meal_time UTC");
					$meal_break_minutes =  $seconds/60;
				}else{
					$meal_break_minutes = 0;
				}
				/* check difference between both time */
				if( strtotime($clock_in_out_data->applicant_clockOut_dateTime) > strtotime($clock_in_out_data->applicant_clockIn_dateTime) ){
					$total_minutes = (strtotime($clock_in_out_data->applicant_clockOut_dateTime) - strtotime($clock_in_out_data->applicant_clockIn_dateTime))/60;
					$hourdiff = $total_minutes / 60;
				}else{
					$hourdiff = 0;
					$total_minutes = 0;
				}
				$total_minutes_after_deduction = $total_minutes - $meal_break_minutes;
				$hours_after_deduction_meal_break_time = $total_minutes_after_deduction/60;
				$data_arr_job_history["total_hours_worked"] = gmdate ('H:i', $total_minutes_after_deduction*60); 
				$data_arr_job_history["credit_before_transacion"] = "N/A";
				$data_arr_job_history["credit_after_transacion"] = "N/A";
				$data_arr_job_history["total_jod_credit"] = ($clock_in_out_data->payment_amount_per_hour != 0) ? $hours_after_deduction_meal_break_time * $clock_in_out_data->payment_amount_per_hour : $hours_after_deduction_meal_break_time * $clock_in_out_data->payment_amount;
			}
			$data_arr_job_history["meal_break_time"] = $clock_in_out_data->meal_hours;
			$data_applicant[] = $data_arr_job_history;
		}
		return $data_applicant;
	}
	
	function convert_seconds_to_time($init){
		//	$init = 685;
			$hours = floor($init / 3600)  > 9 ? floor($init / 3600) : "0".floor($init / 3600) ;
			$minutes = floor(($init / 60) % 60) > 9 ? floor(($init / 60) % 60) : "0".floor(($init / 60) % 60);
			$seconds = ($init % 60) > 9 ? ($init % 60) : "0".($init % 60);
			
			$time =  "$hours:$minutes:$seconds";
			return $time;
		
	}
	
	function get_applicant_detail_job_data($job_id,$applicants_id,$outlet_id){
		$CI = get_instance();
		$query_new  = "SELECT  jod_job_applicants.applicant_user_account_id, jod_job_applicants.meal_hours ,jod_job_applicants.job_id,jod_job_applicants.payment_amount_per_hour, ";
		$query_new .= "jod_clockIn_clockOut.applicant_clockIn_dateTime, jod_clockIn_clockOut.applicant_clockOut_dateTime, ";
		$query_new .= "jod_jobs.job_title,jod_jobs.payment_amount, ";
		$query_new .= "jod_outlets.outlet_id , jod_outlets.outlet_name, ";
		$query_new .= "jod_applicants.first_name , jod_applicants.last_name ";
		$query_new .= "FROM jod_job_applicants ";
		$query_new .= "JOIN jod_clockIn_clockOut ON jod_clockIn_clockOut.applicant_user_account_id = jod_job_applicants.applicant_user_account_id AND jod_clockIn_clockOut.job_id = jod_job_applicants.job_id ";
		$query_new .= "JOIN jod_jobs ON jod_job_applicants.job_id = jod_jobs.job_id ";
		$query_new .= "JOIN jod_outlets ON jod_jobs.outlet_id = jod_outlets.outlet_id ";
		$query_new .= "JOIN jod_applicants ON jod_job_applicants.applicant_user_account_id = jod_applicants.user_account_id ";
		$query_new .= "WHERE jod_job_applicants.clockInOutStatus = '2' AND ";
		$query_new .= "jod_outlets.outlet_id = '".$outlet_id."' AND  " ;
		$query_new .= "jod_job_applicants.applicant_user_account_id = '".$applicants_id."' AND ";
		$query_new .= "jod_job_applicants.job_id = '".$job_id."'";
		$result = $CI->db->query($query_new);
		$res_data_applicants = $result->result();
		
			if(!empty($res_data_applicants)){
				$data_applicant =  job_history_data($res_data_applicants);
				$data_applicant[0]['old_applicant_clock_in_date_time']  = $res_data_applicants[0]->applicant_clockIn_dateTime;
				$data_applicant[0]['old_applicant_clock_out_date_time'] = $res_data_applicants[0]->applicant_clockOut_dateTime;
				$data_applicant[0]['old_payment_amount_per_hour']  	 = $res_data_applicants[0]->payment_amount_per_hour;
				$data_applicant[0]['old_meal_hours']  					 = $res_data_applicants[0]->meal_hours;
				echo json_encode($data_applicant[0]); die;
			}
			
		}


		function getOutletName($outletId){
				$CI = get_instance();
				$prefix = $CI->db->dbprefix;
				$query = "SELECT outlet_name FROM `".$prefix."outlets` WHERE `outlet_id` = '$outletId'";
				$result = $CI->db->query($query);
				if($result->num_rows()>0){
					 $data = $result->row();
					 return  $data->outlet_name; 
				}else{
					return false;
				}

		}

		function getJobTypeName($job_type_id){
				$CI = get_instance();
				$prefix = $CI->db->dbprefix;
				$query = "SELECT name FROM `".$prefix."job_roles` WHERE `job_role_id` = '$job_type_id'";
				$result = $CI->db->query($query);
				if($result->num_rows()>0){
					 $data = $result->row();
					 return  $data->name; 
				}else{
					return false;
				}


		}
		
		function getJobSummaryByDate($date,$search="",$length=0,$start=0,$orderBy="",$column_name=""){
				$CI = get_instance();
				$prefix = $CI->db->dbprefix;
				$query_new  = "SELECT  jod_job_applicants.*,   ";
				//$query_new .= "jod_clockIn_clockOut.applicant_clockIn_dateTime, jod_clockIn_clockOut.applicant_clockOut_dateTime, ";
				$query_new .= "jod_jobs.job_title,jod_jobs.payment_amount,jod_jobs.start_date,jod_jobs.end_date, jod_jobs.is_delete as job_cancel_status,jod_jobs.is_applicants_hired,  ";
				$query_new .= "jod_outlets.outlet_id , jod_outlets.outlet_name, ";
				$query_new .= "jod_applicants.first_name as applicant_fname, jod_applicants.last_name as applicant_lname ";
				$query_new .= "FROM jod_job_applicants ";
				//$query_new .= "JOIN jod_clockIn_clockOut ON jod_clockIn_clockOut.applicant_user_account_id = jod_job_applicants.applicant_user_account_id AND jod_clockIn_clockOut.job_id = jod_job_applicants.job_id ";
				$query_new .= "JOIN jod_jobs ON jod_job_applicants.job_id = jod_jobs.job_id ";
				$query_new .= "JOIN jod_outlets ON jod_jobs.outlet_id = jod_outlets.outlet_id ";
				$query_new .= "JOIN jod_applicants ON jod_job_applicants.applicant_user_account_id = jod_applicants.user_account_id ";
				$query_new .= "WHERE jod_jobs.start_date = '$date' OR jod_jobs.end_date ='$date'  ";
				if($search !=""){
						$query_new .= " AND (jod_outlets.outlet_name LIKE '%".$search."%' OR jod_applicants.first_name LIKE '%".$search."%' OR jod_applicants.last_name LIKE '%".$search."%' OR jod_jobs.job_title LIKE '%".$search."%') ";
				}
				if($column_name!=""){
					$query_new	.= " ORDER BY ".$column_name." ".$orderBy;
				}
				if($length!= 0){
					$query_new .= " LIMIT ".$start .",".$length;
				}
				$applicant_result = $CI->db->query($query_new);
				$res_data_applicants = $applicant_result->result();
				if(!empty($res_data_applicants)){
					return $res_data_applicants; 
				}else{
					return false;
				}
			
				
		}
		
		
		function getApplicantStatus($applicant_detail_array){
			$res = array();
			//print_r($applicant_detail_array); die;
			// if manager has cancelled job ... job_status = 11;  // needs to change to another number
			if ($applicant_detail_array->job_cancel_status == 1) {
				$res['status_code'] = 11;
				$res['status'] = "Cancelled by Manager";
				return $res; 
			}
			
			// if applicant has cancelled the job ... job_status = 2;
			if ($applicant_detail_array->is_delete == 1 && $applicant_detail_array->system_rejected == 0  ) {
				if ($applicant_detail_array->is_applicants_hired == 1 ){
					$res['status_code'] = 12;
				}else {  // job has not started, so can reapply
					$res['status_code'] = 2;
				}
				$res['status'] = "Cancelled by Applicant";
				return $res; 
			}
			
			if($applicant_detail_array->is_delete == 1 && $applicant_detail_array->system_rejected == 1){
				$res['status_code'] = 15;
				$res['status'] = "Rejected due to Time overlap";
				return $res; 
			}
			
			// if manager has not approve ... job_status = 3;
			if ($applicant_detail_array->status == 0) {
				$res['status_code'] = 3;
				$res['status'] = "Pending";
				return $res; 
			}

			// if applicant has been rejected .. job_status = 7;
			if ($applicant_detail_array->status == 2) {
				$res['status_code'] = 7;
				$res['status'] = "Rejected by Manager";
				return $res; 
			}
			// if applicant has completed the clock out and done with the feedback .. job_status = 6;
			if ($applicant_detail_array->clockInOutStatus == 2 && $applicant_detail_array->completed_by_applicant == 1 ) {
				$res['status_code'] = 6;
				$res['status'] = "Clock out Successfully";
				return $res; 
			}
			
			// if applicant has completed the clock out but without the feedback .. job_status = 14;
			if ($applicant_detail_array->clockInOutStatus == 2 && $applicant_detail_array->completed_by_applicant == 0 ) {
				$res['status_code'] = 14;
				$res['status'] = "Clock out Successfully";
				return $res; 
			}
			// if applicant has completed the clock in ... job_status = 10;
			if ($applicant_detail_array->clockInOutStatus == 1) {
				$res['status_code'] = 10;
				$res['status'] = "Clock In done";
				return $res; 
			}
			
			
			// if applicant has not done 1st or 2nd ack
			if ($applicant_detail_array->acknowledged == 0 || $applicant_detail_array->ack_approval == 2 || $applicant_detail_array->ack_approval == 1) {
				$res['status_code'] = 4;
				$res['status'] = "Not Ack";
				return $res; 
			}
			
			// if applicant has completed 1st or 2nd ack
			if ($applicant_detail_array->acknowledged == 1 || $applicant_detail_array->ack_approval == 3 ) {
				$res['status_code'] = 5;
				$res['status'] = "Ack1";
				return $res; 
			}
			
			// if applicant has completed 1st or 2nd ack
			if ($applicant_detail_array->acknowledged == 1 || $applicant_detail_array->ack_approval == 2 ) {
				$res['status_code'] = 5;
				$res['status'] = "Ack2";
				return $res; 
			}
			
			$res['status_code'] = 0;
			$res['status'] = "Error";
			return $res; 
			
		}
		
		function confirm_payment_by_SA($company_id,$transaction_data,$insert_data){
			
			$CI = get_instance();
			$CI->load->model('jobs_model');
			
			$prefix = $CI->db->dbprefix;
			$CI->db->trans_start();
			$get_company_data_query = "SELECT * FROM ".$prefix."companies WHERE company_id = '".$company_id."' FOR UPDATE ";
			$company_result = $CI->db->query($get_company_data_query); 
			$compnay_details = $company_result->result();
			/* Jod credit calculation */
			$transaction_data['credit_before_transacion'] = $compnay_details[0]->current_jod_credit;
			$transaction_data['credit_after_transacion']  = $compnay_details[0]->current_jod_credit - $insert_data['jod_credit_difference'];
		//	echo "Jod credit calculation    "; print_r($transaction_data); print_r($insert_data); die;
			$CI->jobs_model->update_company_data($compnay_details[0]->company_id,array("current_jod_credit"=>$transaction_data['credit_after_transacion'] )) ;
			$CI->jobs_model->save_appliant_payment_data($insert_data);
			$CI->jobs_model->save_appliant_transaction_data($transaction_data);
			if($CI->db->trans_status() == false){
				$CI->db->trans_rollback();
				return false;
			}else{	
				$CI->db->trans_complete(); 
				return true;
			}
		}
		
		
		function applicant_clockin_clockout_details($job_id,$applicant_user_id){
			$CI = get_instance();
			$query = "SELECT * FROM `jod_clockIn_clockOut` WHERE `job_id` = '$job_id' AND `applicant_user_account_id` = '$applicant_user_id' ";
			$result = $CI->db->query($query);
			if($result->num_rows()>0){
				$clock_in_clock_out_details = $result->result();
				return $clock_in_clock_out_details; 
			}else{
				return false;
			}
			
		}
		
		function getSelectedApplicantsName($job_id){
			
			$applicants_name = array(); 
			$CI = get_instance();
			$query_dd = "SELECT `applicant_user_account_id` FROM `jod_job_applicants` WHERE `job_id` = '$job_id' AND `is_delete` = 0 AND `status` = 1 GROUP BY `applicant_user_account_id` ";
		//	echo $query_dd; die;
			$result = $CI->db->query($query_dd);
			if($result->num_rows() > 0){
				$applicants = $result->result();
				foreach ($applicants as  $value) {
					# code...
					$applicant_name_query = "SELECT first_name,last_name	 FROM `jod_applicants` WHERE user_account_id = '".$value->applicant_user_account_id."'";
					$name_res = $CI->db->query($applicant_name_query);
					if($name_res->num_rows() > 0){
						$name_details = $name_res->result();
						$name = $name_details[0]->first_name." ".$name_details[0]->last_name;
						$applicants_name[] = ucwords($name); 
					}
				}
			}
			if(!empty($applicants_name)){
				$name_strings = implode(', ',$applicants_name);
			}else{
				$name_strings = "No applicants selected";
			}
			return $name_strings; die;
		}
	




