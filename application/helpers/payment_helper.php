<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* function to get data for the job payment page */
function getConfirmPaymentData($date,$search="",$length=0,$start=0,$orderBy="",$column_name=""){
	$CI = get_instance();
	$query_new  = "SELECT jod_applicants_payment_by_admin.*, ";
	$query_new .= "jod_outlets.outlet_id , jod_outlets.outlet_name, ";
	$query_new .= "jod_applicants.first_name as applicant_fname, jod_applicants.last_name as applicant_lname, ";
	$query_new .= "jod_jobs.job_title ";
	$query_new .= "FROM jod_applicants_payment_by_admin ";
	$query_new .= "JOIN jod_jobs ON jod_applicants_payment_by_admin.job_id = jod_jobs.job_id ";
	$query_new .= "JOIN jod_applicants ON jod_applicants_payment_by_admin.applicants_user_account_id = jod_applicants.user_account_id ";
	$query_new .= "JOIN jod_outlets ON jod_jobs.outlet_id = jod_outlets.outlet_id ";
	$query_new .= "WHERE (jod_jobs.start_date = '$date' OR jod_jobs.end_date ='$date')  ";
	if($search !=""){
		$query_new .= " AND (jod_outlets.outlet_name LIKE '%".$search."%' OR jod_applicants.first_name LIKE '%".$search."%' OR jod_applicants.last_name LIKE '%".$search."%' OR jod_jobs.job_title LIKE '%".$search."%' OR jod_applicants_payment_by_admin.	clock_in_datetime LIKE '%".$search."%' OR jod_applicants_payment_by_admin.clock_out_datetime LIKE '%".$search."%' OR jod_applicants_payment_by_admin.total_jod_credit LIKE  '%".$search."%') ";
	}
	if($column_name!=""){
		$query_new	.= " ORDER BY ".$column_name." ".$orderBy;
	}
	if($length!= 0){
		$query_new .= " LIMIT ".$start .",".$length;
	}
	
	$applicant_result = $CI->db->query($query_new);
	$res_data_applicants = $applicant_result->result();
	if(!empty($res_data_applicants)){
		return $res_data_applicants; 
	}else{
		return false;
	}
}

/* function to get new payment data */
function get_new_clock_in_out_details($job_id,$applicants_id,$outlet_id){
	$final_arr = array();
	$CI = get_instance(); 
	$query = "SELECT  DATE_FORMAT(clock_in_datetime,' %d %b %Y ')  as  clock_in_date , DATE_FORMAT(clock_out_datetime,' %d %b %Y ')  as  clock_out_date, DATE_FORMAT(clock_in_datetime,' %h:%i %p')  as  clock_in_time, DATE_FORMAT(clock_out_datetime,' %h:%i %p')  as  clock_out_time, jod_applicants_payment_by_admin.*  FROM jod_applicants_payment_by_admin WHERE applicants_user_account_id = '".$applicants_id."' AND job_id = '".$job_id."'";
	$payment_res = $CI->db->query($query);
	$payment_data = $payment_res->result();
	if(!empty($payment_data)){
		$transaction_query = "SELECT * from jod_applicants_transactions WHERE applicants_user_account_id = '".$applicants_id."' AND job_id = '".$job_id."' AND outlet_id ='".$outlet_id."' AND transaction_reference_id != '0'";
		$transaction_res = $CI->db->query($transaction_query);
		$transaction_data = $transaction_res->result();
		$final_arr['payment_data'] 		= $payment_data[0];
		$final_arr['transaction_data']  = $transaction_data[0];
		return $final_arr; 
	}else{
		return false;
	}
}

/* function to check the payment is done */

function check_payment_is_done($applicants_id,$job_id){
		$CI = get_instance(); 
		$query = "SELECT  *  FROM jod_applicants_payment_by_admin WHERE applicants_user_account_id = '".$applicants_id."' AND job_id = '".$job_id."'";
		$res = $CI->db->query($query);
		$data = $res->result();
		if(!empty($data)){
			return true;
		}else{	
			return false;
		}
}
