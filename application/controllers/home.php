<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MX_Controller {
	
	function __construct()
	{
		parent::__construct();		       		
		$this->load->library(array('head','template'));
	}
	/**
	 * Index Page for this controller.
	 **/
	public function index()
	{ 
		$this->template->load('template','register');
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
