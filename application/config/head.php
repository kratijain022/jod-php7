<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$CI =& get_instance();
$lang=$CI->uri->segment(1,'en');




// --------------------------------------------------------------------------
// Head Library Configs
// --------------------------------------------------------------------------

/*
|--------------------------------------------------------------------------
| Default configs
|--------------------------------------------------------------------------
|
| These configs can override class variables for the head class. You can find
| These variables listed and commented in the head class file.
|
*/
/*---------site title-----------*/

$config['site_title'] = 'Jobs-On-Demand';
/*---------main template path--------*/
$templatePath 	= 'templates/JOD/';

/*------define main tempalte path for config------*/
$config['template_path'] = $templatePath;

/*------loginform  css list---------*/
$config['bootstrap_css']			= $templatePath.'vender/stylesheet/bootstrap.css';
$config['animate_min_css']			= $templatePath.'vender/stylesheet/animate.min.css';
$config['signin_css']			= $templatePath.'stylesheet/signin.css';


//common css for admin panel
$config['bootstrap_min_css']					= $templatePath.'vender/stylesheet/bootstrap.min.css';
$config['font-awesome_css']					= $templatePath.'fonts/font-awesome.css';
$config['css_font_awesome_css'] 						= $templatePath.'fonts/css/font-awesome.css';
$config['custom_css']					= $templatePath.'stylesheet/custom.css';
$config['green_css']					= $templatePath.'vender/stylesheet/icheck/flat/green.css';
$config['chosen_css']					= $templatePath.'stylesheet/chosen.css';
$config['parsley_css']					= $templatePath.'stylesheet/parsley.css';




//css for show message

/*------login js list---------*/


$config['ie-emulation-modes-warning_js']  			= $templatePath.'vender/javascript/ie-emulation-modes-warning.js';
$config['ie10-viewport-bug-workaround_js']  			= $templatePath.'vender/javascript/ie10-viewport-bug-workaround.js';
$config['bootstrap_min_js']  			= $templatePath.'vender/javascript/bootstrap.min.js';
$config['jquery_min_js']  			= $templatePath.'vender/javascript/jquery.min.js';

/*----admin js list-----*/
$config['jquery_nicescroll_min_js'] 						= $templatePath.'vender/javascript/jquery.nicescroll.min.js';
$config['icheck_min_js'] 						= $templatePath.'vender/javascript/icheck.min.js';
$config['custom_js'] 						= $templatePath.'javascript/custom.js';
$config['chosen_jquery_min_js'] 						= $templatePath.'javascript/chosen.jquery.min.js';
$config['bootbox_min_js'] 						= $templatePath.'javascript/bootbox.min.js';


//this is for show message
$config['raphel_js'] 						= $templatePath.'js/raphael-2.1.0.min.js';
$config['dashboard_js'] 					= $templatePath.'js/dashboard.js';


//this is common file for all from
$config['colorpicker_js'] 						= $templatePath.'js/colorpicker.js';
$config['dataTables_bootstrap'] 						= $templatePath.'javascript/dataTables.bootstrap.js';
$config['jquery_dataTables'] 						= $templatePath.'javascript/jquery.dataTables.js';
$config['parsley_min_js'] 						= $templatePath.'vender/javascript/Parsley.js-2.6.0/dist/parsley.min.js';


// js and css for datepicker
$config['moment_js'] 						= $templatePath.'javascript/moment.js';
$config['datepicker_js'] 						= $templatePath.'javascript/bootstrap-datetimepicker.js';
$config['timepicker_css'] 						= $templatePath.'stylesheet/bootstrap-timepicker.min.css';
$config['timepicker_js'] 						= $templatePath.'javascript/bootstrap-timepicker.min.js';
$config['datepicker_css'] 						= $templatePath.'stylesheet/bootstrap-datetimepicker.css';

// js and css for datetimepicker
$config['bootstrap_datetimepicker_min_js'] 						= $templatePath.'vender/javascript/bootstrap-datetimepicker.min.js';
$config['bootstrap_datepicker_js'] 						= $templatePath.'vender/javascript/bootstrap-datepicker.js';
$config['bootstrap_combined_min_css'] 						= $templatePath.'vender/stylesheet/bootstrap-combined.min.css';
$config['bootstrap_datetimepicker_min_css'] 						= $templatePath.'vender/stylesheet/bootstrap-datetimepicker.min.css';
$config['datepicker_css'] 						= $templatePath.'vender/stylesheet/datepicker.css';

// js and css for the  start rating
$config['star_rating_js']         =  $templatePath.'vender/javascript/star-rating.js';
$config['parsely_chinese_js']         =  $templatePath.'vender/javascript/Parsley.js-2.6.0/dist/i18n/zh_cn.js';

$config['star_rating_css']         =  $templatePath.'vender/stylesheet/star-rating.css';
