<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$CI =& get_instance();
$lang=$CI->uri->segment(1,'en');




// --------------------------------------------------------------------------
// Head Library Configs
// --------------------------------------------------------------------------

/*
|--------------------------------------------------------------------------
| Default configs
|--------------------------------------------------------------------------
|
| These configs can override class variables for the head class. You can find
| These variables listed and commented in the head class file.
|
*/
/*---------site title-----------*/ 

$config['site_title'] = 'On-Demand';
/*---------main template path--------*/
$templatePath 	= 'templates/JOD/';

/*------define main tempalte path for config------*/
$config['template_path'] = $templatePath;

/*------loginform  css list---------*/
$config['bootstrap_css']			= $templatePath.'vender/stylesheet/bootstrap.css';
$config['animate_min_css']			= $templatePath.'vender/stylesheet/animate.min.css';
$config['signin_css']			= $templatePath.'stylesheet/signin.css';


//common css for admin panel
$config['font-awesome_css']					= $templatePath.'fonts/css/font-awesome.css';
$config['custom_css']					= $templatePath.'stylesheet/custom.css';
$config['green_css']					= $templatePath.'vender/stylesheet/icheck/flat/green.css';

$config['toggles_css'] 						= $templatePath.'css/toggles.css';
$config['bootstrap_css'] 					= $templatePath.'vender/stylesheet/bootstrap.css';
$config['jquery_datatables_css']			= $templatePath.'css/jquery.datatables.css';
$config['jquery_tagsinput_css']				= $templatePath.'css/jquery.tagsinput.css';
$config['jquery-ui-1.10.3_css']				= $templatePath.'css/jquery-ui-1.10.3.css';
$config['weather-icons.min_css']			= $templatePath.'css/weather-icons.min.css';
$config['select2_bootstrap']				= $templatePath.'css/select2-bootstrap.css';
$config['datepicker']						= $templatePath.'css/datepicker.css';
$config['helvetica_css']					= $templatePath.'css/font.helvetica-neue.css';
$config['colorpicker_css']					= $templatePath.'css/colorpicker.css';


//css for show message

$config['default']							= $templatePath.'css/style.default.css';
/*------login js list---------*/


$config['ie-emulation-modes-warning_js']  			= $templatePath.'vender/javascript/ie-emulation-modes-warning.js';
$config['ie10-viewport-bug-workaround_js']  			= $templatePath.'vender/javascript/ie10-viewport-bug-workaround.js';
$config['bootstrap_min_js']  			= $templatePath.'vender/javascript/bootstrap.min.js';
$config['jquery_min_js']  			= $templatePath.'vender/javascript/jquery.min.js';


//this is for show message
$config['raphel_js'] 						= $templatePath.'js/raphael-2.1.0.min.js';
$config['custom_js'] 						= $templatePath.'js/custom.js';
$config['dashboard_js'] 					= $templatePath.'js/dashboard.js';


//this is common file for all from
$config['colorpicker_js'] 						= $templatePath.'js/colorpicker.js';
$config['dataTables_bootstrap'] 						= $templatePath.'js/dataTables.bootstrap.js';
$config['jquery_dataTables_min'] 						= $templatePath.'js/jquery.dataTables.min.js';
$config['form_validation'] 						= $templatePath.'js/form-validation.js';

