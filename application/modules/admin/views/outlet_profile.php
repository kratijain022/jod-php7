<?PHP if ( ! defined('BASEPATH')) exit('No direct script access allowed');  ?>

<?php //echo "<pre>"; print_r($outlet_details); die;
$company_details = getCompanydetails();
$area_manager_detail   =  areaManagerDetails($outlet_details->area_manager_id);
$rating = get_average_rating_outlet($outlet_details->outlet_id);
$total_amount = get_total_dollar_outlet($outlet_details->outlet_id);
$total_data = get_total_time_outlet($outlet_details->outlet_id);
$applicants_count = outlet_applicants($outlet_details->outlet_id);
$consumed_credit = current_month_consumed_jod_credit($outlet_details->outlet_id);
?>

	<div class="right_col">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<ul class="breadcrumb breadcrumb2">
			<li><a href="<?php echo base_url(); ?>"><?php echo $this->lang->line('home'); ?></a></li>
			<li class="active"><?php echo ucfirst($outlet_details->outlet_name); ?></li>
			</ul>
		</div>
		<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 manager-outlet">
			<div class="gray-back">
				<div class="col-lg-5 col-md-6 col-sm-5 col-xs-12 text-center">
					<img class="img-responsive" src="<?php echo (!empty($outlet_details->outlet_logo)) ? base_url('uploads/outlet_logo/thumb/'.$outlet_details->outlet_logo):IMAGE.'/no_image.png';  ?>" />
				</div>
				<div class="col-lg-7 col-md-6 col-sm-7 col-xs-12">
					<h2><?php echo $company_details->company_name; ?></h2>
					<h3><?php echo $outlet_details->outlet_name; ?></h3>
					<p>
						<?php echo $outlet_details->outlet_address; ?>
					<p>
						<?php echo $this->lang->line('area_manager'); ?> : <?php echo (!empty($area_manager_detail)) ? $area_manager_detail[0]->first_name." ".$area_manager_detail[0]->last_name : "No Area Manager assigned."; ?><br />
					<!--	Outlet Manager: David Yeo -->
					</p>
					<input id="outlet_rating" class="rating" data-stars="5" data-step="0.1" data-size="sm" />
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<?php if(LoginUserDetails('role_id') == 2) { ?>
			<a href="<?php echo base_url('admin/outlet_form/'.$outlet_details->outlet_id);  ?>">
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 manager-outlet">
				<div class="org-box4 edit-profile">
					<span> <i class="glyphicon glyphicon-pencil "></i>
					<br />
						<?php echo $this->lang->line("edit_profile"); ?>
					</span>
				</div>
			</a>
		</div>
		<?php } ?>
		<div class="clearfix"></div>
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 manager-details top-mar">
			<div class="gray-back">
				<h2><?php echo sprintf('%0.2f',$total_amount); ?></h2>
				<p><?php echo $this->lang->line('dollars_spent_on_wages_last_month'); ?></p>
			</div>
		</div>

		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 manager-details top-mar">
			<div class="gray-back">
				<h2> <?php echo sprintf('%0.2f',$total_data['total_time']); ?> </h2>
					<p><?php echo $this->lang->line('area_manager'); ?> <?php echo $total_data['job_count']; ?> JODs</p>
			</div>
		</div>

		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 manager-details top-mar">
			<div class="gray-back">
				<h2><?php echo $applicants_count; ?></h2>
				<p><?php echo $this->lang->line('staff_hired'); ?> <br/> <?php echo $this->lang->line("current_month"); ?></p>
			</div>
		</div>

		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 manager-details top-mar">
			<div class="gray-back">
				<h2><?php echo $consumed_credit; ?></h2>
				<p><?php echo $this->lang->line('consumed')." ".$this->lang->line('jod_credits'); ?> <br />(<?php echo $this->lang->line("current_month"); ?>)</p>
				<a href="<?php echo BASEURL.'admin/outlet_jod_credit/'.$outlet_details->outlet_id;?>">
					<button class="how col-lg-12 col-md-12 col-sm-12 col-xs-12  org-box orange-back-button " id="last_transactions"> <?php echo $this->lang->line('view_past_transactions'); ?><i class="fa fa-arrow-circle-right"></i></button>
				</a>
			</div>

		</div>

	</div>
</div>
</div>
<script>
	var rating = '<?php echo $rating; ?>';
	$('#outlet_rating').rating('update', rating);
	$('.rating').rating('refresh', {
			disabled:true,
			showCaption: false
	});
</script>
