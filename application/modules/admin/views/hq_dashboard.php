<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<div class="right_col">
	<div class="row padding-top-one">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<ul class="breadcrumb breadcrumb2">
				<li><a href=""><?php echo $this->lang->line('home'); ?></a></li>
				<li class="active"><?php echo $this->lang->line('dashboard'); ?></li>
			</ul>
			<div class="clearfix"></div>
			 <a class= "download-aptus-link active" href="<?php echo base_url('uploads/APTUS_PDFs/APTUS Technologies Terms Of Use for Customers - version 1.pdf'); ?>" download="APTUS Technologies Terms Of Use for Customers - version 1.pdf"><i class="fa fa-download"></i><?php echo $this->lang->line('aptus_term_condition'); ?> </a>
		</div>

		<div class="animated fadeIn col-lg-4 col-md-4 col-sm-4 col-xs-12 briefcase">
			<a href="<?php echo base_url('jobs/job_section')?>">
				<div class="orange-box text-center orangebox">
					<div class="col-lg-5 col-md-5 col-sm-5 col-xs-4">
						<i class="fa fa-briefcase"></i>
					</div>
					<div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
						<h1><?php echo $no_of_open_jobs; ?></h1>
						<h2 class="as"><?php echo $this->lang->line('all_jobs'); ?></h2>
					</div>
				<button class="how col-lg-7 col-md-7 col-sm-7 col-xs-7 yellow-box org-box "><?php echo $this->lang->line('view_all'); ?>  <i class="fa fa-arrow-circle-right"></i></button>
			 </div>
			</a>
		</div>

		<div class="animated fadeIn col-lg-4 col-md-4 col-sm-4 col-xs-12 briefcase">
			<a href="">
				<div class="orange-box text-center orangebox">
					<div class="col-lg-5 col-md-5 col-sm-5 col-xs-4">
						<i class="fa fa-users" ></i>
					</div>
					<div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
						<h1><?php echo $no_of_heired_applicants; ?></h1>
						<h2 class="as"><?php echo $this->lang->line('hired_today'); ?></h2>
					</div>
					<button class="how col-lg-7 col-md-7 col-sm-7 col-xs-7 org-box org-box "> <?php echo $this->lang->line('view_all'); ?> <i class="fa fa-arrow-circle-right"></i></button>
				</div>
			</a>
		</div>

		<a href="<?php echo base_url('jobs/notification');?>">
			<div class="animated fadeIn col-lg-4 col-md-4 col-sm-4 col-xs-12 briefcase">
				 <div class="orange-box text-center">
					<div class="col-lg-5 col-md-5 col-sm-5 col-xs-4">
						<i class="fa fa-bell-o"></i>
					</div>
					<div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
						<h1><?php echo $hq_manager_notification_count; ?></h1>
						<h2 class="as"><?php echo $this->lang->line('notifications'); ?></h2>
					</div>
				</div>
			</div>
		</a>
	<div class="clearfix"></div>

	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 marb-22">
		<div class="orang">
			<span class="org-span"><?php echo $this->lang->line('credits_overview'); ?></span>
			<div class="clearfix"></div>
			<div class="col-md-3 col-xs-12 text-center ava-cre2">
				<img src="<?php echo IMAGE; ?>jod-credit-icon-white.png">
			</div>
			<div class="col-md-3 col-xs-12 text-center ava-credits">
				<h2 class="star-h3-white"><?php echo (!empty($company_details)) ? round_up($company_details->current_jod_credit , 1, PHP_ROUND_HALF_DOWN)  : "N/A" ; ?></h2>
				<p class="star-h3-white"><?php echo $this->lang->line('jod_credit_with_hq'); ?></p>
			</div>
			<div class="col-md-3 col-xs-12 text-center  ava-credits">
				<h2 class="star-h3-white"><?php echo (!empty($assigned_JOD_credits)) ? round_up($assigned_JOD_credits->total_sum,1, PHP_ROUND_HALF_DOWN) : "N/A" ; ?> </h2>
				<p class="star-h3-white"><?php echo $this->lang->line('current_jod_credit_at_outlets'); ?></p>
			</div>
			<div class="col-md-3 col-xs-12 text-center  ava-credits">
				<h2 class="star-h3-white"><?php echo current_month_consumed_jod_credit(0,$company_details->company_id); ?> </h2>
				<p class="star-h3-white"><?php echo $this->lang->line('jod_credit_consumed'); ?> (<?php echo $this->lang->line('current_month'); ?>)</p>
			</div>
			<div class="clearfix"></div>
			<a href="<?php echo base_url('admin/hq_jod_credits')?>"><button class="how but-add col-lg-7 col-md-7 col-sm-7 col-xs-7 org-box org-box "><?php echo $this->lang->line('view_all'); ?>  <i class="fa fa-arrow-circle-right"></i></button></a>
		</div>
		<div class="clearfix"></div>
	</div>

	  <?php $rating =  get_total_count_company_feedback(); $rate = $rating->total_rating/$rating->total_counts;?>
	<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 mar-t15" >
		<a href="<?php echo BASEURL.'admin/company_feedback'?>">
			<div class="yellow-box overall-rating activity">
				<span> <?php echo $this->lang->line('overall_rating'); ?></span>
				<h2 class="col-lg-8 col-md-8 col-sm-8 col-xs-8 center-block no-float"><?php echo (!empty($rating)  && $rating->total_counts != 0)? round_up($rate, 1, PHP_ROUND_HALF_DOWN):'0.0';?></h2>
				<div class="w_star_rating col-lg-10 col-md-10 col-sm-10 col-xs-10 no-float center-block">
					  <input id="" class="rating" data-stars="5" data-step="0.1" data-size="sm" value="<?php echo (!empty($rating)  && $rating->total_counts != 0)?  round_up($rate, 1, PHP_ROUND_HALF_DOWN):'0.0';?>"/>
				</div>

				 <button class="how col-lg-12 col-md-12 col-sm-12 col-xs-12 org-box"><i class="fa fa-comment"></i><?php echo $this->lang->line('company_feedback'); ?></button>
			</div>

		</a>
	</div>
        <?php
			$company_details = pdf_link(LoginUserDetails('company_id'));
		?>
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
			<a id="pdf_link" href="<?php echo base_url('uploads/contract_of_service/'.$company_details[0]->contract_of_service); ?>" onclick="check_link();" download="<?php echo $company_details[0]->contract_of_service; ?>">
				<button  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 org-box mar-t15 pdf">
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 no-float center-block">
						<h2><?php echo $this->lang->line('contract'); ?><br /><?php echo $this->lang->line('for_service'); ?> (PDF)</h2>
						<div class="upload">
							<i class="fa fa-download"></i><br />
							<span><?php echo $this->lang->line('download'); ?></span>
						</div>
					</div>
				</button>
			</a>
		</div>
		<div class="clearfix"></div>

		<!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mar-t15">
			<div class="yellow-box activity">
				<span>Total Activity</span>
			</div>
        </div> -->

		<div class="clearfix"></div>

		<?php if(!empty($enabled_outlets)) { ?>
		<?php foreach($enabled_outlets as $outlet){ ?>
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 mar-t15">
			<!--- <div class="org-box head-top">Heading</div> -->
			<div class="light-gray-box square-ce dheight">
				<span><img src="<?php echo checkImageFile(base_url('uploads/outlet_logo/thumb/'.$outlet->outlet_logo)) == true ? base_url('uploads/outlet_logo/thumb/'.$outlet->outlet_logo) : IMAGE.'/no_image.png'; ?>"/></span>
				<h2 title="<?php echo ucfirst($outlet->outlet_name); ?>"><?php echo strlen($outlet->outlet_name) > 14 ? ucfirst(substr($outlet->outlet_name,0,14)).'...' :  ucfirst($outlet->outlet_name) ; ?></h2>
				<span><?php echo $this->lang->line('open'); ?> <?php echo $this->lang->line('jobs'); ?>: <?php echo getoutletJObs("open", $outlet->outlet_id); ?></span>
				<span><?php echo $this->lang->line('total'); ?> <?php echo $this->lang->line('jobs'); ?>: <?php echo getoutletJObs("total", $outlet->outlet_id); ?></span>
			</div>
			<a href="<?php echo base_url('admin/outlet_profile/'.$outlet->outlet_id); ?>"><button class="how2 col-lg-12 col-md-12 col-sm-12 col-xs-12 org-box"><?php echo $this->lang->line('view_outlet'); ?></button></a>
		</div>
		<?php } // foreach close ?>
		<?php } ?>
   <div class="clearfix"></div>
</div>

</div>
</div>
</div>

</div>
</div>
<script>
jQuery(document).ready(function() {
   QuickSidebar.init(); // init quick sidebar
   Layout.init(); // init layout
});

function check_link(){
	var pdf_link = document.getElementById("pdf_link").href;
	//alert(pdf_link.indexOf(".pdf"));
	if(pdf_link.indexOf(".pdf") == -1){
		$('#pdf_link').removeAttr( "href" );
		$('#pdf_link').removeAttr( "download" );
		bootbox.alert("The document contract of services not found.", function() {
		});
		return false;
	}else{
		//console.log("PDF found");
	}
}
	$('.rating').rating('refresh', {
		disabled:true,
		showCaption: false
	});
</script>
