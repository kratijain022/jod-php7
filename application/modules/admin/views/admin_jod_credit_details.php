<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php
	$userType=LoginUserDetails('role_id');
	$message = getGlobalMessage();
?>
<div class="right_col right_col" role="main">
	<?php
	if($message['type']=='success') {
	?>
	<div class="Metronic-alerts alert alert-success fade in" id="prefix_167847684379"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button><?php echo $message['msg'] ?></div>
	<?php
	} else if($message['type']=='error') {
	?>
	<div class="Metronic-alerts alert alert-danger fade in" id="prefix_167847684379"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button><?php echo $message['msg'] ?></div>
	<?php
	}
	?>
	<div class="row padding-top-one">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<ul class="breadcrumb breadcrumb2">
				<li><a href="<?php echo base_url(); ?>"><?php echo $this->lang->line('home'); ?></a></li>
				<li class="active"><?php echo $this->lang->line('jod_credits'); ?></li>
			</ul>
		</div>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div id="list" class="trancaction_comp_list">
			<?php if(!empty($companies)) { ?>
				<?php foreach($companies as $company) { ?>
				<div id="company_credits_list">
					<div class="col-lg-10 col-md-10 col-xs-12 marb-22 ">
						<div class="light-gray-box">
							<h3 class="pull-left star-h3 col-md-5 col-xs-12 text-center"><?php echo (isset($company->company_name) && $company->company_name != "" ) ? $company->company_name : "N/A"; ?></h3>
							<div class="col-md-3 col-xs-12 text-center pull-right ava-credits">
								<h2 <?php echo  ($company->current_jod_credit <= $company->min_credit_limit) ? "class='red'" : "" ;?>><?php echo (isset($company->current_jod_credit)) ? $company->current_jod_credit : 0 ; ?> </h2>
								<p <?php echo  ($company->current_jod_credit <= $company->min_credit_limit) ? "class='red'" : "" ;?>><?php echo $this->lang->line('available_credit'); ?></p>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="clearfix"></div>
					</div>

					<div class="col-md-2 col-xs-12 marb-22 ">
						<a href="<?php echo base_url('admin/assign_credits_to_company/'.$company->company_id); ?>" class="assign_credit_but">
						<button class="yellow-cont yellow-back text-center">
							 <h3><?php echo $this->lang->line('assign_credits'); ?></h3>
						</button>
						</a>
					</div>
					<div class="clearfix"></div>
				</div>
				<?php } ?>

			</div>
			<?php }else{ ?>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bmar">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 dark_organge org-boxs no-padding">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 dark_organge org-box  no-padding left-mar nob-mar job-box">
								<div class="col-lg-10 col-md-10 col-sm-10 col-xs-9 center-block no-float">
									<div class="over-scroll-left text-center over-scroll-left2">
										<h2><?php echo "No Companies Found."; ?></h2>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?php } ?>
			<?php if(!empty($transaction_history)) { ?>
			<div id="company_transaction_history_div">
				<h2 class="credits-history"><?php echo $this->lang->line('credit_history'); ?><a class="download_button_credit_his" href="<?php echo base_url('admin/download_transaction_history/all_company'); ?>" onclick="loader()"><i class="fa fa-download"></i></a></h2>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 overf">
					<table class="table table-th-gray table-striped table-data" id="all_companies_history">
						<thead class="firt-top">
							<tr>
								<th></th>
								<th><?php echo $this->lang->line('date'); ?></th>
								<th><?php echo $this->lang->line('company_name'); ?></th>
								<th><?php echo $this->lang->line('amount'); ?> +/-</th>
								<th width="437"><?php echo $this->lang->line('comments'); ?></th>
							</tr>
						</thead>
						<tbody>
							<?php /*foreach($transaction_history as $history_row) { ?>
							<tr class="">
								<td><?php echo (isset($history_row->assigned_at) && $history_row->assigned_at!= "") ? date('d M Y h:i A', strtotime($history_row->assigned_at)) : " " ;?></td>
								<td><?php echo (isset($history_row->company_name)) ? $history_row->company_name : "" ; ?></td>
								<td><?php echo (isset($history_row->transaction_type) && $history_row->transaction_type == 2) ? "+".  $history_row-> jod_credit_amount : "-".  $history_row-> jod_credit_amount; ?></td>
								<td><p class="jod-tooltip" data-toggle="tooltip" data-placement="bottom"  title="<?php echo ucfirst($history_row->comment_text); ?>"   ><?php echo strlen($history_row->comment_text) > 150 ? ucfirst(substr($history_row->comment_text,0,150)).'...' : ucfirst($history_row->comment_text) ;  ?></p></td>
							</tr>
							<?php  } */?>
						</tbody>
					</table>
					<div class="clearfix"></div>
				</div>
			</div>
			<?php  } ?>
		</div>
	</div>
</div>
<script>
	
	<?php if(isset($site_language) && $site_language == "chinese") { ?>
	var lang_url = "//cdn.datatables.net/plug-ins/1.10.12/i18n/Chinese.json";
	<?php }else{ ?>
		var lang_url = "//cdn.datatables.net/plug-ins/1.10.12/i18n/English.json";
	<?php }?>
	
	$(document).ready(function() {
		 $("td").tooltip();
	   setTimeout(function(){
		   $('#all_companies_history_length').show();
		   $('#all_companies_history_filter').show();
		   $('#all_companies_history_paginate').show();

		}, 100);

	});
	$('#all_companies_history').dataTable({
		"bPaginate": true,
		"ajax": '<?php echo base_url('admin/get_company_transaction_history_by_ajax'); ?>',
		"bLengthChange": true,
		"bFilter": true,
		"bSort": true,
		"bInfo": true,
		"bAutoWidth": false,
		"order": [[ 0, "desc" ]],
		"processing": true,
		"serverSide": true,
		"columnDefs": [ { "targets": 0, "bSortable": true,"orderable": true, "visible": false }],
		"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
		},
		"deferRender": true,
		"language": {
                "url": lang_url
            },
    });

	function loader(){
		$('#loader').fadeIn();
		$('#loader').delay(1500).fadeOut('slow');

	}

</script>
