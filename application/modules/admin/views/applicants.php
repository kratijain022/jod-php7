<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$message = getGlobalMessage();
//echo $site_language; die;
?>
<div class="right_col right_col" role="main">
	<div class="row padding-top-one">
		<?php
		if($message['type']=='success') {
		?>
			<div class="Metronic-alerts alert alert-success fade in" id="prefix_167847684379"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button><?php echo $message['msg'] ?></div>
		<?php
		}
		?>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<ul class="breadcrumb breadcrumb2">
			<li><a href="<?php echo base_url(); ?>"><?php echo $this->lang->line('home'); ?></a></li>
			<li class="active"><?php echo $this->lang->line('applicants_list'); ?></li>
			</ul>
		</div>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<table class="table table-hovers" id="table3">
					<thead class="firt-top">
						<tr>
							<th><?php echo $this->lang->line('created_date'); ?></th>
							<th><?php echo $this->lang->line('name'); ?></th>
							<th><?php echo $this->lang->line('age'); ?></th>
							<th><?php echo $this->lang->line('experience'); ?></th>
							<th><?php echo $this->lang->line('total_jobs'); ?></th>
							<th><?php echo $this->lang->line('sum_earned'); ?></th>
							<th><?php echo $this->lang->line('rating'); ?></th>
							<th></th>
							<th><?php echo $this->lang->line('action'); ?></th>
							<th></th>
						</tr>
					</thead>
					<tbody>
					<?php
							/*$i=1;
							foreach($records as $company)
							{
								if($company->status==0) {
									$class = "enable";
									$name = "Enable";
									$fa = "fa fa-check";
									$trClass = "gray-box";
									$title = "Click to enable record";
								} else if($company->status==1) {
									$class = "disable";
									$name = "Disable";
									$fa = "fa fa-ban";
									$trClass = "";
									$title = "Click to disable record";
								}
							?>
						<tr class="<?php echo $trClass; ?>">
							<td><?php echo $company->created_at; ?></td>
							<td><?php echo ucfirst($company->display_name); ?></td>
							<td><?php echo get_age_by_date_of_birth($company->date_of_birth); ?></td>
							<?php $exprience_details =  getEmploymentHistory($company->applicant_id);
							if(!empty($exprience_details)){
								$exp_data = end($exprience_details);?>
							<td><?php echo $exp_data->length_of_service;?> <?php echo ($exp_data->length_of_service>1)?'yrs':'yr';?></td>
							<?php }else{ ?>
							<td>0</td>
							<?php } ?>
							<td><?php echo getTotalvoteCounts($company->user_account_id); ?></td>
							<?php $amt_data = total_earned_money($company->user_account_id) ; ?>
							<td><?php echo (isset($amt_data->total_money) || $amt_data->total_money > 0) ? $amt_data->total_money : 0 ; ?></td>
							<td><?php echo getAverageRating($company->user_account_id) > 0 ? round_up(getAverageRating($company->user_account_id), 1, PHP_ROUND_HALF_DOWN) : 0 ; ?></td>
							<td class="edit light-green">
								<a href="<?php echo base_url('admin/applicant_profile/'.$company->user_account_id); ?>">
									<span><i class="fa fa-eye"></i></span>
									<span>View</span>
								</a>
							</td>
							<td class="edit">
								<a href="<?php echo base_url('admin/applicant_form/'.$company->applicant_id); ?>">
									<span><i class="fa fa-pencil"></i></span>
									<span>Edit</span>
								</a>
							</td>

							<td class="<?php echo $class; ?>">
								<a href="javascript:;" title="<?php echo $title; ?>" onclick="myFunction('<?php echo $company->applicant_id; ?>','<?php echo $company->user_account_id; ?>','<?php echo $company->status; ?>')">
									<span><i class="<?php echo $fa ?>"></i></span>
									<span><?php echo $name; ?></span>
								</a>
							</td>

						</tr>
						<?php } */?>
					</tbody>
				</table>
			<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<div class="modal fade  bs-example-modal-md" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="myModal">
	<div class="cent">
    <div class="modal-dialog col-lg-5 center mode-w">
		<form role="form" id="change_status" method="post" data-parsley-validate onsubmit="$('#loader').fadeIn()" action="<?php echo base_url('admin/applicant_status'); ?>">
			<div class="modal-content">
				<div class="gr-inp shadow-box left-pa" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" >
                   <button data-dismiss="modal" class="close-b"><i class="fa fa-times-circle"></i></button>
					<h4 class="text-center basic-details org-box"><?php echo $this->lang->line('disable_user_popup_msg'); ?></h4>
					<div class="form-group"><div class="col-lg-1 col-md-1 col-sm-1 hidden-xs"></div>
						<div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
							<textarea type="text" placeholder="<?php echo $this->lang->line('reason_to_disable_user'); ?>" class="form-control" id="reason" name="reason" required ></textarea>
							<input type="hidden" id="user_account_id" name="user_account_id" value="" />
							<input type="hidden" id="status_value" name="status" value="" />
							<input type="hidden" id="id_primary" name="id" value="" />
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="col-lg-1 col-md-1 col-sm-1 hidden-xs"></div>
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 m30">
					<button class="create red-box col-lg-5 col-md-5 col-sm-5 col-xs-12 pull-left" id="model-cancel" data-dismiss="modal" >
					<i class="fa fa-close"></i><span><?php echo $this->lang->line('cancel'); ?></span>
					</button>
					<button class="create org-box col-lg-5 col-md-5 col-sm-5 col-xs-12 pull-right" id="save" name="save">
					<span><?php echo $this->lang->line('Ok'); ?> </span> <i class="fa fa-chevron-right"></i>
				</button>
					</div>
				</div>
			</div>
		</form>
	</div></div>
</div>
</div>

<div class="modal fade  bs-example-modal-md" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="myModal_enable">
	<div class="cent">
    <div class="modal-dialog col-lg-5 center mode-w">
		<form role="form" id="change_status" method="post" data-parsley-validate onsubmit="$('#loader').fadeIn()" action="<?php echo base_url('admin/applicant_status'); ?>">
			<div class="modal-content">
				<div class="gr-inp shadow-box left-pa" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" >
                   <button data-dismiss="modal" class="close-b"><i class="fa fa-times-circle"></i></button>
					<h4 class="text-center basic-details org-box"><?php echo $this->lang->line('reason_to_enable_user'); ?></h4>
					<div class="form-group"><div class="col-lg-1 col-md-1 col-sm-1 hidden-xs"></div>
						<div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
							<p class="reason-p" id="reason_for_disable_p"></p>
							<input type="hidden" id="user_account_id1" name="user_account_id" value="" />
							<input type="hidden" id="status_value1" name="status" value="" />
							<input type="hidden" id="id_primary1" name="id" value="" />
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="col-lg-1 col-md-1 col-sm-1 hidden-xs"></div>
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 m30">
					<button class="create red-box col-lg-5 col-md-5 col-sm-5 col-xs-12 pull-left" id="model-cancel" data-dismiss="modal" >
					<i class="fa fa-close"></i><span><?php echo $this->lang->line('cancel'); ?></span>
					</button>
					<button class="create org-box col-lg-5 col-md-5 col-sm-5 col-xs-12 pull-right" id="save" name="save">
					<span><?php echo $this->lang->line('Ok'); ?> </span> <i class="fa fa-chevron-right"></i>
				</button>
					</div>
				</div>
			</div>
		</form>
	</div></div>
</div>
</div>

<script src="<?php echo base_url('templates/system/js/bootbox.min.js'); ?>"></script>

<script>
<?php if(isset($site_language) && $site_language == "chinese") { ?>
	var lang_url = "//cdn.datatables.net/plug-ins/1.10.12/i18n/Chinese.json";
<?php }else{ ?>
	var lang_url = "//cdn.datatables.net/plug-ins/1.10.12/i18n/English.json";
<?php }?>
$('#table3').dataTable({
	"ajax": '<?php echo base_url('admin/getApplicantsByAjax'); ?>',
	"bPaginate": true,
	"bLengthChange": true,
	"bFilter": true,
	"bSort": true,
	"bInfo": true,
	"bAutoWidth": false,
	"order": [[ 0, "desc" ]],
	"processing": true,
    "serverSide": true,
	"columnDefs": [ { "targets": 0, "bSortable": true,"orderable": true, "visible": false }],
	"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
				if(aData[4] != '') {
					$(nRow).addClass(aData[10]);
					//$(nRow).addClass('multiorder');
				}
				$('td:eq(6)', nRow).addClass('edit light-green');
				$('td:eq(7)', nRow).addClass('edit');
				$('td:eq(8)', nRow).addClass(aData[11]);
			},
	"language":{
                "url": lang_url
    },


});

function myFunction(id,userAccountsId,status) {
	
		if(status == 1){
			$('#myModal').modal('show'); 
			$('#user_account_id').val(userAccountsId);
			$('#status_value').val(status);
			$('#id_primary').val(id);
			var applicants_obj  =  JSON.parse(decodeURIComponent("<?php echo rawurlencode(json_encode($records)); ?>"));$('#reason_for_disable_p').html('');
			$.each(applicants_obj, function( index,  value) {
				if(value.user_account_id == userAccountsId && id==value.applicant_id){
					if(value.reason_for_disable != ""){
						$('#reason').val(value.reason_for_disable);
					}
				}
			});
			
		}else{
			$('#user_account_id1').val(userAccountsId);
			$('#status_value1').val(status);
			$('#id_primary1').val(id);
			$('#myModal_enable').modal('show'); 
			<?php if(!empty($records)){ ?> 
			var applicants_obj  =  JSON.parse(decodeURIComponent("<?php echo rawurlencode(json_encode($records)); ?>"));$('#reason_for_disable_p').html('');
			$.each(applicants_obj, function( index,  value) {
				if(value.user_account_id == userAccountsId && id==value.applicant_id){
					if(value.reason_for_disable != ""){
						var msg = "The user has been disabled due to "+value.reason_for_disable+".  Do you want to enable the user?";
						$('#reason_for_disable_p').html(msg);
						
					}
				}
			});
			<?php }?>
			
		}
	}

	$(document).ready(function() {
	   setTimeout(function(){
		   $('#table3_length').show();
		   $('#table3_filter').show();
		   $('#table3_paginate').show();
		}, 1000);

	});
</script>
