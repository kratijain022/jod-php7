<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	$userType = LoginUserDetails('role_id');
	if($userType == 3){
			$outlet_id =  LoginUserDetails('user_outlet_id');
	}
 ?>
<div class="right_col">
   <div class="row padding-top-one">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
         <ul class="breadcrumb breadcrumb2">
            <li><a href=""><?php echo $this->lang->line('home'); ?></a></li>
            <li class="active"><?php echo $this->lang->line('dashboard'); ?></li>
         </ul>
			<div class="clearfix"></div>
			<a class= "download-aptus-link active" href="<?php echo base_url('uploads/APTUS_PDFs/APTUS Technologies Terms Of Use for Customers - version 1.pdf'); ?>" download="APTUS Technologies Terms Of Use for Customers - version 1.pdf"><i class="fa fa-download"></i><?php echo $this->lang->line('aptus_term_condition'); ?></a>
      </div>
      <div class="animated fadeIn col-lg-4 col-md-4 col-sm-4 col-xs-12 briefcase">
        <a href="<?php echo BASEURL.'jobs/open_job_section'?>">
         <div class="orange-box o-box text-center orangebox ">
            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-4">
               <i class="fa fa-briefcase"></i>
            </div>
            <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                  <h1><?php echo $no_of_open_jobs; ?></h1>
                  <h2 class="as"><?php echo $this->lang->line('open')." " ; ?><?php echo $this->lang->line('jobs1'); ?></h2>

            </div>
            <div class="clearfix"></div>
            <button class="how col-lg-7 col-md-7 col-sm-7 col-xs-7 yellow-box org-box "> <?php echo $this->lang->line('view_all'); ?>  <i class="fa fa-arrow-circle-right"></i></button>
         </div></a>
      </div>
      <div class="animated fadeIn col-lg-4 col-md-4 col-sm-4 col-xs-12 briefcase">
         <a href="<?php echo BASEURL.'jobs/active_job_section'?>"> 
         <div class="orange-box g-box text-center orangebox">
            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-4">
               <i class="fa fa-users"></i>
            </div>
            <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">

                  <h1><?php echo $no_of_active_jobs; ?></h1>
                  <h2 class="as"><?php echo $this->lang->line('active_jobs'); ?></h2>

            </div>
           <button class="how col-lg-7 col-md-7 col-sm-7 col-xs-7 org-box org-box dark-green "> <?php echo $this->lang->line('view_all'); ?> <i class="fa fa-arrow-circle-right"></i></button>
         </div></a>
      </div>
      <!-- Removed by kratika on request of Jason on date 23 Feb 2017 
      	<div class="animated fadeIn col-lg-4 col-md-4 col-sm-4 col-xs-12 briefcase">
            <div class="orange-box text-center" title="Total Applicants which have applied for your job.">
            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-4">
               <i class="fa fa-bell-o"></i>
            </div>
            <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
          <?php $notification_count = getAllNotifications(LoginUserDetails('userid'));?>
                  <h1><?php echo (!empty($notification_count))?$notification_count:'0';?></h1>
                  <h2 class="as"><?php echo $this->lang->line('notifications'); ?></h2>
            </div>
         </div>
      </div> -->
      <div class="clearfix"></div>
      <?php $rating =  get_total_count_company_feedback();   $rate = $rating->total_rating/$rating->total_counts;?>

      <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 mar-t15">
          <a href="<?php echo BASEURL.'admin/company_feedback'?>">
			  <div class="yellow-box overall-rating activity">
				<span> <?php echo $this->lang->line('overall_rating'); ?></span>
				<h2 class="col-lg-8 col-md-8 col-sm-12 col-xs-12 center-block no-float"><?php echo (!empty($rating) && $rating->total_counts != 0)? round_up($rate,1, PHP_ROUND_HALF_UP):'0';?></h2>
				<div class="w_star_rating col-lg-10 col-md-10 col-sm-10 col-xs-10 no-float center-block">
					<input id="" class="rating" data-stars="5" data-step="0.1" data-size="sm" value="<?php echo (!empty($rating) && $rating->total_counts != 0)? round_up($rate,1, PHP_ROUND_HALF_UP):'0';?>"/>
				</div>
				<button class="how col-lg-12 col-md-12 col-sm-12 col-xs-12 org-box"><i class="fa fa-comment"></i><?php echo $this->lang->line('outlet_feedback'); ?> </button>
			 </div>
         </a>

      </div>
      <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 mar-t15">
         <div class="dark_yellow activity new-currency new_activity">
            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 graduation">
              <img src="<?php echo IMAGE; ?>jod-credit-icon-white.png">
            </div>
            <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 graduation">
               <h1><?php echo $userType == 3 ? current_month_consumed_jod_credit($outlet_id,0,0) : current_month_consumed_jod_credit(0,0,1) ; ?></h1>
               <span><?php echo $this->lang->line('jod_credit_consumed'); ?> (<?php echo $this->lang->line('current_month'); ?>)</span>

            </div>
            <div class="clearfix"></div>
             <a href="<?php echo ($userType == 5) ? BASEURL.'admin/outlet_level_jod_credits' :  BASEURL.'admin/outlet_jod_credit/'.$outlet_id ; ?>">
				<button class="how col-lg-7 col-md-7 col-sm-7 col-xs-12 red-btn org-box " id="last_transactions"> <?php echo $this->lang->line('view_past_transactions'); ?> <i class="fa fa-arrow-circle-right"></i></button>
			</a>
            <div class="clearfix"></div>
         </div>
      </div>
   <!-- All jobs section  start -->

		<?php if(!empty($all_jobs_records)) { ?>
		<?php foreach ($all_jobs_records as $record){ ?>
			<!-- compeleted jobs -->
				<?php if(($record->is_completed == 1) && ($record->is_applicants_hired == 1) && ($record->is_delete == 0) ) {  ?>

					<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 org-box gray-box  no-padding left-mar job-box">
                    <div class="col-lg-10 col-md-11 col-sm-10 col-xs-9 center-block no-float">
						<h2 class="text-over" title="<?php echo $record->job_title; ?>"><?php echo $record->job_title; ?></h2>
						<p>
							<?php if(LoginUserDetails('role_id') == '5') { ?>
								<span><i class="fa fa-certificate"></i><font class="mar-left"><?php  echo getOutletName($record->outlet_id); ?></font></i></span>
							<?php } ?>
							<span><i class="fa fa-certificate"></i><font class="mar-left" title="<?php echo getJobTypeName($record->job_type_id); ?>"><?php echo strlen(getJobTypeName($record->job_type_id)) > 10 ? ucfirst(substr(getJobTypeName($record->job_type_id),0,10)).'...' : ucfirst(getJobTypeName($record->job_type_id)) ;  ?>  ($<?php echo $record->payment_amount; ?>/hr)</font></span>
							<span><i class="fa fa-certificate"></i><font class="mar-left"><?php echo date('d M-Y H:i A',strtotime($record->start_date."" .$record->start_time)); ?></font></span>
							<span> <font class="mar-left2"> <?php echo $this->lang->line('job_completed_u'); ?></font></span>
						</p></div>
						<div class="clearfix"></div>
						<a href="<?php echo BASEURL.'jobs/job_detail/'.$record->job_id;?>"><button class="job-btn block100 black-dark"><?php echo $this->lang->line('view_job_history'); ?></button></a>
					</div>
				   </div>
				<?php } ?>
				<?php if(($record->is_approved == 1 || $record->is_approved ==0 ) && ($record->is_applicants_hired == 0) && ($record->is_completed == 0) && ($record->is_delete == 0)) { ?>
				<!-- open job -->
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 org-box  no-padding left-mar job-box">
                      <div class="col-lg-10 col-md-11 col-sm-10 col-xs-9 center-block no-float">
						<h2 class="text-over" title="<?php echo $record->job_title; ?>"><?php echo $record->job_title; ?></h2>
						<p>
							
						<?php if(LoginUserDetails('role_id') == '5') { ?>
								<span><i class="fa fa-certificate"></i><font class="mar-left"><?php  echo getOutletName($record->outlet_id); ?></font></i></span>
							<?php } ?>
							<span><i class="fa fa-certificate"></i><font class="mar-left" title="<?php echo getJobTypeName($record->job_type_id); ?>"><?php echo strlen(getJobTypeName($record->job_type_id)) > 10 ? ucfirst(substr(getJobTypeName($record->job_type_id),0,10)).'...' : ucfirst(getJobTypeName($record->job_type_id)) ;  ?>  ($<?php echo $record->payment_amount; ?>/hr)</font></span>
							<span><i class="fa fa-certificate"></i><font class="mar-left"><?php echo date('d M-Y H:i A',strtotime($record->start_date."" .$record->start_time)); ?></font></span>
							<span> <font class="mar-left2"><?php echo getAppliedCandidate($record->job_id);?> <?php echo $this->lang->line('applied'); ?> / 0 <?php echo $this->lang->line('selected'); ?></font></span>
						</p>
                        </div>
						<div class="clearfix"></div>
						<a href="<?php echo BASEURL.'jobs/job_detail/'.$record->job_id.'/1';?>"><button class="job-btn block100 red-box"><?php echo $this->lang->line('view_open_job'); ?></button></a>
					</div>
				  </div>
				 <?php } ?>

				<?php if(($record->is_approved == 1 ) && ($record->is_applicants_hired == 1) && ($record->is_completed == 0) && ($record->is_delete == 0)) { ?>
				<!--- active job -->
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 org-box green-box no-padding left-mar job-box">
                      <div class="col-lg-10 col-md-11 col-sm-10 col-xs-9 center-block no-float">
						<h2 class="text-over" title="<?php echo $record->job_title; ?>"><?php echo $record->job_title; ?></h2>
						<p>
						<?php if(LoginUserDetails('role_id') == '5') { ?>
								<span><i class="fa fa-certificate"></i><font class="mar-left"><?php  echo getOutletName($record->outlet_id); ?></font></i></span>
							<?php } ?>
							<span><i class="fa fa-certificate"></i><font class="mar-left" title="<?php echo getJobTypeName($record->job_type_id); ?>"><?php echo strlen(getJobTypeName($record->job_type_id)) > 10 ? ucfirst(substr(getJobTypeName($record->job_type_id),0,10)).'...' : ucfirst(getJobTypeName($record->job_type_id)) ;  ?>  ($<?php echo $record->payment_amount; ?>/hr)</font></span>
							<span><i class="fa fa-certificate"></i><font class="mar-left"><?php echo date('d M-Y H:i A',strtotime($record->start_date."" .$record->start_time)); ?></font></span>
							<span> <font class="mar-left2"><?php echo getHiredCandidate($record->job_id);?> <?php echo $this->lang->line('hired'); ?></font></span>
						</p>
                        </div>
						<div class="clearfix"></div>
						<a href="<?php echo BASEURL.'jobs/job_detail/'.$record->job_id.'/2';?>"><button class="job-btn block100 dark-green"><?php echo $this->lang->line('view_active_job'); ?></button></a>
					</div>
				</div>
				<?php } ?>
				<!--- Cancelled job -->
				<?php if(($record->is_applicants_hired == 0) && ($record->is_completed == 0) &&  ($record->is_delete == 1)) { ?>
				 <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 org-box red-box2 no-padding left-mar job-box">
						  <div class="col-lg-10 col-md-11 col-sm-10 col-xs-9 center-block no-float">
                       <h2 class="text-over" title="<?php echo $record->job_title; ?>"><?php echo $record->job_title; ?></h2>
						<p>
						<?php if(LoginUserDetails('role_id') == '5') { ?>
								<span><i class="fa fa-certificate"></i><font class="mar-left"><?php  echo getOutletName($record->outlet_id); ?></font></i></span>
							<?php } ?>
							<span><i class="fa fa-certificate"></i><font class="mar-left" title="<?php echo getJobTypeName($record->job_type_id); ?>"><?php echo strlen(getJobTypeName($record->job_type_id)) > 10 ? ucfirst(substr(getJobTypeName($record->job_type_id),0,10)).'...' : ucfirst(getJobTypeName($record->job_type_id)) ;  ?>  ($<?php echo $record->payment_amount; ?>/hr)</font></span>
							<span><i class="fa fa-certificate"></i><font class="mar-left"><?php echo date('d M-Y H:i A',strtotime($record->start_date."" .$record->start_time)); ?></font></span>
							<span> <font class="mar-left2"><?php echo $this->lang->line('job_cancelled_u'); ?></font></span>
						</p>
                        </div>
						<div class="clearfix"></div>
						<a href="<?php echo BASEURL.'jobs/job_detail/'.$record->job_id;?>"><button class="job-btn block100 red-dark"><?php echo $this->lang->line('view_job_history'); ?></button></a>
					</div>
				  </div>
				 	<?php } ?>

		<?php } // foreach ?>
	<?php }else{ // if?>
	<?php } ?>


	<!-- All jobs section  start-->
<?php echo $this->pagination->create_links(); ?>
</div>

</div>
</div>
</div>

<script>
	jQuery(document).ready(function() {
      QuickSidebar.init(); // init quick sidebar
      Layout.init(); // init layout
   });
	$('.rating').rating('refresh', {
		disabled:true,
		showCaption: false
	});
</script>

