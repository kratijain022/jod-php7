<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$message = getGlobalMessage();
$userType=LoginUserDetails('role_id');
?>
<div class="right_col right_col" role="main">
	<?php
	if($message['type']=='success') {
	?>
	<div class="Metronic-alerts alert alert-success fade in" id="prefix_167847684379"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button><?php echo $message['msg'] ?></div>
	<?php
	} else if($message['type']=='error') {
	?>
	<div class="Metronic-alerts alert alert-danger fade in" id="prefix_167847684379"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button><?php echo $message['msg'] ?></div>
	<?php
	}
	?>
	<div class="row padding-top-one">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<ul class="breadcrumb breadcrumb2">
			<li><a href="<?php echo base_url(); ?>"><?php echo $this->lang->line('home'); ?></a></li>
			<li class="active"><?php echo $this->lang->line('location_list'); ?></li>
			</ul>
		</div>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<table class="table table-hovers" id="table3">
					<thead class="firt-top">
						<tr>
							<th style="display:none;"><?php echo $this->lang->line('created_date'); ?></th>
							<th><?php echo $this->lang->line('outlet_name'); ?></th>
							<th><?php echo $this->lang->line('address'); ?></th>
							<?php if($userType == 2) { ?>
							<th></th>
							<th><?php echo $this->lang->line('action'); ?></th>
							<th></th>
							<th></th>
							<?php }else if($userType == 1){ ?>
							<th><?php echo $this->lang->line('company_name'); ?></th>
							<?php } elseif($userType == 5) {?>
								<th><?php echo $this->lang->line('action'); ?></th>
							<?php } ?>

						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			<div class="clearfix"></div>
			</div>
		</div>
	</div>
</div>
<script src="<?php echo base_url('templates/system/js/bootbox.min.js'); ?>"></script>

<script>
	
<?php if(isset($site_language) && $site_language == "chinese") { ?>
	var lang_url = "//cdn.datatables.net/plug-ins/1.10.12/i18n/Chinese.json";
	bootbox.setDefaults({
		locale: "zh_CN"
    });
<?php }else{ ?>
	var lang_url = "//cdn.datatables.net/plug-ins/1.10.12/i18n/English.json";
	bootbox.setDefaults({
		locale: "en"
	 });
<?php }?>
 $('#table3').dataTable({
	  "bPaginate": true,
	  "ajax": '<?php echo base_url('admin/getOutletListByAjax'); ?>',
	  "bLengthChange": true,
	  "bFilter": true,
	  "bSort": true,
	  "bInfo": true,
	  "bAutoWidth": false,
	  "order": [[ 0, "desc" ]],
	  "processing": true,
      "serverSide": true,
	  "columnDefs": [ { "targets": 0, "bSortable": true,"orderable": true, "visible": false }],
	  "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
			if(aData[9] == 2){ // HQ User
				$('td:eq(2)', nRow).addClass('credits edit');
				$('td:eq(3)', nRow).addClass('hq-user');
				$('td:eq(4)', nRow).addClass('edit');
				$('td:eq(5)', nRow).addClass(aData[8]);
				$(nRow).addClass(aData[7]);

			}
			if(aData[9] == 5){ // Area manager
				$('td:eq(2)', nRow).addClass('edit light-green');

			}
		},
		"language": {
                "url": lang_url
            },
  });
function myFunction(id,status) {
	var mesg = '<?php  echo $this->lang->line('status_change_msg')."?"; ?>';
		bootbox.confirm(mesg, function(result) {
			if(result==true) {
				location.href="<?php echo base_url('admin/outlet_status'); ?>/"+id+"/"+status;
			}
		});
	}

	$(document).ready(function() {
	   setTimeout(function(){
		   $('#table3_length').show();
		   $('#table3_filter').show();
		   $('#table3_paginate').show();
		}, 1000);

	});
</script>
