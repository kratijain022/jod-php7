<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$message = getGlobalMessage();
$ci =& get_instance();
$site_language = $ci->session->userdata('language');

?>
<div class="right_col right_col" role="main">
	<?php
	if($message['type']=='success') {
	?>
	<div class="Metronic-alerts alert alert-success fade in" id="prefix_167847684379"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button><?php echo $message['msg'] ?></div>
	<?php
	} else if($message['type']=='error') {
	?>
	<div class="Metronic-alerts alert alert-danger fade in" id="prefix_167847684379"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button><?php echo $message['msg'] ?></div>
	<?php
	}
	?>
	<div class="row padding-top-one">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<ul class="breadcrumb breadcrumb2">
			<li><a href="<?php echo base_url(); ?>"><?php echo $this->lang->line('home'); ?></a></li>
			<li class="active"> <?php echo $this->lang->line('job_type_list'); ?></li>
			</ul>
		</div>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<table class="table table-hovers" id="table3">
					<thead class="firt-top">
						<tr>
							<th><?php echo $this->lang->line('created_date'); ?></th>
							<th><?php echo $this->lang->line('job_type_name'); ?></th>
							<th><?php echo $this->lang->line('action'); ?></th>
							<th></th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			<div class="clearfix"></div>
			</div>
		</div>
	</div>
</div>
<script>

<?php if(isset($site_language) && $site_language == "chinese") { ?>
	bootbox.setDefaults({
		locale: "zh_CN"
    });
	var lang_url = "//cdn.datatables.net/plug-ins/1.10.12/i18n/Chinese.json";
<?php }else{ ?>
	bootbox.setDefaults({
		locale: "en"
    });
	var lang_url = "//cdn.datatables.net/plug-ins/1.10.12/i18n/English.json";
<?php }?>	

$('#table3').dataTable({
	"ajax": '<?php echo base_url('admin/getJobTypesByAjax'); ?>',
	"bPaginate": true,
	"bLengthChange": true,
	"bFilter": true,
	"bSort": true,
	"bInfo": true,
	"bAutoWidth": false,
	"order": [[ 0, "desc" ]],
	"processing": true,
    "serverSide": true,
	"columnDefs": [ { "targets": 0, "bSortable": true,"orderable": true, "visible": false }],
	"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
				if(aData[4] != '') {
					$(nRow).addClass(aData[4]);
					//$(nRow).addClass('multiorder');
					}
				$('td:eq(1)', nRow).addClass('edit');
				$('td:eq(2)', nRow).addClass(aData[5]);
			},
	"language": {
                "url": lang_url
            },
});
function myFunction(id,status) {
	var mesg = '<?php  echo $this->lang->line('status_change_msg')."?"; ?>';
		bootbox.confirm(mesg, function(result) {
			if(result==true) {
				location.href="<?php echo base_url('admin/job_type_status'); ?>/"+id+"/"+status;
			}
		});
	}

	$(document).ready(function() {
	   setTimeout(function(){
		   $('#table3_length').show();
		   $('#table3_filter').show();
		   $('#table3_paginate').show();
		}, 1000);

	});
</script>
