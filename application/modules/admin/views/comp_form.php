<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$message = getGlobalMessage();

$userType=LoginUserDetails('role_id');
if(isset($records)) {
$status = $records->status;
}else {
$status = '1';
}
?>

<div class="right_col right_col " role="main">
	<?php
	if($message['type']=='success') {
	?>
	<div class="Metronic-alerts alert alert-success fade in" id="prefix_167847684379"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button><?php echo $message['msg'] ?></div>
	<?php
	} else if($message['type']=='error') {
	?>
	<div class="Metronic-alerts alert alert-danger fade in" id="prefix_167847684379"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button><?php echo $message['msg'] ?></div>
	<?php
	}
	?>
	<div class="row padding-top-one">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center title-head2">
			<h3><?php
					if($id) {
						echo $this->lang->line('edit_company'); 
						
						$action = $this->lang->line('update');
					} else {
						echo $this->lang->line('company_registration'); 
						$action = $this->lang->line('create');
					}
				?>		</h3>
		</div>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">


					<form action="<?php echo base_url('admin/company_form/'.$id);?>" enctype="multipart/form-data" method='post' id="form" data-parsley-validate onsubmit="$('#loader').fadeIn()">
					<div class="light-org-box pad-bottoms">
					<h4 class="text-center basic-details"><?php echo $this->lang->line('basic_details'); ?></h4>
						<div class="form-group">
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
								<label for="usr"><?php echo $this->lang->line('company_logo'); ?></label>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
								<?php if(isset($records->company_logo) && $records->company_logo != "") { ?>
									<img id="image-picker" style="width: 82%;" src="<?php echo base_url('uploads/company_logo/thumb/'.$records->company_logo); ?>">

								<?php } else { ?>
									<img id="image-picker" style="width: 82%;" src="<?php echo IMAGE ?>compay-logo.png">
								<?php } ?>
								<input type="file" id="image" name="image" style="display:none" onchange="read_url_image(this)" value="<?php echo set_value('company_logo', isset($records->company_logo) ? $records->company_logo : ''); ?>" />
								<span id="company_logo-error" class="help-block help-block-error error-red"><?php echo form_error('company_logo'); ?></span>
							</div>
						</div>
					<div class="clearfix"></div>
						<div class="form-group">
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
								<label for="usr"><?php echo $this->lang->line('company_name'); ?>*</label>
							</div>
							<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
								<input type="text" name="company_name" data-required="1" class="form-control" value="<?php echo set_value('company_name', isset($records->company_name) ? $records->company_name : ''); ?>" required data-parsley-maxlength="100" />
								<span id="company_name-error" class="help-block help-block-error"><?php echo form_error('company_name'); ?></span>
							</div>
						</div>
					<div class="clearfix"></div>
						<div class="form-group">
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
								<label class=" line-h" for="address"><?php echo $this->lang->line('company_main_address'); ?>*</label>
							</div>
							<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
								<textarea name="company_address" class="form-control" data-required="1" required ><?php echo set_value('company_address', isset($records->company_address) ? $records->company_address : ''); ?></textarea>
								<span id="company_address-error" class="help-block help-block-error"><?php echo form_error('company_address'); ?></span>
							</div>
						</div>

					<div class="clearfix"></div>
						<div class="form-group anker">
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
								<label class=" line-h" for="address"><?php echo $this->lang->line('contract_for_service'); ?></label>
							</div>
							<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
								<input type="file" id="contract_of_service" name="contract_of_service" onchange="read_url_pdf(this)" data-required="1" class="form-controls" value="<?php echo set_value('contract_of_service', isset($records->contract_of_service) ? $records->contract_of_service : ''); ?>" />
								<?php if(isset($records->contract_of_service) && $records->contract_of_service != '') { ?>
									<a href="<?php echo base_url('uploads/contract_of_service/'.$records->contract_of_service); ?>"><?php echo $records->contract_of_service; ?></a>
								<?php } ?>
								<span id="contract_of_service-error" class="help-block help-block-error"><?php echo form_error('contract_of_service'); ?></span>
							</div>
						</div>

					<div class="clearfix"></div>

							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
								<label class=" line-h" for="reg"><?php echo $this->lang->line('business_registration_no'); ?> *</label>
							</div>
							<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
								<input type="text" name="company_registration_no" data-required="1"  required class="form-control" value="<?php echo set_value('company_registration_no', isset($records->company_registration_no) ? $records->company_registration_no : ''); ?>" required data-parsley-maxlength="100" data-parsley-required-zh-cn="fdjfdhfjdgldghjfdghdgkdfgh"/>
								<span id="company_registration_no-error" class="help-block help-block-error"><?php echo form_error('company_registration_no'); ?></span>
							</div>
						<div class="clearfix"></div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
							<label class=" line-h" for="reg"><?php echo $this->lang->line('minimum_jod_credit_limit'); ?></label>
							</div>
							<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
								<input type="text" name="min_jod_credit_limit" data-required="1" required class="form-control" value="<?php  echo set_value('min_jod_credit_limit', isset($records->min_credit_limit) ? $records->min_credit_limit : ''); ?>" required data-parsley-maxlength="100" />
								<span id="min_jod_credit_limit-error" class="help-block help-block-error"><?php echo form_error('min_credit_limit'); ?></span>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 m-t80 no-padding">
							<button class="create org-box col-lg-3 col-md-3 col-xs-12 pull-right" type="submit" name='save' value="save">
								<span><?php echo $action; ?></span><i class="fa fa-chevron-right">
							</i>
							</button>
						</div>
					</form>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
</div>

<script>
<?php if(isset($site_language) && $site_language ==  "chinese"){?>
	window.Parsley.setLocale('zh-cn');
<?php }else{ ?>
		window.Parsley.setLocale('en');
	<?php }?>
$("#image-picker").click(function() {
    $("input[name='image']").click();
});

function read_url_image(input) {
	if (input.files && input.files[0]) {
		var file = input.files[0];
		//alert(file.type);
		var imageType = /image.*/;
		if (file.type.match(imageType)) {
			var reader = new FileReader();
			reader.onload = function (e) {
				$('#image-picker').attr('src', e.target.result);
			}
			reader.readAsDataURL(input.files[0]);
		} else {
			$("#image").val("");
			var alret_msg = '<?php echo $this->lang->line('image_not_supported_alert'); ?>';
			alert(alret_msg);
		}
	}
}
function read_url_pdf(input) {
	var file = input.files[0];
	var imageType = /pdf.*/;
	if (!input.files[0].name.match(imageType)) {
		$("#contract_of_service").val("");
		var alert_msg_pdf =  '<?php echo $this->lang->line('pdf_not_supported_alert'); ?>';
		alert(alert_msg_pdf);
	}
}


</script>
