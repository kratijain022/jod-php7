<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$message = getGlobalMessage();

//echo '<pre>';print_r($records);die;
?>
<div class="right_col right_col" role="main">
	<div class="row padding-top-one">
		<?php
		if($message['type']=='success') {
		?>
			<div class="Metronic-alerts alert alert-success fade in" id="prefix_167847684379"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button><?php echo $message['msg'] ?></div>
		<?php
		}
		?>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<ul class="breadcrumb breadcrumb2">
			<li><a href="<?php echo base_url(); ?>"><?php echo $this->lang->line('home'); ?></a></li>
			<li class="active"><?php echo $this->lang->line('billing_record'); ?></li>
			</ul>
		</div>

		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

		<!-- <form action="" method="get"> -->
		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 bottom-mar"><select name="month" class="form-control chosen-select select-our" id="month">
		<option value=""><?php echo $this->lang->line('month'); ?></option>
		<option <?php echo (!empty($month) && $month==1)?'selected':'';?> value="1"><?php echo $this->lang->line('jan'); ?></option>
		<option <?php echo (!empty($month) && $month==2)?'selected':'';?> value="2"><?php echo $this->lang->line('feb'); ?></option>
		<option <?php echo (!empty($month) && $month==3)?'selected':'';?> value="3"><?php echo $this->lang->line('mar'); ?></option>
		<option <?php echo (!empty($month) && $month==4)?'selected':'';?> value="4"><?php echo $this->lang->line('apr'); ?></option>
		<option <?php echo (!empty($month) && $month==5)?'selected':'';?> value="5"><?php echo $this->lang->line('may'); ?></option>
		<option <?php echo (!empty($month) && $month==6)?'selected':'';?> value="6"><?php echo $this->lang->line('jun'); ?></option>
		<option <?php echo (!empty($month) && $month==7)?'selected':'';?> value="7"><?php echo $this->lang->line('jul'); ?></option>
		<option <?php echo (!empty($month) && $month==8)?'selected':'';?> value="8"><?php echo $this->lang->line('aug'); ?></option>
		<option <?php echo (!empty($month) && $month==9)?'selected':'';?> value="9"><?php echo $this->lang->line('sept'); ?></option>
		<option <?php echo (!empty($month) && $month==10)?'selected':'';?> value="10"><?php echo $this->lang->line('oct'); ?></option>
		<option <?php echo (!empty($month) && $month==11)?'selected':'';?> value="11"><?php echo $this->lang->line('nov'); ?></option>
		<option <?php echo (!empty($month) && $month==12)?'selected':'';?> value="12"><?php echo $this->lang->line('dec'); ?></option>
		<option <?php echo (!empty($month) && $month==12)?'selected':'';?> value="12"><?php echo $this->lang->line('dec'); ?></option>
		</select></div>
		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 bottom-mar"><select name="year" class="form-control chosen-select select-our" id="year" >
		<option value=""><?php echo $this->lang->line('year'); ?></option>
		<option <?php echo (!empty($year) && $year==2015)?'selected':'';?> value="2015">2015</option>
		<option <?php echo (!empty($year) && $year==2016)?'selected':'';?> value="2016">2016</option>
		</select></div>
		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 bottom-mar"><select name="outlet" class="form-control chosen-select select-our" id="outlet" >
		<option value=""><?php echo $this->lang->line('outlets'); ?></option>
		<?php if(!empty($outlets)){
			foreach($outlets as $s_outlet){
				$selected = (!empty($outlet) && $outlet==$s_outlet->outlet_id)?'selected':'';
				echo '<option '.$selected.' value="'.$s_outlet->outlet_id.'">'.$s_outlet->outlet_name.'</option>';
			}
		}?>
		</select></div>


		<!-- <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 bottom-mar">
			<button class="create-no create org-box pull-right" type="submit" value="save">
				<span>Submit</span><i class="fa fa-chevron-right"></i>
			</button>
		</div> -->
		<!-- </form> -->

			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 table-no ">
				<table class="table table-hovers" id="table3">
					<thead class="firt-top">
						<tr>
							<th></th>
							<th><?php echo $this->lang->line('created_date'); ?></th>
							<th><?php echo $this->lang->line('outlets'); ?></th>
							<th><?php echo $this->lang->line('job'); ?></th>
							<th><?php echo $this->lang->line('time_in'); ?></th>
							<th><?php echo $this->lang->line('time_out'); ?></th>
							<th><?php echo $this->lang->line('hours'); ?></th>
							<th><?php echo $this->lang->line('wages'); ?></th>
							<th><?php echo $this->lang->line('job_status'); ?></th>
							</tr>
					</thead>
					<tbody>
					<?php /* if(!empty($records)){
						foreach($records as $record){
							?>
							<tr >
								<td></td>
								<td><?php echo date('d M Y' ,strtotime($record->job_created_at));?></td>
								<td><?php echo $record->outlet_name;?></td>
								<td><?php echo $record->job_title;?></td>
								<td><?php echo date('d M Y' ,strtotime($record->start_date));?></td>
								<td><?php echo date('d M Y' ,strtotime($record->end_date));?></td>
								<td><?php
								$time1 = strtotime($record->start_date.' '.$record->start_time);
								$time2 = strtotime($record->end_date.' '.$record->end_time);
								echo getTotalCompletedHoursJOb($record->job_id);?>
								</td>
								<td><?php $total_wages = getWages($record->job_id);
								echo (!empty($total_wages->total_wages))?$total_wages->total_wages:'0';
								?></td>
								<td><?php //echo ($record->is_completed==1)?'Completed':'Active';
									if(($record->is_completed == 1) && ($record->is_applicants_hired == 1) && ($record->is_delete == 0)) {
										echo 'Completed';
										}
									if(($record->is_approved == 1 || $record->is_approved ==0 ) && ($record->is_delete == 0) && ($record->is_applicants_hired == 0) && ($record->is_completed == 0)) {
										echo 'Open';
									}
									if(($record->is_approved == 1 ) && ($record->is_delete == 0) && ($record->is_applicants_hired == 1) && ($record->is_completed == 0)) {
										echo 'Active';
									}
									if(($record->is_applicants_hired == 0) && ($record->is_completed == 0) &&  ($record->is_delete == 1)) {
										echo 'Canceled';
									}
								?></td>

							</tr>
							<?php
						}
					} */?>

					</tbody>
				</table>
			<div class="clearfix"></div>
			</div>
		</div>
	</div>
</div>
<script src="<?php echo base_url('templates/system/js/bootbox.min.js'); ?>"></script>

<script>
<?php if(isset($site_language) && $site_language == "chinese") { ?>
	var lang_url = "//cdn.datatables.net/plug-ins/1.10.12/i18n/Chinese.json";
<?php }else{ ?>
	
	var lang_url = "//cdn.datatables.net/plug-ins/1.10.12/i18n/English.json";
<?php }?>	
	
$('#table3').dataTable({
	"bPaginate": true,
	"bLengthChange": true,
	"bFilter": true,
	"bSort": true,
	"bInfo": true,
	"bAutoWidth": false,
	"order": [[ 0, "desc" ]],
	"columnDefs": [ { "targets": 0, "bSortable": true,"orderable": true, "visible": false }],
	"processing": true,
	"serverSide": true,
	"ajax": {
		"url": "<?php echo base_url('admin/searchBillingRecordByAjax'); ?>",
		"data": function (d) {
				d.month = $('#month').val();
				d.year  = $('#year').val();
				d.outlet  = $('#outlet').val();
			}
	},
	"aoColumns": [null,null,null,null,null,null,{ "bSortable": false },{ "bSortable": false },{ "bSortable": false }],
	"language": {
                "url": lang_url
            },
});

var oTable;
oTable = $('#table3').dataTable();
$("#month").change(function() {
	oTable.fnFilter();
});
$("#year").change(function() {
	oTable.fnFilter();
});
$("#outlet").change(function() {
	oTable.fnFilter();
});

$(document).ready(function() {
	   setTimeout(function(){
		   $('#table3_length').show();
		   $('#table3_filter').show();
		   $('#table3_paginate').show();
		}, 1000);

	});
</script>
