<?php //echo "<pre>"; print_r(get_company_feedback()); die;
$data = get_company_feedback();
 ?>
<div class="right_col right_col" role="main">
		<ul class="breadcrumb breadcrumb2">
            <li><a href=""><?php echo $this->lang->line('home'); ?></a></li>
            <li class=""><?php echo $this->lang->line('dashboard'); ?></li>
              <li class="active"><?php echo $this->lang->line('outlets_feedback'); ?></li>
         </ul>
			<div class="clearfix"></div>

	<?php if(!empty($data)) { ?>

		<?php foreach ($data as $details) {// echo "<pre>";  print_r($details) ?>
			<?php if(!empty($details['outlet_details'])) {  ?>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 main-wrapper">
					<div class="job-btn block100 org-box col-lg-12 col-md-12 col-xs-12 text-center titles"><?php echo $details['outlet_details']->outlet_name;?></div>
				</div>
				<?php if(!empty($details['outlet_data'])) { ?>
				<?php foreach ($details['outlet_data'] as $feedback) { ?>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 main-wrapper">
					<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 gray-back2 no-padding">
						<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 left-sec no-padding">
							<h3 class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><?php echo strlen($feedback->first_name.' '.$feedback->last_name) > 12 ? substr(ucwords($feedback->first_name.' '.$feedback->last_name),0,12).".." : ucwords($feedback->first_name.' '.$feedback->last_name);?> <?php echo ($feedback->gender==1)?'(M)':'(F)';?></h3>
								<span class="profile_img"><img src="<?php echo checkImageFile(IMAGEURL.'applicantes/'.$feedback->portrait) == true ?  IMAGEURL.'applicantes/'.$feedback->portrait :  IMAGE.'/no_image.png'; ?><?php //echo base_url('templates/JOD/img//no_image.png');?>" class="g-img" style="width:203px; height:173px;"><span class="circle"><?php echo getTotalvoteCounts($feedback->applicant_user_account_id); ?></span></span>
								<div class="clearfix"></div>
								<div class="clearfix"></div>
								<a href="<?php echo BASEURL.'jobs/job_detail/'.$feedback->job_id;?>"><button class="job-btn block100 org-box col-xs-12" type="button" title="<?php echo $feedback->job_title; ?>"><?php echo strlen($feedback->job_title) > 20 ? substr($feedback->job_title,0,20).".." :$feedback->job_title ; ?></button></a>
						</div>

						<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
							<h4 class="col-lg-11 col-md-11 col-sm-11 col-xs-12 text-center"><?php echo $this->lang->line('applicants_feedback'); ?></h4>
							<div class="clearfix"></div>
							<div class="col-lg-9 col-md-10 col-sm-10 col-xs-8 text-center no-float center-block">
								<input id="" class="rating" data-stars="5" data-step="0.1" data-size="sm" value="<?php  echo $feedback->rating;?>"/>
							</div>
							<div class="clearfix"></div>
								<p class="discri col-lg-12 col-md-12 col-sm-12 col-xs-12"><?php echo $feedback->feedback_text;?> </p>
							<div class="clearfix"></div>
						</div>
					</div>
					<?php $applicant_data = get_applicants_feedback($feedback->applicant_user_account_id,$feedback->job_id);?>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 org-box8 no-padding">
					<div class="col-lg-11 col-md-11 col-sm-11 col-xs-12 no-float center-block text-center">
						<h4 class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><?php echo $this->lang->line('managers_feedback'); ?></h4>
						<div class="clearfix"></div>
							<div class="col-lg-9 col-md-10 col-sm-10 col-xs-8 text-center no-float center-block">
								<input id="" class="rating" data-stars="5" data-step="0.1" data-size="sm" value="<?php echo (!empty($applicant_data))? $applicant_data->rating:'';?>"/>
							</div>
						<div class="clearfix"></div>
						<p class="discri col-lg-12 col-md-12 col-sm-12 col-xs-12"> <?php echo (!empty($applicant_data))?$applicant_data->additional_comments:'';?></p>
						<div class="clearfix"></div>
					 </div>
                </div>
				</div>
			<?php } // foreach ?>
			<?php }else{ ?>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bmar">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 gray-back no-padding gheight">
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 gray-back  no-padding left-mar nob-mar job-box">
										<div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 center-block no-float">
											<div class="over-scroll-left">
											<h2><?php echo "No Feedback found for this outlet. "; ?></h2>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
			<?php } ?>
			<?php } ?>
		<?php }  ?>
	<?php } ?>
	</div>
<script>
	$('.rating').rating('refresh', {
		disabled:true,
		showCaption: true
	});
</script>
