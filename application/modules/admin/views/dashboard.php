<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<div class="right_col">
	<div class="row padding-top-one">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<ul class="breadcrumb">
				<li><a href="<?php echo base_url(); ?>"><?php echo $this->lang->line('home'); ?></a></li>
				<li class="active"><?php echo $this->lang->line('dashboard'); ?></li>
			</ul>
		</div>
		<div class="animated fadeIn col-lg-4 col-md-4 col-sm-4 col-xs-12">
			<a class="orange-box text-center" href="<?php echo base_url('admin/company_form'); ?>" > <span><i class="fa fa-plus"></i> </span>
				<h2 class="as"><?php echo $this->lang->line('add_company'); ?></h2>
			</a>
		</div>
		<div class="animated fadeIn col-lg-4 col-md-4 col-sm-4 col-xs-12">
			<a class="orange-box text-center" href="<?php echo base_url('admin/headquater_form'); ?>"> <span><i class="fa fa-plus"></i> </span>
				<h2 class="as"><?php echo $this->lang->line('add_hq_manager'); ?></h2>
			</a>
		</div>
		<div class="animated fadeIn col-lg-4 col-md-4 col-sm-4 col-xs-12">
			<a href="<?php echo base_url('admin/notification');?>" class="orange-box text-center padding-left-right">
				<span class="pull-left"><i class="fa fa-bell-o"></i> </span>
				<span class="pull-right col-sm-6 no-padding text-left">
					<h1 class="as"><?php
					$all_outlets = get_outlet();
						echo (!empty($all_outlets))?$all_outlets:'0';
					?></h1>
					<h3 class="as"><?php echo $this->lang->line('notifications'); ?></h3>
				</span>
			</a>
		</div>
	</div>
	<div class="row padding-top-one">
		<div class="animated fadeIn col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="reports-charts text-center"> <?php echo $this->lang->line('reports_charts'); ?> 
			</div>
		</div>
	</div>
</div>
</div>
</div>

</div>
</div>
<script>
jQuery(document).ready(function() {
   QuickSidebar.init(); // init quick sidebar
   Layout.init(); // init layout
});
</script>
