<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$message = getGlobalMessage();
?>
<div class="right_col right_col" role="main">
	<div class="row padding-top-one">
		<?php
if($message['type']=='success') {
?>
<div class="Metronic-alerts alert alert-success fade in" id="prefix_167847684379"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button><?php echo $message['msg'] ?></div>
<?php
}
?>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<ul class="breadcrumb breadcrumb2">
			<li><a href="<?php echo base_url(); ?>"><?php echo $this->lang->line('home'); ?></a></li>
			<li class="active"><?php echo $this->lang->line('list_area_manager'); ?></li>
			</ul>
		</div>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<table class="table table-hovers no-wrap"  id="table3">
					<thead class="firt-top">
						<tr>
							<th><?php echo $this->lang->line('created_date'); ?></th>
							<th><?php echo $this->lang->line('name'); ?></th>
							<th><?php echo $this->lang->line('outlets'); ?></th>
							<th><?php echo $this->lang->line('contact'); ?></th>

							<th><?php echo $this->lang->line('action'); ?></th>
							<th></th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			<div class="clearfix"></div>
			</div>
		</div>
	</div>
</div>
<script src="<?php echo base_url('templates/system/js/bootbox.min.js'); ?>"></script>

<script>
<?php if(isset($site_language) && $site_language == "chinese") { ?>
	bootbox.setDefaults({
		locale: "zh_CN"
    });
	var lang_url = "//cdn.datatables.net/plug-ins/1.10.12/i18n/Chinese.json";
<?php }else{ ?>
	bootbox.setDefaults({
		locale: "en"
	 });
	var lang_url = "//cdn.datatables.net/plug-ins/1.10.12/i18n/English.json";
<?php }?>	
	
$('#table3').dataTable({
	"bPaginate": true,
	"bLengthChange": true,
	"bFilter": true,
	"bSort": true,
	"bInfo": true,
	"bAutoWidth": false,
	"order": [[ 0, "desc" ]],
	"columnDefs": [ { "targets": 0, "bSortable": true,"orderable": true, "visible": false }],
	"processing": true,
    "serverSide": true,
	"ajax": '<?php echo base_url('admin/areaManagerListByAjax'); ?>',
	"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
				if(aData[6] != '') {
					$(nRow).addClass(aData[6]);
					//$(nRow).addClass('multiorder');
				}
			$('td:eq(3)', nRow).addClass('edit');
			$('td:eq(4)', nRow).addClass(aData[7]);
	},
	"language": {
                "url": lang_url
            },
	"aoColumns": [null,null,{ "bSortable": false },null,{ "bSortable": false },{ "bSortable": false }],
});
	function myFunction(id,userAccountsId,status) {
		var mesg = '<?php  echo $this->lang->line('status_change_msg')."?"; ?>';
		bootbox.confirm(mesg, function(result) {
			if(result==true) {
				location.href="<?php echo base_url('admin/area_manager_status'); ?>/"+id+"/"+userAccountsId+"/"+status;
			}
		});
	}
	$(document).ready(function() {
	   setTimeout(function(){
		   $('#table3_length').show();
		   $('#table3_filter').show();
		   $('#table3_paginate').show();
		}, 1000);

	});
</script>
