<?php $avg_rating = getAverageRating($applicant_profile->user_accounts_id);?>
<?php //echo "<pre>"; print_r($applicant_profile);  die;?>
<div class="right_col right_col" role="main">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 main-wrapper">
		<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12"> <!--- User info div start -->
			<div class="gray-back div-height">   <!---  first section div start -->
				<div class="col-lg-5 col-md-6 col-sm-6 col-xs-12 left-sec"> <!--- Profile Img Div strat-->
					<span class="profile_img">
						<img style="width:203px; height:173px;" src="<?php echo checkImageFile(IMAGEURL.'applicantes/'.$applicant_profile->portrait) == true ?  IMAGEURL.'applicantes/'.$applicant_profile->portrait :  IMAGE.'/no_image.png'; ?>" />
					<span class="circle"><?php echo getTotalvoteCounts($applicant_profile->user_accounts_id); ?></span></span>
				</div> <!--- Profile Img Div end-->
				<div class="col-lg-7 col-md-6 col-sm-6 col-xs-12">  <!-- Profile Div start -->
					<h3 class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><?php echo ucwords($applicant_profile->first_name.' '.$applicant_profile->last_name);?> <?php echo ($applicant_profile->gender==1)?'(M)':'(F)';?></h3>
					<div class="clearfix"></div>
					<p class="discri col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<?php
							$dob = date_create($applicant_profile->date_of_birth);
							$today = date_create(date('Y-m-d'));
							$age = date_diff($dob,$today, true);
							echo $age->y;
						?> years old<br />
					     <span class="address-box"><?php echo strlen($applicant_profile->address) > 90 ? substr($applicant_profile->address,0,90).".." : $applicant_profile->address ; ?></span><br />
					</p>
					<div class="clearfix"></div>
					<p class="discri col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<span>
							<input type="checkbox" <?php echo ($applicant_profile->is_NEA_certified==1)?'checked="checked"':'';?>/>
						</span>
						<span><?php echo $this->lang->line('nea_food_handler'); ?></span>
					</p>
					<div class="clearfix"></div>
					<?php if(LoginUserDetails('role_id') == 1 ){ ?>
						 <p class="discri col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<?php echo $this->lang->line('unique_id'); ?> : <?php echo $applicant_profile->unique_id;?><br />
							<?php echo $this->lang->line('contact_no'); ?> : <?php echo $applicant_profile->contact_no;?><br />
							<?php echo $this->lang->line('user_account_id'); ?> : <?php echo $applicant_profile->user_accounts_id;?><br />
						</p> 
					<?php }?>
					<!-- Removed by kratika on date 05 jan 2017 
					<p class="discri col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<?php $exprience_details =  getEmploymentHistory($applicant_profile->user_accounts_id);
						if(!empty($exprience_details)):
						$exp_data = end($exprience_details);?>
						   Years of Experience: <?php echo $exp_data->length_of_service;?> <?php echo ($exp_data->length_of_service>1)?'yrs':'yr';?><br />
						   Job Roles: <?php echo $exp_data->job_types;?><br />
						   Past Employers: <?php echo $exp_data->employer;?> <br />
						<?php else: ?>
							No experience <br />
						<?php endif;?>
					</p> -->
				</div><!-- Profile Div end -->
				<div class="clearfix"></div>
			</div>  <!---  first section div start -->
		</div> <!--- User info div start -->

		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 ">  <!-- working history Div start -->
			<div class="gray-back div-height">
				<div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 work-his no-float center-block">
					<h2><?php echo $this->lang->line('working_history'); ?></h2>
					<?php $exprience_details =  getEmploymentHistory($applicant_profile->user_accounts_id); if(!empty($exprience_details)): $exp_data = end($exprience_details);?>
					<span><?php echo $this->lang->line('years_of_experience'); ?>: <br /><?php echo $exp_data->length_of_service;?> <?php echo ($exp_data->length_of_service > 1 )?'yrs':'yr';?></span>
					<span><?php echo $this->lang->line('job_roles'); ?>: <?php echo $exp_data->job_types;?></span>
					<span><?php echo $this->lang->line('past_employers'); ?>: <?php echo $exp_data->employer;?></span>
					<?php else: ?><?php echo $this->lang->line('no_experience'); ?><br /> <?php endif;?>
				</div>
			</div>
		</div> <!-- working history Div end -->
		<div class="clearfix"></div>

		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 top-mar"> <!-- working hours  start -->
			<div class="org-box hours-com">
				<h2><?php echo getTotalCompletedHours($applicant_profile->user_accounts_id); ?></h2>
				<h3><?php echo $this->lang->line('hour_completed_with'); ?><br /><?php echo getTotalvoteCounts($applicant_profile->user_accounts_id); ?> JODs</h3>
			</div>
		</div>
		<!-- working hours  end -->
		<!-- Average rating start -->
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12  top-mar">
			<div class="org-box4 org-box hours-com">
				<div class="col-lg-10 ol-md-10 ol-sm-10 ol-xs-12 no-float center-block white-theme">
						<input id="average_rating" class="rating" data-stars="5" data-step="0.1" data-size="sm" />
						</div>
				<h3><strong><?php echo $this->lang->line('average_rating'); ?></strong> <br /><?php echo $this->lang->line('total_votes'); ?>: <?php echo getTotalvoteCounts($applicant_profile->user_accounts_id); ?></h3>
			</div>
		</div>
			<!-- Average rating end-->
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12  top-mar">
			<div class="dark-org org-box hours-com check2">
				<div class="col-lg-2 col-md-2 col-sm-2 col-xs-3 center-block no-float">
					<div class="check-box">
						<div class="check-wrapper check-wrapper2">
							<input type="checkbox"   <?php echo ($applicant_profile->is_NEA_certified==1)?'checked="checked"':''; ?> value="1" id="check1" name="payment_mode" data-parsley-multiple="payment_mode" data-parsley-id="9131" autocomplete="off">
							<label for="check1"> </label>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
				<h3><strong><?php echo $this->lang->line('nea_food_handler'); ?></strong></h3>
			</div>
		</div>
		<!-- Feedback button -->
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 top-mar">
			<button name="back" type="button" class="create create2 org-box col-lg-12 col-md-12 col-xs-12 pull-left"><span><?php echo $this->lang->line('feedback'); ?></span></button>
		</div>

		<div class="clearfix"></div>
		<!--- Feedback job section first -->
		<?php if($applicants_feedback != false) { $flag = true;?>
			<div id="applicants_feedback_wrapper" class="a_feedback_wrapper">
				<?php foreach($applicants_feedback as $feedback) { ?>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 top-mar fish_icon">
					<div class="gray-back-new">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
							<img src="<?php echo checkImageFile(base_url('uploads/outlet_logo/thumb/'.$feedback->outlet_logo)) == true ? base_url('uploads/outlet_logo/thumb/'.$feedback->outlet_logo) : IMAGE.'/no_image.png' ; ?>" class="loader-img img-responsive" >
						</div>
						<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 text-center">
							<h1 class=""><?php echo $feedback->job_title; ?></h1>
							<input id="feedback_rating_<?php echo 	$feedback->feedback_id; ?>" class="rating" data-stars="5" data-step="0.1" data-size="sm" rate="<?php echo $feedback->rating; ?>"/>
						</div>
						<div class="clearfix"></div>
						<div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 dummy-text center-block no-float">
							<?php echo $feedback->additional_comments; ?>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
		<?php } // end foreach ?>
			</div>
		<?php } else{ $flag = false; ?>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bmar">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 gray-back no-padding gheight">
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 gray-back  no-padding left-mar nob-mar job-box">
										<div class="col-lg-10 col-md-10 col-sm-10 col-xs-9 center-block no-float">
											<div class="over-scroll-left">
											<h2><?php echo $this->lang->line('no_feedback_message'); ?></h2>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

		<?php } ?>
		<!--- Feedback job section end -->
		<div class="clearfix"></div>
		<!--- open jobs -->
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 top-mar">
			<button name="back" type="button" class="create  org-box col-lg-12 col-md-12 col-xs-12 pull-left"><span><?php echo $this->lang->line('your_open_job'); ?>  <?php echo ucwords($applicant_profile->first_name.' '.$applicant_profile->last_name);?><?php echo $this->lang->line('has_applied_for'); ?></span></button>
		</div>
		<?php if(!empty($applicants_open_job)) { ?>
			<div id="jobs_wrapper" class="jobs_wrapper">
			<?php foreach($applicants_open_job as $job ) { ?>
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 org-box  no-padding left-mar job-box">  <div class="col-lg-10 col-md-10 col-sm-10 col-xs-9 center-block no-float">
						<h2 title="<?php echo $job->job_title; ?>"><?php echo ucfirst(substr($job->job_title,0,8)).'...';  ?> </h2>
						<p>
							<?php if(LoginUserDetails('role_id') == '5' ||  LoginUserDetails('role_id') == '2' ||  LoginUserDetails('role_id') == '1') { ?>
								<span title="<?php echo getOutletName($job->outlet_id); ?>"><i class="fa fa-certificate"></i><font class="mar-left"><?php  echo strlen(getOutletName($job->outlet_id)) > 19 ? ucfirst(substr(getOutletName($job->outlet_id),0,19)).'....' : getOutletName($job->outlet_id) ; ?></font></i></span>
							<?php }?>
							<span><i class="fa fa-certificate"></i><font class="mar-left"><?php echo ucfirst(substr($job->job_title,0,8)).'...';  ?>($<?php echo $job->payment_amount; ?>/hr)</font></span>
							<span><i class="fa fa-certificate"></i><font class="mar-left"><?php echo date('d M',strtotime($job->start_date)); ?> - <?php echo date('d M',strtotime($job->end_date)); ?> </font></span>
							<span><i class="fa fa-certificate"></i><font class="mar-left"><?php echo getAppliedCandidate($job->job_id);?>  applicants</font></span>
							<span><i class="fa fa-certificate"></i><font class="mar-left"><?php echo getHiredCandidate($job->job_id);?> selected</font></span>
						</p>
                        </div>
						<div class="clearfix"></div>
						<!-- <a href="<?php echo BASEURL.'jobs/job_detail/'.$job->job_id.'/1';?>"><button class="job-btn block100 red-box">view open job</button></a> -->
					</div>
				</div>
			<?php } ?>
			</div>
		<?php } else{ ?>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bmar">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 gray-back no-padding gheight">
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 gray-back  no-padding left-mar nob-mar job-box">
										<div class="col-lg-10 col-md-10 col-sm-10 col-xs-9 center-block no-float">
											<div class="over-scroll-left">
												<h2><?php echo ucwords($applicant_profile->first_name.' '.$applicant_profile->last_name);?> <?php echo $this->lang->line('not_applied_for_jobs'); ?></h2>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
		<?php } ?>
		<!-- Active jobs -->
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 top-mar">
			<button name="back" type="button" class="create  org-box col-lg-12 col-md-12 col-xs-12 pull-left"><span><?php echo $this->lang->line('your_active_job_that'); ?> <?php echo ucwords($applicant_profile->first_name.' '.$applicant_profile->last_name);?> <?php echo $this->lang->line('has_selected_for'); ?></span></button>
		</div>
		<?php if(!empty($applicants_active_job)) { ?>
			<div id="active_jobs_wrapper" class="jobs_wrapper">
			<?php foreach($applicants_active_job as $job ) { ?>
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 org-box green-box no-padding left-mar job-box">
						  <div class="col-lg-10 col-md-10 col-sm-10 col-xs-9 center-block no-float">
                        <h2><?php echo ucfirst(substr($job->job_title,0,8)).'...';  ?> </h2>
						<p>
							<?php if(LoginUserDetails('role_id') == '5' ||  LoginUserDetails('role_id') == '2' ||  LoginUserDetails('role_id') == '1') { ?>
								<span title="<?php echo getOutletName($job->outlet_id); ?>"><i class="fa fa-certificate"></i><font class="mar-left"><?php  echo strlen(getOutletName($job->outlet_id)) > 19 ? ucfirst(substr(getOutletName($job->outlet_id),0,19)).'....' : getOutletName($job->outlet_id) ; ?></font></i></span>
							<?php }?>
							<span><i class="fa fa-certificate"></i><font class="mar-left"><?php echo ucfirst(substr($job->job_title,0,8)).'...';  ?>($<?php echo $job->payment_amount; ?>/hr)</font></span>
							<span><i class="fa fa-certificate"></i><font class="mar-left"><?php echo date('d M',strtotime($job->start_date)); ?> - <?php echo date('d M',strtotime($job->end_date)); ?> </font></span>
							<span><i class="fa fa-certificate"></i><font class="mar-left"><?php echo getAppliedCandidate($job->job_id);?>  applicants</font></span>
							<span><i class="fa fa-certificate"></i><font class="mar-left"><?php echo getHiredCandidate($job->job_id);?> selected</font></span>
						</p>
                        </div>
						<div class="clearfix"></div>
						<!-- <a href="<?php echo BASEURL.'jobs/job_detail/'.$job->job_id.'/1';?>"><button class="job-btn block100 red-box">view open job</button></a> -->
					</div>
				</div>
			<?php } ?>
			</div>
		<?php } else{ ?>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bmar">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 gray-back no-padding gheight">
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 gray-back  no-padding left-mar nob-mar job-box">
										<div class="col-lg-10 col-md-10 col-sm-10 col-xs-9 center-block no-float">
											<div class="over-scroll-left">
											<h2><?php echo ucwords($applicant_profile->first_name.' '.$applicant_profile->last_name);?> <?php echo $this->lang->line('not_selected_for_jobs'); ?></h2>
										</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
		<?php } ?>
		<div class="clearfix"></div>
		<!-- compeleted and cancel job -->
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 top-mar">
			<button name="back" type="button" class="create  org-box col-lg-12 col-md-12 col-xs-12 pull-left"><span> <?php echo $this->lang->line('job_history_of'); ?> <?php echo ucwords($applicant_profile->first_name.' '.$applicant_profile->last_name);?> </span></button>
		</div>
		<?php if(!empty($cancel_compelted_jobs)) {   ?>
			<div id="compelted_jobs_wrapper" class="jobs_wrapper">
			<?php foreach($cancel_compelted_jobs as $job ) { ?>
				<?php if($job->is_completed == 1) {?>
					<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 org-box gray-box  no-padding left-mar job-box">
						  <div class="col-lg-10 col-md-10 col-sm-10 col-xs-9 center-block no-float">
                        <h2><?php echo ucfirst(substr($job->job_title,0,8)).'...';  ?> </h2>
						<p>
							<?php if(LoginUserDetails('role_id') == '5' ||  LoginUserDetails('role_id') == '2' ||  LoginUserDetails('role_id') == '1') { ?>
								<span title="<?php echo getOutletName($job->outlet_id); ?>"><i class="fa fa-certificate"></i><font class="mar-left"><?php  echo strlen(getOutletName($job->outlet_id)) > 19 ? ucfirst(substr(getOutletName($job->outlet_id),0,19)).'....' : getOutletName($job->outlet_id) ; ?></font></i></span>
							<?php }?>
							<span><i class="fa fa-certificate"></i><font class="mar-left"><?php echo ucfirst(substr($job->job_title,0,8)).'...';  ?>($<?php echo $job->payment_amount; ?>/hr)</font></span>
							<span><i class="fa fa-certificate"></i><font class="mar-left"><?php echo date('d M',strtotime($job->start_date)); ?> - <?php echo date('d M',strtotime($job->end_date)); ?> </font></span>
							<span><i class="fa fa-certificate"></i><font class="mar-left"><?php echo getAppliedCandidate($job->job_id);?>  applicants</font></span>
							<span><i class="fa fa-certificate"></i><font class="mar-left"><?php echo getHiredCandidate($job->job_id);?> selected</font></span>
						</p>
                        </div>
						<div class="clearfix"></div>
						<!-- <a href="<?php echo BASEURL.'jobs/job_detail/'.$job->job_id.'/1';?>"><button class="job-btn block100 red-box">view open job</button></a> -->
					</div>
				</div>
				<?php  }   ?>
				<?php if($job->cancel_job == 1 && ($job->applicant_user_account_id == $applicant_profile->user_accounts_id)) { ?>
					<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 org-box red-box2 no-padding left-mar job-box">
                      <div class="col-lg-10 col-md-10 col-sm-10 col-xs-9 center-block no-float">
						<h2><?php echo ucfirst(substr($job->job_title,0,8)).'...';  ?> </h2>
						<p>
							<?php if(LoginUserDetails('role_id') == '5' ||  LoginUserDetails('role_id') == '2' ||  LoginUserDetails('role_id') == '1') { ?>
								<span title="<?php echo getOutletName($job->outlet_id); ?>"><i class="fa fa-certificate"></i><font class="mar-left"><?php  echo strlen(getOutletName($job->outlet_id)) > 19 ? ucfirst(substr(getOutletName($job->outlet_id),0,19)).'....' : getOutletName($job->outlet_id) ; ?></font></i></span>
							<?php }?>
							<span><i class="fa fa-certificate"></i><font class="mar-left"><?php echo ucfirst(substr($job->job_title,0,8)).'...';  ?>($<?php echo $job->payment_amount; ?>/hr)</font></span>
							<span><i class="fa fa-certificate"></i><font class="mar-left"><?php echo date('d M',strtotime($job->start_date)); ?> - <?php echo date('d M',strtotime($job->end_date)); ?> </font></span>
							<span><i class="fa fa-certificate"></i><font class="mar-left"><?php echo getAppliedCandidate($job->job_id);?>  applicants</font></span>
							<span><i class="fa fa-certificate"></i><font class="mar-left"><?php echo getHiredCandidate($job->job_id);?> selected</font></span>
						</p>
                        </div>
						<div class="clearfix"></div>
						<!-- <a href="<?php echo BASEURL.'jobs/job_detail/'.$job->job_id.'/1';?>"><button class="job-btn block100 red-box">view open job</button></a> -->
					</div>
				</div>
				<?php  } ?>
			<?php } ?>
			</div>
			<?php } else {  ?>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bmar">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 gray-back no-padding gheight">
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 gray-back  no-padding left-mar nob-mar job-box">
										<div class="col-lg-10 col-md-10 col-sm-10 col-xs-9 center-block no-float">
											<div class="over-scroll-left">
											<h2><?php echo $this->lang->line('no_jobs_found'); ?></h2>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
			<?php } ?>
	</div>
</div>
<script>
	$('#average_rating').rating('refresh', {
		disabled:true,
		showCaption: false
	});
	var rating = '<?php echo $avg_rating; ?>';

	$('#average_rating').rating('update', rating);

	$('.rating').rating('refresh', {
		disabled:true,
		showCaption: false
	});

	var flag = '<?php echo $flag ; ?>';
	if(flag == true){
		var applicants_obj  = JSON.parse(decodeURIComponent("<?php echo rawurlencode(json_encode($applicants_feedback)); ?>"));
		applicants_obj = JSON.parse(applicants_obj);
		$.each(applicants_obj, function( index, value ) {
				var id = "#feedback_rating_"+value.feedback_id ;
				var  rate = $(id).attr('rate');
				$(id).rating('update', rate);

		});
	}

$( document ).ready(function() {
$('input[type="checkbox"]').click(function(event) {
    this.checked = false; // reset first
    event.preventDefault();
});
});
</script>
