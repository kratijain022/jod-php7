<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$message = getGlobalMessage();
?>
<div class="right_col right_col" role="main">
	<?php
	if($message['type']=='success') {
	?>
	<div class="Metronic-alerts alert alert-success fade in" id="prefix_167847684379"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button><?php echo $message['msg'] ?></div>
	<?php
	} else if($message['type']=='error') {
	?>
	<div class="Metronic-alerts alert alert-danger fade in" id="prefix_167847684379"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button><?php echo $message['msg'] ?></div>
	<?php
	}
	?>
	<div class="row padding-top-one">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<ul class="breadcrumb breadcrumb2">
			<li><a href="<?php echo base_url(); ?>"><?php echo $this->lang->line('home'); ?></a></li>
			<li class="active"><?php echo $this->lang->line('company_list'); ?></li>
			</ul>
		</div>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<table class="table table-hovers" id="table3">
					<thead class="firt-top">
						<tr>
							<th><?php echo $this->lang->line('created_date'); ?></th>
							<th><?php echo $this->lang->line('company_name'); ?></th>
							<th><?php echo $this->lang->line('address'); ?></th>
							<th><?php echo $this->lang->line('action'); ?></th>
							<th></th>
							<th></th>
							<th></th>
						</tr>
					</thead>
					<tbody>
					<?php
							/*$i=1;
							foreach($records as $company)
							{
								if($company->status==0) {
									$class = "enable";
									$name = "Enable";
									$fa = "fa fa-check";
									$trClass = "gray-box";
									$title = "Click to enable record";
								} else if($company->status==1) {
									$class = "disable";
									$name = "Disable";
									$fa = "fa fa-ban";
									$trClass = "";
									$title = "Click to disable record";
								}
							?>
						<tr class="<?php echo $trClass; ?>">
							<td><?php echo $company->created_at; ?></td>
							<td><?php echo ucfirst($company->company_name); ?></td>
							<td><?php echo substr($company->company_address,0,500); ?></td>
							  <td class="credits edit"><a href="<?php echo base_url('admin/assign_credits_to_company/'.$company->company_id); ?>">
								<span><img src="<?php echo IMAGE; ?>jod-credit-icon-white.png"></span>
								<span>credits</span>
								</a>
							</td>
							<td class="hq-user">
								<?php if(!in_array($company->company_id,$companyArray)) { ?>
								<a href="<?php echo base_url('admin/headquater_form/0/'.$company->company_id); ?>">
								<span><i class="fa fa-plus"></i></span>
								<span>HQ-User</span>
								</a>
								<?php } else { ?>
								<a href="<?php echo base_url('admin/headquater_form/'.$managerArray[$company->company_id].'/'.$company->company_id); ?>">
									<span><i class="fa fa-pencil"></i></span>
									<span>Edit HQ-User</span>
								</a>
								<?php } ?>
							</td>
							<td class="edit">
								<a href="<?php echo base_url('admin/company_form/'.$company->company_id); ?>">
									<span><i class="fa fa-pencil"></i></span>
									<span>Edit</span>
								</a>
							</td>

							<td class="<?php echo $class; ?>"><a href="javascript:;" title="<?php echo $title; ?>" onclick="myFunction('<?php echo $company->company_id; ?>','<?php echo $company->status; ?>')">
								<span><i class="<?php echo $fa ?>"></i></span>
								<span><?php echo $name; ?></span>
							</a></td>

						</tr>
						<?php } */?>
					</tbody>
				</table>
			<div class="clearfix"></div>
			</div>
		</div>
	</div>
</div>

<script>

<?php if(isset($site_language) && $site_language == "chinese") { ?>
	bootbox.setDefaults({
		locale: "zh_CN"
    });
	var lang_url = "//cdn.datatables.net/plug-ins/1.10.12/i18n/Chinese.json";
<?php }else{ ?>
	bootbox.setDefaults({
		locale: "en"
    });
	var lang_url = "//cdn.datatables.net/plug-ins/1.10.12/i18n/English.json";
<?php }?>


$('#table3').dataTable({
			"bPaginate": true,
			"bLengthChange": true,
			"bFilter": true,
			"bSort": true,
			"bInfo": true,
			"bAutoWidth": false,
			"order": [[ 0, "desc" ]],
			"columnDefs": [ { "targets": 0, "bSortable": true,"orderable": true, "visible": false },
							{ "targets": 3, "bSortable": false,"orderable": false, "visible": true },
							{ "targets": 4, "bSortable": false,"orderable": false, "visible": true },
							{ "targets": 5, "bSortable": false,"orderable": false, "visible": true } ],
			"processing": true,
			"serverSide": true,
			"ajax": '<?php echo base_url('admin/getCompanyListByAjax'); ?>',
			"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
				if(aData[4] != '') {
					$(nRow).addClass(aData[7]);
				}
				$('td:eq(2)', nRow).addClass('credits edit');
				$('td:eq(3)', nRow).addClass('hq-user');
				$('td:eq(4)', nRow).addClass('edit');
				$('td:eq(5)', nRow).addClass(aData[8]);
			},
			"language": {
                "url": lang_url
            },


  });
function myFunction(id,status) {
	var mesg = '<?php  echo $this->lang->line('status_change_msg')."?"; ?>';
		bootbox.confirm(mesg, function(result) {
			if(result==true) {
				location.href="<?php echo base_url('admin/company_status'); ?>/"+id+"/"+status;
			}
		});
	}
$(document).ready(function() {
	   setTimeout(function(){
		   $('#table3_length').show();
		   $('#table3_filter').show();
		   $('#table3_paginate').show();
		}, 1000);

	});
</script>
