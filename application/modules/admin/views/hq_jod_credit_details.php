<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php
	$userType=LoginUserDetails('role_id');
	$message = getGlobalMessage();
	
?>
<div class="right_col right_col" role="main">
	<?php
	if($message['type']=='success') {
	?>
	<div class="Metronic-alerts alert alert-success fade in" id="prefix_167847684379"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button><?php echo $message['msg'] ?></div>
	<?php
	} else if($message['type']=='error') {
	?>
	<div class="Metronic-alerts alert alert-danger fade in" id="prefix_167847684379"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button><?php echo $message['msg'] ?></div>
	<?php
	}
	?>
	<div class="row padding-top-one">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<ul class="breadcrumb breadcrumb2">
			<li><a href="<?php echo base_url(); ?>"><?php echo $this->lang->line('home'); ?></a></li>
			<li class="active"> <?php echo $this->lang->line('jod_credits'); ?></li>
			</ul>
		</div>
		<?php if(!empty($company_details)) { ?>
			<div id="hq_unassigned_credit">
				<div class="col-lg-10 col-md-10 col-xs-12 marb-22">
					<div class="light-gray-box orang">
						<div class="col-md-2 col-xs-12 text-center ava-cre2">
							<img src="<?php echo IMAGE; ?>jod-credit-icon-white.png">
						</div>
						<h3 class="pull-left star-h3-white star-h3 col-md-7 col-xs-12 text-center"><?php echo $this->lang->line('jod_credit_available_HQ'); ?></h3>
						<div class="col-md-3 col-xs-12 text-center  pull-right ava-credits">
							<h2 class="star-h3-white"><?php echo isset($company_details->current_jod_credit) ?  sprintf('%0.2f',$company_details->current_jod_credit) : "N/A" ;?></h2>
						</div>

						<div class="clearfix"></div>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="col-lg-2 col-md-2 col-xs-12 marb-22">
					<button class="light-gray-box2  orang no-btn" data-toggle="modal" data-target="#purchase_jod_credit"> <i class="fa fa-money "></i>
						<div class="clearfix"></div>
						<h2 class="star-h3-white purchase"><?php echo $this->lang->line('purchase'); ?> <br>
							<?php echo $this->lang->line('credits'); ?></h2>
					</button>
					<div class="clearfix"></div>
				</div>
				<div class="clearfix"></div>
			</div>
		<?php } ?>

		<div id="trancaction_outlet_list" class="trancaction_comp_list">
			<?php if(!empty($outlets)) { ?>
				<?php foreach($outlets as $outlet) { ?>
				<div id="company_credits_list">
					<div class="col-lg-10 col-md-10 col-xs-12 marb-22 ">
						<div class="light-gray-box">
							<h3 class="pull-left star-h3 col-md-5 col-xs-12 text-center"><?php echo (isset($outlet->outlet_name) && $outlet->outlet_name != "" ) ? $outlet->outlet_name : "N/A"; ?></h3>
							<div class="col-md-3 col-xs-12 text-center pull-right ava-credits">
								<h2><?php echo (isset($outlet->current_jod_credit)) ? $outlet->current_jod_credit : 0 ; ?> </h2>
								<p><?php echo $this->lang->line('available'); ?> <?php echo $this->lang->line('credits'); ?></p>
							</div>
							<div class="col-md-3 col-xs-12 text-center pull-right ava-credits">
								<h2><?php echo current_month_consumed_jod_credit($outlet->outlet_id); ?></h2>
								<p><?php echo $this->lang->line('consumed'); ?> <?php echo $this->lang->line('credits'); ?></p>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="clearfix"></div>
					</div>

					<div class="col-md-2 col-xs-12 marb-22 ">
						<a href="<?php echo base_url('admin/assign_credits_to_outlet/'.$outlet->outlet_id); ?>" class="assign_credit_but">
						<button class="yellow-cont yellow-back text-center">
							 <h3><?php echo $this->lang->line('assign_credits'); ?></h3>
						</button>
						</a>
					</div>
					<div class="clearfix"></div>
				</div>
				<?php } ?>

			<?php }else{ ?>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bmar">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 dark_organge org-boxs no-padding">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 dark_organge org-box  no-padding left-mar nob-mar job-box">
								<div class="col-lg-10 col-md-10 col-sm-10 col-xs-9 center-block no-float">
									<div class="over-scroll-left text-center over-scroll-left2">
										<h2><?php echo "No outlets Found."; ?></h2>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?php } ?>
		</div>
		<div class="clearfix"></div>
		<?php if($job_history == true){ ?>
		<div id="job_history_list">
				<h2 class="credits-history"><?php echo $this->lang->line('job_history'); ?><a class="download_button_credit_his" href="<?php echo base_url('admin/download_transaction_history/all_job_history/'.$company_details->company_id); ?>" onclick="loader()" ><i class="fa fa-download"></i></a></h2>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 overf">
				<table class="table table-th-gray table-striped" id="job_history">
					<thead class="firt-top">
						<tr>
							<th></th>
							<th><?php echo $this->lang->line('clock_in_date'); ?></th>
							<th><?php echo $this->lang->line('clock_out_date'); ?></th>
							<th><?php echo $this->lang->line('outlet_name'); ?></th>
							<!-- <th>Job Title</th> -->
							<th><?php echo $this->lang->line('full_name'); ?></th>
							<th><?php echo $this->lang->line('clock_in'); ?></th>
							<th><?php echo $this->lang->line('clock_out'); ?></th>
							<th><?php echo $this->lang->line('action'); ?></th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
				<div class="clearfix"></div>
			</div>
		</div>
		<?php } ?>
		<div class="clearfix"></div>
		<?php if(!empty($transaction_history)) { ?>
		<div id="company_transaction_history_div">
			<h2 class="credits-history"><?php echo $this->lang->line('credit_history'); ?><a class="download_button_credit_his" href="<?php echo base_url('admin/download_transaction_history/all_outlets/'.$company_details->company_id.'/'); ?>" onclick="loader()"><i class="fa fa-download"></i></a></h2>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 overf">
				<table class="table table-th-gray table-striped table-data" id="all_outlet_history">
					<thead class="firt-top">
						<tr>
							<th></th>
							<th><?php echo $this->lang->line('date'); ?></th>
							<th><?php echo $this->lang->line('outlet_name'); ?></th>
							<th><?php echo $this->lang->line('amount'); ?> +/-</th>
							<th width="437"><?php echo $this->lang->line('comments'); ?></th>
						</tr>
					</thead>
					<tbody>
					
					</tbody>
				</table>
				<div class="clearfix"></div>
			</div>
		</div>
		<?php } ?>
	</div>
</div>


 <div class="modal fade bs-example-modal-md" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="modal_popup">
 </div>

<div class="modal fade  bs-example-modal-md" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="purchase_jod_credit">
	<div class="cent">
		<div class="modal-dialog col-lg-5 center mode-w">
			<form role="form" id="purchase_jod_credit_form" method="post" data-parsley-validate onsubmit="$('#loader').fadeIn()">
				<div class="modal-content">
					<div class="gr-inp shadow-box left-pa" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" >
						<button data-dismiss="modal" class="close-b"><i class="fa fa-times-circle"></i></button>
						<h4 class="text-center basic-details org-box"><?php echo $this->lang->line('purchase_jod_credit'); ?></h4>
						<div class="form-group"><div class="col-lg-1 col-md-1 col-sm-1 hidden-xs"></div>
							<div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
								<input type="text" placeholder="<?php echo $this->lang->line('jod_credits'); ?>" class="form-control" id="jod_credit" name="jod_credit_amt" required >
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="form-group">
							<div class="col-lg-1 col-md-1 col-sm-1 hidden-xs"></div>
								<div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
								 <textarea placeholder="<?php echo $this->lang->line('enter_text'); ?>" class="form-control" name="email_comments" required></textarea>
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="col-lg-1 col-md-1 col-sm-1 hidden-xs"></div>
						<div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 m30">
							<button class="create red-box col-lg-5 col-md-5 col-sm-5 col-xs-12 pull-left" id="model-cancel" data-dismiss="modal" >
								<i class="fa fa-close"></i><span><?php echo $this->lang->line('cancel'); ?></span>
							</button>
							<button class="create green-box col-lg-5 col-md-5 col-sm-5 col-xs-12 pull-right" type="submit" id="save" name="save">
								<span><?php echo $this->lang->line('send'); ?> </span> <i class="fa fa-chevron-right"></i>
							</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<script>
	<?php if(isset($site_language) && $site_language == "chinese") { ?>
	var lang_url = "//cdn.datatables.net/plug-ins/1.10.12/i18n/Chinese.json";
	<?php }else{ ?>
		var lang_url = "//cdn.datatables.net/plug-ins/1.10.12/i18n/English.json";
	<?php }?>
	
	$(document).ready(function() {
			 $("td").tooltip();
	   setTimeout(function(){
		   $('#all_outlet_history_length').show();
		   $('#all_outlet_history_filter').show();
		   $('#all_outlet_history_paginate').show();
		   $('#job_history_length').show();
		   $('#job_history_filter').show();
		   $('#job_history_paginate').show();
		}, 100);


		$("#purchase_jod_credit_form").submit(function() {
			var base_url = '<?php echo base_url('admin/hq_jod_credits'); ?>';
			$('#loader').fadeIn();
			$.ajax({
				type: "POST",
				url: "<?php echo base_url() ?>admin/send_email_to_super_admin",
				data: $(this).serialize(),
				success: function( data, textStatus, jQxhr ){
					$('#loader').fadeOut();
					location.href = base_url;
				},
				error: function( jqXhr, textStatus, errorThrown ){
					$('#loader').fadeOut();
					alert( errorThrown );
				}
			});
			return false;
	});

	});
	$('#all_outlet_history').dataTable({
		"bPaginate": true,
		"ajax": '<?php echo base_url('admin/getOutletsTransactionHistoryByAjax'); ?>',
		"bLengthChange": true,
		"bFilter": true,
		"bSort": true,
		"bInfo": true,
		"bAutoWidth": false,
		"order": [[ 0, "desc" ]],
		"processing": true,
		"serverSide": true,
		"columnDefs": [ { "targets": 0, "bSortable": true,"orderable": true, "visible": false }],
		"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
		},
		"language": {
                "url": lang_url
            },
    });

	$('#job_history').dataTable({
			"bPaginate": true,
			"bLengthChange": true,
			"bFilter": true,
			"bSort": true,
			"bInfo": true,
			"bAutoWidth": false,
			"order": [[ 0, "desc" ]],
			"columnDefs": [ { "targets": 0, "bSortable": true,"orderable": true, "visible": false }],
			"processing": true,
			"serverSide": true,
			"ajax": '<?php echo base_url('admin/getJobHistoryListByAjax/hq_level'); ?>',
			"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
			},
			"aoColumns": [null,null,null,null,null,null,null,{ "bSortable": false }],
			"language": {
                "url": lang_url
            },

    });
	function loader(){
		$('#loader').fadeIn();
		$('#loader').delay(1500).fadeOut('slow');

	}

	function show_popup(job_id,applicant_id,outlet_id){
		$.ajax({
			type: "POST",
			url: "<?php echo base_url() ?>admin/get_data_jod_credit_job_history",
			data: {job_id : job_id,applicant_id : applicant_id,outlet_id : outlet_id},
			async: false,
			success: function( data, textStatus, jQxhr ){
				var obj_data = JSON.parse(data);
				var new_payment_data_str  = get_clock_in_out_details_adjusted_bySA(job_id,applicant_id,outlet_id);
				var new_payment_data = JSON.parse(new_payment_data_str);
				modal_pop(obj_data,new_payment_data);
			},
			error: function( jqXhr, textStatus, errorThrown ){
				$('#loader').fadeOut();
				alert( errorThrown );
			}
		});
	}
	
	function get_clock_in_out_details_adjusted_bySA(job_id,applicant_id,outlet_id){
		var data = false;
		$.ajax({
			type: "POST",
			url: "<?php echo base_url() ?>admin/get_new_clock_in_out_details",
			data: {job_id : job_id,applicant_id : applicant_id,outlet_id : outlet_id},
			async: false,
			success: function( sdata, textStatus, jQxhr ){
				data = sdata;
			},
			error: function( jqXhr, textStatus, errorThrown ){
				$('#loader').fadeOut();
				alert( errorThrown );
			}
		});
		return data;
	}

	function modal_pop(value_obj,new_payment_data){
			//alert(new_payment_data.data);
		var base_url = '<?php echo base_url('jobs/download_job_history'); ?>';
		var download_excel_url = base_url+"/"+value_obj.outlet_id+"/"+value_obj.job_id+"/"+value_obj.applicant_user_accounts_id;
		if(value_obj.meal_break_time == null || value_obj.meal_break_time === null){
			var meal_time = "N/A";
		}else{
			var meal_time =  value_obj.meal_break_time;
		}

		//console.log("In modal pop up ");
		$("#modal_popup").html(" ");
		var html = "";
			html += '<div class="modal-dialog col-lg-7 center mode-w mode-w2 ">';
				html +=	'<div class="modal-content back-white">';
					html +=	'<div class="col-md-12 col-xs-12 orange-back"><h3>'+'<?php echo $this->lang->line('job_history_details'); ?>'+'</h3></div>';
					html += '<div class="clearfix"></div>';
					html += '<div class="">';
						html += '<ul class="nav nav-tabs job-details"><li class="active" style="width:50%"><a data-toggle="tab" 	href="#div1">'+'<?php echo $this->lang->line('old_clockin_clockout_details'); ?>'+'</a></li><li style="width:50%"><a data-toggle="tab" href="#div2">'+'<?php echo $this->lang->line('new_clockin_clockout_details'); ?>'+ '</a></li></ul>';
					html += '</div>';
						html += '<div class="tab-content">';
							html += '<div id="div1" class="contant col-md-12  tab-pane fade in active">';
									html +=	'<table><tr><td width="50%;">';
									html += '<strong style="width:100%; float:left;">'+'<?php echo $this->lang->line('clock_in_date'); ?>'+': </strong >'+value_obj.date+'</td>';
									html += '<td width="50%;"><strong style="width:100%; float:left;">'+'<?php echo $this->lang->line('clock_out_date'); ?>'+':</strong >'+value_obj.clock_out_date+'</td>';
									html += '</tr>';
									html +=	'<tr><td colspan="2">';
									html += '<strong>'+'<?php echo $this->lang->line('outlet_name'); ?>'+': </strong>'+value_obj.outlet_name+'</td></tr>';
									html += '<tr><td colspan="2">';
									html += '<strong>'+'<?php echo $this->lang->line('full_name'); ?>'+': </strong>'+value_obj.applicant_fullname+'</td></tr>';
									html += '<tr><td colspan="2">';
									html += '<strong>NRIC/FIN: </strong>'+value_obj.unique_id+'</td></tr>';
									html += '<tr><td width="50%;"><strong style="width:100%; float:left;">'+'<?php echo $this->lang->line('clock_in_time'); ?>'+': </strong>'+value_obj.clock_in+'</td><td>';
									html += '<strong style="width:100%; float:left;">'+'<?php echo $this->lang->line('clock_out_time'); ?>'+': </strong>'+value_obj.clock_out+'</td></tr>';
									html += '<tr><td colspan="2"><strong>'+'<?php echo $this->lang->line('meal_break_time'); ?>'+': </strong>'+meal_time+'</td></tr>';
									html += '<tr><td><strong>'+'<?php echo $this->lang->line('total_hours_worked'); ?>'+': </strong>'+ value_obj.total_hours_worked+'</td>';
									html += '<td><strong>'+'<?php echo $this->lang->line('wages_per_hour'); ?>'+': </strong>$'+value_obj.wages_per_hour+'</td></tr>';
									html += '<tr><td colspan="2"><strong>'+'<?php echo $this->lang->line('total_jod_credits'); ?>'+': </strong>'+value_obj.total_jod_credit+'</td></tr>';
									html += '<tr><td width="50%;"><strong style="width:100%; float:left;">'+'<?php echo $this->lang->line('credit_before_deduction'); ?>'+': </strong >'+value_obj.credit_before_transacion+'</td>';
									html += '<td width="50%;"><strong style="width:100%; float:left;">'+'<?php echo $this->lang->line('credit_after_deduction'); ?>'+':</strong >'+value_obj.credit_after_transacion+'</td></tr>';
									html += '</table>';
							html += '</div>';	
								html += '<div id="div2" class="tab-pane fade contant col-md-12">';
										if(new_payment_data.status == false ){
											html +='<div class="tab-content message_content_popup">';
												html +='<div class="contant col-md-12 col-mar message-pad" id="message_content"> '+'<?php echo $this->lang->line('no_adjustment');?>'+'</div>';
											html +='</div>';
										}else{
											if(value_obj.old_applicant_clock_in_date_time != new_payment_data.data.payment_data.clock_in_datetime ||value_obj.old_applicant_clock_out_date_time  !=  new_payment_data.data.payment_data.clock_out_datetime ||  parseFloat(value_obj.old_payment_amount_per_hour) !=  parseFloat(new_payment_data.data.payment_data.wages_per_hour) || value_obj.old_meal_hours != new_payment_data.data.payment_data.meal_break_time ) {
												html +=	'<table>';
													html += '<tr><td width="50%;">';
															html += '<strong style="width:100%; float:left;">'+'<?php echo $this->lang->line('clock_in_date'); ?>'+': </strong >'+new_payment_data.data.payment_data.clock_in_date+'</td>';
															html += '<td width="50%;"><strong style="width:100%; float:left;">'+'<?php echo $this->lang->line('clock_out_date'); ?>'+':</strong >'+new_payment_data.data.payment_data.clock_out_date+'</td>';
													html += '</tr>';
														html += '<tr><td width="50%;">';
															html += '<strong style="width:100%; float:left;">'+'<?php echo $this->lang->line('clock_in_time'); ?>'+': </strong >'+new_payment_data.data.payment_data.clock_in_time+'</td>';
															html += '<td width="50%;"><strong style="width:100%; float:left;">'+'<?php echo $this->lang->line('clock_out_time'); ?>'+':</strong >'+new_payment_data.data.payment_data.clock_out_time+'</td>';
													html += '</tr>';
													html += '<tr><td colspan="2"><strong>'+'<?php echo $this->lang->line('meal_break_time'); ?>'+': </strong>'+new_payment_data.data.payment_data.meal_break_time+'</td></tr>';
													html += '<tr><td><strong>'+'<?php echo $this->lang->line('total_hours_worked'); ?>'+': </strong>'+ new_payment_data.data.payment_data.total_hours_worked+'</td>';
													html += '<td><strong>'+'<?php echo $this->lang->line('wages_per_hour'); ?>'+': </strong>$'+new_payment_data.data.payment_data.wages_per_hour+'</td></tr>';
													html += '<tr><td><strong>'+'<?php echo $this->lang->line('total_jod_credits'); ?>'+': </strong>'+parseFloat(new_payment_data.data.payment_data.total_jod_credit).toFixed(2)+'</td>';
													html += '<td><strong>'+'<?php echo $this->lang->line('adjusted_credit'); ?>'+': </strong>'+parseFloat(new_payment_data.data.payment_data.jod_credit_difference).toFixed(2)+'</td></tr>';
													var before = new_payment_data.data.transaction_data.credit_before_transacion;
													html += '<tr><td width="50%;"><strong style="width:100%; float:left;">'+'<?php echo $this->lang->line('credit_before_deduction'); ?>'+': </strong >'+parseFloat(before).toFixed(2)+'</td>';
													var after = new_payment_data.data.transaction_data.credit_after_transacion;
													html += '<td width="50%;"><strong style="width:100%; float:left;">'+'<?php echo $this->lang->line('credit_after_deduction'); ?>'+':</strong >'+parseFloat(after).toFixed(2)+'</td></tr>';
													html += '<tr><td colspan="2"><strong>'+'<?php echo $this->lang->line('adjusted_on'); ?>'+': </strong>'+new_payment_data.data.payment_data.created_at+'</td></tr>';
												html +=	'</table>';
										}else{
											
											html +='<div class="tab-content message_content_popup">';
												html +='<div class="contant col-md-12 col-mar message-pad" id="message_content">'+'<?php echo $this->lang->line('completed_by_jod_admin');?>'+'</div>';
											html +='</div>';
										}
									}
								html += '</div>';
							html +='</div>';
					
					html += '<div class="clerfix"></div>';
					html += '<div class="fotter">';
						html +=	'<a class="col-md-6 col-xs-12 text-center yellow-back ad-man" href="'+download_excel_url+'" >';
								html += '<?php echo $this->lang->line('download_excel'); ?>';
						html += '</a>';
						html += '<button class="col-md-6 col-xs-12 text-center red-box ad-man" data-dismiss="modal">';
							html += '<?php echo $this->lang->line('close'); ?>';
						html +=	'</button>';
					html += '</div>';
					html += '<div class="clerfix"></div>';
				html +='</div>';
			html += '</div>';
			$("#modal_popup").html(html);
			$("#modal_popup").modal('show');
	}

</script>
