<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$userType=LoginUserDetails('role_id');
$message = getGlobalMessage();
//print_r($records); die;
?>
<div class="right_col right_col" role="main">
	<?php
	if($message['type']=='success') {
	?>
	<div class="Metronic-alerts alert alert-success fade in" id="prefix_167847684379"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button><?php echo $message['msg'] ?></div>
	<?php
	} else if($message['type']=='error') {
	?>
	<div class="Metronic-alerts alert alert-danger fade in" id="prefix_167847684379"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button><?php echo $message['msg'] ?></div>
	<?php
	}
	?>
	<div class="row padding-top-one">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center title-head2">
			<h3>
			<?php if($id){
					echo $this->lang->line('update_applicant');
					$action = $this->lang->line('update');
				}else {
					echo $this->lang->line('add_applicant');
					$action = $this->lang->line('create');
				}
			?>
				</h3>
		</div>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

			<form action="<?php echo base_url('admin/applicant_form/'.$id); ?>" id="form_sample_3" class="form-horizontal" enctype="multipart/form-data" method='post' data-parsley-validate onsubmit="$('#loader').fadeIn()">
			<form role="form">
				<div class="light-org-box pad-bottoms">
				<h4 class="text-center basic-details"><?php echo $this->lang->line('applicant_details');?></h4>

					<div class="form-group">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"><label for="usr1"><?php echo $this->lang->line('first_name');?> *</label></div>
						<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
							<input type="text" name="first_name" data-required="1" class="form-control" value="<?php echo set_value('first_name', isset($records->first_name) ? $records->first_name : ''); ?>" required data-parsley-maxlength="100" />
							<span id="first_name-error" class="help-block help-block-error"><?php echo form_error('first_name'); ?></span>
						</div>
					</div>
					<div class="form-group">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"><label for="usr1"><?php echo $this->lang->line('unique_id');?> *</label></div>
						<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
							<input type="text" name="unique_id" data-required="1" class="form-control" value="<?php echo set_value('unique_id', isset($records->unique_id) ? $records->unique_id : ''); ?>" required data-parsley-maxlength="100" />
							<span id="unique_id-error" class="help-block help-block-error"><?php echo form_error('unique_id'); ?></span>
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="form-group">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"><label for="last-name"><?php echo $this->lang->line('last_name');?> *</label></div>
						<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
							<input type="text" name="last_name" data-required="1" class="form-control" value="<?php echo set_value('last_name', isset($records->last_name) ? $records->last_name : ''); ?>" required  data-parsley-maxlength="100" />
							<span id="last_name-error" class="help-block help-block-error"><?php echo form_error('last_name'); ?></span>
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="form-group">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"><label for="j-title"><?php echo $this->lang->line('display_name');?> *</label></div>
						<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
							<input type="text" name="display_name" data-required="1" class="form-control" value="<?php echo set_value('display_name', isset($records->display_name) ? $records->display_name : ''); ?>" required data-parsley-maxlength="100" />
							<span id="display_name-error" class="help-block help-block-error"><?php echo form_error('display_name'); ?></span>
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="form-group">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"><label for="reg"><?php echo $this->lang->line('contact_no'); ?> *</label></div>
						<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
							<div class="input-group">
								<span class="input-group-addon">
										+65
								</span>
								<input type="text" id="contact_no" name="contact_no" data-required="1" class="form-control" value="<?php echo set_value('contact_no', isset($records->contact_no) ? str_ireplace('+65','',$records->contact_no) : ''); ?>" required data-parsley-type="number" data-parsley-maxlength="8" data-parsley-minlength="8" data-parsley-errors-container="#contact_no-error" />
							</div>
							<span id="contact_no-error" class="help-block help-block-error"><?php echo form_error('contact_no'); ?></span>
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="form-group">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"><label for="email"><?php echo $this->lang->line('work_email'); ?> *</label></div>
						<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
							<input type="email" name="email_id" id="email_id" onblur="checkUsername()" data-required="1" class="form-control" value="<?php echo set_value('email_id', isset($records->email_id) ? $records->email_id : ''); ?>" required data-parsley-maxlength="100" />
							<span id="email_id-error" class="help-block help-block-error"><?php echo form_error('email_id'); ?></span>
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="form-group">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"><label for="date_of_birth"><?php echo $this->lang->line('date_of_birth'); ?> *</label></div>
						<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
							<div class="form-group input-append date" id="datepicker1">
								<input type="text"  placeholder="Date Of Birth" class="form-control" id="date_of_birth" name="date_of_birth" value="<?php echo set_value('date_of_birth', isset($records->date_of_birth) ? $records->date_of_birth : ''); ?>" data-parsley-errors-container="#date_of_birth-error" style="width:100%" readonly	 required>
								<span class="add-on"> <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="fa fa-calendar"></i> </span>
								<span id="date_of_birth-error" class="help-block help-block-error"><?php echo form_error('date_of_birth'); ?></span>
							</div>
						</div>
					</div>

					<div class="clearfix"></div>
					<div class="form-group">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"><label for="school_attend"><?php echo $this->lang->line('school_attend'); ?> *</label></div>
						<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
							<select class="form-control chosen-select" name="school_attend" id="school_attend" required data-parsley-errors-container="#school_attend-error">
								<option value="" >--Select School Attend--</option>
								<?php foreach($schoolAttends as $schoolAttend) { ?>
								<option value="<?php echo $schoolAttend->education_institutes_id; ?>" <?php if(set_value('school_attend', isset($records->school_attend) ? $records->school_attend : '') == $schoolAttend->education_institutes_id){ echo 'selected="selected"'; } ?> ><?php echo $schoolAttend->name;?></option>
								<?php } ?>
							</select>
							<span id="school_attend-error" class="help-block help-block-error"><?php echo form_error('school_attend'); ?></span>
						</div>
					</div>

					<div class="clearfix"></div>
					<div class="form-group">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"><label for="region_number"><?php echo $this->lang->line('region_number'); ?> *</label></div>
						<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
							<input type="text" name="region_number" data-required="1" class="form-control" value="<?php echo set_value('region_number', isset($records->region_number) ? $records->region_number : ''); ?>" required data-parsley-maxlength="100" data-parsley-type="number" />
							<span id="region_number-error" class="help-block help-block-error"><?php echo form_error('region_number'); ?></span>
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="form-group">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
							<label class=" line-h" for="is_NEA_certified"><?php echo $this->lang->line('is_nea_certified'); ?> </label>
						</div>
						<div class="col-lg-1">
							<div class="check-box">
								<div class="check-wrapper">
									<input type="checkbox" name="is_NEA_certified" id="check1" <?php if(isset($records->is_NEA_certified) && $records->is_NEA_certified == 1) { echo "checked"; } ?> >
									<label for="check1"> </label>
								</div>
							</div>
							<span id="is_NEA_certified-error" class="help-block help-block-error"><?php echo form_error('is_NEA_certified'); ?></span>
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="form-group">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"><label for="referral_code"><?php echo $this->lang->line('referral_code'); ?> </label></div>
						<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
							<input type="text" name="referral_code" data-required="1" class="form-control" value="<?php echo set_value('referral_code', isset($records->referral_code) ? $records->referral_code : ''); ?>" />
							<span id="referral_code-error" class="help-block help-block-error"><?php echo form_error('referral_code'); ?></span>
						</div>
					</div>
					<div class="clearfix"></div>
						<input type='hidden' name="user_account_id" id="user_account_id" value="<?php echo set_value('user_account_id', isset($records->user_account_id) ? $records->user_account_id : '0'); ?>">
				</div>

				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 m-t80 no-padding">    
					<button class="create org-box col-lg-3 col-md-3 col-xs-12 pull-right" type="submit" name='save' ><span><?php echo $action; ?></span>
						<i class="fa fa-chevron-right"></i>
					</button>
				</div>
			</form>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
</div>
<script type="text/javascript">
	<?php if(isset($site_language) && $site_language ==  "chinese"){?>
		window.Parsley.setLocale('zh-cn');
	<?php }else{ ?>
		window.Parsley.setLocale('en');
	<?php }?>
  $(".chosen-select").chosen();
    var config = {
      '.chosen-select'           : {},
      '.chosen-select-deselect'  : {allow_single_deselect:true},
      '.chosen-select-no-single' : {disable_search_threshold:10},
      '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
      '.chosen-select-width'     : {width:"95%"}
    }
    for (var selector in config) {
      $(".chosen-select").chosen(config[selector]);
    }

$('.collapse').on('shown.bs.collapse', function(){
$(this).parent().find(".glyphicon-plus").removeClass("glyphicon-plus").addClass("glyphicon-minus");
}).on('hidden.bs.collapse', function(){
$(this).parent().find(".glyphicon-minus").removeClass("glyphicon-minus").addClass("glyphicon-plus");
});


function checkUsername() {
	var emailId=$("#email_id").val();
	var userAccountId=$("#user_account_id").val();
	var emailId = emailId.replace(' ','');
	if(emailId != '') {
			$.post(BASEURL+"admin/get_unique_id/",{emailId:emailId,userAccountId:userAccountId},function(data){
				if(data >= 1){
				$("#email_id-error").html("Email Id Already in Use");
					$('#email_id').val("");
				}
				else{
				$( "#email_id-error" ).empty();
				}
			});
	}
}
$( document ).ready(function() {

	var nowTemp = new Date();
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
    $('#date_of_birth').datepicker({
		format: 'yyyy-mm-dd',
		clearBtn : true,
		autoclose: true,
		 onRender: function(date) {
        return date.valueOf() > now.valueOf() ? 'disabled' : '';
      }
    }).on('changeDate', function(ev) {
		var date_of_birth = $("#date_of_birth").val();
		var dobDate = new Date(date_of_birth);
		var today = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate());
		if(dobDate != '' && today < dobDate) {
			//$("#date_of_birth").val("");
			//$('#date_of_birth').datepicker('clearDates', '');
			alert("Date of birth should not be greater than Today's date");
					 $('#date_of_birth').html('');
			//	$('#date_of_birth').datepicker('update','');

		}
		$(this).datepicker('hide');
    });
});
</script>
