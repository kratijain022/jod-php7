<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php

	$userType=LoginUserDetails('role_id');
	$message = getGlobalMessage();
?>
<div class="right_col right_col" role="main">
	<?php
	if($message['type']=='success') {
	?>
	<div class="Metronic-alerts alert alert-success fade in" id="prefix_167847684379"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button><?php echo $message['msg'] ?></div>
	<?php
	} else if($message['type']=='error') {
	?>
	<div class="Metronic-alerts alert alert-danger fade in" id="prefix_167847684379"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button><?php echo $message['msg'] ?></div>
	<?php
	}
	?>
	<div class="row padding-top-one">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<ul class="breadcrumb breadcrumb2">
			<li><a href="<?php echo base_url(); ?>"><?php echo $this->lang->line('home'); ?></a></li>
			<li class="active"> <?php echo $this->lang->line('jod_credits'); ?></li>
			</ul>
		</div>
		<div id="trancaction_outlet_list" class="">
			<?php if(!empty($outlets)) { ?>
				<?php foreach($outlets as $outlet) { ?>
					<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 marb-22">
						<div class="light-gray-box">
							<h3 class="pull-left star-h3 col-md-5 col-xs-12 text-center"><?php echo (isset($outlet->outlet_name) && $outlet->outlet_name != "" ) ? $outlet->outlet_name : "N/A"; ?></h3>
							<div class="col-md-3 col-xs-12 text-center pull-right ava-credits">
								<h2><?php echo (isset($outlet->current_jod_credit)) ? $outlet->current_jod_credit : 0 ; ?></h2>
								<p><?php echo $this->lang->line('available_credit'); ?></p>
							</div>
							<div class="col-md-3 col-xs-12 text-center pull-right ava-credits">
								<h2><?php echo current_month_consumed_jod_credit($outlet->outlet_id); ?></h2>
								<p><?php echo $this->lang->line('consumed'); ?>   <?php echo $this->lang->line('credits'); ?> </p>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="col-md-2 col-xs-12 marb-22 ">
						<a href="<?php echo base_url('admin/outlet_jod_credit/'.$outlet->outlet_id); ?>" class="assign_credit_but">
							<button class="yellow-cont yellow-back text-center">
								<h3><?php echo $this->lang->line('view_details'); ?></h3>
							</button>
						</a>
					</div>
					<div class="clearfix"></div>
				<?php } ?>
			<?php }else { ?>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bmar">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 dark_organge org-boxs no-padding">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 dark_organge org-box  no-padding left-mar nob-mar job-box">
									<div class="col-lg-10 col-md-10 col-sm-10 col-xs-9 center-block no-float">
										<div class="over-scroll-left text-center over-scroll-left2">
											<h2><?php echo "No outlets Found."; ?></h2>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
			<?php } ?>
		</div>
		<div class="clearfix"></div>
		<?php if(!empty($job_history)){ ?>
		<div id="job_history_list">
				<h2 class="credits-history"><?php echo $this->lang->line('job_history'); ?><a class="download_button_credit_his" href="<?php echo base_url('admin/download_transaction_history/area_manager_outlets'); ?>" onclick="loader()" ><i class="fa fa-download"></i></a></h2>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 overf">
				<table class="table table-th-gray table-striped" id="job_history">
					<thead class="firt-top">
						<tr>
							<th></th>
							<th><?php echo $this->lang->line('clock_in_date'); ?></th>
							<th><?php echo $this->lang->line('clock_out_date'); ?></th>
							<th><?php echo $this->lang->line('outlet_name'); ?></th>
							<!-- <th>Job Title</th> -->
							<th><?php echo $this->lang->line('full_name'); ?></th>
							<th><?php echo $this->lang->line('clock_in'); ?></th>
							<th><?php echo $this->lang->line('clock_out'); ?></th>
							<th><?php echo $this->lang->line('actions'); ?></th>
						</tr>
					</thead>
					<tbody>
						<?php /* foreach($job_history as $key=>$history) { ?>
						<tr >
							<td><?php echo $history["date"]; ?></td>
							<td><?php echo $history["clock_out_date"]; ?></td>
							<td><?php echo $history["outlet_name"]; ?></td>
							<!-- <td><?php echo $history["job_title"]; ?></td> --->
							<td><?php echo $history["applicant_fullname"]; ?></td>
							<td><?php echo $history["clock_in"]; ?></td>
							<td><?php echo $history["clock_out"]; ?></td>
							<td>
								<a class="pull-left text-center action-icon ad-man" onclick="javascript:show_popup(<?php echo $key; ?>)">
									<i class="fa fa-search"></i>
									<div class="clerfix"></div>
									<lable>view</lable>
								</a>
								<a class="pull-left text-center action-icon ad-man" href="<?php echo base_url('jobs/download_job_history/'.$history['outlet_id']."/".$history['job_id']."/".$history['applicant_user_accounts_id']); ?>">
									<i class="fa fa-download"></i>
									<div class="clerfix"></div>
									<lable>excel</lable>
								</a>
							</td>
						</tr>
						<?php } */?>
					</tbody>
				</table>
				<div class="clearfix"></div>
			</div>
		</div>
		<?php } ?>
		<?php if(!empty($transaction_history)) {  ?>
			<div id="company_transaction_history_div">
				<h2 class="credits-history"><?php echo $this->lang->line('credit_history'); ?><a class="download_button_credit_his" href="<?php echo base_url('admin/download_transaction_history/specific_outlet_transactions'); ?>" onclick="loader()"><i class="fa fa-download"></i></a></h2>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 overf">
					<table class="table table-th-gray table-striped table-data" id="all_companies_history">
						<thead class="firt-top">
							<tr>
								<th></th>
								<th><?php echo $this->lang->line('date'); ?></th>
								<th><?php echo $this->lang->line('outlet_name'); ?></th>
								<th><?php echo $this->lang->line('amount'); ?> +/-</th>
								<th width="437"><?php echo $this->lang->line('comments'); ?></th>
							</tr>
						</thead>
						<tbody>
							<?php /* foreach($transaction_history as $history_row) { ?>
							<tr class="">
								<td><?php echo (isset($history_row->assigned_at) && $history_row->assigned_at!= "") ? date('d M Y h:i A', strtotime($history_row->assigned_at)) : " " ;?></td>
								<td><?php echo (isset($history_row->outlet_name)) ? $history_row->outlet_name : "" ; ?></td>
								<td><?php echo (isset($history_row->transaction_type) && $history_row->transaction_type == 2) ? "+".  $history_row-> jod_credit_amount : "-".  $history_row-> jod_credit_amount; ?></td>
								<td><p class="jod-tooltip" data-toggle="tooltip" data-placement="bottom" title="<?php echo ucfirst($history_row->comment_text); ?>"   ><?php echo strlen($history_row->comment_text) > 150 ? ucfirst(substr($history_row->comment_text,0,150)).'...' : ucfirst($history_row->comment_text) ;  ?></p></td>
							</tr>
							<?php  }  */?>
						</tbody>
					</table>
					<div class="clearfix"></div>
				</div>
			</div>
			<?php } ?>

		<div class="clearfix"></div>
	</div>
</div>

 <div class="modal fade bs-example-modal-md" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="modal_popup">
 </div>
<script>
	<?php if(isset($site_language) && $site_language == "chinese") { ?>
		var lang_url = "//cdn.datatables.net/plug-ins/1.10.12/i18n/Chinese.json";
	<?php }else{ ?>
		var lang_url = "//cdn.datatables.net/plug-ins/1.10.12/i18n/English.json";
	<?php }?>
	$(document).ready(function() {
	//	alert("getOutletsTransactionHistoryByAjaxgetOutletsTransactionHistoryByAjaxgetOutletsTransactionHistoryByAjax");
		  $("td").tooltip();
	   setTimeout(function(){
			$('#all_companies_history_length').show();
			$('#all_companies_history_filter').show();
			$('#all_companies_history_paginate').show();
			$('#job_history_length').show();
			$('#job_history_filter').show();
			$('#job_history_paginate').show();
		}, 100);
	});

	$('#job_history').dataTable({
		"bPaginate": true,
		"bDeferRender": true,
		"deferRender" : true,
		"bLengthChange": true,
		"bFilter": true,
		"bSort": true,
		"bInfo": true,
		"bDestroy" : true,
		"bAutoWidth": false,
		"order": [[ 0, "desc" ]],
		"columnDefs": [ { "targets": 0, "bSortable": true,"orderable": true, "visible": false }],
		"processing": true,
		"serverSide": true,
		"ajax": '<?php echo base_url('admin/getJobHistoryListByAjax/area_manager_outlet_level'); ?>',
		"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
		},
		"aoColumns": [null,null,null,null,null,null,null,{ "bSortable": false }],
		"language": {
                "url": lang_url
            },
    });

	$('#all_companies_history').dataTable({
		"bPaginate": true,
		"ajax": '<?php echo base_url('admin/getOutletsTransactionHistoryByAjax/area_manager_level'); ?>',
		"bLengthChange": true,
		"bFilter": true,
		"bSort": true,
		"bInfo": true,
		"bAutoWidth": false,
		"order": [[ 0, "desc" ]],
		"processing": true,
		"serverSide": true,
		"columnDefs": [ { "targets": 0, "bSortable": true,"orderable": true, "visible": false }],
		"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
		},
		"deferRender": true,
		"language": {
                "url": lang_url
            },
    });


function loader(){
		$('#loader').fadeIn();
		$('#loader').delay(1500).fadeOut('slow');

	}

	function show_popup(job_id,applicant_id,outlet_id){
		$.ajax({
			type: "POST",
			url: "<?php echo base_url() ?>admin/get_data_jod_credit_job_history",
			data: {job_id : job_id,applicant_id : applicant_id,outlet_id : outlet_id},
			async: false,
			success: function( data, textStatus, jQxhr ){
				var obj_data = JSON.parse(data);
				var new_payment_data_str  = get_clock_in_out_details_adjusted_bySA(job_id,applicant_id,outlet_id);
				var new_payment_data = JSON.parse(new_payment_data_str);
				modal_pop(obj_data,new_payment_data);
			},
			error: function( jqXhr, textStatus, errorThrown ){
				$('#loader').fadeOut();
				alert( errorThrown );
			}
		});
	}
	
	function get_clock_in_out_details_adjusted_bySA(job_id,applicant_id,outlet_id){
		var data = false;
		$.ajax({
			type: "POST",
			url: "<?php echo base_url() ?>admin/get_new_clock_in_out_details",
			data: {job_id : job_id,applicant_id : applicant_id,outlet_id : outlet_id},
			async: false,
			success: function( sdata, textStatus, jQxhr ){
				data = sdata;
			},
			error: function( jqXhr, textStatus, errorThrown ){
				$('#loader').fadeOut();
				alert( errorThrown );
			}
		});
		return data;
	}
	

	function modal_pop(value_obj,new_payment_data){
			//alert(new_payment_data.data);
		var base_url = '<?php echo base_url('jobs/download_job_history'); ?>';
		var download_excel_url = base_url+"/"+value_obj.outlet_id+"/"+value_obj.job_id+"/"+value_obj.applicant_user_accounts_id;
		if(value_obj.meal_break_time == null || value_obj.meal_break_time === null){
			var meal_time = "N/A";
		}else{
			var meal_time =  value_obj.meal_break_time;
		}

		//console.log("In modal pop up ");
		$("#modal_popup").html(" ");
		var html = "";
			html += '<div class="modal-dialog col-lg-7 center mode-w mode-w2 ">';
				html +=	'<div class="modal-content back-white">';
					html +=	'<div class="col-md-12 col-xs-12 orange-back"><h3>'+'<?php echo $this->lang->line('job_history_details'); ?>'+'</h3></div>';
					html += '<div class="clearfix"></div>';
					html += '<div class="">';
						html += '<ul class="nav nav-tabs job-details"><li class="active" style="width:50%"><a data-toggle="tab" 	href="#div1">'+'<?php echo $this->lang->line('old_clockin_clockout_details'); ?>'+'</a></li><li style="width:50%"><a data-toggle="tab" href="#div2">'+'<?php echo $this->lang->line('new_clockin_clockout_details'); ?>'+ '</a></li></ul>';
					html += '</div>';
						html += '<div class="tab-content">';
							html += '<div id="div1" class="contant col-md-12  tab-pane fade in active">';
									html +=	'<table><tr><td width="50%;">';
									html += '<strong style="width:100%; float:left;">'+'<?php echo $this->lang->line('clock_in_date'); ?>'+': </strong >'+value_obj.date+'</td>';
									html += '<td width="50%;"><strong style="width:100%; float:left;">'+'<?php echo $this->lang->line('clock_out_date'); ?>'+':</strong >'+value_obj.clock_out_date+'</td>';
									html += '</tr>';
									html +=	'<tr><td colspan="2">';
									html += '<strong>'+'<?php echo $this->lang->line('outlet_name'); ?>'+': </strong>'+value_obj.outlet_name+'</td></tr>';
									html += '<tr><td colspan="2">';
									html += '<strong>'+'<?php echo $this->lang->line('full_name'); ?>'+': </strong>'+value_obj.applicant_fullname+'</td></tr>';
									html += '<tr><td colspan="2">';
									html += '<strong>NRIC/FIN: </strong>'+value_obj.unique_id+'</td></tr>';
									html += '<tr><td width="50%;"><strong style="width:100%; float:left;">'+'<?php echo $this->lang->line('clock_in_time'); ?>'+': </strong>'+value_obj.clock_in+'</td><td>';
									html += '<strong style="width:100%; float:left;">'+'<?php echo $this->lang->line('clock_out_time'); ?>'+': </strong>'+value_obj.clock_out+'</td></tr>';
									html += '<tr><td colspan="2"><strong>'+'<?php echo $this->lang->line('meal_break_time'); ?>'+': </strong>'+meal_time+'</td></tr>';
									html += '<tr><td><strong>'+'<?php echo $this->lang->line('total_hours_worked'); ?>'+': </strong>'+ value_obj.total_hours_worked+'</td>';
									html += '<td><strong>'+'<?php echo $this->lang->line('wages_per_hour'); ?>'+': </strong>$'+value_obj.wages_per_hour+'</td></tr>';
									html += '<tr><td colspan="2"><strong>'+'<?php echo $this->lang->line('total_jod_credits'); ?>'+': </strong>'+value_obj.total_jod_credit+'</td></tr>';
									html += '<tr><td width="50%;"><strong style="width:100%; float:left;">'+'<?php echo $this->lang->line('credit_before_deduction'); ?>'+': </strong >'+value_obj.credit_before_transacion+'</td>';
									html += '<td width="50%;"><strong style="width:100%; float:left;">'+'<?php echo $this->lang->line('credit_after_deduction'); ?>'+':</strong >'+value_obj.credit_after_transacion+'</td></tr>';
									html += '</table>';
							html += '</div>';	
								html += '<div id="div2" class="tab-pane fade contant col-md-12">';
										if(new_payment_data.status == false ){
											html +='<div class="tab-content message_content_popup">';
												html +='<div class="contant col-md-12 col-mar message-pad" id="message_content"> '+'<?php echo $this->lang->line('no_adjustment');?>'+'</div>';
											html +='</div>';
										}else{
											if(value_obj.old_applicant_clock_in_date_time != new_payment_data.data.payment_data.clock_in_datetime ||value_obj.old_applicant_clock_out_date_time  !=  new_payment_data.data.payment_data.clock_out_datetime ||  parseFloat(value_obj.old_payment_amount_per_hour) !=  parseFloat(new_payment_data.data.payment_data.wages_per_hour) || value_obj.old_meal_hours != new_payment_data.data.payment_data.meal_break_time ) {
												html +=	'<table>';
													html += '<tr><td width="50%;">';
															html += '<strong style="width:100%; float:left;">'+'<?php echo $this->lang->line('clock_in_date'); ?>'+': </strong >'+new_payment_data.data.payment_data.clock_in_date+'</td>';
															html += '<td width="50%;"><strong style="width:100%; float:left;">'+'<?php echo $this->lang->line('clock_out_date'); ?>'+':</strong >'+new_payment_data.data.payment_data.clock_out_date+'</td>';
													html += '</tr>';
														html += '<tr><td width="50%;">';
															html += '<strong style="width:100%; float:left;">'+'<?php echo $this->lang->line('clock_in_time'); ?>'+': </strong >'+new_payment_data.data.payment_data.clock_in_time+'</td>';
															html += '<td width="50%;"><strong style="width:100%; float:left;">'+'<?php echo $this->lang->line('clock_out_time'); ?>'+':</strong >'+new_payment_data.data.payment_data.clock_out_time+'</td>';
													html += '</tr>';
													html += '<tr><td colspan="2"><strong>'+'<?php echo $this->lang->line('meal_break_time'); ?>'+': </strong>'+new_payment_data.data.payment_data.meal_break_time+'</td></tr>';
													html += '<tr><td><strong>'+'<?php echo $this->lang->line('total_hours_worked'); ?>'+': </strong>'+ new_payment_data.data.payment_data.total_hours_worked+'</td>';
													html += '<td><strong>'+'<?php echo $this->lang->line('wages_per_hour'); ?>'+': </strong>$'+new_payment_data.data.payment_data.wages_per_hour+'</td></tr>';
													html += '<tr><td><strong>'+'<?php echo $this->lang->line('total_jod_credits'); ?>'+': </strong>'+parseFloat(new_payment_data.data.payment_data.total_jod_credit).toFixed(2)+'</td>';
													html += '<td><strong>'+'<?php echo $this->lang->line('adjusted_credit'); ?>'+': </strong>'+parseFloat(new_payment_data.data.payment_data.jod_credit_difference).toFixed(2)+'</td></tr>';
													var before = new_payment_data.data.transaction_data.credit_before_transacion;
													html += '<tr><td width="50%;"><strong style="width:100%; float:left;">'+'<?php echo $this->lang->line('credit_before_deduction'); ?>'+': </strong >'+parseFloat(before).toFixed(2)+'</td>';
													var after = new_payment_data.data.transaction_data.credit_after_transacion;
													html += '<td width="50%;"><strong style="width:100%; float:left;">'+'<?php echo $this->lang->line('credit_after_deduction'); ?>'+':</strong >'+parseFloat(after).toFixed(2)+'</td></tr>';
													html += '<tr><td colspan="2"><strong>'+'<?php echo $this->lang->line('adjusted_on'); ?>'+': </strong>'+new_payment_data.data.payment_data.created_at+'</td></tr>';
												html +=	'</table>';
											}else{
												
												html +='<div class="tab-content message_content_popup">';
													html +='<div class="contant col-md-12 col-mar message-pad" id="message_content">'+'<?php echo $this->lang->line('completed_by_jod_admin');?>'+'</div>';
												html +='</div>';
											}
										}
								html += '</div>';
							html +='</div>';
					
					html += '<div class="clerfix"></div>';
					html += '<div class="fotter">';
						html +=	'<a class="col-md-6 col-xs-12 text-center yellow-back ad-man" href="'+download_excel_url+'" >';
								html += '<?php echo $this->lang->line('download_excel'); ?>';
						html += '</a>';
						html += '<button class="col-md-6 col-xs-12 text-center red-box ad-man" data-dismiss="modal">';
							html += '<?php echo $this->lang->line('close'); ?>';
						html +=	'</button>';
					html += '</div>';
					html += '<div class="clerfix"></div>';
				html +='</div>';
			html += '</div>';
			$("#modal_popup").html(html);
			$("#modal_popup").modal('show');
	}
</script>
