<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$message = getGlobalMessage();

	$userType=LoginUserDetails('role_id');
?>
<div class="right_col right_col" role="main">
	<?php
	if($message['type']=='success') {
	?>
	<div class="Metronic-alerts alert alert-success fade in" id="prefix_167847684379"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button><?php echo $message['msg'] ?></div>
	<?php
	} else if($message['type']=='error') {
	?>
	<div class="Metronic-alerts alert alert-danger fade in" id="prefix_167847684379"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button><?php echo $message['msg'] ?></div>
	<?php
	}
	?>
	<div class="row padding-top-one">

		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<ul class="breadcrumb breadcrumb2">
			<li><a href="<?php echo base_url(); ?>"><?php echo $this->lang->line('home'); ?></a></li>
			<?php if($id) { ?>
				<li class="active"><?php echo $this->lang->line('update'); ?> <?php echo $this->lang->line('area_manager'); ?></li>
			<?php } else { ?>
				<li class="active"><?php echo $this->lang->line('add'); ?> <?php echo $this->lang->line('area_manager'); ?></li>
			<?php } ?>
			</ul>
		</div>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center title-head2">
			<h3>
			<?php if($id) {
					//echo 'Edit Area Manager';
					echo $this->lang->line('edit').$this->lang->line('area_manager'); 
					$action = $this->lang->line('update');
				} else {
					echo $this->lang->line('add_new_area_manager');
					$action = $this->lang->line('create');
				}
			?>
				</h3>
		</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

			<form action="<?php echo base_url('admin/area_manager_form/'.$id); ?>" id="form_sample_3" class="form-horizontal" enctype="multipart/form-data" method='post' data-parsley-validate onsubmit="$('#loader').fadeIn()" >
			<form role="form">
				<div class="light-org-box pad-bottoms">
				<h4 class="text-center basic-details"><?php echo $this->lang->line('area_manager_details'); ?></h4>
			<div class="form-group">
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"><label for="usr1"><?php echo $this->lang->line('first_name'); ?> *</label></div>
				<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
					<input type="text" name="first_name" data-required="1" class="form-control" value="<?php echo set_value('first_name', isset($records->first_name) ? $records->first_name : ''); ?>" required data-parsley-maxlength="100" />
					<span id="first_name-error" class="help-block help-block-error"><?php echo form_error('first_name'); ?></span>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="form-group">
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"><label for="last-name"><?php echo $this->lang->line('last_name'); ?> *</label></div>
				<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
					<input type="text" name="last_name" data-required="1" class="form-control" value="<?php echo set_value('last_name', isset($records->last_name) ? $records->last_name : ''); ?>" required data-parsley-maxlength="100" />
					<span id="last_name-error" class="help-block help-block-error"><?php echo form_error('last_name'); ?></span>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="form-group">
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"><label for="j-title"><?php echo $this->lang->line('job_title'); ?> *</label></div>
				<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
					<input type="text" name="job_title" data-required="1" class="form-control" value="<?php echo set_value('job_title', isset($records->job_title) ? $records->job_title : ''); ?>" required data-parsley-maxlength="100" />
					<span id="job_title-error" class="help-block help-block-error"><?php echo form_error('job_title'); ?></span>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="form-group">
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"><label for="reg"><?php echo $this->lang->line('contact_no'); ?> *</label></div>
				<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
					<div class="input-group">
						<span class="input-group-addon">
								+65
						</span>
						<input type="text" id="contact_no" name="contact_no" data-required="1" class="form-control" value="<?php echo set_value('contact_no', isset($records->contact_no) ? str_ireplace('+65','',$records->contact_no) : ''); ?>" required data-parsley-type="number" data-parsley-maxlength="8" data-parsley-minlength="8" data-parsley-errors-container="#contact_no-error" data-parsley-maxlength-message="<?php echo $this->lang->line('max_length_msg'); ?>" data-parsley-minlength-message="<?php echo $this->lang->line('min_length_msg'); ?>" />
					</div>
					<span id="contact_no-error" class="help-block help-block-error"><?php echo form_error('contact_no'); ?></span>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="form-group">
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"><label for="email"><?php echo $this->lang->line('work_email'); ?>*</label></div>
				<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
					<input type="email" name="email_id" id="email_id" onblur="checkUsername()" data-required="1" class="form-control" value="<?php echo set_value('email_id', isset($records->email_id) ? $records->email_id : ''); ?>"  required data-parsley-maxlength="100" />
					<span id="email_id-error" class="help-block help-block-error"><?php echo form_error('email_id'); ?></span>
				</div>
			</div>
				<div class="clearfix"></div>
				<input type='hidden' name="user_account_id" id="user_account_id" value="<?php echo set_value('user_account_id', isset($records->user_accounts_id) ? $records->user_accounts_id : '0'); ?>">
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 m-t80 no-padding">
				<button class="create org-box col-lg-3 col-md-3 col-xs-12 pull-right" type="submit" name='save' ><span><?php echo $action; ?></span>
					<i class="fa fa-chevron-right"></i>
				</button>
			</div>
			</form>
			<div class="clearfix"></div>
		</div>
	</div>
	</div>
</div>
<script type="text/javascript">
<?php if(isset($site_language) && $site_language ==  "chinese"){?>
		window.Parsley.setLocale('zh-cn');
	<?php }else{ ?>
		window.Parsley.setLocale('en');
	<?php }?>
	
$(".chosen-select").chosen();
var config = {
	'.chosen-select'           : {},
	'.chosen-select-deselect'  : {allow_single_deselect:true},
	'.chosen-select-no-single' : {disable_search_threshold:10},
	'.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
	'.chosen-select-width'     : {width:"95%"}
}
for (var selector in config) {
	$(".chosen-select").chosen(config[selector]);
}
$('.collapse').on('shown.bs.collapse', function(){
	$(this).parent().find(".glyphicon-plus").removeClass("glyphicon-plus").addClass("glyphicon-minus");
}).on('hidden.bs.collapse', function(){
	$(this).parent().find(".glyphicon-minus").removeClass("glyphicon-minus").addClass("glyphicon-plus");
});

function checkUsername() {
	var emailId=$("#email_id").val();
	var userAccountId=$("#user_account_id").val();
	var emailId = emailId.replace(' ','');
	if(emailId != '') {
			$.post(BASEURL+"admin/get_unique_id/",{emailId:emailId,userAccountId:userAccountId},function(data){
				if(data >= 1){
				$("#email_id-error").html("Email Id Already in Use");
					$('#email_id').val("");
				}
				else{
				$( "#email_id-error" ).empty();
				}
			});
	}
}
  </script>
