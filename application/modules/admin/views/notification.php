<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$message = getGlobalMessage();
?>
<div class="right_col right_col" role="main">
	<div class="row padding-top-one">
		<?php
		if($message['type']=='success') {
		?>
		<div class="Metronic-alerts alert alert-success fade in" id="prefix_167847684379"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button><?php echo $message['msg'] ?></div>
		<?php
		}
		?>
		<ul class="breadcrumb breadcrumb2">
			<li><a href="<?php echo base_url(); ?>"><?php echo $this->lang->line('home'); ?></a></li>
			<li class="active"><?php echo $this->lang->line('notifications'); ?></li>
		</ul>

		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
			<?php
			//~ echo '<pre>';
			//~ print_r($records);
			if(!empty($records)){
				$i=0;
				foreach($records as $record)
					{
						if($i<15){

			?>
						<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 org-box dark-org no-padding job-box text-center">
							  <div class="col-lg-10 col-md-11 col-sm-10 col-xs-9 center-block no-float">
								<h3 title="<?php echo $record->outlet_name; ?>"><?php if(strlen($record->outlet_name) > 11) { echo strtoupper(substr($record->outlet_name,0,11)).'...'; } else { echo strtoupper($record->outlet_name); }?></h3>
								<p>
									<span title="<?php echo $record->company_name; ?>"><?php if(strlen($record->company_name) > 11) { echo strtoupper(substr($record->company_name,0,11)).'..'; } else { echo strtoupper($record->company_name); }?></span>
									<span><?php echo $this->lang->line('new_outlet'); ?></span>
								</p>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
			<?php
					}
					$i++;
				}
			}else{ ?>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bmar">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 dark_organge org-boxs no-padding">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 dark_organge org-box  no-padding left-mar nob-mar job-box">
								<div class="col-lg-10 col-md-10 col-sm-10 col-xs-9 center-block no-float">
									<div class="over-scroll-left">
										<h2><?php echo "No Notifications Found."; ?></h2>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?php	}
			?>
		</div>
	</div>
</div>
<script>
function approve_job(id) {
	bootbox.confirm("Are you sure want to change the status?", function(result) {
		if(result==true) {
			$('#loader').fadeIn();
			var status = 1;
			location.href="<?php echo base_url('jobs/update_job_status'); ?>/"+id+"/"+status;
		}
	});
}

function reject_job(id) {
	bootbox.confirm("Are you sure want to change the status?", function(result) {
		if(result==true) {
			$('#loader').fadeIn();
			var status = 2;
			location.href="<?php echo base_url('jobs/update_job_status'); ?>/"+id+"/"+status;
		}
	});
}
</script>
