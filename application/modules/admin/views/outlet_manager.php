<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$message = getGlobalMessage();
?>
<div class="right_col right_col" role="main">
	<div class="row padding-top-one">
		<?php
		if($message['type']=='success') {
?>
<div class="Metronic-alerts alert alert-success fade in" id="prefix_167847684379"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button><?php echo $message['msg'] ?></div>
<?php
}
?>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<ul class="breadcrumb breadcrumb2">
			<li><a href="<?php echo base_url(); ?>"><?php echo $this->lang->line('home'); ?></a></li>
			<li class="active"><?php echo $this->lang->line('list_location_manager'); ?></li>
			</ul>
		</div>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<table class="table table-hovers no-wrap" id="table3">
					<thead class="firt-top">
						<tr>
							<th><?php echo $this->lang->line('created_date'); ?></th>
							<th><?php echo $this->lang->line('name'); ?></th>
							<th><?php echo $this->lang->line('outlet'); ?></th>
							<th><?php echo $this->lang->line('contact'); ?></th>

							<th></th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php /*
							$i=1;
							foreach($records as $company)
							{
								if($company->status==0) {
									$class = "enable";
									$name = "Enable";
									$fa = "fa fa-check";
									$trClass = "gray-box";
									$title = "Click to enable record";
								} else if($company->status==1) {
									$class = "disable";
									$name = "Disable";
									$fa = "fa fa-ban";
									$trClass = "";
									$title = "Click to disable record";
								}
							?>
						<tr class="<?php echo $trClass; ?>">
							<td><?php echo $company->created_at ?></td>
							<td><?php echo ucfirst($company->first_name).' '.ucfirst($company->last_name); ?></td>
							<td><?php
							if($company->outlet_id != '0') {
								$data = getOutletDetail($company->outlet_id);
								echo $data->outlet_name;
							} else {
								echo "No Outlet Assign";
							}
							 ?></td>
							<td><span><?php echo $company->contact_no; ?></span>
							<span><?php echo $company->email_id; ?></span></td>

							<td class="edit">
								<a href="<?php echo base_url('admin/outlet_manager_form/'.$company->outlet_manager_id); ?>">
									<span><i class="fa fa-pencil"></i></span>
									<span>Edit</span>
								</a>
							</td>

							<td class="<?php echo $class; ?>"><a href="javascript:;" title="<?php echo $title; ?>" onclick="myFunction('<?php echo $company->outlet_manager_id; ?>','<?php echo $company->user_accounts_id; ?>','<?php echo $company->status; ?>')">
								<span><i class="<?php echo $fa ?>"></i></span>
								<span><?php echo $name; ?></span>
							</a></td>
						</tr>
							<?php } */

						?>
					</tbody>
				</table>
			<div class="clearfix"></div>
			</div>
		</div>
	</div>
</div>
<script src="<?php echo base_url('templates/system/js/bootbox.min.js'); ?>"></script>

<script>
<?php if(isset($site_language) && $site_language == "chinese") { ?>
	bootbox.setDefaults({
		locale: "zh_CN"
    });
	var lang_url = "//cdn.datatables.net/plug-ins/1.10.12/i18n/Chinese.json";
<?php }else{ ?>
	bootbox.setDefaults({
		locale: "en"
	 });
	var lang_url = "//cdn.datatables.net/plug-ins/1.10.12/i18n/English.json";
<?php }?>
	
$('#table3').dataTable({
	"ajax": '<?php echo base_url('admin/outletManagerListByAjax'); ?>',
	"bPaginate": true,
	"bLengthChange": true,
	"bFilter": true,
	"bSort": true,
	"bInfo": true,
	"bAutoWidth": false,
	"order": [[ 0, "desc" ]],
	"columnDefs": [ { "targets": 0, "bSortable": true,"orderable": true, "visible": false }],
	"processing": true,
    "serverSide": true,
    "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
		if(aData[6] != '') {
					$(nRow).addClass(aData[6]);
					//$(nRow).addClass('multiorder');
		}
		$('td:eq(3)', nRow).addClass('edit');
		$('td:eq(4)', nRow).addClass(aData[7]);
	},
	"aoColumns": [null,null,null,null,{ "bSortable": false },{ "bSortable": false }],
	"language": {
                "url": lang_url
            },
});
function myFunction(id,userAccountsId,status) {
	var mesg = '<?php  echo $this->lang->line('status_change_msg')."?"; ?>';
		bootbox.confirm(mesg, function(result) {
			if(result==true) {
				location.href="<?php echo base_url('admin/outlet_manager_status'); ?>/"+id+"/"+userAccountsId+"/"+status;
			}
		});
	}

	$(document).ready(function() {
	   setTimeout(function(){
		   $('#table3_length').show();
		   $('#table3_filter').show();
		   $('#table3_paginate').show();
		}, 1000);

	});
</script>
