<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');  
$message = getGlobalMessage();
if(isset($records)) {
$status = $records->status;
}else {
$status = '1';
}
?>

<div class="right_col right_col " role="main">
	<?php
	if($message['type']=='success') {
	?>
	<div class="Metronic-alerts alert alert-success fade in" id="prefix_167847684379"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button><?php echo $message['msg'] ?></div>
	<?php
	} else if($message['type']=='error') {
	?>
	<div class="Metronic-alerts alert alert-danger fade in" id="prefix_167847684379"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button><?php echo $message['msg'] ?></div>
	<?php	
	}
	?>
	<div class="row padding-top-one">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center title-head2">
			<h3><?php 
					if($id) {
						echo $this->lang->line('edit').$this->lang->line('job_type'); 
						$action = $this->lang->line('update');
					} else {
						echo $this->lang->line('add_new').$this->lang->line('job_type'); 
						$action = $this->lang->line('create');
					}
				?>		</h3>
		</div>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				
				
					<form action="<?php echo base_url('admin/job_type_form/'.$id);?>" enctype="multipart/form-data" method='post' id="form" data-parsley-validate onsubmit="$('#loader').fadeIn()" >
					<div class="light-org-box pad-bottoms">
					<div class="clearfix"></div>
						<div class="form-group">
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"> 
								<label for="job_type"><?php echo $this->lang->line('job_type'); ?> * </label>
							</div>

							<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
								<input type="text" name="name" data-required="1" class="form-control" value="<?php echo set_value('name', isset($records->name) ? $records->name : ''); ?>" required data-parsley-maxlength="100"/>

							
								<span id="name-error" class="help-block help-block-error"><?php echo form_error('name'); ?></span>
							</div>
						</div>
						<div class="clearfix"></div>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 m-t80 no-padding">  
							<button class="create org-box col-lg-3 col-md-3 col-xs-12 pull-right" type="submit" name='save'>
								<span><?php echo $action; ?></span><i class="fa fa-chevron-right">
							</i>
							</button>
						</div>  
					</form>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
</div>
