<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php
	$userType=LoginUserDetails('role_id');
	$message = getGlobalMessage();
	//echo $site_language; die;
	//echo "<pre>"; print_r($transaction_history) ; die;
?>
<div class="right_col right_col" role="main">
	<?php
	if($message['type']=='success') {
	?>
	<div class="Metronic-alerts alert alert-success fade in" id="prefix_167847684379"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button><?php echo $message['msg'] ?></div>
	<?php
	} else if($message['type']=='error') {
	?>
	<div class="Metronic-alerts alert alert-danger fade in" id="prefix_167847684379"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button><?php echo $message['msg'] ?></div>
	<?php
	}
	?>
	<div class="row padding-top-one">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<ul class="breadcrumb breadcrumb2">
					<li><a href="<?php echo base_url(); ?>"><?php echo $this->lang->line('home'); ?></a></li>
					<li class=""><?php echo $this->lang->line('jod_credits'); ?></li>
					<li class="active"><?php echo $this->lang->line('assign_credits'); ?></li>
				</ul>
		</div>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 marb-22">
				<div class="light-gray-box orang">
					<div class="col-md-2 col-xs-12 text-center ava-cre2">
						<img src="<?php echo IMAGE; ?>jod-credit-icon-white.png">
					</div>
					<h3 class="pull-left star-h3-white star-h3 col-md-7 col-xs-12 text-center"><?php echo $this->lang->line('unassigned_jod_credit_hq'); ?></h3>
					<div class="col-md-3 col-xs-12 text-center  pull-right ava-credits">
						<h2 class="star-h3-white"><?php echo  sprintf('%0.2f',$company_details->current_jod_credit); ?></h2>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 marb-22">
				<div class="light-gray-box">
					<h3 class="pull-left star-h3 col-md-4 col-xs-12 text-center"><?php echo (!empty($outlet_data) && isset($outlet_data->outlet_name) && $outlet_data->outlet_name != "") ? $outlet_data->outlet_name : "N/A" ; ?></h3>
					<div class="col-md-3 col-xs-12 text-center pull-right ava-credits red">
						<h2 ><?php echo (isset($outlet_data->current_jod_credit)) ? $outlet_data->current_jod_credit : "0" ; ?></h2>
						<p ><?php echo $this->lang->line('available_credit'); ?></p>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="col-md-12 marb-22">
				<div class="yellow-back">
					<form id="assign_credit_form_outlet" method="post" action="" >
						<div class=" col-md-9">
								<div class="col-md-12">
									<div class="first">
										<div class="col-md-9 col-xs-12">
										<label ><?php echo $this->lang->line('enter_additional_credit'); ?> :</label>
										</div>
										<div class="col-md-4 col-xs-12">
										<input class="pull-right " name="jod_credit" required type="text">
										<div class="clearfix"></div>
									</div>
									</div>
									<div class="second">
										<div class="col-md-4 col-xs-12">
										<label ><?php echo $this->lang->line('comments'); ?> :</label>
									</div>
									<div class="col-md-8 col-xs-12">
									<textarea name="comment" required ></textarea>
									</div>
									</div>
								</div>
						</div>
						<div class="col-md-3 col-xs-12 enable-green width-height">
							<button type="submit" name="save_assign_credit" class="enable-green create">
								<span><i class="fa fa-check"></i></span>
								<div class="clearfix"></div>
								<span><?php echo $this->lang->line('confirm'); ?></span>
							</button>
						</div>
					</form>
					<div class="clearfix"></div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="clearfix"></div>
		<?php if(!empty($transaction_history)) { ?>
			<h2 class="credits-history"><?php echo $this->lang->line('credit_history'); ?><a class="download_button_credit_his" href="<?php echo base_url('admin/download_transaction_history/specific_outlet/'.$company_details->company_id.'/'.$outlet_data->outlet_id); ?>" onclick="loader()" ><i class="fa fa-download"></i></a></h2>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 overf">
				<table class="table table-th-gray table-striped table-data"  id="outlet_histoty_list">
					<thead class="firt-top">
						<tr>
							<th><?php echo $this->lang->line('date'); ?></th>
							<th><?php echo $this->lang->line('outlet_name'); ?></th>
							<th><?php echo $this->lang->line('amount'); ?> +/-</th>
							<th width="437"><?php echo $this->lang->line('comments'); ?></th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($transaction_history as $history){ ?>
							<tr class="">
								<td><?php echo (isset($history->assigned_at) && $history->assigned_at!= "") ? date('d M Y h:i A', strtotime($history->assigned_at)) : " " ;?></td>
								<td><?php echo (isset($history->outlet_name) && $history->outlet_name!= "") ? $history->outlet_name : " " ;?></td>
								<td> <?php echo (isset($history->transaction_type) && $history->transaction_type == 2) ? "+".  $history-> jod_credit_amount : "-".  $history-> jod_credit_amount; ?></td>
								<td><p class="jod-tooltip" data-toggle="tooltip" data-placement="bottom"  title="<?php echo ucfirst($history->comment_text); ?>"><?php echo strlen($history->comment_text) > 150 ? ucfirst(substr($history->comment_text,0,150)).'...' : ucfirst($history->comment_text) ;  ?></p></td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
				<div class="clearfix"></div>
			</div>
			<?php }?>

		<div class="clearfix"></div>
		</div>
		</div>
	</div>
</div>
<!-- /page content -->
<script>
	<?php if(isset($site_language) && $site_language == "chinese") { ?>
	var lang_url = "//cdn.datatables.net/plug-ins/1.10.12/i18n/Chinese.json";
	<?php }else{ ?>
		var lang_url = "//cdn.datatables.net/plug-ins/1.10.12/i18n/English.json";
	<?php }?>
	
$(".chosen-select").chosen();
    var config = {
      '.chosen-select'           : {},
      '.chosen-select-deselect'  : {allow_single_deselect:true},
      '.chosen-select-no-single' : {disable_search_threshold:10},
      '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
      '.chosen-select-width'     : {width:"95%"}
    }
    for (var selector in config) {
      $(".chosen-select").chosen(config[selector]);
    }

	$('.collapse').on('shown.bs.collapse', function(){
	$(this).parent().find(".glyphicon-plus").removeClass("glyphicon-plus").addClass("glyphicon-minus");
	}).on('hidden.bs.collapse', function(){
	$(this).parent().find(".glyphicon-minus").removeClass("glyphicon-minus").addClass("glyphicon-plus");
	});

	$(document).ready(function() {
		 $("td").tooltip();
	   $('#assign_credit_form_outlet').parsley();
	   setTimeout(function(){
		   $('#outlet_histoty_list_length').show();
		   $('#outlet_histoty_list_filter').show();
		   $('#outlet_histoty_list_paginate').show();
		}, 1000);

	});

	function loader(){
		$('#loader').fadeIn();
		$('#loader').delay(1500).fadeOut('slow');

	}

	$('#outlet_histoty_list').dataTable({
			"bPaginate": true,
			"bLengthChange": true,
			"bFilter": true,
			"bSort": true,
			"bInfo": true,
			"bAutoWidth": false,
			"language": {
                "url": lang_url
            },
			//"order": [[ 0, "desc" ]],
			//"columnDefs": [ { "targets": 0, "bSortable": true,"orderable": true, "visible": false }]
  });

</script>
