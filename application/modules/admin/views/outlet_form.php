<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$message = getGlobalMessage();
$all_outlet_status = check_all_outlet_job($id);

?>
<div class="right_col right_col " role="main">
	<?php
	if($message['type']=='success') {
	?>
	<div class="Metronic-alerts alert alert-success fade in" id="prefix_167847684379"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button><?php echo $message['msg'] ?></div>
	<?php
	} else if($message['type']=='error') {
	?>
	<div class="Metronic-alerts alert alert-danger fade in" id="prefix_167847684379"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button><?php echo $message['msg'] ?></div>
	<?php
	}
	?>
	<div class="row padding-top-one">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<ul class="breadcrumb breadcrumb2">
			<li><a href="<?php echo base_url(); ?>"><?php echo $this->lang->line('home'); ?></a></li>
			<?php if($id) { ?>
				<li class="active"><?php echo $this->lang->line('update_location'); ?> </li>
			<?php } else { ?>
				<li class="active"> <?php echo $this->lang->line('add_location'); ?></li>
			<?php } ?>
			</ul>
		</div>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center title-head2">
			<h3><?php
					if($id) {
						//echo 'Edit F&B Outlet';
						echo  $this->lang->line('edit').$this->lang->line('fnb_outlet');
						$action = $this->lang->line('update')." ".$this->lang->line('outlet');
					} else {
						echo  $this->lang->line('add_new').$this->lang->line('fnb_outlet');
						$action =$this->lang->line('add')." ".$this->lang->line('outlet');
					}
				?>		</h3>
		</div>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">


					<form action="<?php echo base_url('admin/outlet_form/'.$id);?>" enctype="multipart/form-data" method='post' data-parsley-validate onsubmit="$('#loader').fadeIn()">
					<div class="light-org-box pad-bottoms">
					<h4 class="text-center basic-details"><?php echo $this->lang->line('basic_details'); ?></h4>
						<div class="form-group">
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
								<label for="usr"><?php echo $this->lang->line('fnb_outlet_logo'); ?></label>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
								<?php if(isset($records->outlet_logo) && $records->outlet_logo != "") { ?>
									<img id="image-picker" style="width: 82%;" src="<?php echo base_url('uploads/outlet_logo/thumb/'.$records->outlet_logo); ?>">

								<?php } else { ?>
									<img id="image-picker" style="width: 82%;" src="<?php echo IMAGE ?>compay-logo.png">
								<?php } ?>
								<input type="file" name="image" style="display:none" onchange="read_url_image(this)" value="<?php echo set_value('outlet_logo', isset($records->outlet_logo) ? $records->outlet_logo : ''); ?>"/>
								<span id="company_logo-error" class="help-block help-block-error error-red"><?php echo form_error('outlet_logo'); ?></span>
							</div>
						</div>
					<div class="clearfix"></div>
						<div class="form-group">
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
								<label for="usr"><?php echo $this->lang->line('fnb_outlet_name'); ?> *</label>
							</div>
							<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
								<input  type="text" name="outlet_name" data-required="1" class="form-control" value="<?php echo set_value('outlet_name', isset($records->outlet_name) ? $records->outlet_name : ''); ?>" required data-parsley-minlength="1" data-parsley-maxlength="100" />
								<span id="outlet_name-error" class="help-block help-block-error error-red"><?php echo form_error('outlet_name'); ?></span>
							</div>
						</div>
					<div class="clearfix"></div>
						<div class="form-group">
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
								<label class=" line-h" for="address"><?php echo $this->lang->line('fnb_outlet_address'); ?> *</label>
							</div>
							<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
								<textarea name="outlet_address" class="form-control" data-required="1" required ><?php echo set_value('outlet_address', isset($records->outlet_address) ? $records->outlet_address : ''); ?></textarea>
								<span id="outlet_address-error" class="help-block help-block-error"><?php echo form_error('outlet_address'); ?></span>
							</div>
						</div>

					<div class="clearfix"></div>
						<div class="form-group">
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
								
								<label class=" line-h" for="checkbox"><?php echo $this->lang->line('job_approval_required'); ?> </label>
							</div>
                            <div class="col-lg-1">
								<div class="check-box">
									<div class="check-wrapper">
										<input type="checkbox" name="is_approval_required" id="check1" value="1" <?php if(isset($records->is_approval_required) && $records->is_approval_required == 1) { echo "checked"; } ?> >
										<label for="check1"> </label>
									</div>
								</div>
								<span id="is_approval_required-error" class="help-block help-block-error"><?php echo form_error('is_approval_required'); ?></span>
							</div>
						</div>

					<div class="clearfix"></div>
						<div class="form-group">
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
								<label class=" line-h" for="checkbox"><?php echo $this->lang->line('create_template_only'); ?></label>
							</div>
                            <div class="col-lg-1">
								<div class="check-box">
									<div class="check-wrapper">
										<input type="checkbox" name="is_template_required" id="check2" value="1" <?php if(isset($records->is_template_required) && $records->is_template_required == 1) { echo "checked"; } ?> >
										<label for="check2"> </label>
									</div>
								</div>
								<span id="is_template_required-error" class="help-block help-block-error"><?php echo form_error('is_template_required'); ?></span>
							</div>
						</div>
					<!--- added by Kratika for the job can also be created by outlet Manager -->
					<div class="clearfix"></div>
						<div class="form-group">
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
								<label class=" line-h" for="checkbox"><?php echo $this->lang->line('om_also_create_job'); ?></label>
							</div>
                            <div class="col-lg-1">
								<div class="check-box">
									<div class="check-wrapper">
										<input type="checkbox" name="job_created_by_OM" id="check3" value="1" <?php if(isset($records->job_created_by_outlet_manager) && $records->job_created_by_outlet_manager == 1) { echo "checked"; } ?> >
										<label for="check3"> </label>
									</div>
								</div>
								<span id="job_created_by_OM-error" class="help-block help-block-error"><?php echo form_error('job_created_by_OM'); ?></span>
							</div>
						</div>

					<div class="clearfix"></div>
						<div class="form-group">
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
								<label class=" line-h" for="checkbox"><?php echo $this->lang->line('area_manager'); ?></label>
							</div>
							<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
								<div class="col-lg-9 col-md-12 col-xs-12 left-pa bot-mar remove-pad">
									<select class="form-control chosen-select" name="area_manager_id" id="area_manager_id">
									<option value="" >--<?php echo $this->lang->line('area_manager'); ?>--</option>
										<?php foreach($areaManagers as $areaManager) { ?>
									<option value="<?php echo $areaManager->area_manager_id; ?>" <?php if(set_value('area_manager_id', isset($records->area_manager_id) ? $records->area_manager_id : '') == $areaManager->area_manager_id){ echo 'selected="selected"'; } ?> ><?php echo $areaManager->first_name.' '.$areaManager->last_name;?></option>
										<?php } ?>
									</select>
								</div>
								<button class="ad-man create black-dark col-lg-3 col-md-4 col-sm-4 col-xs-12" type="button" data-toggle="modal" data-target=".fade">
								<span><?php echo $this->lang->line('add_manager'); ?></span>
								</button>
							</div>
						</div>
					<div class="clearfix"></div>
					<?php if($action == 'Create') { ?>
						<div class="form-group">
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
								<label class=" line-h" for="checkbox">Outlet Manager *</label>
							</div>
							<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
								<div class="col-lg-9 col-md-12 col-xs-12 left-pa bot-mar remove-pad">
									<select class="form-control chosen-select" name="outlet_manager_id" id="outlet_manager_id" required data-parsley-errors-container="#outlet_manager_id-error">
									<option value="" >--Select Outlet Manager--</option>
										<?php foreach($outletManagers as $outletManager) { ?>
									<option value="<?php echo $outletManager->outlet_manager_id; ?>"><?php echo $outletManager->first_name.' '.$outletManager->last_name;?></option>
										<?php } ?>
									</select>
									<span id="outlet_manager_id-error" class="help-block help-block-error"><?php echo form_error('outlet_manager_id'); ?></span>
								</div>
								<button class="ad-man create black-dark col-lg-3 col-md-4 col-sm-4 col-xs-12" type="button" id="addOutletMan">
								<span>Add Manager</span>
								</button>
							</div>
						</div>
					<div class="clearfix"></div>
					<?php } else if($action == 'Update') {
						?>
						<?php
									$data = getOutletManagerByOutlet($id);
									$j = 0;
									if(!empty($data)) {
									foreach($data as $newData) {
									?>
						<div class="form-group">
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
								<?php if($j == 0 ) { ?>
								<label class=" line-h" for="checkbox">Outlet Manager *</label>
								<?php } ?>
							</div>

							<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
								<div class="col-lg-9 col-md-12 col-xs-12 left-pa bot-mar remove-pad">
									<label class=" line-h" for="checkbox"><?php echo  $newData->first_name." ".$newData->last_name; ?></label>
								</div>
								<button class="ad-man create black-dark col-lg-3 col-md-4 col-sm-4 col-xs-12" type="button" id="unassignOutletMan" onclick="check_outlet_man('<?php echo $newData->user_accounts_id; ?>','<?php echo $newData->outlet_manager_id; ?>',<?php echo $id ?>)" >
								<span>Unassign</span>
								</button>
							</div>

						</div>
						<?php
							$j = 1;
							} ?>
						<?php
						}
						}
						?>
						<div class="clearfix"></div>
						</div>
						</div>
						</div>
					</div>

					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 m-t80 ">
						<button class="create org-box col-lg-3 col-md-3 col-xs-12 pull-right no-padding" type="submit" name='save' value="save">
							<span><?php echo $action; ?></span><i class="fa fa-chevron-right"></i>
						</button>
					</div>
					</form>
				<div class="clearfix"></div>
			</div>
		</div>

	</div>
</div>


<div class="modal fade  bs-example-modal-md" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
	<div class="cent">
    <div class="modal-dialog col-lg-5 center mode-w">
		<form role="form" id="add_manager_form" method="post" data-parsley-validate onsubmit="$('#loader').fadeIn()">
			<div class="modal-content">
				<div class="gr-inp shadow-box left-pa" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" >
                   <button data-dismiss="modal" class="close-b"><i class="fa fa-times-circle"></i></button>
					<h4 class="text-center basic-details org-box"><?php echo $this->lang->line('add_new_area_manager'); ?></h4>
					<div class="form-group"><div class="col-lg-1 col-md-1 col-sm-1 hidden-xs"></div>
						<div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
							<input type="text" placeholder="<?php echo $this->lang->line('first_name'); ?>" class="form-control" id="first_name" name="first_name" required >
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="form-group">
						<div class="col-lg-1 col-md-1 col-sm-1 hidden-xs"></div>
						<div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
							<input type="text" placeholder="<?php echo $this->lang->line('last_name'); ?>" class="form-control" id="last_name" name="last_name" required>
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="form-group">
						<div class="col-lg-1 col-md-1 col-sm-1 hidden-xs"></div>
						<div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 ">
							<input type="text" placeholder="<?php echo $this->lang->line('job_title'); ?>" class="form-control" id="job_title" name="job_title" required>
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="form-group">
						<div class="col-lg-1 col-md-1 col-sm-1 hidden-xs"></div>
						<div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
							<div class="input-group">
								<span class="input-group-addon">
										+65
								</span>
								<input type="text" placeholder="<?php echo $this->lang->line('contact_no'); ?>"  class="form-control" id="contact_no" name="contact_no" required data-parsley-type="number" data-parsley-maxlength="8" data-parsley-minlength="8" data-parsley-errors-container="#contact_no-error" data-parsley-maxlength-message="This value is too long. It should have 8 characters." data-parsley-minlength-message="This value is too short. It should have 8 characters." >
							</div>
						<span id="contact_no-error" class="help-block help-block-error"><?php echo form_error('contact_no'); ?></span>
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="form-group">
						<div class="col-lg-1 col-md-1 col-sm-1 hidden-xs"></div>
						<div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
							<input type="email" placeholder="<?php echo $this->lang->line('email_address'); ?>" class="form-control" id="email_id" name="email_id" required onblur="checkUsername()" >
							<span id="email_id-error" class="help-block help-block-error"><?php echo form_error('email_id'); ?></span>
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="col-lg-1 col-md-1 col-sm-1 hidden-xs"></div>
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 m30">
					<button class="create red-box col-lg-5 col-md-5 col-sm-5 col-xs-12 pull-left" id="model-cancel" data-dismiss="modal" >
					<i class="fa fa-close"></i><span><?php echo $this->lang->line('cancel'); ?></span>
					</button>
					<button class="create org-box col-lg-5 col-md-5 col-sm-5 col-xs-12 pull-right" id="save" name="save">

					<span><?php echo $this->lang->line('add'); ?> </span> <i class="fa fa-chevron-right"></i>
				</button>
					</div>
				</div>
			</div>
		</form>
	</div></div>
</div>



<script>
<?php if(isset($site_language) && $site_language ==  "chinese"){?>
		window.Parsley.setLocale('zh-cn');
	<?php }else{ ?>
		window.Parsley.setLocale('en');
	<?php }?>	
	
	
$(".chosen-select").chosen();
var config = {
  '.chosen-select'           : {},
  '.chosen-select-deselect'  : {allow_single_deselect:true},
  '.chosen-select-no-single' : {disable_search_threshold:10},
  '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
  '.chosen-select-width'     : {width:"95%"}
}
for (var selector in config) {
  $(".chosen-select").chosen(config[selector]);
}

$('.collapse').on('shown.bs.collapse', function(){
$(this).parent().find(".glyphicon-plus").removeClass("glyphicon-plus").addClass("glyphicon-minus");
}).on('hidden.bs.collapse', function(){
$(this).parent().find(".glyphicon-minus").removeClass("glyphicon-minus").addClass("glyphicon-plus");
});


$("#image-picker").click(function() {
    $("input[name='image']").click();
});
function read_url_image(input) {
	if (input.files && input.files[0]) {
		var file = input.files[0];
		//alert(file.type);
		var imageType = /image.*/;
		if (file.type.match(imageType)) {
			var reader = new FileReader();
			reader.onload = function (e) {
				$('#image-picker').attr('src', e.target.result);
			}
			reader.readAsDataURL(input.files[0]);
		} else {
			$("#image").val("");
			alert("File not supported!");
		}
	}
}
$("document").ready(function(){
	$("#check3").change(function() {
		if($(this).is(':checked')){}else{
			if('<?php echo $all_outlet_status;?>'=='exist'){
				$("#check3").trigger('click');
				bootbox.dialog({title: "",message: "Outlet managers of this outlet have already having some open jobs.So you can't change this status."});


			}
		}

	});


	$("#add_manager_form").submit(function() {
		$('#loader').fadeIn();
		$.ajax({
			type: "POST",
			url: "<?php echo base_url() ?>admin/outlet_save_area_manager",
			data: $(this).serialize(),
			success: function( data, textStatus, jQxhr ){
				if(data != 0)	{
					var dataArray = data.split('#BREAKTHIS#');
					$('#area_manager_id').append('<option selected="selected" value="'+$.trim(dataArray[0])+'">'+$.trim(dataArray[1])+'</option>');
					$("#area_manager_id").trigger("chosen:updated");
					$( "#model-cancel" ).trigger( "click" );
					$('#loader').fadeOut();
				} else {
					$('#loader').fadeOut();
					$("#email_id-error").html("Email Id Already in Use");
					$('#email_id').val("");
				}
			},
			error: function( jqXhr, textStatus, errorThrown ){
				$('#loader').fadeOut();
				alert( errorThrown );
			}
		});
		return false;
	});
});


function checkUsername() {
	var emailId=$("#email_id").val();
	var userAccountId=0;
	var emailId = emailId.replace(' ','');
	if(emailId != '') {
			$.post(BASEURL+"admin/get_unique_id/",{emailId:emailId,userAccountId:userAccountId},function(data){
				if(data >= 1){
				$("#email_id-error").html("Email Id Already in Use");
					$('#email_id').val("");
				}
				else{
				$( "#email_id-error" ).empty();
				}
			});
	}
}

$("#addOutletMan").click(function() {
	location.href="<?php echo base_url('admin/outlet_manager_form'); ?>";
});

function check_outlet_man(userAccountId,id,outletId) {  // function will check, all job of outlet managers are complete or not
	if(userAccountId != '') {
			$.post(BASEURL+"admin/check_outlet_man/"+userAccountId,function(data){
				if(data == 0){
					bootbox.confirm("Are you sure want to Unassign the outlet manager?", function(result) {
						if(result==true) {
							location.href="<?php echo base_url('admin/unassigns_outlet_manager'); ?>/"+id+"/"+outletId;
						}
					});
				}
				else{
					alert('Some of jobs of outlet manager is not complete, So he can\'t unassign from outlet.');
				}
			});
	}
}

var action = '<?php echo $action; ?>';

$( "#area_manager_id" ).change(function() {
	if(action == "Update outlet"){
		var outlet_id = '<?php echo $id; ?>';
		var assined_area_manger = '<?php echo isset($records->area_manager_id) ? $records->area_manager_id : 0; ?>';
		$.ajax({
		type : "POST",
		async: false,
		url : BASEURL+"admin/get_active_jobs",
		data : {
			outlet_id : outlet_id,
			area_manager : assined_area_manger,
		},
		error : function(jqXHR, textStatus, errorThrown) {
			alert("ERROR");
		},
		success : function(data, textStatus, jqXHR) {
			var obj_data = JSON.parse(data);
			if(obj_data.code == 0){
				var message = "Existing Area Manager have some incompeleted jobs with outlet, So currently you can not change the area manger of this outlet.";
				bootbox.alert(message, function() {
				});
				$('select').chosen();
				$('select').val(assined_area_manger);
				$('select').trigger("chosen:updated");
				//$( "#area_manager_id" ).val(assined_area_manger);
			}else{
				//console.log("No open jobs exist");
			}
		}
	});

	}
});


</script>
