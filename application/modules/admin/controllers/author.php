<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @description: This controller is used for Author section
 * @create date: 9th Jan 2015
 * @auther: Gagan David
 * 
 */ 

class Author extends MX_Controller{

	function __construct(){
		parent::__construct();
		
		$this->load->library(array('admin_template','head'));
		$this->load->model(array('admin_model','transaction_model'));
		
		$this->load->language(array('common'));
		$this->form_validation->set_error_delimiters('<div class="server_msg_show">', '</div>');
		$this->session_check->checkSession(); 	
		$this->load->dbutil();
		$this->load->helper('text');
		
		//$this->dbutil->optimize_table('hk_user');
	}
	
	/*Description : function to get author list
	* Author: Gagan David
	* Date : 9th Jan 2015
	* 
	*/
	public function index(){
		
		//call factory method for event register user data
		$data['records'] = $this->admin_model->get_author();
		$this->admin_template->load('admin_template','author',$data);
	}
	
	
	/*
	 * Description : function to add author 
	* Author: Gagan David
	* Date : 9th Jan 2015
	* 
	*/
	
	public function author_form()
	{
		$data['id'] = '';
		if(isset($_POST['save']))
		{
			$this->save_author();
		}
		$this->admin_template->load('admin_template','author_form',$data);
	}
	
	/*
	 * Description : function to save author
	* Author: Gagan David
	* Date : 9th Jan 2015
	* 
	*/
	
	public function save_author()
	{
		$data = array();
		$this->form_validation->set_rules('title','title','required|trim|max_length[255]');
		$this->form_validation->set_rules('description','description','required|trim');
		$this->form_validation->set_rules('image_value','image','required|trim');
		$image = upload_images('','image',1);
		
		if ($this->form_validation->run() === FALSE)
		{
			return FALSE;
		}
		$data['author'] = $_POST['title'];
		$data['author_description'] = $_POST['description'];
		$data['photo'] = $image;
		$this->admin_model->save_author($data);
		set_global_messages('Author Added Successfully','success');
		redirect('admin/author');
		//print_r($data);die;
		
	}
	
	
	/*
	 * Description : function to delete author
	* Author: Gagan David
	* Date : 10th Jan 2015
	* 
	*/
	public function author_delete($id='')
	{
		$this->admin_model->delete_author($id);
		set_global_messages('Author Delete Successfully','success');
		redirect('admin/author');
		
	}
	
	
	/*
	 * Description : function to advertisement transactions
	* Author: Gagan David
	* Date : 10th Jan 2015 
	*/
	/*    For Advertisement      */
	
	public function transaction($type='',$id='')
	{
		$data['type'] = $type;
		$data['records'] = $this->transaction_model->get_transactions($type,$id);
		$this->admin_template->load('admin_template','transaction',$data);
	}
	
	
	/*
	 * Description : function to advertisement transactions export
	* Author: Gagan David
	* Date : 12th Jan 2015 
	*/
	
	public function transaction_export($type='',$id='')
	{
		
		$data['records'] = $this->transaction_model->get_transactions($type,$id);
		$filename = "transaction" . date('Ymd') . ".csv";
		$i=0;
		$re_data = array();
		foreach($data['records'] as $transaction)
		{
			$details = get_user_name($transaction->user_id);
			$video_name = get_video_name($transaction->video_id);
			$originalDate = $transaction->created_at;
			$created_at  = date("d M Y h:i A", strtotime($originalDate));
			 
			$re_data[$i]['first_name'] = $details->first_name;
			$re_data[$i]['video'] = $video_name->description;
			$re_data[$i]['consumed_time'] = $transaction->consumed_time;
			$re_data[$i]['earn_time'] = $transaction->earn_time;
			$re_data[$i]['created_at'] = $created_at;
			$i++;
		}
		function cleanData(&$str)
		{
			$str = preg_replace("/\t/", "\\t", $str);
			$str = preg_replace("/\r?/", "", $str);
			if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
		}
		
		header('Content-Type: text/csv; charset=utf-8');
		header("Content-Disposition: attachment; filename=\"$filename\"");
		$flag = false;
		$i=1;
		foreach($re_data as $row) 
		{
			if(!$flag) 
			{
				echo 'S.No,Name,Video,Consumed Time,Earn Time,Transaction Time'. "\n";
				$flag = true;
			}
			array_walk($row, 'cleanData');
			echo $i++.',';
			echo implode(",",array_values($row))."\n";
		}

		exit;
	
	}
	
	/*
	 * Description : function to get the season list
	* Author: Gagan David
	* Date : 13th Jan 2015 
	*/
	
	public function season()
	{
		
		if(isset($_POST['action']))
		{
			//print_r($_POST);die;
			$id = $_POST['id'];
			$action =$_POST['action'];
			foreach($id as $id)
			{
				if($action==3)
				{
					$this->delete_season($id,1);
				}
				
			}
			if($_POST['action']==3)
			{
				set_global_messages('Season Deleted Successfully','success');
				
			}
		}
		$data['type'] ='';
		$data['records'] = $this->transaction_model->get_season();
		$this->admin_template->load('admin_template','season',$data);
	}
	
	
	/*
	* Description : function to get the Add season
	* Author: Gagan David
	* Date : 13th Jan 2015 
	*/
	
	public function add_season($id='')
	{
		if(isset($_POST['save']))
		{
			$this->save_season($id);
		}
		$data['type'] ='';
		$data['id'] =$id;
		$data['records'] = $this->transaction_model->get_season($id);
		//print_r($data['records']);die;
		$data['program'] = $this->admin_model->get_episode();
		$this->admin_template->load('admin_template','add_season',$data);
	}
	
	
	/*
	* Description : function to save season
	* Author: Gagan David
	* Date : 13th Jan 2015 
	*/
	
	public function save_season($id='')
	{
		$this->form_validation->set_rules('title','title','required|trim|max_length[255]');
		$this->form_validation->set_rules('image_value','image','required|trim|max_length[255]');
		if ($this->form_validation->run() === FALSE)
		{
			return FALSE;
		}
	
		if(($id) && ($_FILES['image']['name']!='') )
		{
			$data['image_url'] = upload_images('image',1);
		}
		else
		{
			if(($_FILES['image']['name']!=''))
			{
				$data['image_url'] = upload_images('image',1);
			}
		}
		$data['serial_id'] = $_POST['episode'];
		$data['season_name'] = $_POST['title'];
		$data['created_at'] = date('Y-m-d h:i:s');
		$data['start_date'] = $_POST['start_date'];
		$data['end_date'] = $_POST['end_date'];
		if($id)
		{
			$data['action'] = 'U';
			$this->transaction_model->update_season($data,$id);
		}
		else
		{
			$this->transaction_model->add_season($data);
		}
		set_global_messages('Season  Added Successfully','success');
		redirect('admin/author/season');
		//print_r($data);die;
	}
	
	/*
	 * Description: deletes season
	 * Author : Gagan David
	 * Date :14 Jan 2015
	 */
	
	public function delete_season($id='',$type='')
	{
		
		$this->transaction_model->delete_season($id);
		if(!$type)
		{
			set_global_messages('Season  Deleted Successfully','success');
			redirect('admin/author/season');
		}
	}
	
	/*
	* Description: Get Serial seasons
	* Author : Gagan David
	* Date :14 Jan 2015
	*/
	public function get_serial_season($id='')
	{
		$id = $_POST['id'];
		if(isset( $_POST['select_season']))
		{
			$select_season = $_POST['select_season'];
		}
		else
		{
			$select_season ='';
		}
		$seasons = $this->transaction_model->get_serial_season($id);
		$select ='';
		if(!empty($seasons))
		{
			foreach($seasons as $season)
			{
				if($season->id == $select_season)
				{
					$select = 'selected="selected"';
				}
				else
				{
					$select ='';
				}
				echo '<option value='.$season->id.' '.$select.'>'.$season->season_name.'</option>';
			}
		}
		else
		{
			echo '<option value="0">Season 1(Default)</option>';
		}
		//print_r($season);
	}
	
	/*
	* Description: Get Episode sequence
	* Author : Gagan David
	* Date :14 Jan 2015
	*/
	public function get_episode_seq($episode_id='',$edit_id='')
	{
		$check=1;
		$episode_no = $_POST['episode_no'];
		if($edit_id)
		{
			if($episode_id== $episode_no)
			{
				$check=0;
				echo json_encode(true);
				exit;
			}
			else
			{
				$check=1;
			}
		}
		
		if($check==1)
		{
			$serial_id = $_POST['serial_id'];
			$season_id = $_POST['season_id'];
			$seasons = $this->transaction_model->get_episode_seq($episode_no,$serial_id,$season_id);
			if(count($seasons)>0)
			{
				echo json_encode(false);
			}
			else
			{
				echo json_encode(true);
			}
		}
	}
	
	/*
	* Description: Get all Episodes
	* Author : Gagan David
	* Date :15 Jan 2015
	*/
	
	public function get_all_episodes($serial_id='',$season_id ='')
	{
		if($serial_id=='' && $season_id =='' )
		{
			$serial_id = $_POST['serial_id'];
			$season_id = $_POST['season_id'];
			$data['classification'] = $_POST['classic_id'];
			$data['channel'] = $_POST['channel'];
			$data['season_id'] = $_POST['season_id'];
			$data['serial'] = $serial_id ;
		}
		$data['seasons'] = $this->transaction_model->get_all_episodes($serial_id,$season_id);
		$this->load->view('get_all_episodes',$data);
		
	}
	
	/*
	* Description: Delete Episodes
	* Author : Gagan David
	* Date :15 Jan 2015
	*/
	public function delete_episode($id='')
	{
		//print_r($_GET);die;
		if(isset($_GET))
		{
			$classification = $_GET['classification'];
			$channel = $_GET['channel'];
			$season = $_GET['season'];
			$serial_id = $_GET['serial_id'];
		}
		$id = $this->transaction_model->delete_episode($id);
		if($id)
		{//die;
			set_global_messages('Episode  Deleted Successfully','success');
			redirect('admin/video/episode_list?classification='.$classification.'&channel='.$channel.'&season='.$season_id.'&serial_id='.$serial_id);
		}
	}
	
	
	
}
