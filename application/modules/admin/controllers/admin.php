<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller
{

	function __construct() {
		parent::__construct();
		if (!$this->input->is_ajax_request()) {
			checkStatus();
		}
		$ci =& get_instance();
		$siteLang = $ci->session->userdata('language');
		//echo $siteLang; die;
		$this->load->library(array('admin_template','head'));
		$this->load->model(array('admin_model'));
		$this->load->model(array('transaction_model'));
		$this->load->model('jobs/jobs_model');
		$this->session_check->checkSession();
		$this->load->library('excel');
		$this->config->set_item('language', $siteLang);
		//echo $siteLang; die;
		$this->lang->load(array("admin","common","jod_credit","hq_manager","job"),$siteLang);
	}


    public function index() {
		$userType=LoginUserDetails('role_id');
		//echo $userType; die;
		$data['title'] = "Dashboard";
		$data['no_of_open_jobs'] = $this->jobs_model->get_open_jobs('','count');
		$data['no_of_active_jobs'] = $this->jobs_model->get_active_jobs('','count','','');
		//$data['hq_manager_notification_count'] =$this->jobs_model->get_notification('','count','','');
		//$data['hq_manager_notification_count'] =$this->jobs_model->get_hq_manager_notification();
		$data['hq_manager_notification_count'] =get_hq_manager_notification('count');
		//echo $data['hq_manager_notification_count'];die;
		$date = date('Y-m-d');
		$data['no_of_heired_applicants'] = get_today_hired_applicants_count($date);
		if($userType == 1) {
			$page = 'dashboard';
		} else if($userType == 2) {
			$data['no_of_open_jobs'] = $this->jobs_model->get_all_jobs('','count');
			$data['enabled_outlets'] = $this->jobs_model->get_enable_outlet_for_HQ_Manager();
			$data["company_details"] = getCompanydetails();
			$data["assigned_JOD_credits"] = get_total_assigned_JOD_credit_to_outlets($data["company_details"]->company_id);
			$page = 'hq_dashboard';
		} else if($userType == 3) {
			$page = 'area_manager_dashboard';
			$outlet_managers_id = LoginUserDetails('userid');
			$outlet_manager = getOMManagerDetails($outlet_managers_id);
			$outlet_data = $this->admin_model->get_outlet($outlet_manager[0]->outlet_id);
			$data["assigned_JOD_credits"] = $outlet_data->current_jod_credit;

		} else if($userType == 5) {
			$page = 'area_manager_dashboard';
			$user_id = LoginUserDetails('userid');
			$area_manager_details = getAreaManagerId($user_id);
			$details = get_current_jod_credits_outlets($area_manager_details[0]->area_manager_id);
			if(!empty($details)){
				$data["assigned_JOD_credits"] = $details[0]->total_current_jod_credit;
			}
		}
		if($page == 'area_manager_dashboard'){
			$this->load->library('pagination');
			$limit= 10;
			$pageNo = 1;
			if ($this->input->get('per_page')!="" && $this->input->get('per_page')!==FALSE)
				$pageNo = $this->input->get('per_page');
				$this->load->library('paging_library');
				$pagingRecord = array('url'=>base_url().'/admin?','count'=>$this->jobs_model->get_all_jobs('','count','',''),'per_page'=>$limit);
				$data['pagination']   = $this->paging_library->get_config_paging($pagingRecord);
				$data['all_jobs_records'] = $this->jobs_model->get_all_jobs('','result',$pageNo,$limit);
				
		}
		$this->admin_template->load('admin_template',$page,$data);
	}

		/*****************************Image Upload Start*****************************/
	function uploadProfileImage($imagefolder) {
		$data = array();
		$dirPath = IMAGEUPLOADPATH.$imagefolder.'/';
        $response=array();
		if (isset($_FILES["image"]["name"]) && $_FILES["image"]["name"]!="") {
			$imageName = date("dmyHis");
			$this->load->library('upload');
			$this->upload->initialize(set_upload_options($dirPath.'','jpg|JPEG|PNG|png|gif|jpeg',$imageName));
			if($this->upload->do_upload('image')) {
			$data=$this->upload->data();
			$file_path=$dirPath.''.$data['file_name'];
			$this->load->library('image_lib');
			$config = array();
			$config['allowed_types'] 	= 'gif|jpg|png';
			$config['image_library'] 	= 'gd2';
			$config['source_image'] 	= $file_path;
			$config['maintain_ratio'] 	= FALSE;
			$config['create_thumb'] 	= FALSE;
			$config['new_image'] 		= $dirPath.'crop/'.$data['file_name'];
			$config['width'] 			= 320;
			$config['quality'] 			= 100;
			$config['height'] 			= 369;
			$this->image_lib->initialize($config);
			$this->image_lib->resize();

			$config['new_image'] 		= $dirPath.'thumb/'.$data['file_name'];
			$config['width'] 			= 200;
			$config['height'] 			= 200;
			$this->image_lib->initialize($config);
			$this->image_lib->resize();

			$this->image_lib->clear();
			$response = array(
				"status" => 'success',
				"imageName" => $data['file_name'],
				"width" => $data['image_width'],
				"height" => $data['image_height']
			);
			} else {
				$response = array(
				"status" => $this->upload->display_errors() );
			}
		}

		return $response;
	}

	function upload_document() {
		$data = array();
		$dirPath = IMAGEUPLOADPATH.'contract_of_service/';
		$response=array();
		if (isset($_FILES["contract_of_service"]["name"]) && $_FILES["contract_of_service"]["name"]!="") {
			$docName = $_FILES["contract_of_service"]["name"];
			$this->load->library('upload');
			$this->upload->initialize(set_upload_options($dirPath.'','pdf',$docName));
			if($this->upload->do_upload('contract_of_service')) {
				$data=$this->upload->data();
				$file_path=$dirPath.''.$data['file_name'];
				$config['source_image'] 	= $file_path;
				$response = array(
					"status" => 'success',
					"docName" => $data['file_name'],
				);
			} else {
				$response = array(
				"status" => $this->upload->display_errors() );
			}
		}
		return $response;
	}
		/*****************************Image Upload End*****************************/

		/*****************************Company Section Start*****************************/
	public function company_section() {
		$ci =& get_instance();
		$siteLang = $ci->session->userdata('language');
		$userType=LoginUserDetails('role_id');
		if($userType == 1){
			$data = array();
			$data['site_language'] = $siteLang ;
			$this->config->set_item('site_title', $this->lang->line('company'));
			$this->admin_template->load('admin_template','company',$data);
		}else{
			$this->admin_template->load('admin_template','not_authorize');
		}
	}

	public function getCompanyListByAjax(){
		$search = "";
		$search_set = 0;
		$dirs = "";
		$column_name  ="";
		if(isset($_GET['order'][0]['dir']) && !empty($_GET['order'][0]['dir'])) {
			$dirs = $_GET['order'][0]['dir'];

		}
		if($_GET['order'][0]['column'] == 0) {
			$dirs = 'ASC';

		}
		if(isset($_GET['order'][0]['column']) && !empty($_GET['order'][0]['column'])){
			if($_GET['order'][0]['column'] == 1) {
				$column_name = "company_name";
			}
			if($_GET['order'][0]['column'] == 2) {
				$column_name = "company_address";
			}
		}
		if(isset($_GET['search']['value']) && !empty($_GET['search']['value'])) {
			$search = $_GET['search']['value'];
		    $search = trim($search);
		    $search_set = 1 ;
		}
		$companyArray = array();
		$managerArray = array();
		$selectedCompany = $this->admin_model->get_selected_company();
		foreach($selectedCompany as $company){
				$companyArray[] = $company->company_id;
				$managerArray[$company->company_id] = $company->hq_manager_id;
		}
		//
		$records = $this->admin_model->get_company("",$search,$_GET['length'],$_GET['start'],$dirs,$column_name);
		//print_r($records); die;
		$all_records = $this->admin_model->get_company();
		$total_records = count($all_records);
		$records_filtered = $total_records;
		if(!empty($records) || !empty($all_records)) {
			$messages      = array();
			$i=0;
			$s = $_GET['start'] + 1;
			if($search_set == 1){
				$records_filtered	= count($records);
			}
			$content='[';
			foreach($records as $record) {
				if($record->status==0) {
					$class = "enable";
					$name = $this->lang->line('enable') ;
					$fa = "fa fa-check";
					$trClass = "gray-box";
					$title = "Click to enable record";
				} else if($record->status==1) {
					$class = "disable";
					$name = $this->lang->line('disable');
					$fa = "fa fa-ban";
					$trClass = "";
					$title = "Click to disable record";
				}
				$content .='[';
				$messages[$i][0] = $record->created_at;
				$messages[$i][1] = (strlen($record->company_name) > 100) ? ucfirst(substr($record->company_name,0,75)).'...' : ucfirst($record->company_name);
				$messages[$i][2] =  substr($record->company_address,0,500);
				$messages[$i][3] = '<a href="'.base_url('admin/assign_credits_to_company/'.$record->company_id).'"><span><img src="'.base_url('templates/JOD/img').'/jod-credit-icon-white.png"></span><span>'.$this->lang->line('credits').'</span></a>';
				$messages[$i][4]  =  (!in_array($record->company_id,$companyArray)) ? '<a href="'.base_url('admin/headquater_form/0/'.$company->company_id).'"><span><i class="fa fa-plus"></i></span><span>'.$this->lang->line('hq_user').'</span></a>' : '<a href="'.base_url('admin/headquater_form/'.$managerArray[$record->company_id].'/'.$record->company_id).'"><span><i class="fa fa-pencil"></i></span><span>'.$this->lang->line('edit_hq_user').'</span></a>';
				$messages[$i][5] = '<a href="'.base_url('admin/company_form/'.$record->company_id).'"><span><i class="fa fa-pencil"></i></span><span>'.$this->lang->line('edit').'</span></a>';
				$messages[$i][6] = '<a href="javascript:;" title="'.$title.'" onclick="myFunction('.$record->company_id.','.$record->status.')"><span><i class="'.$fa.'"></i></span><span>'.$name.'</span></a>';
				$messages[$i][7] = $trClass;
				$messages[$i][8] = $class;
				$i++;
				$s++;

			}
			$content .= ']';
			$final_data = json_encode($messages);
		}else{
			$final_data = [];
			$total_records = 0;
			$records_filtered = 0;
		}
		echo '{"draw":'.$_GET['draw'].',"recordsTotal":'.$total_records.',"recordsFiltered":'.$records_filtered.',"data":'.$final_data.'}';
		die;
	}

	public function company_form($id='') {
		$userType=LoginUserDetails('role_id');
		$ci =& get_instance();
		$siteLang = $ci->session->userdata('language');
		if($userType == 1){
			if((isset($_POST['save'])) && $id=='') {
				$checkCompany = $this->admin_model->check_company($_POST['company_name']);
				if(!$checkCompany) {
					if($this->save_company()) {
						set_global_messages($this->lang->line('company_suc_mesg'),'success');
						redirect('admin/company_section');
					}
				} else {
					set_global_messages($this->lang->line('company_already_exist'),'error');
					redirect('admin/company_section');
				}
			} else if(isset($_POST['save'])) {
				if($this->save_company('update',$id)) {
					set_global_messages($this->lang->line('company_updated_success_msg'),'success');
					redirect('admin/company_section');
				}
			}

			if($id) {
				$this->config->set_item('site_title', $this->lang->line('edit_company'));
				$data['records'] = $this->admin_model->get_company($id);
				$data['id']=$id;
				$data['site_language'] = $siteLang; 
			} else {
				$this->config->set_item('site_title', $this->lang->line('add_company'));
				$data = '';
				$data['id']='';
				$data['site_language'] = $siteLang; 
			}
				$this->admin_template->load('admin_template','comp_form',$data);
		}else{
			$this->admin_template->load('admin_template','not_authorize');
		}
	}

	private function save_company($type = 'insert',$id=0) {

		if($id != 0) {
			$_POST['id'] = $id;

		}
		$this->form_validation->set_rules('company_name','Company Name','required|trim|max_length[100]');
		$this->form_validation->set_rules('company_address','Company Address','required|trim');
		$this->form_validation->set_rules('company_registration_no','Company Registration No','required|trim');
		$this->form_validation->set_rules('min_jod_credit_limit','Minimum JOD credit limit','required|trim');
		//$this->form_validation->set_rules('status','Status','required|trim');
		if($this->form_validation->run() === FALSE) {
			return FALSE;
		}

		$data = array();
		$data['company_name']        = $this->input->post('company_name');
		$data['company_address']     = $this->input->post('company_address');
		$data['company_registration_no']      = $this->input->post('company_registration_no');
		$data['min_credit_limit']      = $this->input->post('min_jod_credit_limit');
		$data['status']      		 = 1;
		$data['updated_at'] = CURRENT_TIME;

		$response=$this->uploadProfileImage('company_logo');
        if(!empty($response) && $response['status']=="success") {
			$data['company_logo']=$response['imageName'];
		} else if(!empty($response)) {
			set_global_messages($response,'error');
			return FALSE;
		}
		$docUpload=$this->upload_document();
        if(!empty($docUpload) && $docUpload['status']=="success") {
			$data['contract_of_service']=$docUpload['docName'];
		} else if(!empty($docUpload)) {
			set_global_messages($docUpload,'error');
			return FALSE;
		}
		if($type == 'insert') {
			$data['created_at'] = CURRENT_TIME;
			$id = $this->admin_model->add_company($data);
		}
		else if ($type == 'update') {
			$return = $this->admin_model->update_company($id, $data);
		}
		return $id;

	}

	public function company_delete($id='') {
		$data['is_delete'] = '1';
		$data['updated_at'] = CURRENT_TIME;
		$this->admin_model->update_company($id, $data);
		
		set_global_messages($this->lang->line('company_deleted_success_msg'),'success');
		redirect('admin/company_section');
	}

	public function company_status($id='',$status='') {
		if($status == 1) {
			$data['status'] = 0;
		} else if($status == 0) {
			$data['status'] = 1;
		}
		$data['updated_at'] = CURRENT_TIME;
		$this->admin_model->update_company($id, $data);
		set_global_messages($this->lang->line('company_status_success_msg'),'success');
		redirect('admin/company_section');
	}

		/*****************************Company Section End*****************************/

		/*****************************Headquarters Section Start*****************************/
	public function headquater_section() {
		$userType=LoginUserDetails('role_id');
		$ci =& get_instance();
		$siteLang = $ci->session->userdata('language');
		if($userType == 1){
			$this->config->set_item('site_title', $this->lang->line('hq_list_page_title'));
			$data['records'] = $this->admin_model->get_headquater();
			$data['site_language'] = $siteLang; 
			$this->admin_template->load('admin_template','headquater',$data);
		}else{
			$this->admin_template->load('admin_template','not_authorize');
		}
	}

	public function getHqManagerByAjax(){
		$search = "";
		$search_set = 0;
		$dirs = "";
		$column_name  ="";
		if(isset($_GET['order'][0]['dir']) && !empty($_GET['order'][0]['dir'])) {
			$dirs = $_GET['order'][0]['dir'];

		}
		if($_GET['order'][0]['column'] == 0) {
			$dirs = 'ASC';

		}
		if(isset($_GET['order'][0]['column']) && !empty($_GET['order'][0]['column'])){
			if($_GET['order'][0]['column'] == 1) {
				$column_name = "jod_headquater_manager.first_name";
			}
			if($_GET['order'][0]['column'] == 2) {
				$column_name = "jod_companies.company_address";
			}
			if($_GET['order'][0]['column'] == 3) {
				$column_name = "jod_headquater_manager.email_id";
			}
		}
		if(isset($_GET['search']['value']) && !empty($_GET['search']['value'])) {
			$search = $_GET['search']['value'];
		    $search = trim($search);
		    $search_set = 1 ;
		}

		$records = $this->admin_model->get_headquater("",$search,$_GET['length'],$_GET['start'],$dirs,$column_name);

		$all_records = $this->admin_model->get_headquater();
		$total_records = count($all_records);
		$records_filtered = $total_records;
		if(!empty($records) || !empty($all_records)) {
			$messages      = array();
			$i=0;
			$s = $_GET['start'] + 1;
			if($search_set == 1){
				$records_filtered	= count($records);
			}
			$content='[';
			foreach($records as $record) {
				if($record->status==0){
					$class = "enable";
					$name = $this->lang->line('enable') ;
					$fa = "fa fa-check";
					$trClass = "gray-box";
					$title = "Click to enable record";
				}else if($record->status==1) {
					$class = "disable";
					$name = $this->lang->line('disable') ;
					$fa = "fa fa-ban";
					$trClass = "";
					$title = "Click to disable record";
				}
				$content .='[';
				$messages[$i][0] = $record->created_at;
				$messages[$i][1] =  ucfirst($record->first_name).' '.ucfirst($record->last_name);
				$messages[$i][2] =  $record->company_name;
				$messages[$i][3] = '<span>'.$record->contact_no.'</span><span>'.$record->email_id.'</span>';
				$messages[$i][4] = '<a href="'.base_url('admin/headquater_form/'.$record->hq_manager_id).'"><span><i class="fa fa-pencil"></i></span><span>'.$this->lang->line('edit').'</span></a>';
				$messages[$i][5] = '<a href="javascript:;" title="'.$title.'" onclick="myFunction('.$record->hq_manager_id.','.$record->user_accounts_id.','.$record->status.')"><span><i class="'.$fa.'"></i></span><span>'.$name.'</span></a>';
				$messages[$i][6] = $trClass;
				$messages[$i][7] = $class;
				$i++;
				$s++;
			}
			$content .= ']';
			$final_data = json_encode($messages);
		}else{
			$final_data = [];
			$total_records = 0;
			$records_filtered = 0;
		}
		echo '{"draw":'.$_GET['draw'].',"recordsTotal":'.$total_records.',"recordsFiltered":'.$records_filtered.',"data":'.$final_data.'}';
		die;
	}

	public function headquater_form($id=0,$companyId='') {
		$userType=LoginUserDetails('role_id');
		$ci =& get_instance();
		$siteLang = $ci->session->userdata('language');
		if($userType == 1){
			if((isset($_POST['save'])) && $id=='0') {
				if($this->save_headquater()) {
					set_global_messages($this->lang->line('hq_added_success_msg'),'success');
					redirect('admin/headquater_section');
				}
			} else if(isset($_POST['save'])) {
				if($this->save_headquater('update',$id)) {
					set_global_messages($this->lang->line('hq_updated_success_msg'),'success');
					redirect('admin/headquater_section');
				}
			}
			if($id) {
				$this->config->set_item('site_title', $this->lang->line('edit_hq_manager_title'));
				$data['records'] = $this->admin_model->get_headquater($id);
				$data['id']=$id;
			} else {
				$this->config->set_item('site_title', $this->lang->line('add_hq_manager_title'));
				$data = '';
				$data['id']='';
			}
			$companyArray = array();
			$selectedCompany = $this->admin_model->get_selected_company();
			foreach($selectedCompany as $company){
					$companyArray[] = $company->company_id;
			}
			$data['allcomapany'] = $this->admin_model->get_not_selected_company($companyArray);
			$data['addHqUser'] = $companyId;
			$data['site_language'] = $siteLang; 
			$this->admin_template->load('admin_template','headquater_form',$data);
		}else{
			$this->admin_template->load('admin_template','not_authorize');
		}
	}

	private function save_headquater($type = 'insert',$id=0) {
		$logo_url =  base_url()."templates/JOD/img/logo-small.png";
		//echo $type;die;
		$user_account_id = $this->input->post('user_account_id');
		$this->form_validation->set_rules('company_id','Company Name','required|trim');
		$this->form_validation->set_rules('first_name','First Name','required|trim|max_length[100]');
		$this->form_validation->set_rules('last_name','Last Name','required|trim|max_length[100]');
		$this->form_validation->set_rules('job_title','Job Title','required|trim|max_length[100]');
		$this->form_validation->set_rules('contact_no','Contact No','required|trim|numeric|min_length[8]|max_length[8]');
		//$this->form_validation->set_rules('status','Status','required|trim');
		$this->form_validation->set_rules('email_id','Work Email','required|trim|check_email_exist['.$user_account_id.']|valid_email');

		if($this->form_validation->run() === FALSE) {
			return FALSE;
		}
		$data = array();
		$data['company_id']        	= $this->input->post('company_id');
		$data['first_name']     	= $this->input->post('first_name');
		$data['last_name']      	= $this->input->post('last_name');
		$data['job_title']     		= $this->input->post('job_title');
		$data['contact_no']      	= '+65'.$this->input->post('contact_no');
		$data['status']      		= 1;
		$data['updated_at'] 		= CURRENT_TIME;
		$data['email_id']      		= $this->input->post('email_id');

		/*for user account start */
		$userAccount['role_id'] 	= 2;
		$userAccount['status'] 		= 1;
		$userAccount['updated_at']  = CURRENT_TIME;
		$userAccount['updated_by']  = LoginUserDetails('userid');
		$userAccount['unique_id']   = $this->input->post('email_id');
		$userAccount['email_id']	= $this->input->post('email_id');
		/*for user account end*/
		$response=$this->uploadProfileImage('headquarter_manager');
        if(!empty($response) && $response['status']=="success")
        {
			$data['profile_image']	= $response['imageName'];
		}
		if($id != 0) {
			$_POST['id'] = $id;
		}
		if($type == 'insert') {
			$password = $data['contact_no'];
			$userAccount['password'] 	= md5($password);
			$userAccount['created_at'] 	= CURRENT_TIME;
			$data['user_accounts_id'] 	= $this->admin_model->add_account($userAccount);
			$data['password'] 			= md5($password);
			$data['created_at'] 		= CURRENT_TIME;
			$id = $this->admin_model->add_headquater($data);
			$emailData= array();
			$email_template = get_email_template('account_creation');
			$emailData['recieverId'] 	= $this->input->post('email_id');
			$emailData['subject'] 		= $email_template->subject;
			$emailData['template'] 		= 'email';
			$emailData['fname']			= $this->input->post('first_name');
			$companyName = $this->admin_model->get_company($this->input->post('company_id'))->company_name;
			$emailData['body'] 			= 'You are added by Jobs on demand\'s admin as a <b>Headquarter manager</b> of <b>'.$companyName.'</b> and your login details are given below :-';
			$emailData['loginId'] 		= $this->input->post('email_id');
			$emailData['password'] 		= $password;

			$body = $email_template->body;
			$searchArray = array("{user_name}","{added_by}","{user_role}","{login_url}","{login_id}","{password}","{site_logo}");
			$replaceArray = array(ucfirst($emailData['fname']),'Jobs on demand\'s admin','Headquarter manager of '.$companyName,base_url(),$emailData['loginId'],$password,$logo_url);
			$emailData['message']=str_replace($searchArray, $replaceArray, $body);
			send_email($emailData);
		} else if ($type == 'update') {
			$aid = $this->input->post('user_account_id');
			$this->admin_model->update_account($aid,$userAccount);
			$return = $this->admin_model->update_headquater($id, $data);
		}
		return $id;
	}

	public function headquarter_delete($id='') {
		$data['is_delete'] 	= '1';
		$data['updated_at'] = CURRENT_TIME;
		$this->admin_model->update_headquater($id, $data);
		set_global_messages($this->lang->line('hq_deleted_success_msg'),'success');
		redirect('admin/headquater_section');
	}

	public function headquarter_status($id='',$userAccountsId='',$status='') {
		if($status == 1) {
			$data['status'] = 0;
		} else if($status == 0) {
			$data['status'] = 1;
		}
		$data['updated_at'] = CURRENT_TIME;
		$this->admin_model->update_account($userAccountsId,$data);
		$this->admin_model->update_headquater($id, $data);
		set_global_messages($this->lang->line('hq_status_updated_success_msg'),'success');
		redirect('admin/headquater_section');
	}
		/*****************************Headquater Section End*****************************/

		/*****************************Outlet Section Start*****************************/
	public function outlet_section() {
		$userType=LoginUserDetails('role_id');
		$ci =& get_instance();
		$site_language = $ci->session->userdata('language');
		if($userType == 1 || $userType == 5 ||  $userType == 2 ){
			$this->config->set_item('site_title', $this->lang->line('outlets'));
			$data = array();
			$data["site_language"] =  $site_language; 
			$this->admin_template->load('admin_template','outlet',$data);
		}else{
			$this->admin_template->load('admin_template','not_authorize');
		}
	}

	public function getOutletListByAjax(){
		$userType=LoginUserDetails('role_id');
		$search = "";
		$search_set = 0;
		$dirs = "";
		$column_name = "";
		if(isset($_GET['order'][0]['dir']) && !empty($_GET['order'][0]['dir'])) {
			$dirs = $_GET['order'][0]['dir'];

		}
		if($_GET['order'][0]['column'] == 0) {
			$dirs = 'ASC';

		}else{
			if($_GET['order'][0]['column'] == 1){
				$column_name = "jod_outlets.outlet_name";
			}elseif($_GET['order'][0]['column'] == 2){
				$column_name = "jod_outlets.outlet_address";
			}
			elseif($_GET['order'][0]['column'] == 3){
				if($userType == 1){
					$column_name = "jod_companies.company_name";
				}
			}
		}
		if(isset($_GET['search']['value']) && !empty($_GET['search']['value'])) {
			$search = $_GET['search']['value'];
		    $search = trim($search);
		    $search_set = 1 ;
		}
		$records = $this->admin_model->get_outlet("",$search,$_GET['length'],$_GET['start'],$dirs,$column_name);
		$all_records =  $this->admin_model->get_outlet();
		$total_records = count($all_records);
		$records_filtered = $total_records;

		if(!empty($records) || !empty($all_records)) {
			$messages      = array();
			$i=0;
			$s = $_GET['start'] + 1;
			if($search_set == 1){
				$records_filtered	= count($records);
			}
			$content='[';
			foreach($records as $record) {
				if($record->status==0) {
					$class = "enable";
					$name = $this->lang->line('enable');
					$fa = "fa fa-check";
					$trClass = "gray-box";
					$title = "Click to enable record";
				} else if($record->status==1) {
					$class = "disable";
					$name = $this->lang->line('disable');
					$fa = "fa fa-ban";
					$trClass = "";
					$title = "Click to disable record";
				}
				$content .='[';
				$messages[$i][0] = $record->created_at;
				$messages[$i][1] = ucfirst($record->outlet_name);
				$messages [$i][2] =  $record->outlet_address;
				if($userType == 2){ // HQ Manager
					// JOD credit icon
					$messages[$i][3] = '<a href="'.base_url('admin/assign_credits_to_outlet/'.$record->outlet_id).'"><span><img src="'.base_url('templates/JOD/img').'/jod-credit-icon-white.png"></span><span>'.$this->lang->line('credits').'</span></a>';
					// HQ user
					$messages[$i][4] = '<a href="'.base_url('admin/outlet_manager_form/0/'.$record->outlet_id).'"><span><i class="fa fa-plus"></i></span><span>'.$this->lang->line('manager').'</span></a>';
					// edit pencil
					$messages[$i][5] = '<a href="'.base_url('admin/outlet_form/'.$record->outlet_id).'"><span><i class="fa fa-pencil"></i></span><span>'.$this->lang->line('edit').'</span></a>';
					// enable disable button
					$messages[$i][6] = '<a href="javascript:;" title="'.$title.'" onclick="myFunction('.$record->outlet_id.','.$record->status.')"><span><i class="'.$fa.'"></i></span><span>'.$name.'</span></a>';
				}elseif($userType == 5){ // area manager
					//echo "sss"; die;
					$messages[$i][3] = '<a href="'.base_url('admin/outlet_profile/'.$record->outlet_id).'"><span><i class="fa fa-eye"></i></span><span>'.$this->lang->line('view').'</span></a>';
				}else{
					// Company Name
					$messages[$i][3] = ucfirst($record->company_name);
				}
				$messages[$i][7] = $trClass;
				$messages[$i][8] = $class;
				$messages[$i][9] = $userType;
				$i++;
				$s++;

			}
			$content .= ']';
			$final_data = json_encode($messages);
		}else{
			$final_data = '{}';
			$total_records = 0;
			$records_filtered = 0;
		}
		echo '{"draw":'.$_GET['draw'].',"recordsTotal":'.$total_records.',"recordsFiltered":'.$records_filtered.',"data":'.$final_data.'}';
		die;

	}

	public function outlet_form($id='') {
		$userType=LoginUserDetails('role_id');
		$ci =& get_instance();
		$site_language = $ci->session->userdata('language');
		if($userType == 2 ){
			if((isset($_POST['save'])) && $id=='') {
				if($this->save_outlet()) {
					set_global_messages($this->lang->line('outlet_added_success_msg'),'success');
					redirect('admin/outlet_section');
				}
			} else if(isset($_POST['save'])) {
				if($this->save_outlet('update',$id)) {
					set_global_messages($this->lang->line('outlet_updated_success_msg'),'success');
					redirect('admin/outlet_section');
				}
			}
			if($id) {
				$this->config->set_item('site_title', $this->lang->line('edit')." ".$this->lang->line('outlet'));
				$data['records'] = $this->admin_model->get_outlet($id);
				$data['id']=$id;
			} else {
				$this->config->set_item('site_title', $this->lang->line('add')." ".$this->lang->line('outlet'));
				$data = '';
				$data['id']='';
			}
			$data['site_language'] = $site_language;
			$data['outletManagers'] = $this->admin_model->get_unassigns_outlet_manager();
			$data['areaManagers'] = $this->admin_model->get_enable_area_manager();
			$this->admin_template->load('admin_template','outlet_form',$data);
		}else{
			$this->admin_template->load('admin_template','not_authorize');
		}
	}

	private function save_outlet($type = 'insert',$id=0) {
		$checkOutlet = $this->admin_model->check_outlet($_POST['outlet_name'],$id);
		if($checkOutlet) {
			set_global_messages($this->lang->line('outlet_already_added'),'error');
			redirect('admin/outlet_section');
		}
		if($id != 0) {
			$_POST['id'] = $id;

		}
		$this->form_validation->set_rules('outlet_name','F&B Outlet Name','required|trim|max_length[100]');
		$this->form_validation->set_rules('outlet_address','F&B Outlet Address','required|trim');
		//$this->form_validation->set_rules('status','Status','required|trim');
		if($this->form_validation->run() === FALSE) {
			return FALSE;
		}
		$data = array();
		$data['outlet_name']        = $this->input->post('outlet_name');
		$data['outlet_address']     = $this->input->post('outlet_address');
		if($this->input->post('is_approval_required')) {
			$data['is_approval_required']     = '1';
		} else {
			$data['is_approval_required']     = '0';
		}
		if($this->input->post('is_template_required')) {
			$data['is_template_required']     = '1';
		} else {
			$data['is_template_required']     = '0';
		}
		if($this->input->post('job_created_by_OM')){
			$data["job_created_by_outlet_manager"] = '1';
		}else{
			$data["job_created_by_outlet_manager"] = '0';
		}

		$data['company_id'] = LoginUserDetails('company_id');
		$data['area_manager_id']     = $this->input->post('area_manager_id');
		$data['updated_at'] = CURRENT_TIME;

		$response=$this->uploadProfileImage('outlet_logo');
        if(!empty($response) && $response['status']=="success")
        {
			$data['outlet_logo']=$response['imageName'];
		}
		if($type == 'insert') {
			$outlet_manager_id  = $this->input->post('outlet_manager_id');
			$data['status']      		 = 1;
			$data['created_at'] = CURRENT_TIME;
			$id = $this->admin_model->add_outlet($data);
			$outletData['outlet_id'] = $id;
			$this->admin_model->update_outlet_manager($outlet_manager_id, $outletData);
		} else if ($type == 'update') {
			$return = $this->admin_model->update_outlet($id, $data);
		}
		return $id;

	}

	public function outlet_delete($id='') {
		$data['is_delete'] = '1';
		$data['updated_at'] = CURRENT_TIME;
		$this->admin_model->update_outlet($id, $data);
		set_global_messages($this->lang->line('outlet_deleted_successfully'),'success');
		redirect('admin/outlet_section');
	}

	public function outlet_status($id='',$status='') {
		if($status == 1) {
			$data['status'] = 0;
		} else if($status == 0) {
			$data['status'] = 1;
		}
		$data['updated_at'] = CURRENT_TIME;
		$this->admin_model->update_outlet($id, $data);
		set_global_messages($this->lang->line('outlet_status_successfully'),'success');
		redirect('admin/outlet_section');
	}

	public function outlet_profile($outlet_id="") {
		$data = array();
		$data['outlet_details'] = getOutletDetail($outlet_id);
		$this->admin_template->load('admin_template','outlet_profile',$data);
	}

		/*****************************Outlet Section End*****************************/

		/*****************************Outlet Manager Section Start*****************************/

	public function outlet_manager_section() {
		$ci =& get_instance();
		$site_language = $ci->session->userdata('language');
		$userType=LoginUserDetails('role_id');
		if($userType == 2){
			$this->config->set_item('site_title', $this->lang->line('om_list_page_title'));
			$data = array();
			//$data['records'] = $this->admin_model->get_outlet_manager();
			$data['site_language'] = $site_language;
			$this->admin_template->load('admin_template','outlet_manager',$data);
		}else{
			$this->admin_template->load('admin_template','not_authorize');
		}
	}

	public function outletManagerListByAjax(){
		$search = "";
		$search_set = 0;
		$dirs = "";
		$column_name  ="";
		if(isset($_GET['order'][0]['dir']) && !empty($_GET['order'][0]['dir'])) {
			$dirs = $_GET['order'][0]['dir'];

		}
		if($_GET['order'][0]['column'] == 0) {
			$dirs = 'ASC';

		}
		if(isset($_GET['order'][0]['column']) && !empty($_GET['order'][0]['column'])){
			if($_GET['order'][0]['column'] == 1) {
				$column_name = "jod_outlet_managers.first_name";
			}
			if($_GET['order'][0]['column'] == 2) {
				$column_name = "jod_outlets.outlet_name";
			}
			if($_GET['order'][0]['column'] == 3) {
				$column_name = "jod_outlet_managers.email_id";
			}
		}
		if(isset($_GET['search']['value']) && !empty($_GET['search']['value'])) {
			$search = $_GET['search']['value'];
		    $search = trim($search);
		    $search_set = 1 ;
		}
		$records = $this->admin_model->get_outlet_manager("",$search,$_GET['length'],$_GET['start'],$dirs,$column_name);
		$all_records = $this->admin_model->get_outlet_manager();
		$total_records = count($all_records);
		$records_filtered = $total_records;
		if(!empty($records) || !empty($all_records)) {
			$messages      = array();
			$i=0;
			$s = $_GET['start'] + 1;
			if($search_set == 1){
				$records_filtered	= count($records);
			}
			$content='[';
			foreach($records as $record) {
				if($record->status==0){
					$class = "enable";
					$name = $this->lang->line('enable'); 
					$fa = "fa fa-check";
					$trClass = "gray-box";
					$title = "Click to enable record";
				}else if($record->status==1) {
					$class = "disable";
					$name = $this->lang->line('disable'); 
					$fa = "fa fa-ban";
					$trClass = "";
					$title = "Click to disable record";
				}
				$content .='[';
				$messages[$i][0] = $record->created_at;
				$messages[$i][1] =  ucfirst($record->first_name).' '.ucfirst($record->last_name);
				$messages[$i][2] =  ($record->outlet_name != "" ) ? $record->outlet_name : "No outlet assigned" ;
				$messages[$i][3] = '<span>'.$record->contact_no.'</span><span>'.$record->email_id.'</span>';
				$messages[$i][4] = '<a href="'.base_url('admin/outlet_manager_form/'.$record->outlet_manager_id).'"><span><i class="fa fa-pencil"></i></span><span>'.$this->lang->line('edit').'</span></a>';
				$messages[$i][5] = '<a href="javascript:;" title="'.$title.'" onclick="myFunction('.$record->outlet_manager_id.','.$record->user_accounts_id.','.$record->status.')"><span><i class="'.$fa.'"></i></span><span>'.$name.'</span></a>';
				$messages[$i][6] = $trClass;
				$messages[$i][7] = $class;
				$i++;
				$s++;
			}
			$content .= ']';
			$final_data = json_encode($messages);
		}else{
			$final_data = "{}";
			$total_records = 0;
			$records_filtered = 0;
		}
		echo '{"draw":'.$_GET['draw'].',"recordsTotal":'.$total_records.',"recordsFiltered":'.$records_filtered.',"data":'.$final_data.'}';
		die;

	}
	
	public function outlet_manager_form($id=0,$outletId='') {
		$ci =& get_instance();
		$site_language = $ci->session->userdata('language');
		$userType=LoginUserDetails('role_id');
		if($userType == 2){
			if((isset($_POST['save'])) && $id=='0') {
				if($this->save_outlet_manager()) {
					set_global_messages($this->lang->line('om_added_successfully'),'success');
					redirect('admin/outlet_manager_section');
				}
			} else if(isset($_POST['save'])) {
				if($this->save_outlet_manager('update',$id)) {
					set_global_messages($this->lang->line('om_updated_successfully'),'success');
					redirect('admin/outlet_manager_section');
				}
			}
			if($id) {
				$this->config->set_item('site_title', $this->lang->line('edit_outlet_manager'));
				$data['records'] = $this->admin_model->get_outlet_manager($id);
				$data['id']=$id;
			} else {
				$this->config->set_item('site_title', $this->lang->line('add_outlet_manager'));
				$data = '';
				$data['id']='';
			}
			$data['title']='outlets';
			$data['site_language'] = $site_language; 
			$data['outlets'] = $this->admin_model->get_enable_outlet();
			$data['addManager'] = $outletId;
			$this->admin_template->load('admin_template','outlet_manager_form',$data);
		}else{
			$this->admin_template->load('admin_template','not_authorize');
		}
	}
	private function save_outlet_manager($type = 'insert',$id=0) {
		$logo_url =  base_url()."templates/JOD/img/logo-small.png";
		$user_account_id = $this->input->post('user_account_id');
		//$this->form_validation->set_rules('outlet_id','F&B Outlet','required|trim');
		$this->form_validation->set_rules('first_name','First Name','required|trim|max_length[100]');
		$this->form_validation->set_rules('last_name','Last Name','required|trim|max_length[100]');
		$this->form_validation->set_rules('job_title','Job Title','required|trim|max_length[100]');
		$this->form_validation->set_rules('contact_no','Contact No','required|trim|numeric|min_length[8]|max_length[8]');
		//$this->form_validation->set_rules('status','Status','required|trim');
		$this->form_validation->set_rules('email_id','Work Email','required|trim|check_email_exist['.$user_account_id.']|valid_email');

		if($this->form_validation->run() === FALSE) {
			return FALSE;
		}
		$data = array();
		$data['outlet_id']        = $this->input->post('outlet_id');
		$data['company_id']     = LoginUserDetails('company_id');
		$data['first_name']     = $this->input->post('first_name');
		$data['last_name']      = $this->input->post('last_name');
		$data['job_title']      = $this->input->post('job_title');
		$data['contact_no']      = '+65'.$this->input->post('contact_no');
		$data['email_id']      = $this->input->post('email_id');
		$data['status']      		 = 1;
		$data['updated_at'] = CURRENT_TIME;
		/*for user account start */
		$userAccount['role_id'] = 3;
		$userAccount['status']  = 1;
		$userAccount['updated_at']  = CURRENT_TIME;
		$userAccount['updated_by']  = LoginUserDetails('userid');
		$userAccount['unique_id']   = $this->input->post('email_id');
		$userAccount['email_id']	= $this->input->post('email_id');
		/*for user account end*/

		if($id != 0) {
			$_POST['id'] = $id;
		}
		if($type == 'insert') {
			$password = $data['contact_no'];
			$userAccount['password'] = md5($password);
			$userAccount['created_at'] = CURRENT_TIME;
			$data['user_accounts_id'] = $this->admin_model->add_account($userAccount);
			$data['password'] = md5($password);
			$data['created_at'] = CURRENT_TIME;
			$id = $this->admin_model->add_outlet_manager($data);
			$emailData= array();
			$email_template = get_email_template('account_creation');
			$emailData['recieverId'] = $this->input->post('email_id');
			$emailData['subject'] = $email_template->subject;
			$emailData['template'] = 'email';
			$emailData['fname']				= $this->input->post('first_name');
			$companyName = $this->admin_model->get_company(LoginUserDetails('company_id'))->company_name;
			$outletName = $this->admin_model->get_outlet($this->input->post('outlet_id'))->outlet_name;
			if($this->input->post('outlet_id') != '0') {
				$emailData['body'] = 'You are added by Headquarter manager of '.$companyName.' as a F&B Outlet Manager for '.$outletName.' and your login details are given below :-</p>';
			} else {
				$emailData['body'] = 'You are added by Headquarter manager of '.$companyName.' as a F&B Outlet Manager, your login details are given below :-</p>';
			}
			$emailData['loginId'] = $this->input->post('email_id');
			$emailData['password'] = $password;
			$body = $email_template->body;
			$searchArray = array("{user_name}","{added_by}","{user_role}","{login_url}","{login_id}","{password}","{site_logo}");
			$replaceArray = array(ucfirst($emailData['fname']),'Headquarter manager of '.$companyName,'F&B Outlet Manager',base_url(),$emailData['loginId'],$password,$logo_url);
			$emailData['message']=str_replace($searchArray, $replaceArray, $body);

			send_email($emailData);
		} else if ($type == 'update') {
			$aid = $this->input->post('user_account_id');
			$this->admin_model->update_account($aid,$userAccount);
			$return = $this->admin_model->update_outlet_manager($id, $data);
		}
		return $id;
	}

	public function outlet_manager_status($id='',$userAccountsId='',$status='') {
		if($status == 1) {
			$data['status'] = 0;
		} else if($status == 0) {
			$data['status'] = 1;
		}
		$data['updated_at'] = CURRENT_TIME;
		$this->admin_model->update_account($userAccountsId,$data);
		$this->admin_model->update_outlet_manager($id, $data);
		set_global_messages($this->lang->line('om_status_updated_successfully'),'success');
		redirect('admin/outlet_manager_section');
	}

	public function check_outlet_man($userAccountsId='') {
		echo $this->admin_model->job_assigned_to_outlet_manager($userAccountsId); // return the no of job not complete
		//echo 1;
	}

	public function unassigns_outlet_manager($id='',$outletId='') {
		$data['outlet_id'] = '0';
		$this->admin_model->update_outlet_manager($id,$data);
		set_global_messages($this->lang->line('om_unassigned_successfully'),'success');
		redirect('admin/outlet_form/'.$outletId);
	}

		/*****************************Outlet Manager Section End*****************************/

		/*****************************Area Manager Section Start*****************************/
	public function area_manager_section() {
		$ci =& get_instance();
		$site_language = $ci->session->userdata('language');
		$userType=LoginUserDetails('role_id');
		if($userType == 2){
			$this->config->set_item('site_title', $this->lang->line('area_manager'));
			$data = array();
			$data['site_language'] = $site_language;
			//$data['records'] = $this->admin_model->get_area_manager();
			$this->admin_template->load('admin_template','area_manager',$data);
		}else{
			$this->admin_template->load('admin_template','not_authorize');
		}
	}

	public function areaManagerListByAjax(){
		$search = "";
		$search_set = 0;
		$dirs = "";
		$column_name  ="";
		if(isset($_GET['order'][0]['dir']) && !empty($_GET['order'][0]['dir'])) {
			$dirs = $_GET['order'][0]['dir'];

		}
		if($_GET['order'][0]['column'] == 0) {
			$dirs = 'ASC';

		}
		if(isset($_GET['order'][0]['column']) && !empty($_GET['order'][0]['column'])){
			if($_GET['order'][0]['column'] == 1) {
				$column_name = "first_name";
			}
			if($_GET['order'][0]['column'] == 3) {
				$column_name = "email_id";
			}
		}
		if(isset($_GET['search']['value']) && !empty($_GET['search']['value'])) {
			$search = $_GET['search']['value'];
		    $search = trim($search);
		    $search_set = 1 ;
		}

		$records = $this->admin_model->get_area_manager("",$search,$_GET['length'],$_GET['start'],$dirs,$column_name);
		//print_r($records); die;
		$all_records = $this->admin_model->get_area_manager();
		$total_records = count($all_records);
		$records_filtered = $total_records;
		if(!empty($records) || !empty($all_records)) {
			$messages = array();
			$i=0;
			$s = $_GET['start'] + 1;
			if($search_set == 1){
				$records_filtered	= count($records);
			}
			$content='[';
			foreach($records as $record) {
				if($record->status==0) {
					$class = "enable";
					$name =  $this->lang->line('enable');
					$fa = "fa fa-check";
					$trClass = "gray-box";
					$title = "Click to enable record";
				} else if($record->status==1) {
					$class = "disable";
					$name =  $this->lang->line('disable');
					$fa = "fa fa-ban";
					$trClass = "";
					$title = "Click to disable record";
				}
				$content .='[';
				$messages[$i][0] = $record->created_at;
				$messages[$i][1] =  ucfirst($record->first_name).' '.ucfirst($record->last_name);
				$outlets = get_outlets_by_area_manager_id($record->area_manager_id);
				if(!empty($outlets)) {
					$str = array();
					foreach($outlets as $outlet) {
						array_push($str, $outlet->outlet_name);
						//echo $outlet->outlet_name.", ";
					}
					$outlets_str =  implode(', ', $str);
				} else {
					$outlets_str =  "No Outlet Assign";
				}
				$messages[$i][2] =  $outlets_str;
				$messages[$i][3] =  '<span>'.$record->contact_no.'</span><span>'.$record->email_id.'</span>';

				$messages[$i][4] = '<a href="'.base_url('admin/area_manager_form/'.$record->area_manager_id).'"><span><i class="fa fa-pencil"></i></span><span>'.$this->lang->line('edit').'</span></a>';
				$messages[$i][5] = '<a href="javascript:;" title="'.$title.'" onclick="myFunction('.$record->area_manager_id.','.$record->user_accounts_id.','.$record->status.')"><span><i class="'.$fa.'"></i></span><span>'.$name.'</span></a>';
				$messages[$i][6] = $trClass;
				$messages[$i][7] = $class;
				$i++;
				$s++;

			}
			$content .= ']';
			$final_data = json_encode($messages);
		}else{
			$final_data = "{}";
			$total_records = 0;
			$records_filtered = 0;
		}
		echo '{"draw":'.$_GET['draw'].',"recordsTotal":'.$total_records.',"recordsFiltered":'.$records_filtered.',"data":'.$final_data.'}';
		die;

	}

	public function area_manager_form($id=0) {
		$userType=LoginUserDetails('role_id');
		$ci =& get_instance();
		$site_language = $ci->session->userdata('language');
		if($userType == 2){
			if((isset($_POST['save'])) && $id=='0') {
				if($this->save_area_manager()) {
					set_global_messages($this->lang->line('am_added_successfully'),'success');
					redirect('admin/area_manager_section');
				}
			} else if(isset($_POST['save'])) {
				if($this->save_area_manager('update',$id)) {
					set_global_messages($this->lang->line('am_updated_successfully'),'success');
					redirect('admin/area_manager_section');
				}
			}
			if($id) {
				$this->config->set_item('site_title', $this->lang->line('edit')." ".$this->lang->line('area_manager'));
				$data['records'] = $this->admin_model->get_area_manager($id);
				$data['id']=$id;
			} else {
				$this->config->set_item('site_title', $this->lang->line('add')." ".$this->lang->line('area_manager'));
				$data = '';
				$data['id']='';
			}
			$data['site_language'] = $site_language;
			$this->admin_template->load('admin_template','area_manager_form',$data);
		}else{
			$this->admin_template->load('admin_template','not_authorize');
		}
	}



	private function save_area_manager($type = 'insert',$id=0) {
		$logo_url =  base_url()."templates/JOD/img/logo-small.png";
		$user_account_id = $this->input->post('user_account_id');
		$this->form_validation->set_rules('first_name','First Name','required|trim|max_length[100]');
		$this->form_validation->set_rules('last_name','Last Name','required|trim|max_length[100]');
		$this->form_validation->set_rules('job_title','Job Title','required|trim|max_length[100]');
		$this->form_validation->set_rules('contact_no','Contact No','required|trim|numeric|min_length[8]|max_length[8]');
		$this->form_validation->set_rules('email_id','Work Email','required|trim|check_email_exist['.$user_account_id.']|valid_email');

		if($this->form_validation->run() === FALSE) {
			return FALSE;
		}
		$data = array();
		$data['company_id']     = LoginUserDetails('company_id');
		$data['first_name']     = $this->input->post('first_name');
		$data['last_name']      = $this->input->post('last_name');
		$data['job_title']      = $this->input->post('job_title');
		$data['contact_no']     = '+65'.$this->input->post('contact_no');
		$data['email_id']      = $this->input->post('email_id');
		$data['status']      		 = 1;
		$data['updated_at'] = CURRENT_TIME;
		/*for user account start */
		$userAccount['role_id'] = 5;
		$userAccount['status']  = 1;
		$userAccount['updated_at']  = CURRENT_TIME;
		$userAccount['updated_by']  = LoginUserDetails('userid');
		$userAccount['unique_id']      = $this->input->post('email_id');
		$userAccount['email_id']= $this->input->post('email_id');
		/*for user account end*/
		if($id != 0) {
			$_POST['id'] = $id;
		}
		if($type == 'insert') {
			$password = $data['contact_no'] ;
			$userAccount['password'] = md5($password);
			$userAccount['created_at'] = CURRENT_TIME;
			$data['user_accounts_id'] = $this->admin_model->add_account($userAccount);
			$data['password'] = md5($password);
			$data['created_at'] = CURRENT_TIME;
			$id = $this->admin_model->add_area_manager($data);
			$emailData= array();
			$email_template = get_email_template('account_creation');
			$emailData['recieverId'] = $this->input->post('email_id');
			$emailData['subject'] = $email_template->subject;
			$emailData['template'] = 'email';
			$emailData['fname']				= $this->input->post('first_name');
			$companyName = $this->admin_model->get_company(LoginUserDetails('company_id'))->company_name;
			$emailData['body'] = 'You are added by Headquarter manager of '.$companyName.' as a Area Manager and your login details are given below :-</p>';
			$emailData['loginId'] = $this->input->post('email_id');
			$emailData['password'] = $password;

			$email_template = get_email_template('account_creation');
			$body = $email_template->body;
			$searchArray = array("{user_name}","{added_by}","{user_role}","{login_url}","{login_id}","{password}","{site_logo}");
			$replaceArray = array(ucfirst($emailData['fname']),'Headquarter manager of '.$companyName,'Area Manager',base_url(),$emailData['loginId'],$password,$logo_url);
			$emailData['message']=str_replace($searchArray, $replaceArray, $body);

			send_email($emailData);
		} else if ($type == 'update') {
			$aid = $this->input->post('user_account_id');
			$this->admin_model->update_account($aid,$userAccount);
			$return = $this->admin_model->update_area_manager($id, $data);
		}
		return $id;
	}

	public function area_manager_status($id='',$userAccountsId='',$status='') {
		if($status == 1) {
			$data['status'] = 0;
		} else if($status == 0) {
			$data['status'] = 1;
		}
		$data['updated_at'] = CURRENT_TIME;
		$this->admin_model->update_account($userAccountsId,$data);
		$this->admin_model->update_area_manager($id, $data);
		set_global_messages($this->lang->line('am_status_updated_successfully'),'success');
		redirect('admin/area_manager_section');
	}

	public function outlet_save_area_manager() { //on outlet add/edit with ajax request
		$data['company_id']     = LoginUserDetails('company_id');
		$data['first_name']     = $this->input->post('first_name');
		$data['last_name']      = $this->input->post('last_name');
		$data['job_title']      = $this->input->post('job_title');
		$data['contact_no']     = '+65'.$this->input->post('contact_no');
		$data['status']      		 = 1;
		$data['updated_at'] = CURRENT_TIME;
		if(check_email_exist('0',$this->input->post('email_id')) > 0) {
			echo 0;
			return ;
		}
		/*for user account start */
		$userAccount['role_id'] = 5;
		$userAccount['status']  = 1;
		$userAccount['updated_at']  = CURRENT_TIME;
		$userAccount['updated_by']  = LoginUserDetails('userid');
		$data['email_id']      = $this->input->post('email_id');
		$userAccount['unique_id']      = $this->input->post('email_id');
		$userAccount['email_id']= $this->input->post('email_id');
		$password = $data['contact_no'];
		$userAccount['password'] = md5($password);
		$userAccount['created_at'] = CURRENT_TIME;
		$data['user_accounts_id'] = $this->admin_model->add_account($userAccount);
		$data['password'] = md5($password);
		$data['created_at'] = CURRENT_TIME;
		$id = $this->admin_model->add_area_manager($data);
		$emailData= array();
		$emailData['recieverId'] = $this->input->post('email_id');
		$emailData['subject'] = 'JOBS ON DEMANDS\'s Account Creation';
		$emailData['template'] = 'email';
		$emailData['fname']				= $this->input->post('first_name');
		$companyName = $this->admin_model->get_company(LoginUserDetails('company_id'))->company_name;
		$emailData['body'] = 'You are added by Headquarter manager of '.$companyName.' as a Area Manager and your login details are given below :-</p>';
		$emailData['loginId'] = $this->input->post('email_id');
		$emailData['password'] = $password;

		$email_template = get_email_template('account_creation');
		$body = $email_template->body;
		$searchArray = array("{user_name}","{added_by}","{user_role}","{login_url}","{login_id}","{password}");
		$replaceArray = array(ucfirst($emailData['fname']),'Headquarter manager of '.$companyName,'Area Manager',base_url(),$emailData['loginId'],$password);
		$emailData['message']=str_replace($searchArray, $replaceArray, $body);

		send_email($emailData);
		$data = $this->admin_model->get_area_manager($id);
		echo $data->area_manager_id.'#BREAKTHIS#'.$data->first_name.' '.$data->last_name;
	}
		/*****************************Area Manager Section End*****************************/

		/*****************************Job Type Section Start*****************************/
	public function job_type_section() {
		
		$userType=LoginUserDetails('role_id');
		if($userType == 1){
			$this->config->set_item('site_title', $this->lang->line('job_type'));
			$this->admin_template->load('admin_template','job_type');
		}else{
			$this->admin_template->load('admin_template','not_authorize');
		}
	}

	public function getJobTypesByAjax(){

		$search = "";
		$search_set = 0;
		$dirs = "";
		if(isset($_GET['order'][0]['dir']) && !empty($_GET['order'][0]['dir'])) {
			$dirs = $_GET['order'][0]['dir'];

		}
		if($_GET['order'][0]['column'] == 0) {
			$dirs = 'ASC';

		}
		if(isset($_GET['search']['value']) && !empty($_GET['search']['value'])) {
			$search = $_GET['search']['value'];
		    $search = trim($search);
		    $search_set = 1 ;
		}
		$records = $this->admin_model->get_job_type("",$search,$_GET['length'],$_GET['start'],$dirs);
		$all_records = $this->admin_model->get_job_type('');
		$total_records = count($all_records);
		$records_filtered = $total_records;
		if(!empty($records) || !empty($all_records)) {
			$messages      = array();
			$i=0;
			$s = $_GET['start'] + 1;
			if($search_set == 1){
				$records_filtered	= count($records);
			}
			$content='[';
			foreach($records as $record) {
				if($record->status==0) {
					$class = "enable";
					$name = $this->lang->line('enable');
					$fa = "fa fa-check";
					$trClass = "gray-box";
					$title = "Click to enable record";
				} else if($record->status==1) {
					$class = "disable";
					$name = $this->lang->line('disable');
					$fa = "fa fa-ban";
					$trClass = "";
					$title = "Click to disable record";
				}
				$content .='[';
				$messages[$i][0] = $record->created_at;
				$messages[$i][1] = (strlen($record->name) > 100) ? ucfirst(substr($record->name,0,75)).'...' : ucfirst($record->name);
				$messages[$i][2] = '<a href="'.base_url('admin/job_type_form/'.$record->job_role_id).'"><span><i class="fa fa-pencil"></i></span><span>'.$this->lang->line('edit').'</span></a>';
				$messages[$i][3] = '<a href="javascript:;" title="'.$title.'" onclick="myFunction('.$record->job_role_id.','.$record->status.')"><span><i class="'.$fa.'"></i></span><span>'.$name.'</span></a>';
				$messages[$i][4] = $trClass;
				$messages[$i][5] = $class;
				$i++;
				$s++;

			}
			$content .= ']';
			$final_data = json_encode($messages);
		}else{
			$final_data = [];
			$total_records = 0;
			$records_filtered = 0;
		}
		echo '{"draw":'.$_GET['draw'].',"recordsTotal":'.$total_records.',"recordsFiltered":'.$records_filtered.',"data":'.$final_data.'}';
		die;


	}

	public function job_type_form($id='') {
		$userType=LoginUserDetails('role_id');
		if($userType == 1){
			if((isset($_POST['save'])) && $id=='') {
				if($this->save_job_type()) {
					set_global_messages($this->lang->line('job_type_added_successfully'),'success');
					redirect('admin/job_type_section');
				}
			} else {
				if(isset($_POST['save'])) {
					if($this->save_job_type('update',$id)) {
						set_global_messages($this->lang->line('job_type_updated_successfully'),'success');
						redirect('admin/job_type_section');
					}
				}
			}
			if($id) {
				$this->config->set_item('site_title', $this->lang->line('edit')." ".$this->lang->line('job_type'));
				$data['records'] = $this->admin_model->get_job_type($id);
				$data['id']=$id;
			} else {
				$this->config->set_item('site_title', $this->lang->line('add_new')." ".$this->lang->line('job_type'));
				$data = '';
				$data['id']='';
			}
			$this->admin_template->load('admin_template','job_type_form',$data);
		}else{
			$this->admin_template->load('admin_template','not_authorize');
		}
	}

	private function save_job_type($type = 'insert',$id=0) {
		$checkJobType = $this->admin_model->check_job_type($_POST['name'],$id);
		if($checkJobType) {
			set_global_messages($this->lang->line('job_type_already_added'),'error');
			redirect('admin/job_type_form/'.$id);
		}
		if($id != 0) {
			$_POST['id'] = $id;

		}
		$this->form_validation->set_rules('name','Job Type Name','required|trim|max_length[100]');
		if($this->form_validation->run() === FALSE) {
			return FALSE;
		}

		$data = array();
		$data['name']        = $this->input->post('name');
		$data['updated_at'] = CURRENT_TIME;

		if($type == 'insert') {
			$data['created_at'] = CURRENT_TIME;
			$data['approved']      		 = 1;
			$data['status']      		 = 1;
			$id = $this->admin_model->add_job_type($data);
		}
		else if ($type == 'update') {
			$return = $this->admin_model->update_job_type($id, $data);
		}
		return $id;

	}

	public function job_type_status($id='',$status='') {
		if($status == 1) {
			$data['status'] = 0;
		} else if($status == 0) {
			$data['status'] = 1;
		}
		$data['updated_at'] = CURRENT_TIME;
		$this->admin_model->update_job_type($id, $data);
		set_global_messages($this->lang->line('job_type_status_updated_suc_msg'),'success');
		redirect('admin/job_type_section');
	}

		/*****************************Job Type Section End*****************************/

		/*****************************Applicant List Section Start****************************/

	public function applicant_section() {
		$ci =& get_instance();
		$site_language = $ci->session->userdata('language');
		$userType=LoginUserDetails('role_id');
		if($userType == 1){
			$this->config->set_item('site_title', $this->lang->line('applicants'));
			$data['records'] = $this->admin_model->get_applicant();
			$data['site_language'] = $site_language; 
			$this->admin_template->load('admin_template','applicants',$data);
		}else{
			$this->admin_template->load('admin_template','not_authorize');
		}
	}

	public function getApplicantsByAjax(){
		$search = "";
		$search_set = 0;
		$dirs = "";
		if(isset($_GET['order'][0]['dir']) && !empty($_GET['order'][0]['dir'])) {
			$dirs = $_GET['order'][0]['dir'];

		}
		if($_GET['order'][0]['column'] == 0) {
			$dirs = 'ASC';

		}
		if(isset($_GET['search']['value']) && !empty($_GET['search']['value'])) {
			$search = $_GET['search']['value'];
		    $search = trim($search);
		    $search_set = 1 ;
		}
		$records = $this->admin_model->get_applicant("",$search,$_GET['length'],$_GET['start'],$dirs);
		$all_records = $this->admin_model->get_applicant();
		$total_records = count($all_records);
		$records_filtered = $total_records;
		if(!empty($records) || !empty($all_records)) {
			$messages      = array();
			$i=0;
			$s = $_GET['start'] + 1;
			if($search_set == 1){
				$records_filtered	= count($records);
			}
			$content='[';
			foreach($records as $record) {
				$exprience_details =  getEmploymentHistory($record->user_account_id);
				$amt_data = total_earned_money($record->user_account_id) ;
				if($record->status==0) {
					$class = "enable";
					$name = $this->lang->line('enable');
					$fa = "fa fa-check";
					$trClass = "gray-box";
					$title = "Click to enable record";
				} else if($record->status==1) {
					$class = "disable";
					$name = $this->lang->line('disable');
					$fa = "fa fa-ban";
					$trClass = "";
					$title = "Click to disable record";
				}
				$content .='[';
				$messages[$i][0] = $record->created_at;
				$messages[$i][1] = ucfirst($record->display_name);
				$messages[$i][2] = get_age_by_date_of_birth($record->date_of_birth);
				$messages[$i][3] = (isset($exprience_details) && $exprience_details!=false && !empty($exprience_details)) ?  ($exprience_details[0]->length_of_service > 1 ? $exprience_details[0]->length_of_service." yrs" : $exprience_details[0]->length_of_service." yr"): "0 yr";
				$messages[$i][4] = getTotalvoteCounts($record->user_account_id);
				$messages[$i][5] = (isset($amt_data->total_money) || $amt_data->total_money > 0) ? $amt_data->total_money : 0 ;
				$messages[$i][6] = getAverageRating($record->user_account_id) > 0 ? round_up(getAverageRating($record->user_account_id), 1, PHP_ROUND_HALF_DOWN) : 0 ;
				$messages[$i][7] = '<a href="'.base_url('admin/applicant_profile/'.$record->user_account_id).'"><span><i class="fa fa-eye"></i></span><span>'.$this->lang->line('view').'</span></a>';
				$messages[$i][8] = '<a href="'.base_url('admin/applicant_form/'.$record->applicant_id).'"><span><i class="fa fa-pencil"></i></span><span>'.$this->lang->line('edit').'</span></a>';
				$messages[$i][9] = '<a href="javascript:;" title="'.$title.'" onclick="myFunction('.$record->applicant_id.','.$record->user_account_id.','.$record->status.')"><span><i class="'.$fa.'"></i></span><span>'.$name.'</span></a>';
				$messages[$i][10] = $trClass;
				$messages[$i][11] = $class;
				$i++;
				$s++;

			}
			$content .= ']';
			$final_data = json_encode($messages);
		}else{
			$final_data = [];
			$total_records = 0;
			$records_filtered = 0;
		}
		echo '{"draw":'.$_GET['draw'].',"recordsTotal":'.$total_records.',"recordsFiltered":'.$records_filtered.',"data":'.$final_data.'}';
		die;

	}

	public function applicant_form($id='') {
		$userType=LoginUserDetails('role_id');
		$ci =& get_instance();
		$siteLang = $ci->session->userdata('language');
		if($userType == 1){
			if((isset($_POST['save'])) && $id=='') {
				if($this->save_applicant()) {
					set_global_messages($this->lang->line('applicant_added_successfully'),'success');
					redirect('admin/applicant_section');
				}
			} else {
				if(isset($_POST['save'])) {
					if($this->save_applicant('update',$id)) {
						set_global_messages($this->lang->line('applicant_updated_successfully'),'success');
						redirect('admin/applicant_section');
					}
				}
			}
			if($id) {
				$this->config->set_item('site_title',$this->lang->line('update_applicant'));
				$data['records'] = $this->admin_model->get_applicant($id);
				$data['id']=$id;
			} else {
				$this->config->set_item('site_title', $this->lang->line('add_applicant'));
				$data = '';
				$data['id']='';
			}
			$data['site_language'] = $siteLang;
			$data['schoolAttends'] = $this->admin_model->get_education_institutes("",1);
			$this->admin_template->load('admin_template','applicants_form',$data);
		}else{
			$this->admin_template->load('admin_template','not_authorize');
		}
	}

	private function save_applicant($type = 'insert',$id=0) {
		if($id != 0) {
			$_POST['id'] = $id;
		}
		$user_account_id = $this->input->post('user_account_id');
		$this->form_validation->set_rules('first_name','First Name','required|trim|max_length[100]');
		$this->form_validation->set_rules('unique_id','Unique Id','required|trim|check_unique_id_exist['.$user_account_id.']');
		$this->form_validation->set_rules('last_name','Last Name','required|trim|max_length[100]');
		$this->form_validation->set_rules('display_name','Display Name','required|trim|max_length[100]');
		$this->form_validation->set_rules('contact_no','Contact No','required|trim|numeric|min_length[8]|max_length[8]');
		$this->form_validation->set_rules('email_id','Work Email','required|trim|check_email_exist['.$user_account_id.']|valid_email');
		$this->form_validation->set_rules('date_of_birth','Date Of Birth','required|trim');
		$this->form_validation->set_rules('school_attend','School Attend','required|trim');
		$this->form_validation->set_rules('region_number','Region Number','required|trim|numeric');
		if($this->form_validation->run() === FALSE) {
			return FALSE;
		}

		$data = array();
		$data['first_name']			= $this->input->post('first_name');
		$data['last_name']        	= $this->input->post('last_name');
		$data['display_name']       = $this->input->post('display_name');
		$data['contact_no']        	= '+65'.$this->input->post('contact_no');
		$data['email_id']        	= $this->input->post('email_id');
		$data['date_of_birth']      = $this->input->post('date_of_birth');
		$data['school_attend']      = $this->input->post('school_attend');
		$data['region_number']      = $this->input->post('region_number');
		$data['referral_code']      = $this->input->post('referral_code');
		if($this->input->post('is_NEA_certified')) {
			$data['is_NEA_certified']     = '1';
		} else {
			$data['is_NEA_certified']     = '0';
		}
		$data['updated_at'] 		= CURRENT_TIME;
		$data['status']  	= 1;

		$userAccount['role_id'] 	= 4; //aplicant
		$userAccount['status']  	= 1;
		$userAccount['updated_by']  = LoginUserDetails('userid');
		$userAccount['email_id']	= $this->input->post('email_id');
		$userAccount['unique_id']	= strtoupper($this->input->post('unique_id'));

		if($type == 'insert') {
			$password = randomPassword();
			$userAccount['password'] 	= md5($password);
			$userAccount['created_at'] 	= CURRENT_TIME;

			$data['user_account_id']	= $this->admin_model->add_account($userAccount);

			$data['created_at'] = CURRENT_TIME;

			$id = $this->admin_model->add_applicant($data);

			$emailData= array();
			$emailData['recieverId'] = $this->input->post('email_id');
			$emailData['subject'] = 'JOBS ON DEMANDS\'s Account Creation';
			$emailData['template'] = 'email';
			$emailData['fname']				= $this->input->post('first_name');
			$emailData['body'] = 'You are added by Jobs on demand\'s admin as a applicant user and your login details are given below :-';
			$emailData['loginId'] = $this->input->post('email_id');
			$emailData['password'] = $password;

			$email_template = get_email_template('account_creation');
			$body = $email_template->body;
			$searchArray = array("{user_name}","{added_by}","{user_role}","{login_url}","{login_id}","{password}");
			$replaceArray = array(ucfirst($emailData['fname']),'Jobs on demand\'s admin','applicant user',base_url(),$emailData['loginId'],$password);
			$emailData['message']=str_replace($searchArray, $replaceArray, $body);

			send_email($emailData);
		}
		else if ($type == 'update') {
			$aid = $this->input->post('user_account_id');
			$this->admin_model->update_account($aid,$userAccount);
			$return = $this->admin_model->update_applicant($id, $data);
		}
		return $id;
	}


	public function applicant_status($id='',$userAccountsId='',$status='') {
		$account_data = array();
		if($userAccountsId == "" || $status == "" || $id == "")
		{
			$userAccountsId = $_POST['user_account_id'];
			$status = $_POST['status'];
			$id= $_POST['id'];
		}
		
		if($status == 1) { /* disable */
			$data['status'] = 0;
			$data['is_delete'] = 1;
			$data['reason_for_disable'] = $_POST['reason'];
			$account_data['secret_token'] = "";
			$account_data['is_delete'] = 1;
			$account_data['is_user_verified'] = 0;
			$account_data['status'] = 0; 
		} else if($status == 0) { /* to make enable */
			$data['status'] = 1;
			$data['is_delete'] = 0;
			$account_data['is_delete'] = 0;
			$account_data['status'] = 1;
		}
		
		$account_data['updated_at']  = $data['updated_at'] = CURRENT_TIME;
		$this->admin_model->update_account($userAccountsId,$account_data);
		$this->admin_model->update_applicant($id, $data);
		//echo "dsdsd"; die;
		set_global_messages($this->lang->line('applicant_status_updated_mesg'),'success');
		redirect('admin/applicant_section');
	}
		/*****************************Applicant List Section End*****************************/

		/*****************************Account Setting Section Start*****************************/
	public function profile() {
		$this->config->set_item('site_title', $this->lang->line('update_profile_page_title'));
		$userid= LoginUserDetails('userid');
		$userType=LoginUserDetails('role_id');
		if(isset($_POST['save'])) {
			if($this->profile_form()) {
				set_global_messages($this->lang->line('profile_updated_mesg'),'success');
				redirect('admin/profile');
			}
		}
		$data['records'] = $this->admin_model->get_profile_details($userid,$userType);
		$language_data = $this->admin_model->get_user_langauage($userid);
		$data['language_id'] = $language_data->language_id; 
		$data['id']=$userid;
		if($userType == 2) {
			$data['companyName'] = $this->admin_model->get_company($data['records']->company_id)->company_name;
		}
		$this->admin_template->load('admin_template','profile',$data);
	}

	public function profile_form() {

		$userid= LoginUserDetails('userid');
		$user_account_id = $this->input->post('user_account_id');
		$this->form_validation->set_rules('first_name','First Name','required|trim|max_length[100]');
		$this->form_validation->set_rules('last_name','Last Name','required|trim|max_length[100]');
		$this->form_validation->set_rules('contact_no','Contact No','required|trim|numeric|min_length[8]|max_length[8]');
		$this->form_validation->set_rules('job_title','Job Title','required|trim|max_length[100]');
		$this->form_validation->set_rules('email_id','Email Id','required|trim|check_email_exist['.$user_account_id.']|valid_email');
		$this->form_validation->set_rules('language','Language','required|trim');
		if($this->input->post('password') != ''){
			$this->form_validation->set_rules('password','Password','required|trim|min_length[5]|matches[confirm_password]');
			$this->form_validation->set_rules('confirm_password','Confirm Password','required|trim|min_length[5]');
		}
		if($this->form_validation->run() === FALSE) {
			set_global_messages('The Password field does not match the Confirm Password field.','error');
			return FALSE;
		}
		$data = array();
		$userAccount = array();
		
		$data['first_name']     = $this->input->post('first_name');
		$data['last_name']      = $this->input->post('last_name');
		$data['contact_no']     = '+65'.$this->input->post('contact_no');
		$data['job_title']      = $this->input->post('job_title');
		$data['email_id']      = $this->input->post('email_id');
		$langauge_id = $this->input->post('language');
		
		if($this->input->post('password') != '') {
			$data['password']			= md5($this->input->post('password'));
			$userAccount['password']    = md5($this->input->post('password'));
		}
		$data['updated_at'] = CURRENT_TIME;
		$userAccount['updated_at'] = CURRENT_TIME;
		$userAccount['email_id'] = $this->input->post('email_id');
		$userAccount['unique_id'] = $this->input->post('email_id');
		$response=$this->uploadProfileImage('headquarter_manager');
        if(!empty($response) && $response['status']=="success") {
			$data['profile_image']=$response['imageName'];
		}
		$userType= LoginUserDetails('role_id');
		if($userType == 1) {
			$this->admin_model->update_superadmin_profile($userid, $data);
		} else if($userType == 2) {
			$this->admin_model->update_headquater_profile($userid, $data);
		} else if($userType == 3) {
			$this->admin_model->update_outlet_manager_profile($userid, $data);
		} else if($userType == 5) {
			$this->admin_model->update_area_manager_profile($userid, $data);
		}
		//echo $langauge_id; die;
		$this->admin_model->update_language($user_account_id,$langauge_id);
		$this->session->set_userdata('user_first_name',$this->input->post('first_name'));  // set updated value into session
		$this->session->set_userdata('user_last_name',$this->input->post('last_name'));
		if($langauge_id == 1){
			$language = 'english';		
		}else{
			$language = 'chinese';		
		} 
		$this->session->set_userdata('language',$language);
		return $this->admin_model->update_account($userid, $userAccount);
	}

	public function get_unique_id() {
		$emailId=$this->input->post('emailId');
		$userAccountId=$this->input->post('userAccountId');
		echo check_email_exist($userAccountId,$emailId);
	}

		/*****************************Account Setting Section End*****************************/

		/*****************************Educationa Institute Section Start*****************************/
	public function institute_section() {
		$ci =& get_instance();
		$siteLang = $ci->session->userdata('language');
		$userType=LoginUserDetails('role_id');
		if($userType == 1){
			$this->config->set_item('site_title', $this->lang->line('educational_insti'));
			$data['site_language'] = $siteLang;
		//	$data['records'] = $this->admin_model->get_education_institutes();
			$this->admin_template->load('admin_template','institute_section',$data);
		}else{
			$this->admin_template->load('admin_template','not_authorize');
		}
	}

	public function getInstituteListByAjax(){
		$search = "";
		$search_set = 0;
		$dirs = "";
		if(isset($_GET['order'][0]['dir']) && !empty($_GET['order'][0]['dir'])) {
			$dirs = $_GET['order'][0]['dir'];

		}
		if($_GET['order'][0]['column'] == 0) {
			$dirs = 'ASC';

		}
		if(isset($_GET['search']['value']) && !empty($_GET['search']['value'])) {
			$search = $_GET['search']['value'];
		    $search = trim($search);
		    $search_set = 1 ;
		}
		$records = $this->admin_model->get_education_institutes("","",$search,$_GET['length'],$_GET['start'],$dirs);
		$all_records = $this->admin_model->get_education_institutes();
		$total_records = count($all_records);
		$records_filtered = $total_records;
		if(!empty($records) || !empty($all_records)) {
			$messages      = array();
			$i=0;
			$s = $_GET['start'] + 1;
			if($search_set == 1){
				$records_filtered	= count($records);
			}
			$content='[';
			foreach($records as $record) {
				if($record->status==0) {
					$class = "enable";
					$name = $this->lang->line('enable');
					$fa = "fa fa-check";
					$trClass = "gray-box";
					$title = "Click to enable record";
				} else if($record->status==1) {
					$class = "disable";
					$name = $this->lang->line('disable');
					$fa = "fa fa-ban";
					$trClass = "";
					$title = "Click to disable record";
				}
				$content .='[';
				$messages[$i][0] = $record->created_at;
				$messages[$i][1] = (strlen($record->name) > 100) ? ucfirst(substr($record->name,0,75)).'...' : ucfirst($record->name);
				$messages[$i][2] = '<a href="'.base_url('admin/institute_form/'.$record->education_institutes_id).'"><span><i class="fa fa-pencil"></i></span><span>'.$this->lang->line('edit').'</span></a>';
				$messages[$i][3] = '<a href="javascript:;" title="'.$title.'" onclick="myFunction('.$record->education_institutes_id.','.$record->status.')"><span><i class="'.$fa.'"></i></span><span>'.$name.'</span></a>';
				$messages[$i][4] = $trClass;
				$messages[$i][5] = $class;
				$i++;
				$s++;

			}
			$content .= ']';
			$final_data = json_encode($messages);
		}else{
			$final_data = [];
			$total_records = 0;
			$records_filtered = 0;
		}
		echo '{"draw":'.$_GET['draw'].',"recordsTotal":'.$total_records.',"recordsFiltered":'.$records_filtered.',"data":'.$final_data.'}';
		die;
	}

	public function institute_form($id='') {
		$userType=LoginUserDetails('role_id');
		$ci =& get_instance();
		$siteLang = $ci->session->userdata('language');
		if($userType == 1){
			if((isset($_POST['save'])) && $id=='') {
				if($this->save_institute()) {
					set_global_messages($this->lang->line('institute_added_successfully'),'success');
					redirect('admin/institute_section');
				}
			} else {
				if(isset($_POST['save'])) {
					if($this->save_institute('update',$id)) {
						set_global_messages($this->lang->line('institute_updated_successfully'),'success');
						redirect('admin/institute_section');
					}
				}
			}
			if($id) {
				$this->config->set_item('site_title', $this->lang->line('edit')." " .$this->lang->line('educational_insti'));
				$data['records'] = $this->admin_model->get_education_institutes($id);
				$data['id']=$id;
			} else {
				$this->config->set_item('site_title', $this->lang->line('add_new')." ".$this->lang->line('educational_insti'));
				$data = '';
				$data['id']='';
			}
			$data['site_language'] = $siteLang; 
			$this->admin_template->load('admin_template','institute_form',$data);
		}else{
			$this->admin_template->load('admin_template','not_authorize');
		}
	}

	private function save_institute($type = 'insert',$id=0) {
		$checkInstitute = $this->admin_model->check_institute($_POST['name'],$id);
		if($checkInstitute) {
			set_global_messages($this->lang->line('edu_insti_already_exist'),'error');
			redirect('admin/institute_form/'.$id);
		}
		if($id != 0) {
			$_POST['id'] = $id;

		}
		$this->form_validation->set_rules('name','Institute Name','required|trim|max_length[100]');
		if($this->form_validation->run() === FALSE) {
			return FALSE;
		}

		$data = array();
		$data['name']        = $this->input->post('name');
		$data['updated_at'] = CURRENT_TIME;

		if($type == 'insert') {
			$data['created_at'] = CURRENT_TIME;
			$data['approved']      		 = 1;
			$data['status']      		 = 1;
			$id = $this->admin_model->add_institute($data);
		}
		else if ($type == 'update') {
			$return = $this->admin_model->update_institute($id, $data);
		}
		return $id;

	}

	public function institute_status($id='',$status='') {
		if($status == 1) {
			$data['status'] = 0;
		} else if($status == 0) {
			$data['status'] = 1;
		}
		$data['updated_at'] = CURRENT_TIME;
		$this->admin_model->update_institute($id, $data);
		set_global_messages($this->lang->line('edu_insti_status_updated'),'success');
		redirect('admin/institute_section');
	}

	/*****************************Educationa Institute Section End*****************************/

	public function notification() {
		$userType=LoginUserDetails('role_id');
		$ci =& get_instance();
		$siteLang = $ci->session->userdata('language');
		if($userType == 1){
			$this->config->set_item('site_title', $this->lang->line('outlets'));
			$data['records'] = $this->admin_model->get_outlet();
			
			$this->admin_template->load('admin_template','notification',$data);
		}else{
			$this->admin_template->load('admin_template','not_authorize');
		}
	}

	public function billing_records() {
		$userType=LoginUserDetails('role_id');
		$ci =& get_instance();
		$siteLang = $ci->session->userdata('language');
		if($userType == 1){
			$data['outlets'] = $this->admin_model->get_billing_records('outlet');
			$data['site_language'] = $siteLang;
			$this->admin_template->load('admin_template','billing_records',$data);
		}else{
			$this->admin_template->load('admin_template','not_authorize');
		}
	}

	public function searchBillingRecordByAjax(){
		$search = "";
		$search_set = 0;
		$dirs = "";
		$length = 0;
		$start = 0;
		$column_name = "";
		if(isset($_GET['order'][0]['dir']) && !empty($_GET['order'][0]['dir'])) {
			$dirs = $_GET['order'][0]['dir'];

		}
		if(isset($_GET['order'][0]['column']) && $_GET['order'][0]['column']  == 0) {
			$dirs = 'ASC';

		}
		if(isset($_GET['order'][0]['column'])){
				if($_GET['order'][0]['column'] == 1){
					$column_name = "jod_jobs.created_at";
				}
				if($_GET['order'][0]['column'] == 2){
					$column_name = "jod_outlets.outlet_name";
				}
				if($_GET['order'][0]['column'] == 3){
					$column_name = "jod_jobs.job_title";
				}
				if($_GET['order'][0]['column'] == 4){
					$column_name = "jod_jobs.start_date";
				}
				if($_GET['order'][0]['column'] == 5){
					$column_name = "jod_jobs.end_date";
				}

		}
		if(isset($_GET['search']['value']) && !empty($_GET['search']['value'])) {
			$search = $_GET['search']['value'];
		    $search = trim($search);
		    $search_set = 1 ;
		}
		if(isset($_GET['length'])){
			$length = $_GET['length'];
		}
		if(isset($_GET['start'])){
			$start = $_GET['start'];
		}
		if(isset($_GET['month'])){
				$month = $_GET['month'];
		}if(isset($_GET['year'])){
				$year = $_GET['year'];
		}
		if(isset($_GET['outlet'])){
				$outlet = $_GET['outlet'];
		}
		$all_records = $this->admin_model->get_billing_records('',$month,$year,$outlet);
		$records = $this->admin_model->get_billing_records("",$month,$year,$outlet,$search,$length,$start,$dirs,$column_name);
		//$all_records = $this->admin_model->get_applicant();
		//print_r($all_records); die;
		$total_records = count($all_records);
		$records_filtered = $total_records;
		if(!empty($records) || !empty($all_records)) {
			$messages      = array();
			$i=0;
			$s = $start + 1;
			if($search_set == 1){
				$records_filtered	= count($records);
			}
			$content='[';
			foreach($records as $record) {
				$total_wages = getWages($record->job_id);
				$content .='[';
				$messages[$i][0] = "";
				$messages[$i][1] = date('d M Y' ,strtotime($record->job_created_at));
				$messages[$i][2] = $record->outlet_name;
				$messages[$i][3] = $record->job_title;
				$messages[$i][4] = date('d M Y' ,strtotime($record->start_date));
				$messages[$i][5] = date('d M Y' ,strtotime($record->end_date));
				$messages[$i][6] = getTotalCompletedHoursJOb($record->job_id);
				$messages[$i][7] = (!empty($total_wages->total_wages))?$total_wages->total_wages:'0';
				if(($record->is_completed == 1) && ($record->is_applicants_hired == 1) && ($record->is_delete == 0)) {
					$job_status = 'Completed';
				}
				if(($record->is_approved == 1 || $record->is_approved ==0 ) && ($record->is_delete == 0) && ($record->is_applicants_hired == 0) && ($record->is_completed == 0)) {
					$job_status =  'Open';
				}
				if(($record->is_approved == 1 ) && ($record->is_delete == 0) && ($record->is_applicants_hired == 1) && ($record->is_completed == 0)) {
					$job_status ='Active';
				}
				if(($record->is_applicants_hired == 0) && ($record->is_completed == 0) &&  ($record->is_delete == 1)) {
					$job_status = 'Canceled';
				}
				$messages[$i][8] = $job_status;
				$i++;
				$s++;

			}
			$content .= ']';
			$final_data = json_encode($messages);
		}else{
			$final_data = json_encode(array());
			$total_records = 0;
			$records_filtered = 0;
		}
		if(isset($_GET['draw'])){
		 $draw = $_GET['draw'] ;
		}else{
		 $draw = 1;
		}
		echo '{"draw":'.$draw.',"recordsTotal":'.$total_records.',"recordsFiltered":'.$records_filtered.',"data":'.$final_data.'}'; die;


	}


	public function get_active_jobs(){
		$res = array();
		$input_arr =  $this->input->post();
		$data = $this->admin_model->get_outlet_jobs($input_arr['outlet_id'],$input_arr['area_manager']);
		if(!empty($data)){
			$res["code"] = 0 ;
			$res["message"] = "open job exist";
		}else{
			$res["code"] = 1 ;
			$res["message"] = "open job not exist";
		}
		echo json_encode($res);
	}

	public function get_active_jobs_outlet_manger(){
		$res = array();
		$input_arr =  $this->input->post();
		$data = $this->admin_model->get_jobs_outlet_manager($input_arr['outlet_id'],$input_arr['outlet_manager_id']);
		if(!empty($data)){
			$res["code"] = 0 ;
			$res["message"] = "open job exist";
		}else{
			$res["code"] = 1 ;
			$res["message"] = "open job not exist";
		}
		echo json_encode($res);
	}

	public function company_feedback(){
		$this->admin_template->load('admin_template','company_feedback_screen');
	}

	public function not_authorize(){
		$this->admin_template->load('admin_template','not_authorize');
	}

	/**
	 * @method :  user_profile
	 * @param : applicant_id
	 * @return : NULL. Show applicant profile information
	*/
	public function applicant_profile($applicant_id=''){
		$data = array();
		$this->config->set_item('site_title', 'Applicant Profile');
		$data['applicant_profile'] 		= $this->jobs_model->applicant_profile($applicant_id);
		$data['applicants_active_job'] 	= $this->jobs_model->applicants_active_job($applicant_id,1);
		$data['applicants_open_job'] 	= $this->jobs_model->applicants_open_job($applicant_id,1);
		$data['cancel_compelted_jobs'] 	= $this->jobs_model->applicants_cancel_or_compeleted_jobs($applicant_id);
		$data['applicants_feedback']  	= applicant_feedback_data($applicant_id);
		$this->admin_template->load('admin_template','applicant_profile',$data);
	}

	/* @Method : To assign credits to the HQ Manager
	 * @Params : company_id
	 * @Response :return Array render in the view
	 * @Author : Kratika Jain
	 * */

	public function assign_credits_to_company($company_id=""){
		$ci =& get_instance();
		$siteLang = $ci->session->userdata('language');
		$userType=LoginUserDetails('role_id');
		if($userType == 1){
			$data = array();
			/** To get the company details **/
			$data['company_details'] = $this->admin_model->get_company($company_id);

			/* To get the JOD credit transaction History for company  */
			$data['transaction_history'] = $this->transaction_model->get_company_transaction_history($company_id);

			if((isset($_POST['save_assign_credit']))) {
				$this->form_validation->set_rules('jod_credit','JOD credit','required|max_length[100]');
				$this->form_validation->set_rules('comment','Comment','required|trim');
				/* Check for the server side validations */
				if($this->form_validation->run() === FALSE) {
					set_global_messages($this->lang->line('jod_credit_error_msg'),'error');
					return FALSE;
				}else{

					$input_arr = array();
					$jod_credit_amt = $this->input->post('jod_credit');
					$input_arr["jod_credit_amount"] = abs($jod_credit_amt);
					//echo $input_arr["jod_credit_amount"] ; die;
					if($jod_credit_amt >  0 ){
						/* positive */
						$input_arr["transaction_type"] =  2 ; 	/* 1 for debit and 2 for the credit */
						$input_arr["activity_type"] = 1; 		/** defined in the DB in the trasaction_activity_types **/
					}else{
						/* negative */
						$input_arr["transaction_type"] =  1 ;
						$input_arr["activity_type"] = 2; 		/** defined in the DB in the trasaction_activity_types **/
					}
					$input_arr["comment_text"] = $this->input->post('comment');
					$input_arr["company_id"] = $company_id;
					$HQManager_details = get_HQMananger_companyId($company_id);

					if(!empty($HQManager_details)){
						$company_details = $this->admin_model->get_company($company_id);
						$input_arr["assigned_to_manager"] = $HQManager_details->user_accounts_id ;
						$input_arr["transaction_level"]  =  1 ;   			/* 1: SuperAdmin to HQ , 2: HQ manager to outlets */
						$input_arr["assigned_by_manager"]  =  1 ;   		/* 1: SuperAdmin, 2: HQ manager */
						$input_arr["assigned_at"] = date("Y-m-d H:i:s") ;  /* 1: SuperAdmin, 2: HQ manager */
						$input_arr["outlet_id"]   =  0 ;  					/* 1: SuperAdmin, 2: HQ manager */
						$input_arr["created_at"]  =  date("Y-m-d H:i:s")  ;
						$input_arr["updated_at"]  =  date("Y-m-d H:i:s")  ;
						$input_arr["credit_before_transaction"]  =  $company_details->current_jod_credit;
						$insert = $this->transaction_model->add_transaction($input_arr);
						if($insert){
							$update_data = array ();
							if($jod_credit_amt > 0 ){ 					/*  Addeed */
								$current_amt =  $company_details->current_jod_credit +	$input_arr["jod_credit_amount"] ;
							}else{  									/* 2 for deducted */

								$current_amt =  $company_details->current_jod_credit -	$input_arr["jod_credit_amount"] ;
								//echo $current_amt." --  ".$company_details->min_credit_limit;
								if($current_amt <= $company_details->min_credit_limit){
									// send email to the aptus account

										$flag = "company_level";
										$this->email_to_admin_notify_min_credit($company_id,$flag);
								}
							}
							$update_data["current_jod_credit"] = $current_amt;
							/* update the amount in the company table for the current JOD credits */
							$this->transaction_model->update_current_JOD_credit($update_data,$company_id);
							set_global_messages($this->lang->line('jod_credit_assign_suc_msg'),'success');
							redirect('admin/jod_credits');
						}else{
							set_global_messages('Some DB error occured.','error');
							redirect('admin/assign_credits_to_company/'.$company_id);
						}
					}else{
							set_global_messages($this->lang->line('hq_manager_not_assigned_err'),'error');
							redirect('admin/assign_credits_to_company/'.$company_id);
					}
				}
			}
			$data['site_language'] = $siteLang;
			$this->admin_template->load('admin_template','assign_credits_company',$data);
		}else{
			$this->admin_template->load('admin_template','not_authorize');
		}
	}


	/* @Method : To get the all companies data related to the JOD credits
	 * @Params : -
	 * @Response : return Array render in the view
	 * @Author : Kratika Jain
	 * */
	public function jod_credits(){
		$data = array();
		$ci =& get_instance();
		$siteLang = $ci->session->userdata('language');
		$userType=LoginUserDetails('role_id');
		if($userType == 1){
			$data['companies'] = $this->admin_model->get_company();
			$data['transaction_history'] = $this->transaction_model->get_company_transaction_history();
			$data['site_language']  =  $siteLang; 
			$this->admin_template->load('admin_template','admin_jod_credit_details',$data);
		}else{
			$this->admin_template->load('admin_template','not_authorize');
		}

	}

	public function get_company_transaction_history_by_ajax($id=""){
		$search = "";
		$search_set = 0;
		$dirs = "";
		$column_name = "";
		if(isset($_GET['order'][0]['dir']) && !empty($_GET['order'][0]['dir'])) {
			$dirs = $_GET['order'][0]['dir'];

		}
		if($_GET['order'][0]['column'] == 0) {
			$dirs = 'ASC';
		}
		if(isset($_GET['order'][0]['column'])){
			if($_GET['order'][0]['column'] == 1){
				$column_name = "jod_transactions.assigned_at";
			}
			if($_GET['order'][0]['column'] == 2){
				$column_name = "jod_companies.company_name";
			}
			if($_GET['order'][0]['column'] == 3){
				$column_name = "jod_transactions.jod_credit_amount";
			}if($_GET['order'][0]['column'] == 4){
				$column_name = "jod_transactions.comment_text";
			}

		}
		if(isset($_GET['search']['value']) && !empty($_GET['search']['value'])) {
			$search = $_GET['search']['value'];
		    $search = trim($search);
		    $search_set = 1 ;
		}
		$records = $this->transaction_model->get_company_transaction_history($id,$search,$_GET['length'],$_GET['start'],$dirs,$column_name);
		$all_records = $this->transaction_model->get_company_transaction_history($id);
		$total_records = count($all_records);
		$this->getDataTableViewForTransactionHistory($records,$all_records,$total_records,$search_set,"admin");
	}

	public function getDataTableViewForTransactionHistory($records=array(),$all_records=array(),$total_records=0,$search_set=0,$flag=""){
		if(!empty($records) || !empty($all_records)) {
			$messages      = array();
			$i=0;
			$s = $_GET['start'] + 1;
			$records_filtered = $total_records;
			if($search_set == 1){
				$records_filtered	= count($records);
			}
			//print_r($records); die;
			$content='[';
			foreach($records as $record) {
				$content .='[';
				$messages[$i][0] ="";
				$messages[$i][1] = (isset($record->assigned_at) && $record->assigned_at!= "") ? date('d M Y h:i A', strtotime($record->assigned_at)) : " " ;
				if($flag == "admin"){
					$messages[$i][2] = (isset($record->company_name)) ? $record->company_name : "" ;
				}
				elseif($flag == "hq_manager"){
					$messages[$i][2] = (isset($record->outlet_name)) ? $record->outlet_name : "" ;
				}
				$messages[$i][3] = (isset($record->transaction_type) && $record->transaction_type == 2) ? "+".  $record-> jod_credit_amount : "-".  $record-> jod_credit_amount;
				$messages[$i][4] = '<p class="jod-tooltip" data-toggle="tooltip" data-placement="bottom"  title="sjkfdkfgg kjdsfhlsjhsgggk">'.strlen($record->comment_text) > 150 ? ucfirst(substr($record->comment_text,0,150)).'...' : ucfirst($record->comment_text).'</p>';
				$i++;
				$s++;

			}
			$content .= ']';
			$final_data = json_encode($messages);
		}else{
			$final_data = [];
			$total_records = 0;
			$records_filtered = 0;
		}
		echo '{"draw":'.$_GET['draw'].',"recordsTotal":'.$total_records.',"recordsFiltered":'.$records_filtered.',"data":'.$final_data.'}';
		die;
	}


	/* @Method : To Download the  content of the transaction history
	 * @Params :  flag, company_id
	 * @Response : create the excel sheet and then make it download
	 * @Author : Kratika Jain
	 */

	public function download_transaction_history($flag="",$company_id="",$outlet_id=""){
		//echo $flag ; die;
		if(!empty($flag)){
			if($flag != "" && $flag == "all_company"){  /* To download the transaction history for the all companies Starts */
				$transaction_history = $this->transaction_model->get_company_transaction_history();
				$transaction_history_data = array();
				if(!empty($transaction_history)){
					$i = 1;
					foreach($transaction_history as $transaction){
						$data_arr = array();
						$data_arr["serial_num"] = $i;
						$data_arr["date"] = date('d M Y h:i A', strtotime($transaction->assigned_at));
						$data_arr["company_name"] = $transaction->company_name;
						$data_arr["amount"] = (isset($transaction->transaction_type) && $transaction->transaction_type == 2)  ? "+".  $transaction-> jod_credit_amount : "-".  $transaction-> jod_credit_amount;
						$data_arr["comments"] = $transaction->comment_text;
						$transaction_history_data[]    = $data_arr;
						$i++;
					}
					$filename = "JOD_credit_transactions_".strtotime("now").".xlsx";
					$title = "JOD Credit Transaction History";
					$this->generate_excel($transaction_history_data,$title,$filename,1);
				}else{
					set_global_messages($this->lang->line('no_transac_history_found'),'error');
					redirect('admin/jod_credits');
				}
			} /* To download the transaction history for the all companies ends */
			elseif($flag != "" && $flag == "specific_company"){   /* To download the transaction history for the specific company  starts*/
				if($company_id!="" && $company_id!=0){
					$company_details = $this->admin_model->get_company($company_id);
					$transaction_history =  $this->transaction_model->get_company_transaction_history($company_id);
					$transaction_history_data = array();
					if(!empty($transaction_history)){
						$i = 1;
						foreach($transaction_history as $transaction){
							$data_arr = array();
							$data_arr["serial_num"] = $i;
							$data_arr["date"] = date('d M Y h:i A', strtotime($transaction->assigned_at));
							$data_arr["company_name"] = $transaction->company_name;
							$data_arr["amount"] = (isset($transaction->transaction_type) && $transaction->transaction_type == 2)  ? "+".  $transaction-> jod_credit_amount : "-".  $transaction-> jod_credit_amount;
							$data_arr["comments"] = $transaction->comment_text;
							$transaction_history_data[]    = $data_arr;
							$i++;
						}
						$filename = "JOD_credits_history_".$company_details->company_name."_".strtotime("now").".xlsx";
						$title = "JOD Credit Transaction History";
						$this->generate_excel($transaction_history_data,$title,$filename,1);
					}else{
						set_global_messages($this->lang->line('no_transac_history_found'),'error');
						redirect('admin/assign_credits_to_company/'.$company_id);
					}
				}else{
					set_global_messages('Company id not found.','error');
					redirect('admin/jod_credits');
				}
			} /* To download the transaction history for the specific company  ends*/
			/* To download transaction History for the specific outlet */
			elseif($flag != "" && $flag == "specific_outlet"){
				if($outlet_id !="" && $company_id != "" && $company_id != 0){
					$outlet_data = $this->admin_model->get_outlet($outlet_id);
					$userType=LoginUserDetails('role_id');
					if($userType == 2){
						$hq_managerId = LoginUserDetails('userid');
					}else{
						$hq_managerDetails = get_HQMananger_companyId($company_id);
						$hq_managerId = $hq_managerDetails->user_accounts_id;
					}
					$transaction_history = $this->transaction_model->get_outelt_transaction_history($outlet_id,$hq_managerId);
					if(!empty($transaction_history)){
						$i = 1;
						foreach($transaction_history as $transaction){
							$data_arr = array();
							$data_arr["serial_num"] = $i;
							$data_arr["date"] = date('d M Y h:i A', strtotime($transaction->assigned_at));
							$data_arr["outelt_name"] = $transaction->outlet_name;
							$data_arr["amount"] = (isset($transaction->transaction_type) && $transaction->transaction_type == 2)  ? "+".  $transaction-> jod_credit_amount : "-".  $transaction-> jod_credit_amount;
							$data_arr["comments"] = $transaction->comment_text;
							$transaction_history_data[]    = $data_arr;
							$i++;
						}
						$filename = "JOD_credits_history_".$outlet_data->outlet_name."_".strtotime("now").".xlsx";
						$title = "JOD Credit Transaction History";
						$this->generate_excel($transaction_history_data,$title,$filename,0);
					}else{
						set_global_messages($this->lang->line('no_transac_history_found'),'error');
						redirect('admin/assign_credits_to_company/'.$company_id);
					}
				}else{
					set_global_messages($this->lang->line('oultlet_id_company_id_err'),'error');
					redirect('admin/assign_credits_to_outlet/'.$outlet_id);
				}
			}
			/* To download transaction History for the all outlet */
			elseif($flag != "" && $flag == "all_outlets" && $company_id !=0 ){
				$userType=LoginUserDetails('role_id');
				$company_details = $this->admin_model->get_company($company_id);
				if($userType == 2){
					$hq_managerId = LoginUserDetails('userid');
				}else{
					$hq_managerDetails = get_HQMananger_companyId($company_id);
					$hq_managerId = $hq_managerDetails->user_accounts_id;
				}
				$transaction_history = $this->transaction_model->get_outelt_transaction_history("",$hq_managerId);
				if(!empty($transaction_history)){
					$i = 1;
					foreach($transaction_history as $transaction){
						$data_arr = array();
						$data_arr["serial_num"] = $i;
						$data_arr["date"] = date('d M Y h:i A', strtotime($transaction->assigned_at));
						$data_arr["outelt_name"] = $transaction->outlet_name;
						$data_arr["amount"] = (isset($transaction->transaction_type) && $transaction->transaction_type == 2)  ? "+".  $transaction-> jod_credit_amount : "-".  $transaction-> jod_credit_amount;
						$data_arr["comments"] = $transaction->comment_text;
						$transaction_history_data[]    = $data_arr;
						$i++;
					}
					$filename = "JOD_credits_history_".$company_details->company_name."_".strtotime("now").".xlsx";
					$title = "JOD Credit Transaction History";
					$this->generate_excel($transaction_history_data,$title,$filename,0);
				}else{
					set_global_messages($this->lang->line('no_transac_history_found'),'error');
					redirect('admin/assign_credits_to_company/'.$company_id);
				}
			}elseif($flag != "" && $flag == "all_job_history" && $company_id!=0){
				$company_details = $this->admin_model->get_company($company_id);
				$history_data = array();
				$history_array  = get_job_history("hq_level");
				//echo "<pre>" ; print_r($history_array ); die;
				$i= 1;
				foreach ($history_array as $key => $row) {
					$job_data = array();
					$clock_in[$key] = $row['clock_in_date'];
					$job_data["serial_num"] = $i;
					$job_data["applicant_fullname"] = $row["applicant_fullname"];
					$job_data["unique_id"] = $row["unique_id"];
					$job_data["outlet_name"] = $row["outlet_name"];
					$job_data["date"] = $row["date"];
					$job_data["clock_out_date"] = $row["clock_out_date"];
					$job_data["clock_in"] = $row["clock_in"];
					$job_data["clock_out"] = $row["clock_out"];
					if($row["meal_break_time"] !=""){
						$job_data["meal_break_time"]  = $row["meal_break_time"];
					}else{
						$job_data["meal_break_time"] = "N/A";
					}
					$job_data["total_hours_worked"] = $row["total_hours_worked"];
					$job_data["wages_per_hour"] = $row["wages_per_hour"];
					$job_data["total_jod_credit"] = $row["total_jod_credit"];
					$job_data["credit_before_transacion"] = $row["credit_before_transacion"];
					$job_data["credit_after_transacion"] = $row["credit_after_transacion"];
					/* fetch new details adjusted by Super admin */
					$data_adjusted_by_SA = get_new_clock_in_out_details($row["job_id"],$row["applicant_user_accounts_id"],$row["outlet_id"]);
					$job_data['new_clock_in_date'] = ($data_adjusted_by_SA != false) ? $data_adjusted_by_SA['payment_data']->clock_in_date : " N/A ";
					$job_data['new_clock_out_date'] = ($data_adjusted_by_SA != false) ? $data_adjusted_by_SA['payment_data']->clock_out_date : " N/A ";
					$job_data['new_clock_in_time'] = ($data_adjusted_by_SA != false) ? date("H:i", strtotime($data_adjusted_by_SA['payment_data']->clock_in_time)) : " N/A ";
					$job_data['new_clock_out_time'] = ($data_adjusted_by_SA != false) ? date("H:i", strtotime($data_adjusted_by_SA['payment_data']->clock_out_time)) : " N/A ";
					$job_data['new_meal_break_time'] = ($data_adjusted_by_SA != false) ? $data_adjusted_by_SA['payment_data']->meal_break_time : " N/A ";
					$job_data['mew_total_hours_worked'] = ($data_adjusted_by_SA != false) ? $data_adjusted_by_SA['payment_data']->total_hours_worked : " N/A ";
					$job_data['new_wages_per_hour'] = ($data_adjusted_by_SA != false) ? $data_adjusted_by_SA['payment_data']->wages_per_hour : " N/A ";
					$job_data['new_total_jod_credit'] = ($data_adjusted_by_SA != false) ? $data_adjusted_by_SA['payment_data']->total_jod_credit : " N/A ";
					$job_data['new_credit_before_deduction'] = ($data_adjusted_by_SA != false) ?  sprintf('%0.2f',$data_adjusted_by_SA['transaction_data']->credit_before_transacion) : "N/A";
					$job_data['new_credit_after_deduction'] = ($data_adjusted_by_SA != false) ?  sprintf('%0.2f',$data_adjusted_by_SA['transaction_data']->credit_after_transacion) : "N/A";
					
	
					$history_data[] = $job_data;
					$i++;
				}
				//echo "<pre>"; print_r($history_data); die;
			//	array_multisort($clock_in,SORT_DESC,$history_data);
				$filename = "job_history_".$company_details->company_name."_".strtotime("now").".xlsx";
				$title = "JOD JOB History - ".$company_details->company_name;
				//print_r($history_data); die;
				$this->generate_excel($history_data,$title,$filename,2);
			}elseif($flag != "" && $flag == "area_manager_outlets"){
				$company_id = LoginUserDetails('company_id');
				$company_details = $this->admin_model->get_company($company_id);
				$history_array = get_job_history("area_manager_outlet_level");
			//	print_r($history_array); die("ss");
				$i= 1;
				foreach ($history_array as $key => $row) {
					$job_data = array();
					$job_data["serial_num"] = $i;
					$job_data["applicant_fullname"] = $row["applicant_fullname"];
					$job_data["unique_id"] = $row["unique_id"];
					$job_data["outlet_name"] = $row["outlet_name"];
					$job_data["date"] = $row["date"];
					$job_data["clock_out_date"] = $row["clock_out_date"];
					$job_data["clock_in"] = $row["clock_in"];
					$job_data["clock_out"] = $row["clock_out"];
					if($row["meal_break_time"] !=""){
						$job_data["meal_break_time"]  = $row["meal_break_time"];
					}else{
						$job_data["meal_break_time"] = "N/A";
					}
					$job_data["total_hours_worked"] = $row["total_hours_worked"];
					$job_data["wages_per_hour"] = $row["wages_per_hour"];
					$job_data["total_jod_credit"] = $row["total_jod_credit"];
					$job_data["credit_before_transacion"] = $row["credit_before_transacion"];
					$job_data["credit_after_transacion"] = $row["credit_after_transacion"];
					
					$data_adjusted_by_SA = get_new_clock_in_out_details($row["job_id"],$row["applicant_user_accounts_id"],$row["outlet_id"]);
					$job_data['new_clock_in_date'] = ($data_adjusted_by_SA != false) ? $data_adjusted_by_SA['payment_data']->clock_in_date : " N/A ";
					$job_data['new_clock_out_date'] = ($data_adjusted_by_SA != false) ? $data_adjusted_by_SA['payment_data']->clock_out_date : " N/A ";
					$job_data['new_clock_in_time'] = ($data_adjusted_by_SA != false) ? date("H:i", strtotime($data_adjusted_by_SA['payment_data']->clock_in_time)) : " N/A ";
					$job_data['new_clock_out_time'] = ($data_adjusted_by_SA != false) ? date("H:i", strtotime($data_adjusted_by_SA['payment_data']->clock_out_time)) : " N/A ";
					$job_data['new_meal_break_time'] = ($data_adjusted_by_SA != false) ? $data_adjusted_by_SA['payment_data']->meal_break_time : " N/A ";
					$job_data['mew_total_hours_worked'] = ($data_adjusted_by_SA != false) ? $data_adjusted_by_SA['payment_data']->total_hours_worked : " N/A ";
					$job_data['new_wages_per_hour'] = ($data_adjusted_by_SA != false) ? $data_adjusted_by_SA['payment_data']->wages_per_hour : " N/A ";
					$job_data['new_total_jod_credit'] = ($data_adjusted_by_SA != false) ? $data_adjusted_by_SA['payment_data']->total_jod_credit : " N/A ";
					$job_data['new_credit_before_deduction'] = ($data_adjusted_by_SA != false) ?  sprintf('%0.2f',$data_adjusted_by_SA['transaction_data']->credit_before_transacion) : "N/A";
					$job_data['new_credit_after_deduction'] = ($data_adjusted_by_SA != false) ?  sprintf('%0.2f',$data_adjusted_by_SA['transaction_data']->credit_after_transacion) : "N/A";
					$history_data[] = $job_data;
					$i++;
				}
				$filename = "job_history_".$company_details->company_name."_".strtotime("now").".xlsx";
				$title = "JOD JOB History - ".$company_details->company_name;
				$this->generate_excel($history_data,$title,$filename,2);

			}elseif($flag!="" && $flag=="specific_outlet_transactions"){
				$logged_in_user_id = LoginUserDetails('userid');
				$outlet_data = $this->admin_model->get_outlet();
				$outlet_ids = array();
				foreach($outlet_data as $outlet){
					$outlet_ids[] = $outlet->outlet_id;
				}
				$outlet_str = implode(",",$outlet_ids);
				$transaction_history = $this->outlet_transaction_history($outlet_str,'area_manager_level',$logged_in_user_id);
				if(!empty($transaction_history)){
					$i = 1;
					foreach($transaction_history as $transaction){
						$data_arr = array();
						$data_arr["serial_num"] = $i;
						$data_arr["date"] = date('d M Y h:i A', strtotime($transaction->assigned_at));
						$data_arr["outelt_name"] = $transaction->outlet_name;
						$data_arr["amount"] = (isset($transaction->transaction_type) && $transaction->transaction_type == 2)  ? "+".  $transaction-> jod_credit_amount : "-".  $transaction-> jod_credit_amount;
						$data_arr["comments"] = $transaction->comment_text;
						$transaction_history_data[]    = $data_arr;
						$i++;
					}
					$filename = "JOD_credits_history_".strtotime("now").".xlsx";
					$title = "JOD Credit Transaction History";
					$this->generate_excel($transaction_history_data,$title,$filename,0);
				}else{
					set_global_messages($this->lang->line('no_transac_history_found'),'error');
					redirect('admin/assign_credits_to_company/'.$company_id);
				}
			}elseif($flag != "" && $flag == "specific_outlet_job_history"){
				$outlet_data = $this->admin_model->get_outlet($outlet_id);
				//echo $outlet_id; die; 
				$history_array = get_job_history("outlet_profile",$outlet_id);
				//print_r($history_array); die;
				$i= 1;
				foreach ($history_array as $key => $row) {
					$job_data = array();
					$job_data["serial_num"] = $i;
					$job_data["date"] = $row["date"];
					$job_data["clock_out_date"] = $row["clock_out_date"];
					$job_data["outlet_name"] = $row["outlet_name"];
					$job_data["applicant_fullname"] = $row["applicant_fullname"];
					$job_data["unique_id"] = $row["unique_id"];
					$job_data["clock_in"] = $row["clock_in"];
					$job_data["clock_out"] = $row["clock_out"];
					if($row["meal_break_time"] !=""){
						$job_data["meal_break_time"]  = $row["meal_break_time"];
					}else{
						$job_data["meal_break_time"] = "N/A";
					}
					$job_data["total_hours_worked"] = $row["total_hours_worked"];
					$job_data["wages_per_hour"] = $row["wages_per_hour"];
					$job_data["total_jod_credit"] = $row["total_jod_credit"];
					$job_data["credit_before_transacion"] = $row["credit_before_transacion"];
					$job_data["credit_after_transacion"] = $row["credit_after_transacion"];
					
					$data_adjusted_by_SA = get_new_clock_in_out_details($row["job_id"],$row["applicant_user_accounts_id"],$row["outlet_id"]);
					$job_data['new_clock_in_date'] = ($data_adjusted_by_SA != false) ? $data_adjusted_by_SA['payment_data']->clock_in_date : " N/A ";
					$job_data['new_clock_out_date'] = ($data_adjusted_by_SA != false) ? $data_adjusted_by_SA['payment_data']->clock_out_date : " N/A ";
					$job_data['new_clock_in_time'] = ($data_adjusted_by_SA != false) ? date("H:i", strtotime($data_adjusted_by_SA['payment_data']->clock_in_time)) : " N/A ";
					$job_data['new_clock_out_time'] = ($data_adjusted_by_SA != false) ? date("H:i", strtotime($data_adjusted_by_SA['payment_data']->clock_out_time)) : " N/A ";
					$job_data['new_meal_break_time'] = ($data_adjusted_by_SA != false) ? $data_adjusted_by_SA['payment_data']->meal_break_time : " N/A ";
					$job_data['new_total_hours_worked'] = ($data_adjusted_by_SA != false) ? $data_adjusted_by_SA['payment_data']->total_hours_worked : " N/A ";
					$job_data['new_wages_per_hour'] = ($data_adjusted_by_SA != false) ? $data_adjusted_by_SA['payment_data']->wages_per_hour : " N/A ";
					$job_data['new_total_jod_credit'] = ($data_adjusted_by_SA != false) ? $data_adjusted_by_SA['payment_data']->total_jod_credit : " N/A ";
					$job_data['new_credit_before_deduction'] = ($data_adjusted_by_SA != false) ?  sprintf('%0.2f',$data_adjusted_by_SA['transaction_data']->credit_before_transacion) : "N/A";
					$job_data['new_credit_after_deduction'] = ($data_adjusted_by_SA != false) ?  sprintf('%0.2f',$data_adjusted_by_SA['transaction_data']->credit_after_transacion) : "N/A";
					
					$history_data[] = $job_data;
					$i++;
				}
				$filename = "job_history_".$outlet_data->outlet_name."_".strtotime("now").".xlsx";
				$title = "JOD JOB History - ".$outlet_data->outlet_name;
			//	$filename = "job_history_".$company_details->company_name."_".strtotime("now").".xlsx";
				//$title = "JOD JOB History - ".$company_details->company_name;
				
				$this->generate_excel($history_data,$title,$filename,2);

			}
		}
	}

	/* @Method : To genearte the  excel file for transaction history
	 * @Params :  transaction_history_data, title, filename
	 * @Response : create the excel sheet and then make it download
	 * @Author : Kratika Jain
	 */

	public function generate_excel($transaction_history_data,$title,$filename,$iscompany=""){

		$this->excel->setActiveSheetIndex(0);
		//name the worksheet
		//$this->excel->getActiveSheet()->setTitle($title);
		//set cell A1 content with some text
		if($iscompany == "2"){
			$this->excel->getActiveSheet()->setCellValue('A1', $title);
				$this->excel->getActiveSheet()->setCellValue('A3', 'S. No.');
				$this->excel->getActiveSheet()->setCellValue('B3', 'Full Name');
				$this->excel->getActiveSheet()->setCellValue('C3', 'NRIC/FIN');
				$this->excel->getActiveSheet()->setCellValue('D3', 'Outlet Name');
				$this->excel->getActiveSheet()->setCellValue('E3', 'Clock In Date');
				$this->excel->getActiveSheet()->setCellValue('F3', 'Clock Out Date');
				$this->excel->getActiveSheet()->setCellValue('G3', 'Clock In');
				$this->excel->getActiveSheet()->setCellValue('H3', 'Clock Out');
				$this->excel->getActiveSheet()->setCellValue('I3', 'Meal Break Time');
				$this->excel->getActiveSheet()->setCellValue('J3', 'Total Hours Worked');
				$this->excel->getActiveSheet()->setCellValue('K3', 'Wages Per Hour');
				$this->excel->getActiveSheet()->setCellValue('L3', 'Total JOD Credit');
				$this->excel->getActiveSheet()->setCellValue('M3', 'Before Deduction');
				$this->excel->getActiveSheet()->setCellValue('N3', 'After Deduction');
				
				$this->excel->getActiveSheet()->setCellValue('O3', 'Clock In Date');
				$this->excel->getActiveSheet()->setCellValue('P3', 'Clock Out Date');
				$this->excel->getActiveSheet()->setCellValue('Q3', 'Clock In');
				$this->excel->getActiveSheet()->setCellValue('R3', 'Clock Out');
				$this->excel->getActiveSheet()->setCellValue('S3', 'Meal Break Time');
				$this->excel->getActiveSheet()->setCellValue('T3', 'Total Hours Worked');
				$this->excel->getActiveSheet()->setCellValue('U3', 'Wages Per Hour');
				$this->excel->getActiveSheet()->setCellValue('V3', 'Total JOD Credit');
				$this->excel->getActiveSheet()->setCellValue('W3', 'Before Deduction');
				$this->excel->getActiveSheet()->setCellValue('X3', 'After Deduction');
				
				//merge cell A1 until G1
				$this->excel->getActiveSheet()->mergeCells('A1:X1');
				
				$this->excel->getActiveSheet()->mergeCells('E2:N2');
				$this->excel->getActiveSheet()->mergeCells('O2:X2');
				$this->excel->getActiveSheet()->setCellValue('E2',"Old Clock In Clock out Details");
				$this->excel->getActiveSheet()->setCellValue('O2',"New Clock In Clock out Details");
				
		}else{
				$this->excel->getActiveSheet()->setCellValue('A1', $title);
				$this->excel->getActiveSheet()->setCellValue('A3', 'S. No.');
				$this->excel->getActiveSheet()->setCellValue('B3', 'Date');
				if($iscompany !="" && $iscompany == 1){
					$this->excel->getActiveSheet()->setCellValue('C3', 'Company Name');
				}else{
					$this->excel->getActiveSheet()->setCellValue('C3', 'Outlet Name');
				}
				$this->excel->getActiveSheet()->setCellValue('D3', 'Amount');
				$this->excel->getActiveSheet()->setCellValue('E3', 'Comments');
				//merge cell A1 until C1
				$this->excel->getActiveSheet()->mergeCells('A1:E1');
		}


		//set aligment to center for that merged cell (A1 to C1)
		$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		 //make the font become bold
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(16);
		$this->excel->getActiveSheet()->getStyle('A1')->getFill()->getStartColor()->setARGB('#333');
		$this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(123);
		
		$this->excel->getActiveSheet()->getStyle('E2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('E2')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		 //make the font become bold
		$this->excel->getActiveSheet()->getStyle('E2')->getFont()->setBold(true);
		$this->excel->getActiveSheet()->getStyle('E2')->getFont()->setSize(12);
		$this->excel->getActiveSheet()->getStyle('E2')->getFill()->getStartColor()->setARGB('#333');
		$this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(123);

		$this->excel->getActiveSheet()->getStyle('O2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('O2')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		 //make the font become bold
		$this->excel->getActiveSheet()->getStyle('O2')->getFont()->setBold(true);
		$this->excel->getActiveSheet()->getStyle('O2')->getFont()->setSize(12);
		$this->excel->getActiveSheet()->getStyle('O2')->getFill()->getStartColor()->setARGB('#333');
		$this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(123);
		
		for($col = ord('A'); $col <= ord('Z'); $col++){
			//set column dimension
			$this->excel->getActiveSheet()->getColumnDimension(chr($col))->setAutoSize(true);
			$this->excel->getActiveSheet()->getRowDimension(1)->setRowHeight(-1);
			//change the font size
			$this->excel->getActiveSheet()->getStyle(chr($col))->getFont()->setSize(10);
			$this->excel->getActiveSheet()->getStyle(chr($col))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		}

		$exceldata = $transaction_history_data;
		//Fill data
		$this->excel->getActiveSheet()->fromArray($exceldata, null, 'A4');

		$this->excel->getActiveSheet()->getStyle('A3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('B3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('C3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('D3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('E3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('F3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('G3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('H3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('I3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('J3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('K3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('L3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('M3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('N3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('O3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('P3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('Q3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('R3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('S3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('T3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('U3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('V3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('W3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('X3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		//$filename='PHPExcelDemo.xls'; //save our workbook as this file name
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache

		//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		//if you want to save it as .XLSX Excel 2007 format
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
		//force user to download the Excel file without writing it to server's HD
		$objWriter->save('php://output');
	}

	/***  JOD credit functionality for the  HQ level 20 July starts  by Kratika */


	/* @Method : To assign credits to the outlets by HQ manager
	 * @Params : company_id
	 * @Response :return Array render in the view
	 * @Author : Kratika Jain
	 * */
	public function assign_credits_to_outlet($outlet_id=""){
		$ci =& get_instance();
		$siteLang = $ci->session->userdata('language');
		$userType=LoginUserDetails('role_id');
		if($userType == 2){
			$data = array();
			$data["company_details"] = getCompanydetails();
			$data['outlet_data'] = $this->admin_model->get_outlet($outlet_id);
			/** Get the transaction history for the outelt */
			$data['transaction_history'] = $this->transaction_model->get_outelt_transaction_history($outlet_id,LoginUserDetails('userid'));
			$data['site_language'] = $siteLang; 
			if((isset($_POST['save_assign_credit']))) {
				$this->form_validation->set_rules('jod_credit','JOD credit','required|max_length[100]');
				$this->form_validation->set_rules('comment','Comment','required|trim');
				/* Check for the server side validations */
				if($this->form_validation->run() === FALSE) {
					set_global_messages($this->lang->line('jod_credit_error_msg'),'error');
					return FALSE;
				}else{
					$jod_credit_amt = $this->input->post('jod_credit');
					/** check the JOD credits HQ manager going to assign to the Outlet is not greater then HQ managers current JOD credit balance */
					if($data["company_details"]->current_jod_credit >= $jod_credit_amt) {
						$input_parent_arr = array();

						$input_parent_arr["transaction_level"] = 2 ; /* HQ manager to outlet */
						$areaManagerDetails = areaManagerDetails($data['outlet_data']->area_manager_id);
						$input_parent_arr["assigned_to_manager"] =  $areaManagerDetails[0]->user_accounts_id  ;  /* Area manager id */
						$input_parent_arr["assigned_by_manager"] = LoginUserDetails('userid');  /* HQ manager id */ ;
						$input_parent_arr["assigned_at"] = date("Y-m-d H:i:s") ;
						$input_parent_arr["company_id"] = $data["company_details"]->company_id;
						$input_parent_arr["outlet_id"] = $outlet_id;
						$input_parent_arr["comment_text"]  = $this->input->post('comment');
						$input_parent_arr["created_at"] = date("Y-m-d H:i:s") ;
						$input_parent_arr["updated_at"] = date("Y-m-d H:i:s") ;
						$input_parent_arr["jod_credit_amount"] = abs($jod_credit_amt) ;
						$input_parent_arr["credit_before_transaction"] = $data['outlet_data']->current_jod_credit;
						$input_parent_arr["parent_transaction_id"] = 0;

						$input_arr = array();
						$input_arr["transaction_level"] = 2 ; 		/* HQ manager to outlet */
						$input_arr["assigned_to_manager"] =  0 ;  	/* When autometed transaction */
						$input_arr["assigned_by_manager"] = 0 ;   	/* When autometed transaction */
						$input_arr["assigned_at"] = date("Y-m-d H:i:s") ;
						$input_arr["company_id"] = $data["company_details"]->company_id;
						$input_arr["outlet_id"] = $outlet_id;
						$input_arr["comment_text"]  = "auto_transaction_system_generated" ;
						$input_arr["created_at"] = date("Y-m-d H:i:s") ;
						$input_arr["updated_at"] = date("Y-m-d H:i:s") ;
						$input_arr["jod_credit_amount"] = abs($jod_credit_amt) ;
						$input_arr["credit_before_transaction"] = $data["company_details"]->current_jod_credit;

						if($jod_credit_amt < 0 ){  /* deduction */
							/**  Check the amount HQ manager wants to deducted from the outlet is available to the outlet or not */
								if($data['outlet_data']->current_jod_credit > abs($jod_credit_amt)){
									$input_parent_arr["activity_type"] =  4  ; 		/* HQ Manager deducted the credits to the outlet */
									$input_parent_arr["transaction_type"] = 1  ; 	/* debit */
									$input_arr["activity_type"] =  6  ; 			/* JOD credits deducted from HQ manager When HQ manager assigns/added the JOD credits to the Outlet.   */
									$input_arr["transaction_type"] = 2 ; 			/* credit */
									$current_amt_outlet =  $data['outlet_data']->current_jod_credit - $input_arr["jod_credit_amount"] ;
									$current_amt_company =  $data['company_details']->current_jod_credit + $input_parent_arr["jod_credit_amount"] ;
								}else{
									set_global_messages($this->lang->line('deduct_jod_credit_err_msg'),'error');
									redirect('admin/assign_credits_to_outlet/'.$outlet_id);
								}
						}else{
							/* credited */
							$input_parent_arr["activity_type"] =  3  ; 			/* HQ Manager assigns/added the credits to the outlet */
							$input_parent_arr["transaction_type"] = 2 ; 		/* credit/added */
							$input_arr["activity_type"] =  5  ;					/* JOD credits added from HQ manager When HQ manager assigns/added the JOD credits to the Outlet.   */
							$input_arr["transaction_type"] = 1 ; 				/* debit/deducted */
							$current_amt_outlet =  $data['outlet_data']->current_jod_credit + $input_arr["jod_credit_amount"] ;
							$current_amt_company =  $data['company_details']->current_jod_credit - $input_parent_arr["jod_credit_amount"] ;
							if($data['company_details']->min_credit_limit >= $current_amt_company){
								$flag  = "company_level";
								/* Notify to the super admin that JOD credit balance reach for this company at its minimum level */
								$this->email_to_admin_notify_min_credit($data['company_details']->company_id,$flag);
							}
						}
						$parent_data_insert = $this->transaction_model->add_transaction($input_parent_arr);
						if($parent_data_insert){
							$input_arr["parent_transaction_id"] = $parent_data_insert ;
							$insert = $this->transaction_model->add_transaction($input_arr);
							if($insert){
								$update_data_outlet["current_jod_credit"] = $current_amt_outlet;
								$update_data_compny["current_jod_credit"] = $current_amt_company;
								/* update the amount in the outlet table for the current JOD credits */
								$this->transaction_model->update_current_JOD_credit_outlet($update_data_outlet,$outlet_id);
								/* update the amount in the company table */
								$this->transaction_model->update_current_JOD_credit($update_data_compny,$data["company_details"]->company_id);
								set_global_messages($this->lang->line('jod_credit_assign_suc_msg'),'success');
								redirect('admin/assign_credits_to_outlet/'.$outlet_id);
							}else{
								set_global_messages('Some DB error occured.','error');
								redirect('admin/assign_credits_to_outlet/'.$outlet_id);
							}
						}else{
								set_global_messages('Some DB error occured.','error');
								redirect('admin/assign_credits_to_outlet/'.$outlet_id);
						}
					}else{
						set_global_messages($this->lang->line('assign_jod_credit_err_msg'),'error');
					}
				}
			}
			$this->admin_template->load('admin_template','assign_credits_outlet',$data);
		}else{
			$this->admin_template->load('admin_template','not_authorize');
		}
	}

	/* @Method : To get all outlets data for the JOD credit
	 * @Params :  -
	 * @Response :return Array render in the view
	 * @Author : Kratika Jain
	 * */
	public function hq_jod_credits(){
		$ci =& get_instance();
		$siteLang = $ci->session->userdata('language');
		$userType=LoginUserDetails('role_id');
		if($userType == 2){
			$data = array();
			$data["outlets"] = $this->admin_model->get_outlet();
			$data["company_details"] = getCompanydetails();
			$data['site_language'] = $siteLang; 
			$data['transaction_history'] = $this->transaction_model->get_outelt_transaction_history("",LoginUserDetails('userid'),'',10,0);
			$history_array  = get_job_history("hq_level",'','',10,0);
			if(!empty($history_array)){
				$data["job_history"]  = $history_array;
			}else{
				$data["job_history"]  = false;
			}
			$data['site_language'] = $siteLang; 
			$this->admin_template->load('admin_template','hq_jod_credit_details',$data);
		}else{
			$this->admin_template->load('admin_template','not_authorize');
		}
	}

	public function getJobHistoryListByAjax($flag="",$outlet_id=""){		
		$search = "";
		$search_set = 0;
		$dirs = "";
		$column_name  ="";
		if(isset($_GET['order'][0]['dir']) && !empty($_GET['order'][0]['dir'])) {
			$dirs = $_GET['order'][0]['dir'];

		}
		if($_GET['order'][0]['column'] == 0) {
			$dirs = 'ASC';

		}
		if(isset($_GET['order'][0]['column']) && !empty($_GET['order'][0]['column'])){
			if($_GET['order'][0]['column'] == 1) {
				$column_name = "jod_clockIn_clockOut.applicant_clockIn_dateTime";
			}
			if($_GET['order'][0]['column'] == 2) {
				$column_name = "jod_clockIn_clockOut.applicant_clockOut_dateTime";
			}
			if($_GET['order'][0]['column'] == 3) {
				$column_name = "jod_outlets.outlet_name";
			}
			if($_GET['order'][0]['column'] == 4) {
				$column_name = "jod_applicants.first_name";
			}
			if($_GET['order'][0]['column'] == 5) {
				$column_name = "jod_clockIn_clockOut.applicant_clockIn_dateTime";
			}
			if($_GET['order'][0]['column'] == 6) {
				$column_name = "jod_clockIn_clockOut.applicant_clockOut_dateTime";
			}
		}
		if(isset($_GET['search']['value']) && !empty($_GET['search']['value'])) {
			$search = $_GET['search']['value'];
		    $search = trim($search);
		    $search_set = 1 ;
		}
		if($flag == "outlet_profile"){
			$outletId = $outlet_id;
		}else{
			$outletId  = '';
		}
		$records = get_job_history($flag,$outletId,$search,$_GET['length'],$_GET['start'],$dirs,$column_name);
		$all_records = get_job_history_count($flag,$outletId);
		if(!empty($all_records) && $all_records != false){
			$total_records = $all_records->job_history_count; 
		}else{
			$total_records = 0;
		}
		$records_filtered = $total_records;
		if($records != false){
			if(!empty($records) || !empty($all_records)) {
				$messages      = array();
				$i=0;
				$s = $_GET['start'] + 1;
				if($search_set == 1){
					$records_filtered	= count($records);
				}
				$content='[';
				foreach($records as $key=>$record) {
					$content .='[';
					$messages[$i][0] = "";
					$messages[$i][1] =  $record["date"];
					$messages[$i][2] =  $record["clock_out_date"];
					$messages[$i][3] =  $record["outlet_name"];
					$messages[$i][4] =  $record["applicant_fullname"];
					$messages[$i][5] =  $record["clock_in"];
					$messages[$i][6] =  $record["clock_out"];
					$messages[$i][7] = "<a class='pull-left text-center action-icon ad-man' onclick='javascript:show_popup(". $record["job_id"].','.$record["applicant_user_accounts_id"].','.$record['outlet_id'].")'><i class='fa fa-search'></i><div class='clerfix'></div><lable>". $this->lang->line('view')."</lable></a><a class='pull-left text-center action-icon ad-man' href='". base_url('jobs/download_job_history/'.$record['outlet_id'].'/'.$record['job_id'].'/'.$record['applicant_user_accounts_id'])."'><i class='fa fa-download'></i><div class='clerfix'></div><lable>excel</lable></a>";
					$i++;
					$s++;
				}
				$content .= ']';
				$final_data = json_encode($messages);
			}else{
				$final_data = [];
				$total_records = 0;
				$records_filtered = 0;
			}
		}else{
			$final_data = "{}";
			$total_records = 0;
			$records_filtered = 0;
		}
		echo '{"draw":'.$_GET['draw'].',"recordsTotal":'.$total_records.',"recordsFiltered":'.$records_filtered.',"data":'.$final_data.'}';
		die;

	}

	public function getOutletsTransactionHistoryByAjax($flag="",$outlet_id=0){
		$logged_in_user_id = LoginUserDetails('userid');
		$data['outlets'] = $this->admin_model->get_outlet();
		$outlet_ids = array();
		foreach($data['outlets'] as $outlet){
			$outlet_ids[] = $outlet->outlet_id;
		}
		if($outlet_id > 0){
			$outlet_str = $outlet_id;
		}else{
			$outlet_str = implode(",",$outlet_ids);
		}
		$search = "";
		$search_set = 0;
		$dirs = "";
		$column_name = "";
		if(isset($_GET['order'][0]['dir']) && !empty($_GET['order'][0]['dir'])) {
			$dirs = $_GET['order'][0]['dir'];

		}
		if($_GET['order'][0]['column'] == 0) {
			$dirs = 'ASC';
		}

		if(isset($_GET['order'][0]['column'])){
			if($_GET['order'][0]['column'] == 1){
				$column_name = "jod_transactions.assigned_at";
			}
			if($_GET['order'][0]['column'] == 2){
				$column_name = "jod_outlets.outlet_name";
			}
			if($_GET['order'][0]['column'] == 3){
				$column_name = "jod_transactions.jod_credit_amount";
			}if($_GET['order'][0]['column'] == 4){
				$column_name = "jod_transactions.comment_text";
			}

		}
		if(isset($_GET['search']['value']) && !empty($_GET['search']['value'])) {
			$search = $_GET['search']['value'];
		    $search = trim($search);
		    $search_set = 1 ;
		}
		if($flag="area_manager_level"){
			$records = $this->outlet_transaction_history($outlet_str,'area_manager_level',$logged_in_user_id,$search,$_GET['length'],$_GET['start'],$dirs,$column_name);
			$all_records = $this->outlet_transaction_history($outlet_str,'area_manager_level',$logged_in_user_id);
		}elseif("outlet_level"){
			$records = $this->outlet_transaction_history($outlet_str,'area_manager_level',$logged_in_user_id,$search,$_GET['length'],$_GET['start'],$dirs,$column_name);
			$all_records = $this->outlet_transaction_history($outlet_str,'area_manager_level',$logged_in_user_id);
		}else{
			$records = $this->transaction_model->get_outelt_transaction_history("",LoginUserDetails('userid'),$search,$_GET['length'],$_GET['start'],$dirs,$column_name);
			$all_records = $this->transaction_model->get_outelt_transaction_history("",LoginUserDetails('userid'));
		}
		$total_records = count($all_records);
		$this->getDataTableViewForTransactionHistory($records,$all_records,$total_records,$search_set,"hq_manager");

	}



	/* @Method : To send email to the super admin
	 * @Params :  -
	 * @Response :return Array render in the view
	 * @Author : Kratika Jain
	 * */
	public function send_email_to_super_admin(){
		$data["company_details"] = getCompanydetails();
		$jod_credit_amt = $this->input->post('jod_credit_amt');
		$email_comments = $this->input->post('email_comments');
		$aptusEmailId 	= 'notification@aptus.com.sg';
		//$email = "kratikajain@cdnsol.com";
		$emailData= array();
		$emailData['recieverId'] = $aptusEmailId;
		$companyName = $this->admin_model->get_company(LoginUserDetails('company_id'))->company_name;
		$body_msg = 'Headquarter manager of '.$companyName.' has requesting to add the '.$jod_credit_amt.' JOD credit.';
		$email_template = get_email_template('purchase_jod_credit_request_by_HQ_Manager');
		$body = $email_template->body;
		$emailData['subject'] = $email_template->subject;
		$searchArray = array("{body}","{email_comments}");
		$replaceArray = array($body_msg,$email_comments);
		$emailData['message']=str_replace($searchArray, $replaceArray, $body);
		send_email($emailData);
		set_global_messages(' Email sent successfully.','success');
		echo true;
	}


	/* @Method : To send email to the super admin
	 * @Params :  -
	 * @Response :return Array render in the view
	 * @Author : Kratika Jain
	 * */
	public function email_to_admin_notify_min_credit($company_id="",$flag,$outlet_id=""){
		if($company_id != "" && $flag = "company_level"){
			$logo_url =  base_url()."templates/JOD/img/logo-small.png";
			$company_details = $this->admin_model->get_company($company_id);
			//print_r($company_details); die;
			$aptusEmailId 	= 'notification@aptus.com.sg';
			//$aptusEmailId = "kratikajain@cdnsol.com";
			$emailData= array();
			$emailData['recieverId'] = $aptusEmailId;
			$companyName = $company_details->company_name;
			$body_msg = 'JOD credit limit of '.$companyName.' has reached to the  its minimum credit limit.';
			$email_template = get_email_template('JOD_credit_limit_reached_at_its_closest_limit');
			$body = $email_template->body;
			$emailData['subject'] = $email_template->subject;
			$searchArray = array("{site_logo}","{body}");
			$replaceArray = array($logo_url,$body_msg);

			$emailData['message']=str_replace($searchArray, $replaceArray, $body);
			$searchArray1 = array("{company_name}");
			$replaceArray1 = array($companyName);
			$emailData['subject']=str_replace($searchArray1, $replaceArray1, $email_template->subject);
			//	print_r($emailData); die;
			send_email($emailData);
			//set_global_messages(' Email sent successfully.','success');
			return true;
		}
	}


	/* ---------------------------------------- JOD credit functionality Outlet Level-----------------------*/

	/* @Method : To show the page at the outlet level for the JOB credits
	 * @Params :  -
	 * @Response :return Array render in the view
	 * @Author : Kratika Jain
	 * */

	public function outlet_level_jod_credits(){
		$userType=LoginUserDetails('role_id');
		$logged_in_user_id = LoginUserDetails('userid');
		$data = array();
		$ci =& get_instance();
		$siteLang = $ci->session->userdata('language');
		$data['site_language'] = $siteLang;
		if($userType == 3 || $userType == 5){
			$data['outlets'] = $this->admin_model->get_outlet();
			$outlet_ids = array();
			foreach($data['outlets'] as $outlet){
				$outlet_ids[] = $outlet->outlet_id;
			}
			$outlet_str = implode(",",$outlet_ids);
			$data['transaction_history'] = $this->outlet_transaction_history($outlet_str,'area_manager_level',$logged_in_user_id);
			$data['job_history'] = get_job_history("area_manager_outlet_level",'','',10,0);
			$this->admin_template->load('admin_template','outlet_jod_credit_detail',$data);
		}else{
			$this->admin_template->load('admin_template','not_authorize');
		}

	}

	public function outlet_transaction_history($outlet_ids="",$level,$manager_id,$search="",$length=0,$start=0,$dirs="",$column_name=""){
		$hqManagerDetails = getHQManager();
		$data = $this->transaction_model->get_outlet_transaction_data($outlet_ids,$manager_id,$level,$hqManagerDetails->user_accounts_id,$search,$length,$start,$dirs,$column_name);
		return $data;
	}

	public function outlet_jod_credit($outlet_id=""){
		$logged_in_user_id = LoginUserDetails('userid');
		$userType=LoginUserDetails('role_id');
		$ci =& get_instance();
		$siteLang = $ci->session->userdata('language');
		if($userType == 3 || $userType == 5 || $userType == 2){
			$data["company_details"] = getCompanydetails();
			$data['outlet_data'] = $this->admin_model->get_outlet($outlet_id);
			//$data['transaction_history'] = $this->outlet_transaction_history($outlet_id,'area_manager_level',$logged_in_user_id);
			$data['transaction_history'] = $this->outlet_transaction_history($outlet_id,'area_manager_level',$logged_in_user_id,'',10,0);
			if($userType == 2){
				$data['job_history'] = get_job_history("outlet_profile",$outlet_id,'',10,00);
			}else{
				//$data['job_history'] = get_job_history("outlet_profile",$outlet_id);
				$data['job_history'] = get_job_history("outlet_profile",$outlet_id,'',10,00);
				///print_r($data['job_history']); die;
			}
			$data['site_language'] = $siteLang;
			$this->admin_template->load('admin_template','outlet_credit_detail_page',$data);
		}else{
			$this->admin_template->load('admin_template','not_authorize');
		}
	}
	
	public function get_data_jod_credit_job_history(){
		$job_id = $this->input->post('job_id');
		$applicant_id = $this->input->post('applicant_id');
		$outlet_id = $this->input->post('outlet_id');
		$details = get_applicant_detail_job_data($job_id,$applicant_id,$outlet_id);
		
	}
	
	public function get_new_clock_in_out_details(){
		$results = array();
		$job_id = $this->input->post('job_id');
		$applicant_id = $this->input->post('applicant_id');
		$outlet_id = $this->input->post('outlet_id');
		$details = get_new_clock_in_out_details($job_id,$applicant_id,$outlet_id);
		
		if($details == false){
			$results['status'] = false;
		}else{
			$results['status'] = true;
			$results['data'] =  $details;
		}
		echo json_encode($results); die;
	}


}
