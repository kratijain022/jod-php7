<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @description: This controller is used for admin panel
 * @create date: 21-Nov-2014
 * @auther: Rajendra Patidar
 * 
 */ 

class Video extends MX_Controller{

	function __construct(){
		parent::__construct();
		
		
		$this->load->library(array('admin_template','head','S3'));
		//$this->load->model(array('admin_model'));
		
		$this->load->language(array('common'));
		$this->form_validation->set_error_delimiters('<div class="server_msg_show">', '</div>');
		$this->session_check->checkSession(); 	
		
		
		//$this->dbutil->optimize_table('hk_user');
	}
	
	
	/*Description : function to add vedio
	 * Author: Gagan David
	 * Date : 13th Dec 2014
	 * 
	 */
	public function index(){
		if(isset($_POST['action']))
		{
			//print_r($_POST);die;
			$id = $_POST['id'];
			$action =$_POST['action'];
			foreach($id as $id)
			{
				if($action==3)
				{
					$this->delete_video($id,1);
				}
				else
				{
					$this->update_videos($action,$id);
				}
			}
			
			if($_POST['action']==1)
			{
				set_global_messages('Video Activated Successfully','success');
			}
			if($_POST['action']==0)
			{
				set_global_messages('Video Deactivated Successfully','success');
			}
			if($_POST['action']==3)
			{
				set_global_messages('Video Deleted Successfully','success');
				
			}
			//set_global_messages('Classification Deleted Successfully','success');
			
		}
		
		$data['records'] = $this->admin_model->get_videos('',1);
		
		 $this->admin_template->load('admin_template','vedio',$data);
	}
	

	/*Description : function to update vedio
	* Author: Gagan David
	* Date : 23rd Dec 2014
	* 
	*/
	public function update_videos($action,$id)
	{
		$this->admin_model->change_video_status($action,$id);
	}
	
	
	
	
	public function video_form($id='',$video_id='',$all=''){
		if(isset($_POST['save']))
		{
			
			$this->save_video($video_id,$all);
		}
		
		$data['id']=$id;
		$data['classification'] = $this->admin_model->classification('',1);
		$data['channels_list'] = $this->admin_model->channels();
		$data['episode'] = $this->admin_model->get_episode('',1);
		$data['author'] = $this->admin_model->get_author();
		$data['video_id'] = $video_id;
		if($id)
		{
			$data['classification'] = $this->admin_model->classification('');
			$data['records'] = $this->admin_model->get_episode($id);
			$data['video'] = $this->admin_model->get_videos($video_id);
			//print_r($data['video']);die;
			$data['authors'] = $this->admin_model->get_author($data['video']->author);
			$parent_id = $data['records']->parent_id;
			$parent_id = $this->admin_model->channels($parent_id);
			$c_id = $parent_id->child_id;
			$pid = $parent_id->parent_id;
			$data['channel_id'] = $c_id;
			$c_id = $this->admin_model->classification($pid);
			$data['channel_select_id'] = $c_id->channel_id;
		}
		
		$this->admin_template->load('admin_template','vedio_form',$data);
	}
	
	/*Description : function to save video
	 * Author: Gagan David
	 * Date : 16th Dec 2014
	 * 
	 */
	public function save_video($id='',$all='')
	{
		//print_r($_POST);die;
		$this->form_validation->set_rules('title','title','required|trim|max_length[255]');
		$this->form_validation->set_rules('description','description','required|trim');
		$this->form_validation->set_rules('upload_file_name','video','required|trim');
		$this->form_validation->set_rules('video_type','type','required|trim');
		$this->form_validation->set_rules('episode','episode','required|trim');
		$this->form_validation->set_rules('episode_no','episode number','required|trim|numeric');
		$this->form_validation->set_rules('channel','channel','required|trim');
		$this->form_validation->set_rules('classification','classification','required|trim');
		if ($this->form_validation->run() === FALSE)
		{
			return FALSE;
		}
		$data = array();
		$data['serial_id'] = $_POST['episode'];
		$data['episode_sequence'] = $_POST['episode_no'];
		$data['season_id'] = $_POST['season'];
		$data['video_url'] = trim($_POST['upload_file_name']);
		$data['title'] = $_POST['title'];
		$data['description'] = $_POST['description'];
		$data['video_duration'] = $_POST['video_duration'];
		$filename = pathinfo($data['video_url'], PATHINFO_FILENAME); 
		$data['thumbnail'] = $filename.'_thumb.jpg';
		$data['video_type'] = $_POST['video_type'];
		if($_POST['video_type']==0)
		{
			$data['earn_time'] = $_POST['earn_time'];
		}
		$data['author'] = $_POST['author'];
		//print_r($_POST['tags']);die;
		if($_POST['tags']!='')
		{
			$data['hash_tag'] = $_POST['tags'];
		}
		if($_POST['tags']=='tag1,tag2')
		{
			$data['hash_tag'] = '';
		}
		if($id)
		{
			$data['status'] = $_POST['status'];
			$data['created_at'] = date('Y-m-d h:i:s');
			$this->admin_model->update_video($data,$id);
			
			if($all)
			{
				//print_r($_GET);die;
				$classification= $_GET['classification'];
				$channel= $_GET['channel'];
				$season= $_GET['season'];
				$serial_id= $_GET['serial_id'];
				set_global_messages('Episode Updated Successfully','success');
				redirect('admin/video/episode_list/?classification='.$classification.'&channel='.$channel.'&season_id='.$season.'&serial_id='.$serial_id);
			}
			else
			{
				set_global_messages('Video Updated Successfully','success');
				redirect('admin/video');
			}
		}
		else
		{
			$data['status'] = 1;
			$this->admin_model->add_video($data);
			set_global_messages('Video Uploaded Successfully','success');
			redirect('admin/video');
		}
		
	}
	
	/*Description : function to delete video
	* Author: Gagan David
	* Date : 23rd Dec 2014
	* 
	*/
	public function delete_video($id='',$type='2')
	{
		$result = $this->admin_model->delete_video($id);
		if($result)
		{
			if($type=='2')
			{
				set_global_messages('Video Deleted Successfully','success');
				redirect('admin/video');
			}
		}
	}
	 
	 
	
	/*Description : function to upload vedio
	 * Author: Gagan David
	 * Date : 15th Dec 2014
	 * 
	 */
	public function upload_vedios($type='',$name,$onserver='')
	{
		ini_set('post_max_size', '2000M');
		ini_set('upload_max_filesize', '2000M');
			$this->form_validation->set_rules('title','title','required|trim|max_length[255]');
			
			if ($this->form_validation->run() === FALSE)
			{
				return FALSE;
			}
			$config['upload_path'] =  './uploads/';
			$config['allowed_types'] =   "*"; 
			if($onserver=='1')
			{
				$config['max_size']      =   "2048";
			}
			else
			{
				$config['max_size']      =   "204800000";
			}
			$config['max_width']     =   "1907";
			$config['max_height']    =   "1280";
			$config['encrypt_name'] = true;
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			
			if ( $this->upload->do_upload($name))
			{
				$data = array('upload_data' => $this->upload->data());
				$photo= $data['upload_data']['file_name'];
				if($onserver !='1')
				{
					$result = $this->upload_on_s3bucket($data['upload_data']['file_name'],$data['upload_data']['full_path']);
					if($result)
					{
						return $photo;
					}
				}
				else
				{
					return $photo;
				}
			}
			else
			{
				//print_r($this->upload->display_errors());die;
				set_global_messages($this->upload->display_errors(),'error');
				return false;
			}
	}
	
	/*Description : function to upload vedio on amazon s3 bucket
	 * Author: Gagan David
	 * Date : 15th Dec 2014
	 * 
	 */
	public function upload_on_s3bucket($file_name='',$full_path)
	{
		ini_set('post_max_size', '2000M');
		ini_set('upload_max_filesize', '2000M');
		
		if (!defined('awsAccessKey')) define('awsAccessKey', 'AKIAJJ2S4GPS7ACUJ5RA');
		if (!defined('awsSecretKey')) define('awsSecretKey', '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');
		$s3 = new S3(awsAccessKey, awsSecretKey);
		$s3->setEndpoint("s3.amazonaws.com");
		$S3_check = $s3->listBuckets();
		$result = $s3->putObject($s3->inputFile($full_path, false), 'managevideo', $file_name, S3::ACL_PUBLIC_READ);
		unlink($full_path);
		return $result;
	}
	
	/*
	 * function :To Add Classification 
	 * Date : 15th Dec 2014
	 * Author: Gagan David
	 * 
	 */
	
	public function classification_form($cid='')
	{
		if(isset($_POST['save']))
		{
			if($cid)
			{
				$this->save_classification(1,$cid);
			}
			else
			{
				$this->save_classification();
			}
		}
		$data['id']= $cid;
		$data['records'] = $this->admin_model->classification($cid);
		$this->admin_template->load('admin_template','classification_form',$data);
	}
	
	
	/*
	 * function :To save classification
	 * Date : 15th Dec 2014
	 * Author: Gagan David
	 * 
	 */
	 
	public function save_classification($type='',$id='')
	{
		
		$this->form_validation->set_rules('title','title','required|trim|max_length[255]');
		if ($this->form_validation->run() === FALSE)
		{
			return FALSE;
		}
		//print_r($_FILES);die;
		if(($type) && ($_FILES['image']['name']!='') )
		{
			$data['image'] = $this->upload_vedios('','image',1);
		}
		else
		{
			if(($_FILES['image']['name']!=''))
			{
				$data['image'] = $this->upload_vedios('','image',1);
			}
		}
		$data['title'] = $_POST['title'];
		$data['status'] = $_POST['status'];
		$data['type'] = 'classification';
		
		if($type)
		{
			$data['action'] = 'U';
			$data['updated_at'] = date('Y-m-d h:i:s');
			$this->admin_model->update_classification($data,$id);
			set_global_messages('Classification Updates Successfully','success');
		}
		else
		{
			$data['action'] = 'A';
			$this->admin_model->add_classification($data);
			set_global_messages('Classification Added Successfully','success');
		}
		
		redirect('admin/video/classification');
	}
	
	/*
	 * function :To show classification list
	 * Date : 16th Dec 2014
	 * Author: Gagan David
	 * 
	 */
	 
	public function classification($cid='')
	{
		//echo phpinfo();die;
		if(isset($_POST['action']))
		{
			$id = $_POST['id'];
			$action =$_POST['action'];
			foreach($id as $id)
			{
				if($action==3)
				{
					$this->classification_delete($id);
				}
				else
				{
					$this->update_classification($action,$id);
				}
			}
			
			if($_POST['action']==1)
			{
				set_global_messages('Classification Activated Successfully','success');
			}
			if($_POST['action']==0)
			{
				set_global_messages('Classification Deactivated Successfully','success');
			}
			if($_POST['action']==3)
			{
				set_global_messages('Classification Deleted Successfully','success');
				
			}
			//set_global_messages('Classification Deleted Successfully','success');
			
		}
		
		$data['records'] = $this->admin_model->classification($cid);
		$this->admin_template->load('admin_template','classification',$data);
	}
	
	/*
	 * function :To show update classification list
	 * Date : 17th Dec 2014
	 * Author: Gagan David
	 * 
	 */
	
	public function update_classification($action='',$id)
	{
		//echo $action.' '.$id;die;
		$this->admin_model->change_classification_status($action,$id);
	}
	
	
	/*
	 * function :To delete classification
	 * Date : 16th Dec 2014
	 * Author: Gagan David
	 * 
	 */
	public function classification_delete($id,$type='')
	{
		$this->admin_model->classification_delete($id);
		if($type)
		{
			set_global_messages('Classification Deleted Successfully','success');
			redirect('admin/video/classification');
		}
	}

	/*
	 * function :function to get channel list
	 * Date : 17th Dec 2014
	 * Author: Gagan David
	 */
	public function channel($cid='')
	{
		if(isset($_POST['action']))
		{
			$id = $_POST['id'];
			$action =$_POST['action'];
			foreach($id as $id)
			{
				if($action==3)
				{
					$this->channel_delete($id,1);
				}
				else
				{
					$this->channel_update($action,$id);
				}
			}
			
			if($_POST['action']==1)
			{
				set_global_messages('Channel Activated Successfully','success');
			}
			if($_POST['action']==0)
			{
				set_global_messages('Channel Deactivated Successfully','success');
			}
			if($_POST['action']==3)
			{
				set_global_messages('Channel Deleted Successfully','success');
				
			}
			//set_global_messages('Classification Deleted Successfully','success');
			
		}
		$data['records'] = $this->admin_model->channels($cid);
		$this->admin_template->load('admin_template','channel',$data);
	}
	
	
	/*
	 * function :function to add channel
	 * Date : 17th Dec 2014
	 * Author: Gagan David
	 * 
	 */
	 
	 public function channel_form($id='')
	 {
		if(isset($_POST['save']))
		{
			$this->save_channel($id);
		}
		if($id)
		{
			$data['id'] = $id;
			$data['classification'] = $this->admin_model->classification('');
		}
		else
		{
			$data['id'] = '';
			$data['classification'] = $this->admin_model->classification('',1);
		}
		$data['records'] = $this->admin_model->channels($id);
		
		$this->admin_template->load('admin_template','channel_form',$data);
	 }
	 
	 /*
	 * function :function to save channel
	 * Date : 18th Dec 2014
	 * Author: Gagan David
	 * 
	 */
	 
	public function save_channel($id='')
	{
		ini_set('post_max_size', '2000M');
		ini_set('upload_max_filesize', '2000M');
		$data = array();
		$this->form_validation->set_rules('title','title','required|trim|max_length[255]');
		if($id=='')
		{
			$this->form_validation->set_rules('image_value','image','required|trim');
		}
		if ($this->form_validation->run() === FALSE)
		{
			return FALSE;
		}
		
		if($_FILES['image']['name']!='') 
		{
			$image = $this->upload_vedios('','image',1);
			if($image)
			{
				$data['image'] = $image;
			}
			else
			{
				return false;
			}
		}
		$data['title'] = $_POST['title'];
		$data['status'] = $_POST['status'];
		$data['type'] = 'channel';
		
		$classification = $_POST['classification'];
		if($id)
		{	
			$data['action'] = 'U';
			$data['updated_at'] = date('Y-m-d h:i:s');
			$this->admin_model->update_channels($data,$classification,$id);
			set_global_messages('Channel Updated Successfully','success');
			redirect('admin/video/channel');
		}
		else
		{
			$data['action'] = 'A';
			$data['records'] = $this->admin_model->save_channels($data,$classification);
			set_global_messages('Channel Added Successfully','success');
			redirect('admin/video/channel');
		}
	}
	
	 /*
	 * function :function to delete channel
	 * Date : 18th Dec 2014
	 * Author: Gagan David
	 * 
	 */
	 
		public function channel_delete($id='',$type='')
		{
			
			/*Same function can be used to delete classification and channels*/   
			//echo $type;die;
			if(!($type))
			{
				$this->admin_model->classification_delete($id,1);
				set_global_messages('Channel Deleted Successfully','success');
				redirect('admin/video/channel');
			}
			else
			{
				if($type==2)
				{
					$this->admin_model->classification_delete($id,1);
					set_global_messages('Episode Deleted Successfully','success');
					redirect('admin/video/episode');
				}
				else
				{
					return  $this->admin_model->classification_delete($id,1);
				}
			}
		}
		
	/*
	 * function :function to update channel
	 * Date : 18th Dec 2014
	 * Author: Gagan David
	 * 
	 */
	public function channel_update($data='',$id)
	{
		$cha = array();
		$cha['status']=$data;
		$this->admin_model->update_channels($cha,'',$id,1);
	}
	
	/*
	 * function :function to list Episode
	 * Date : 18th Dec 2014
	 * Author: Gagan David
	 * 
	 */
	public function episode()
	{
		if(isset($_POST['action']))
		{
			$id = $_POST['id'];
			$action =$_POST['action'];
			foreach($id as $id)
			{
				if($action==3)
				{
					
					$this->channel_delete($id,1);
				}
				else
				{
					$this->channel_update($action,$id);
				}
			}
			
			if($_POST['action']==1)
			{
				set_global_messages('Serial Activated Successfully','success');
			}
			if($_POST['action']==0)
			{
				set_global_messages('Serial Deactivated Successfully','success');
			}
			if($_POST['action']==3)
			{
				set_global_messages('Serial Deleted Successfully','success');
				
			}
			//set_global_messages('Classification Deleted Successfully','success');
			
		}
		$data['records'] = $this->admin_model->get_episode();
		$this->admin_template->load('admin_template','episode',$data);
	}
	
	/*
	 * function :function to Add Episode
	 * Date : 18th Dec 2014
	 * Author: Gagan David
	 * 
	 */
	public function episode_form($id='')
	{
		if(isset($_POST['save']))
		{
			$this->save_episode($id);
			
		}
		$data['id']=$id;
		$data['classification'] = $this->admin_model->classification('',1);
		$data['channels_list'] = $this->admin_model->channels();
		
		if($id)
		{
			$data['classification'] = $this->admin_model->classification('');
			$data['records'] = $this->admin_model->get_episode($id);
			$parent_id = $data['records']->parent_id;
			$parent_id = $this->admin_model->channels($parent_id);
			$c_id = $parent_id->child_id;
			$pid = $parent_id->parent_id;
			$data['channel_id'] = $c_id;
			$c_id = $this->admin_model->classification($pid);
			$data['channel_select_id'] = $c_id->channel_id;
		}
		$this->admin_template->load('admin_template','episode_form',$data);
	}
	
	/*
	 * function :function to get channel w.r.t classification
	 * Date : 18th Dec 2014
	 * Author: Gagan David
	 * 
	 */
	public function get_channels($type='',$parent_id= '')
	{
		
		$id = $_POST['id'];
		$channels = $this->admin_model->get_channels($id);
		//print_r($channels);
		if($type)
		{
			if(!empty($channels))
			{
				$first_val = $channels[0];
				//print_r($first_val);
				$val = $first_val->channel_id;
				echo $val;
			}
		}
		else
		{
			if(isset($_POST['parent_id']))
			{
				$parent_id = $_POST['parent_id'];
			}
			else
			{
				$parent_id = '';
			}
			$selectbox = '<option value=""> select</option>';
			foreach($channels as $channel)
			{
				if($channel->channel_id== $parent_id)
				{
					$select = 'selected="selected"';
				}
				else
				{
					$select ='';
				}
				$selectbox .= '<option value='.$channel->channel_id.' '.$select.'>'.$channel->title.'</option>';
			}
			echo $selectbox;
		}
	}
	
	/*
	 * function :function to save episode
	 * Date : 18th Dec 2014
	 * Author: Gagan David
	 * 
	 */
	 
	public function save_episode($id='')
	{
		//print_r($_POST);die;
		//die;
		$data = array();
		$this->form_validation->set_rules('title','title','required|trim|max_length[255]');
		if($id=='')
		{
			$this->form_validation->set_rules('image_value','image','required|trim');
		}
		
		if ($this->form_validation->run() === FALSE)
		{
			return FALSE;
		}
		
		if($_FILES['image']['name']!='') 
		{
			$image = $this->upload_vedios('','image',1);
			if($image)
			{
				$data['image'] = $image;
			}
			else
			{
				return false;
			}
		}
		$data['title'] = $_POST['title'];
		$data['status'] = $_POST['status'];
		$data['type'] = 'Episode';
		$data['channel'] = $_POST['channel'];
		$classification = $_POST['classification'];
		if($id)
		{
			$data['action'] = 'U';
			$data['updated_at'] = date('Y-m-d h:i:s');
			$this->admin_model->update_episode($data,$classification,$id);
			set_global_messages('Serial Updated Successfully','success');
			redirect('admin/video/episode');
		}
		else
		{
			$data['action'] = 'A';
			$data['records'] = $this->admin_model->save_episode($data,$classification);
			set_global_messages('Serial Added Successfully','success');
			redirect('admin/video/episode');
		}
		
	}
	
	/*Description : function to upload  video on s3
	 * Author: Gagan David
	 * Date : 22th Dec 2014
	 * 
	 */
	public function s3_upload()
	{
		
		$path = $_FILES['file']['name'];
		$ext = pathinfo($path, PATHINFO_EXTENSION);
		if (!defined('awsAccessKey')) define('awsAccessKey', 'AKIAJTEWZ2RJ2HULK4DA');
		if (!defined('awsSecretKey')) define('awsSecretKey', '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');
		$s3 = new S3(awsAccessKey, awsSecretKey);
		$s3->setEndpoint("s3-ap-southeast-1.amazonaws.com");
		$S3_check = $s3->listBuckets();
		$file_name = date('dmyhis').'.'.$ext;
		$result = $s3->putObjectFile($_FILES['file']['tmp_name'], 'hkair/video', $file_name);
		
		$bucket = 'hkair/video';
		$fileurl = $s3->getAuthenticatedURL($bucket, trim($file_name), 300, $hostBucket = false, $https = false);
		
		$filename = pathinfo($file_name, PATHINFO_FILENAME).'_thumb.jpg'; 
		$Image_target_path = $_SERVER['DOCUMENT_ROOT'].'/hkair/PHP/cms/uploads/'.pathinfo($file_name, PATHINFO_FILENAME).'_thumb.jpg'; 
		$size_100_100 = '100x100';
		$output ='';
		$size_100_100 = '100x100';
		$time_get = "00:00:02";
		$command= "ffmpeg -i '".$fileurl."' -an -s '".$size_100_100."' -ss '".$time_get."' -r 1 -y '".$Image_target_path."'";
		
		exec($command." 2>&1", $output);
		$bucket = 'hkair/video_thumbnail';
		$result = $s3->putObject($s3->inputFile($Image_target_path, false), $bucket , $filename);
		
		echo trim($file_name);
	}
	
	/*
	 * Description : function for video thumbnail
	 * Author: Gagan David
	 * Date : 2nd Jan 2015
	 */ 
	 
	public function thumbnail_video()
	{
		if (!defined('awsAccessKey')) define('awsAccessKey', 'AKIAJTEWZ2RJ2HULK4DA');
		if (!defined('awsSecretKey')) define('awsSecretKey', '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');
		$s3 = new S3(awsAccessKey, awsSecretKey);
		
		$s3->setEndpoint("s3-ap-southeast-1.amazonaws.com");
		$S3_check = $s3->listBuckets();
		
		$bucket = 'hkair/video';
		$fileurl = $s3->getAuthenticatedURL($bucket,'060115062032.mp4', '86400', $hostBucket = false, $https = false);
		echo $fileurl;
	
		$Image_target_path = $_SERVER['DOCUMENT_ROOT'].'/hkair/PHP/cms/uploads/060115062032_thumb.jpg'; 
		
		$output ='';
		$size_100_100 = '100x100';
		$time_get = "00:00:02";
		$command= "ffmpeg -i '".$fileurl."' -an -s '".$size_100_100."' -ss '".$time_get."' -r 1 -y '".$Image_target_path."'";
		//echo $command;
		exec($command." 2>&1", $output);
		
		$bucket = 'hkair/video_thumbnail';
		$s3->setEndpoint("s3-ap-southeast-1.amazonaws.com");
		$S3_check = $s3->listBuckets();
		$filename ='gagans_thumbs.jpg';
		$result = $s3->putObject($s3->inputFile($Image_target_path, false), $bucket, $filename);
		echo $result;
		//unlink($Image_target_path);
	}
	
	/*
	 * Description : function for s3 url
	 * Author: Gagan David
	 * Date : 5th Jan 2015
	 */ 
	 
	public function get_url()
	{
		if (!defined('awsAccessKey')) define('awsAccessKey', 'AKIAJJ2S4GPS7ACUJ5RA');
		if (!defined('awsSecretKey')) define('awsSecretKey', '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');
		$s3 = new S3(awsAccessKey, awsSecretKey);
		$s3->setEndpoint("s3-ap-southeast-1.amazonaws.com");
		$S3_check = $s3->listBuckets();
		$bucket = 'managevideo';
		$fileurl = $s3->getAuthenticatedURL($bucket,'030115011623.mp4','86400', $hostBucket = false, $https = false);
		echo $fileurl;
	}
	
	public function put_s3()
	{
		if (!defined('awsAccessKey')) define('awsAccessKey', 'AKIAJTEWZ2RJ2HULK4DA');
		if (!defined('awsSecretKey')) define('awsSecretKey', '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');
		$s3 = new S3(awsAccessKey, awsSecretKey);
		$s3->setEndpoint("s3-ap-southeast-1.amazonaws.com");
		$S3_check = $s3->listBuckets();
		print_r($S3_check);
		$file_name = 'testing_link.mp4';
		$Image_target_path = $_SERVER['DOCUMENT_ROOT'].'/hkair/PHP/cms/uploads/testing_link.mp4'; 
		$result = $s3->putObject($Image_target_path, 'hkair/video', $file_name);
		echo $result;
	}
	
	
	/*
	 * Description : function for listing episode
	 * Author: Gagan David
	 * Date : 15th Jan 2015
	 */ 
	public function episode_list()
	{
		$data['classification'] = $this->admin_model->classification('',1);
		$data['channels_list'] = $this->admin_model->channels();
		$data['episode'] = $this->admin_model->get_episode('',1);
		$this->admin_template->load('admin_template','episode_list',$data);
	}
	
	
	
	
}
