<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Admin_model extends CI_model{
	function __construct() {
		parent::__construct();
	}
		/*****************************Company Section Start*****************************/

	function get_company($id='',$search="",$length=0,$start=0,$orderby="",$column_name="") {
		$prefix = $this->db->dbprefix;
		$this->db->select('*');
		$this->db->where('is_delete','0');
		if($id) {
			$this->db->where('company_id',$id);
			$query = $this->db->get($prefix.'companies');
			return $query->row();
		}
		if($length!=0) {
			$this->db->limit($length,$start);
		 }
		if($search !=""){
			$this->db->where("(company_name LIKE '%".$search."%' OR company_address LIKE '%".$search."%')");
		}
		if($column_name!=""){
				$this->db->order_by($column_name, $orderby);
		}else{
			$this->db->order_by("created_at", "desc");
		}
		$query = $this->db->get($prefix.'companies');
		return $query->result();
	}

	function check_company($name) {
		$prefix = $this->db->dbprefix;
		$this->db->select('company_name');
		$this->db->where('company_name',$name);
		$this->db->where('is_delete','0');
		$query = $this->db->get($prefix.'companies');
		//echo $this->db->last_query(); die;
		return $query->row();
	}

	function add_company($data='') {
		$prefix = $this->db->dbprefix;
		return $this->db->insert($prefix.'companies', $data);
	}

	function update_company($id='',$data='') {
		$prefix = $this->db->dbprefix;
		$this->db->where('company_id', $id);

		return $this->db->update($prefix.'companies' ,$data);
	}
	/*
	function company_delete($id='',$data='') {
		$prefix = $this->db->dbprefix;
		$this->db->where('company_id', $id);
		return $this->db->update($prefix.'companies' ,$data);
	} */

	/*****************************Company Section End*****************************/

	/*****************************Headquater Section Start*****************************/

	function get_headquater($id='',$search="",$length=0,$start=0,$orderby="",$column_name="") {
		$prefix = $this->db->dbprefix;
		$this->db->select($prefix.'headquater_manager.*');
		$this->db->select($prefix.'companies.company_name');
		$this->db->join($prefix.'companies', $prefix.'companies.company_id = headquater_manager.company_id');
		$this->db->where($prefix.'headquater_manager.is_delete','0');
		if($id) {
			$this->db->where($prefix.'headquater_manager.hq_manager_id',$id);
			$query = $this->db->get($prefix.'headquater_manager');
			return $query->row();
		}
		if($length!=0) {
			$this->db->limit($length,$start);
		 }
		if($search !=""){
			$this->db->where("(".$prefix."companies.company_name LIKE '%".$search."%' OR ".$prefix."headquater_manager.email_id LIKE '%".$search."%' OR ".$prefix."headquater_manager.contact_no LIKE '%".$search."%' OR ".$prefix."headquater_manager.first_name LIKE '%".$search."%' OR " .$prefix."headquater_manager.last_name LIKE '%".$search."%')");
		}
		if($column_name!=""){
				$this->db->order_by($column_name, $orderby);
		}else{
			$this->db->order_by($prefix.'headquater_manager.created_at', "desc");
		}
		$query = $this->db->get($prefix.'headquater_manager');
		//echo $this->db->last_query(); die;
		return $query->result();

	}

	function get_selected_company() {
		$prefix = $this->db->dbprefix;
		$this->db->select('company_id,hq_manager_id');
		$this->db->where('is_delete','0');
		$query = $this->db->get($prefix.'headquater_manager');
		return $query->result();
	}

	function get_not_selected_company($company) {
		$prefix = $this->db->dbprefix;
		$this->db->select('company_id ,company_name');
		$this->db->where('is_delete','0');
		$this->db->where('status','1');

		if(!empty($company)){
			$this->db->where_not_in('company_id', $company);
		}
		$this->db->order_by("company_name", "asc");
		$query = $this->db->get($prefix.'companies');
		return $query->result();
	}

	function add_headquater($data='') {
		$prefix = $this->db->dbprefix;
		return $this->db->insert($prefix.'headquater_manager', $data);
	}

	function update_headquater($id='',$data='') {
		$prefix = $this->db->dbprefix;
		$this->db->where('hq_manager_id', $id);
		return $this->db->update($prefix.'headquater_manager' ,$data);

	}
	/*****************************Headquater Section End*****************************/

	/*****************************Outlet Section Start*****************************/

	function check_outlet($name,$id=''){
		$prefix = $this->db->dbprefix;
		$this->db->select('outlet_name');
		$this->db->where('outlet_name',$name);
		$this->db->where('is_delete','0');
		if($id != 0){
			$this->db->where_not_in('outlet_id',$id);
		}
		$query = $this->db->get($prefix.'outlets');
		//echo $this->db->last_query(); die;
		return $query->row();
	}

	function get_outlet($id='',$search_str="",$length=0,$start=0,$orderBy="",$column_name="") {
		//echo $search_str; die;
		$area_manager_detail = getAreaManagerId(LoginUserDetails('userid'));
		$prefix = $this->db->dbprefix;
		$this->db->select($prefix.'outlets.*');
		$this->db->select($prefix.'companies.company_name');
		$this->db->join($prefix.'outlets', $prefix.'outlets.company_id = companies.company_id');
		$this->db->where($prefix.'outlets.is_delete','0');
		if(LoginUserDetails('role_id') != 1) {
			$this->db->where($prefix.'outlets.company_id',LoginUserDetails('company_id'));
		}
		if(LoginUserDetails('role_id') == 5) {
			$this->db->where($prefix.'outlets.area_manager_id',$area_manager_detail[0]->area_manager_id);
		}
		if($length!=0) {
			$this->db->limit($length,$start);
		 }
		/* Added By Kratika Later */
		if(LoginUserDetails('role_id') == 1 && $search_str!=""){
			$this->db->where("(".$prefix."outlets.outlet_name LIKE '%".$search_str."%' OR ".$prefix."outlets.outlet_address LIKE '%".$search_str."%' OR ".$prefix."companies.company_name LIKE '%".$search_str."%')");
		}elseif((LoginUserDetails('role_id') == 2 || LoginUserDetails('role_id') == 5) && $search_str!=""){
			$this->db->where("(".$prefix."outlets.outlet_name LIKE '%".$search_str."%' OR ".$prefix."outlets.outlet_address LIKE '%".$search_str."%')");
		}
		if($column_name != ""){
			$this->db->order_by($column_name, $orderBy);
		}

		if($id) {
			$this->db->where($prefix.'outlets.outlet_id',$id);
			$query = $this->db->get($prefix.'companies');
			return $query->row();
		}
		$this->db->order_by($prefix.'outlets.created_at', "desc");
		$query = $this->db->get($prefix.'companies');
		//echo $this->db->last_query(); die;
		return $query->result();

	}

	function get_enable_outlet($id='') {
		$prefix = $this->db->dbprefix;
		$this->db->select($prefix.'outlets.*');
		$this->db->select($prefix.'companies.company_name');
		$this->db->join($prefix.'outlets', $prefix.'outlets.company_id = companies.company_id');
		$this->db->where($prefix.'outlets.is_delete','0');
		$this->db->where($prefix.'outlets.status','1');
		$this->db->where($prefix.'outlets.company_id',LoginUserDetails('company_id'));
		if($id) {
			$this->db->where($prefix.'outlets.outlet_id',$id);
			$query = $this->db->get($prefix.'companies');
			return $query->row();
		}
		$this->db->order_by($prefix.'outlets.created_at', "desc");
		$query = $this->db->get($prefix.'companies');
		//echo $this->db->last_query(); die;
		return $query->result();
	}

	function add_outlet($data='') {
		$prefix = $this->db->dbprefix;
		$this->db->insert($prefix.'outlets', $data);
		$insert_id = $this->db->insert_id();
		return  $insert_id;
	}

	function update_outlet($id='',$data='') {
		$prefix = $this->db->dbprefix;
		$this->db->where('outlet_id', $id);
		return $this->db->update($prefix.'outlets' ,$data);

	}
	/*****************************Outlet Section End*****************************/

	/*****************************Outlet Manager Section Start*****************************/
	function get_outlet_manager($id='',$search_str="",$length=0,$start=0,$orderBy="",$column_name="") {
		$prefix = $this->db->dbprefix;
		$this->db->select($prefix.'outlet_managers.*');
		$this->db->select($prefix.'outlets.outlet_name');
		$this->db->join($prefix.'outlets', $prefix.'outlets.outlet_id = outlet_managers.outlet_id','left outer');
		$this->db->where($prefix.'outlet_managers.is_delete','0');
		$this->db->where($prefix.'outlet_managers.company_id',LoginUserDetails('company_id'));
		if($id) {
			$this->db->where($prefix.'outlet_managers.outlet_manager_id',$id);
			$query = $this->db->get($prefix.'outlet_managers');
			return $query->row();
		}
		if($column_name != ""){
			$this->db->order_by($column_name, $orderBy);
		}
		if($length!=0) {
			$this->db->limit($length,$start);
		 }
		 if($search_str !=""){
			$this->db->where("(".$prefix."outlet_managers.first_name LIKE '%".$search_str."%' OR  ".$prefix."outlet_managers.last_name LIKE '%".$search_str."%' OR ".$prefix."outlet_managers.email_id LIKE '%".$search_str."%' OR ".$prefix."outlet_managers.contact_no LIKE '%".$search_str."%' OR ".$prefix."outlets.outlet_name LIKE '%".$search_str."%')");
		}
		$this->db->order_by("created_at", "desc");
		$query = $this->db->get($prefix.'outlet_managers');
		//echo $this->db->last_query(); die;
		return $query->result();
	}

	function add_outlet_manager($data='') {
		$prefix = $this->db->dbprefix;
		return $this->db->insert($prefix.'outlet_managers', $data);
	}

	function update_outlet_manager($id='',$data='') {
		$prefix = $this->db->dbprefix;
		$this->db->where('outlet_manager_id', $id);
		return $this->db->update($prefix.'outlet_managers' ,$data);

	}

	function get_unassigns_outlet_manager($id='') {
		$prefix = $this->db->dbprefix;
		$this->db->select('*');
		$this->db->where('is_delete','0');
		$this->db->where('outlet_id','0');
		$this->db->where($prefix.'outlet_managers.company_id',LoginUserDetails('company_id'));
		if($id) {
			$this->db->where('outlet_manager_id',$id);
			$query = $this->db->get($prefix.'outlet_managers');
			return $query->row();
		}
		$this->db->order_by("created_at", "desc");
		$query = $this->db->get($prefix.'outlet_managers');
		//echo $this->db->last_query(); die;
		return $query->result();
	}

	function job_assigned_to_outlet_manager($userAccountsId=''){
		$prefix = $this->db->dbprefix;
		$this->db->select('*');
		$this->db->where('outlet_manager_id',$userAccountsId);
		// status : job is active or not
		// is_delete : job is delete or not
		//is_completed = 0 : job is approved and applicant start to apply for job or applicant is working on job
		$this->db->where(array('is_completed '=> 0,'is_delete'=>0,'status'=>0));    //is_completed = 0 : job is approved and applicant start to apply for job or applicant is working on job
		$this->db->where_in('is_approved ', array(1,0));  // outlet/ area manager creat the job but no approve
		$query = $this->db->get($prefix.'jobs');
		$this->db->last_query();
		return $query->num_rows();
	}
	/*****************************Outlet Manager Section End*****************************/

	/*****************************Area Manager Section Start*****************************/

	function get_area_manager($id='',$search_str="",$length=0,$start=0,$orderBy="",$column_name="") {
		$prefix = $this->db->dbprefix;
		$this->db->select('*');
		$this->db->where('is_delete','0');
		$this->db->where('company_id',LoginUserDetails('company_id'));
		if($id) {
			$this->db->where('area_manager_id',$id);
			$query = $this->db->get($prefix.'area_manager');
			return $query->row();
		}
		if($column_name != ""){
			$this->db->order_by($column_name, $orderBy);
		}
		if($length!=0) {
			$this->db->limit($length,$start);
		 }
		if($search_str !=""){
			$this->db->where("(first_name LIKE '%".$search_str."%' OR  last_name LIKE '%".$search_str."%' OR email_id LIKE '%".$search_str."%' OR contact_no LIKE '%".$search_str."%')");
		}
		$this->db->order_by("created_at", "desc");
		$query = $this->db->get($prefix.'area_manager');
		//echo $this->db->last_query(); die;
		return $query->result();
	}

	function get_enable_area_manager($id='') {
		$prefix = $this->db->dbprefix;
		$this->db->select('*');
		$this->db->where('is_delete','0');
		$this->db->where('status','1');
		$this->db->where('company_id',LoginUserDetails('company_id'));
		if($id) {
			$this->db->where('area_manager_id',$id);
			$query = $this->db->get($prefix.'area_manager');
			return $query->row();
		}
		$this->db->order_by("created_at", "desc");
		$query = $this->db->get($prefix.'area_manager');
		//echo $this->db->last_query(); die;
		return $query->result();
	}

	function add_area_manager($data='') {
		$prefix = $this->db->dbprefix;
		$this->db->insert($prefix.'area_manager', $data);
		return $this->db->insert_id();
	}

	function update_area_manager($id='',$data='') {
		$prefix = $this->db->dbprefix;
		$this->db->where('area_manager_id', $id);
		return $this->db->update($prefix.'area_manager' ,$data);

	}
	/*****************************Area Manager Section End*****************************/

	/*****************************Job Type Section start*****************************/
	function check_job_type($name,$id='') {
		$prefix = $this->db->dbprefix;
		$this->db->select('name');
		$this->db->where('name',$name);
		$this->db->where('is_delete','0');
		if($id != 0){
			$this->db->where_not_in('job_role_id',$id);
		}
		$query = $this->db->get($prefix.'job_roles');
		//echo $this->db->last_query(); die;
		return $query->row();
	}

	function get_job_type($id='',$search="",$length=0,$start=0,$orderby="") {
		$prefix = $this->db->dbprefix;
		$this->db->select('*');
		$this->db->where('is_delete','0');
		if($id) {
			$this->db->where('job_role_id',$id);
			$query = $this->db->get($prefix.'job_roles');
			return $query->row();
		}
		if($length!=0) {
			$this->db->limit($length,$start);
		 }
		if($search !=""){
			$this->db->like('name', $search);
		}
		if($orderby !=""){
			$this->db->order_by("name",$orderby);
		}else{
			$this->db->order_by("created_at", "desc");
		}
		$query = $this->db->get($prefix.'job_roles');

		//echo $this->db->last_query(); die;
		return $query->result();
	}

	function add_job_type($data='') {
		$prefix = $this->db->dbprefix;
		return $this->db->insert($prefix.'job_roles', $data);
	}

	function update_job_type($id='',$data='') {
		$prefix = $this->db->dbprefix;
		$this->db->where('job_role_id', $id);
		return $this->db->update($prefix.'job_roles' ,$data);
	}
	/*****************************Job Type Section End*****************************/

	/*****************************Applicant List Section Start*****************************/
	function get_applicant($id='',$search="",$length=0,$start=0,$orderby="") {
		$prefix = $this->db->dbprefix;
		$this->db->select($prefix.'applicants.*');
		$this->db->select($prefix.'user_accounts.unique_id');
		$this->db->join($prefix.'user_accounts', $prefix.'applicants.user_account_id = user_accounts.user_accounts_id');
		//	$this->db->where($prefix.'applicants.is_delete','0');
		if($id) {
			$this->db->where($prefix.'applicants.applicant_id',$id);
			$query = $this->db->get($prefix.'applicants');
			return $query->row();
		}
		if($length!=0) {
			$this->db->limit($length,$start);
		 }
		if($search !=""){
			$this->db->where("(".$prefix."applicants.display_name LIKE '%".$search."%' OR ".$prefix."applicants.first_name LIKE '%".$search."%' OR ".$prefix."applicants.last_name LIKE '%".$search."%' OR ".$prefix."applicants.email_id LIKE '%".$search."%' OR ".$prefix."user_accounts.unique_id LIKE '%".$search."%')");
			
		}
		$this->db->order_by($prefix.'applicants.created_at', "desc");
		$query = $this->db->get($prefix.'applicants');
		//echo $this->db->last_query(); die;
		return $query->result();
	}

	function add_applicant($data='') {
		$prefix = $this->db->dbprefix;
		$this->db->insert($prefix.'applicants', $data);
		return $this->db->insert_id();
	}

	function update_applicant($id='',$data='') {
		$prefix = $this->db->dbprefix;
		$this->db->where('applicant_id', $id);
		return $this->db->update($prefix.'applicants' ,$data);

	}
	/*****************************Applicant List Section End*****************************/


	/*****************************User Account Section Start****************************/

	function add_account($data='') {
		$prefix = $this->db->dbprefix;
		$this->db->insert($prefix.'user_accounts', $data);
		return $this->db->insert_id();
	}
	function update_account($id='',$data='') {
		$prefix = $this->db->dbprefix;
		$this->db->where('user_accounts_id', $id);
		return $this->db->update($prefix.'user_accounts' ,$data);
		//echo $this->db->last_query(); die;
	}

	function get_profile_details($id='',$userType='') {
		$prefix = $this->db->dbprefix;
		$this->db->select('*');
		$this->db->where('is_delete','0');
		$this->db->where('user_accounts_id',$id);
		if($userType == 1) {
			$query = $this->db->get($prefix.'superadmin');
		} else if($userType == 2) {
			$query = $this->db->get($prefix.'headquater_manager');
		} else if($userType == 3) {
			$query = $this->db->get($prefix.'outlet_managers');
		} else if($userType == 5) {
			$query = $this->db->get($prefix.'area_manager');
		}
		//
		//echo $this->db->last_query(); die;
		return $query->row();
	}

	function update_superadmin_profile($id='',$data='') {
		$prefix = $this->db->dbprefix;
		$this->db->where('user_accounts_id', $id);
		return $this->db->update($prefix.'superadmin' ,$data);
	}

	function update_headquater_profile($id='',$data='') {
		$prefix = $this->db->dbprefix;
		$this->db->where('user_accounts_id', $id);
		return $this->db->update($prefix.'headquater_manager' ,$data);
	}

	function update_outlet_manager_profile($id='',$data='') {
		$prefix = $this->db->dbprefix;
		$this->db->where('user_accounts_id', $id);
		return $this->db->update($prefix.'outlet_managers' ,$data);
	}

	function update_area_manager_profile($id='',$data='') {
		$prefix = $this->db->dbprefix;
		$this->db->where('user_accounts_id', $id);
		return $this->db->update($prefix.'area_manager' ,$data);
	}

	/*****************************User Account Section End*****************************/

	/*****************************Education Institute Section start*****************************/
	function check_institute($name,$id='') {
		$prefix = $this->db->dbprefix;
		$this->db->select('name');
		$this->db->where('name',$name);
		$this->db->where('is_delete','0');
		if($id != 0){
			$this->db->where_not_in('education_institutes_id',$id);
		}
		$query = $this->db->get($prefix.'education_institutes');
		//echo $this->db->last_query(); die;
		return $query->row();
	}

	function get_education_institutes($id='',$active='',$search="",$length=0,$start=0,$orderby="") {
		//echo $orderby; die;
		$prefix = $this->db->dbprefix;
		$this->db->select('*');
		$this->db->where('is_delete','0');
		if($active) {
			if($active == 0) {
				$this->db->where('status','0');
			} else if($active == 1) {
				$this->db->where('status','1');
			}
		}
		if($length!=0) {
			$this->db->limit($length,$start);
		 }
		if($search !=""){
			$this->db->like('name', $search);
		}
		if($id) {
			$this->db->where('education_institutes_id',$id);
			$query = $this->db->get($prefix.'education_institutes');
			return $query->row();
		}

		if($orderby !=""){
			$this->db->order_by("name",$orderby);
		}else{
			$this->db->order_by("name", "asc");
		}
		$query = $this->db->get($prefix.'education_institutes');
		//echo $this->db->last_query(); die;
		return $query->result();
	}

	function add_institute($data='') {
		$prefix = $this->db->dbprefix;
		return $this->db->insert($prefix.'education_institutes', $data);
	}

	function update_institute($id='',$data='') {
		$prefix = $this->db->dbprefix;
		$this->db->where('education_institutes_id', $id);
		return $this->db->update($prefix.'education_institutes' ,$data);

	}
	/*****************************Education Institute Section End*****************************/



	function get_billing_records($label='',$month='',$year='',$outlet='',$search="",$length=0,$start=0,$orderby="",$column_name="") {
		$str = "";
		$prefix = $this->db->dbprefix;
		if($label=='outlet'){
			$this->db->select($prefix.'outlets.*');
			$this->db->group_by($prefix."outlets.outlet_id");
		}else{
			$this->db->select($prefix.'jobs.*');
			$this->db->select($prefix.'jobs.created_at as job_created_at');
			$this->db->select($prefix.'outlets.*');
		}
		if(!empty($month)){
			if($month<10){
				$str .= "0"; //If the number is below 10, it will add a leading zero
				$str .= $month;
			}else{
				$str = $month;
			}
			$this->db->like($prefix.'jobs.created_at', '-'.$str.'-', 'both');
			//$this->db->where('MONTH("jobs.created_at")',$month);
		}

		if($column_name!=""){
				$this->db->order_by($column_name, $orderby);
		}else{
			$this->db->order_by("jod_jobs.created_at", "desc");
		}
		if(!empty($year)){
			$this->db->like($prefix.'jobs.created_at', $year.'-', 'after');
		}
		if(!empty($outlet)){
			$this->db->where($prefix.'outlets.outlet_id',$outlet);
		}
		if($search != ""){
			$this->db->where("(".$prefix."outlets.outlet_name LIKE '%".$search."%' OR ".$prefix."jobs.job_title LIKE '%".$search."%')");
		}
		if($length!=0) {
			$this->db->limit($length,$start);
		}
		$this->db->join($prefix.'outlets', $prefix.'jobs.outlet_id = outlets.outlet_id');
		//$this->db->where($prefix.'jobs.is_delete','0');
		$this->db->where($prefix.'jobs.is_approved','1');

		$query = $this->db->get($prefix.'jobs');
			//echo $this->db->last_query(); die;
		return $query->result();
	}

	function get_jobs_outlet_manager($outlet_id,$outlet_manager_id){
		$outlet_manager_data = getOutletManagerById($outlet_manager_id);
		$outlet_manager_user_account_id = $outlet_manager_data[0]->user_accounts_id;
		$prefix = $this->db->dbprefix;
		$this->db->select('*');
		$this->db->where('outlet_id', $outlet_id);
		$this->db->where('created_by', $outlet_manager_user_account_id);
		$this->db->or_where('outlet_manager_id', $outlet_manager_user_account_id);
		$this->db->where('is_completed',0);
		$this->db->where('is_delete', 0);
		//$this->db->where('is_approved', 1);
		$query = $this->db->get($prefix.'jobs');
		return $query->result();
	}

	function get_outlet_jobs($outlet_id,$area_manager_id){
		$area_manager_details = areaManagerDetails($area_manager_id);
		$area_manager_user_account_id = $area_manager_details[0]->user_accounts_id;
		$prefix = $this->db->dbprefix;
		$this->db->select('*');
		$this->db->where('outlet_id', $outlet_id);
		$this->db->where('created_by', $area_manager_user_account_id);
		$this->db->where('is_completed', 0);
		$this->db->where('is_delete', 0);
		//$this->db->where('is_approved', 1);
		$query = $this->db->get($prefix.'jobs');
		return $query->result();
	}
	
	function update_language($user_account_id,$langauge_id){
		$data = array('language_id'=>$langauge_id);
		$prefix = $this->db->dbprefix;
		$this->db->where('user_accounts_id', $user_account_id);
		return $this->db->update($prefix.'user_accounts' ,$data);		
	}
	
	function get_user_langauage($user_accounts_id){
		$prefix = $this->db->dbprefix;
		$this->db->select('language_id');
		$this->db->where('user_accounts_id',$user_accounts_id);
		$query = $this->db->get($prefix.'user_accounts');
		return $query->row();
		
	}
}
?>
