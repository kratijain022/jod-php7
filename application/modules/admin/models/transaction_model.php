<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Transaction_model extends CI_model{


	function __construct(){

		parent::__construct();

	}

	public function to_get_activity_types(){
		$prefix = $this->db->dbprefix;
		$this->db->select('*');
		$query = $this->db->get($prefix.'trasaction_activity_types');
		return $query->result();
	}

	function add_transaction($data='') {
		$prefix = $this->db->dbprefix;
		$this->db->insert($prefix.'transactions', $data);
		 return $this->db->insert_id();
	}


	function get_company_transaction_history($company_id="",$search="",$length=0,$start=0,$dirs="",$column_name=""){
		$prefix = $this->db->dbprefix;
		$this->db->select('transactions.*');
		if($company_id != ""){
			$this->db->select($prefix.'companies.company_name');
			$this->db->join($prefix.'companies', $prefix.'companies.company_id = '.$prefix.'transactions.company_id');
			$this->db->where('companies.company_id',$company_id);
		}else{
			$this->db->select($prefix.'companies.company_name');
			$this->db->join($prefix.'companies', $prefix.'companies.company_id = '.$prefix.'transactions.company_id');
		}
		if($length!=0) {
			$this->db->limit($length,$start);
		}
		if($search !=""){
			$this->db->where("(".$prefix."companies.company_name LIKE '%".$search."%' OR ".$prefix."transactions.jod_credit_amount LIKE '%".$search."%' OR ".$prefix."transactions.comment_text LIKE '%".$search."%')");
		}
		if($column_name!=""){
			$this->db->order_by($column_name, $dirs);
		}else{
			$this->db->order_by("created_at", "desc");
		}
		$this->db->where('assigned_by_manager','1');
		$this->db->where('transaction_level','1');
		$this->db->where_in('activity_type',array("1","2"));
		$this->db->order_by("assigned_at","desc");
		//$this->db->limit(1000);
		$query = $this->db->get($prefix.'transactions');
		//echo $this->db->last_query(); die;
		return $query->result();
	}

	function update_current_JOD_credit($data,$company_id){
		$prefix = $this->db->dbprefix;
		$this->db->where('company_id', $company_id);
		return $this->db->update($prefix.'companies' ,$data);
	}

	function update_current_JOD_credit_outlet($data,$outlet_id){
		$prefix = $this->db->dbprefix;
		$this->db->where('outlet_id', $outlet_id);
		return $this->db->update($prefix.'outlets' ,$data);
	}


	function get_outelt_transaction_history($outet_id="",$hq_manager_id,$search="",$length=0,$start=0,$orderBy="",$column_name=""){
		$prefix = $this->db->dbprefix;
		$this->db->select('transactions.*');
		$this->db->select($prefix.'outlets.outlet_name');
		$this->db->join($prefix.'outlets', $prefix.'outlets.outlet_id = '.$prefix.'transactions.outlet_id');
		if($outet_id != ""){
			$this->db->where('outlets.outlet_id',$outet_id);
		}if($length!=0) {
			$this->db->limit($length,$start);
		}
		if($search !=""){
			$this->db->where("(".$prefix."outlets.outlet_name LIKE '%".$search."%' OR ".$prefix."transactions.jod_credit_amount LIKE '%".$search."%' OR ".$prefix."transactions.comment_text LIKE '%".$search."%')");
		}
		if($column_name!=""){
			$this->db->order_by($column_name, $orderBy);
		}else{
			$this->db->order_by("created_at", "desc");
		}
		$this->db->where('assigned_by_manager',$hq_manager_id);
		$this->db->where('transaction_level','2');
		$this->db->where_in('activity_type',array("3","4"));
		$this->db->order_by("assigned_at","desc");
		$this->db->limit(1000);
		$query = $this->db->get($prefix.'transactions');
		//echo $this->db->last_query(); die;
		return $query->result();
	}

	function get_outlet_transaction_data($outlet_ids,$manager_id,$level,$hq_manager_id,$search="",$length=0,$start=0,$orderBy="",$column_name=""){
		$outlet_id_arr = explode(",",$outlet_ids);
		$prefix = $this->db->dbprefix;
		$this->db->select('transactions.*');
		$this->db->select($prefix.'outlets.outlet_name');
		$this->db->join($prefix.'outlets', $prefix.'outlets.outlet_id = '.$prefix.'transactions.outlet_id');
		if($outlet_ids != ""){
			$this->db->where_in('outlets.outlet_id',$outlet_id_arr);
		}
		if($length!=0) {
			$this->db->limit($length,$start);
		}
		if($search !=""){
			$this->db->where("(".$prefix."outlets.outlet_name LIKE '%".$search."%' OR ".$prefix."transactions.jod_credit_amount LIKE '%".$search."%' OR ".$prefix."transactions.comment_text LIKE '%".$search."%')");
		}
		if($column_name!=""){
			$this->db->order_by($column_name, $orderBy);
		}else{
			$this->db->order_by("created_at", "desc");
		}
		$this->db->where('assigned_by_manager',$hq_manager_id);
		$this->db->where('transaction_level','2');
		$this->db->where_in('activity_type',array("3","4"));
		$this->db->order_by("assigned_at","desc");
		$this->db->limit(100);
		$query = $this->db->get($prefix.'transactions');
		//echo $this->db->last_query(); die;
		return $query->result();
	}


}
