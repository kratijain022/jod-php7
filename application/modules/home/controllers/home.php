<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MX_Controller{

	function __construct(){
		parent::__construct();
		$this->load->library(array('home_template','head'));
		$this->load->language(array('common'));
		$this->load->model('home_model');

	}
    function index() {
		if(loggedId()) {
			redirect('admin');
		}
		$data['userEmailCookie']	=$this->input->cookie('loginUsername');
		$data['userPassCookie']		=$this->input->cookie('loginPassword');
		$data['userRememberCookie']		=$this->input->cookie('loginRemember');
		$this->home_template->load('home_template','login',$data);
	}

	function admin_login() {
		$this->loginValidate();
		$login_= new stdClass();
		if($_POST){
			$this->form_validation->set_rules($this->loginValidate());
			if ($this->form_validation->run() == TRUE) {
				if (!$this->_checkuserauth()) {
					set_global_messages('Invalid username or password.','error');
				}
				redirect('home/index');
			}
		}
		$this->home_template->load('home_template','login');
	}
	private function loginValidate() {
		//set validation rules
		return $validation_rules = array(
			array(
				'field' => 'username',
				'label' => 'username',
				'rules' => 'trim|required|max_length[100]|'
			),
			array(
				'field' => 'password',
				'label' => 'Password',
				'rules' => 'trim|required|max_length[100]'
			),
		);
	}

	private function _checkuserauth() {
		$username=$this->input->post('username');
		$password=$this->input->post('password');
		$passwordMd5 = md5($password);
		if($userdata=$this->home_model->checkuserexit($username,$passwordMd5)) {
			
			if($userdata->role_id == '3') {
				//$this->session->sess_expiration = '0';
			}
			$this->session->set_userdata('userid',$userdata->user_accounts_id);
			$this->session->set_userdata('unique_id',$userdata->unique_id);
			$this->session->set_userdata('role_id',$userdata->role_id);
			$this->session->set_userdata('email',$userdata->email_id);
			if($userdata->language_id == 1){
				$language = 'english';		
			}else{
				$language = 'chinese';		
			}
			$this->session->set_userdata('language',$language);
			$this->setLoggedInUserCookie();
			if($userdata->role_id == '2') {
				$data=$this->home_model->get_company_id($userdata->user_accounts_id);
				$company =$this->home_model->get_company_details($data->company_id);
				$status = $company[0]->status ;
				// check company is enabled or not
				if($status == 1){
					$this->session->set_userdata('company_id',$data->company_id);
					$this->session->set_userdata('user_first_name',$data->first_name);
					$this->session->set_userdata('user_last_name',$data->last_name);
				}else{
						$this->session->unset_userdata('userid');
						$this->session->unset_userdata('unique_id');
						$this->session->unset_userdata('role_id');
						$this->session->unset_userdata('email');
						return false;
				}
			}else if($userdata->role_id == '3') {
				// user is outlet Manager
				$data=$this->home_model->get_outlet_details($userdata->user_accounts_id);
				$company =$this->home_model->get_company_details($data->company_id);
				$comany_status = $company[0]->status ;
				if($comany_status == 1) {
					// check HQ Manager is enabled or not
					$status = $this->home_model->get_status_HQ_Manager($data->company_id);
					if($status == true){
						if($data->outlet_id == 0) {
							$this->session->unset_userdata('userid');
							$this->session->unset_userdata('unique_id');
							$this->session->unset_userdata('role_id');
							$this->session->unset_userdata('email');
							return false;
						}
						$this->session->set_userdata('company_id',$data->company_id);
						$this->session->set_userdata('user_first_name',$data->first_name);
						$this->session->set_userdata('user_last_name',$data->last_name);
						$this->session->set_userdata('user_outlet_name',$data->outlet_name);
						$this->session->set_userdata('user_outlet_id',$data->outlet_id);
					}else{
						// when HQ Manager is disable
						$this->session->unset_userdata('userid');
						$this->session->unset_userdata('unique_id');
						$this->session->unset_userdata('role_id');
						$this->session->unset_userdata('email');
						return false;
					}
				}else{
					// when HQ Manager is disable
						$this->session->unset_userdata('userid');
						$this->session->unset_userdata('unique_id');
						$this->session->unset_userdata('role_id');
						$this->session->unset_userdata('email');
						return false;
				}
			}
			else if($userdata->role_id == '5'){
				// check HQ Manager is enabled or not
				$company_deatils =$this->home_model->get_company_id($userdata->user_accounts_id);
				$company =$this->home_model->get_company_details($company_deatils->company_id);
				$comany_status = $company[0]->status ;
				if($comany_status == 1) {
					$status = $this->home_model->get_status_HQ_Manager($company_deatils->company_id);
					if($status == true) {
						// outlet is assigned  to
						$area_manager = $this->home_model->get_area_manager_id($userdata->user_accounts_id);
						if(get_outlets_by_area_manager_id($area_manager[0]->area_manager_id) =='0'){
							$this->session->unset_userdata('userid');
							$this->session->unset_userdata('unique_id');
							$this->session->unset_userdata('role_id');
							$this->session->unset_userdata('email');
							return false;
						}else{
							$outlet_id = get_outlets_by_area_manager_id($area_manager[0]->area_manager_id);
							$data=$this->home_model->get_company_id($userdata->user_accounts_id);
							$this->session->set_userdata('company_id',$data->company_id);
							$this->session->set_userdata('user_first_name',$data->first_name);
							$this->session->set_userdata('user_last_name',$data->last_name);
						}
					}else{
							// when HQ Manager is disable
							$this->session->unset_userdata('userid');
							$this->session->unset_userdata('unique_id');
							$this->session->unset_userdata('role_id');
							$this->session->unset_userdata('email');
							return false;
					}
				}else{
						$this->session->unset_userdata('userid');
						$this->session->unset_userdata('unique_id');
						$this->session->unset_userdata('role_id');
						$this->session->unset_userdata('email');
						return false;
				}
			}
			$data=$this->home_model->update_last_login($userdata->user_accounts_id);
			redirect('admin');
		}
		return false;
	}

	function setLoggedInUserCookie() {
		if($this->input->post('remember')) {
			$passwordCookie = array(
			'name'   => 'loginPassword',
			'value'  => base64_encode($this->input->post('password')),
			'expire' => '2592000'
			);
			$usernameCookie = array(
			'name'   => 'loginUsername',
			'value'  => $this->input->post('username'),
			'expire' => '2592000'
			);
			$rememberCookie = array(
			'name'   => 'loginRemember',
			'value'  => 1,
			'expire' => '2592000'
			);

			$this->input->set_cookie($usernameCookie);
			$this->input->set_cookie($passwordCookie);
			$this->input->set_cookie($rememberCookie);
		}else{
			delete_cookie("loginUsername");
			delete_cookie("loginPassword");
			delete_cookie("loginRemember");
		}
		return true;
	}

	function forget_password() {
		$logo_url =  base_url()."templates/JOD/img/logo-small.png";
		$data = array();
		if(isset($_POST['send'])) {
			$this->form_validation->set_rules('email_id','Work Email','required|trim|valid_email');
			if($this->form_validation->run() === FALSE) {
				return FALSE;
			}
			if($this->home_model->check_email($this->input->post('email_id'))) {
				$result = $this->home_model->check_email($this->input->post('email_id'));
				$userAccountId = $result->user_accounts_id;
				if($result->role_id == 2){ // HQM
					$user_details = getHQManagerDetails($userAccountId);
				}
				elseif($result->role_id == 3) { // OM
					$user_details = getOMManagerDetails($userAccountId);
				}
				elseif($result->role_id == 4) { // applicnats
					$user_details = getApplicantDetails($userAccountId);
				}
				elseif($result->role_id == 5) { // Area Manager
					$user_details = getAreaManagerId($userAccountId);
				}
				$email_template = get_email_template('reset_password');
				//print_r($email_template); die;
				//echo $emailData['fname']; die;
				$data['verification_code'] = sha1(rand(0,1000));
				$this->home_model->set_verification_code($userAccountId,$data);
				$emailData= array();
				$emailData['recieverId'] = $this->input->post('email_id');
				$emailData['subject'] = $email_template->subject;

				if(!(empty($user_details))){
					$fname  = $user_details[0]->first_name;
					$fname = ucfirst($fname);
				}else{
					$fname = 'Candidate';
				}
				$body = $email_template->body;
				$searchArray = array("{user_name}","{reset_password_link}","{site_logo}");
				$reset_password_link = base_url()."home/reset_password/".$data['verification_code'];
				$replaceArray = array($fname,$reset_password_link,$logo_url);
				$emailData['message']=str_replace($searchArray, $replaceArray, $body);
				//print_r($emailData['message']); die;
				send_email($emailData);
				set_global_messages('Please check email to reset password.','success');
			} else {
				$data['email'] = $this->input->post('email_id');
				set_global_messages('Email address does not exist.','error');
			}
		}
		$this->home_template->load('home_template','forgot_pass',$data);
	}

	function reset_password($code=''){
		$data = array();
		if((isset($_POST['send']))){
			if($this->reset_password_submit($code)) {
				set_global_messages("Password Reset Successfully Now Sign In .", 'success');
				redirect('admin');
			}
		}
    	if ($code) {
			$result = $this->home_model->get_profile_by_vcode($code);
            if (!empty($result)) {
					$data['code'] = $code;
					$this->home_template->load('home_template','reset_password',$data);
            } else {
                $this->home_template->load('home_template','404',$data);
            }
        } else {
			$this->home_template->load('home_template','404',$data);
        }
    }

    function reset_password_submit($code=''){
		if($code) {
			$this->form_validation->set_rules('password','Password','required|trim|min_length[5]|matches[confirm_password]');
			$this->form_validation->set_rules('confirm_password','Confirm Password','required|trim|min_length[5]');
			$this->form_validation->set_message('password', 'Password and confirm password should be same');
			if($this->form_validation->run() === FALSE) {
				return FALSE;
			} else {
				$result = $this->home_model->get_profile_by_vcode($code);
				$userAccountId = $result->user_accounts_id;
				$userType = $result->role_id;
				$data['password']         			= md5($this->input->post('password'));
				$userAccount['password']         	= md5($this->input->post('password'));
				$userAccount['verification_code']	= " ";
				$this->home_model->set_verification_code($userAccountId,$userAccount);
				if($userType == 1) {
					$this->home_model->superadmin_password($userAccountId,$data);
				} else if($userType == 2) {
					$this->home_model->headquater_password($userAccountId,$data);
				} else if($userType == 3) {
					$this->home_model->outlet_manager_password($userAccountId,$data);
				} else if($userType == 5) {
					$this->home_model->area_manager_password($userAccountId,$data);
				}
				return true;
			}
		} else {
				redirect('home/reset_password/'.$code);
		}
	}

	function logout() {
		$userType=LoginUserDetails('role_id');
		$this->session->unset_userdata('userid');
		$this->session->unset_userdata('unique_id');
		$this->session->unset_userdata('role_id');
		if($userType == '2' || $userType == '3' || $userType == '5') {
			$this->session->unset_userdata('company_id');
			$this->session->unset_userdata('user_first_name');
			$this->session->unset_userdata('user_last_name');
		}
		set_global_messages('You have been logged out.','success');
		redirect('');
	}
}
