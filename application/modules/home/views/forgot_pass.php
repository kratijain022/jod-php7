<?php
$userRemember=(!empty($userEmailCookie))?'checked':'';
$userEmail=(!empty($userEmailCookie))?$userEmailCookie:'';
$userPass=(!empty($userPassCookie))?$userPassCookie:'';
?>

<div class="container1">
  <form method="post" action="<?php echo base_url().'home/forget_password'; ?>" class="form-signin wow fadeInDown animated ani-new" data-wow-duration="1000ms" data-wow-delay="600ms" role="form" data-parsley-validate>
     <figure class="text-center logo-signin"><img src="<?php echo base_url() ?>/templates/JOD/img/logo.png"></figure>
     <?php
					$message = getGlobalMessage();
					//print_r($message);
					if($message['type']=='error')
					{?>
					<div class="alert alert-danger display">
						<button class="close" data-close="alert"></button>
						<span><?php echo $message['msg']; ?> </span>
					</div>
					<?php
					}else if($message['type']=='success'){?>
						<div class="alert alert-success display">
							<button class="close" data-close="alert"></button>
							<span><?php echo $message['msg']; ?> </span>
						</div>
					<?php }	?>
      <h3 class="forgot-text">Forgot Your Password?</h3>

    <label for="inputEmail" class="sr-only">Email address</label>
    <input type="email" id="inputEmail"  class="form-control" placeholder="Email Address" required autofocus name="email_id" value="<?php if(isset($email)) { echo $email; } ?>" data-parsley-maxlength="100" >


    <button class="btn btn-lg btn-primary btn-block cutom-btn" type="submit" name='send'>Reset</button>
    <label><a href="<?php echo base_url() ?>">Return to Sign In</a></label>
  </form>
</div>

<script>
 $(".alert").fadeOut( 5000 );
</script>

