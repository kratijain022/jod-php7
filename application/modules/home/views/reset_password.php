<?php
$userRemember=(!empty($userEmailCookie))?'checked':'';
$userEmail=(!empty($userEmailCookie))?$userEmailCookie:'';
$userPass=(!empty($userPassCookie))?$userPassCookie:'';
?>

<div class="container1">
  <form method="post" action="<?php echo base_url().'home/reset_password/'.$code; ?>" class="form-signin wow fadeInDown animated ani-new" data-wow-duration="1000ms" data-wow-delay="600ms" role="form" data-parsley-validate>
     <figure class="text-center logo-signin"><img src="<?php echo base_url() ?>/templates/JOD/img/logo.png"></figure>
     <?php
					$message = getGlobalMessage();
					//print_r($message);
					if($message['type']=='error')
					{?>
					<div class="alert alert-danger display">
						<button class="close" data-close="alert"></button>
						<span><?php echo $message['msg']; ?> </span>
					</div>
					<?php
					}else if($message['type']=='success'){?>
						<div class="alert alert-success display">
							<button class="close" data-close="alert"></button>
							<span><?php echo $message['msg']; ?> </span>
						</div>
					<?php }	?>
      <h3 class="forgot-text">Reset Password</h3>

    <label for="newpassword" class="sr-only">Password</label>
	<input id="password" class="form-control" type="password" name="password" placeholder="Password" data-parsley-required-message="Password is required" required data-parsley-maxlength="100" />
	<span id="password-error" class="help-block help-block-error error-red"><?php echo form_error('password'); ?></span>
	<label for="newpassword" class="sr-only">Password</label>
	<input id="confirm_password" class="form-control" type="password" name="confirm_password" placeholder="Confirm Password" data-parsley-required-message="Confirm password is required" data-parsley-equalto="#password" data-parsley-equalto-message="Password and confirm password should be same." required data-parsley-maxlength="100" />
    <span id="confirm_password-error" class="help-block help-block-error error-red"><?php echo form_error('confirm_password'); ?></span>
    <button class="btn btn-lg btn-primary btn-block cutom-btn" type="submit" name='send'>Reset</button>
    <label><a href="<?php echo base_url() ?>">Return to Sign in</a></label>
  </form>
</div>

<script>
 $(".alert").fadeOut( 5000 );
</script>
