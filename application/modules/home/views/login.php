<?php 
$userRemember=(!empty($userRememberCookie))?'checked':'';
$userEmail=(!empty($userEmailCookie))?$userEmailCookie:'';
$userPass=(!empty($userPassCookie))?$userPassCookie:'';
?>

<div class="container1">
  <form method="post" action="<?php echo base_url('home/admin_login/'); ?>" class="form-signin wow fadeInDown animated ani-new" data-wow-duration="1000ms" data-wow-delay="600ms" role="form" data-parsley-validate>
    <figure class="text-center logo-signin"><img src="<?php echo base_url() ?>/templates/JOD/img/logo.png" /></figure>
    <?php
					$message = getGlobalMessage();
					if($message['type']=='error')
					{?>
					<div class="alert alert-danger display">
						<button class="close" data-close="alert"></button>
						<span><?php echo $message['msg']; ?> </span>
					</div>
					<?php
					}else if($message['type']=='success'){?>
						<div class="alert alert-success display">
							<button class="close" data-close="alert"></button>
							<span><?php echo $message['msg']; ?> </span>
						</div>
					<?php }	?>
    <label for="inputEmail" class="sr-only">Email address</label>
    <input type="email" id="inputEmail" class="form-control" placeholder="Email Address" name="username" value="<?php echo $userEmail;  ?>" required autofocus autocomplete="off" data-parsley-maxlength="100" >
    <label for="inputPassword" class="sr-only">Password</label>
    <input type="password" id="inputPassword" class="form-control" placeholder="Password" name="password" value='<?php echo base64_decode($userPass);  ?>' required autocomplete="off" data-parsley-maxlength="100" >
    <div class="checkbox">
      <label>
        <input type="checkbox" name="remember" <?php echo $userRemember; ?>>
        Remember me </label>
    </div>
    <button class="btn btn-lg btn-primary btn-block cutom-btn" >Sign in</button>
    <label><a href="<?php echo base_url().'home/forget_password' ?>">Forgot Your Password? </a></label>
  </form>
</div>
<script>
 $(".alert").fadeOut( 5000 );
</script>

