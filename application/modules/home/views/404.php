<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
?>
<div class="right_col right_col" role="main">
	<div class="row padding-top-one">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bmar">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 dark_organge org-boxs no-padding">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 dark_organge org-box  no-padding left-mar nob-mar job-box">
						<div class="col-lg-10 col-md-10 col-sm-10 col-xs-9 center-block no-float">
							<div class="over-scroll-left">
								<h2> The link is invalid. You could be using an expired link. Please reset your password again by pressing reset once and wait for the email. You should receive the email within 5 mins.</h2>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
