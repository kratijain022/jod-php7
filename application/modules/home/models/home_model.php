<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Home_model extends CI_model{
	function __construct(){
		parent::__construct();
	}
	function checkuserexit($username,$password) {
		$prefix = $this->db->dbprefix;
		$this->db->select('*');
		$this->db->where('email_id',$username);
		$this->db->where('password',$password);
		$this->db->where('status','1');
		$this->db->where('is_delete','0');
		$this->db->where_not_in('role_id', array('4'));
		$query = $this->db->get($prefix.'user_accounts');
		//echo $this->db->last_query(); die;
		if($query->row()) {
			return $query->row();
		} else {
			return false;
		}
	}
	function get_company_id($userAccountsId) {
		$userType=LoginUserDetails('role_id');
		$prefix = $this->db->dbprefix;
		$this->db->select('*');
		$this->db->where('user_accounts_id',$userAccountsId);
		$this->db->where('status','1');
		if($userType == 2) {
			$query = $this->db->get($prefix.'headquater_manager');
		} else if($userType == 5) {
			$query = $this->db->get($prefix.'area_manager');
		}
		//echo $this->db->last_query(); die;
		if($query->row()) {
			return $query->row();
		} else {
			return false;
		}
	}
	function get_outlet_details($userAccountsId) {
		$userType=LoginUserDetails('role_id');
		$prefix = $this->db->dbprefix;
		$this->db->select('*');
		$this->db->where('user_accounts_id',$userAccountsId);
		$query = $this->db->get($prefix.'outlet_managers');
		if($query->row()) {
			return $query->row();
		} else {
			return false;
		}
	}

	function check_email($email_id) {
		$prefix = $this->db->dbprefix;
		$this->db->select('*');
		$this->db->where('email_id',$email_id);
		$query = $this->db->get($prefix.'user_accounts');
		//echo $this->db->last_query(); die;
		if($query->row()) {
			return $query->row();
		} else {
			return false;
		}
	}
	function set_verification_code($userAccountsId,$data) {
        $prefix = $this->db->dbprefix;
		$this->db->where('user_accounts_id', $userAccountsId);
		return $this->db->update($prefix.'user_accounts' ,$data);
	}
	function get_profile_by_vcode($code) {
		$prefix = $this->db->dbprefix;
		$this->db->select('*');
		$this->db->where('verification_code',$code);
		$query = $this->db->get($prefix.'user_accounts');
		//echo $this->db->last_query(); die;
		if($query->row()) {
			return $query->row();
		} else {
			return false;
		}
	}
	function superadmin_password($userAccountsId,$data) {
        $prefix = $this->db->dbprefix;
		$this->db->where('user_accounts_id', $userAccountsId);
		return $this->db->update($prefix.'superadmin' ,$data);
	}

	function headquater_password($userAccountsId,$data) {
        $prefix = $this->db->dbprefix;
		$this->db->where('user_accounts_id', $userAccountsId);
		return $this->db->update($prefix.'headquater_manager' ,$data);
	}

	function area_manager_password($userAccountsId,$data) {
        $prefix = $this->db->dbprefix;
		$this->db->where('user_accounts_id', $userAccountsId);
		return $this->db->update($prefix.'area_manager' ,$data);
	}

	function outlet_manager_password($userAccountsId,$data) {
        $prefix = $this->db->dbprefix;
		$this->db->where('user_accounts_id', $userAccountsId);
		return $this->db->update($prefix.'outlet_managers' ,$data);
	}

	function get_area_manager_id($user_accounts_id){
		$prefix = $this->db->dbprefix;
		$this->db->select('*');
		$this->db->where('user_accounts_id',$user_accounts_id);
		$query = $this->db->get($prefix.'area_manager');
		return $query->result();
	}

	function get_status_HQ_Manager($company_id){
		$prefix = $this->db->dbprefix;
		$this->db->select('*');
		$this->db->where('company_id',$company_id);
		$query = $this->db->get($prefix.'headquater_manager');
		$data = $query->result();
		if($data[0]->status == 0){
			return false;
		}else{
			return true;
		}

	}

	function get_company_details($comnany_id){
		$prefix = $this->db->dbprefix;
		$this->db->select('*');
		$this->db->where('company_id',$comnany_id);
		$query = $this->db->get($prefix.'companies');
		return $query->result();
	}

	function update_last_login($userAccountsId) {
		$current_date = CURRENT_TIME;
		$data = array("last_login_at"=>$current_date);
        $prefix = $this->db->dbprefix;
		$this->db->where('user_accounts_id', $userAccountsId);
		return $this->db->update($prefix.'user_accounts' ,$data);
	}

}
?>
