<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$message = getGlobalMessage();

?>
<div class="right_col right_col" role="main">
	<?php
	if($message['type']=='success') {
	?>
	<div class="Metronic-alerts alert alert-success fade in" id="prefix_167847684379"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button><?php echo $message['msg'] ?></div>
	<?php
	} else if($message['type']=='error') {
	?>
	<div class="Metronic-alerts alert alert-danger fade in" id="prefix_167847684379"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button><?php echo $message['msg'] ?></div>
	<?php
	}
	?>
	<div class="row padding-top-one">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<ul class="breadcrumb breadcrumb2">
			<li><a href="<?php echo base_url(); ?>"><?php echo $this->lang->line('home'); ?></a></li>
			<li class="active"> <?php echo $this->lang->line('history'); ?></li>
			</ul>
		</div>
		<?php
			$i=1;
			if(!empty($records)) {
			foreach($records as $record)
			{
		?>
		<!-- Cancelled Jobs start -->
		<?php if($record->is_delete == 1) { ?>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bmar">
        	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 org-boxs dark_red no-padding">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 org-box dark_red  no-padding left-mar nob-mar job-box">
				   <div class="col-lg-10 col-md-10 col-sm-10 col-xs-9 center-block no-float"><div class="over-scroll-left">
						<h2 class="text-over no-height" title="<?php echo $record->job_title; ?>"><?php echo $record->job_title; ?></h2>
					<p>
					<?php if(LoginUserDetails('role_id') == '5') { ?>
								<span><i class="fa fa-certificate"></i><font class="mar-left"><?php  echo getOutletName($record->outlet_id); ?></font></i></span>
							<?php } ?>
					<span><i class="fa fa-certificate"></i>
                        <font class="mar-left marleft3">
                        <?php if(strlen($record->name) > 8) { ?>
						<?php echo ucfirst(substr($record->name,0,8)).'...'; ?> ($<?php echo $record->payment_amount; ?>/hr)
                        <?php } else { ?>
                        <?php echo $record->name; ?> ($<?php echo $record->payment_amount; ?>/hr)
                        <?php } ?>
                        </font>
                    </span>
						<span><i class="fa fa-certificate"></i><font class="mar-left"><?php echo date('d M',strtotime($record->start_date)).' - '.date('d M',strtotime($record->end_date)); ?></font></span>
						<span> <font class="mar-left2"><?php echo getAppliedCandidate($record->job_id);?> <?php echo $this->lang->line('applied'); ?> / <?php echo getHiredCandidate($record->job_id);?> <?php echo $this->lang->line('selected'); ?></font></span>
					</p>
					</div></div>

					<div class="clearfix"></div>
					<?php if(LoginUserDetails('role_id') != 2) { // if loged in user is headquarder manager?>
					<a class="job-btn block100 red-box very_dark_red col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center"><?php echo $this->lang->line('job_cancelled'); ?></a>
					<?php } ?>
				</div>
			</div>

			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 org-boxs dark_red no-padding">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 org-box dark_red  no-padding left-mar nob-mar job-box">
					<p class="no-margin pads over-scroll">
						<span><font class="mar-bottom"><?php echo $record->description; ?></font></span>
					</p>
					<div class="clearfix"></div>
					<?php if(LoginUserDetails('role_id') != 2) { // if loged in user is headquarder manager?>
						<?php if(manage_job_access($record->job_id) == true){ ?>
							<a	 class="job-btn dark_yellow  col-lg-6 col-md-6 col-sm-6 col-xs-12 text-center" href="jobs/job_form/<?php echo $record->job_id ?>/1">   <i class="fa fa-files-o"></i> <?php echo $this->lang->line('duplicate_job'); ?></a>
						<?php }else{ ?>
								<a	class="job-btn black-box col-lg-6 col-md-6 col-sm-6 col-xs-12 text-center">   <i class="fa fa-files-o"></i> <?php echo $this->lang->line('duplicate_job'); ?></a>
						<?php } ?>
					<a href="<?php echo BASEURL.'jobs/job_detail/'.$record->job_id.'/3';?>" class="job-btn dark-org col-lg-6 col-md-6 col-sm-6 col-xs-12 text-center"><i class="glyphicon glyphicon-exclamation-sign"></i><?php echo $this->lang->line('more_info'); ?></a>
					<?php } ?>
				</div>
			</div>
            </div>
		</div>
		<?php } ?>
		<!-- Cancelled Jobs end -->
		<!-- completed Jobs start -->
		<?php if($record->is_completed == 1) { ?>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bmar">
        	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 org-boxs gray-box2 no-padding">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 org-box gray-box2  no-padding left-mar nob-mar job-box">
					 <div class="col-lg-10 col-md-10 col-sm-10 col-xs-9 center-block no-float"><div class="over-scroll-left">
                   	<h2 class="text-over no-height" title="<?php echo $record->job_title; ?>"><?php echo ucfirst($record->job_title) ;  ?></h2>
					<p>
					<?php if(LoginUserDetails('role_id') == '5') { ?>
								<span><i class="fa fa-certificate"></i><font class="mar-left"><?php  echo getOutletName($record->outlet_id); ?></font></i></span>
							<?php } ?>
					<span><i class="fa fa-certificate"></i>
                        <font class="mar-left marleft3">
                        <?php if(strlen($record->name) > 8) { ?>
						<?php echo ucfirst(substr($record->name,0,8)).'...'; ?> ($<?php echo $record->payment_amount; ?>/hr)
                        <?php } else { ?>
                        <?php echo $record->name; ?> ($<?php echo $record->payment_amount; ?>/hr)
                        <?php } ?>
                        </font>
                    </span>
						<span><i class="fa fa-certificate"></i><font class="mar-left"><?php echo date('d M-Y',strtotime($record->start_date)); ?></font></span>
						<span> <font class="mar-left2"><?php echo getAppliedCandidate($record->job_id);?> <?php echo $this->lang->line('applied'); ?> / <?php echo getHiredCandidate($record->job_id);?> <?php echo $this->lang->line('selected'); ?></font></span>
					</p>
					</div></div>
					<div class="clearfix"></div>
					<?php if(LoginUserDetails('role_id') != 2) { // if loged in user is headquarder manager?>
					<a class="job-btn block100 black-box col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center"><i class="glyphicon glyphicon-user"></i><?php echo getAppliedCandidate($record->job_id);?> <?php echo $this->lang->line('applied'); ?></a>
					<?php } ?>
				</div>
			</div>
			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 org-boxs gray-box2 no-padding">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 org-box gray-box2  no-padding left-mar nob-mar job-box">
					<p class="no-margin pads over-scroll">
						<span><font class="mar-bottom"><?php echo $record->description; ?></font></span>
					</p>
					<div class="clearfix"></div>
					<?php if(LoginUserDetails('role_id') != 2) { // if loged in user is headquarder manager?>
					<a	 class="job-btn dark_yellow  col-lg-6 col-md-6 col-sm-6 col-xs-12 text-center" href="jobs/job_form/<?php echo $record->job_id ?>/1">   <i class="fa fa-files-o"></i><?php echo $this->lang->line('duplicate_job'); ?></a>
					<a href="<?php echo BASEURL.'jobs/job_detail/'.$record->job_id.'/3';?>" class="job-btn dark-org col-lg-6 col-md-6 col-sm-6 col-xs-12 text-center"><i class="glyphicon glyphicon-exclamation-sign"></i><?php echo $this->lang->line('more_info'); ?></a>
					<?php } ?>
				</div>
			</div>
            </div>
		</div>
		<?php } ?>
		<!-- completed Jobs end -->
		<div class="clearfix"></div>
		<?php }
		} else{
			?><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bmar">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 org-boxs no-padding">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 org-box  no-padding left-mar nob-mar job-box">
							 <div class="col-lg-10 col-md-10 col-sm-10 col-xs-9 center-block no-float"><div class="over-scroll-left">
						<h2>
			<?php
			echo "No jobs history are found.";
			?></h2>
			</div></div>
			</div>
			</div>
			</div>
			</div>
			<?php
		}
		echo $this->pagination->create_links();
		 ?>
	</div>
</div>
