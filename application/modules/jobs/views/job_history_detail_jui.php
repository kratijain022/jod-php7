<?php
$message = getGlobalMessage();
if($message['type']=='success') {
	?>
	<div class="Metronic-alerts alert alert-success fade in" id="prefix_167847684379">
	<button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
	<?php echo $message['msg'] ?></div>
	<?php
} else if($message['type']=='error') {
?>
	<div class="Metronic-alerts alert alert-danger fade in" id="prefix_167847684379">
	<button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
	<?php echo $message['msg'] ?></div>
	<?php
}
?>
<?php
if($job_details->is_delete == 0 ){
	$is_delete = 0;
}else{
	$is_delete = 1;
}
?>

<div class="right_col">
<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<ul class="breadcrumb breadcrumb2">
			<li><a href="<?php echo base_url(); ?>">Home</a></li>
			<li class="active">Job Postings</li>
			<li class="active"><?php echo $job_details->job_title; ?> (<?php echo date('d M', strtotime($job_details->start_date)); ?> - <?php echo date('d M', strtotime($job_details->end_date)); ?>)</li>
			</ul>
		</div></div>
    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 left-mar nob-mar job-box yellow-box pad-bottom top-mar">
          <div class="col-lg-10 col-md-11 col-sm-12 col-xs-12 center-block no-float">
            <div class="over-scroll-left">
                <h3 class="text-over"  title="<?php echo $job_details->job_title; ?>"><?php echo ucfirst($job_details->job_title) ; ?></h3>
                <p>
					<?php if(LoginUserDetails('role_id') == '5') { ?>
                                <span><i class="fa fa-certificate"></i><font class="mar-left"><?php echo strlen((getOutletName($job_details->outlet_id))) > 10  ? substr(ucfirst(getOutletName($job_details->outlet_id)),0,10)."..." 
                                : ucfirst(getOutletName($job_details->outlet_id)); ?></font></i></span>
                    <?php } ?>
					<span><i class="fa fa-certificate"></i><font class="mar-left">$<?php echo $job_details->payment_amount; ?>/<?php echo $this->lang->line('hour'); ?></font></span>
                    <span><i class="fa fa-certificate"></i><font class="mar-left"><?php echo date('d-M', strtotime($job_details->start_date)); ?> - <?php echo date('d-M', strtotime($job_details->end_date)); ?></font></span>
                    <span><i class="fa fa-certificate"></i><font class="mar-left"><?php echo getAppliedCandidate($job_details->job_id);?>Applicants</font></span>
                    <span><i class="fa fa-certificate"></i><font class="mar-left"><?php echo getHiredCandidate($job_details->job_id);?> selected</font></span>
                </p>
            </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <!-- If Login user is area Manager and job is created by the same area manager then outlet Manager is not able to edit delete and copy the same job -->
   <?php if((LoginUserDetails('role_id') == "5" &&  $job_details->created_by == LoginUserDetails("userid") ) || ($job_details->job_created_by == 1 && manage_job_access($job_details->job_id) == true )){ ?>
    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 checked-box">
		<a href="javascript:;" onclick="complete_job_popup();">
			<button class="col-lg-12 col-md-12 col-sm-12 col-xs-12 left-mar nob-mar job-box green-box top-mar" >
				<span>
					<i class="fa fa-check-square-o"></i>
					<div class="clearfix"></div>
					Job Completed
				</span>
			</button>
        </a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 checked-box">
        <a href=" <?php echo BASEURL.'jobs/job_form/'.$job_details->job_id; ?>/1">
			<button class="col-lg-12 col-md-12 col-sm-12 col-xs-12 left-mar nob-mar job-box org-box top-mar">
				<span>
					<i class="fa fa-files-o"></i>
					<div class="clearfix"></div>
					Copy Job
				</span>
			</button>
        </a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 checked-box">
        <a href="javascript:;" onclick="delete_popup()"><button class="col-lg-12 col-md-12 col-sm-12 col-xs-12 left-mar nob-mar job-box red-box top-mar">
            <span>
                <i class="fa fa-ban"></i>
                <div class="clearfix"></div>
                Delete Job
            </span>
        </button></a>
    </div>

    <!-- If Login user is outlet Manager and job is created by the same outlet manager -->
    <?php } elseif((LoginUserDetails('role_id') == "3")) {?>
		<?php if(manage_job_access($job_details->job_id) == true) { ?>
		<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 checked-box">
			<a href="javascript:;" onclick="complete_job_popup();">
				<button class="col-lg-12 col-md-12 col-sm-12 col-xs-12 left-mar nob-mar job-box green-box top-mar" >
					<span>
						<i class="fa fa-check-square-o"></i>
						<div class="clearfix"></div>
						Job Completed
					</span>
				</button>
			</a>
		</div>
		<?php } ?>
		<?php if(manage_job_access($job_details->job_id) == true) { ?>
	   <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 checked-box">
			<a href=" <?php echo BASEURL.'jobs/job_form/'.$job_details->job_id; ?>/1">
				<button class="col-lg-12 col-md-12 col-sm-12 col-xs-12 left-mar nob-mar job-box org-box top-mar">
					<span>
						<i class="fa fa-files-o"></i>
						<div class="clearfix"></div>
						Copy Job
					</span>
				</button>
			</a>
		</div>
		<?php } ?>
		<?php if(manage_job_access($job_details->job_id) == true) { ?>
		   <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 checked-box">
				<a href="javascript:;" onclick="delete_popup()"><button class="col-lg-12 col-md-12 col-sm-12 col-xs-12 left-mar nob-mar job-box red-box top-mar">
					<span>
						<i class="fa fa-ban"></i>
						<div class="clearfix"></div>
						Delete Job
					</span>
				</button></a>
			</div>
		<?php } ?>
	<?php }	?>
    <div class="clearfix"></div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 job-des left-mar nob-mar job-box yellow-box pad-bottom">
             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 center-block no-float">
            <div class="dess">
                <h3>Job Description</h3>
                <p>
                    <span>
                    <font class="mar-left">
						<?php echo $job_details->description; ?>
                    </font>
                    </span>
                </p>
            </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 left-mar nob-mar top-mar light-org pad-bottom">
           <div class="col-lg-10 col-md-11 col-sm-12 col-xs-12 center-block no-float">  <div class="over-scroll-left">
                <h3>Payment Method</h3>
                <div class="check-box">
                    <div class="check-wrapper">
                        <input type="checkbox" <?php if(set_value('payment_mode', isset($job_details->payment_mode) ? $job_details->payment_mode : '') == 1){ echo 'checked'; } ?> value="1" id="check1" name="payment_mode" data-parsley-multiple="payment_mode" data-parsley-id="0433" autocomplete="off">
                        <label for="check1"> </label>
                        <span class="check-text">Cash</span>
                    </div>
                    <ul class="parsley-errors-list" id="parsley-id-multiple-payment_mode"></ul>
                    <div class="check-wrapper">
                        <input type="checkbox" <?php if(set_value('payment_mode', isset($job_details->payment_mode) ? $job_details->payment_mode : '') == 2){ echo 'checked'; } ?> onclick="checkCheckBox(this.id)" value="2" id="check2" name="payment_mode" data-parsley-multiple="payment_mode" data-parsley-id="0433" autocomplete="off">
                        <label for="check2"> </label>
                        <span class="check-text">Bank Transfer</span>
                    </div>
                    <div class="check-wrapper">
                        <input type="checkbox" <?php if(set_value('payment_mode', isset($job_details->payment_mode) ? $job_details->payment_mode : '') == 3){ echo 'checked'; } ?> value="3" id="check3" name="payment_mode" data-parsley-multiple="payment_mode" data-parsley-id="0433" autocomplete="off">
                        <label for="check3"> </label>
                        <span class="check-text">Cheque</span>
                    </div>
                </div>
            </div></div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 left-mar nob-mar top-mar org-box start pad-bottom">
            <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12 center-block no-float"> <div class="over-scroll-left">
                <h3>Start</h3>
                <span>
                    <h1><?php echo date('d-m-y', strtotime($job_details->start_date)); ?></h1>
                    <?php echo date('h:i:A', strtotime($job_details->start_time)); ?>
                </span>
            </div></div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 left-mar nob-mar top-mar org-box start pad-bottom">
            <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12 center-block no-float"> <div class="over-scroll-left">
                <h3>End</h3>
                <span>
                    <h1><?php echo date('d-m-y', strtotime($job_details->end_date)); ?></h1>
                    <?php echo date('h:i:A', strtotime($job_details->end_time)); ?>
                </span>
            </div></div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="clearfix"></div>
    <?php if($job_details->is_applicants_hired == 1) { ?>
		<?php if(!empty($hired_candidates)) {
			foreach($hired_candidates as $applicant){
				if($applicant->job_status =='1' && $applicant-> acknowledged == 1 && $applicant->completed_by_manager ==  1 ){ ?>

					 <?php
					 /*
			            //rsolanki
			            if(!empty($applicant->clockInImg)) { ?>
			            <div>
			            <a href="javascript:;" onclick="clockInOut('<?php echo $applicant->clockInImg; ?>')">
			                <button class="job-btn block100 green-box col-xs-12 mar-bottom1" data-toggle="modal" data-target=".fade2"><i class="glyphicon glyphicon-time"></i>Clock In</button>
			            </a>
			        <?php } ?>

			        <?php
			            //rsolanki
			            if(!empty($applicant->clockOutImg)) { ?>
			            <div>
			            <a href="javascript:;" onclick="clockInOut('<?php echo $applicant->clockOutImg; ?>')">
			                <button class="job-btn block100 green-box col-xs-12 mar-bottom1" data-toggle="modal" data-target=".fade2"><i class="glyphicon glyphicon-time"></i>Clock Out</button>
			            </a>
			        <?php } */ ?>


					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 gray-back left-sec no-padding top-mar">
							<span class="profile_img"><img style="width:203px; height:173px;" src="<?php echo checkImageFile(IMAGEURL.'applicantes/'.$applicant->portrait) == true ? IMAGEURL.'applicantes/'.$applicant->portrait :  IMAGE.'/no_image.png'  ; ?>" /><span class="circle"><?php echo getTotalvoteCounts($applicant->applicant_user_account_id); ?></span></span>
							<div class="clearfix"></div>
							<span class="jod-details col-lg-9 col-md-9 col-sm-9 col-xs-12 center-block">
								<h4 title="<?php echo  ucwords($applicant->first_name.' '.$applicant->last_name);?>"><?php echo strlen($applicant->first_name.' '.$applicant->last_name) > 10 ? substr(ucwords($applicant->first_name.' '.$applicant->last_name),0,10).".." : ucwords($applicant->first_name.' '.$applicant->last_name);?> <?php echo ($applicant->gender==1)?'(M)':'(F)';?></h4>
								<?php
									$dob = date_create($applicant->date_of_birth);
									$today = date_create(date('Y-m-d'));
									$age = date_diff($dob,$today, true);?>
								<p><?php echo $age->y;?> years old<br/>
									<?php echo getTotalvoteCounts($applicant->applicant_user_account_id); ?>  JODs completed
								</p>
							</span>
							<div class="clearfix"></div>
							<a href="<?php echo base_url('jobs/user_profile/'.$applicant->user_account_id); ?>"><button class="job-btn block100 org-box col-xs-12"><i class="glyphicon glyphicon-user"></i>View Profile</button></a>
						</div>
					</div>
				<?php } ?>
			<?php }?>
		<?php }?>
    <div class="clearfix"></div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 m-t80">
        <a href="<?php echo BASEURL.'jobs/get_rejected_applicant/'.$job_details->job_id;?>"><button class="create view-pad org-box col-lg-12 col-md-12 col-xs-12" type="submit" name="save"><span>View All Rejected Member Applicants</span><i class="fa fa-chevron-right"></i>  </button></a>
    </div>
    <?php } ?>
		<?php if($job_details->is_applicants_hired == 0) { ?>
			 <?php if(!empty($applied_candidates)) {
				foreach($applied_candidates as $applicant){
					if($applicant-> job_status == '0' ){?>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 gray-back left-sec no-padding top-mar">
								<span class="profile_img"><img style="width:203px; height:173px;" src="<?php echo checkImageFile(IMAGEURL.'applicantes/'.$applicant->portrait) == true ? IMAGEURL.'applicantes/'.$applicant->portrait : IMAGE.'/no_image.png' ; ?>" /><span class="circle"><?php echo getTotalvoteCounts($applicant->applicant_user_account_id); ?></span></span>
								<div class="clearfix"></div>
								<span class="jod-details col-lg-9 col-md-9 col-sm-9 col-xs-12 center-block">
									<h4 title="<?php echo  ucwords($applicant->first_name.' '.$applicant->last_name);?>" ><?php echo strlen($applicant->first_name.' '.$applicant->last_name) > 10 ? substr(ucwords($applicant->first_name.' '.$applicant->last_name),0,10).".." : ucwords($applicant->first_name.' '.$applicant->last_name);?> <?php echo ($applicant->gender==1)?'(M)':'(F)';?></h4>
									<?php
										$dob = date_create($applicant->date_of_birth);
										$today = date_create(date('Y-m-d'));
										$age = date_diff($dob,$today, true);?>
									<p><?php echo $age->y;?> years old<br/>
										<?php echo getTotalvoteCounts($applicant->applicant_user_account_id); ?> JODs completed
									</p>
								</span>
								<div class="clearfix"></div>
								<a href="<?php echo base_url('jobs/user_profile/'.$applicant->user_account_id); ?>"><button class="job-btn block100 org-box col-xs-12"><i class="glyphicon glyphicon-user"></i>View Profile</button></a>
							</div>
						</div>
				<?php }?>
			<?php } ?>
		<?php } ?>
		<div class="clearfix"></div>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 m-t80">
			<a href="<?php echo BASEURL.'jobs/get_applied_applicant/'.$job_details->job_id.'/4';?>"><button class="create view-pad org-box col-lg-12 col-md-12 col-xs-12" type="submit" name="save"><span>View All Applied Applicants</span><i class="fa fa-chevron-right"></i>  </button></a>
		</div>
    <?php }?>

<script>

$( document ).ready(function() {
$('input[type="checkbox"]').click(function(event) {
    this.checked = false; // reset first
    event.preventDefault();
});
});
function complete_job_popup(){
	var is_delete = '<?php echo $is_delete; ?>';
	if(is_delete == 0){
		var message = "Completed jobs can not be completed again.";
	}else{
		var message = "Deleted jobs can not be completed";
	}
	bootbox.alert(message, function() {
	});
}

function delete_popup(){
	var is_delete = '<?php echo $is_delete; ?>';
	if(is_delete == 0){
		var message = "Completed jobs can not be deleted.";
	}else{
		var message = "Deleted jobs can not be deleted again.";
	}
	bootbox.alert(message, function() {
	});
}

function clockInOut(imgName){
    if(imgName != ''){
        bootbox.dialog({
          message: "<img src=<?php echo base_url(); ?>qr_images/"+imgName+">",
          title: "Custom title",
          onEscape: function() {},
          show: true,
          backdrop: true,
          closeButton: true,
          animate: true,
          className: "my-modal",
          buttons: {
            Cancel: {
              label: "Cancel!",
              className: "btn-Cancel",
              callback: function() {}
            },
          }
        });
    }
}
</script>
