<?php
//print_r($applied_candidates); die;
if(!empty($applied_candidates)){
		$applicant_count = count($applied_candidates);
}else{
		$applicant_count = 0;
}

?>
<div class="right_col">
<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<ul class="breadcrumb breadcrumb2">
			<li><a href="<?php echo base_url(); ?>"><?php echo $this->lang->line('home'); ?></a></li>
			<li class="active"><?php echo $this->lang->line('job_posting'); ?></li>
			<li class="active"><?php echo $job_details->job_title; ?> (<?php echo date('d M', strtotime($job_details->start_date)); ?> - <?php echo date('d M', strtotime($job_details->end_date)); ?>)</li>
			</ul>
		</div></div>
    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 left-mar nob-mar job-box yellow-box pad-bottom top-mar">
          <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12 center-block no-float">
            <div class="over-scroll-left">
                <h3 class="text-over" title="<?php echo $job_details->job_title; ?>"><?php echo $job_details->job_title; ?></h3>
                <p>
                     <?php if(LoginUserDetails('role_id') == '5') { ?>
                                <span><i class="fa fa-certificate"></i><font class="mar-left"><?php  echo getOutletName($job_details->outlet_id); ?></font></i></span>
                     <?php } ?>
                  
                    <span><i class="fa fa-certificate"></i><font class="mar-left">$<?php echo $job_details->payment_amount; ?>/<?php echo $this->lang->line('hour'); ?>
                    </font>
                    </span>
                    <span><i class="fa fa-certificate"></i><font class="mar-left"><?php echo date('d-M', strtotime($job_details->start_date)); ?> - <?php echo date('d-M', strtotime($job_details->end_date)); ?></font></span>
                    <span><i class="fa fa-certificate"></i><font class="mar-left"><?php echo getAppliedCandidate($job_details->job_id); ?> <?php echo $this->lang->line('applicants'); ?></font></span>
                    <span><i class="fa fa-certificate"></i><font class="mar-left"><?php echo getHiredCandidate($job_details->job_id);?> <?php echo $this->lang->line('selected'); ?></font></span>
                </p>
            </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <!-- If Login user is area Manager and job is created by the same area manager then outlet Manager is not able to edit delete and copy the same job -->
    <?php if((LoginUserDetails('role_id') == "5" &&  $job_details->created_by == LoginUserDetails("userid") ) || ($job_details->job_created_by == 1 && manage_job_access($job_details->job_id) == true )){ ?>
    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 checked-box">
		<a href=" <?php echo BASEURL.'jobs/job_form/'.$job_details->job_id; ?>">
			<button class="col-lg-12 col-md-12 col-sm-12 col-xs-12 left-mar nob-mar job-box org-box3 top-mar">
				<span>
					<i class="glyphicon glyphicon-pencil"></i>
					<div class="clearfix"></div>
					    <?php echo $this->lang->line('edit_job'); ?>
				</span>
			</button>
		</a>
    </div>
     <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 checked-box">
        <a href=" <?php echo BASEURL.'jobs/job_form/'.$job_details->job_id; ?>/1">
			<button class="col-lg-12 col-md-12 col-sm-12 col-xs-12 left-mar nob-mar job-box org-box top-mar">
				<span>
					<i class="fa fa-files-o"></i>
					<div class="clearfix"></div>
					<?php echo $this->lang->line('copy_job'); ?>
				</span>
			</button>
        </a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 checked-box">
        <button class="col-lg-12 col-md-12 col-sm-12 col-xs-12 left-mar nob-mar job-box red-box top-mar" onclick="cancel_job(<?php echo $job_details->job_id;?>,<?php echo $applicant_count; ?>)">
            <span>
                <i class="fa fa-ban"></i>
                <div class="clearfix"></div>
               <?php echo $this->lang->line('delete_job'); ?>
            </span>
        </button>
    </div>
    <!-- If Login user is outlet Manager and job is created by the same outlet manager  -->
	<?php } elseif(LoginUserDetails('role_id') == "3")  {?>
	<?php if(manage_job_access($job_details->job_id) == true ){ ?>
	<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 checked-box">
		<a href=" <?php echo BASEURL.'jobs/job_form/'.$job_details->job_id; ?>">
			<button class="col-lg-12 col-md-12 col-sm-12 col-xs-12 left-mar nob-mar job-box org-box3 top-mar">
				<span>
					<i class="glyphicon glyphicon-pencil"></i>
					<div class="clearfix"></div>
					  <?php echo $this->lang->line('edit_job'); ?>
				</span>
			</button>
        </a>
    </div>
	<?php } ?>
	<?php if(manage_job_access($job_details->job_id) == true ){ ?>
     <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 checked-box">
         <a href=" <?php echo BASEURL.'jobs/job_form/'.$job_details->job_id; ?>/1">
			<button class="col-lg-12 col-md-12 col-sm-12 col-xs-12 left-mar nob-mar job-box org-box top-mar">
				<span>
					<i class="fa fa-files-o"></i>
					<div class="clearfix"></div>
					   <?php echo $this->lang->line('copy_job'); ?>
				</span>
			</button>
        </a>
    </div>
    <?php } ?>
    <?php if(manage_job_access($job_details->job_id) == true ){ ?>
    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 checked-box">
        <button class="col-lg-12 col-md-12 col-sm-12 col-xs-12 left-mar nob-mar job-box red-box top-mar" onclick="cancel_job(<?php echo $job_details->job_id;?>,<?php echo $applicant_count; ?>)">
            <span>
                <i class="fa fa-ban"></i>
                <div class="clearfix"></div>
                    <?php echo $this->lang->line('delete_job'); ?>
            </span>
        </button>
    </div>
    <?php } ?>
	<?php } ?>
    <div class="clearfix"></div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 job-des left-mar nob-mar job-box yellow-box pad-bottom">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 center-block no-float">
            <div class="dess">
                <h3><?php echo $this->lang->line('job_description'); ?></h3>
                <p>
                    <span>
                    <font class="mar-left">
						<?php echo $job_details->description; ?>
                    </font>
                    </span>
                </p>
            </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 left-mar nob-mar top-mar light-org pad-bottom">
          <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12 center-block no-float">   <div class="over-scroll-left">
                <h3><?php echo $this->lang->line('payment_method'); ?></h3>
                <div class="check-box">
                    <div class="check-wrapper">
                        <input type="checkbox" <?php if(set_value('payment_mode', isset($job_details->payment_mode) ? $job_details->payment_mode : '') == 1){ echo 'checked'; } ?> value="1" id="check1" name="payment_mode" data-parsley-multiple="payment_mode" data-parsley-id="0433" autocomplete="off">
                        <label for="check1"> </label>
                        <span class="check-text"><?php echo $this->lang->line('cash'); ?></span>
                    </div>
                    <ul class="parsley-errors-list" id="parsley-id-multiple-payment_mode"></ul>
                    <div class="check-wrapper">
                        <input type="checkbox" <?php if(set_value('payment_mode', isset($job_details->payment_mode) ? $job_details->payment_mode : '') == 2){ echo 'checked'; } ?> onclick="checkCheckBox(this.id)" value="2" id="check2" name="payment_mode" data-parsley-multiple="payment_mode" data-parsley-id="0433" autocomplete="off">
                        <label for="check2"> </label>
                        <span class="check-text"><?php echo $this->lang->line('bank_transfer'); ?></span>
                    </div>
                    <div class="check-wrapper">
                        <input type="checkbox" <?php if(set_value('payment_mode', isset($job_details->payment_mode) ? $job_details->payment_mode : '') == 3){ echo 'checked'; } ?> value="3" id="check3" name="payment_mode" data-parsley-multiple="payment_mode" data-parsley-id="0433" autocomplete="off">
                        <label for="check3"> </label>
                        <span class="check-text"><?php echo $this->lang->line('cheque'); ?></span>
                    </div>
                </div>
            </div></div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 left-mar nob-mar top-mar org-box start pad-bottom">
            <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12 center-block no-float"> <div class="over-scroll-left">
                <h3><?php echo $this->lang->line('start'); ?></h3>
                <span>
                    <h1><?php echo date('d-m-y', strtotime($job_details->start_date)); ?></h1>
                    <?php echo date('h:i:A', strtotime($job_details->start_time)); ?>
                </span>
            </div></div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 left-mar nob-mar top-mar org-box start pad-bottom">
          <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12 center-block no-float">   <div class="over-scroll-left">
                <h3><?php echo $this->lang->line('end'); ?></h3>
                <span>
                    <h1><?php echo date('d-m-y', strtotime($job_details->end_date)); ?></h1>
                    <?php echo date('h:i:A', strtotime($job_details->end_time)); ?>
                </span>
            </div></div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="clearfix"></div>

    <?php
     if(!empty($applied_candidates)):
		foreach($applied_candidates as $applicant):
			if($applicant->job_status=='0' || ($applicant->cancel_status ==1 && $applicant->system_rejected ==1) || ($applicant->cancel_status != 1)):?>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 gray-back-two left-sec no-padding top-mar">
                    <!--
                    <button class="job-btn block100 green-box col-xs-12 mar-bottom1" data-toggle="modal" data-target=".fade2"><i class="glyphicon glyphicon-time"></i>Clock In</button>
                    -->
						<?php if($applicant->system_rejected == 1) { ?>
								<button class="job-btn block100 red-box  mar-bottom1">
									<i class="glyphicon glyphicon-time"></i>
									<?php echo $this->lang->line('job_time_overlap'); ?>
								</button>
						<?php }  else { 
							 if($applicant->system_rejected == 0 && $applicant->cancel_status ==1){
							?>
								<button class="job-btn block100 red-box  mar-bottom1">
									<i class="glyphicon glyphicon-remove"></i>
									<?php echo $this->lang->line('self_cancelled'); ?>
								</button>
							<?php }else{ ?>
										
									<button class="job-btn block100 trans-box col-xs-12 mar-bottom1">
										
									</button>
								
							<?php } ?>
							<?php }  ?>
						<span class="profile_img"><img style="width:203px; height:173px;" src="<?php echo checkImageFile(IMAGEURL.'applicantes/'.$applicant->portrait) == true ? IMAGEURL.'applicantes/'.$applicant->portrait : IMAGE.'/no_image.png' ; ?>" /><span class="circle"><?php echo getTotalvoteCounts($applicant->applicant_user_account_id); ?></span></span>
						<div class="clearfix"></div>
						<span class="jod-details col-lg-9 col-md-9 col-sm-9 col-xs-12 center-block">
							<h4 title="<?php echo  ucwords($applicant->first_name.' '.$applicant->last_name);?>"><?php echo strlen($applicant->first_name.' '.$applicant->last_name) > 10 ? substr(ucwords($applicant->first_name.' '.$applicant->last_name),0,10).".." : ucwords($applicant->first_name.' '.$applicant->last_name);?> <?php echo ($applicant->gender==1)?'(M)':'(F)';?></h4>
							<?php
								$dob = date_create($applicant->date_of_birth);
								$today = date_create(date('Y-m-d'));
								$age = date_diff($dob,$today, true);?>
							<p><?php echo $age->y;?> <?php echo $this->lang->line('years_old'); ?><br/>
								<?php echo getTotalvoteCounts($applicant->applicant_user_account_id); ?><?php echo $this->lang->line('JOD_completed'); ?>
							</p>
						</span>
						<div class="clearfix"></div>
						<a href="<?php echo base_url('jobs/user_profile/'.$applicant->user_account_id); ?>"><button class="job-btn block100 org-box col-xs-12"><i class="glyphicon glyphicon-user"></i><?php echo $this->lang->line('view_profile'); ?></button></a>
					</div>
				</div>
			<?php endif;?>
		<?php endforeach;?>
    <div class="clearfix"></div>

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 m-t80">
        <a href="<?php echo BASEURL.'jobs/get_applied_applicant/'.$job_details->job_id.'/3';?>"><button class="create view-pad org-box col-lg-12 col-md-12 col-xs-12" type="submit" name="save"><span><?php echo $this->lang->line('view_all_applicants'); ?></span><i class="fa fa-chevron-right"></i>  </button></a>
    </div>
    <?php else:?>
    <div class="clearfix"></div>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 m-t80">
			<a href="<?php echo BASEURL.'jobs/get_applied_applicant/'.$job_details->job_id.'/3';?>"><button class="create view-pad org-box col-lg-12 col-md-12 col-xs-12" type="submit" name="save"><span><?php echo $this->lang->line('view_all_applicants'); ?></span><i class="fa fa-chevron-right"></i>  </button></a>
		</div>

    <?php endif;?>

<script>
<?php if(isset($site_language) && $site_language == "chinese") { ?>
    bootbox.setDefaults({
        locale: "zh_CN"
    });
<?php }else{ ?>
    bootbox.setDefaults({
        locale: "en"
     });
<?php } ?>

function cancel_job(id,count) {
	var msg ="";
	if(count > 0){
		msg = count+" <?php echo $this->lang->line('delete_job_mesg'); ?>";
	}else{
		msg = "<?php echo $this->lang->line('confirm_delete_job_mesg'); ?>";
	}
	bootbox.confirm(msg, function(result) {
		if(result==true) {
			var status = 1;
			location.href="<?php echo base_url('jobs/cancel_job'); ?>/"+id+"/"+count;
		}
	});
}
$( document ).ready(function() {
$('input[type="checkbox"]').click(function(event) {
    this.checked = false; // reset first
    event.preventDefault();
});
});

</script>
<style>
.btn.btn-Cancel {
    float: right;
    height: auto;width: 60px;
}
.btn.btn-ok{
     height: 36px; padding: 6px 14px;
}
.job-btn {
    border: 0 none;
    color: #fff;
    cursor: pointer !important;
    font-family: "robotoregular";
    font-size: 15px;
    margin: 0;
    padding: 10px 0;
}
</style>
