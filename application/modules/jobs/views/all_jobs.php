<?php $message = getGlobalMessage(); ?>

<div class="right_col right_col" role="main">

	<div class="row padding-top-one">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	<ul class="breadcrumb breadcrumb2">
			<li class="gray"><a href="<?php echo base_url(); ?>"><?php echo $this->lang->line('home'); ?></a></li>
			<li class="active"><?php echo $this->lang->line('job_posting'); ?></li>
			<li class="active"><?php echo $this->lang->line('all_job_posting'); ?></li>
		</ul>
	</div>
	<?php if(!empty($records)) { ?>
		<?php foreach ($records as $record){ ?>
			<!-- compeleted jobs -->
				<?php if(($record->is_completed == 1) && ($record->is_applicants_hired == 1) && ($record->is_delete == 0)) { ?>
					<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 org-box gray-box  no-padding left-mar job-box">
						  <div class="col-lg-10 col-md-11 col-sm-10 col-xs-9 center-block no-float">
                        <h2  class="text-over" title="<?php echo $record->job_title; ?>"><?php echo $record->job_title; ?> </h2>
						<p>
						<?php if(LoginUserDetails('role_id') == '5') { ?>
								<span><i class="fa fa-certificate"></i><font class="mar-left"><?php  echo getOutletName($record->outlet_id); ?></font></i></span>
							<?php } ?>
							<span><i class="fa fa-certificate"></i><font class="mar-left" title="<?php echo getJobTypeName($record->job_type_id); ?>"><?php echo strlen(getJobTypeName($record->job_type_id)) > 10 ? ucfirst(substr(getJobTypeName($record->job_type_id),0,10)).'...' : ucfirst(getJobTypeName($record->job_type_id)) ;  ?>  ($<?php echo $record->payment_amount; ?>/hr)</font></span>
							<span><i class="fa fa-certificate"></i><font class="mar-left"><?php echo date('d M-Y H:m A',strtotime($record->start_date."" .$record->start_time)); ?></font></span>
							<span><i class="fa fa-certificate"></i><font class="mar-left"><?php echo date('d M-Y H:m A',strtotime($record->end_date."" .$record->end_time)); ?></font></span>
							<!-- <span><i class="fa fa-certificate"></i><font class="mar-left"><?php echo date('d M',strtotime($record->start_date)).'-'.date('d M',strtotime($record->end_date)); ?></font></span> -->
							<span><i class="fa fa-certificate"></i><font class="mar-left"><?php echo ucfirst($this->jobs_model->get_outlet_name_by_jobid($record->outlet_id)); ?></font></span>
							<span> <font class="mar-left2"><?php echo $this->lang->line('job_completed_u'); ?></font></span>
						</p>
                        </div>
						<div class="clearfix"></div>
						<a href="<?php echo BASEURL.'jobs/job_detail/'.$record->job_id;?>"><button class="job-btn block100 black-dark"><?php echo $this->lang->line('view_job_history'); ?></button></a>
					</div>
				   </div>
				<?php } ?>
				<?php if(($record->is_approved == 1 || $record->is_approved ==0 ) && ($record->is_delete == 0) && ($record->is_applicants_hired == 0) && ($record->is_completed == 0)) { ?>
				<!-- open job -->
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 org-box  no-padding left-mar job-box">
						  <div class="col-lg-10 col-md-11 col-sm-10 col-xs-9 center-block no-float">
                        <h2  class="text-over" title="<?php echo $record->job_title; ?>"><?php echo $record->job_title; ?> </h2>
						<p>
						<?php if(LoginUserDetails('role_id') == '5') { ?>
								<span><i class="fa fa-certificate"></i><font class="mar-left"><?php  echo getOutletName($record->outlet_id); ?></font></i></span>
							<?php } ?>
							<span><i class="fa fa-certificate"></i><font class="mar-left" title="<?php echo getJobTypeName($record->job_type_id); ?>"><?php echo strlen(getJobTypeName($record->job_type_id)) > 10 ? ucfirst(substr(getJobTypeName($record->job_type_id),0,10)).'...' : ucfirst(getJobTypeName($record->job_type_id)) ;  ?>  ($<?php echo $record->payment_amount; ?>/hr)</font></span>
							<span><i class="fa fa-certificate"></i><font class="mar-left"><?php echo date('d M-Y H:m A',strtotime($record->start_date."" .$record->start_time)); ?></font></span>
							<span><i class="fa fa-certificate"></i><font class="mar-left"><?php echo date('d M-Y H:m A',strtotime($record->end_date."" .$record->end_time)); ?></font></span>
							<!-- <span><i class="fa fa-certificate"></i><font class="mar-left"><?php echo date('d M',strtotime($record->start_date)).'-'.date('d M',strtotime($record->end_date)); ?></font></span> -->
							<span><i class="fa fa-certificate"></i><font class="mar-left"><?php echo ucfirst($this->jobs_model->get_outlet_name_by_jobid($record->outlet_id)); ?></font></span>
							<span> <font class="mar-left2"><?php echo getAppliedCandidate($record->job_id);?> <?php echo $this->lang->line('applied'); ?> / 0 <?php echo $this->lang->line('selected'); ?></font></span>
						</p>
                        </div>
						<div class="clearfix"></div>
						<a href="<?php echo BASEURL.'jobs/job_detail/'.$record->job_id.'/1';?>"><button class="job-btn block100 red-box"><?php echo $this->lang->line('view_open_job'); ?></button></a>
					</div>
				  </div>
				 <?php } ?>

				<!-- compeleted job -->
				<?php if(($record->is_approved == 1 ) && ($record->is_delete == 0) && ($record->is_applicants_hired == 1) && ($record->is_completed == 0)) { ?>
				<!-- active job -->
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 org-box green-box no-padding left-mar job-box">
						  <div class="col-lg-10 col-md-11 col-sm-10 col-xs-9 center-block no-float">
                         <h2  class="text-over" title="<?php echo $record->job_title; ?>"><?php echo $record->job_title; ?> </h2>
						<p>
						<?php if(LoginUserDetails('role_id') == '5') { ?>
								<span><i class="fa fa-certificate"></i><font class="mar-left"><?php  echo getOutletName($record->outlet_id); ?></font></i></span>
							<?php } ?>
							<span><i class="fa fa-certificate"></i><font class="mar-left" title="<?php echo getJobTypeName($record->job_type_id); ?>"><?php echo strlen(getJobTypeName($record->job_type_id)) > 10 ? ucfirst(substr(getJobTypeName($record->job_type_id),0,10)).'...' : ucfirst(getJobTypeName($record->job_type_id)) ;  ?>  ($<?php echo $record->payment_amount; ?>/hr)</font></span>
							<span><i class="fa fa-certificate"></i><font class="mar-left"><?php echo date('d M-Y H:m A',strtotime($record->start_date."" .$record->start_time)); ?></font></span>
							<span><i class="fa fa-certificate"></i><font class="mar-left"><?php echo date('d M-Y H:m A',strtotime($record->end_date."" .$record->end_time)); ?></font></span>
							<!-- <span><i class="fa fa-certificate"></i><font class="mar-left"><?php echo date('d M',strtotime($record->start_date)).'-'.date('d M',strtotime($record->end_date)); ?></font></span> -->
							<span><i class="fa fa-certificate"></i><font class="mar-left"><?php echo ucfirst($this->jobs_model->get_outlet_name_by_jobid($record->outlet_id)); ?></font></span>
							<span> <font class="mar-left2"><?php echo getHiredCandidate($record->job_id);?> <?php echo $this->lang->line('hired'); ?></font></span>
						</p>
                        </div>
						<div class="clearfix"></div>
						<a href="<?php echo BASEURL.'jobs/job_detail/'.$record->job_id.'/2';?>"><button class="job-btn block100 dark-green"><?php echo $this->lang->line('view_active_job'); ?></button></a>
					</div>
				</div>
				<?php } ?>
				<!-- Cancelled job -->
				<?php if(($record->is_applicants_hired == 0) && ($record->is_completed == 0) &&  ($record->is_delete == 1)) { ?>
				 <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 org-box red-box2 no-padding left-mar job-box">
						  <div class="col-lg-10 col-md-11 col-sm-10 col-xs-9 center-block no-float">
                        <h2  class="text-over" title="<?php echo $record->job_title; ?>"><?php echo $record->job_title; ?> </h2>
						<p>
						<?php if(LoginUserDetails('role_id') == '5') { ?>
								<span><i class="fa fa-certificate"></i><font class="mar-left"><?php  echo getOutletName($record->outlet_id); ?></font></i></span>
							<?php } ?>
							<span><i class="fa fa-certificate"></i><font class="mar-left" title="<?php echo getJobTypeName($record->job_type_id); ?>"><?php echo strlen(getJobTypeName($record->job_type_id)) > 10 ? ucfirst(substr(getJobTypeName($record->job_type_id),0,10)).'...' : ucfirst(getJobTypeName($record->job_type_id)) ;  ?>  ($<?php echo $record->payment_amount; ?>/hr)</font></span>
							<span><i class="fa fa-certificate"></i><font class="mar-left"><?php echo date('d M-Y H:m A',strtotime($record->start_date."" .$record->start_time)); ?></font></span>
							<span><i class="fa fa-certificate"></i><font class="mar-left"><?php echo date('d M-Y H:m A',strtotime($record->end_date."" .$record->end_time)); ?></font></span>
							<!-- <span><i class="fa fa-certificate"></i><font class="mar-left"><?php echo date('d M',strtotime($record->start_date)).'-'.date('d M',strtotime($record->end_date)); ?></font></span> -->
							<span><i class="fa fa-certificate"></i><font class="mar-left"><?php echo ucfirst($this->jobs_model->get_outlet_name_by_jobid($record->outlet_id)); ?></font></span>
							<span> <font class="mar-left2"><?php echo $this->lang->line('job_cancelled_u'); ?></font></span>
						</p>
                        </div>
						<div class="clearfix"></div>
						<a href="<?php echo BASEURL.'jobs/job_detail/'.$record->job_id;?>"><button class="job-btn block100 red-dark"><?php echo $this->lang->line('view_job_history'); ?></button></a>
					</div>
				  </div>
				 	<?php } ?>
		<?php } // foreach ?>
	<?php }else{ // if?>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bmar">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 dark_organge org-boxs no-padding">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 dark_organge org-box  no-padding left-mar nob-mar job-box">
						<div class="col-lg-10 col-md-10 col-sm-10 col-xs-9 center-block no-float">
							<div class="over-scroll-left text-center over-scroll-left2">
							<h2><?php echo "No Jobs Found."; ?></h2>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php } ?>

    </div>
    	<?php if(!empty($records)){
			  echo $this->pagination->create_links();
		}
		?>
</div>

<script>
//alert('The page is under development');
</script>
