<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$message = getGlobalMessage();
	$userType=LoginUserDetails('role_id');
?>
<div class="right_col right_col" role="main"> <!-- main div -->
	<?php
	if($message['type']=='success') {
	?>
		<div class="Metronic-alerts alert alert-success fade in" id="prefix_167847684379">
		<button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button><?php echo $message['msg'] ?></div>
  <?php
	} else if($message['type']=='error') {
	?>
	<div class="Metronic-alerts alert alert-danger fade in" id="prefix_167847684379">
		<button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button><?php echo $message['msg'] ?></div>
  <?php
	}
	?>
	<ul class="breadcrumb breadcrumb2">
		<li><a href="<?php echo base_url(); ?>"><?php echo $this->lang->line('home'); ?></a></li>
		<?php if($id) { ?>
			<li class="active"><?php echo $this->lang->line('edit_job_template'); ?></li>
		<?php } else { ?>
			<li class="active"><?php echo $this->lang->line('create_job_template'); ?></li>
		<?php } ?>
	</ul>
	 <div class="row padding-top-one"> <!-- row padding top one div start -->
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center title-head2">
			<h3>
				<?php if($id) {
					echo  $this->lang->line('edit_job_template');
					$action = $this->lang->line('update');
				} else {
					//echo 'Create a New Job Template';
					echo $this->lang->line('create_job_template');
					$action = $this->lang->line('create');
				}
			?>

			</h3>
		</div>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <!--- div inner start-->
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 parent-form">  <!--- parent form div start -->
				<form action="<?php echo base_url('jobs/job_template/'.$id); ?>" id="template_form" class="form-horizontal" enctype="multipart/form-data" method='post' data-parsley-validate onsubmit="validation()"> <!--- form  -->
					<div class="org-box pad-bottoms"> <!-- div org-box start -->
						<h4 class="text-center basic-details"><?php echo $this->lang->line('job_template_details'); ?></h4>
						<div class="form-group">   <!--- template name div start -->
							<div class="col-lg-1 col-md-1 col-sm-1 hidden-xs">
								<label for="usr1"></label>
							</div>
							<div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
								<input type="text" placeholder="<?php echo $this->lang->line('template_name'); ?>" class="form-control" id="template_name" name="template_name" value="<?php echo set_value('template_name', isset($records[0]->template_name) ? $records[0]->template_name : ''); ?>" required data-parsley-maxlength="100" >
								<span id="template_name-error" class="help-block help-block-error"><?php echo form_error('template_name'); ?></span>
							</div>
						</div>

						<div class="form-group">   <!--- job_title div start -->
							<div class="col-lg-1 col-md-1 col-sm-1 hidden-xs ">
								<label for="usr1"></label>
							</div>
							<div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
								<input type="text" placeholder="<?php echo $this->lang->line('job_title'); ?>" class="form-control" id="job_title" name="job_title" value="<?php echo set_value('job_title', isset($records[0]->job_title) ? $records[0]->job_title : ''); ?>" required data-parsley-maxlength="100" >
								<span id="job_title-error" class="help-block help-block-error"><?php echo form_error('job_title'); ?></span>
							</div>
						</div>

						<div class="form-group">   <!--- job_type div start -->
							<div class="col-lg-1 col-md-1 col-sm-1 hidden-xs">
								<label for="usr1"></label>
							</div>
							<div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
								<select class="form-control chosen-select" name="job_type_id" id="job_type_id" required data-parsley-errors-container="#job_type_id-error">
									<option value="" >--<?php echo $this->lang->line('select_job_type'); ?>--</option>
									<?php foreach($jobTypes as $jobType) { ?>
										<option value="<?php echo $jobType->job_role_id; ?>" <?php if(set_value('job_type_id', isset($records[0]->job_type_id) ? $records[0]->job_type_id : '') == $jobType->job_role_id){ echo 'selected="selected"'; } ?> ><?php echo $jobType->name;?></option>
									<?php } ?>
								</select>
								<span id="job_type_id-error" class="help-block help-block-error"><?php echo form_error('job_type_id'); ?></span>
							</div>
						</div>

						<div class="form-group"> <!--- job_decription div start -->
							<div class="col-lg-1 col-md-1 col-sm-1 hidden-xs">
								<label for="usr1"></label>
							</div>
							<div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
								<textarea type="text" rows="5" placeholder="<?php echo $this->lang->line('job_description'); ?>"  class="form-control text-area" id="description" name="description" required><?php echo set_value('description', isset($records[0]->job_description) ? $records[0]->job_description : ''); ?></textarea>
								<span id="description-error" class="help-block help-block-error"><?php echo form_error('description'); ?></span>
							</div>
						</div>

						<div class="form-group"> <!-- start date div -->
							<div class="col-lg-1 col-md-1 col-sm-1 hidden-xs">
								<label for="usr1"></label>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
								<div class="form-group input-append date" id="datepicker1">
									<input type="text"  placeholder="<?php echo $this->lang->line('start_date'); ?>" class="form-control" id="start_date" name="start_date" value="<?php echo set_value('start_date', isset($records[0]->start_date) ? $records[0]->start_date : ''); ?>" data-parsley-errors-container="#start_date-error" style="width:100%" readonly required>
									<span class="add-on" id="start_date_add_on" > <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="fa fa-calendar"></i> </span>
									<span id="start_date-error" class="help-block help-block-error"><?php echo form_error('start_date'); ?></span>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
								<div class="form-group input-append date" id="timepicker1">
									<input type="text"  data-format="hh:mm:ss"  placeholder="<?php echo $this->lang->line('start_time'); ?>" class="form-control" id="start_time" name="start_time" value="<?php echo set_value('start_time', isset($records[0]->start_time) ? $records[0]->start_time : ''); ?>" data-parsley-errors-container="#start_time-error" style="width:100%" readonly	 required>
									<span class="add-on"  id="start_time_add_on"> <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="fa fa-clock-o"></i> </span>
									<span id="start_time-error" class="help-block help-block-error"><?php echo form_error('start_time'); ?></span>
								</div>
							</div>
						</div>

						<div class="form-group"> <!-- end date div -->
							<div class="col-lg-1 col-md-1 col-sm-1 hidden-xs">
								<label for="usr1"></label>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
								<div class="form-group input-append date" id="datepicker2">
									<input type="text"  placeholder="<?php echo $this->lang->line('end_date'); ?>" class="form-control" id="end_date" name="end_date" value="<?php echo set_value('end_date', isset($records[0]->end_date) ? $records[0]->end_date : ''); ?>" data-parsley-errors-container="#end_date-error" style="width:100%" readonly required>
									<span class="add-on" id="end_date_add_on"> <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="fa fa-calendar"></i> </span>
									<span id="end_date-error" class="help-block help-block-error"><?php echo form_error('end_date'); ?></span>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
								<div class="form-group input-append date" id="timepicker2">
									<input type="text"  data-format="hh:mm:ss"  placeholder="<?php echo $this->lang->line('end_time'); ?>" class="form-control" id="end_time" name="end_time" value="<?php echo set_value('end_time', isset($records[0]->end_time) ? $records[0]->end_time : ''); ?>" data-parsley-errors-container="#end_time-error" style="width:100%" readonly	 required>
									<span class="add-on" id="end_time_add_on"> <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="fa fa-clock-o"></i> </span>
									<span id="end_time-error" class="help-block help-block-error"><?php echo form_error('end_time'); ?></span>
								</div>
							</div>
						</div>

						<div class="form-group"> <!--- payment div start -->
							<div class="col-lg-1 col-md-1 col-sm-1 hidden-xs">
								<label for="usr1"></label>
							</div>
                            <div class="cash-bank">
							<div class="col-lg-6 col-md-6 col-sm-10 col-xs-12">
								<input type="text" placeholder="<?php echo $this->lang->line('earning_per_hour'); ?>" class="form-control"  id="payment_amount" name="payment_amount" value="<?php echo set_value('payment_amount', isset($records[0]->payment_amount) ? $records[0]->payment_amount : ''); ?>" data-parsley-errors-container="#payment_amount-error"   data-parsley-pattern-message="Please enter valid number" 	data-parsley-type="number" required >
								<span id="payment_amount-error" class="help-block help-block-error"><?php echo form_error('payment_amount'); ?></span>
							</div>

							<div class="col-lg-6 col-md-6 col-sm-10 col-xs-12">
								<div class="check-box">
									<div class="check-wrapper">
										<input type="checkbox" name="payment_mode" id="check1" value="1" onclick="checkCheckBox(this.id)" <?php if(set_value('payment_mode', isset($records[0]->payment_mode) ? $records[0]->payment_mode : '') == 1){ echo 'checked'; } ?>   data-parsley-errors-container="#payment_mode-error" data-parsley-mincheck="1" required>
										<label for="check1"> </label>
										<span class="check-text"><?php echo $this->lang->line('cash'); ?></span>
									</div>
									<div class="check-wrapper">
										<input type="checkbox" name="payment_mode" id="check2" value="2" onclick="checkCheckBox(this.id)" <?php if(set_value('payment_mode', isset($records[0]->payment_mode) ? $records[0]->payment_mode : '') == 2){ echo 'checked'; } ?> >
										<label for="check2"> </label>
										<span class="check-text"><?php echo $this->lang->line('bank_transfer'); ?></span>
									</div>
									<div class="check-wrapper">
										<input type="checkbox" name="payment_mode" id="check3" value="3" onclick="checkCheckBox(this.id)" <?php if(set_value('payment_mode', isset($records[0]->payment_mode) ? $records[0]->payment_mode : '') == 3){ echo 'checked'; } ?> >
										<label for="check3"> </label>
										<span class="check-text"><?php echo $this->lang->line('cheque'); ?></span>
									</div>
								</div>
								<br><br>
								<span id="payment_mode-error" class="help-block help-block-error"><?php echo form_error('payment_mode'); ?></span>
							</div></div>
						</div>  <!--- payment div start -->
							 <div class="clearfix"></div>

						<div class="form-group"> <!-- special instructions div start -->
							<div class="col-lg-1 col-md-1 col-sm-1 hidden-xs">
								<label for="usr1"></label>
							</div>
							<div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
								<textarea type="text" rows="5" placeholder="<?php echo $this->lang->line('special_instuctions'); ?>" class="form-control text-area" id="special_instructions" name="special_instructions"  required><?php echo set_value('special_instructions', isset($records[0]->special_instructions) ? $records[0]->special_instructions : ''); ?></textarea>
								<span id="special_instructions-error" class="help-block help-block-error"><?php echo form_error('special_instructions'); ?></span>
							</div>
						</div>

						<div class="clearfix"></div>
						<!-- NEA certification div -->
						<div class="col-lg-1 col-md-1 col-sm-1 col-xs-12"></div>
						<div class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
							<label class="nea"><?php echo $this->lang->line('nea_certificate_req'); ?></label>
						</div>
                        <div class="clearfix">  </div>
  <div class="col-lg-1 col-md-1 col-sm-1 hidden-xs">
                <label for="usr1"></label>
              </div>
						<div class="col-lg-6 col-md-6 col-sm-10 col-xs-12">
							<div class="check-box">
								<div class="check-wrapper">
									<input type="checkbox"class="NEA_certification" name="required_NEA_certificate" id="check4" value="1"  <?php if(set_value('required_NEA_certificate', isset($records[0]->required_NEA_certificate) ? $records[0]->required_NEA_certificate : '') == 1){ echo 'checked'; } ?> >
									<label for="check4"> </label>
									<span class="check-text"><?php echo $this->lang->line('yes'); ?></span>
								</div>
								<div class="check-wrapper">
									<input type="checkbox" class="NEA_certification" name="required_NEA_certificate" id="check5 " value="0"  <?php if(set_value('required_NEA_certificate', isset($records[0]->required_NEA_certificate) ? $records[0]->required_NEA_certificate : '') == 0){ echo 'checked'; } ?> >
									<label for="check5"> </label>
									<span class="check-text"><?php echo $this->lang->line('no'); ?></span>
								</div>
								<span id="required_NEA_certification-error" class="help-block help-block-error"><?php echo form_error('required_NEA_certification'); ?></span>
							</div>
						</div>
						<div class="clearfix"></div>

					</div><!--- div org-box start -->
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  no-padding">
							<button class="create org-box col-lg-3 col-md-3 col-xs-12 pull-right" type="submit" name="save"><span><?php echo ($action == "Update") ?  $this->lang->line('update_template') :  $this->lang->line('save_template'); ?></span> <i class="fa fa-chevron-right"></i> </button>
					</div>
				</form>
				<div class="clearfix"></div>
			</div> <!--- parent form div close -->
		</div> <!--- div inner end -->
	</div><!-- row padding top one div end -->
</div><!-- main div close -->

<script type="text/javascript">
<?php if(isset($site_language) && $site_language ==  "chinese"){?>
	window.Parsley.setLocale('zh-cn');
<?php }else{ ?>
	window.Parsley.setLocale('en');
<?php }?>

$(".chosen-select").chosen();
var config = {
	'.chosen-select'           : {},
	'.chosen-select-deselect'  : {allow_single_deselect:true},
	'.chosen-select-no-single' : {disable_search_threshold:10},
	'.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
	'.chosen-select-width'     : {width:"95%"}
}
for (var selector in config) {
	$(".chosen-select").chosen(config[selector]);
}
$('.collapse').on('shown.bs.collapse', function(){
	$(this).parent().find(".glyphicon-plus").removeClass("glyphicon-plus").addClass("glyphicon-minus");
}).on('hidden.bs.collapse', function(){
	$(this).parent().find(".glyphicon-minus").removeClass("glyphicon-minus").addClass("glyphicon-plus");
});


	/* time picker code */
$('#timepicker1,#timepicker2').datetimepicker({
    pickDate: false,
    pickSeconds: false,
    format: "hh:mm",
}).on('changeDate', function(ev) {
	//alert("saa");
	// $(this).datetimepicker('hide');
});
$('#start_time').click(function () 	{
	$('#start_time_add_on').trigger("click");
});
$('#end_time').click(function () {
	$('#end_time_add_on').trigger("click");
});

$('#start_date_add_on').click(function () {
	$('#start_date').trigger("focus");
});
$('#end_date_add_on').click(function () {
	$('#end_date').trigger("focus");
});


$( document ).ready(function() {
	var nowTemp = new Date();
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
$('#start_date').datepicker({
	format: 'yyyy-mm-dd',
    pickTime: false,
     onRender: function(date) {
        return date.valueOf() < now.valueOf() ? 'disabled' : '';
      }
}).on('changeDate', function(ev) {
	var start_date = $("#start_date").val();
	var sDate = new Date(start_date);
	var today = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate());
	if(start_date != '' && today > sDate) {
		var alert_msg1 =  '<?php echo $this->lang->line('date_alert_message'); ?>';
		alert(alert_msg1);
		$( "#start_date" ).val('');
		$( "#end_date" ).val('');
	}
		var end_date = $("#end_date").val();
		var eDate = new Date(end_date);
		if(start_date != '' && end_date != '' && sDate > eDate) {
			var alert_msg5 =  '<?php echo $this->lang->line('end_date_grea_start_date'); ?>';
    		alert(alert_msg5);
			$( "#end_date" ).val('');
		}
		 $(this).datepicker('hide');
});

$('#end_date').datepicker({
	format: 'yyyy-mm-dd',
    pickTime: false,

     onRender: function(date) {
        return date.valueOf() < now.valueOf() ? 'disabled' : '';
      }
}).on('changeDate', function(ev) {
		var start_date = $("#start_date").val();
		var end_date = $("#end_date").val();
		var sDate = new Date(start_date);
		var eDate = new Date(end_date);
		//alert("S date "+ sDate +"E date" + eDate);
		if(start_date != '' && end_date != '' && sDate > eDate) {
			var alert_msg5 =  '<?php echo $this->lang->line('end_date_grea_start_date'); ?>';
			alert(alert_msg5);
			$( "#end_date" ).val('');
		}
		$(this).datepicker('hide');
	});
});


function checkCheckBox(checkbox){
	//alert(checkbox);
	for(var i=1;i<=3;i++){
		if(checkbox != 'check'+i) {
			$('#check'+i).attr('checked', false);
		}
	}

}

$(".NEA_certification").click( function() {
    var n = $(this).attr('name');
    var id = $(this).attr('id');
    $("[name=" + n + ']').prop("checked", false);
    $(this).prop("checked", true);
});

function validation() {
	var startDate = document.getElementById("start_date").value;
    var endDate = document.getElementById("end_date").value;
    var starttime = document.getElementById("start_time").value;
    var endtime = document.getElementById("end_time").value;
    if (startDate ==  endDate) {
			var aa1=starttime.split(":");
			var aa2=endtime.split(":");

			var d1=new Date(parseInt("2001",10),(parseInt("01",10))-1,parseInt("01",10),parseInt(aa1[0],10),parseInt(aa1[1],10),parseInt(aa1[2],10));
			var d2=new Date(parseInt("2001",10),(parseInt("01",10))-1,parseInt("01",10),parseInt(aa2[0],10),parseInt(aa2[1],10),parseInt(aa2[2],10));
			var dd1=d1.valueOf();
			var dd2=d2.valueOf();

			if(dd1>dd2)
			{
				var alert_msg4 =  '<?php echo $this->lang->line('end_time_alert_message'); ?>';
				alert(alert_msg4);
			}
	}
 }


</script>
