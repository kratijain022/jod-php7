<?php

$message = getGlobalMessage();


if($cancel_applicant_count == $acknowledged_applicant_count){
	$cancellation = true;
}else{
	$cancellation = false;
}

?>

<div class="right_col">
	 <?php
	if($message['type']=='success') {
	?>
  <div class="Metronic-alerts alert alert-success fade in" id="prefix_167847684379">
    <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
    <?php echo $message['msg'] ?></div>
  <?php
	} else if($message['type']=='error') {
	?>
  <div class="Metronic-alerts alert alert-danger fade in" id="prefix_167847684379">
    <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
    <?php echo $message['msg'] ?></div>
  <?php
	}
	?>
	<!-- yaha kaam krna hai -->
	<?php if(!empty($applicants) && $acknowledged_applicant_count > 0) {  ?>
	<div class="col-lg-12 col-md-12 col-sm-12 fbac">
		<h4>Feedback</h4>
		<p>Please grade your applicants based on 1 to 5 stars. 5 being the best.</p>
		<?php if(count($not_ack_applicants) > 0){ ?>

			<p class="black-box" style="color:red; padding: 4px; text-align: justify;"> <i class="fa fa-exclamation-triangle "></i> There are applicants whom did not acknowledge their job.  If these applicants did not turn up, you may continue to complete the job. If any of the applicants have turn up, please get them to acknowledge the job before you complete the job.   If you continue, applicants who did not acknowledge will be rejected by the system as no show.</p>
		<?php } ?>
	</div>


	<?php
		//rsolanki
		$pendingCheckApplinamrUserIds  = '';
		$applinamrDidNotClocinClockOut = '';
		foreach ($applicants as $key=>$applicant) {
			if($applicant->acknowledged == 1) {
				//check clock in and clock out time-
				//echo $applicant->user_account_id." - ".$applicant->job_id;
				$checkClickInApplicant = $this->jobs_model->checkPendingClockInOutJobs($applicant->job_id,$applicant->user_account_id);
				if($checkClickInApplicant>0){
					if($pendingCheckApplinamrUserIds=='')
					{
						$pendingCheckApplinamrUserIds = $applicant->user_account_id;
					}else{
						$pendingCheckApplinamrUserIds .= ",".$applicant->user_account_id;
					}
				}else{
					if($applinamrDidNotClocinClockOut=='')
					{
						$applinamrDidNotClocinClockOut = $applicant->user_account_id;
					}else{
						$applinamrDidNotClocinClockOut .= ",".$applicant->user_account_id;
					}
				}
			}
		}
	?>

	<?php
		if($pendingCheckApplinamrUserIds!=''){
		$totalRemain=explode(",",$pendingCheckApplinamrUserIds);
	?>
	<div class="col-lg-12 col-md-12 col-sm-12 fbac">
		<p class="black-box" style="color:red; padding: 4px; text-align: justify;">
		<i class="fa fa-exclamation-triangle "></i>
		There are <?php  echo count($totalRemain); ?> applicants whom not completed their job.
		 If any of the applicants have not completed, please get them to complete the
		 job before you complete the job.
		</p>
	</div>
	<?php } ?>


	<form action="<?php echo base_url('jobs/job_feedback/'.$job_details->job_id."/1"); ?>" enctype="multipart/form-data" method='post' data-parsley-validate onsubmit="validation()">
		<?php
			//show data
			foreach ($applicants as $key=>$applicant) {
			?>
			<?php if($applicant->acknowledged == 1) {
			?>
 			<!-- applicant div name header start-->
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 main-wrapper">
					<div class="org-box text-center head-title">
						<h3><?php echo $applicant->first_name." ".$applicant->last_name; ?> </h3>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 gray-back no-padding">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 left-sec no-padding">
							<span class="profile_img">
								<img style="width:203px; height:173px;" src="<?php echo checkImageFile(IMAGEURL.'applicantes/'.$applicant->portrait) == true ?  IMAGEURL.'applicantes/'.$applicant->portrait :  IMAGE.'/no_image.png'; ?>" />
								<span class="circle"><?php echo getTotalvoteCounts($applicant->applicant_user_account_id); ?></span>
							</span>
							<div class="clearfix"></div>
								 <input id="<?php echo "rating_".$applicant->applicant_user_account_id; ?>" class="rating" data-stars="5" data-step="1" data-size="sm" applicant_id="applicants_rating<?php echo $applicant->applicant_user_account_id; ?>" />
							<div class="clearfix"></div>
							<a href="<?php echo base_url('jobs/user_profile/'.$applicant->applicant_user_account_id); ?>">
							<button type="button" class="job-btn block100 org-box col-xs-12">
								<i class="glyphicon glyphicon-user"></i>View Profile
							</button></a>
						</div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
							<?php $clock_in_clock_out_details = get_job_details_data($job_details->outlet_id,$job_details->job_id,$applicant->			applicant_user_account_id);
								  $checkClickInApplicant_status = $this->jobs_model->checkPendingClockInOutJobs($job_details->job_id,$applicant->applicant_user_account_id);
								  $clock_out_status = $this->jobs_model->checkPendingClockInOutStatus($job_details->job_id,$applicant->applicant_user_account_id);
								//print_r($clock_out_status); die;
							?>

							<div class="wells well">
							<div  id="check_in_time_<?php echo $applicant->applicant_user_account_id; ?>" class="input-append date">
								<input data-format="yyyy-MM-dd hh:mm:ss" type="text" name="check_in_time[]" placeholder="Clock In Time" value="<?php echo  !empty($clock_out_status)? ($clock_out_status[0]->applicant_clockIn_status == 1 ? date('d M Y H:i ',strtotime($clock_out_status[0]->applicant_clockIn_dateTime)) : "" ): ""; ?>" required readonly />
							  </div>
							</div>

							<div class="wells well">
							  <div  id="check_out_time_<?php echo $applicant->applicant_user_account_id; ?>" class="input-append date">
								<input data-format="yyyy-MM-dd hh:mm:ss" type="text" name="check_out_time[]" placeholder="Clock Out Time" value="<?php echo !empty($clock_out_status)? ($clock_out_status[0]->applicant_clockOut_status == 1 ? date('d M Y H:i ',strtotime($clock_out_status[0]->applicant_clockOut_dateTime)) : ( $clock_out_status[0]->applicant_clockIn_status == 1 ? "0000-00-00 00:00" : "") ): ""; ?>" required  readonly />
							  </div>
							</div>

							<input type="text" id="meal_hour_<?php echo $applicant->applicant_user_account_id; ?>" name="meal_hour[]" class="clock-input" value="<?php echo !empty($clock_out_status) ? ($clock_out_status[0]->applicant_clockIn_status == 1 ? (!empty($clock_in_clock_out_details) && $clock_out_status[0]->applicant_clockOut_status  == 1 ? $clock_in_clock_out_details['meal_break_time']  : "N/A")  : "") :"";?>" placeholder="Total Meal Hour" required  readonly/>

							<input type="text" id="total_hours_<?php echo $applicant->applicant_user_account_id; ?>" name="amount[]" class="clock-input" value="<?php echo !empty($clock_out_status) ? ($clock_out_status[0]->applicant_clockIn_status == 1 ? (!empty($clock_in_clock_out_details) && $clock_out_status[0]->applicant_clockOut_status  == 1 ? $clock_in_clock_out_details['total_hours_worked'] : "0") : "") :"";?>" placeholder="Total Hours Worked" required readonly/>

							<input type="hidden" id="applicants_rating<?php echo $applicant->applicant_user_account_id; ?>" name="rating[]" value="" class="clock-input" placeholder="Total Amount Paid" />
							<input type="hidden" id="applicants_id_<?php echo $applicant->applicant_user_account_id; ?>" value="<?php echo $applicant->applicant_user_account_id; ?>" name="applicant_id[]" />
							<input type="hidden" id="jobID" value="<?php echo $job_details->job_id; ?>" name="jobID"/>
							<input type="hidden" id="applinamrDidNotClocinClockOut" value="<?php if(!empty($applinamrDidNotClocinClockOut)) { echo $applinamrDidNotClocinClockOut; } ?>" name="applinamrDidNotClocinClockOut"/>

						</div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
							<textarea class="additional" id="additional_comment_<?php echo $applicant->applicant_user_account_id; ?>" name="additional_comment[]" placeholder="Additional Comments..." required></textarea>
						</div>
					</div>
				</div>
				<script type="text/javascript">
					$(function () {
						$('.date').datetimepicker();
						//$('#check_out_time_<?php echo $applicant->applicant_user_account_id; ?>').datetimepicker();
					});
				</script>
			<?php } else {  ?>

			<?php } ?>
		<?php
		} // foreach loop close
		//die;

		if($pendingCheckApplinamrUserIds==''){
		?>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 m-t80">
				<input type="hidden" value="<?php echo count($applicants); ?>" name="applicants_count" />
				<button name="save" type="submit" class="create org-box col-lg-12 col-md-12 col-xs-12 pull-left"><span>Done </span>  </button>
			</div>
		<?php
		}else{
			//$totalRemain=explode(",",$pendingCheckApplinamrUserIds);
		?>
		<!--
		<div class="col-lg-12 col-md-12 col-sm-12 fbac">
			<p class="black-box" style="color:red; padding: 4px; text-align: justify;">
			<i class="fa fa-exclamation-triangle "></i>
			There are <?php  //echo count($totalRemain); ?> applicants whom not completed their job.
			 If any of the applicants have not completed, please get them to complete the
			 job before you complete the job.
			</p>
		</div>
		-->
		<?php
		}
		?>
</form>
	<?php } elseif($all_rejected == true) { ?>
		<form action="<?php echo base_url('jobs/job_compelete_feedback/'.$job_details->job_id); ?>" enctype="multipart/form-data" method='post'>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bmar">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 gray-back  gheight">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 gray-back  no-padding left-mar nob-mar job-box">
								<div class="col-lg-10 col-md-10 col-sm-10 col-xs-9 center-block no-float">
									<div >
										<h2 style="text-align:justify; word-break:keep-all;"><?php echo "All applicants either have cancelled the job or rejected by system or by Manager. So you can proceed to complete the job."; ?></h2>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<button name="save" type="submit" class="create org-box col-lg-12 col-md-12 col-xs-12 pull-left"><span>Compelete</span></button>
					</div>
				</div>
		</form>
	<?php } else{ ?>
		<?php if($cancellation == true ) { ?>
			<form action="<?php echo base_url('jobs/job_compelete_feedback/'.$job_details->job_id); ?>" enctype="multipart/form-data" method='post'>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bmar">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 gray-back  gheight">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 gray-back  no-padding left-mar nob-mar job-box">
									<div class="col-lg-10 col-md-10 col-sm-10 col-xs-9 center-block no-float">
										<div >
											<h2 style="text-align:justify; word-break:keep-all;"><?php echo "All applicants either did not acknowledge or have cancelled the job. If none of the applicants turns up, you may continue to reject all applicants and complete the job. If any of the applicants have turn up, please get them to acknowledge the job before you complete the job."; ?></h2>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<button name="save" type="submit" class="create org-box col-lg-12 col-md-12 col-xs-12 pull-left"><span>Compelete</span></button>
						</div>
					</div>
			</form>
		<?php }else{ ?>
		<div class="org-box text-center head-title">
			<h3>No Applicants has acknowledged for the job yet.</h3>
		</div>
		<?php } ?>
	<?php }?>
</div>
<script>
 $('.rating').on('rating.change', function(event, value, caption, target) {
		var id = "#"+$(this).attr('applicant_id');
		//console.log(id +' value = '+ value);
		$(id).val(value);
 });


</script>
