
<?php $message = getGlobalMessage(); ?>

<div class="right_col right_col job-payment" role="main">
	 <?php
		if($message['type']=='success') {
		?>
		  <div class="Metronic-alerts alert alert-success fade in" id="prefix_167847684379">
			<button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
			<?php echo $message['msg'] ?></div>
	  <?php
		} else if($message['type']=='error') {
		?>
		  <div class="Metronic-alerts alert alert-danger fade in" id="prefix_167847684379">
			<button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
			<?php echo $message['msg'] ?></div>
	  <?php
		}
	?>
	
	<div class="row padding-top-one">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		   <ul class="breadcrumb breadcrumb2">
			  <li><a href="<?php echo base_url(); ?>"><?php echo $this->lang->line('home'); ?></a></li>
			  <li class="active"> <?php echo $this->lang->line('payments'); ?></li>
			  <li class="active"><?php echo $this->lang->line('job_payments'); ?></li>
		   </ul>
		</div>
		<div class="col-md-12" > 
		   <form id="select_date_form" method="post">
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 bottom-mar">
				   <input readonly class="form-control f-15" name="date" id="date" type="text" placeholder="<?php echo $this->lang->line('job_start_date'); ?>" />
				</div> 
				<button class="create select-btn org-box col-lg-3 col-md-3 col-xs-12" type="submit" name="select_button" id="select_date_but"><span><?php echo $this->lang->line('select'); ?></span>
				</button>
			</form>
		</div>
		<div class="clerfix"></div>			
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<h2 class="credits-history"><?php echo $this->lang->line('job_payments'); ?></i>
			<a id="download_but" class="download_button_credit_his" href=""><i class="fa fa-download"></i></a>
			</h2>
				
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					 <table class="table table-th-gray table-striped" id="job_payment_table">
						<thead class="firt-top">
							<tr>
							   <th></th>
							   <th><?php echo $this->lang->line('full_name'); ?></th>
							   <th><?php echo $this->lang->line('job_title'); ?></th>
							   <th><?php echo $this->lang->line('outlet_name'); ?></th>
							   <th><?php echo $this->lang->line('clock_in_date'); ?></th>
							   <th><?php echo $this->lang->line('clock_out_date'); ?></th>
							   <th><?php echo $this->lang->line('total_hours_worked'); ?></th>
							   <th><?php echo $this->lang->line('total_jod_credits'); ?></th>
							   <th><?php echo $this->lang->line('action'); ?></th>
							</tr>
                        </thead>
						 <tbody>
						  
						 </tbody>
                    </table>
                    <div class="clearfix"></div>
				</div>
		</div>	
	</div>
</div>

<!-------------- Modal Pop up start ---------------------->
<div id="modal_popup_clockin_clockout" aria-labelledby="myLargeModalLabel" role="dialog" tabindex="-1" class="modal fade bs-example-modal-md in job-payment"  aria-hidden="false">
   <div class="modal-dialog col-lg-7 center mode-w mode-w2 ">
		<div class="modal-content back-white">
			 <button data-dismiss="modal" class="close-b"><i class="fa fa-times-circle"></i></button>
			<div class="col-md-12 col-xs-12 orange-back">
				<h3><?php echo $this->lang->line('applicants_job_details_u'); ?></h3>
			</div>
			<div class="clearfix"></div>
			<div class="">
				<div class="tab-content">
					   <div id="home" class="tab-pane fade in active">
						  <div class="contant col-md-12 col-mar">
							 <div class="col-md-6"><label class="control-label inblock clock-time text-left"><?php echo $this->lang->line('full_name'); ?>: </label><span class="text-mar" id="applicant_fullname"></span></div>
							 <div class="col-md-6"><label class="control-label inblock clock-time text-left">NRIC/FIN: </label><span id="nric_name" class="text-mar"></span></div>
							 <div class="clearfix"></div>
							 <div class="col-md-6">
								<div class="form-group disp-center no-margin">
								   <label class="control-label inblock clock-time  text-left"><?php echo $this->lang->line('clock_in_date'); ?></label>
								   <div class="w-125" id="clock_in_date_text">
									  
								   </div>
								</div>
							 </div>
							 <div class="col-md-6">
								<div class="form-group no-margin disp-center">
								   <label class="control-label inblock clock-time  text-left"><?php echo $this->lang->line('clock_in_time'); ?></label>
								   <div class="w-125" id="clock_in_time_text">
									 
								   </div>
								</div>
							 </div>
							 <div class="clearfix"></div>
							 <div class="col-md-6">
								<div id="datepicker1" class="form-group disp-center input-append date no-margin">
								   <label class="control-label inblock clock-time  text-left"><?php echo $this->lang->line('clock_out_date'); ?></label>
								   <div class="w-125" id="clock_out_date_text">
									  
								   </div>
								</div>
							 </div>
							 <div class="col-md-6">
								<div class="form-group no-margin disp-center">
								   <label class="control-label inblock clock-time  text-left"><?php echo $this->lang->line('clock_out_time'); ?></label>
								   <div class="w-125" id="clock_out_time_text">
									  
								   </div>
								</div>
							 </div>
							 <div class="clearfix"></div>
							 <div class="col-md-6"><label class="control-label inblock clock-time text-left"><?php echo $this->lang->line('total_break_time'); ?>: </label><span class="text-mar"  id="total_break_time_text" ></span></div>
							 <div class="col-md-6"><label class="control-label inblock clock-time text-left"><?php echo $this->lang->line('total_hours_worked'); ?>: </label><span class="text-mar" id="total_hours_worked_text"></span></div>
							 <div class="clearfix"></div>
							 <div class="col-md-6"><label class="control-label inblock clock-time text-left"><?php echo $this->lang->line('wages_per_hour'); ?>: </label><span class="text-mar"  id="wages_per_hour_text"></span></div>
							 <div class="col-md-6"><label class="control-label inblock clock-time text-left"><?php echo $this->lang->line('total_jod_credits'); ?>: </label><span class="text-mar" id="total_jod_credits_text"></span></div>
							 <div class="clearfix"></div>
							 <div class="col-md-6"><label class="control-label inblock clock-time text-left"><?php echo $this->lang->line('bank_name'); ?>: </label><span class="text-mar"  id="bank_name_text"></span></div>
							 <div class="col-md-6"><label class="control-label inblock clock-time text-left"><?php echo $this->lang->line('bank_account_name'); ?>: </label><span class="text-mar" id="bank_account_name_text"></span></div>
							  <div class="clearfix"></div>
							  <div class="col-md-6"><label class="control-label inblock clock-time text-left"><?php echo $this->lang->line('bank_account_number'); ?>: </label><span class="text-mar"  id="bank_account_number_text"></span></div>
							  <div class="col-md-6"><label class="control-label inblock clock-time text-left"><?php echo $this->lang->line('additional_jod_allowance'); ?>: </label><span class="text-mar"  id="additional_jod_allowance"></span></div>
							 <div class="clerfix"></div>
							 <div class="col-md-12"></div>
						  </div>
						
						  <div class="col-md-12">
							  <label style="text-align:center; float:left; width:100%; text-transform:uppercase; border-bottom:1px solid #ccc; padding-bottom:6px;"><?php echo $this->lang->line('super_admin_adjustments'); ?></label>
						  </div>
						  <div class="contant col-md-12 col-mar" id="adjusted_detail_div">
							 <div class="col-md-6">
								<div class="form-group disp-center no-margin">
								   <label class="control-label inblock clock-time  text-left"><?php echo $this->lang->line('clock_in_date'); ?>: </label>
								   <div class="w-125">
									   <div class="w-125" id="clock_in_date_text_bySA">
									  
									</div>
								   </div>
								</div>
							 </div>
							 <div class="col-md-6">
								<div class="form-group no-margin disp-center">
								   <label class="control-label inblock clock-time  text-left"><?php echo $this->lang->line('clock_in_time'); ?>: </label>
								  <div class="w-125" id="clock_in_time_text_bySA">
									  
									</div>
								  
								</div>
							 </div>
							 <div class="clearfix"></div>
							 <div class="col-md-6">
								<div id="datepicker1" class="form-group disp-center input-append date no-margin">
								   <label class="control-label inblock clock-time  text-left"><?php echo $this->lang->line('clock_out_date'); ?>: </label>
								   <div class="w-125">
									<div class="w-125" id="clock_out_date_text_bySA">
									  
									</div>
								   </div>
								</div>
							 </div>
							 <div class="col-md-6">
								<div class="form-group no-margin disp-center">
								   <label class="control-label inblock clock-time  text-left"><?php echo $this->lang->line('clock_out_time'); ?>: </label>
									<div class="w-125" id="clock_out_time_text_bySA">
									  
									</div>
								</div>
							 </div>
							 <div class="clearfix"></div>
							 <div class="col-md-6">
								<div class="form-group no-margin disp-center">
								   <label class="control-label inblock clock-time  text-left"><?php echo $this->lang->line('total_break_time'); ?>: </label>
									<div class="w-125" id="total_break_time_bySA">
									</div>
								</div>
							 </div>
							  <div class="col-md-6">
								<div class="form-group disp-center no-margin">
								   <label class="control-label inblock clock-time  text-left"><?php echo $this->lang->line('wages_per_hour'); ?>: </label>
								   <div class="w-125" id="wages_per_hour_bySA" >
									 
								   </div>
								</div>
							 </div>
							 <div class="clearfix"></div>
							   <div class="col-md-6"> 
									<div class="form-group disp-center no-margin">
									   <label class="control-label inblock clock-time  text-left"><?php echo $this->lang->line('total_jod_credits'); ?>:</label>
									   <div class="w-125" id="total_jod_credit_new">
										 
									   </div>
									</div>
								</div>
							  <div class="col-md-6">
								<div class="form-group disp-center no-margin">
								   <label class="control-label inblock clock-time  text-left"><?php echo $this->lang->line('adjusted_credit'); ?>:</label>
								   <div class="w-125" id="adjusted_credit_new">
									 
								   </div>
								</div>
							 </div>
							 
							  <div class="clearfix"></div>
							
							  <div class="col-md-12">
								   <label class="control-label inblock clock-time  text-left"><?php echo $this->lang->line('adjusted_on'); ?>: </label>
								   <div   style="margin-top: 9px; width: 100%;" id="adjusted_on_div" >
									 
								   </div>
								
							 </div>
							
							 <div class="col-md-12" style="padding-bottom: 5px;">
								   <label class="control-label inblock clock-time  text-left "><?php echo $this->lang->line('comments'); ?> :</label>
									<div style="margin-top: 9px; width: 100%;" id="comment_bySA" >
									 
								   </div>
							</div>
						  </div>
						  
						  
						  <div class="contant col-md-12 col-mar" id="no_adjustment_done_div" style="display:none; padding-bottom:10px;">
							<center><h2><?php echo $this->lang->line('no_adjustment_made'); ?></h2></center>
						  </div>
					   </div>
					</div>
				</div>
			</div>
			<div class="clerfix"></div>
		</div>
</div>

<script>
	$(document).ready(function() {
			var nowTemp = new Date();
			var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
			/* Date Picker code */
			$('#date').datepicker({
			format: 'yyyy-mm-dd',
			pickTime: false,
			minDate : now,
			 onRender: function(date) {
				//return date.valueOf() < back_2days_from_now.valueOf() ? 'disabled' : '';
			  }
			}).on('changeDate', function(ev) {
				var date_selected = $('#date').val();
				var download_url = "<?php echo base_url()."jobs/download_payment_summary/" ?>"+date_selected;
				$('#download_but').attr("href", download_url);
				$(this).datepicker('hide');
			});	
			
			
			<?php if(isset($siteLang) && $siteLang == "chinese") {  ?>
				var lang_url = "//cdn.datatables.net/plug-ins/1.10.12/i18n/Chinese.json";
			<?php }else{ ?>
				var lang_url = "//cdn.datatables.net/plug-ins/1.10.12/i18n/English.json";
			<?php }?>
			
			$("#select_date_form").submit(function(event) {
				var date = $('#date').val();
				if(date !="" && date != undefined ){
					var oTable;
					oTable = $('#job_payment_table').dataTable();
					oTable.fnFilter();
				}else{
					alert("Please select one date.");
				}
				event.preventDefault();
			});
			
			$('#job_payment_table').dataTable({
				"bPaginate": true,
				"bLengthChange": true,
				"bFilter": true,
				"bSort": true,
				"bInfo": true,
				"bAutoWidth": false,
				"order": [[ 0, "desc" ]],
				"columnDefs": [ { "targets": 0, "bSortable": true,"orderable": true, "visible": false }],
				"processing": true,
				"serverSide": true,
				"deferLoading": 0, // here
				"ajax": {
					"url": "<?php echo base_url('jobs/getPaymentDataByAjax'); ?>",
					"data": function (d) {
							d.date = $('#date').val();
						}
				},
				//"aoColumns": [null,null,null,null,null,null],
				"language": {
							"url": lang_url
						},
			});
			
			
			$( "#download_but" ).click(function() {
				var date = $('#date').val();
				if(date =="" || date == undefined ){
					alert("Please select a date.");
					return false;
				}
			});
	});
	
	function show_popup(job_id,applicant_user_account_id,outlet_id,$payment_table_primary_id){
		$.ajax({
					type: "POST",
					url: "<?php echo base_url();?>jobs/applicant_job_summary_details",
					data: {"job_id": job_id, "user_account_id": applicant_user_account_id, "outlet_id" : outlet_id},
					async: false,
					success: function( data, textStatus, jQxhr ){
						var obj_data = JSON.parse(data);
						$("#applicant_fullname").text(obj_data.data.applicant_full_name);
						$("#nric_name").text(obj_data.data.nric_unique_id);
						$("#clock_in_date_text").text(obj_data.data.clock_in_date_text);
						$("#clock_in_time_text").text(obj_data.data.clock_in_time);
						$("#clock_out_date_text").text(obj_data.data.clock_out_date_text);
						$("#clock_out_time_text").text(obj_data.data.clock_out_time);
						$("#total_break_time_text").text(obj_data.data.total_meal_break_time);
						$("#total_hours_worked_text").text(obj_data.data.total_hours_worked);
						$("#wages_per_hour_text").text(obj_data.data.wages_per_hour);
						$("#total_jod_credits_text").text(obj_data.data.total_jod_credit);
						$("#bank_name_text").text(obj_data.data.bank_name);
						$("#bank_account_name_text").text(obj_data.data.bank_account_name);
						$("#bank_account_number_text").text(obj_data.data.bank_account_number);
						$("#additional_jod_allowance").text(obj_data.data.additional_jod_allowance);
						
						if(obj_data.data.new_adjusted_data.new_clock_in_date_time != obj_data.data.old_clock_in_date_time ||obj_data.data.new_adjusted_data.new_clock_out_date_time !=  obj_data.data.old_clock_out_date_time ||  parseFloat(obj_data.data.new_adjusted_data.wages_per_hour_adjusted_SA) !=  parseFloat(obj_data.data.wages_per_hour) || obj_data.data.total_meal_break_time != obj_data.data.new_adjusted_data.new_meal_break_time ) {
								$("#clock_in_date_text_bySA").text(obj_data.data.new_adjusted_data.clock_in_date);
								$("#clock_in_time_text_bySA").text(obj_data.data.new_adjusted_data.clock_in_hour+":"+obj_data.data.new_adjusted_data.clock_in_min);
								$("#clock_out_date_text_bySA").text(obj_data.data.new_adjusted_data.clock_out_date);
								$("#clock_out_time_text_bySA").text(obj_data.data.new_adjusted_data.clock_out_hour+":"+obj_data.data.new_adjusted_data.clock_out_min);
								$("#total_break_time_bySA").text(obj_data.data.new_adjusted_data.meal_hour+":"+obj_data.data.new_adjusted_data.meal_min);
								$("#wages_per_hour_bySA").text(obj_data.data.new_adjusted_data.wages_per_hour_adjusted_SA);
								$("#comment_bySA").text(obj_data.data.new_adjusted_data.comments_SA);
								$("#adjusted_on_div").text(obj_data.data.new_adjusted_data.created_at);
								$('#total_jod_credit_new').html(obj_data.data.new_adjusted_data.total_jod_credit);
								
								$('#adjusted_credit_new').html(parseFloat(obj_data.data.new_adjusted_data.jod_credit_difference).toFixed(2));
							
								$('#adjusted_detail_div').show();
								$('#no_adjustment_done_div').hide();
						}else{
							$('#adjusted_detail_div').hide();
							$('#no_adjustment_done_div').show();
						}
						
						$("#modal_popup_clockin_clockout").modal("show");
						
					},
					error: function( jqXhr, textStatus, errorThrown ){
						alert( errorThrown );
					}
				});		
	}
	
</script>
