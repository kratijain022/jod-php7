<div class="right_col right_col job-payment" role="main">
		 <div class="row padding-top-one">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			   <ul class="breadcrumb breadcrumb2">
				  <li><a href="<?php echo base_url(); ?>"><?php echo $this->lang->line('home'); ?></a></li>
				  <li class="active"> <?php echo $this->lang->line('payments'); ?></li>
				  <li class="active"><?php echo $this->lang->line('applicants_job_summary'); ?></li>
			   </ul>
			</div>
		   <div class="col-md-12" > 
			   <form id="select_date_form" method="post">
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 bottom-mar">
					   <input readonly class="form-control f-15" name="date" id="date" type="text" placeholder="<?php echo $this->lang->line('job_start_date'); ?>" />
					</div> 
					<button class="create select-btn org-box col-lg-3 col-md-3 col-xs-12" type="submit" name="select_button" id="select_date_but"><span><?php echo $this->lang->line('select'); ?></span>
					</button>
				</form>
			</div>
			 <div class="clerfix"></div>			
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h2 class="credits-history"><?php echo $this->lang->line('applicants_job_summary'); ?></i></h2>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <table class="table table-th-gray table-striped" id="job_summary_table">
                                 <thead class="firt-top">
                                    <tr>
									   <th></th>
                                       <th><?php echo $this->lang->line('full_name'); ?></th>
                                       <th><?php echo $this->lang->line('job_id'); ?></th> 
                                       <th><?php echo $this->lang->line('job_title'); ?></th>
                                       <th><?php echo $this->lang->line('outlet_name'); ?></th>
                                       <th><?php echo $this->lang->line('status'); ?></th>
                                       <th><?php echo $this->lang->line('action'); ?></th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                  
                                 </tbody>
                              </table>
                              <div class="clearfix"></div>
                           </div>
                           <div class="clearfix"></div>
                        </div>
                     </div>
                  </div>
                  
				<div id="modal_popup_clockin_clockout" aria-labelledby="myLargeModalLabel" role="dialog" tabindex="-1" class="modal fade bs-example-modal-md in job-payment"  aria-hidden="false">
				<form id="clock_in_clock_out_adjusted_by_SA" method="post" name="clock_in_clock_out_adjusted_by_SA" 	data-parsley-validate >
					   <div class="modal-dialog col-lg-7 center mode-w mode-w2 ">
						  <div class="modal-content back-white">
							   <button data-dismiss="modal" class="close-b"><i class="fa fa-times-circle"></i></button>
							 <div class="col-md-12 col-xs-12 orange-back">
								<h3><?php echo $this->lang->line('applicants_job_details_u'); ?></h3>
							 </div>
							 <div class="clearfix"></div>
							 <div class="">
								<ul class="nav nav-tabs job-details">
								   <li class="active" style="width:50%"><a data-toggle="tab" href="#home"><?php echo $this->lang->line('job_details'); ?></a></li>
								   <li style="width:50%"><a data-toggle="tab" href="#menu1"><?php echo $this->lang->line('feedback'); ?> </a></li>
								</ul>
								<div class="tab-content">
								   <div id="home" class="tab-pane fade in active">
									  <div class="contant col-md-12 col-mar">
										
										 <div class="col-md-6"><label class="control-label inblock clock-time text-left"><?php echo $this->lang->line('full_name'); ?>: </label><span class="text-mar" id="applicant_fullname"></span></div>
										 <div class="col-md-6"><label class="control-label inblock clock-time text-left">NRIC/FIN: </label><span id="nric_name" class="text-mar"></span></div>
										 <div class="clearfix"></div>
										 <div class="col-md-6">
											<div class="form-group disp-center no-margin">
											   <label class="control-label inblock clock-time  text-left"><?php echo $this->lang->line('clock_in_date'); ?></label>
											   <div class="w-125" id="clock_in_date_text">
												  
											   </div>
											</div>
										 </div>
										 <div class="col-md-6">
											<div class="form-group no-margin disp-center">
											   <label class="control-label inblock clock-time  text-left"><?php echo $this->lang->line('clock_in_time'); ?></label>
											   <div class="w-125" id="clock_in_time_text">
												 
											   </div>
											</div>
										 </div>
										 <div class="clearfix"></div>
										 <div class="col-md-6">
											<div id="datepicker1" class="form-group disp-center input-append date no-margin">
											   <label class="control-label inblock clock-time  text-left"><?php echo $this->lang->line('clock_out_date'); ?></label>
											   <div class="w-125" id="clock_out_date_text">
												  
											   </div>
											</div>
										 </div>
										 <div class="col-md-6">
											<div class="form-group no-margin disp-center">
											   <label class="control-label inblock clock-time  text-left"><?php echo $this->lang->line('clock_in_time'); ?></label>
											   <div class="w-125" id="clock_out_time_text">
												  
											   </div>
											</div>
										 </div>
										 <div class="clearfix"></div>
										 <div class="col-md-6"><label class="control-label inblock clock-time text-left"><?php echo $this->lang->line('total_break_time'); ?>: </label><span class="text-mar"  id="total_break_time_text" ></span></div>
										 <div class="col-md-6"><label class="control-label inblock clock-time text-left"><?php echo $this->lang->line('total_hours_worked'); ?>: </label><span class="text-mar" id="total_hours_worked_text"></span></div>
										 <div class="clearfix"></div>
										 <div class="col-md-6"><label class="control-label inblock clock-time text-left"><?php echo $this->lang->line('wages_per_hour'); ?>: </label><span class="text-mar"  id="wages_per_hour_text"></span></div>
										 <div class="col-md-6"><label class="control-label inblock clock-time text-left"><?php echo $this->lang->line('total_jod_credits'); ?>: </label><span class="text-mar" id="total_jod_credits_text"></span></div>
										 <div class="clearfix"></div>
										 <div class="clerfix"></div>
										 <div class="col-md-12"></div>
									  </div>
									
									  <div class="col-md-12">
										  <label style="text-align:center; float:left; width:100%; text-transform:uppercase; border-bottom:1px solid #ccc; padding-bottom:6px;"><?php echo $this->lang->line('super_admin_adjustments'); ?></label>
									  </div>
									  
									  <div class="contant col-md-12 col-mar">
										<div id="new_details">
										
										 <div class="col-md-6">
											<div class="form-group disp-center no-margin">
											   <label class="control-label inblock clock-time  text-left"><?php echo $this->lang->line('clock_in_date'); ?></label>
											   <div class="w-125">
												  <input class="optional_disable" type="text" readonly required placeholder="Clock In Date" value="" id="clock_in_date" name="clock_in_date" autocomplete="off">
											   </div>
											</div>
										 </div>
										 <div class="col-md-6">
											<div class="form-group no-margin disp-center">
											   <label class="control-label inblock clock-time  text-left"><?php echo $this->lang->line('clock_in_time'); ?></label>
											   <div class="select-box inblock">
												  <select id="clock_in_hour" class="select2_group form-control optional_disable" name="clock_in_hour">
													 
												  </select>
											   </div>
											   <div class="select-box inblock">
												  <select id="clock_in_min" class="select2_group form-control optional_disable" name="clock_in_min">
													
												  </select>
											   </div>
											</div>
										 </div>
										 <div class="clearfix"></div>
										 <div class="col-md-6">
											<div id="datepicker1" class="form-group disp-center input-append date no-margin">
											   <label class="control-label inblock clock-time  text-left"><?php echo $this->lang->line('clock_out_date'); ?></label>
											   <div class="w-125">
												  <input type="text" class="optional_disable" readonly required placeholder="Clock Out Date" value="" id="clock_out_date" name="clock_out_date" autocomplete="off">
											   </div>
											</div>
										 </div>
										 <div class="col-md-6">
											<div class="form-group no-margin disp-center">
											   <label class="control-label inblock clock-time  text-left"><?php echo $this->lang->line('clock_out_time'); ?></label>
											   <div class="select-box inblock">
												  <select id="clock_out_hour" class="select2_group form-control optional_disable" name="clock_out_hour">
													 
												  </select>
											   </div>
											   <div class="select-box inblock">
												  <select id="clock_out_min" class="select2_group form-control optional_disable" name="clock_out_min">
													 
												  </select>
											   </div>
											</div>
										 </div>
										 <div class="clearfix"></div>
										 <div class="col-md-6">
											<div class="form-group no-margin disp-center">
											   <label class="control-label inblock clock-time  text-left"><?php echo $this->lang->line('total_break_time'); ?></label>
											   <div class="select-box inblock">
												  <select name="meal_hour" id="meal_hour" class="select2_group form-control optional_disable">
													 <option value="00">00</option>
													 <option value="01">01</option>
													 <option value="02">02</option>
													 <option value="03">03</option>
													 <option value="04">04</option>
												  </select>
											   </div>
											   <div class="select-box inblock ">
												  <select name="meal_min" id="meal_min" class="select2_group form-control optional_disable">
													 <option value="00">00</option>
													 <option value="15">15</option>
													 <option value="30">30</option>
													 <option value="45">45</option>
												  </select>
											   </div>
											</div>
										 </div>
										  <div class="col-md-6">
											<div class="form-group disp-center no-margin">
											   <label class="control-label inblock clock-time  text-left"><?php echo $this->lang->line('wages_per_hour'); ?></label>
											   <div class="w-125">
												  <input type="text" class="optional_disable" required placeholder="Wages/hr" value="" id="wages_per_hour" name="wages_per_hour" autocomplete="off">
											   </div>
											</div>
										 </div>
										 
										<div class="clearfix"></div>
										
												  <div class="col-md-6" id="new_data_related_adjustment1"  style="display:none;"> 
													<div class="form-group disp-center no-margin">
													   <label class="control-label inblock clock-time  text-left"><?php echo $this->lang->line('total_jod_credits'); ?></label>
													   <div class="w-125" id="total_jod_credit_new">
														 
													   </div>
													</div>
												 </div>
												 
												  <div class="col-md-6" id="new_data_related_adjustment2"  style="display:none;">
													<div class="form-group disp-center no-margin" >
													   <label class="control-label inblock clock-time  text-left"><?php echo $this->lang->line('adjusted_credit'); ?></label>
													   <div class="w-125" id="adjusted_credit_new"> 
													   </div>
													</div>
												</div>
									
									
										 <div class="clearfix"></div>
										   <div class="col-md-6" id="" > 
												<div class="form-group disp-center no-margin">
													<label class="control-label inblock clock-time  text-left"><?php echo $this->lang->line('bank_name'); ?></label>
													<div class="w-125" id="bank_name_div">
														 <input type="text" class="optional_disable" required placeholder="<?php echo $this->lang->line('bank_name'); ?>" value="" id="bank_name_input" name="bank_name_input" autocomplete="off">
													</div>
												</div>
										 </div>
										 
										<div class="col-md-6" id="" > 
											<div class="form-group disp-center no-margin">
											   <label class="control-label inblock clock-time  text-left"><?php echo $this->lang->line('bank_account_name'); ?></label>
											   <div class="w-125" id="bank_name_div">
												 <input type="text" class="optional_disable" required placeholder="<?php echo $this->lang->line('bank_account_name'); ?>" value="" id="bank_account_name_input" name="bank_account_name_input" autocomplete="off">
											   </div>
											</div>
										</div>
										
										<div class="col-md-6" id="" > 
											<div class="form-group disp-center no-margin">
											   <label class="control-label inblock clock-time  text-left"><?php echo $this->lang->line('bank_account_number'); ?></label>
											   <div class="w-125" id="bank_name_div">
												 <input type="text" class="optional_disable" required placeholder="<?php echo $this->lang->line('bank_account_number'); ?>" value="" id="bank_account_number_input" name="bank_account_number_input" autocomplete="off">
											   </div>
											</div>
										</div>
										<div class="col-md-6" id="" > 
											<div class="form-group disp-center no-margin">
											   <label class="control-label inblock clock-time  text-left"><?php echo $this->lang->line('additional_jod_allowance'); ?></label>
											   <div class="w-125" id="bank_name_div">
												 <input type="text" class="optional_disable" required placeholder="<?php echo $this->lang->line('additional_jod_allowance'); ?>" value="" id="additional_jod_allowance" name="additional_jod_allowance" autocomplete="off">
											   </div>
											</div>
										</div>
										
										<div class="clearfix"></div>
										 <div class="col-md-12">
											   <label class="control-label inblock clock-time  text-left "><?php echo $this->lang->line('comments'); ?></label>
												<div class="form-group no-margin ">
													<textarea placeholder="<?php echo $this->lang->line('comments'); ?>" class="optional_disable comment-sa"  id="super_admin_comments" name="super_admin_comments"  class="text-area text_area_border"></textarea>
													 <div class="clearfix"></div>
												</div>
										</div>
										<div class="col-md-12"></div>
										<div class="clearfix"></div>
									</div>
										<div class="contant col-mar" id="no_adjustment_done_div" style="display:none; padding-bottom:10px;">
											  <div class="">
													<center><h2><?php echo $this->lang->line('no_adjustment_made'); ?></h2></center>
											  </div>
										</div>
										<input type="hidden" name="old_meal_hours" value="" id="old_meal_hours" >
										<input type="hidden" name="job_id_input" value="" id="job_id" >
										<input type="hidden" value="" name="user_account_id_input" id="user_account_id" >
										<input type="hidden" value="" name="outlet_id" id="outlet_id" >
										<input type="hidden" value="" name="old_wages_per_hour" id="old_wages_per_hour" >
										 
									  </div>
								   </div>
								   <div id="menu1" class="tab-pane fade">
									  <div class="contant col-md-12 col-mar">
										 <div class="clearfix"></div>
										 <div class="col-md-12"><strong style="text-align:center; float:left; width:100%; text-transform:uppercase; border-bottom:1px solid #ccc; padding-bottom:6px;"><?php echo $this->lang->line('applicant_feedback'); ?></strong></div>
										 <div class="col-md-6">
											<form>
											   <input id="outlet_rating_star" class="rating" min="0" max="5" step="0.5" data-size="sm"
												  data-symbol="&#xf005;" data-glyphicon="false" data-rating-class="rating-fa">
											</form>
										 </div>
										 <div class="col-md-6" id="applicant_comment"> </div>
										 <div class="col-md-12"><strong  style="text-align:center; float:left; width:100%;  text-transform:uppercase; border-bottom:1px solid #ccc; padding-bottom:6px;"><?php echo $this->lang->line('managers_feedback'); ?></strong></div>
										 <div class="col-md-6">
											<form>
											   <input id="applicant_rating" class="rating" min="0" max="5" step="0.5" data-size="sm"
												  data-symbol="&#xf005;" data-glyphicon="false" data-rating-class="rating-fa">
											</form>
										 </div>
										 <div class="col-md-6" id="managers_comment"></div>
										 <div class="clerfix"></div>
										 <div class="col-md-12"></div>
									  </div>
								   </div>
								</div>
							 </div>
							 <div class="clerfix"></div>
							 <div class="fotter" id="footer">
								 <div  id="buttton_div">
									<button type="submit" class="col-md-6 col-xs-12 text-center green-box ad-man"><?php echo $this->lang->line('confirm_pay'); ?></button>
								</div>	
								<button data-dismiss="modal" type="button" class="col-md-6 col-xs-12 text-center red-box ad-man"><?php echo $this->lang->line('close'); ?></button></div>
							 <div class="clerfix"></div>
						  </div>
					   </div>
				
				</form>
				</div>
				
				<div id="other_then_clockin_clockout_poupup" aria-labelledby="myLargeModalLabel" role="dialog" tabindex="-1" class="modal fade bs-example-modal-md in job-payment"  tabindex="-1" aria-hidden="false">
					
					   <div class="modal-dialog col-lg-7 center Center-model mode-w mode-w2 ">
						   <button data-dismiss="modal" class="close-b"><i class="fa fa-times-circle"></i></button>
							<div class="modal-content back-white">
								<div class="col-md-12 col-xs-12 orange-back">
									<h3><?php echo $this->lang->line('applicants_job_details_u'); ?></h3>
								</div>
								<div class="clearfix"></div>
								
									<div class="tab-content message_content_popup">
										<div id="home" class="tab-pane fade in active">
											<div class="contant col-md-12 col-mar" id="message_content">
											</div>
										</div>
									
								</div>
								<div class="clerfix"></div>
							</div>
					   </div>
				</div>
    <script>
		
	<?php if(isset($site_language) && $site_language ==  "chinese"){?>
		window.Parsley.setLocale('zh-cn');
	<?php }else{ ?>
		window.Parsley.setLocale('en');
	<?php }?>
		$('.rating').rating('refresh', {
			disabled:true,
			showCaption: false
		});
		
		
		$(document).ready(function() {
			
			var nowTemp = new Date();
			var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
			/* Date Picker code */
			$('#date').datepicker({
			format: 'yyyy-mm-dd',
			pickTime: false,
			minDate : now,
			 onRender: function(date) {
				//return date.valueOf() < back_2days_from_now.valueOf() ? 'disabled' : '';
			  }
			}).on('changeDate', function(ev) {
				$(this).datepicker('hide');
			});
			
			
			/* Date picker clock in date and clock out date */
			$('#clock_out_date').datepicker({
				format: 'yyyy-mm-dd',
				pickTime: false,
				minDate : now,
				onRender: function(date) {
					//return date.valueOf() < back_2days_from_now.valueOf() ? 'disabled' : '';
				  }
			}).on('changeDate', function(ev) {
				var clock_in_date = $("#clock_in_date").val();
				var clock_out_date = $("#clock_out_date").val();
				var sDate = new Date(clock_in_date);
				var eDate = new Date(clock_out_date);
				if(clock_in_date != '' && clock_out_date != '' && sDate > eDate) {
					alert("<?php echo $this->lang->line('date_range_alert_message'); ?>");
					$( "#clock_out_date" ).val('');
				}
					 $(this).datepicker('hide');
			});
		
			$('#clock_in_date').datepicker({
				format: 'yyyy-mm-dd',
				pickTime: false,
				minDate : now,
				 onRender: function(date) {
					//return date.valueOf() < back_2days_from_now.valueOf() ? 'disabled' : '';
				  }
			}).on('changeDate', function(ev) {
				$(this).datepicker('hide');
			});
			
			
			$("#select_date_form").submit(function(event) {
				var date = $('#date').val();
				if(date !="" && date != undefined ){
					var oTable;
					oTable = $('#job_summary_table').dataTable();
					oTable.fnFilter();
				}else{
					alert("Please select one date.");
				}
				event.preventDefault();
			});
			
			
			<?php if(isset($siteLang) && $siteLang == "chinese") {  ?>
				var lang_url = "//cdn.datatables.net/plug-ins/1.10.12/i18n/Chinese.json";
			<?php }else{ ?>
				var lang_url = "//cdn.datatables.net/plug-ins/1.10.12/i18n/English.json";
			<?php }?>	
			
			$('#job_summary_table').dataTable({
				"bPaginate": true,
				"bLengthChange": true,
				"bFilter": true,
				"bSort": true,
				"bInfo": true,
				"bAutoWidth": false,
				"order": [[ 0, "desc" ]],
				"columnDefs": [ { "targets": 0, "bSortable": true,"orderable": true, "visible": false }],
				"processing": true,
				"serverSide": true,
				"deferLoading": 0, // here
				"ajax": {
					"url": "<?php echo base_url('jobs/getJobSummaryByAjax'); ?>",
					"data": function (d) {
							d.date = $('#date').val();
						}
				},
				"aoColumns": [null,null,null,null,null,null,null],
				"language": {
							"url": lang_url
						},
			});
			
			/* Pop up form submit */
		 
			$("#clock_in_clock_out_adjusted_by_SA").submit(function(event) {
				var clock_in_date = $("#clock_in_date").val();
				var clock_in_hour = $("#clock_in_hour").val();
				var clock_in_min = $("#clock_in_min").val();
				var clock_out_date = $("#clock_out_date").val();
				var clock_out_hour = $("#clock_out_hour").val();
				var clock_out_min = $("#clock_out_min").val();
				
				var clockin_date_time 	= clock_in_date+" "+clock_in_hour+":"+clock_in_min+":"+"00";
				var clockout_date_time = clock_out_date+" "+clock_out_hour+":"+clock_out_min+":"+"00";
				clockout_date_time = clockout_date_time.replace(/-/g, "/"); 
				clockin_date_time  = clockin_date_time.replace(/-/g, "/"); 
				var cInDateTime = new Date(clockin_date_time);
				var cOutDateTime = new Date(clockout_date_time); 
				 $("#clock_in_date").removeClass('parsley-success');
				 $("#clock_out_date").removeClass('parsley-success');
				 $("#wages_per_hour").removeClass('parsley-success');
				 $("#super_admin_comments").removeClass('parsley-success');
				if (cInDateTime < cOutDateTime){
					var form_data  = $(this).serialize();
					$.ajax({
						type: "POST",
						url: "<?php echo base_url();?>jobs/applicants_clockin_clockout_details_adjusted_by_super_admin",
						data: form_data,
						async: false,
						success: function( data, textStatus, jQxhr ){
							if(data == 1){
								alert("<?php echo $this->lang->line('details_adjusted_successfully'); ?>");
								$("#modal_popup_clockin_clockout").modal("hide");
							}else if(data == 2){
									alert("<?php echo $this->lang->line('othere_transaction_progress'); ?>");
									$("#modal_popup_clockin_clockout").modal("hide");
							}else if(data == 3){
									alert("Please fill the comments field.");
							}		
							else{
								alert("<?php echo $this->lang->line('adjustment_already_done'); ?>");	
								$("#modal_popup_clockin_clockout").modal("hide");
							}
							
						},
						error: function( jqXhr, textStatus, errorThrown ){
							alert( errorThrown );
						}
					});	
					event.preventDefault();
				}else{
					alert("<?php echo $this->lang->line('clock_out_grt_then_clock_in'); ?>");
					event.preventDefault();
				}
			});
		});
		
		
		
		function n(n){
			return n > 9 ? "" + n: "0" + n;
		}
		function show_popup(job_id,user_account_id,outlet_id,status){
			if(status == "6" || status == "14" ) {// When applicant has done the clock out 
				$.ajax({
					type: "POST",
					url: "<?php echo base_url();?>jobs/applicant_job_summary_details",
					data: {"job_id": job_id, "user_account_id": user_account_id, "outlet_id" : outlet_id},
					async: false,
					success: function( data, textStatus, jQxhr ){
					
						var obj_data = JSON.parse(data);
						if(obj_data.status == true && obj_data.data != false){
							$("#applicant_fullname").text(obj_data.data.applicant_full_name);
							$("#nric_name").text(obj_data.data.nric_unique_id);
							$("#clock_in_date_text").text(obj_data.data.clock_in_date_text);
							$("#clock_in_time_text").text(obj_data.data.clock_in_time);
							$("#clock_out_date_text").text(obj_data.data.clock_out_date_text);
							$("#clock_out_time_text").text(obj_data.data.clock_out_time);
							$("#total_break_time_text").text(obj_data.data.total_meal_break_time);
							$("#total_hours_worked_text").text(obj_data.data.total_hours_worked);
							$("#wages_per_hour_text").text(obj_data.data.wages_per_hour);
							$("#total_jod_credits_text").text(obj_data.data.total_jod_credit);
							var meal_break_time = obj_data.data.meal_hour+":"+obj_data.data.meal_min;
					
							$("#old_meal_hours").val(meal_break_time);
							$("#job_id").val(job_id);
							$("#user_account_id").val(user_account_id);
							$("#outlet_id").val(outlet_id);
							
							$('#clock_in_date').val('');
							$('#clock_out_date').val('');
							$('#clock_in_date').val(obj_data.data.clock_in_date);
							$('#clock_out_date').val(obj_data.data.clock_out_date);
							
							$('#clock_in_hour').empty();
							for(i=00; i<=23; i++){
								$('#clock_in_hour').append(
								$("<option></option>")
								  .attr("value", n(i))
								  .text(n(i))
								);
							}
							$("#clock_in_hour").val(obj_data.data.clock_in_hour);
							$('#clock_in_min').empty();
							for(i=00; i<60; i++){
								$('#clock_in_min').append(
								$("<option></option>")
								  .attr("value", n(i))
								  .text(n(i))
								);
							}
							$("#clock_in_min").val(obj_data.data.clock_in_min);
							$('#clock_out_hour').empty();
							for(i=0; i<=23; i++){
								$('#clock_out_hour').append(
								$("<option></option>")
								  .attr("value", n(i))
								  .text(n(i))
								);
							}
							$("#clock_out_hour").val(obj_data.data.clock_out_hour);
							$('#clock_out_min').empty();
							for(i=0; i<60; i++){
								$('#clock_out_min').append(
								$("<option></option>")
								  .attr("value", n(i))
								  .text(n(i))
								);
							}
							$("#clock_out_hour").val(obj_data.data.clock_out_hour);
							
							$("#clock_out_min").val(obj_data.data.clock_out_min);
							$('#clock_out_min').empty();
							for(i=0; i<60; i++){
								$('#clock_out_min').append(
								$("<option></option>")
								  .attr("value", n(i))
								  .text(n(i))
								);
							}
							$("#clock_out_min").val(obj_data.data.clock_out_min);
							$("#meal_hour").val(obj_data.data.meal_hour);
							$("#meal_min").val(obj_data.data.meal_min);
							$("#wages_per_hour").val(obj_data.data.wages_per_hour);
							$("#old_wages_per_hour").val(obj_data.data.wages_per_hour);
							$("#bank_name_input").val(obj_data.data.bank_name);
							$("#bank_account_name_input").val(obj_data.data.bank_account_name);
							$("#bank_account_number_input").val(obj_data.data.bank_account_number);
							$("#additional_jod_allowance").val(obj_data.data.additional_jod_allowance);
							
							$('#new_details').show();
							$('#no_adjustment_done_div').hide();
							$('#new_data_related_adjustment1').hide();
							$('#new_data_related_adjustment2').hide();
							if(obj_data.data.transaction_already_done == true){
								$('#new_details').show();
								$('#no_adjustment_done_div').hide();
								$("#buttton_div").html("");
								if(obj_data.data.new_adjusted_data.new_clock_in_date_time != obj_data.data.old_clock_in_date_time ||obj_data.data.new_adjusted_data.new_clock_out_date_time !=  obj_data.data.old_clock_out_date_time ||  parseFloat(obj_data.data.new_adjusted_data.wages_per_hour_adjusted_SA) !=  parseFloat(obj_data.data.wages_per_hour) || obj_data.data.total_meal_break_time != obj_data.data.new_adjusted_data.new_meal_break_time ) {
									$('#footer').show();
									$("#buttton_div").html("<button type='button' class='col-md-6 col-xs-12 text-center gray-box ad-man'>"+"<?php echo $this->lang->line('payment_done'); ?>"+"</button>");
								}else{
									$('#new_details').hide();
									$('#no_adjustment_done_div').show();
									$('#new_data_related_adjustment1').hide();
									$('#new_data_related_adjustment2').hide();
									$('#credit_diif_new').hide();
									$('#footer').hide();
									$("#buttton_div").html("<button type='button' class='col-md-6 col-xs-12 text-center gray-box ad-man'>"+"<?php echo $this->lang->line('no_adjustment_made'); ?>"+"</button>");
								}
								if(obj_data.data.new_adjusted_data.length != 0){
									$('#new_data_related_adjustment1').show();
									$('#new_data_related_adjustment2').show();
								
									$('#total_jod_credit_new').html(obj_data.data.new_adjusted_data.total_jod_credit);
									$('#adjusted_credit_new').html(parseFloat(obj_data.data.new_adjusted_data.jod_credit_difference).toFixed(2));
									$('#adjusted_credit_new').html(parseFloat(obj_data.data.new_adjusted_data.jod_credit_difference).toFixed(2));
									$('#clock_in_date').val(obj_data.data.new_adjusted_data.clock_in_date);
									$('#clock_out_date').val(obj_data.data.new_adjusted_data.clock_out_date);
									$('#clock_in_hour').val(obj_data.data.new_adjusted_data.clock_in_hour);
									$('#clock_out_hour').val(obj_data.data.new_adjusted_data.clock_out_hour);
									$('#clock_in_min').val(obj_data.data.new_adjusted_data.clock_in_min);
									$('#clock_out_min').val(obj_data.data.new_adjusted_data.clock_out_min);
									$('#meal_hour').val(obj_data.data.new_adjusted_data.meal_hour);
									$('#meal_min').val(obj_data.data.new_adjusted_data.meal_min);
									$('#wages_per_hour').val(obj_data.data.new_adjusted_data.wages_per_hour_adjusted_SA);
									$('#super_admin_comments').val(obj_data.data.new_adjusted_data.comments_SA);
									$("#bank_name_input").val(obj_data.data.bank_name);
									$("#bank_account_name_input").val(obj_data.data.bank_account_name);
									$("#bank_account_number_input").val(obj_data.data.bank_account_number);
									$('.optional_disable').attr('disabled', true);
								}
							}else{
								$('#footer').show();
								$('#super_admin_comments').val("");
								$("#buttton_div").html("");
								$('.optional_disable').attr('disabled', false);
								$("#buttton_div").html("<button type='submit' class='col-md-6 col-xs-12 text-center green-box ad-man'>"+"<?php echo $this->lang->line('confirm_pay'); ?>"+"</button>");
							}
							/* check applicant feedback data and outlet feedback data is not empty */ 
							if(obj_data.outlet_feedback.length != 0){
								$('#outlet_rating_star').rating('update', obj_data.outlet_feedback.rating);
								$('#applicant_comment').html(obj_data.outlet_feedback.feedback_text);
								
							}
							if(obj_data.applicant_feedback.length != 0){
								$('#applicant_rating').rating('update', obj_data.applicant_feedback.rating);
								$('#managers_comment').html(obj_data.applicant_feedback.additional_comments);
								
							}
							$("#modal_popup_clockin_clockout").modal("show");
						}
						
					},
					error: function( jqXhr, textStatus, errorThrown ){
						alert( errorThrown );
					}
				});	
				
			}else{ 
				// Other cases then Clock out 
				if(status == 12){
					$('#message_content').html("<?php echo $this->lang->line('applicant_cancel_job_msg'); ?>");
				}else if(status == 15){
					$('#message_content').html("<?php echo $this->lang->line('applicant_rejected_time_overlap'); ?>");
				}else if(status == 3){
					$('#message_content').html("<?php echo $this->lang->line('applicant_not_selected_by_manager'); ?>");	
				}else if(status == 7){
					$('#message_content').html("<?php echo $this->lang->line('applicant_rejected_by_manager'); ?>");	
				}else if(status == 10){
					$('#message_content').html("<?php echo $this->lang->line('applicant_done_clock_in'); ?>");	
				}else if(status == 5){
					$('#message_content').html("<?php echo $this->lang->line('applicant_done_acknowledgement'); ?>");	
				}else if(status == 4 ){
					$('#message_content').html("<?php echo $this->lang->line('applicant_not_ack'); ?>");
				}
				$("#other_then_clockin_clockout_poupup").modal("show");
			}
				
				
		}
		
	</script>
	
<style>
.datepicker{z-index: 9991;}
</style>
    
