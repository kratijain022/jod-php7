<?php
$message = getGlobalMessage();
?>

<div class="right_col">
	 <?php
		if($message['type']=='success') {
		?>
		  <div class="Metronic-alerts alert alert-success fade in" id="prefix_167847684379">
			<button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
			<?php echo $message['msg'] ?></div>
	  <?php
		} else if($message['type']=='error') {
		?>
		  <div class="Metronic-alerts alert alert-danger fade in" id="prefix_167847684379">
			<button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
			<?php echo $message['msg'] ?></div>
	  <?php
		}
	?>
	<?php if(!empty($applicants) && $acknowledged_applicant_count > 0) {  ?>
	<div class="col-lg-12 col-md-12 col-sm-12 fbac">
		<h4><?php echo $this->lang->line('feedback'); ?></h4>
		<p><?php echo $this->lang->line('grade_raring'); ?></p>
		<?php /* first check any of the applicants who have not clock out */
				$not_clockout_flag = false;
				foreach ($applicants as $key=>$applicant) {
					if($applicant->clockInOutStatus == "1"){
						$not_clockout_flag = true;
						break;
					}else{
						$not_clockout_flag = false;
					}
				}
				if($not_clockout_flag == true){ ?>
					<p class="black-box" style="color:red; padding: 4px; text-align: justify;"> <i class="fa fa-exclamation-triangle "></i> <?php echo $this->lang->line("job_feedback_page_message_1"); ?></p>
				<?php }else{
					if($applicants_acknowledged_but_not_clock_in > 0){ ?>
						<p class="black-box" style="color:red; padding: 4px; text-align: justify;"> <i class="fa fa-exclamation-triangle "></i> <?php echo $this->lang->line("job_feedback_page_message_2"); ?></p>
				<?php 	}
					if(count($not_ack_applicants) > 0){ ?>
						<p class="black-box" style="color:red; padding: 4px; text-align: justify;"> <i class="fa fa-exclamation-triangle "></i> <?php echo $this->lang->line("job_feedback_page_message_3"); ?>.</p>
					<?php } ?>
			<?php } ?>
	</div>

	<form action="<?php echo base_url('jobs/job_feedback/'.$job_details->job_id."/1"); ?>" enctype="multipart/form-data" method='post' data-parsley-validate onsubmit="validation()" id="feedback_form">
		<?php
			//show data
			foreach ($applicants as $key=>$applicant) {
			?>
			<?php if($applicant->acknowledged == 1) {
			?>
 			<!-- applicant div name header start-->
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 main-wrapper">
					<div class="org-box text-center head-title">
						<h3><?php echo $applicant->first_name." ".$applicant->last_name; ?> </h3>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 gray-back no-padding">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 left-sec no-padding">
							<span class="profile_img">
								<img style="width:203px; height:173px;" src="<?php echo checkImageFile(IMAGEURL.'applicantes/'.$applicant->portrait) == true ?  IMAGEURL.'applicantes/'.$applicant->portrait :  IMAGE.'/no_image.png'; ?>" />
								<span class="circle"><?php echo getTotalvoteCounts($applicant->applicant_user_account_id); ?></span>
							</span>
							<div class="clearfix"></div>
								 <input id="<?php echo "rating_".$applicant->applicant_user_account_id; ?>" class="rating" data-stars="5" data-step="1" data-size="sm" applicant_id="applicants_rating<?php echo $applicant->applicant_user_account_id; ?>" />
							<div class="clearfix"></div>
							<a href="<?php echo base_url('jobs/user_profile/'.$applicant->applicant_user_account_id); ?>">
							<button type="button" class="job-btn block100 org-box col-xs-12">
								<i class="glyphicon glyphicon-user"></i><?php echo $this->lang->line('view_profile'); ?>
							</button></a>
						</div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
							<?php $clock_in_clock_out_details = get_job_details_data($job_details->outlet_id,$job_details->job_id,$applicant->			applicant_user_account_id);
								  $checkClickInApplicant_status = $this->jobs_model->checkPendingClockInOutJobs($job_details->job_id,$applicant->applicant_user_account_id);
								  $clock_out_status = $this->jobs_model->checkPendingClockInOutStatus($job_details->job_id,$applicant->applicant_user_account_id);
								//print_r($clock_out_status); die;
							?>

							<div class="wells well">
							<div  id="check_in_time_<?php echo $applicant->applicant_user_account_id; ?>" class="input-append date">
								<input data-format="yyyy-MM-dd hh:mm:ss" type="text" name="check_in_time[]" placeholder="<?php echo $this->lang->line('clock_in_time'); ?>" value="<?php echo  !empty($clock_out_status)? ($clock_out_status[0]->applicant_clockIn_status == 1 ? date('d M Y H:i ',strtotime($clock_out_status[0]->applicant_clockIn_dateTime)) : "" ): ""; ?>"  readonly />
							  </div>
							</div>

							<div class="wells well">
							  <div  id="check_out_time_<?php echo $applicant->applicant_user_account_id; ?>" class="input-append date">
								<input data-format="yyyy-MM-dd hh:mm:ss" type="text" name="check_out_time[]" placeholder="<?php echo $this->lang->line('clock_out_time'); ?>" value="<?php echo !empty($clock_out_status)? ($clock_out_status[0]->applicant_clockOut_status == 1 ? date('d M Y H:i ',strtotime($clock_out_status[0]->applicant_clockOut_dateTime)) : ( $clock_out_status[0]->applicant_clockIn_status == 1 ? "0000-00-00 00:00" : "") ): ""; ?>"   readonly />
							  </div>
							</div>

							<input type="text" id="meal_hour_<?php echo $applicant->applicant_user_account_id; ?>" name="meal_hour[]" class="clock-input" value="<?php echo !empty($clock_out_status) ? ($clock_out_status[0]->applicant_clockIn_status == 1 ? (!empty($clock_in_clock_out_details) && $clock_out_status[0]->applicant_clockOut_status  == 1 ? $clock_in_clock_out_details['meal_break_time']  : "N/A")  : "") :"";?>" placeholder="<?php echo $this->lang->line('total_meal_hour'); ?>"   readonly/>

							<input type="text" id="total_hours_<?php echo $applicant->applicant_user_account_id; ?>" name="amount[]" class="clock-input" value="<?php echo !empty($clock_out_status) ? ($clock_out_status[0]->applicant_clockIn_status == 1 ? (!empty($clock_in_clock_out_details) && $clock_out_status[0]->applicant_clockOut_status  == 1 ? $clock_in_clock_out_details['total_hours_worked'] : "0") : "") :"";?>" placeholder="<?php echo $this->lang->line('total_hours_worked'); ?>"  readonly/>

							<input type="hidden" id="applicants_rating<?php echo $applicant->applicant_user_account_id; ?>" name="rating[]" value="0" class="clock-input" placeholder="Total Amount Paid" />
							<input type="hidden" id="applicants_id_<?php echo $applicant->applicant_user_account_id; ?>" value="<?php echo $applicant->applicant_user_account_id; ?>" name="applicant_id[]" />
							<input type="hidden" id="jobID" value="<?php echo $job_details->job_id; ?>" name="jobID"/>
						</div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
							<textarea class="additional" id="additional_comment_<?php echo $applicant->applicant_user_account_id; ?>" name="additional_comment[]" placeholder="<?php echo $this->lang->line('additional_comments'); ?> ..." required></textarea>
						</div>
					</div>
				</div>

				<script type="text/javascript">
					$(function () {
						$('.date').datetimepicker();
					});
				</script>
			<?php } else {  ?>

			<?php } ?>
		<?php
		} // foreach loop close
		?>
			<?php if($not_clockout_flag == false ) { ?>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 m-t80">
						<input type="hidden" value="<?php echo count($applicants); ?>" name="applicants_count" />
						<button id="save_but" name="save" type="submit" class="create org-box col-lg-12 col-md-12 col-xs-12 pull-left"><span>Done </span>  </button>
					</div>
				<?php } ?>
</form>
	<?php } else{ ?>
			<?php if($all_rejected == true) { ?>
				<form action="<?php echo base_url('jobs/job_compelete_feedback/'.$job_details->job_id); ?>" enctype="multipart/form-data" method='post'>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bmar">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 gray-back  gheight">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 gray-back  no-padding left-mar nob-mar job-box">
									<div class="col-lg-10 col-md-10 col-sm-10 col-xs-9 center-block no-float">
										<div >
											<h2 style="text-align:justify; word-break:keep-all;"><?php echo $this->lang->line('no_ack_all_cancel'); ?></h2>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<button name="save" type="submit" class="create org-box col-lg-12 col-md-12 col-xs-12 pull-left"><span><?php echo $this->lang->line('complete'); ?></span></button>
						</div>
					</div>
			</form>
			<?php } else{ ?>
				<?php if(!empty($applicant_clock_in_not_clock_out)){  ?>	
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bmar">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 gray-back  gheight">
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 gray-back  no-padding left-mar nob-mar job-box">
										<div class="col-lg-10 col-md-10 col-sm-10 col-xs-9 center-block no-float">
											<div >
												<h2 style="text-align:justify; word-break:keep-all;"><?php echo $this->lang->line("job_feedback_page_message_1"); ?></h2>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					

				<?php  } else { ?>
					<form action="<?php echo base_url('jobs/job_compelete_feedback/'.$job_details->job_id); ?>" enctype="multipart/form-data" method='post'>
						<div class="org-box text-center head-title">
							<h3><?php echo $this->lang->line('no_applicants_ack');?></h3>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bmar">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 gray-back  gheight">
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 gray-back  no-padding left-mar nob-mar job-box">
										<div class="col-lg-10 col-md-10 col-sm-10 col-xs-9 center-block no-float">
											<div >
												<h2 style="text-align:justify; word-break:keep-all;"><?php echo $this->lang->line('warning_msg1'); ?></h2>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<button name="save" type="submit" class="create org-box col-lg-12 col-md-12 col-xs-12 pull-left"><span><?php echo $this->lang->line('complete'); ?></span></button>
							</div>
						</div>
					</form>
				<?php } ?>
			<?php } ?>
	<?php }?>
</div>
<script>
 $('.rating').on('rating.change', function(event, value, caption, target) {
		var id = "#"+$(this).attr('applicant_id');
		//console.log(id +' value = '+ value);
		$(id).val(value);
 });


</script>
