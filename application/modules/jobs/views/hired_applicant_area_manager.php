<?php
//echo '<pre>'; print_r($hired_applicants);die;
$message = getGlobalMessage();
	if($message['type']=='success') {
	?>
  <div class="Metronic-alerts alert alert-success fade in" id="prefix_167847684379">
    <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
    <?php echo $message['msg'] ?></div>
  <?php
	} else if($message['type']=='error') {
	?>
  <div class="Metronic-alerts alert alert-danger fade in" id="prefix_167847684379">
    <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
    <?php echo $message['msg'] ?></div>
  <?php
	}
	?>
<div class="right_col right_col" role="main">
	  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<ul class="breadcrumb breadcrumb2">
			<li><a href="<?php echo base_url(); ?>">Home</a></li>
			<li class="active"> Job Postings</li>
			<li >Active Job Postings</li>
			<li class="active">Hired Candidates</li>

			</ul>
		</div>
	<?php if(!empty($hired_applicants)): ?>
<!--	<form method="post" action="<?php echo BASEURL.'jobs/changeCandidateJobStatus/'.$hired_applicants[0]->job_id?>"> --->
		<?php foreach($hired_applicants as $applicant):
			if($applicant->job_status == 1 ): $flag=true;?>
				<input type="hidden" name="candidate_ids[]" id="cand_id<?php echo $applicant->applicant_user_account_id;?>">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 main-wrapper">
				  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 gray-back no-padding table-rate">
					 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 left-sec no-padding table-cell-rate">
						<span class="profile_img">
							<img style="width:203px; height:173px;" src="<?php echo  checkImageFile(IMAGEURL.'applicantes/'.$applicant->portrait) == true ? IMAGEURL.'applicantes/'.$applicant->portrait : IMAGE.'/no_image.png'; ?>" /><span class="circle"><?php echo getTotalvoteCounts($applicant->applicant_user_account_id); ?>
						</span></span>
						<div class="clearfix"></div>
							<div class="new_rate"><input id="<?php echo "rating_".$applicant->user_account_id; ?>" class="rating" data-stars="5" data-step="0.1" data-size="sm" rate="<?php echo getAverageRating($applicant->applicant_user_account_id);?>"/></div>
						<div class="clearfix"></div>
					 	<div class="high30"></div>
						<a href="<?php echo base_url('jobs/user_profile/'.$applicant->user_account_id); ?>"><button type="button" class="job-btn block100 org-box col-xs-12 ab-btn"><i class="glyphicon glyphicon-user"></i>View Profile</button></a>
					 </div>
					 <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 no-padding min-h321 table-cell-rate">
						<h3 class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><?php echo ucwords($applicant->first_name.' '.$applicant->last_name);?> <?php echo ($applicant->gender==1)?'(M)':'(F)';?></h3>
						<div class="clearfix"></div>
						<p class="discri col-lg-12 col-md-12 col-sm-12 col-xs-12 m-heig44 res-height">
							<?php
								$dob = date_create($applicant->date_of_birth);
								$today = date_create(date('Y-m-d'));
								$age = date_diff($dob,$today, true);
								echo $age->y;
							?> years old<br />
						   <span class="address-box"><?php echo strlen($applicant->address) > 110 ? substr($applicant->address,0,110).".." : $applicant->address ; ?></span><br />
						</p>
						<div class="clearfix"></div>
						<p class="discri col-lg-12 col-md-12 col-sm-12 col-xs-12"><span><input type="checkbox" <?php echo ($applicant->is_NEA_certified==1)?'checked="checked"':'';?>/> </span><span>NEA Food Handler</span> </p>
						<div class="clearfix"></div>
						<p class="discri col-lg-12 col-md-12 col-sm-12 col-xs-12 m-heig76 res-height">
							<?php $exprience_details =  getEmploymentHistory($applicant->applicant_user_account_id);
							if(!empty($exprience_details)):
							$exp_data = end($exprience_details);?>
							   Years of Experience: <?php echo $exp_data->length_of_service;?> <?php echo ($exp_data->length_of_service>1)?'yrs':'yr';?><br />
							    <span class="" title="<?php echo $exp_data->job_types; ?>">Job Roles: <?php echo strlen($exp_data->job_types) > 50 ?  substr($exp_data->job_types,0,50)."..." : $exp_data->job_types ;?></span> <br />
							   <span title="<?php echo $exp_data->employer; ?>">Past Employers: <?php echo strlen($exp_data->employer) > 40 ? substr($exp_data->employer,0,40).".." : $exp_data->employer ;?></span><br />
							<?php else: ?>
								No experience <br />
							<?php endif;?>
						</p>
						<?php if($applicant->acknowledged == 1 && $applicant->cancel_status == 0) { ?>
							<div class="job-btn block100  green-box org-box col-xs-12 mar-t10 text-center posi ab-btn" type="button"><i class="glyphicon glyphicon-flag"></i>Acknowledged</div>
						<?php }elseif(($applicant->acknowledged == 0 || $applicant->acknowledged == 1) && $applicant->cancel_status == 1){ ?>
							<div class="job-btn block100  red-box org-box col-xs-12 mar-t10 text-center posi ab-btn" type="button"><i class="glyphicon glyphicon-flag"></i>Self Cancelled</div>
						<?php }elseif($applicant->acknowledged == 0  && $applicant->cancel_status == 0){ ?>
								<div class="job-btn block100  black-box org-box col-xs-12 mar-t10 text-center posi ab-btn" type="button"><i class="glyphicon glyphicon-flag"></i>Not Acknowledged</div>
						<?php }?>
					 </div>

				  </div>
					<!----
					<?php if($applicant->job_status==0):?>
					  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no-padding right-cross">
						 <button type="button" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 green-box" id="select<?php echo $applicant->applicant_user_account_id;?>" value="<?php echo $applicant->applicant_user_account_id;?>" onclick="return setCandiDateId(this.value, 'select');">
							<i class="fa fa-check"></i><span>Select</span>
						 </button>
						 <button type="button" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 red-box" id="reject<?php echo $applicant->applicant_user_account_id;?>" value="<?php echo $applicant->applicant_user_account_id;?>" onclick="return setCandiDateId(this.value, 'reject');">
							<i class="fa fa-times"></i><span>Reject</span>
						 </button>
					  </div>
					<?php endif;?> ---->
				</div>
				<div class="clearfix"></div>
				<!-- <div class="high30"></div> -->
			<?php endif;?>
		<?php endforeach;?>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 m-t80">
			<a href="<?php echo BASEURL.'jobs/active_job_section';?>"><button name="back" type="button" class="create org-box col-lg-3 col-md-3 col-xs-12 pull-left"><i class="fa fa-chevron-left"></i><span>Back to Job </span>  </button></a>
			<button name="submit" type="submit" value="submit" class="create start_job_btn org-box col-lg-3 col-md-3 col-xs-12 pull-left ab-btn"><span>Confirm Selection </span><i class="fa fa-chevron-right"></i></button>
		</div>
		<!-- </form> --->
		<div class="clearfix"></div>
	<?php else: $flag= false;?>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bmar">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 dark_organge org-boxs no-padding">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 dark_organge org-box  no-padding left-mar nob-mar job-box">
						<div class="col-lg-10 col-md-10 col-sm-10 col-xs-9 center-block no-float text-center no-candidate">
							<div class="over-scroll-left">
							<h2><?php echo "No Candidates Found."; ?></h2>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php endif;?>
</div>
<script>
	var flag = '<?php echo $flag ; ?>';
	if(flag == true){
		var applicants_obj  =  JSON.parse(decodeURIComponent("<?php echo rawurlencode(json_encode($hired_applicants)); ?>"));
		//var applicants_obj  = '<?php echo json_encode($hired_applicants); ?>';
		//applicants_obj = JSON.parse(applicants_obj);
		$.each(applicants_obj, function( index, value ) {
			if(value.cancel_status != 1){
				var id = "#rating_"+value.applicant_user_account_id ;
				var  rate = $(id).attr('rate');
				$(id).rating('update', rate);
			}
		});
	}
	$('.rating').rating('refresh', {
		disabled:true,
		showCaption: false
	});
	function setCandiDateId(id, type){
		if(type=='select'){
			$('#cand_id'+id).val(id);
			$('#select'+id).removeClass('green-box').addClass('gray-box');
			$('#select'+id+' span').text('Selected');
			$('.start_job_btn').show();
		}else{
			$('#cand_id'+id).val('');
			$('#select'+id).removeClass('gray-box').addClass('green-box');
			$('#select'+id+' span').text('Select');
		}
	}
	$( document ).ready(function() {
		$('input[type="checkbox"]').click(function(event) {
			this.checked = false; // reset first
			event.preventDefault();
		});
	});

</script>
<style>

</style>
