<?php $message = getGlobalMessage();?>
<style>
.job-btn {
    border: 0 none;
    color: #fff;
    cursor: pointer !important;
    font-family: "robotoregular";
    font-size: 15px;
    margin: 0;
    padding: 10px 0;
}
.btn.btn-Cancel {
    float: right;
    height: auto;width: 60px;
}
.btn.btn-ok{
     height: 36px; padding: 6px 14px;
}

.w-170 input {
    padding: 0 12px;

}
.datepicker{z-index: 9991;}
</style>

<div class="right_col">
    <?php
if ($message['type'] == 'success') {
    ?>
    <div class="Metronic-alerts alert alert-success fade in" id="prefix_167847684379"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button><?php echo $message['msg'] ?></div>
    <?php
} else if ($message['type'] == 'error') {
    ?>
    <div class="Metronic-alerts alert alert-danger fade in" id="prefix_167847684379"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button><?php echo $message['msg'] ?></div>
    <?php
}
?>
    <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <ul class="breadcrumb breadcrumb2">
        <li><a href="<?php echo base_url(); ?>"><?php echo $this->lang->line('home'); ?></a></li>
        <li class="active"><?php echo $this->lang->line('job_posting'); ?></li>
        <li class="active"><?php echo $job_details->job_title; ?> (<?php echo date('d M', strtotime($job_details->start_date)); ?> - <?php echo date('d M', strtotime($job_details->end_date)); ?>)</li>
        </ul>
    </div></div>
   <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 left-mar nob-mar job-box yellow-box pad-bottom top-mar">
          <div class="col-lg-10 col-md-11 col-sm-12 col-xs-12 center-block no-float">
            <div class="over-scroll-left">
                <h3 class="text-over"  title="<?php echo $job_details->job_title; ?>"><?php echo ucfirst($job_details->job_title); ?></h3>
                <p>
                    <?php if (LoginUserDetails('role_id') == '5') {
    ?>
                                <span><i class="fa fa-certificate"></i><font class="mar-left"><?php echo strlen((getOutletName($job_details->outlet_id))) > 20 ? substr(ucfirst(getOutletName($job_details->outlet_id)), 0, 20) . "..."
    : ucfirst(getOutletName($job_details->outlet_id)); ?></font></i></span>
                    <?php }?>
                    <span><i class="fa fa-certificate"></i><font class="mar-left">$<?php echo $job_details->payment_amount; ?>/<?php echo $this->lang->line('hour'); ?></font></span>
                    <span><i class="fa fa-certificate"></i><font class="mar-left"><?php echo date('d-M', strtotime($job_details->start_date)); ?> - <?php echo date('d-M', strtotime($job_details->end_date)); ?></font></span>
                    <span><i class="fa fa-certificate"></i><font class="mar-left"><?php echo getAppliedCandidate($job_details->job_id); ?> <?php echo " " . $this->lang->line('applicants'); ?></font></span>
                    <span><i class="fa fa-certificate"></i><font class="mar-left"><?php echo getHiredCandidate($job_details->job_id); ?> <?php echo $this->lang->line('selected'); ?></font></span>
                </p>
            </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <!--If Login user is area Manager and job is created by the same area manager then outlet Manager is not able to edit delete and copy the same job -->
  <?php if ((LoginUserDetails('role_id') == "5" && $job_details->created_by == LoginUserDetails("userid")) || ($job_details->job_created_by == 1 && manage_job_access($job_details->job_id) == true)) {?>

    <!--  <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 checked-box">
        <a href="<?php echo BASEURL . 'jobs/job_feedback/' . $job_details->job_id; ?>">
            <button class="col-lg-12 col-md-12 col-sm-12 col-xs-12 left-mar nob-mar job-box green-box top-mar" >
                <span>
                    <i class="fa fa-check-square-o"></i>
                    <div class="clearfix"></div>
                    <?php echo $this->lang->line('job_completed'); ?>
                </span>
            </button>
        </a>
    </div> -->
    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 checked-box">
        <a href=" <?php echo BASEURL . 'jobs/job_form/' . $job_details->job_id; ?>/1">
            <button class="col-lg-12 col-md-12 col-sm-12 col-xs-12 left-mar nob-mar job-box org-box top-mar">
                <span>
                    <i class="fa fa-files-o"></i>
                    <div class="clearfix"></div>
                    <?php echo $this->lang->line('copy_job'); ?>
                </span>
            </button>
        </a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 checked-box">
        <a href="javascript:;" onclick="delete_popup()"><button class="col-lg-12 col-md-12 col-sm-12 col-xs-12 left-mar nob-mar job-box red-box top-mar">
            <span>
                <i class="fa fa-ban"></i>
                <div class="clearfix"></div>
               <?php echo $this->lang->line('delete_job'); ?>
            </span>
        </button></a>
    </div>

    <!-- If Login user is outlet Manager and job is created by the same outlet manager -->
    <?php } elseif ((LoginUserDetails('role_id') == "3")) {?>
    <?php if (manage_job_access($job_details->job_id) == true) {?>
        <!-- <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 checked-box">
            <a href="<?php echo BASEURL . 'jobs/job_feedback/' . $job_details->job_id; ?>">
                <button class="col-lg-12 col-md-12 col-sm-12 col-xs-12 left-mar nob-mar job-box green-box top-mar" >
                    <span>
                        <i class="fa fa-check-square-o"></i>
                        <div class="clearfix"></div>
                        <?php echo $this->lang->line('job_completed'); ?>
                    </span>
                </button>
            </a>
        </div> -->
    <?php }?>
    <?php if (manage_job_access($job_details->job_id) == true) {?>
    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 checked-box">
        <a href=" <?php echo BASEURL . 'jobs/job_form/' . $job_details->job_id; ?>/1">
            <button class="col-lg-12 col-md-12 col-sm-12 col-xs-12 left-mar nob-mar job-box org-box top-mar">
                <span>
                    <i class="fa fa-files-o"></i>
                    <div class="clearfix"></div>
                    <?php echo $this->lang->line('copy_job'); ?>
                </span>
            </button>
        </a>
    </div>
    <?php }?>
    <?php if (manage_job_access($job_details->job_id) == true) {?>
    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 checked-box">
        <a href="javascript:;" onclick="delete_popup()"><button class="col-lg-12 col-md-12 col-sm-12 col-xs-12 left-mar nob-mar job-box red-box top-mar">
            <span>
                <i class="fa fa-ban"></i>
                <div class="clearfix"></div>
                <?php echo $this->lang->line('delete_job'); ?>
            </span>
        </button></a>
    </div>
    <?php }?>
    <?php }?>
    <div class="clearfix"></div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 job-des left-mar nob-mar job-box yellow-box pad-bottom">
          <div class="col-lg-12 col-md-12 col-sm-11 col-xs-12 center-block no-float">
            <div class="dess">
                <h3><?php echo $this->lang->line('job_description'); ?></h3>
                <p>
                    <span>
                    <font class="mar-left">
                        <?php echo $job_details->description; ?>
                    </font>
                    </span>
                </p>
            </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 left-mar nob-mar top-mar light-org pad-bottom">
         <div class="over-scroll-left">
                <h3><?php echo $this->lang->line('payment_method'); ?></h3>
                <div class="check-box">
                    <div class="check-wrapper">
                        <input type="checkbox" <?php if (set_value('payment_mode', isset($job_details->payment_mode) ? $job_details->payment_mode : '') == 1) {echo 'checked';}?> value="1" id="check1" name="payment_mode" data-parsley-multiple="payment_mode" data-parsley-id="0433" autocomplete="off">
                        <label for="check1"> </label>
                        <span class="check-text"><?php echo $this->lang->line('cash'); ?></span>
                    </div>
                    <ul class="parsley-errors-list" id="parsley-id-multiple-payment_mode"></ul>
                    <div class="check-wrapper">
                        <input type="checkbox" <?php if (set_value('payment_mode', isset($job_details->payment_mode) ? $job_details->payment_mode : '') == 2) {echo 'checked';}?> onclick="checkCheckBox(this.id)" value="2" id="check2" name="payment_mode" data-parsley-multiple="payment_mode" data-parsley-id="0433" autocomplete="off">
                        <label for="check2"> </label>
                        <span class="check-text"><?php echo $this->lang->line('bank_transfer'); ?></span>
                    </div>
                    <div class="check-wrapper">
                        <input type="checkbox" <?php if (set_value('payment_mode', isset($job_details->payment_mode) ? $job_details->payment_mode : '') == 3) {echo 'checked';}?> value="3" id="check3" name="payment_mode" data-parsley-multiple="payment_mode" data-parsley-id="0433" autocomplete="off">
                        <label for="check3"> </label>
                        <span class="check-text"><?php echo $this->lang->line('cheque'); ?></span>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 left-mar nob-mar top-mar org-box start pad-bottom">
           <div class="over-scroll-left">
                <h3><?php echo $this->lang->line('start'); ?></h3>
                <span>
                    <h1><?php echo date('d-m-y', strtotime($job_details->start_date)); ?></h1>
                    <?php echo date('h:i:A', strtotime($job_details->start_time)); ?>
                </span>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 left-mar nob-mar top-mar org-box start pad-bottom">
             <div class="over-scroll-left">
                <h3><?php echo $this->lang->line('end'); ?></h3>
                <span>
                    <h1><?php echo date('d-m-y', strtotime($job_details->end_date)); ?></h1>
                    <?php echo date('h:i:A', strtotime($job_details->end_time)); ?>
                </span>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="clearfix"></div>

    <?php if ($job_details->is_applicants_hired == 1) {
    ?>
    <?php if (!empty($applied_candidates)):
        foreach ($applied_candidates as $applicant):
            if ($applicant->job_status == '1'): ?>

                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 gray-back3 left-sec no-padding top-mar">
                                <div class="">
                                    <?php
        if ($applicant->acknowledged == '1') {
                $checkClockClockOut = $this->jobs_model->checkClockInOut($applicant->job_id, $applicant->applicant_user_account_id);
                if ($checkClockClockOut && $checkClockClockOut[0]->applicant_clockIn_status == 1) {?>
                                    <?php if ($checkClockClockOut[0]->applicant_clockOut_status == 1) {?>
                                         <a href="javascript:void(0);">
                                            <button class="job-btn block100 green-box col-xs-12 mar-bottom1" data-toggle="modal" data-target=".fade2"><i class="glyphicon glyphicon-time">  </i><?php echo $this->lang->line('clock_out_successfully'); ?></button>
                                         </a>
                                    <?php } else {?>
                                        <a href="javascript:void(0);" onclick="clockInOut('<?php echo $applicant->job_id; ?>','<?php echo $applicant->applicant_user_account_id; ?>','2')">
                                            <button class="job-btn block100 green-box col-xs-12 mar-bottom1" data-toggle="modal" data-target=".fade2"><i class="glyphicon glyphicon-time"></i> <?php echo $this->lang->line('clock_out'); ?></button>
                                        </a>
                                    <?php }?>
                                <?php } else {?>
                                    <?php if ($applicant->cancel_status != 1) {?>
                                        <?php if ($applicant->acknowledged == 1 && $applicant->ack_approval == 2 && $applicant->clockInOutStatus == 0) {?>
                                            <button class="job-btn block10 col-md-6 green-box col-xs-12" data-toggle="modal" data-target=".fade2"><i class="glyphicon glyphicon-flag"></i> Not ACK2</button>
                                        <?php } else {?>
                                            <a href="javascript:void(0);" onclick="clockInOut('<?php echo $applicant->job_id; ?>','<?php echo $applicant->applicant_user_account_id; ?>','1')">
                                                <button id="clockin_but" class="job-btn block10 col-md-6 green-box col-xs-12" data-toggle="modal" data-target=".fade2"><i class="glyphicon glyphicon-time"></i> <?php echo $this->lang->line('clock_in'); ?></button>
                                            </a>
                                        <?php }?>
                                            <button class="job-btn block10 col-md-6 red-box col-xs-12 mar-bottom1" data-toggle="" data-target="" onclick="reject_applicant('<?php echo $applicant->job_id; ?>','<?php echo $applicant->applicant_user_account_id; ?>','<?php echo ucwords($applicant->first_name . ' ' . $applicant->last_name); ?>')"><i class="glyphicon glyphicon-remove"></i> <?php echo $this->lang->line('cancel'); ?></button>

                                    <?php } else {?>
                                        <button class="job-btn block100 red-box col-xs-12 mar-bottom1"><i class="glyphicon glyphicon-flag"></i> <?php echo $this->lang->line('cancel'); ?></button>
                                    <?php }?>
                            <?php }} else {?>
                                        <?php if ($applicant->cancel_status != 1) {?>
                                        <a href="javascript:void(0);">
                                            <?php if ($applicant->acknowledged == 0 && $applicant->ack_approval == 1) {?>
                                                <button class="job-btn block10 col-md-6 green-box col-xs-12" data-toggle="modal" data-target=".fade2"><i class="glyphicon glyphicon-time"></i> Not ACK1</button>
                                                <?php } elseif ($applicant->acknowledged == 0 && $applicant->ack_approval == 2) {?>
                                        <button class="job-btn block10 col-md-6 green-box col-xs-12" data-toggle="modal" data-target=".fade2"><i class="glyphicon glyphicon-time"></i> Not ACK2</button>
                                        <?php }?>
                                        <button class="job-btn block10 col-md-6 red-box col-xs-12 mar-bottom1" data-toggle="" data-target="" onclick="reject_applicant('<?php echo $applicant->job_id; ?>','<?php echo $applicant->applicant_user_account_id; ?>','<?php echo ucwords($applicant->first_name . ' ' . $applicant->last_name); ?>')"><i class="glyphicon glyphicon-remove"></i> <?php echo $this->lang->line('cancel'); ?></button>
                                </a>
                                <?php } else {?>
                                    <button class="job-btn block100 red-box col-xs-12 mar-bottom1"><i class="glyphicon glyphicon-flag"></i> <?php echo $this->lang->line('cancel'); ?></button>
                                <?php }?>
                    <?php }?>
                        </div>
                                <span class="profile_img"><img style="width:203px; height:173px;" src="<?php echo checkImageFile(IMAGEURL . 'applicantes/' . $applicant->portrait) == true ? IMAGEURL . 'applicantes/' . $applicant->portrait : IMAGE . '/no_image.png'; ?>" /><span class="circle"><?php echo getTotalvoteCounts($applicant->applicant_user_account_id); ?></span></span>
                                <div class="clearfix"></div>
                                <span class="jod-details col-lg-9 col-md-9 col-sm-9 col-xs-12 center-block">
                                    <h4 title="<?php echo ucwords($applicant->first_name . ' ' . $applicant->last_name); ?>"><?php echo strlen($applicant->first_name . ' ' . $applicant->last_name) > 9 ? substr(ucwords($applicant->first_name . ' ' . $applicant->last_name), 0, 10) . ".." : ucwords($applicant->first_name . ' ' . $applicant->last_name); ?> <?php echo ($applicant->gender == 1) ? '(M)' : '(F)'; ?></h4>
                                    <?php
    $dob   = date_create($applicant->date_of_birth);
        $today = date_create(date('Y-m-d'));
        $age   = date_diff($dob, $today, true);?>
                                    <p><?php echo $age->y; ?> <?php echo $this->lang->line('years_old'); ?><br/>
                                        <?php echo getTotalvoteCounts($applicant->applicant_user_account_id); ?><?php echo $this->lang->line('JOD_completed'); ?>
                                    </p>
                                </span>
                                <div class="clearfix"></div>
                                <a href="<?php echo base_url('jobs/user_profile/' . $applicant->user_account_id); ?>"><button class="job-btn org-box col-lg-6 col-sm-12 col-md-6 col-xs-12"><i class="glyphicon glyphicon-user"></i><?php echo $this->lang->line('view_profile'); ?></button></a>
                                <!--- Acknowledged by applicant but in the 1st acknowledgment phase and not done clock in  and also not cancelled -->
                                <?php if ($applicant->acknowledged == 1 && $applicant->cancel_status == 0 && $applicant->ack_approval == 3 && $applicant->clockInOutStatus == 0) {?>
                                    <a ><button class="job-btn green-box org-box col-lg-6 col-sm-12 col-md-6 col-xs-12"><i class="glyphicon glyphicon-flag"></i>ACK1</button></a>
                                <!-- Acknowledged or not acknowledged but self cancelled  by applicant -->
                                <?php } elseif (($applicant->acknowledged == 0 || $applicant->acknowledged == 1) && $applicant->cancel_status == 1) {?>
                            <a ><button class="job-btn red-box org-box col-lg-6 col-sm-12 col-md-6 col-xs-12"><i class="glyphicon glyphicon-flag"></i>Cancel</button></a>
                        <!--- Not acknowledged and also in the 1st phase of the acknowledgment -->
                        <?php } elseif ($applicant->acknowledged == 0 && $applicant->cancel_status == 0 && $applicant->ack_approval == 1) {?>
                            <a ><button class="job-btn green-box org-box col-lg-6 col-sm-12 col-md-6 col-xs-12"><i class="glyphicon glyphicon-flag"></i>Not ACK 1</button></a>
                            <!--- Done acknowledgment or not but do not clock in and 2nd acknowledgment phase started  -->
                        <?php } elseif (($applicant->acknowledged == 1 || $applicant->acknowledged == 0) && $applicant->cancel_status == 0 && $applicant->ack_approval == 2 && $applicant->clockInOutStatus == 0) {?>
                            <a ><button class="job-btn green-box org-box col-lg-6 col-sm-12 col-md-6 col-xs-12"><i class="glyphicon glyphicon-flag"></i>Not ACK 2</button></a>
                            <!--- Done acknowledgment or not but do not clock in and 2nd acknowledgment phase started  -->
                        <?php } elseif ($applicant->acknowledged == 1 && $applicant->cancel_status == 0 && ($applicant->clockInOutStatus == 1 || $applicant->clockInOutStatus == 2)) {?>
                            <a ><button class="job-btn green-box org-box col-lg-6 col-sm-12 col-md-6 col-xs-12"><i class="glyphicon glyphicon-flag"></i>ACK </button></a>
                            <!-- Done 2nd acknowledgment but not clocked in yet and acknowlegment phase is 2nd -->
                        <?php } elseif ($applicant->acknowledged == 1 && $applicant->cancel_status == 0 && $applicant->clockInOutStatus == 0 && $applicant->ack_approval == 4) {?>
                            <a ><button class="job-btn green-box org-box col-lg-6 col-sm-12 col-md-6 col-xs-12"><i class="glyphicon glyphicon-flag"></i>ACK 2</button></a>
                        <?php }?>

                    </div>
                </div>
            <?php endif;?>
        <?php endforeach;?>
    <div class="clearfix"></div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 m-t80">
        <a href="<?php echo BASEURL . 'jobs/get_rejected_applicant/' . $job_details->job_id . '/2'; ?>"><button class="create view-pad org-box col-lg-12 col-md-12 col-xs-12" type="submit" name="save"><span><?php echo $this->lang->line('view_all_rejected_applicants'); ?></span><i class="fa fa-chevron-right"></i>  </button></a>
    </div>
    <?php else: ?>
    <div class="clearfix"></div>
        <!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 m-t80">
            <a href="<?php echo BASEURL . 'jobs/get_applied_applicant/' . $job_details->job_id . '/3'; ?>"><button class="create view-pad org-box col-lg-12 col-md-12 col-xs-12" type="submit" name="save"><span>View All Applied Applicants</span><i class="fa fa-chevron-right"></i>  </button></a>
        </div> -->
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 m-t80">
            <a href="<?php echo BASEURL . 'jobs/get_rejected_applicant/' . $job_details->job_id . '/2'; ?>"><button class="create view-pad org-box col-lg-12 col-md-12 col-xs-12" type="submit" name="save"><span><?php echo $this->lang->line('view_all_rejected_applicants'); ?></span><i class="fa fa-chevron-right"></i>  </button></a>
        </div>
    <?php endif;?>
<?php }?>

 <div class="clearfix"></div>

 <!-- modal pop up starts -->
<div class="modal fade bs-example-modal-md" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="clock_out_popup">

    <form class="form-horizontal form-label-left text-center" id="clock_out_popup_form" method="post">

        <div class="modal-dialog col-lg-7 center mode-w mode-w2 ">
            <button data-dismiss="modal" class="close-b"><i class="fa fa-times-circle"></i></button>
            <div class="modal-content back-white">
                <div class="col-md-12 col-xs-12 orange-back">
                    <h3><?php echo $this->lang->line('clock_out_u'); ?></h3>
                </div>
            <div class="contant col-md-12 text-center">
                <p class="col-md-12 col-sm-12 inblock popuptitle"><?php echo $this->lang->line('clock_out_popup_message'); ?></p>
                <div class="x_content">
                    <!-- Clock In date added By Kratika 24 Nov 2016 start -->
                    <div class="form-group disp-center">
                        <label class="control-label inblock clock-time  text-left"><?php echo $this->lang->line('clock_in_date'); ?></label>
                        <div class="inblock w-170">
                            <input type="text" name="clock_in_date" id="clock_in_date" value="" placeholder="Clock In Date" required readonly />
                        </div>
                    </div>
                    <!-- Clock In date added By Kratika 24 Nov 2016 end -->

                    <div class="form-group" >
                        <label class="control-label inblock clock-time  text-left"><?php echo $this->lang->line('clock_in_time'); ?></label>
                        <div class="select-box inblock">
                            <select class="select2_group form-control" id="clock_in_hour">

                            </select>
                        </div>
                        <div class="select-box inblock">
                            <select class="select2_group form-control" id="clock_in_min">
                            </select>
                        </div>
                    </div>
                    <!-- Clock Out date added By Kratika 24 Nov 2016 start -->
                    <div class="form-group disp-center input-append date" id="datepicker1">
                            <label class="control-label inblock clock-time  text-left"><?php echo $this->lang->line('clock_out_date'); ?></label>
                                <div class="inblock w-170">
                                    <input type="text" name="clock_out_date" id="clock_out_date" value="" placeholder="Clock Out Date"  required readonly />
                            </div>
                    </div>
                    <!-- Clock Out date added By Kratika 24 Nov 2016 end -->

                    <div class="form-group">
                        <label class="control-label inblock clock-time  text-left"><?php echo $this->lang->line('clock_out_time'); ?></label>
                        <div class="select-box inblock">
                            <select class="select2_group form-control" id="clock_out_hour">
                            </select>
                        </div>
                        <div class="select-box inblock">
                            <select class="select2_group form-control" id="clock_out_min">

                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label inblock clock-time  text-left"><?php echo $this->lang->line('job_start_time'); ?></label>
                        <div class="select-box inblock">
                            <select class="select2_group form-control drp" id="job_start_hour">
                            </select>
                        </div>
                        <div class="select-box inblock">
                            <select class="select2_group form-control drp" id="job_start_min">

                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label inblock clock-time  text-left"><?php echo $this->lang->line('job_end_time'); ?></label>
                        <div class="select-box inblock">
                            <select class="select2_group form-control drp" id="job_end_hour">
                            </select>
                        </div>
                        <div class="select-box inblock">
                            <select class="select2_group form-control drp" id="job_end_min">
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label inblock clock-time  text-left"><?php echo $this->lang->line('total_break_time'); ?></label>
                        <div class="select-box inblock">
                            <select class="select2_group form-control" id="meal_hour" name="meal_hour">

                            </select>
                        </div>
                        <div class="select-box inblock">
                            <select class="select2_group form-control" id="meal_min" name="meal_min">

                            </select>
                        </div>
                        <input type="hidden" name="job_id_input" value="" id="job_id" >
                        <input type="hidden" value="" name="user_account_id_input" id="user_account_id" >
                        <input type="hidden" value="0" name="is_feedback_given" id="is_feedback_given" >

                    </div>

                    <div class="form-group disp-center">
                        <label class="control-label inblock clock-time  text-left"><?php echo $this->lang->line('wages_per_hour'); ?></label>
                        <div class="inblock w-170">
                            <input type="text" name="wages_per_hour" id="wages_per_hour" <?php echo (isset($is_template_required) && $is_template_required == 1) ? "readonly" : "" ?> />
                        </div>

                    </div>
                    <!-- Added by Kratika on date 03 Jan 2017 -->
                    <div class="form-group disp-center">
                        <label class="control-label inblock clock-time  text-left"><?php echo $this->lang->line('rating'); ?></label>
                        <div class="inblock">
                            <input id="rating_star" class="rating" min="0" max="5" data-step="1" data-size="sm" data-symbol="&#xf005;" data-glyphicon="false" data-rating-class="rating-fa" >
                            <input type="hidden" value="0" id="rating_star_text" />
                        </div>
                    </div>

                    <div class="form-group disp-center">
                        <label class="control-label inblock clock-time  text-left"><?php echo $this->lang->line('comments'); ?></label>
                        <div class="inblock w-170">
                            <textarea id="comments" name="comments_text"></textarea>
                        </div>
                    </div>

            </div>
        </div>
        <div class="clerfix"></div>
        <div class="fotter">
            <button type="submit" class="col-md-12 col-xs-12 text-center green-box">
                <?php echo $this->lang->line('click_here_to_continue'); ?>
            </button>
        </div>
        <div class="clerfix"></div>
        </div>
    </div>
    </form>
</div>


<!-- modal popup ends -->
<script>
function delete_popup(){
    bootbox.alert("Active jobs can not be deleted/cancel.", function() {
      //Example.show("Hello world callback");
    });
}
$( document ).ready(function() {
    var nowTemp = new Date();
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

    var back_2days_from_now = now.setDate(now.getDate() - 2);
   // alert(back_2days_from_now);
    $('input[type="checkbox"]').click(function(event) {
        this.checked = false; // reset first
        event.preventDefault();
    });

    /* Added By kratika at 24 Nov 2016 start */
    $('#clock_out_date').datepicker({
        format: 'yyyy-mm-dd',
        pickTime: false,
        minDate : now,
        onRender: function(date) {
            return date.valueOf() < back_2days_from_now.valueOf() ? 'disabled' : '';
          }
    }).on('changeDate', function(ev) {
        var clock_in_date = $("#clock_in_date").val();
        var clock_out_date = $("#clock_out_date").val();
        var sDate = new Date(clock_in_date);
        var eDate = new Date(clock_out_date);
        if(clock_in_date != '' && clock_out_date != '' && sDate > eDate) {
            alert("Please ensure that the Clock out Date is greater than or equal to the Clock In Date.");
            $( "#clock_out_date" ).val('');
        }
             $(this).datepicker('hide');
    });

    $('#clock_in_date').datepicker({
        format: 'yyyy-mm-dd',
        pickTime: false,
        minDate : now,
         onRender: function(date) {
            return date.valueOf() < back_2days_from_now.valueOf() ? 'disabled' : '';
          }
    }).on('changeDate', function(ev) {
        $(this).datepicker('hide');
    });

    /* Added By kratika at 24 Nov 2016 end */


});

function reject_applicant(job_id,user_account_id,applicant_name){
        bootbox.dialog({
          message: " Are you sure you want to cancel "+applicant_name+" for this job?",
          title: "Cancel "+applicant_name,
          onEscape: function() {},
          show: true,
          backdrop: true,
          closeButton: true,
          animate: true,
          className: "reject_applicant_popup",
          buttons: {
            Ok: {
              label: "Ok",
              className: "btn-ok",
              callback: function() { reject_Status(job_id,user_account_id);  }
            },
            Cancel: {
              label: "Cancel",
              className: "btn-Cancel",
              callback: function() {}
            },
          }
        });
}
    function reject_Status(job_id,user_account_id){
        $.ajax({
            type : "POST",
            async: false,
            url : "<?php echo base_url(); ?>jobs/reject_applicant",
            data : {
                job_id : job_id,
                applicant_user_account_id : user_account_id //save.php converts this to a safe title
            },
            error : function(jqXHR, textStatus, errorThrown) {
                alert("An error occured in the ajax");
            },
            success : function(data, textStatus, jqXHR) {
                if(data == 1){
                    var url = "<?php echo base_url(); ?>jobs/get_rejected_applicant/"+job_id;
                    location.href= url;
                }
            }
        });
    }



function clockInOut(job_id,applicant_user_account_id,type){
        //Create in time
        //alert(job_id+' - '+applicant_user_account_id);
        if(type=='1'){
            $("#clockin_but").attr('disabled',true);
         //   alert("aaa");
            title="Clock In Date Time";
            var url = "<?php echo base_url(); ?>jobs/clockInCurrentTime";
            $.ajax({
                type : "POST",
                async: false,
                url :  url,
                data : {'job_id': job_id,'applicant_user_account_id': applicant_user_account_id},
                error : function(jqXHR, textStatus, errorThrown) {
                    alert("An error occured in the ajax");
                },
                success : function(data, textStatus, jqXHR) {
                    if(data != ''){
                        bootbox.dialog({
                          message: "<img  src=<?php echo base_url(); ?>qr_images/"+data+">",
                          title: title,
                          onEscape: function() {},
                          show: true,
                          backdrop: true,
                          closeButton: true,
                          animate: true,
                          className: "my-modal",
                          buttons: {
                            Ok: {
                              label: "Ok",
                              className: "btn-ok",
                              callback: function() { location.reload(); }
                            },
                            Cancel: {
                              label: "Cancel",
                              className: "btn-Cancel",
                              callback: function() {}
                            },
                          }
                        });
                    }
                }
            });
            $("#clockin_but").attr('disabled',false);
        }else{
            $.ajax({
                type : "POST",
                async: false,
                url : "<?php echo base_url(); ?>jobs/get_clockin_clokcout_detail",
                data : {
                    job_id : job_id,
                    applicant_user_account_id : applicant_user_account_id   //save.php converts this to a safe title
                },
                error : function(jqXHR, textStatus, errorThrown) {
                    alert("An error occured in the ajax");
                },
                success : function(data, textStatus, jqXHR) {
                     $(".datepicker dropdown-menu").css("z-index", "9999");
                //  alert(data.toSource());
                    var data = JSON.parse(data);
                    $('#clock_in_hour').empty();
                    for(i=00; i<=23; i++){
                        $('#clock_in_hour').append(
                        $("<option></option>")
                          .attr("value", n(i))
                          .text(n(i))
                        );
                    }
                    $("#clock_in_hour").val(data.clock_in_hour);
                    /* added By Kratika 24 Nov 2016 start */
                    $('#clock_in_date').val('');
                    $('#clock_out_date').val('');
                    $('#clock_in_date').val(data.clock_in_date);
                    $('#clock_out_date').val(data.clock_out_date);
                    /* added By Kratika 24 Nov 2016 end */
                    $('#clock_in_min').empty();
                    for(i=00; i<=60; i++){
                        $('#clock_in_min').append(
                        $("<option></option>")
                          .attr("value", n(i))
                          .text(n(i))
                        );
                    }
                    $("#clock_in_min").val(data.clock_in_min);
                    $('#clock_out_hour').empty();
                    for(i=0; i<=23; i++){
                        $('#clock_out_hour').append(
                        $("<option></option>")
                          .attr("value", n(i))
                          .text(n(i))
                        );
                    }
                    $("#clock_out_hour").val(data.clock_out_hour);

                    $('#clock_out_min').empty();
                    for(i=0; i<=60; i++){
                        $('#clock_out_min').append(
                        $("<option></option>")
                          .attr("value", n(i))
                          .text(n(i))
                        );
                    }
                    $("#clock_out_min").val(data.clock_out_min);

                    $('#job_start_hour').empty();
                    $('#job_start_hour').append(
                        $("<option></option>")
                          .attr("value", data.job_start_hour)
                          .text(data.job_start_hour)
                    );

                    $('#job_start_min').empty();
                    $('#job_start_min').append(
                        $("<option></option>")
                          .attr("value", data.job_start_min)
                          .text(data.job_start_min)
                    );

                    $('#job_end_hour').empty();
                    $('#job_end_hour').append(
                        $("<option></option>")
                          .attr("value", data.job_end_hour)
                          .text(data.job_end_hour)
                    );

                    $('#job_end_min').empty();
                    $('#job_end_min').append(
                        $("<option></option>")
                          .attr("value", data.job_end_min)
                          .text(data.job_end_min)
                    );

                    $('#meal_hour').empty();
                    for(i=0; i<=4; i++){
                        $('#meal_hour').append(
                        $("<option></option>")
                          .attr("value", n(i))
                          .text(n(i))
                        );
                    }

                    $('#meal_min').empty();
                    for(i=0; i<=3; i++){
                        $('#meal_min').append(
                        $("<option></option>")
                          .attr("value", i*15)
                          .text(n(i*15))
                        );
                    }

                    $('#job_id').val('');
                    $('#job_id').val(data.job_id);
                    $('#user_account_id').val('');
                    $('#user_account_id').val(data.applicant_id);

                    $('#wages_per_hour').val('');
                    $('#wages_per_hour').val(data.wages_per_hour);
                    $('.drp').prop('disabled', true);

                    if(data.is_feedback_data == true){
                         $('#is_feedback_given').val(1);
                        $("#comments").val(data.feedback_data.additional_comments);
                        $("#comments").attr('disabled', true);
                        $('#rating_star').rating('update', data.feedback_data.rating);
                        $('#rating_star_text').val(data.feedback_data.rating);
                        $('.rating').rating('refresh', {
                            disabled:true,
                            showCaption: false
                        });
                    }
                    $("#clock_out_popup").modal("show");
                }
            });
        }
}

function n(n){
    return n > 9 ? "" + n: "0" + n;
}

$("#clock_out_popup_form").submit(function(event) {
     event.preventDefault();
        var clock_in_hour = $('#clock_in_hour').val();
        var clock_in_min = $('#clock_in_min').val();
        var clock_out_hour = $('#clock_out_hour').val();
        var clock_out_min = $('#clock_out_min').val();
        var clock_in_date = $('#clock_in_date').val();
        var clock_out_date = $('#clock_out_date').val();
        var wages_per_hour = $('#wages_per_hour').val();
        var meal_min = $('#meal_min').val();
        var meal_hour = $('#meal_hour').val();
        var job_id = $('#job_id').val();
        var rating_star = $('#rating_star_text').val();

        var comments = $('#comments').val();
        var is_feedback_given = $('#is_feedback_given').val();
        var applicant_user_account_id = $('#user_account_id').val();
        if($.trim(clock_in_date)!="" && $.trim(clock_out_date)!= ""){
            if($.trim(rating_star) != 0 ){ // Rating should not be zero
                var clockin_date_time   = clock_in_date+" "+clock_in_hour+":"+clock_in_min+":"+"00";
                var clockout_date_time = clock_out_date+" "+clock_out_hour+":"+clock_out_min+":"+"00";
                clockout_date_time = clockout_date_time.replace(/-/g, "/");
                clockin_date_time  = clockin_date_time.replace(/-/g, "/");
                var cInDateTime = new Date(clockin_date_time);
                var cOutDateTime = new Date(clockout_date_time);
                if (cInDateTime < cOutDateTime){
                    var time_data = check_time_difference(clockout_date_time,clockin_date_time);
                    var time_data_obj = JSON.parse(time_data);
                    if(meal_hour > 0 &&  parseInt(meal_hour) > parseInt(time_data_obj.hours)  &&  time_data_obj.days == 0 ){
                        alert("<?php echo $this->lang->line('meal_break_time_msg'); ?>");
                    }else{
                        var ajax_post_data  = {};
                        ajax_post_data.job_id                       = job_id;
                        ajax_post_data.applicant_user_account_id    = applicant_user_account_id;
                        ajax_post_data.clock_in_hour                = clock_in_hour;
                        ajax_post_data.clock_in_min                 = clock_in_min;
                        ajax_post_data.clock_out_hour               = clock_out_hour;
                        ajax_post_data.clock_out_min                = clock_out_min;
                        ajax_post_data.clock_out_date               = clock_out_date;
                        ajax_post_data.clock_in_date                = clock_in_date;
                        ajax_post_data.meal_min                     = meal_min;
                        ajax_post_data.meal_hour                    = meal_hour;
                        ajax_post_data.wages_per_hour               = wages_per_hour;
                        ajax_post_data.rating_star                  = rating_star;
                        ajax_post_data.feedback_comments            = comments;
                        if(time_data_obj.hours > 18 || time_data_obj.days > 0){
                         bootbox.dialog({
                              message: "<?php echo $this->lang->line('clock_in_clock_out_period_diff_msg'); ?>",
                              title: "",
                              onEscape: function() {},
                              show: true,
                              backdrop: true,
                              closeButton: true,
                              animate: true,
                              className: "my-modal_qr_code",
                              buttons: {
                                Ok: {
                                  label: "Yes",
                                  className: "btn-ok",
                                  callback: function() { generate_clock_out_qrcode(ajax_post_data);
                                      }
                                },
                                Cancel: {
                                  label: "No",
                                  className: "btn-Cancel",
                                  callback: function() {}
                                },
                              }
                            });
                        }else{
                            generate_clock_out_qrcode(ajax_post_data);
                        }
                    }

                }else{
                    alert("<?php echo $this->lang->line('clock_out_grt_then_clock_in'); ?>");
                }
            }else{
                // comment field check else condition
                    alert("<?php echo $this->lang->line('rating_required'); ?>");

            }
        }else{
            alert("<?php echo $this->lang->line('clock_in_clock_out_req'); ?>");
        }
    });





/* Added By Kratika on date 24 nov 2016 start*/

function generate_clock_out_qrcode(post_data){
    $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>jobs/update_meal_hours",
        data: {'user_account_id_input': post_data.applicant_user_account_id, 'job_id_input': post_data.job_id, 'meal_hour': post_data.meal_hour, 'meal_min':  post_data.meal_min , 'wages_per_hour' : post_data.wages_per_hour},
        success: function( data, textStatus, jQxhr ){
            $('#loader').fadeOut();
            if(data == "1"){
                $("#clock_out_popup").modal("hide");
                var title="Clock Out Date Time";
                var url = "<?php echo base_url(); ?>jobs/clockInCurrentTime";
                $.post(url,
                {  'job_id': post_data.job_id,'applicant_user_account_id': post_data.applicant_user_account_id , 'clock_in_hour': post_data.clock_in_hour, 'clock_in_min': post_data.clock_in_min, 'clock_out_hour' : post_data.clock_out_hour, 'clock_out_min' : post_data.clock_out_min, 'clock_out_date': post_data.clock_out_date, 'clock_in_date':post_data.clock_in_date, 'feedback_comments': post_data.feedback_comments, 'rating': post_data.rating_star},
                function(data){
                    //alert(data);
                        var data_obj = JSON.parse(data);
                if(data_obj.error != true ){
                    bootbox.dialog({
                              message: "<img class='qr_img' src=<?php echo base_url(); ?>qr_images/"+data_obj.image_path+">",
                              title: title,
                              onEscape: function() {},
                              show: true,
                              backdrop: true,
                              closeButton: true,
                              animate: true,
                              className: "my-modal_qr_code",
                              buttons: {
                                Ok: {
                                  label: "Ok",
                                  className: "btn-ok",
                                  callback: function() { location.reload(); }
                                },
                                Cancel: {
                                  label: "Cancel",
                                  className: "btn-Cancel",
                                  callback: function() {}
                                },
                              }
                            });
                        }else{
                            alert("<?php echo $this->lang->line('required_clock_inout'); ?>");
                        }
                    }
                );
            }
        },
        error: function( jqXhr, textStatus, errorThrown ){
            $('#loader').fadeOut();
            alert( errorThrown );
        }
    });
}




function check_time_difference(clock_out_date_time,clock_in_date_time){
    var res_data = '';
    $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>jobs/checkDateTimeDifference",
            data: {"clock_out_date_time": clock_out_date_time, "clock_in_date_time": clock_in_date_time},
            async: false,
            success: function( data, textStatus, jQxhr ){
                res_data = data;
            },
            error: function( jqXhr, textStatus, errorThrown ){
                alert( errorThrown );
            }
        });
    return res_data;
}
/* Added By Kratika on date 24 nov 2016 end*/

/* Added by kratika on date 03 Jan 2017 Start */

    $('.rating').rating('refresh', {
            disabled:false,
            showCaption: false,
        });

$('.rating').on('rating.change', function(event, value, caption, target) {

        $("#rating_star_text").val(value);
 });
</script>




