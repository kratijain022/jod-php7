<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$message = getGlobalMessage();
$userType =LoginUserDetails('role_id');
if($userType == 3) {   // outlet Manager
$user_account_id  = $userType=LoginUserDetails('userid');
$OManager = getOMManagerDetails($user_account_id);
$outlet_id = $OManager[0]->outlet_id;
$outlet_details	= getOutletDetail($outlet_id);
$template_req = $outlet_details->is_template_required;
}else{

	$template_req = 0;
}

?>

<div class="right_col right_col" role="main">
  <?php
	if($message['type']=='success') {
	?>
  <div class="Metronic-alerts alert alert-success fade in" id="prefix_167847684379">
    <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
    <?php echo $message['msg'] ?></div>
  <?php
	} else if($message['type']=='error') {
	?>
  <div class="Metronic-alerts alert alert-danger fade in" id="prefix_167847684379">
    <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
    <?php echo $message['msg'] ?></div>
  <?php
	}
	?>
  <div class="row padding-top-one">
	  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<ul class="breadcrumb breadcrumb2">
			<li><a href="<?php echo base_url(); ?>"><?php echo $this->lang->line('home'); ?></a></li>
			<?php if(!empty($isCopy) && !empty($id)) { ?>
				<li class="active"> <?php echo $this->lang->line('copy_job'); ?> </li>
			<?php }else if($id) { ?>
				<li class="active"> <?php echo $this->lang->line('update_job'); ?> </li>
			<?php }else{ ?>
				<li class="active"> <?php echo $this->lang->line('create_new_job'); ?> </li>
			<?php } ?>
			</ul>
		</div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center title-head2">
      <h3>
        <?php
				if(!empty($isCopy) && !empty($id)){
					echo $this->lang->line('copy_job');
					$action = $this->lang->line('copy_job_u');
				}
				else if($id) {
					echo $this->lang->line('edit_job');
					$action =  $this->lang->line('update_job');
				} else {
					echo $this->lang->line('create_new_job');
					$action =  $this->lang->line('create_job');
				}
			?>
      </h3>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <?php
        if(!empty($isCopy)){
			$base_url = base_url('jobs/job_form/');
        }else{
			$base_url = base_url('jobs/job_form/'.$id);
		}
        ?>
        <form action="<?php echo $base_url; ?>" id="form_sample_3" class="form-horizontal" enctype="multipart/form-data" method='post' data-parsley-validate onsubmit="return validation()">
          <div class="org-box pad-bottoms no-pa-from">
            <h4 class="text-center basic-details"> <?php echo $this->lang->line('job_details'); ?> </h4>
            <div class="form-group">
              <div class="col-lg-1 col-md-1 col-sm-1 hidden-xs">
                <label for="usr"></label>
              </div>
              <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                <select  class="form-control chosen-select" name="job_template_id" id="job_template_id" data-parsley-errors-container="#job_template_id-error">
                  <option value="" >-- <?php echo $this->lang->line('select_job_template'); ?> --</option>
                  <?php foreach($jobTemplatesList as $jobTemplates) { ?>
                  <option value="<?php echo $jobTemplates->template_id; ?>"><?php echo $jobTemplates->template_name;?></option>
                  <?php } ?>
                </select>
                <span id="job_template_id-error" class="help-block help-block-error"><?php echo form_error('job_template_id'); ?></span> </div>
            </div>
            <div class="form-group">
              <div class="col-lg-1 col-md-1 col-sm-1 hidden-xs">
                <label for="usr1"></label>
              </div>
              <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                <input type="text" placeholder="<?php echo $this->lang->line('job_title'); ?>" class="form-control" id="job_title" name="job_title" value="<?php echo set_value('job_title', isset($records->job_title) ? $records->job_title : ''); ?>" required data-parsley-maxlength="100" >
                <span id="job_title-error" class="help-block help-block-error"><?php echo form_error('job_title'); ?></span> </div>
            </div>
            <div class="form-group">
              <div class="col-lg-1 col-md-1 col-sm-1 hidden-xs">
                <label for="usr"></label>
              </div>
              <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12" id="job_type_div"  <?php echo ($template_req  == 1) ? "disable='true'" : "" ;?>>
                <select  class="form-control chosen-select" name="job_type_id" id="job_type_id" required data-parsley-errors-container="#job_type_id-error">
                  <option value="" >--<?php echo $this->lang->line('select_job_type'); ?>--</option>
                  <?php foreach($jobTypes as $jobType) { ?>
                  <option value="<?php echo $jobType->job_role_id; ?>" <?php if(set_value('job_type_id', isset($records->job_type_id) ? $records->job_type_id : '') == $jobType->job_role_id){ echo 'selected="selected"'; } ?> ><?php echo $jobType->name;?></option>
                  <?php } ?>
                </select>
                <span id="job_type_id-error" class="help-block help-block-error"><?php echo form_error('job_type_id'); ?></span> </div>
            </div>

            <div class="clearfix"></div>
            <?php if(LoginUserDetails('role_id') == 5) { ?> <!-- If area manager creates the job, then he have to select the outlet manager -->
             <div class="form-group">
              <div class="col-lg-1 col-md-1 col-sm-1 hidden-xs">
                <label for="usr"></label>
              </div>
              <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                <select class="form-control chosen-select" name="outlet_id" id="outlet_id" required data-parsley-errors-container="#outlet_id-error">
                  <option value="" >--<?php echo $this->lang->line('select_outlet'); ?>--</option>
                  <?php foreach($outlets as $outlet) { ?>
                  <option value="<?php echo $outlet->outlet_id; ?>" <?php if(set_value('outlet', isset($records->outlet_id) ? $records->outlet_id : '') == $outlet->outlet_id){ echo 'selected="selected"'; } ?> ><?php echo $outlet->outlet_name;?></option>
                  <?php } ?>
                </select>
                <span id="outlet_id-error" class="help-block help-block-error"><?php echo form_error('outlet_id'); ?></span> </div>
            </div>
            <div class="clearfix"></div>
             <div class="form-group">
              <div class="col-lg-1 col-md-1 col-sm-1 hidden-xs">
                <label for="usr"></label>
              </div>
              <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">

                <select class="form-control chosen-select" name="outlet_manager_id" id="outlet_manager_id" >
					<option value="" >--<?php echo $this->lang->line('select_outlet_manager'); ?>--</option>
                </select>
                <span id="outlet_manager_id-error" class="help-block help-block-error"><?php echo form_error('outlet_manager_id'); ?></span> </div>
            </div>
            <div class="clearfix"></div>
            <?php } ?>
            <div class="form-group">
              <div class="col-lg-1 col-md-1 col-sm-1 hidden-xs">
                <label for="usr1"></label>
              </div>
              <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                <textarea type="text" rows="5" placeholder="<?php echo $this->lang->line('job_description'); ?>" class="form-control text-area" id="description" name="description" required><?php echo set_value('description', isset($records->description) ? $records->description : ''); ?></textarea>
                <span id="description-error" class="help-block help-block-error"><?php echo form_error('description'); ?></span> </div>
            </div>
            <div class="clearfix"></div>
            <div class="form-group">
              <div class="col-lg-1 col-md-1 col-sm-1 hidden-xs">
                <label for="usr1"></label>
              </div>
              <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                <textarea type="text" rows="5" placeholder="<?php echo $this->lang->line('special_instuctions'); ?>" class="form-control text-area" id="special_instructions" name="special_instructions" required><?php echo set_value('special_instructions', isset($records->special_instructions) ? $records->special_instructions : ''); ?></textarea>
                <span id="special_instructions-error" class="help-block help-block-error"><?php echo form_error('special_instructions'); ?></span> </div>
            </div>
            <div class="clearfix"></div>
            <div class="form-group">
				<div class="col-lg-1 col-md-1 col-sm-1 hidden-xs">
					<label for="usr1"></label>
				  </div>
				<div class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
					<div class="form-group input-append date" id="datepicker1">
						<input type="text"  placeholder="<?php echo $this->lang->line('start_date'); ?>" class="form-control" id="start_date" name="start_date" value="<?php echo set_value('start_date', isset($records->start_date) ? $records->start_date : ''); ?>" data-parsley-errors-container="#start_date-error" style="width:100%" readonly required>
						<span class="add-on" id="start_date_add_on" > <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="fa fa-calendar"></i> </span>
						<span id="start_date-error" class="help-block help-block-error"><?php echo form_error('start_date'); ?></span>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
					<div class="form-group input-append time-pick date" id="timepicker1">
						<input type="text"  data-format="hh:mm:ss"  placeholder="<?php echo $this->lang->line('start_time'); ?>" class="form-control" id="start_time" name="start_time" value="<?php echo set_value('start_time', isset($records->start_time) ? $records->start_time : ''); ?>" data-parsley-errors-container="#start_time-error" style="width:100%" readonly	 required>
						<span class="add-on" id="start_time_add_on"> <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="fa fa-clock-o"></i> </span>
						<span id="start_time-error" class="help-block help-block-error"><?php echo form_error('start_time'); ?></span>
					</div>
                </div>
			</div>
			<div class="clearfix"></div>
            <div class="form-group">
				<div class="col-lg-1 col-md-1 col-sm-1 hidden-xs">
					<label for="usr1"></label>
				  </div>
				<div class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
					<div class="form-group input-append date" id="datepicker2">
						<input type="text"  placeholder="<?php echo $this->lang->line('end_date'); ?>" class="form-control" id="end_date" name="end_date" value="<?php echo set_value('end_date', isset($records->end_date) ? $records->end_date : ''); ?>" data-parsley-errors-container="#end_date-error" style="width:100%" readonly required>
						<span class="add-on" id="end_date_add_on"> <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="fa fa-calendar"></i> </span>
						<span id="end_date-error" class="help-block help-block-error"><?php echo form_error('end_date'); ?></span>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
					<div class="form-group input-append  time-pick date" id="timepicker2">
						<input type="text"  data-format="hh:mm:ss"  placeholder="<?php echo $this->lang->line('end_time'); ?>" class="form-control" id="end_time" name="end_time" value="<?php echo set_value('end_time', isset($records->end_time) ? $records->end_time : ''); ?>" data-parsley-errors-container="#end_time-error" style="width:100%" readonly	 required>
						<span class="add-on" id="end_time_add_on"> <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="fa fa-clock-o"></i> </span>
						<span id="end_time-error" class="help-block help-block-error"><?php echo form_error('end_time'); ?></span>
					</div>
                </div>
			</div>
            <div class="clearfix"></div>
            <div class="form-group">
              <div class="col-lg-1 col-md-1 col-sm-1 hidden-xs">
                <label for="usr1"></label>
              </div>
              <div class="cash-bank">
              <div class="col-lg-6 col-md-6 col-sm-5 col-xs-12">
                <input type="text" placeholder="<?php echo $this->lang->line('earning_per_hour'); ?>" class="form-control"  id="payment_amount" name="payment_amount" value="<?php echo set_value('payment_amount', isset($records->payment_amount) ? $records->payment_amount : ''); ?>" data-parsley-errors-container="#payment_amount-error" required  data-parsley-type="number" data-parsley-pattern-message="Please enter valid number" data-parsley-pattern="^\+?[0-9]*\.?[0-9]+$">
                <span id="payment_amount-error" class="help-block help-block-error"><?php echo form_error('payment_amount'); ?></span> </div>


              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="check-box">
                  <!-- <div class="check-wrapper">
                    <input type="checkbox" class="payment_mode set_check_readonly" name="payment_mode" id="check1" value="1" onclick="checkCheckBox(this.id)" <?php if(set_value('payment_mode', isset($records->payment_mode) ? $records->payment_mode : '') == 1){ echo 'checked'; } ?> data-parsley-errors-container="#payment_mode-error" data-parsley-mincheck="1" required>
                    <label for="check1"> </label>
                    <span class="check-text"><?php echo $this->lang->line('cash'); ?></span>
                   </div> -->
                      <div class="check-wrapper">
                  <input type="checkbox" class="payment_mode set_check_readonly"  name="payment_mode" id="check2" value="2" onclick="checkCheckBox(this.id)" <?php if(set_value('payment_mode', isset($records->payment_mode) ? $records->payment_mode : '') == 2){ echo 'checked'; }else{ echo "checked"; }  ?>  >
                  <label for="check2"> </label>
                  <span class="check-text"><?php echo $this->lang->line('bank_transfer'); ?></span></div>
                    <!-- <div class="check-wrapper">
                  <input type="checkbox" class="payment_mode set_check_readonly" name="payment_mode" id="check3" value="3" onclick="checkCheckBox(this.id)" <?php if(set_value('payment_mode', isset($records->payment_mode) ? $records->payment_mode : '') == 3){ echo 'checked'; } ?>  >
                  <label for="check3"> </label>
                  <span class="check-text"><?php echo $this->lang->line('cheque'); ?></span> </div> -->
                  </div><br><br>
                <span id="payment_mode-error" class="help-block help-block-error"><?php echo form_error('payment_mode'); ?></span> </div></div>

            </div>
            <div class="clearfix"></div>
            <div class="form-group">
			<div class="col-lg-1 col-md-1 col-sm-1 col-xs-12"></div>
			<div class="cash-bank">
			<div class="col-lg-6 col-md-6 col-sm-5 col-xs-12">
				<label class="nea"><?php echo $this->lang->line('nea_certificate_req'); ?></label>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="check-box">
					<div class="check-wrapper">
						<input type="checkbox"class="NEA_certification set_check_readonly" name="required_NEA_certification" id="check4" value="1"   <?php if(set_value('required_NEA_certificate', isset($records->required_NEA_certificate) ? $records->required_NEA_certificate	 : '') == 1){ echo 'checked'; } ?>   >
						<label for="check4"> </label>
						<span class="check-text"><?php echo $this->lang->line('yes'); ?></span>
					</div>
					<div class="check-wrapper">
						<input type="checkbox" class="NEA_certification set_check_readonly" name="required_NEA_certification" id="check5" value="2"  <?php if(set_value('required_NEA_certificate', isset($records->required_NEA_certificate) ? $records->required_NEA_certificate : '') == 2){ echo 'checked'; } ?>  >
						<label for="check5"> </label>
						<span class="check-text"><?php echo $this->lang->line('no'); ?></span></div>
						<span id="required_NEA_certification-error" class="help-block help-block-error"><?php echo form_error('required_NEA_certification'); ?></span>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			</div>
			</div>
          <div class="clearfix"></div>
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 m-t80 no-padding">
            <button class="create org-box col-lg-3 col-md-3 col-xs-12 pull-right" type="submit" name="save"><span><?php echo $action ?></span> <i class="fa fa-chevron-right"></i> </button>
          </div>
        </form>
        <div class="clearfix"></div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
<?php if(isset($site_language) && $site_language ==  "chinese"){?>
  window.Parsley.setLocale('zh-cn');
<?php }else{ ?>
  window.Parsley.setLocale('en');
<?php }?>
$(".chosen-select").chosen();
var config = {
	'.chosen-select'           : {},
	'.chosen-select-deselect'  : {allow_single_deselect:true},
	'.chosen-select-no-single' : {disable_search_threshold:10},
	'.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
	'.chosen-select-width'     : {width:"95%"}
}
for (var selector in config) {
	$(".chosen-select").chosen(config[selector]);
}
$('.collapse').on('shown.bs.collapse', function(){
	$(this).parent().find(".glyphicon-plus").removeClass("glyphicon-plus").addClass("glyphicon-minus");
}).on('hidden.bs.collapse', function(){
	$(this).parent().find(".glyphicon-minus").removeClass("glyphicon-minus").addClass("glyphicon-plus");
});

$(".payment_mode").click( function() {
    var n = $(this).attr('name');
    var id = $(this).attr('id');
    $("[name=" + n + ']').prop("checked", false);
    $(this).prop("checked", true);
});

function checkUsername() {
	var emailId=$("#email_id").val();
	var emailId = emailId.replace(' ','');
	if(emailId != '') {
			$.post(BASEURL+"admin/get_unique_id/",{emailId:emailId},function(data){
				if(data >= 1){
				$("#email_id-error").html("Email Id Already in Use");
					$('#email_id').val("");
				}
				else{
				$( "#email_id-error" ).empty();
				}
			});
	}
}


$(".NEA_certification").click( function() {
    var n = $(this).attr('name');
    var id = $(this).attr('id');
    $("[name=" + n + ']').prop("checked", false);
    $(this).prop("checked", true);
});

function checkCheckBox(checkbox){
	$(checkbox).prop('checked', true);
	for(var i=1;i<=3;i++){
		if(checkbox != 'check'+i) {
			$('#check'+i).attr('checked', false);
		}
	}

}

//~ function validation() {
	//~ var startDate = document.getElementById("start_date").value;
    //~ var endDate = document.getElementById("end_date").value;
    //~ if ((Date.parse(startDate) >= Date.parse(endDate))) {
        //~ alert("End date should be greater than Start date");
        //~ document.getElementById("end_date").value = "";
        //~ return false;
    //~ }else{
		//~ $('#loader').fadeIn();
	//~ }
//~ }

function get_area_manager(outletManagerId) {
	var outletId = $('#outlet_id').val();
	if(outletId != '') {
			$.post(BASEURL+"jobs/get_outlet_manager/",{outletId:outletId,outletManagerId:outletManagerId},function(data){
				if(data.trim() != '') {
					$("#outlet_manager_id").empty();
					$("#outlet_manager_id").append(data);
					$("#outlet_manager_id").trigger("chosen:updated");
				} else {
					//alert('There is no outlet managers available.');
					//$('#outlet_id').val("");
					//$("#outlet_id").trigger("chosen:updated");
					$("#outlet_manager_id").empty();
					$("#outlet_manager_id").trigger("chosen:updated");
				}
			});
	} else {
		$("#outlet_manager_id").empty();
		$("#outlet_manager_id").trigger("chosen:updated");
	}
}
$('#outlet_id').change(function () 	{
	get_area_manager(0)
});

function convert_num(n){
			return n > 9 ? "" + n: "0" + n;
		}
$('#job_template_id').change(function () {
	var nowTemp = new Date();
	var today = nowTemp.getFullYear()+"-"+convert_num(nowTemp.getMonth()+1)+"-"+convert_num(nowTemp.getDate());
	$(".set_check_readonly").attr("disabled", false);
	var job_template_id = $('#job_template_id').val();
	if(job_template_id != '') {
		$.getJSON(BASEURL+"jobs/get_template_details/"+job_template_id,function(data){
			//console.log(data.job_title);
			var job_title = data.job_title;
			var job_description = data.job_description;
			var special_instructions = data.special_instructions;
			var start_date = today;
			var start_time = data.start_time;
			var end_date = today;
			var end_time = data.end_time;
			var payment_amount = data.payment_amount;
			var payment_mode = data.payment_mode;
			var required_NEA_certificate = data.required_NEA_certificate;
			var job_type_id = data.job_type_id;
			var job_type_name = data.name;

			//$("#job_title").empty();
			$('#job_title').val(job_title);
			$('#job_type_id').val(job_type_id);
			$('#description').val(job_description);
			$('#special_instructions').val(special_instructions);
			$('#start_date').val(start_date);
			$('#start_time').val(start_time);
			$('#end_date').val(end_date);
			$('#end_time').val(end_time);
			$('#payment_amount').val(payment_amount);
			//var htm = "";
			var htm = get_job_types (job_type_id,job_type_name);
			$('#job_type_div').html(htm);
			if(payment_mode =='1'){

				$('#check1').trigger('click');
			}
			if(payment_mode =='2'){

				$('#check2').trigger('click');
			}
			if(payment_mode=='3'){

				$('#check3').trigger('click');
			}
			if(required_NEA_certificate == 0){

				$('#check5').trigger('click');
			}
			if(required_NEA_certificate== 1){
				$('#check4').trigger('click');
			}

			var outlet_id = $('#outlet_id').val() ;
			if( outlet_id != "" ){
				$.ajax({
			type : "POST",
			async: false,
			url : BASEURL+"jobs/get_outlet_status",
			data : {
				outlet_id : outlet_id,
			},
			error : function(jqXHR, textStatus, errorThrown) {
				alert('An error has occured.');
			},
			success : function(data, textStatus, jqXHR) {
				var obj_data = JSON.parse(data);
				//alert(obj_data.toSource());
				if(obj_data.is_template_required == 1){
					if(job_template_id != ""){
						$('#job_type_div').html("");
						var htm  = "";
						var selected = "";
						htm += '<select class="form-control job_type_htm" name="job_type_id" id="job_type_id" required data-parsley-errors-container="#job_type_id-error">';
						selected = ' selected="selected"';
						htm += '<option value="'+job_type_id+'" '+selected+'>'+job_type_name+'</option>';
						htm += '</select>';
						//alert(htm);
						$('#job_type_div').html(htm);
						$("#form_sample_3 :input").prop("readonly", true);
						$(".set_check_readonly").attr("disabled", true);
						$("#form_sample_3 #special_instructions").prop("readonly", false);
						$("#form_sample_3 #job_title").prop("readonly", false);
					}
				}else{
					//alert("ss");
					$("#form_sample_3 :input").prop("readonly", false);
					$(".set_check_readonly").attr("disabled", false);
					$("#form_sample_3 #special_instructions").prop("readonly", false);
					$("#form_sample_3 #job_title").prop("readonly", false);
				//	alert("No Template Required");
				}
			}
			});

		}



		var roleId = "<?php echo LoginUserDetails('role_id')?>";
		if(roleId == 3){
			var template_req = '<?php echo $template_req; ?>';
			if(template_req == 1){
					$('#job_type_div').html("");
					var htm  = "";
					var selected = "";
					htm += '<select class="form-control job_type_htm" name="job_type_id" id="job_type_id" required data-parsley-errors-container="#job_type_id-error">';
					selected = ' selected="selected"';
					htm += '<option value="'+job_type_id+'" '+selected+'>'+job_type_name+'</option>';
					htm += '</select>';
					//alert(htm);
					$('#job_type_div').html(htm);
				$("#form_sample_3 :input").prop("readonly", true);
				$("#job_type_div").attr('readonly',true);
				$(".set_check_readonly").attr("disabled", true);
				$('#job_type_div').attr("disable", "true");
				$("#form_sample_3 #job_title").prop("readonly", false);
				$("#form_sample_3 #special_instructions").prop("readonly", false);
				//$("#form_sample_3 #special_instructions").prop("readonly", false);
			}
		}
			//$("#form_sample_3 :input:not([type='submit'])").prop("disabled", true);
			//$("#form_sample_3 :input").prop("readonly", true);
			//$(".set_check_readonly").attr("disabled", true);
			//$("#form_sample_3 #special_instructions").prop("readonly", false);
			//$("#form_sample_3 #job_title").prop("readonly", false);

			//$(".add-on").off("click");
		});
	}
});
$("#form_sample_3").submit(function(){
	var roleId = "<?php echo $userType; ?>";
	if($("#job_type_id").val()!=null){
		if((roleId=='5') && ($("#outlet_id").val()=="")){
			return false;
		}
		$("#form_sample_3 :input").prop("disabled", false);
	}
});

<?php if($id) { ?>
	if($("#outlet_id").length != 0) {
			get_area_manager(<?php echo $records->outlet_manager_id ?>);
		}
		<?php
	}
?>
$('#start_time').click(function () 	{
	$('#start_time_add_on').trigger("click");
});
$('#end_time').click(function () {
	$('#end_time_add_on').trigger("click");
});

$('#start_date_add_on').click(function () {
	$('#start_date').trigger("focus");
});
$('#end_date_add_on').click(function () {
	$('#end_date').trigger("focus");
});

$('#timepicker1,#timepicker2').datetimepicker({
    pickDate: false,
    pickSeconds: false,
    format: "hh:mm",
});


$( document ).ready(function() {
	var nowTemp = new Date();
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
$('#start_date').datepicker({
	format: 'yyyy-mm-dd',
    pickTime: false,
    minDate : now,
     onRender: function(date) {
        return date.valueOf() < now.valueOf() ? 'disabled' : '';
      }
}).on('changeDate', function(ev) {
	var start_date = $("#start_date").val();
	var sDate = new Date(start_date);
	var today = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate());
	if(start_date != '' && today > sDate) {
    var alert_msg1 =  "<?php echo $this->lang->line('date_alert_message'); ?>";
		alert(alert_msg1);
		$( "#start_date" ).val('');
		$( "#end_date" ).val('');
	}
		var end_date = $("#end_date").val();
		var eDate = new Date(end_date);
		if(start_date != '' && end_date != '' && sDate > eDate) {
			var alert_msg5 =  "<?php echo $this->lang->line('end_date_grea_start_date'); ?>";
			alert(alert_msg5);
			$( "#end_date" ).val('');
		}
    $("#end_date").val(start_date);
		 $(this).datepicker('hide');
});

$('#end_date').datepicker({
	format: 'yyyy-mm-dd',
    pickTime: false,
    autoclose :  true,
     onRender: function(date) {
        return date.valueOf() < now.valueOf() ? 'disabled' : '';
      }
}).on('changeDate', function(ev) {
		var start_date = $("#start_date").val();
		var end_date = $("#end_date").val();
		var sDate = new Date(start_date);
		var eDate = new Date(end_date);
		if(start_date != '' && end_date != '' && sDate > eDate) {
      var alert_msg5 =  "<?php echo $this->lang->line('end_date_grea_start_date'); ?>";
			alert(alert_msg5);
			$( "#end_date" ).val('');
		}
			 $(this).datepicker('hide');
	});

	/* ---------- code to disable the fields form  if login by OM-----*/
	var roleId = "<?php echo LoginUserDetails('role_id')?>";
	if(roleId == 3){
		var template_req = '<?php echo $template_req; ?>';
		if(template_req == 1){
			$("#form_sample_3 :input").prop("readonly", true);
			$("#job_type_div").attr('readonly',true);
			$(".set_check_readonly").attr("disabled", true);
			$('#job_type_div').attr("disable", "true");
			$("#form_sample_3 #job_title").prop("readonly", false);
		}
	}
});

function validation() {
	var nowTemp = new Date();
	var startDate = document.getElementById("start_date").value;
    var endDate = document.getElementById("end_date").value;
    var starttime = document.getElementById("start_time").value;
    var endtime = document.getElementById("end_time").value;
	var today = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate());
	var sDate = new Date(startDate);
	var eDate = new Date(endDate);
	if(startDate != '' && today > sDate) {
    var alert_msg1 =  "<?php echo $this->lang->line('date_alert_message'); ?>";
		alert(alert_msg1);
		return false;
	}else if(startDate != '' && today > eDate){
     var alert_msg2 =  "<?php echo $this->lang->line('end_date_alert_message'); ?>";
		alert(alert_msg2);
		return false;
	}
	else{
		if (startDate ==  endDate) {
			    var current_time = nowTemp.getHours()+":"+nowTemp.getMinutes()+":"+nowTemp. getSeconds();
				var aa1=starttime.split(":");
				var aa2=endtime.split(":");
				var current_time1 = current_time.split(":");
				var d1=new Date(parseInt("2001",10),(parseInt("01",10))-1,parseInt("01",10),parseInt(aa1[0],10),parseInt(aa1[1],10),parseInt(aa1[2],10));
				var d2=new Date(parseInt("2001",10),(parseInt("01",10))-1,parseInt("01",10),parseInt(aa2[0],10),parseInt(aa2[1],10),parseInt(aa2[2],10));
				var current_time2=new Date(parseInt("2001",10),(parseInt("01",10))-1,parseInt("01",10),parseInt(current_time1[0],10),parseInt(current_time1[1],10),parseInt(current_time1[2],10));
				var dd1=d1.valueOf();
				var dd2=d2.valueOf();
				var sDate = new Date(startDate);
				var start_date = new Date(sDate.getFullYear(), sDate.getMonth(), sDate.getDate());
				if(today.valueOf() == start_date.valueOf()){
					if(dd1 < current_time2.valueOf()){
            var alert_msg3 =  '<?php echo $this->lang->line('start_time_alert_message'); ?>';
						alert(start_time_alert_message);
						return false;
					}
				}
				if(dd1>dd2)
				{
					var alert_msg4 =  "<?php echo $this->lang->line('end_time_alert_message'); ?>";
					alert(alert_msg4);
					return false;
				}
		}
	}
 }

	$('#outlet_id').change(function () {
		$(".set_check_readonly").attr("disabled", false);
		var job_template_id = $('#job_template_id').val();
		var outlet_id = this.value;
			$.ajax({
			type : "POST",
			async: false,
			url : BASEURL+"jobs/get_outlet_status",
			data : {
				outlet_id : outlet_id,
			},
			error : function(jqXHR, textStatus, errorThrown) {
				alert('An error has occured.');
			},
			success : function(data, textStatus, jqXHR) {

				var obj_data = JSON.parse(data);
				if(obj_data.is_template_required == 1){
					if(job_template_id != ""){

						$.getJSON(BASEURL+"jobs/get_template_details/"+job_template_id,function(data){
							var nowTemp = new Date();
							var today = nowTemp.getFullYear()+"-"+convert_num(nowTemp.getMonth()+1)+"-"+convert_num(nowTemp.getDate());
							var job_title = data.job_title;
							var job_description = data.job_description;
							var special_instructions = data.special_instructions;
							var start_date = today;
							var start_time = data.start_time;
							var end_date = today;
							var end_time = data.end_time;
							var payment_amount = data.payment_amount;
							var payment_mode = data.payment_mode;
							var required_NEA_certificate = data.required_NEA_certificate;
							var job_type_id = data.job_type_id;
							var job_type_name = data.name;

							//$("#job_title").empty();
							$('#job_title').val(job_title);
							$('#job_type_id').val(job_type_id);
							$('#description').val(job_description);
							$('#special_instructions').val(special_instructions);
							$('#start_date').val(start_date);
							$('#start_time').val(start_time);
							$('#end_date').val(end_date);
							$('#end_time').val(end_time);
							$('#payment_amount').val(payment_amount);
							//var htm = "";
							var htm = get_job_types (job_type_id,job_type_name);
							$('#job_type_div').html(htm);

							if(payment_mode =='1'){
								$(".set_check_readonly").attr("disabled", false);
								$('#check1').trigger('click');
							}
							if(payment_mode =='2'){
								$(".set_check_readonly").attr("disabled", false);
								$('#check2').trigger('click');
							}
							if(payment_mode=='3'){
								$(".set_check_readonly").attr("disabled", false);
								$('#check3').trigger('click');
							}
							if(required_NEA_certificate == 0){

								$('#check5').trigger('click');
							}
							if(required_NEA_certificate== 1){
								$('#check4').trigger('click');
							}

							var value = $("#job_type_id").val();
							var text  = $('#job_type_id :selected').text();
							$('#job_type_div').html("");
							var htm  = "";
							var selected = "";
							htm += '<select class="form-control job_type_htm" name="job_type_id" id="job_type_id" required data-parsley-errors-container="#job_type_id-error">';
							selected = ' selected="selected"';
							htm += '<option value="'+value+'" '+selected+'>'+text+'</option>';
							htm += '</select>';
							$('#job_type_div').html(htm);
							$("#form_sample_3 :input").prop("readonly", true);
							$(".set_check_readonly").attr("disabled", true);
							$("#form_sample_3 #special_instructions").prop("readonly", false);
							$("#form_sample_3 #job_title").prop("readonly", false);
						});
					}	else{
						$("#form_sample_3 :input").prop("readonly", true);
						$("#form_sample_3 :input").val('');
						$(".set_check_readonly").attr("disabled", true);
						$("#form_sample_3 #special_instructions").prop("readonly", false);
						$("#form_sample_3 #job_title").prop("readonly", false);
						$("#form_sample_3 #job_title").val('');
					}
				}else{
					var value = $("#job_type_id").val();
					var text  = $('#job_type_id :selected').text();
					$('#job_type_div').html("");
					var htm = get_job_types (value,text);
					$('#job_type_div').html(htm);
					$("#form_sample_3 :input").prop("readonly", false);
					$(".set_check_readonly").attr("disabled", false);
					$("#form_sample_3 #special_instructions").prop("readonly", false);
					$("#form_sample_3 #job_title").prop("readonly", false);
					//alert("No Template Required");
				}
			}
		});
	});

	function get_job_types(job_type_id,job_type_name){
		var htm  = "";
		var selected = "";
		var json_obj = '<?php echo json_encode($jobTypes); ?>';
		var obj_data = JSON.parse(json_obj);
		htm += '<select class="form-control job_type_htm" name="job_type_id" id="job_type_id" required data-parsley-errors-container="#job_type_id-error">';
		$.each(obj_data, function( index, value ) {
			if(job_type_id == value.job_role_id){ selected = ' selected="selected"'; }else{ selected ="";}
				htm += '<option value="'+value.job_role_id+'" '+selected+'>'+value.name+'</option>';

		});
		htm += '</select>';
		return htm;
	}
</script>
