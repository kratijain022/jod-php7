<?php
//echo '<pre>'; print_r($applicants);die;
$f = false;
$message = getGlobalMessage();
	if($message['type']=='success') {
	?>
  <div class="Metronic-alerts alert alert-success fade in" id="prefix_167847684379">
    <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
    <?php echo $message['msg'] ?></div>
  <?php
	} else if($message['type']=='error') {
	?>
  <div class="Metronic-alerts alert alert-danger fade in" id="prefix_167847684379">
    <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
    <?php echo $message['msg'] ?></div>
  <?php
	}
	?>
<style>
.star-rating .caption { display:none; }
</style>
<div class="right_col right_col" role="main">
		<ul class="breadcrumb breadcrumb2">
			<li class="gray"><a href="<?php echo base_url(); ?>"><?php echo $this->lang->line('home'); ?></a></li>
			<li class="active"><?php echo $this->lang->line('job_posting'); ?></li>
			<li class="active"><?php echo $this->lang->line('active_job_posting'); ?></li>
			<li class="active"><?php echo $this->lang->line('rejected_applicants'); ?></li>
		</ul>
	<?php //echo "<pre>"; print_r($applicants); die;
	 if(!empty($applicants)):  $flag= true; ?>
		<?php foreach($applicants as $applicant):
			if(($applicant->job_status == 2 && $applicant->self_cancel_status == 0)):  $f = true;?>
				<input type="hidden" name="candidate_ids[]" id="cand_id<?php echo $applicant->applicant_user_account_id;?>">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 main-wrapper">
				  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 gray-back no-padding table-rate">
					 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 left-sec no-padding table-cell-rate">
						<span class="profile_img"><img style="width:203px; height:173px;" src="<?php echo  checkImageFile(IMAGEURL.'applicantes/'.$applicant->portrait) == true ? IMAGEURL.'applicantes/'.$applicant->portrait : IMAGE.'/no_image.png'; ?>" /><span class="circle"><?php echo getTotalvoteCounts($applicant->applicant_user_account_id); ?></span></span>
						<div class="clearfix"></div>
						<div class="new_rate"><input id="<?php echo "rating_".$applicant->user_account_id; ?>" class="rating" data-stars="5" data-step="0.1" data-size="sm" rate="<?php echo getAverageRating($applicant->applicant_user_account_id);?>"/></div>
						<div class="clearfix"></div>
						<div class="m-height58"> </div>
						<a href="<?php echo base_url('jobs/user_profile/'.$applicant->user_account_id); ?>"><button class="job-btn block100 org-box col-xs-12 ab-btn"><i class="glyphicon glyphicon-user"></i><?php echo $this->lang->line('view_profile'); ?></button></a>
					 </div>
					<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 no-padding min-h321 table-cell-rate">
						<h3 class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><?php echo ucwords($applicant->first_name.' '.$applicant->last_name);?> <?php echo ($applicant->gender==1)?'(M)':'(F)';?></h3>
						<div class="clearfix"></div>
						<p class="discri col-lg-12 col-md-12 col-sm-12 col-xs-12 m-heig85 res-height">
							<?php
								$dob = date_create($applicant->date_of_birth);
								$today = date_create(date('Y-m-d'));
								$age = date_diff($dob,$today, true);
								echo $age->y;
							?> <?php echo $this->lang->line('years_old'); ?><br />
							<span class="address-box"><?php echo strlen($applicant->address) > 110 ? substr($applicant->address,0,110).".." : $applicant->address ; ?></span>
						</p>
						<div class="clearfix"></div>
						<p class="discri col-lg-12 col-md-12 col-sm-12 col-xs-12"><span><input type="checkbox" <?php echo ($applicant->is_NEA_certified==1)?'checked="checked"':'';?>/> </span><span><?php echo $this->lang->line('nea_food_handler'); ?></span> </p>
						<div class="clearfix"></div>
						<p class="discri col-lg-12 col-md-12 col-sm-12 col-xs-12 m-heig76 res-height" style="height:84px;">
							<?php $exprience_details =  getEmploymentHistory($applicant->applicant_user_account_id);
							if(!empty($exprience_details)):
							$exp_data = end($exprience_details);?>
							   <?php echo $this->lang->line('years_of_exp'); ?>: <?php echo $exp_data->length_of_service;?> <?php echo ($exp_data->length_of_service>1)?'yrs':'yr';?><br />
							     <span class="" title="<?php echo $exp_data->job_types; ?>"><?php echo $this->lang->line('job_roles'); ?>: <?php echo strlen($exp_data->job_types) > 50 ?  substr($exp_data->job_types,0,50)."..." : $exp_data->job_types ;?></span> <br />
							   <span title="<?php echo $exp_data->employer; ?>"><?php echo $this->lang->line('past_employers'); ?>: <?php echo strlen($exp_data->employer) > 50 ? substr($exp_data->employer,0,50).".." : $exp_data->employer ;?></span><br />
							<?php else: ?>
								<?php echo $this->lang->line('no_experience'); ?> <br />
							<?php endif;?>
						</p>
						<div class="clearfix"></div>
						<div class="high30"></div>
						<div class="job-btn block100 red-box col-xs-12 text-center ab-btn"><?php echo $this->lang->line('rejected'); ?></div>

					 </div>
				  </div>
				</div>

			<?php endif;?>
			<?php /* if($applicant->self_cancel_status == 1 && ($applicant->job_status == 0 || $applicant->job_status == 1 || $applicant->job_status == 2  )  && $applicant->system_rejected == 0 ): $f = true; ?>
			<input type="hidden" name="candidate_ids[]" id="cand_id<?php echo $applicant->applicant_user_account_id;?>">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 main-wrapper">
				  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 gray-back no-padding table-rate">
					 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 left-sec no-padding table-cell-rate">
						<span class="profile_img"><img style="width:203px; height:173px;" src="<?php echo  checkImageFile(IMAGEURL.'applicantes/'.$applicant->portrait) == true ? IMAGEURL.'applicantes/'.$applicant->portrait : IMAGE.'/no_image.png'; ?>" /><span class="circle"><?php echo getTotalvoteCounts($applicant->applicant_user_account_id); ?></span></span>
						<div class="clearfix"></div>
						<div class="new_rate"><input id="<?php echo "rating_".$applicant->user_account_id; ?>" class="rating" data-stars="5" data-step="0.1" data-size="sm" rate="<?php echo getAverageRating($applicant->applicant_user_account_id);?>"/></div>
						<div class="clearfix"></div>
						<div class="m-height58"></div>
						<a href="<?php echo base_url('jobs/user_profile/'.$applicant->user_account_id); ?>"><button class="job-btn block100 btn-bottom org-box col-xs-12 ab-btn"><i class="glyphicon glyphicon-user"></i>View Profile</button></a>
					 </div>
					 <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 no-padding min-h321 table-cell-rate">
						<h3 class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><?php echo ucwords($applicant->first_name.' '.$applicant->last_name);?> <?php echo ($applicant->gender==1)?'(M)':'(F)';?></h3>
						<div class="clearfix"></div>
						<p class="discri col-lg-12 col-md-12 col-sm-12 col-xs-12 m-heig85 res-height">
							<?php
								$dob = date_create($applicant->date_of_birth);
								$today = date_create(date('Y-m-d'));
								$age = date_diff($dob,$today, true);
								echo $age->y;
							?> years old<br />
							<?php //echo strlen($applicant->address) > 40 ? substr($applicant->address,0,40)."..." : $applicant->address ; ?>
							<span class="address-box"><?php echo strlen($applicant->address) > 110 ? substr($applicant->address,0,110).".." : $applicant->address ; ?></span>
						</p>
						<div class="clearfix"></div>

						<p class="discri col-lg-12 col-md-12 col-sm-12 col-xs-12"><span><input type="checkbox" <?php echo ($applicant->is_NEA_certified==1)?'checked="checked"':'';?>/> </span><span>NEA Food Handler</span> </p>
						<div class="clearfix"></div>
						<p class="discri col-lg-12 col-md-12 col-sm-12 col-xs-12 m-heig76 res-height" style="height:84px;">
							<?php $exprience_details =  getEmploymentHistory($applicant->applicant_user_account_id);
							if(!empty($exprience_details)):
							$exp_data = end($exprience_details);?>
							   Years of Experience: <?php echo $exp_data->length_of_service;?> <?php echo ($exp_data->length_of_service>1)?'yrs':'yr';?><br />
							    <span class="" title="<?php echo $exp_data->job_types; ?>">Job Roles: <?php echo strlen($exp_data->job_types) > 50 ?  substr($exp_data->job_types,0,50)."..." : $exp_data->job_types ;?></span> <br />
							   <span title="<?php echo $exp_data->employer; ?>">Past Employers: <?php echo strlen($exp_data->employer) > 50 ? substr($exp_data->employer,0,50).".." : $exp_data->employer ;?></span><br />
							<?php else: ?>
								No experience <br />
							<?php endif;?>
						</p>
						<div class="clearfix"></div>
						<div class="high30"></div>
						<div class="job-btn block100 red-box col-xs-12 text-center btn-bottom ab-btn">Self Cancelled</div>

					 </div>


				  </div>
				</div>
				<?php endif; */?>

			<?php /* if($applicant->self_cancel_status == 0  && $applicant->job_status == 1  &&  $applicant->acknowledged == 0 ):  $f = true?>
			<input type="hidden" name="candidate_ids[]" id="cand_id<?php echo $applicant->applicant_user_account_id;?>">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 main-wrapper">
				  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 gray-back no-padding table-rate">
					 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 left-sec no-padding table-cell-rate">
						<span class="profile_img"><img style="width:203px; height:173px;" src="<?php echo  checkImageFile(IMAGEURL.'applicantes/'.$applicant->portrait) == true ? IMAGEURL.'applicantes/'.$applicant->portrait : IMAGE.'/no_image.png'; ?>" /><span class="circle"><?php echo getTotalvoteCounts($applicant->applicant_user_account_id); ?></span></span>
						<div class="clearfix"></div>
						<div class="new_rate"> <input id="<?php echo "rating_".$applicant->user_account_id; ?>" class="rating" data-stars="5" data-step="0.1" data-size="sm" rate="<?php echo getAverageRating($applicant->applicant_user_account_id);?>"/></div>
						<div class="clearfix"></div>
						<div class="m-height58"> </div>
						<a href="<?php echo base_url('jobs/user_profile/'.$applicant->user_account_id); ?>"><button class="job-btn block100 btn-bottom org-box col-xs-12 ab-btn"><i class="glyphicon glyphicon-user"></i>View Profile</button></a>
					 </div>

					 <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 no-padding min-h321 table-cell-rate">

						<h3 class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><?php echo ucwords($applicant->first_name.' '.$applicant->last_name);?> <?php echo ($applicant->gender==1)?'(M)':'(F)';?></h3>
						<div class="clearfix"></div>
						<p class="discri col-lg-12 col-md-12 col-sm-12 col-xs-12 m-heig85 res-height">
							<?php
								$dob = date_create($applicant->date_of_birth);
								$today = date_create(date('Y-m-d'));
								$age = date_diff($dob,$today, true);
								echo $age->y;
							?> years old<br />
							<?php //echo strlen($applicant->address) > 40 ? substr($applicant->address,0,40)."..." : $applicant->address ; ?>
							<span class="address-box"><?php echo strlen($applicant->address) > 100 ? substr($applicant->address,0,100)."..." : $applicant->address ; ?></span>
						</p>
						<div class="clearfix"></div>
						<p class="discri col-lg-12 col-md-12 col-sm-12 col-xs-12"><span><input type="checkbox" <?php echo ($applicant->is_NEA_certified==1)?'checked="checked"':'';?>/> </span><span>NEA Food Handler</span> </p>
						<div class="clearfix"></div>
						<p class="discri col-lg-12 col-md-12 col-sm-12 col-xs-12 m-heig76 res-height" style="height:84px;">
							<?php $exprience_details =  getEmploymentHistory($applicant->applicant_user_account_id);
							if(!empty($exprience_details)):
							$exp_data = end($exprience_details);?>
							   Years of Experience: <?php echo $exp_data->length_of_service;?> <?php echo ($exp_data->length_of_service>1)?'yrs':'yr';?><br />
							   <span class="" title="<?php echo $exp_data->job_types; ?>">Job Roles: <?php echo strlen($exp_data->job_types) > 50 ?  substr($exp_data->job_types,0,50)."..." : $exp_data->job_types ;?></span> <br />
							   <span title="<?php echo $exp_data->employer; ?>">Past Employers: <?php echo strlen($exp_data->employer) > 50 ? substr($exp_data->employer,0,50).".." : $exp_data->employer ;?></span><br />
							<?php else: ?>
								No experience <br />
							<?php endif;?>
						</p>
						<div class="clearfix"></div>
						<div class="high30"></div>
						<div class="job-btn block100 black-box col-xs-12 text-center btn-bottom ab-btn">Not Acknowledged</div>

					 </div>


				  </div>
				</div>
				<?php endif; */?>
					<?php if($applicant->self_cancel_status == 1 && ($applicant->job_status == 0 || $applicant->job_status == 1 || $applicant->job_status == 2  )  && $applicant->system_rejected == 1 ): $f = true; ?>
			<input type="hidden" name="candidate_ids[]" id="cand_id<?php echo $applicant->applicant_user_account_id;?>">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 main-wrapper">
				  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 gray-back no-padding table-rate">
					 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 left-sec no-padding table-cell-rate">
						<span class="profile_img"><img style="width:203px; height:173px;" src="<?php echo  checkImageFile(IMAGEURL.'applicantes/'.$applicant->portrait) == true ? IMAGEURL.'applicantes/'.$applicant->portrait : IMAGE.'/no_image.png'; ?>" /><span class="circle"><?php echo getTotalvoteCounts($applicant->applicant_user_account_id); ?></span></span>
						<div class="clearfix"></div>
						<div class="new_rate"><input id="<?php echo "rating_".$applicant->user_account_id; ?>" class="rating" data-stars="5" data-step="0.1" data-size="sm" rate="<?php echo getAverageRating($applicant->applicant_user_account_id);?>"/></div>
						<div class="clearfix"></div>
						<div class="m-height58"></div>
						<a href="<?php echo base_url('jobs/user_profile/'.$applicant->user_account_id); ?>"><button class="job-btn block100 btn-bottom org-box col-xs-12 ab-btn"><i class="glyphicon glyphicon-user"></i><?php echo $this->lang->line('view_profile'); ?></button></a>
					 </div>
					 <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 no-padding min-h321 table-cell-rate">
						<h3 class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><?php echo ucwords($applicant->first_name.' '.$applicant->last_name);?> <?php echo ($applicant->gender==1)?'(M)':'(F)';?></h3>
						<div class="clearfix"></div>
						<p class="discri col-lg-12 col-md-12 col-sm-12 col-xs-12 m-heig85 res-height">
							<?php
								$dob = date_create($applicant->date_of_birth);
								$today = date_create(date('Y-m-d'));
								$age = date_diff($dob,$today, true);
								echo $age->y;
							?> <?php echo $this->lang->line('years_old'); ?><br />
							<?php //echo strlen($applicant->address) > 40 ? substr($applicant->address,0,40)."..." : $applicant->address ; ?>
							<span class="address-box"><?php echo strlen($applicant->address) > 110 ? substr($applicant->address,0,110).".." : $applicant->address ; ?></span>
						</p>
						<div class="clearfix"></div>

						<p class="discri col-lg-12 col-md-12 col-sm-12 col-xs-12"><span><input type="checkbox" <?php echo ($applicant->is_NEA_certified==1)?'checked="checked"':'';?>/> </span><span><?php echo $this->lang->line('nea_food_handler'); ?></span> </p>
						<div class="clearfix"></div>
						<p class="discri col-lg-12 col-md-12 col-sm-12 col-xs-12 m-heig76 res-height" style="height:84px;">
							<?php $exprience_details =  getEmploymentHistory($applicant->applicant_user_account_id);
							if(!empty($exprience_details)):
							$exp_data = end($exprience_details);?>
							   <?php echo $this->lang->line('years_of_exp'); ?>: <?php echo $exp_data->length_of_service;?> <?php echo ($exp_data->length_of_service>1)?'yrs':'yr';?><br />
							    <span class="" title="<?php echo $exp_data->job_types; ?>"> <?php echo $this->lang->line('job_roles'); ?>: <?php echo strlen($exp_data->job_types) > 50 ?  substr($exp_data->job_types,0,50)."..." : $exp_data->job_types ;?></span> <br />
							   <span title="<?php echo $exp_data->employer; ?>"><?php echo $this->lang->line('past_employers'); ?>: <?php echo strlen($exp_data->employer) > 50 ? substr($exp_data->employer,0,50).".." : $exp_data->employer ;?></span><br />
							<?php else: ?>
								<?php echo $this->lang->line('no_experience'); ?> <br />
							<?php endif;?>
						</p>
						<div class="clearfix"></div>
						<div class="high30"></div>
						<div class="job-btn block100 red-box col-xs-12 text-center btn-bottom ab-btn"><?php echo $this->lang->line('rejected_due_to_time_overlap'); ?> </div>

					 </div>


				  </div>
				</div>
				<?php endif;?>
		<?php endforeach;?>
			<div class="clearfix"></div>
			<?php if($f == true ) { ?>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 m-t80">
				<a href="<?php echo BASEURL.'jobs/job_detail/'.$applicants[0]->job_id.'/2';?>"><button type="button" name="back" type="button" class="create org-box col-lg-3 col-md-3 col-xs-12 pull-left"><i class="fa fa-chevron-left"></i><span><?php echo $this->lang->line('back_to_job'); ?> </span>  </button></a>
				<button name="submit" type="submit" value="submit" class="create start_job_btn org-box col-lg-3 col-md-3 col-xs-12 pull-left"><span><?php echo $this->lang->line('confirm_selection'); ?> </span><i class="fa fa-chevron-right"></i></button>
			</div>
		<?php } ?>
		<div class="clearfix"></div>
	<?php endif; ?>
	<?php if($f == false ) { ?>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bmar">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 dark_organge org-boxs no-padding">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 dark_organge org-box  no-padding left-mar nob-mar job-box">
						<div class="col-lg-10 col-md-10 col-sm-10 col-xs-9 center-block no-float text-center no-candidate">
							<div class="over-scroll-left">
							<h2><?php echo $this->lang->line('no_candidates_found'); ?></h2>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php } ?>

</div>
<script>
	var flag = '<?php echo $flag ; ?>';
	if(flag == true){
	//	var applicants_obj  = '<?php echo json_encode($applicants); ?>';
		var applicants_obj  =  JSON.parse(decodeURIComponent("<?php echo rawurlencode(json_encode($applicants)); ?>"));
		//applicants_obj = JSON.parse(applicants_obj);
		$.each(applicants_obj, function( index, value ) {
			if(value.cancel_status != 1){
				var id = "#rating_"+value.applicant_user_account_id ;
				var  rate = $(id).attr('rate');
				$(id).rating('update', rate);
			}
		});
	}
	$('.rating').rating('refresh', {
		disabled:true,
		showCaption: false
	});
	$( document ).ready(function() {
		$('input[type="checkbox"]').click(function(event) {
			this.checked = false; // reset first
			event.preventDefault();
		});
	});
</script>
