<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$message = getGlobalMessage();
?>
<div class="right_col right_col" role="main">
	<div class="row padding-top-one">
		<?php
if($message['type']=='success') {
?>
<div class="Metronic-alerts alert alert-success fade in" id="prefix_167847684379"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button><?php echo $message['msg'] ?></div>
<?php
}
?>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<ul class="breadcrumb breadcrumb2">
			<li><a href="<?php echo base_url(); ?>"><?php echo $this->lang->line('home'); ?></a></li>
			<li class="active"><?php echo $this->lang->line('template_list'); ?></li>
			</ul>
		</div>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<table class="table table-hovers no-wrap"  id="table3">
					<thead class="firt-top">
						<tr>
							<th></th>
							<th><?php echo $this->lang->line('template_name'); ?></th>
							<th><?php echo $this->lang->line('job_title'); ?></th>
							<th><?php echo $this->lang->line('job_description'); ?></th>
							<th><?php echo $this->lang->line('action'); ?></th>
							<th></th>
						</tr>
					</thead>
						<?php /*
							$i=1;
							foreach($records as $template)
							{
								//echo "<pre>";
								//print_r($template);
								//die;

								if($template->status==0) {
									$class = "enable";
									$name = "Enable";
									$fa = "fa fa-check";
									$trClass = "gray-box";
									$title = "Click to enable record";
								} else if($template->status==1) {
									$class = "disable";
									$name = "Disable";
									$fa = "fa fa-ban";
									$trClass = "";
									$title = "Click to disable record";
								}
						?>
							<tr class="<?php echo $trClass; ?>">
								<td></td>
								<td><?php echo $template->template_name; ?></td>
								<td><?php echo $template->job_title; ?></td>
								<td><?php echo $template->job_description ;?></td>
								<td class="edit">
									<a href="<?php echo base_url('jobs/job_template/'.$template->template_id); ?>">
										<span><i class="fa fa-pencil"></i></span>
										<span>Edit</span>
									</a>
								</td>
								<td class="<?php echo $class; ?>"><a href="javascript:;" title="<?php echo $title; ?>" onclick="myFunction('<?php echo $template->template_id; ?>','<?php echo $template->status; ?>')">
									<span><i class="<?php echo $fa ?>"></i></span>
									<span><?php echo $name; ?></span>
								</a></td>
							</tr>
						<?php } */
						?>
					</tbody>
				</table>
			<div class="clearfix"></div>
			</div>
		</div>
	</div>
</div>
<script src="<?php echo base_url('templates/system/js/bootbox.min.js'); ?>"></script>

<script>
<?php if(isset($site_language) && $site_language == "chinese") { ?>
	var lang_url = "//cdn.datatables.net/plug-ins/1.10.12/i18n/Chinese.json";
	bootbox.setDefaults({
		locale: "zh_CN"
    });
<?php }else{ ?>
	var lang_url = "//cdn.datatables.net/plug-ins/1.10.12/i18n/English.json";
	bootbox.setDefaults({
		locale: "en"
	 });
<?php }?>

$('#table3').dataTable({
	"bPaginate": true,
	"bLengthChange": true,
	"bFilter": true,
	"bSort": true,
	"bInfo": true,
	"bAutoWidth": false,
	"order": [[ 0, "desc" ]],
	"columnDefs": [ { "targets": 0, "bSortable": true,"orderable": true, "visible": false }],
	"processing": true,
    "serverSide": true,
	"ajax": '<?php echo base_url('jobs/jobTemplateListByAjax'); ?>',
	"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
				if(aData[6] != '') {
					$(nRow).addClass(aData[6]);
					//$(nRow).addClass('multiorder');
				}
			$('td:eq(3)', nRow).addClass('edit');
			$('td:eq(4)', nRow).addClass(aData[7]);
	},
	"language":{
                "url": lang_url
    },

});
function myFunction(id,status) {
	var msg = "<?php echo $this->lang->line('status_change_msg')."?";  ?>";
		bootbox.confirm(msg, function(result) {
			if(result==true) {
				location.href="<?php echo base_url('jobs/job_template_status'); ?>/"+id+"/"+status;

			}
		});
	}

	$(document).ready(function() {
	   setTimeout(function(){
		   $('#table3_length').show();
		   $('#table3_filter').show();
		   $('#table3_paginate').show();
		}, 1000);

	});
</script>
