<div class="right_col right_col" role="main">
	<?php
	if(!empty($applicants_feedback)){
		foreach($applicants_feedback as $applicants_feed){
			$managers_feedback = get_managers_feedback($applicants_feed->job_id,$applicants_feed->applicant_user_account_id);
			?>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 main-wrapper">
				  <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 gray-back2 no-padding">
					 <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 left-sec no-padding">
                   <h3 class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><?php echo strlen($applicants_feed->first_name.' '.$applicants_feed->last_name) > 12 ? substr(ucwords($applicants_feed->first_name.' '.$applicants_feed->last_name),0,12).".." : ucwords($applicants_feed->first_name.' '.$applicants_feed->last_name);?> <?php echo ($applicants_feed->gender==1)?'(M)':'(F)';?></h3>
						<span class="profile_img"><img src="<?php echo checkImageFile(IMAGEURL.'applicantes/'.$applicants_feed->portrait) == true ?  IMAGEURL.'applicantes/'.$applicants_feed->portrait :  IMAGE.'/no_image.png'; ?>" class="g-img" style="width:203px; height:173px;"><span class="circle"><?php echo getTotalvoteCounts($applicants_feed->applicant_user_account_id); ?></span></span>
						<div class="clearfix"></div>

						<div class="clearfix"></div>
						<a href="<?php echo base_url('jobs/user_profile/'.$applicants_feed->applicant_user_account_id);?>"><button class="job-btn block100 org-box col-xs-12" type="button"><i class="glyphicon glyphicon-user"></i>View Profile</button></a>
					 </div>
					 <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
						<h4 class="col-lg-11 col-md-11 col-sm-11 col-xs-12 text-center">Applicant's Feedback</h4>
						<div class="clearfix"></div>
				<div class="col-lg-9 col-md-10 col-sm-10 col-xs-12 text-center no-float center-block">
							<input id="" class="rating" data-stars="5" data-step="0.1" data-size="sm" value="<?php echo $applicants_feed->rating;?>"/>
					</div>
						<div class="clearfix"></div>
						<p class="discri col-lg-12 col-md-12 col-sm-12 col-xs-12"><?php echo $applicants_feed->additional_comments;?> </p>
						<div class="clearfix"></div>
					 </div>
				</div>

				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 org-box8 no-padding">
					<div class="col-lg-11 col-md-11 col-sm-11 col-xs-12 no-float center-block text-center">
						<h4 class="col-lg-12 col-md-12 col-sm-12 col-xs-12">Manager's Feedback</h4>
						<div class="clearfix"></div>
							<input id="" class="rating" data-stars="5" data-step="0.1" data-size="sm" value="<?php echo (!empty($managers_feedback))?$managers_feedback->rating:'';?>"/>
						<div class="clearfix"></div>
						<p class="discri col-lg-12 col-md-12 col-sm-12 col-xs-12"><?php echo (!empty($managers_feedback))?$managers_feedback->feedback_text:'';?></p>
						<div class="clearfix"></div>
					 </div>
                </div>
				</div>


			<?php
		}
	}else{ ?>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bmar">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 dark_organge org-boxs no-padding">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 dark_organge org-box  no-padding left-mar nob-mar job-box">
						<div class="col-lg-10 col-md-10 col-sm-10 col-xs-9 center-block no-float no-candidate">
							<div class="over-scroll-left">
							<h2><?php echo "This job get completed with no applicants."; ?></h2>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php }
	?>

         <div class="clearfix"></div>
                </div>
<script>
	$('.rating').rating('refresh', {
		disabled:true,
		showCaption: true
	});
</script>
