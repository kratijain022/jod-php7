<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$message = getGlobalMessage();
//print_r($records); die;

?>
<div class="right_col right_col" role="main">
	<div class="row padding-top-one">
		<?php
		if($message['type']=='success') {
		?>
		<div class="Metronic-alerts alert alert-success fade in" id="prefix_167847684379"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button><?php echo $message['msg'] ?></div>
		<?php
		}
		?>
		<ul class="breadcrumb breadcrumb2">
			<li><a href="<?php echo base_url(); ?>"><?php echo $this->lang->line('home'); ?></a></li>
			<li class="active"><?php echo $this->lang->line('notifications'); ?></li>
		</ul>

		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
			<?php
			if(!empty($records)){
				if($count > 0){
					foreach($records as $record)
						{
							$outlet = getOutletDetail($record->outlet_id) ;
							$job_role = getJobRole($record->job_type_id) ;
							if($record->is_approved == 0){
				?>
							<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 org-box  no-padding job-box">
									  <div class="col-lg-10 col-md-10 col-sm-10 col-xs-9 center-block no-float">
									<h3><?php echo $this->lang->line('new_job_created'); ?></h3>
									<p>
										<span><?php if(strlen($outlet->outlet_name) > 15) { echo ucfirst(substr($outlet->outlet_name,0,15)).'...'; } else { echo $outlet->outlet_name; }?></span>
										<a href="<?php echo BASEURL.'jobs/job_detail/'.$record->job_id;?>"><span><?php if(strlen($job_role->name) > 15) { echo ucfirst(substr($job_role->name,0,15)).'...'; } else { echo $job_role->name; }?> ($<?php echo $record->payment_amount ?>/hr)</span></a>
										<span><?php echo date('d M Y',strtotime($record->start_date))?></span>
									</p>
									</div>
									<div class="clearfix"></div>
									<a class="approve job-btn" href="javascript:;" onclick="approve_job('<?php echo $record->job_id; ?>')"><?php echo $this->lang->line('approve'); ?></a>
									<a class="reject job-btn red-box"  href="javascript:;" onclick="reject_job('<?php echo $record->job_id; ?>')"><?php echo $this->lang->line('reject'); ?></a>
								</div>
							</div>
				<?php
						}
						if($record->is_completed == 1) {
				?>
							<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 org-box dark-org no-padding job-box">
								  <div class="col-lg-10 col-md-10 col-sm-10 col-xs-9 center-block no-float">
									<h3><?php echo $this->lang->line('job_completed'); ?></h3>
									<p>
										<span><?php if(strlen($outlet->outlet_name) > 15) { echo ucfirst(substr($outlet->outlet_name,0,30)).'...'; } else { echo $outlet->outlet_name; }?></span>
										<a href="<?php echo BASEURL.'jobs/job_detail/'.$record->job_id;?>"><span><?php if(strlen($job_role->name) > 15) { echo ucfirst(substr($job_role->name,0,15)).'...'; } else { echo $job_role->name; }?> ($<?php echo $record->payment_amount ?>/hr)</span></a>
										<span><?php echo date('d M Y',strtotime($record->created_at))?></span>
									</p>
									</div>
									<div class="clearfix"></div>
									<a href="<?php echo base_url('jobs/job_feedback_section/'.$record->job_id); ?>" class="view-feedback job-btn"><?php echo $this->lang->line('view_feedback'); ?><i class="mar-left fa fa-arrow-circle-right"></i></a>
								</div>
							</div>
				<?php
						}
					}
				}else{ ?>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bmar">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 dark_organge org-boxs no-padding">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 dark_organge org-box  no-padding left-mar nob-mar job-box">
									<div class="col-lg-10 col-md-10 col-sm-10 col-xs-9 center-block no-float">
										<div class="over-scroll-left">
											<h2><?php echo $this->lang->line('no_notification_msg'); ?></h2>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
			<?php	} ?>
			<?php }else{ ?>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bmar">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 dark_organge org-boxs no-padding">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 dark_organge org-box  no-padding left-mar nob-mar job-box">
								<div class="col-lg-10 col-md-10 col-sm-10 col-xs-9 center-block no-float">
									<div class="over-scroll-left">
										<h2><?php echo $this->lang->line('no_notification_msg'); ?></h2>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?php	}
			?>
		</div>
	</div>
	<?php if(!empty($records)){
			  echo $this->pagination->create_links();
		}
	?>
</div>
<script>
<?php if(isset($site_language) && $site_language == "chinese") { ?>
	bootbox.setDefaults({
		locale: "zh_CN"
    });
<?php }else{ ?>
	bootbox.setDefaults({
		locale: "en"
	 });
	
<?php }?>	
function approve_job(id) {
	var msg = "<?php echo $this->lang->line('status_change_msg')."?";  ?>";
	bootbox.confirm(msg, function(result) {
		if(result==true) {
			$('#loader').fadeIn();
			var status = 1;
			location.href="<?php echo base_url('jobs/update_job_status'); ?>/"+id+"/"+status;
		}
	});
}

function reject_job(id) {
	var msg = "<?php echo $this->lang->line('status_change_msg')."?";  ?>";
	bootbox.confirm(msg, function(result) {
		if(result==true) {
			$('#loader').fadeIn();
			var status = 2;
			location.href="<?php echo base_url('jobs/update_job_status'); ?>/"+id+"/"+status;
		}
	});
}
</script>
