<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jobs extends MX_Controller {

	function __construct() {
		parent::__construct();
		$ci =& get_instance();
		$siteLang = $ci->session->userdata('language');
		$this->config->set_item('language', $siteLang);
		$this->lang->load(array("admin","common","jod_credit","hq_manager","job"),$siteLang);
		$this->load->library(array('admin_template','head'));
		$this->load->model(array('jobs_model'));
		$this->session_check->checkSession();
		
		$this->load->library('excel');
		
	}

	function job_section() { // function to load all the open jobs to A.M./O.M. which is created by them
		$this->load->library('pagination');
		$limit= 12;
		$pageNo = 1;
		if ($this->input->get('per_page')!="" && $this->input->get('per_page')!==FALSE)
			$pageNo = $this->input->get('per_page');
			$this->load->library('paging_library');
			$pagingRecord = array('url'=>base_url().'jobs/job_section?','count'=>$this->jobs_model->get_all_jobs('','count','',''),'per_page'=>$limit);
			$data['pagination']   = $this->paging_library->get_config_paging($pagingRecord);
			if($this->input->post('job_title') == null ){
				$data['records'] = $this->jobs_model->get_all_jobs('','result',$pageNo,$limit);
			}else{
				$search = $this->input->post('job_title');
				$data['records'] = $this->jobs_model->get_all_jobs('','result',$pageNo,$limit,$search );
			}
		$this->admin_template->load('admin_template','all_jobs',$data);
	}

	

	function open_job_section() { // function to load all the jobs to hq manager in company and all the open jobs to A.M./O.M. which is created by them
		$this->load->library('pagination');
		$limit= 10;
		$pageNo = 1;
		if ($this->input->get('per_page')!="" && $this->input->get('per_page')!==FALSE)
			$pageNo = $this->input->get('per_page');
		$this->load->library('paging_library');
			$pagingRecord = array('url'=>base_url().'jobs/open_job_section?','count'=>$this->jobs_model->get_open_jobs('','count','',''),'per_page'=>$limit);
			$data['pagination']   = $this->paging_library->get_config_paging($pagingRecord);
		$data['records'] = $this->jobs_model->get_open_jobs('','result',$pageNo,$limit);
		//echo '<pre>';print_r($data['records']);die;
		$this->admin_template->load('admin_template','open_jobs',$data);
	}

	function active_job_section() { // function to load all the active jobs create by A.M./O.M.
		$this->load->library('pagination');
		$limit= 10;
		$pageNo = 1;
		if ($this->input->get('per_page')!="" && $this->input->get('per_page')!==FALSE)
			$pageNo = $this->input->get('per_page');
		$this->load->library('paging_library');
			$pagingRecord = array('url'=>base_url().'jobs/active_job_section?','count'=>$this->jobs_model->get_active_jobs('','count','',''),'per_page'=>$limit);
			$data['pagination']   = $this->paging_library->get_config_paging($pagingRecord);

		$data['records'] = $this->jobs_model->get_active_jobs('','result',$pageNo,$limit);
		$this->admin_template->load('admin_template','active_jobs',$data);
	}

	function job_history_section() { // function to load all the complete jobs create by A.M./O.M. or job history
		$this->load->library('pagination');
		$limit= 10;
		$pageNo = 1;
		if ($this->input->get('per_page')!="" && $this->input->get('per_page')!==FALSE)
			$pageNo = $this->input->get('per_page');
			$this->load->library('paging_library');
			$pagingRecord = array('url'=>base_url().'jobs/job_history_section?','count'=>$this->jobs_model->get_complete_jobs('','count','',''),'per_page'=>$limit);
			$data['pagination']   = $this->paging_library->get_config_paging($pagingRecord);

		$data['records'] = $this->jobs_model->get_complete_jobs('','result',$pageNo,$limit);
		$this->admin_template->load('admin_template','complete_jobs',$data);
	}

	/**get_outlet_manager_by_outlet **/
	function get_template_details($job_template_id=''){
		if(empty($job_template_id)){
			$job_template_id = $this->input->post('job_template_id');
		}
		$companyId = LoginUserDetails('company_id');
		$records = $this->jobs_model->get_job_templates($companyId, $job_template_id);
		//$recordArray[] = $records->template_name;
		echo json_encode($records);
	}

	function job_form($id='',$isCopy='') {
		$ci =& get_instance();
		$site_language = $ci->session->userdata('language');
		if((isset($_POST['save'])) && $id=='' && $isCopy=='') {
			if($this->save_job()) {
				set_global_messages($this->lang->line('job_added_successfully'),'success');
				//redirect('jobs/job_section');
				redirect('jobs/open_job_section');
			}
		} else if(isset($_POST['save']) && isset($id) && $isCopy=='') {
			if($this->save_job('update',$id)) {
				set_global_messages($this->lang->line('job_updated_successfully'),'success');
				//redirect('jobs/job_section');
				redirect('jobs/open_job_section');
			}
		} else if(isset($_POST['save']) && isset($id) && isset($isCopy)) {
			if($this->save_job()) {
				set_global_messages($this->lang->line('job_copied_successfully'),'success');
				//redirect('jobs/job_section');
				redirect('jobs/open_job_section');
			}
		}
		if($id && $isCopy) {
			$data['records'] = $this->jobs_model->get_single_job($id,1);
			//print_r($data['records']); die;
			$data['id']=$id;
			$data['isCopy']=$isCopy;
		} else if($id) {
			$data['records'] = $this->jobs_model->get_open_jobs($id,'row','','');
			$data['id']=$id;
		} else {
			$data = '';
			$data['id']='';
		}
		$data['site_language'] = $site_language;
		$data['outlets'] = $this->jobs_model->get_enable_outlet();
		$data['jobTypes'] = $this->jobs_model->get_job_type();
		$companyId = LoginUserDetails('company_id');
		$data['jobTemplatesList'] = $this->jobs_model->get_job_templates($companyId);
		$this->admin_template->load('admin_template','job_form',$data);
	}

	function save_job($type = 'insert',$id=0) {
		if($id != 0) {
			$_POST['id'] = $id;
		}
		$this->form_validation->set_rules('job_title','Job Title','required|trim|max_length[255]');
		$this->form_validation->set_rules('job_type_id','Job Type','required|trim');
		$this->form_validation->set_rules('description','Job Description','required|trim');
		$this->form_validation->set_rules('special_instructions','Special Instructions','required|trim');
		$this->form_validation->set_rules('start_date','Start Date','required|trim');
		$this->form_validation->set_rules('start_time','Start Time','required|trim');
		$this->form_validation->set_rules('end_date','End Date','required|trim');
		$this->form_validation->set_rules('end_time','End TIme','required|trim');
		$this->form_validation->set_rules('payment_amount','Payment Amount','required|trim|numeric');
		$this->form_validation->set_rules('payment_mode','Payment Mode','required|trim');
		if(LoginUserDetails('role_id') == 5) { // If area manager creates the job, then this to parameter will come
			$this->form_validation->set_rules('outlet_id','Outlet','required|trim');
			$this->form_validation->set_rules('outlet_manager_id','Outlet Manager','required|trim');
		}
		if($this->form_validation->run() === FALSE) {
			return FALSE;
		}

		$data = array();
		$data['job_title']        	= $this->input->post('job_title');
		$data['job_type_id']     	= $this->input->post('job_type_id');
		$data['description']     	= $this->input->post('description');
		$data['special_instructions']     	= $this->input->post('special_instructions');
		$data['start_date']      	= $this->input->post('start_date');
		$data['start_time']      	= $this->input->post('start_time');
		$data['end_date']     		= $this->input->post('end_date');
		$data['end_time']     		= $this->input->post('end_time');
		$data['payment_amount']     = $this->input->post('payment_amount');
		$data['payment_mode']     	= $this->input->post('payment_mode');
		$data['required_NEA_certificate']    = $this->input->post('required_NEA_certification');
		$data['updated_at'] 		= CURRENT_TIME;
		if(LoginUserDetails('role_id') == 5) { // if job creator is area manager
			if($type != "update") {
				$data['job_created_by']		= 0; // (Its only to identify that job is created by area manager or outlet manager 0 ====> Area manager and 1=====> outlet Manager)
			}
			$data['outlet_id']     		= $this->input->post('outlet_id');
			$data['outlet_manager_id']  = $this->input->post('outlet_manager_id');
		} else if(LoginUserDetails('role_id') == 3) { // if job creator is outlet manager
			if($type != "update") {
				$data['job_created_by']		= 1;
			}
			$data['outlet_manager_id'] = LoginUserDetails('userid');
			$outletData = $this->jobs_model->get_outlet_id($data['outlet_manager_id']);
			$data['outlet_id'] 			= $outletData[0]->outlet_id;
		}
		$outletDetails = $this->jobs_model->get_outlet_data($data['outlet_id'] );
		
		if($type == 'insert') {
			if($outletDetails[0]->is_approval_required == 1){
			/* If job approval is required from the HQ Manager then set is_aproved tag zero while created job */
			$data['is_approved']   = 0;
			}else{
				/* If job approval is not required from the HQ Manager then set is_aproved tag one while created job */
				$data['is_approved']   = 1;
			}
			$data['created_by'] = LoginUserDetails('userid');
			$data['created_at'] = CURRENT_TIME;
			$id = $this->jobs_model->add_job($data);
			if($id){
					$status = $data['is_approved'] ;
					$this->send_email_notification($id,$status,$data['outlet_id']);
			}
		}
		else if ($type == 'update') {
			$return = $this->jobs_model->update_job($id, $data);
		}
		return $id;
	}

	/**get_outlet_manager_by_outlet **/
	function get_outlet_manager(){
		$outletId = $this->input->post('outletId');
		$outletManagerId = $this->input->post('outletManagerId');
		$response = '';
		$selected = '';
		$response = "<option value= 0 > No Outlet Manager</option>";
		$records = $this->jobs_model->get_outlet_manager_by_outlet($outletId);
		foreach($records as $record) {
			if(!empty($record)) {
				if($outletManagerId == $record->user_accounts_id) { $selected = ' selected="selected"'; }
				$response .= '<option value="'.$record->user_accounts_id.'"' ;
				$response .= $selected;
				$response .= '>'.$record->first_name.' '.$record->last_name.'</option>';
				$selected = '';
			}
		}
		//$response.= "<option value= 0 > No Outlet Manager</option>";
		echo $response;
	}

	/********	hq manager job notification section : start	************/
	function notification() {
		$userType=LoginUserDetails('role_id');
		$ci =& get_instance();
		$siteLang = $ci->session->userdata('language');
		if($userType == 2){
			$this->load->library('pagination');
			$limit= 10;
			$pageNo = 1;
			if ($this->input->get('per_page')!="" && $this->input->get('per_page')!==FALSE)
				$pageNo = $this->input->get('per_page');
			$this->load->library('paging_library');
				//$pagingRecord = array('url'=>base_url().'jobs/notification?','count'=>$this->jobs_model->get_notification('','count','',''),'per_page'=>$limit);
				$pagingRecord = array('url'=>base_url().'jobs/notification?','count'=>get_hq_manager_notification('count'),'per_page'=>$limit);
				$data['pagination']   = $this->paging_library->get_config_paging($pagingRecord);
			//~ $data['count'] = $this->jobs_model->get_notification('','count','','');
			//~ $data['records'] = $this->jobs_model->get_notification('','result',$pageNo,$limit);
			$data['count'] = get_hq_manager_notification('count');
			$data['records'] = get_hq_manager_notification('result',$pageNo,$limit);
			$data['site_language'] = $siteLang;
			$this->admin_template->load('admin_template','hq_notification',$data);
		}else{
			redirect('admin/not_authorize');
		}
	}

	/* @Created Date : 29 Sept 2015
	 * @Author : Kratika Jain
	 * @ Details : Function to update the status of the Job when it is approve or reject by HQ Manager
	 * and also to send notifications to related peoples of the JOB
	 * @params : jobid, status by HQ Manager */

	function update_job_status($jobId,$status){
		$data = array();
		if($status == 1){
			$data["is_approved"] = 1;
		}else{
			$data["is_approved"] = 2;
		}
		$update  = $this->jobs_model->update_job($jobId,$data);
		$url = base_url('jobs/notification');
		// send notification to outlet mangers and the applicants
		$emails = $this->send_notifications($jobId,$status);
		header("location: ".$url);
	}

	/* @Created Date : 29 Sept 2015
	 * @Author : Kratika Jain
	 * @ Details : Fuction to send notifications to related peoples of the JOB
	 * when its approved or rejected by HQ Manager
	 * @params : jobid , aprrove status */


	function send_notifications($jobId,$status){
		$jobdetails  = $this->jobs_model->get_outlet_manager_id($jobId);
		$area_manager = $this->jobs_model->get_area_manager_id($jobdetails->outlet_id);
		if(!empty($area_manager[0]->area_manager_id)){
			$ManagerDetails = $this->jobs_model->get_area_manager_details($area_manager[0]->area_manager_id);
			$this->send_email_notification_to_Area_manager($ManagerDetails,$jobdetails,$status);
		}
		$outletManagerId  = $jobdetails ->outlet_manager_id;
		$ManagerDetails  = getUserDetailsByID($outletManagerId);
		if(!empty($ManagerDetails)){
			$this->send_email_notification_to_outlet_manager($ManagerDetails,$jobdetails,$status);
		}
		// if job get approved then notification email will sent to the applicants
		if($status == 1) {
			$emails_data = array();
			$emails  = $this->jobs_model->get_applicants_email($jobId);
			foreach ($emails as $key => $value){
				//array_push($emails_data,$value->email_id);
				array_push($emails_data,$value->user_accounts_id);
			}
			if($jobdetails->is_approved == 1){
				$this->send_email_notification_to_applicants($emails_data,$jobdetails);
			}
		}
 	}

	/* @Created Date : 30 Sept 2015
	 * @Author 	: Kratika Jain
	 * @Details : Fuction to send notifications to applicants of the JOB
	 * when its approved by HQ Manager
	 * @params 	: emails_list , jobdetails status */

	function send_email_notification_to_applicants($emails_list,$jobdetails){
		if($jobdetails->is_approved == 1){
			$reason = "Opening";
			$job_role = $jobdetails->job_title;
			$body = "Jobs on Demand ogranization is looking for the candidates whose profile will match for the following requirements.";
			$receivers = array();
			foreach ($emails_list as $key => $value){
				$receivers[] = $value;
				//~ $emailData= array();
				//~ $emailData['recieverId'] = $value;
				//~ $emailData['subject'] = "Job ".$reason." Notification ";
				//~ $emailData['template'] = 'email_notifications';
				//~ $emailData['body'] = $body;
				//~ $emailData['job_role'] = $job_role;
				//~ $emailData['job_desc'] = $jobdetails->description;
				//send_email($emailData);
			}
			$emailData['subject'] = "Job ".$reason." Notification ";
			$insertData['job_id'] = $jobdetails->job_id;
			$insertData['sender_id'] = LoginUserDetails('userid');
			$insertData['sender_user_type'] = LoginUserDetails('role_id');
			$insertData['receiver_id'] = implode(',',$receivers);
			$insertData['receiver_user_type'] = '4';
			$insertData['purpose'] = 'applicant_notification';
			$insertData['subject'] = $emailData['subject'];
			$insertData['created_at'] = date('Y-m-d h:i:s');
			$insertData['updated_at'] = date('Y-m-d h:i:s');
			$insertData['is_mail_sent'] = '0';

			if($reason != "Opening"){
				$this->jobs_model->add_email_notification($insertData);
			}

			$insertData1['notification_type'] = 'job_opening';
			$insertData1['sender_id'] = LoginUserDetails('userid');
			$insertData1['job_id'] = $jobdetails->job_id;
			$insertData1['user_id'] = implode(',',$receivers);
			$insertData1['user_type'] = LoginUserDetails('role_id');
			$insertData1['status'] = '0';
			$insertData1['created_at'] = date('Y-m-d h:i:s');
			$insertData1['updated_at'] = date('Y-m-d h:i:s');
			$insertData1['is_delete'] = '0';
			$this->jobs_model->add_push_notification($insertData1);
		}
	}

	/* @Created Date : 30 Sept 2015
	 * @Author 	: Kratika Jain
	 * @Details : Fuction to send notifications to applicants of the JOB and HQ Manager if job is created by outlet Manager and don't need approval of the HQ manager
	 * when its approved by HQ Manager
	 * @params 	: emails_list , jobdetails status */

	function send_email_notification($jobId,$status,$outletId){
		$jobdetails  = $this->jobs_model->get_outlet_manager_id($jobId);
		$outletManagerId  = $jobdetails ->outlet_manager_id;
		$loggedUserId  = LoginUserDetails('userid');
		$ManagerDetails = getUserDetailsByID($loggedUserId);
		$HQManagerDetails = $this->jobs_model->get_HQ_Manager_Data($outletId);
		// Send email notification to HQ Manager

	// if job get approved then notification email will sent to the applicants
		if($status == 1) {
			$emails_data = array();
			$emails  = $this->jobs_model->get_applicants_email($jobId);
			foreach ($emails as $key => $value){
				//array_push($emails_data,$value->email_id);
				array_push($emails_data,$value->user_accounts_id);
			}
			if($jobdetails->is_approved == 1){
				$this->send_email_notification_to_applicants($emails_data,$jobdetails);
			}
		}else{
			$this->send_email_notification_to_HQ_manager($HQManagerDetails,$jobdetails,$ManagerDetails);
		}
	}




	/* @Created Date : 30 Sept 2015
	 * @Author 	: Kratika Jain
	 * @Details : Fuction to send notifications to applicants of the JOB
	 * when its approved by HQ Manager
	 * @params 	: emails_list , jobdetails status */

	function send_email_notification_to_outlet_manager($ManagerDetails,$jobdetails,$status){
		$ManagerProfileDetails = $this->jobs_model->get_outlet_manager_by_id($ManagerDetails->user_accounts_id);
		if($status == 1){
			$reason = "Approval";
			$body = "The job created by you for the ".$jobdetails->job_title." having job Id ".$jobdetails->job_id." is approved by HQ Mangaer.";
		}else{
			$reason = "Rejection";
			$body = "The job created by you for the ".$jobdetails->job_title." having job Id ".$jobdetails->job_id." is rejected by HQ Mangaer. Please contact to Head Quater Manager for further queries. ";
		}
		$emailData= array();
		$emailData['recieverId'] = $ManagerProfileDetails->email_id;
		$emailData['subject'] = "Job ".$reason." Notification ";
		$emailData['template'] = 'job_notifications';
		$emailData['body'] = $body;
		$emailData['job_id'] = $jobdetails->job_id;
		$emailData['outlet_manager_name'] = $ManagerProfileDetails->first_name." ".$ManagerProfileDetails->last_name;

		$insertData['job_id'] = $jobdetails->job_id;
		$insertData['sender_id'] = LoginUserDetails('userid');
		$insertData['sender_user_type'] = LoginUserDetails('role_id');
		$insertData['receiver_id'] = $ManagerDetails->user_accounts_id;
		$insertData['receiver_user_type'] = '3';
		$insertData['purpose'] = 'manager_notification';
		$insertData['subject'] = $emailData['subject'];
		$insertData['created_at'] = date('Y-m-d h:i:s');
		$insertData['updated_at'] = date('Y-m-d h:i:s');
		$insertData['is_mail_sent'] = '0';
		$this->jobs_model->add_email_notification($insertData);


		//send_email($emailData);
	}


	function send_email_notification_to_Area_manager($ManagerDetails,$jobdetails,$status){
		if($status == 1){
			$reason = "Approval";
			$body = "The job created by you for the ".$jobdetails->job_title." having job Id ".$jobdetails->job_id." is approved by HQ Mangaer.";
		}else{
			$reason = "Rejection";
			$body = "The job created by you for the ".$jobdetails->job_title." having job Id ".$jobdetails->job_id." is rejected by HQ Mangaer. Please contact to Head Quater Manager for further queries. ";
		}
		$emailData= array();
		$insertData= array();
		$emailData['subject'] = "Job ".$reason." Notification ";
		$emailData['template'] = 'job_notifications';
		$emailData['body'] = $body;
		$emailData['job_id'] = $jobdetails->job_id;

		$insertData['job_id'] = $jobdetails->job_id;
		$insertData['sender_id'] = LoginUserDetails('userid');
		$insertData['sender_user_type'] = LoginUserDetails('role_id');
		$insertData['receiver_id'] = $ManagerDetails->user_accounts_id;
		$insertData['receiver_user_type'] = '5';
		$insertData['purpose'] = 'manager_notification';
		$insertData['subject'] = $emailData['subject'];
		$insertData['created_at'] = date('Y-m-d h:i:s');
		$insertData['updated_at'] = date('Y-m-d h:i:s');
		$insertData['is_mail_sent'] = '0';
		//print_r($insertData);die;
		$this->jobs_model->add_email_notification($insertData);


		//send_email($emailData);
	}


	/* @Created Date : 30 Sept 2015
	 * @Author 	: Kratika Jain
	 * @Details : Fuction to send notifications to HQ Manager when job is created by HQ Manager
	 * when its approved by HQ Manager
	 * @params 	: emails_list , jobdetails status */

	function send_email_notification_to_HQ_manager($HQManagerDetails,$jobdetails,$ManagerDetails){
		$reason = "Approval";
		$body = "One new job created by ".LoginUserDetails("user_first_name")." ".LoginUserDetails("user_last_name")." for the ".$jobdetails->job_title." having job Id ".$jobdetails->job_id.".";

		$emailData= array();
		//$emailData['recieverId'] = $HQManagerDetails->email_id;
		$emailData['subject'] = "Job ".$reason." Notification ";
		//$emailData['template'] = 'job_notifications_HQ_manager';
		//$emailData['body'] = $body;
		//$emailData['job_id'] = $jobdetails->job_id;
		//$emailData['HQ_Manager_name'] = $HQManagerDetails->first_name." ".$HQManagerDetails->last_name;

		$insertData['job_id'] = $jobdetails->job_id;
		$insertData['sender_id'] = LoginUserDetails('userid');
		$insertData['sender_user_type'] = LoginUserDetails('role_id');
		$insertData['receiver_id'] = $HQManagerDetails->user_accounts_id;
		$insertData['receiver_user_type'] = '2';
		$insertData['purpose'] = 'manager_notification';
		$insertData['subject'] = $emailData['subject'];
		$insertData['created_at'] = date('Y-m-d h:i:s');
		$insertData['updated_at'] = date('Y-m-d h:i:s');
		$insertData['is_mail_sent'] = '0';
		$this->jobs_model->add_email_notification($insertData);
		 //send_email($emailData);
	}

	/********	hq manager job notification section : end	************/

	/**************************** hq manager job template section start *************************************/
	function job_template($id=""){
		$ci =& get_instance();
		$site_language = $ci->session->userdata('language');
		$userType=LoginUserDetails('role_id');
		if($userType == 2){
			if((isset($_POST['save'])) && $id=='') {
				if($this->save_job_template()) {
					set_global_messages($this->lang->line('template_updated_successfully'),'success');
					redirect('jobs/job_template_section');
				}
			}else if(isset($_POST['save'])) {
				if($this->save_job_template('update',$id)) {
					set_global_messages($this->lang->line('template_added_successfully'),'success');
					redirect('jobs/job_template_section');
				}
			}
			if($id) {
				$data['records'] = $this->jobs_model->fetch_template_by_id($id);
				$data['id']=$id;
			} else {
				$data = '';
				$data['id']='';
			}
			$data['site_language'] = $site_language;
			$data['jobTypes'] = $this->jobs_model->get_job_type();
			$this->admin_template->load('admin_template','template_form',$data);
		}else{
			redirect('admin/not_authorize');
		}
	}

	/* This function is to save the template */

	function save_job_template($type = 'insert',$id=0){
		if($id != 0) {
			$_POST['id'] = $id;
		}
		$this->form_validation->set_rules('template_name','Template Name','required|trim|max_length[255]');
		$this->form_validation->set_rules('job_title','Job Title','required|trim|max_length[255]');
		$this->form_validation->set_rules('job_type_id','Job Type','required');
		$this->form_validation->set_rules('description','Job Description','required|trim');
		$this->form_validation->set_rules('start_date','Start Date','required|trim');
		$this->form_validation->set_rules('start_time','Start Time','required|trim');
		$this->form_validation->set_rules('end_date','End Date','required|trim');
		$this->form_validation->set_rules('end_time','End TIme','required|trim');
		$this->form_validation->set_rules('payment_amount','Payment Amount','required|trim|numeric');
		$this->form_validation->set_rules('payment_mode','Payment Mode','required|trim');
		$this->form_validation->set_rules('special_instructions','Special Instructions','required|trim');
		if($this->form_validation->run() === FALSE) {
			return FALSE;
		}

		$data = array();
		$data['template_name']      		= $this->input->post('template_name');
		$data['job_title']     				= $this->input->post('job_title');
		$data['job_type_id']     			= $this->input->post('job_type_id');
		$data['job_description']     		= $this->input->post('description');
		$data['start_date']      			= $this->input->post('start_date');
		$data['start_time']      			= $this->input->post('start_time');
		$data['end_date']     				= $this->input->post('end_date');
		$data['end_time']     				= $this->input->post('end_time');
		$data['payment_amount']    			= $this->input->post('payment_amount');
		$data['payment_mode']     			= $this->input->post('payment_mode');
		$data['special_instructions']     	= $this->input->post('special_instructions');
		$data['required_NEA_certificate']   = $this->input->post('required_NEA_certificate');
		$data['company_id']   = LoginUserDetails('company_id');
		if($type == 'insert') {
			$data['created_by'] = LoginUserDetails('userid');
			$data['created_at'] = CURRENT_TIME;
			$data['updated_at'] = CURRENT_TIME;
			$data['status'] 	= 1;
			$id = $this->jobs_model->add_templates($data);
		}
		else if ($type == 'update') {
			$return = $this->jobs_model->update_templates($id, $data);
		}
		return $id ;
	}

	function job_template_section(){
		$data = array();
		$userType=LoginUserDetails('role_id');
		$ci =& get_instance();
		$site_language = $ci->session->userdata('language');
		$data['site_language'] = $site_language;
		if($userType == 2){
			$this->config->set_item('site_title', 'Job Template');
			$this->admin_template->load('admin_template','job_template',$data);
		}else{
			redirect('admin/not_authorize');
		}
	}

	public function jobTemplateListByAjax(){
		$search = "";
		$search_set = 0;
		$dirs = "";
		$column_name  ="";
		if(isset($_GET['order'][0]['dir']) && !empty($_GET['order'][0]['dir'])) {
			$dirs = $_GET['order'][0]['dir'];

		}
		if($_GET['order'][0]['column'] == 0) {
			$dirs = 'ASC';

		}
		if(isset($_GET['order'][0]['column']) && !empty($_GET['order'][0]['column'])){
			if($_GET['order'][0]['column'] == 1) {
				$column_name = "jod_templates.template_name";
			}
			if($_GET['order'][0]['column'] == 2) {
				$column_name = "jod_templates.job_title";
			}
			if($_GET['order'][0]['column'] == 3) {
				$column_name = "jod_templates.job_description";
			}
		}
		if(isset($_GET['search']['value']) && !empty($_GET['search']['value'])) {
			$search = $_GET['search']['value'];
		    $search = trim($search);
		    $search_set = 1 ;
		}
		$records = $this->jobs_model->get_templates($search,$_GET['length'],$_GET['start'],$dirs,$column_name);
		$all_records = $this->jobs_model->get_templates();
		$total_records = count($all_records);
		$records_filtered = $total_records;
		if(!empty($records) || !empty($all_records)) {
			$messages      = array();
			$i=0;
			$s = $_GET['start'] + 1;
			if($search_set == 1){
				$records_filtered	= count($records);
			}
			$content='[';
			foreach($records as $record) {
				if($record->status==0){
					$class = "enable";
					$name = $this->lang->line('enable');
					$fa = "fa fa-check";
					$trClass = "gray-box";
					$title = "Click to enable record";
				}else if($record->status==1) {
					$class = "disable";
					$name = $this->lang->line('disable');
					$fa = "fa fa-ban";
					$trClass = "";
					$title = "Click to disable record";
				}
				$content .='[';
				$messages[$i][0] = "";
				$messages[$i][1] =  $record->template_name;
				$messages[$i][2] =  $record->job_title;
				$messages[$i][3] =  $record->job_description;
				$messages[$i][4] = '<a href="'.base_url('jobs/job_template/'.$record->template_id).'"><span><i class="fa fa-pencil"></i></span><span>'.$this->lang->line('edit').'</span></a>';
				$messages[$i][5] = '<a href="javascript:;" title="'.$title.'" onclick="myFunction('.$record->template_id.','.$record->status.')"><span><i class="'.$fa.'"></i></span><span>'.$name.'</span></a>';
				$messages[$i][6] = $trClass;
				$messages[$i][7] = $class;
				$i++;
				$s++;
			}
			$content .= ']';
			$final_data = json_encode($messages);
		}else{
			$final_data = "{}";
			$total_records = 0;
			$records_filtered = 0;
		}
		echo '{"draw":'.$_GET['draw'].',"recordsTotal":'.$total_records.',"recordsFiltered":'.$records_filtered.',"data":'.$final_data.'}';
		die;
	}

	public function job_template_status($id='',$status='') {
		if($status == 1) {
			$data['status'] = 0;
		} else if($status == 0) {
			$data['status'] = 1;
		}
		$data['updated_at'] = CURRENT_TIME;
		$this->jobs_model->update_templates($id, $data);
		set_global_messages($this->lang->line('template_status_updated_successfully'),'success');
		redirect('jobs/job_template_section');
	}

		/**************************** hq manager job template section End *************************************/

	/********	Applied Applicant List : start	************/
	/*
	 * candidate_type = 1 - hired
	 * candidate_type = 2 - rejected
	 * candidate_type = 3 - pending
	 * candidate_type = 4 - canceled
	 * */
	public function get_applied_applicant($job_id='', $candidate_type=3){
		//echo $candidate_type ; die;
		$data = array();
		$job_details = $this->jobs_model->get_job_details($job_id);
		//echo "<pre>"; print_r($job_details); die;
		if(!empty($job_id)){
			$data['applicants'] = $this->jobs_model->get_applied_applicants($job_id, $candidate_type);
			$data['is_applicants_hired'] = $job_details->is_applicants_hired;
		}
		//print_r($data['applicants']); die;
		if(LoginUserDetails('role_id') == 3){  // outlet_manager
			if($candidate_type == 4){
				$this->admin_template->load('admin_template','applied_applicant_area_manager',$data);
			}else{
				$this->admin_template->load('admin_template','applied_applicant',$data);
			}

		}elseif(LoginUserDetails('role_id') == 5 ){ // area_manager
			if(manage_job_access($job_id) == true){
				if($candidate_type == 4){
				$this->admin_template->load('admin_template','applied_applicant_area_manager',$data);
				}else{
					$this->admin_template->load('admin_template','applied_applicant',$data);
				}
			}
		}elseif(LoginUserDetails('role_id') == 2){ // HQ manager
			$this->admin_template->load('admin_template','applied_applicant_area_manager',$data);
		}
	}
	public function get_rejected_applicant($job_id){
		$data["applicants"] = $this->jobs_model->get_rejected_applicant($job_id);
		$this->admin_template->load('admin_template','rejected_applicants',$data);
	}

	function get_hired_applicants($job_id){
		$res['hired_applicants'] = $this->jobs_model->get_hired_applicants($job_id,"result");
		$this->admin_template->load('admin_template','hired_applicant_area_manager',$res);
	}
	/********	Applied Applicant List : end	************/


	/********	Job Details: start	************/
	public function job_detail($job_id = "",$status=""){
		$ci =& get_instance();
		$siteLang = $ci->session->userdata('language');
		$data = array();
		if(!empty($job_id)){
			$data['job_details'] = $this->jobs_model->get_job_details($job_id);
			$data['site_language'] = $siteLang;
 			if($status == "1" && $data['job_details']->is_delete != 1){
				// go to open jobs
				$data['applied_candidates'] = $this->jobs_model->get_applied_applicants($job_id);
				$this->admin_template->load('admin_template','open_job_detail',$data);
			}elseif($status == "2" && $data['job_details']->is_delete != 1){
				// go to Active jobs
				$outlet_details = getOutletDetail($data['job_details']->outlet_id);
				$data['is_template_required'] = $outlet_details->is_template_required;
				$data['applied_candidates'] = $this->jobs_model->get_hired_applicants($job_id,"result");
				$this->admin_template->load('admin_template','job_detail',$data);
			}else{
				$data['applied_candidates'] = $this->jobs_model->get_applied_applicants($job_id,"result");
				$data['hired_candidates'] = $this->jobs_model->get_hired_applicants($job_id,"result");
				$this->admin_template->load('admin_template','job_history_detail',$data);
			}
		}


	}
	/********	Job Details: end	************/


	/********	dashboard: start	************/
	public function dashboard(){
		$data = array();
		$data['no_of_open_jobs'] = $this->jobs_model->get_job_count('open');
		$date = date('Y-m-d');
		$data['no_of_heired_applicants'] = $this->jobs_model->get_hired_applicants_count($date);
		//$data['applied_candidates'] = $this->jobs_model->get_applied_applicants($job_id);
		//echo '<pre>'; print_r($data);die;
	}
	/********	dashboard: end	************/

	public function changeCandidateJobStatus($job_id=''){
		if(!empty($job_id)){
			$temp_job_id = $job_id;
			$candidate_ids = array();
			foreach($_POST['candidate_ids'] as $cid){
				if(!empty($cid)){
					$candidate_ids[] = $cid;
				}
			}

			if(!empty($candidate_ids)){
				$cids =  implode(',', $candidate_ids);
				/* Added by Kratika for
				 * the auto reject those jobs of the applicants for which they have
				 * already applied and have the Time overlap */
				$this->system_auto_rejection($cids,$job_id);
				//echo $cids ; die;
				$data = $this->jobs_model->getJobStatusForHired($job_id);
				if(empty($data) == true) {
					$this->jobs_model->updateJobStatusForHired($job_id);
					$is_change = $this->jobs_model->changeCandidateJobStatus($job_id, $cids, date('Y-m-d H:i:s'));
					//$is_change = true;
					if($is_change){
						$applicants = $this->jobs_model->get_all_jobs_applicants($job_id);
						$hiredApplicants = array();
						$rejectedApplicants = array();

						foreach($applicants as $applicant){
							// $applicant->status = 1;
							if($applicant->status=='1'){
								$hiredApplicants[] = $applicant->applicant_user_account_id;
							}else if($applicant->status=='2' && $applicant->is_delete == 0 ){
								$rejectedApplicants[] = $applicant->applicant_user_account_id;
							}
						}
						if(!empty($hiredApplicants)){

							$insertData['job_id'] = $job_id;
							$insertData['sender_id'] = LoginUserDetails('userid');
							$insertData['sender_user_type'] = LoginUserDetails('role_id');
							$insertData['receiver_id'] = implode(',',$hiredApplicants);
							$insertData['receiver_user_type'] = '4';
							$insertData['purpose'] = 'applicant_hired';
							$insertData['subject'] = "Hired for Job";
							$insertData['created_at'] = date('Y-m-d h:i:s');
							$insertData['updated_at'] = date('Y-m-d h:i:s');
							$insertData['is_mail_sent'] = '0';
							$this->jobs_model->add_email_notification($insertData);

							$insertData1['notification_type'] = 'applicant_hired';
							$insertData1['sender_id'] = LoginUserDetails('userid');
							$insertData1['job_id'] = $job_id;
							$insertData1['user_id'] = implode(',',$hiredApplicants);
							$insertData1['user_type'] = LoginUserDetails('role_id');
							$insertData1['status'] = '0';
							$insertData1['created_at'] = date('Y-m-d h:i:s');
							$insertData1['updated_at'] = date('Y-m-d h:i:s');
							$insertData1['is_delete'] = '0';
							$this->jobs_model->add_push_notification($insertData1);
						}
						if(!empty($rejectedApplicants)){
							$insertData['job_id'] = $job_id;
							$insertData['sender_id'] = LoginUserDetails('userid');
							$insertData['sender_user_type'] = LoginUserDetails('role_id');
							$insertData['receiver_id'] = implode(',',$rejectedApplicants);
							$insertData['receiver_user_type'] = '4';
							$insertData['purpose'] = 'applicant_rejected';
							$insertData['subject'] = "Rejected for Job";
							$insertData['created_at'] = date('Y-m-d h:i:s');
							$insertData['updated_at'] = date('Y-m-d h:i:s');
							$insertData['is_mail_sent'] = '0';
							$this->jobs_model->add_email_notification($insertData);

							$insertData2['notification_type'] = 'applicant_rejected';
							$insertData2['sender_id'] = LoginUserDetails('userid');
							$insertData2['job_id'] = $job_id;
							$insertData2['user_id'] = implode(',',$rejectedApplicants);
							$insertData2['user_type'] = LoginUserDetails('role_id');
							$insertData2['status'] = '0';
							$insertData2['created_at'] = date('Y-m-d h:i:s');
							$insertData2['updated_at'] = date('Y-m-d h:i:s');
							$insertData2['is_delete'] = '0';
							$this->jobs_model->add_push_notification($insertData2);

						}
						set_global_messages($this->lang->line('job_started_successfully'),'success');
					}else{
						set_global_messages($this->lang->line('applicant_already_hired'),'error');
					}
				}else{
					set_global_messages($this->lang->line('job_not_started'),'error');
				}
			}else{
				set_global_messages($this->lang->line('select_atleast_one_candidate'),'error');
			}
		}else{
			set_global_messages($this->lang->line('job_not_started'),'error');
		}
		redirect('jobs/job_detail/'.$job_id.'/2');
	}

	public function system_auto_rejection($applicants_ids,$job_id){
		$job_details = $this->jobs_model->get_job_details($job_id);
		$applicant_id_arr  = explode(",",$applicants_ids);
		foreach($applicant_id_arr as $candidate_id){
			$candidate_jobs = $this->jobs_model->reject_otherjobs_applicants($candidate_id,$job_details);
			if(!empty($candidate_jobs)){
				foreach($candidate_jobs as $candidate_job_detail){
					//print_r($candidate_job_detail);
					// update candidate record
					$this->jobs_model->system_cancellation($candidate_job_detail->job_id,$candidate_job_detail->applicant_user_account_id);
				}
			}
		}
	}


	public function job_feedback($job_id='',$type=""){
		$res = array();
		/* applicant details who aknowledged */
		$res['applicants'] 					= $this->jobs_model->get_hired_applicants($job_id,"result","at_feedback");
		/*applicants  count who acknowledged */
		$res['acknowledged_applicant_count']= $this->jobs_model->get_hired_applicants($job_id,"count");
		/* applicants count who have not acknowledged */
		$res["not_ack_applicants"] 			= $this->jobs_model->get_not_acknowleged_applicants($job_id,"not_ack");

		/* Job details */
		$res['job_details'] = $this->jobs_model->get_job_details($job_id);
		/* find applicants who had acknowledged but not do clock in */
		$res['applicants_acknowledged_but_not_clock_in'] = $this->get_applicants_acknowledged_but_not_clock_in($res['applicants']);
		/* fetch applied applicants count */
		$res['applied_candidates_count']			= getAppliedCandidate($job_id);

		/* cancel appliants count */
		$res['cancel_applicant_count']  	= $this->jobs_model->get_hired_applicants($job_id,"count","cancel",1);
		/* Rejected appliants count */
		$rejected_applicants = $this->jobs_model->rejected_applicants($job_id);
		$res['rejected_applicants']  = !empty($rejected_applicants) ?  count($rejected_applicants): 0;


		$res['applicant_clock_in_not_clock_out'] = $this->jobs_model->get_clockin_not_clock_out_applicants($job_id);
		
		//echo "all applied appliants  =>  ".$res['applied_candidates_count']."   Cancel Applicants ===> ".$res['cancel_applicant_count']. "  Rejected  appliants count ===>". $res['rejected_applicants'];  die;
		$res['all_rejected']  = false;
		if($res['rejected_applicants'] == $res['applied_candidates_count'] ){
			// all applicants are either rejected or system cacelled
			$res['all_rejected'] = true;
		}
		if(($res['rejected_applicants'] + $res['cancel_applicant_count']) == $res['applied_candidates_count'] ){
			$res['all_rejected'] = true;
			// all appliants either self cancelled or rejected by system or by manager
		}
		/* check job is not already completed */
		if($res['job_details']->is_completed == 1){
			/* redirect on the job details page */
			redirect('jobs/job_detail/'.$job_id);
		}
		if($type == 1 && $res['job_details']->is_completed != 1){
			
			if(isset($_REQUEST['save'])) {
				/* when form get submitted */
			
				$form_response = $this->save_feedback($job_id);
				if($form_response["message_type"] == "error") {
					set_global_messages($form_response["message"],$form_response["message_type"]);
				}else{
					set_global_messages($form_response["message"],$form_response["message_type"]);
					redirect('jobs/job_history_section');
				}
			}else{
				set_global_messages($this->lang->line('job_already_completed'),"error");
				redirect('jobs/job_history_section');
			}
		}else{
			/* redirect if job is completed */
			if($res['job_details']->is_completed == 1){
				set_global_messages($this->lang->line('job_already_completed'),"error");
				redirect('jobs/job_history_section');
			}
		}
		$this->admin_template->load('admin_template','job_feedback',$res);
	}

	public function get_applicants_acknowledged_but_not_clock_in($applicants_arr){
		$count = 0;
		foreach($applicants_arr as $applicant_data){
			if($applicant_data->clockInOutStatus == 0 && $applicant_data->acknowledged == 1 && $applicant_data->is_delete == 0 && $applicant_data->status == 1){
				$count = $count + 1;
			}

		}
		return $count;
	}
	public function user_profile($applicant_id=''){
		$data = array();
		$data['applicant_profile'] = $this->jobs_model->applicant_profile($applicant_id);
		$data['applicants_active_job'] = $this->jobs_model->applicants_active_job($applicant_id);
		$data['applicants_open_job'] = $this->jobs_model->applicants_open_job($applicant_id);
		$data['cancel_compelted_jobs'] = $this->jobs_model->applicants_cancel_or_compeleted_jobs($applicant_id);
		$data['applicants_feedback']  = applicant_feedback_data($applicant_id);
		$this->admin_template->load('admin_template','user_profile',$data);
	}


	public function save_feedback($job_id){

		$result_array = array();
		//$this->form_validation->set_rules('check_in_time[]','Check In Time','required|trim|max_length[255]');
		//$this->form_validation->set_rules('check_out_time[]','Check out Time','required|trim|max_length[255]');
		//$this->form_validation->set_rules('amount[]','Amount','required|trim|max_length[255]');
		$this->form_validation->set_rules('additional_comment[]','Additional Comment','required|trim|max_length[255]');
		if($this->form_validation->run() === FALSE) {
			$result_array["message_type"] = "error";
			$result_array["message"]  = validation_errors();
			return $result_array;
		}

		$check_in_time 	= $this->input->post("check_in_time");
		$check_out_time = $this->input->post("check_out_time");
		$amount 		= $this->input->post("amount");
		$additional_comment = $this->input->post("additional_comment");
		$rating_arr = $this->input->post("rating");
		$applicant_id = $this->input->post("applicant_id");
		$applicants_count = $this->input->post("applicants_count");
		//echo $applicants_count; die;
		for($i=0; $i<$applicants_count; $i++ ){
			$data = array();
			$data["clock_in_time"] 	= $check_in_time[$i];
			$data["clock_out_time"] = $check_out_time[$i];
			$data["total_Amout"] 	= $amount[$i];
			$data["additional_comments"] = $additional_comment[$i];
			$data["applicant_user_account_id"] = $applicant_id[$i];
			$data["job_id"] = $job_id;
			$data["manager_id"]   = LoginUserDetails('userid');
			$data["created_at"]   = date('Y-m-d h:i:s');
			$rating = $rating_arr[$i];
			$data["rating"]   = $rating;
			if(empty($this->jobs_model->check_applicant_feedback_data($applicant_id[$i],$job_id,LoginUserDetails('userid')))){
				$this->jobs_model->save_feedback($data);
			}
			$insertData['job_id'] = $job_id;
			$insertData['sender_id'] = LoginUserDetails('userid');
			$insertData['sender_user_type'] = LoginUserDetails('role_id');
			$insertData['receiver_id'] = $applicant_id[$i];
			$insertData['receiver_user_type'] = '4';
			$insertData['purpose'] = 'manager_feedback';
			$insertData['subject'] = "Manager feedback";
			$insertData['created_at'] = date('Y-m-d h:i:s');
			$insertData['updated_at'] = date('Y-m-d h:i:s');
			$insertData['is_mail_sent'] = '0';
			$this->jobs_model->add_email_notification($insertData);

			$insertData1['notification_type'] = 'manager_feedback';
			$insertData1['sender_id'] = LoginUserDetails('userid');
			$insertData1['job_id'] = $job_id;
			$insertData1['user_id'] = $applicant_id[$i];
			$insertData1['user_type'] = LoginUserDetails('role_id');
			$insertData1['status'] = '0';
			$insertData1['created_at'] = date('Y-m-d h:i:s');
			$insertData1['updated_at'] = date('Y-m-d h:i:s');
			$insertData1['is_delete'] = '0';
			$this->jobs_model->add_push_notification($insertData1);

			$update_appliants = $this->jobs_model->update_applicants_job($job_id,$applicant_id[$i]);
		}
		$not_ack_applicants = $this->jobs_model->get_not_acknowleged_applicants($job_id,"not_ack_cancel");
		if(!empty($not_ack_applicants)){
			foreach($not_ack_applicants as $applicant){
				$this->jobs_model->reject_candidate($job_id,$applicant->applicant_user_account_id);
			}
		}
		$complete_job = $this->jobs_model->compelete_job($job_id);
		if($complete_job == true){
			$result_array["message_type"] = "Success";
			$result_array["message"]   = "Job is completed successfully.";
		}
		return $result_array;
	}
	
	/* Function added by kratika on date 04 Jan 2017 for adding the feedback of the applicant (New Updated) */
	public function save_applicant_feedback($applicant_id,$job_id,$clock_in_out_data,$rating,$feedback_text){
			$data = array();
			$data["clock_in_time"] 	= $clock_in_out_data['applicant_clockIn_dateTime'];
			$data["clock_out_time"] = $clock_in_out_data['clockout_current_dateTime'];
			$data["total_Amout"] 	= 0;
			$data["additional_comments"] = $feedback_text;
			$data["applicant_user_account_id"] = $applicant_id;
			$data["job_id"] = $job_id;
			$data["manager_id"]   = LoginUserDetails('userid');
			$data["created_at"]   = date('Y-m-d h:i:s');
			$data["rating"]   = $rating;
			if(empty($this->jobs_model->check_applicant_feedback_data($applicant_id,$job_id,LoginUserDetails('userid')))){
				$this->jobs_model->save_feedback($data);
				
				$insertData['job_id'] = $job_id;
				$insertData['sender_id'] = LoginUserDetails('userid');
				$insertData['sender_user_type'] = LoginUserDetails('role_id');
				$insertData['receiver_id'] = $applicant_id;
				$insertData['receiver_user_type'] = '4';
				$insertData['purpose'] = 'manager_feedback';
				$insertData['subject'] = "Manager feedback";
				$insertData['created_at'] = date('Y-m-d h:i:s');
				$insertData['updated_at'] = date('Y-m-d h:i:s');
				$insertData['is_mail_sent'] = '0';
				$this->jobs_model->add_email_notification($insertData);

				$insertData1['notification_type'] = 'manager_feedback';
				$insertData1['sender_id'] = LoginUserDetails('userid');
				$insertData1['job_id'] = $job_id;
				$insertData1['user_id'] = $applicant_id;
				$insertData1['user_type'] = LoginUserDetails('role_id');
				$insertData1['status'] = '0';
				$insertData1['created_at'] = date('Y-m-d h:i:s');
				$insertData1['updated_at'] = date('Y-m-d h:i:s');
				$insertData1['is_delete'] = '0';
				$this->jobs_model->add_push_notification($insertData1);
			}
	}

	/*****------------------ Cancel job section start ---------------------******/
	public function cancel_job($job_id,$applicant_count){
		$job_details  = $this->jobs_model->get_job_details($job_id);
		if($job_details->is_delete == 0){
			$receivers = array();
			$this->jobs_model->delete_notification('email_notifications','job_id',$job_id,'is_mail_sent');
			$this->jobs_model->delete_notification('notification','job_id',$job_id,'status');

			$hq_manager = getHQManager();
			$insertData2['job_id'] = $job_id;
			$insertData2['sender_id'] = LoginUserDetails('userid');
			$insertData2['sender_user_type'] = LoginUserDetails('role_id');
			$insertData2['receiver_id'] = $hq_manager->user_accounts_id;
			$insertData2['receiver_user_type'] = '2';
			$insertData2['purpose'] = 'manager_notification';
			$insertData2['subject'] = "Job Rejection Notification ";
			$insertData2['created_at'] = date('Y-m-d h:i:s');
			$insertData2['updated_at'] = date('Y-m-d h:i:s');
			$insertData2['is_mail_sent'] = '0';
			$this->jobs_model->add_email_notification($insertData2);

			if($applicant_count > 0){
				$applied_candidates = $this->jobs_model->get_applied_applicants($job_id);
				foreach ($applied_candidates as $applicant){
					$receivers[] = $applicant->applicant_user_account_id;
					$update_candidate = $this->jobs_model->cancel_job_for_applied_candidates($applicant->job_id,$applicant->applicant_user_account_id);
				}
				$insertData['job_id'] = $job_id;
				$insertData['sender_id'] = LoginUserDetails('userid');
				$insertData['sender_user_type'] = LoginUserDetails('role_id');
				$insertData['receiver_id'] = implode(',',$receivers);
				$insertData['receiver_user_type'] = '4';
				$insertData['purpose'] = 'applicant_notification';
				$insertData['subject'] = "Job Rejection Notification ";
				$insertData['created_at'] = date('Y-m-d h:i:s');
				$insertData['updated_at'] = date('Y-m-d h:i:s');
				$insertData['is_mail_sent'] = '0';
				$this->jobs_model->add_email_notification($insertData);

				$insertData1['notification_type'] = 'job_rejection';
				$insertData1['sender_id'] = LoginUserDetails('userid');
				$insertData1['job_id'] = $job_id;
				$insertData1['user_id'] = implode(',',$receivers);
				$insertData1['user_type'] = LoginUserDetails('role_id');
				$insertData1['status'] = '0';
				$insertData1['created_at'] = date('Y-m-d h:i:s');
				$insertData1['updated_at'] = date('Y-m-d h:i:s');
				$insertData1['is_delete'] = '0';
				$this->jobs_model->add_push_notification($insertData1);
			}
			$update_job = $this->jobs_model->cancel_job($job_id);
			set_global_messages($this->lang->line('job_cancel_msg'),'success');
			redirect('jobs/open_job_section');
		}else{
			set_global_messages($this->lang->line('job_already_cancel_msg'),'success');
			redirect('jobs/job_history_section');
		}

	}
	/*****------------------ Cancel job section end ---------------------******/

	public function job_feedback_section($job_id){
		$data['applicants_feedback'] = $this->jobs_model->get_applicants_feedback($job_id);
		$this->admin_template->load('admin_template','job_feedback_screen',$data);
	}

	public function get_outlet_status(){
		$input_arr =  $this->input->post();
		$data = getOutletDetail($input_arr ['outlet_id']);
		echo json_encode($data);
	}

	public function job_compelete_feedback($job_id){
		$res['job_details'] = $this->jobs_model->get_job_details($job_id);
		if($res['job_details']->is_completed == 1){
			redirect('jobs/job_detail/'.$job_id);
		}else{
 
			$applicants = $this->jobs_model->get_not_acknowleged_applicants($job_id);
			foreach($applicants as $applicant){
				$this->jobs_model->reject_candidate($job_id,$applicant->applicant_user_account_id);
			}
			$complete_job = $this->jobs_model->compelete_job($job_id);
			set_global_messages($this->lang->line('job_completed_successfully'),"success");
			redirect('jobs/job_history_section');
		}
	}

	//Create clockin and out images - rsolanki
	function clockInCurrentTime()
	{
		$imgPath = dirname(__FILE__).'/../../../../qr_images/';
		$jobId 		 				= trim($this->input->post('job_id'));
		$applicant_user_account_id 	= trim($this->input->post('applicant_user_account_id'));
		$clock_in_hour = trim($this->input->post('clock_in_hour'));
		$clock_in_min = trim($this->input->post('clock_in_min'));
		$clock_out_hour = trim($this->input->post('clock_out_hour'));
		$clock_out_min = trim($this->input->post('clock_out_min'));
		$clock_in_date = trim($this->input->post('clock_in_date'));
		$clock_out_date = trim($this->input->post('clock_out_date'));
		$cdateTime 	 = date('Y-m-d H:i:s');
		
		/* added by kratika on date 04 Jan 2017 for the rating and feedback text */
		$rating  = trim($this->input->post('rating'));
		$feedback_comments  = trim($this->input->post('feedback_comments'));
		
		
		if($jobId!='' && $applicant_user_account_id!=''){
			//echo $jobId." - ".$applicant_user_account_id;
			//Get clock in out data
			$id = '';
			$applicant_clockIn_dateTime  = '';
			$applicant_clockIn_status 	 = '';
			$applicant_clockOut_dateTime = '';
			$applicant_clockOut_status   = '';
			$clockIn_img	=	'';
			$clockOut_img	=	'';
			$clockInOutData = $this->jobs_model->checkClockInOut($jobId,$applicant_user_account_id);
			
			$user_data = getUserDetailsByID($applicant_user_account_id);
			if(!empty($clockInOutData)){
				$id = $clockInOutData[0]->id;
				$applicant_clockIn_dateTime  = $clockInOutData[0]->applicant_clockIn_dateTime;
				$applicant_clockIn_status 	 = $clockInOutData[0]->applicant_clockIn_status;
				$applicant_clockOut_dateTime = $clockInOutData[0]->applicant_clockOut_dateTime;
				$applicant_clockOut_status   = $clockInOutData[0]->applicant_clockOut_status;
				$clockIn_img   				 = $clockInOutData[0]->clockIn_img;
				$clockOut_img  				 = $clockInOutData[0]->clockOut_img;
				$clockout_current_dateTime   = $clockInOutData[0]->clockout_current_dateTime;
			}
			//create QR image-
			$imgPath = '';
			if($applicant_clockIn_status =='1'){   
				$res = array();   //Check Clock in
				//Delete old image clockOut img
				$file_name = $imgPath.$clockOut_img;
				if (file_exists($file_name)) unlink($file_name);
					$meal_hours_data = $this->jobs_model->to_get_meal_hours($jobId,$applicant_user_account_id);
					//echo "<pre>". print_r($meal_hours_data); die;
					$updateData = array();
					$dt = new DateTime($applicant_clockIn_dateTime);
					$applicant_clock_in_date  = $dt->format('Y-m-d');
					if(strcmp($clock_in_date,"") != 0 && strcmp($clock_out_date,"") != 0){
						$updateData['clockout_current_dateTime'] = $clock_out_date." ".$clock_out_hour.":".$clock_out_min.":00";
						$updateData['applicant_clockIn_dateTime'] = $clock_in_date." ".$clock_in_hour.":".$clock_in_min.":00";
						$msg 	= $jobId."~".strtoupper($user_data->unique_id)."~".$updateData['clockout_current_dateTime']."~".$updateData['applicant_clockIn_dateTime']."~".$meal_hours_data->meal_hours;
						//echo $msg; die;
						$imgPath = $this->createQrImg($msg);
						$updateData['clockOut_img'] = $imgPath;
						$return = $this->jobs_model->update_clockInOut($jobId,$applicant_user_account_id,$updateData);
						
						/* added by kratika to add the feedback of the applicant on date 04 jan 2017 start */
						if(empty($this->jobs_model->check_applicant_feedback_data($applicant_user_account_id,$jobId,LoginUserDetails('userid')))){
							/* to save the feedback of the applicant */
							$this->save_applicant_feedback($applicant_user_account_id,$jobId,$updateData,$rating,$feedback_comments);
							$update_data = array();
							$update_data['is_feedback_given']  = 1 ;
							$this->jobs_model->update_bank_details($update_data,$applicant_user_account_id,$jobId);
						}
						/* date 04 jan 2017 end */
						$res["error"] = false;
						$res["image_path"] = $imgPath;
						echo json_encode($res); die;
					}else{
						$res["error"] = true;
						$res["error_msg"] = "Clock in and Clock out date should not be empty";
						echo json_encode($res); die;
					}
			}else{
				//Delete old image clockIn img
				if($clockIn_img!=''){
					$file_name = $imgPath.$clockIn_img;
					if (file_exists($file_name)) unlink($file_name);
				}
				//echo $cdateTime; die;

				$msg 	 = "Clock in current dateTime : ".$jobId."~".strtoupper($user_data->unique_id)."~".$cdateTime;
				$imgPath = $this->createQrImg($msg);
				//update or insert data
				if(!empty($clockInOutData)){
					$updateData = array();
					$updateData['clockIn_current_dateTime'] = $cdateTime;
					$updateData['clockIn_img'] = $imgPath;
					$return = $this->jobs_model->update_clockInOut($jobId,$applicant_user_account_id,$updateData);
				}else{
					$insertData 				= array();
					$insertData['job_id']	 	= $jobId;
					$insertData['applicant_user_account_id']= $applicant_user_account_id;
					$insertData['clockIn_current_dateTime'] = $cdateTime;
					$insertData['clockIn_img'] 	= $imgPath;
					$return = $this->jobs_model->add_clockInOut($insertData);
				}
				echo $imgPath;
				die();
			}
			
		}
	}



	//Create QR code - rsolanki
	function createQrImg($msg){
		//error_reporting(0);
		require_once("ws/models/libraries/Ciqrcode.php");
		$path = dirname(__FILE__).'/../../../../qr_images/';
		//$path = dirname(__FILE__).'/qr_images/';;
		//die();
		$name ="";
		if (!empty($msg)) {
			$name = md5(date('Y-m-d H:i:s')).uniqid().'.png';
			$params['data'] 	= $msg;
			$params['level'] 	= 'H';
			$params['size'] 	= 5;
			$params['savename'] = $path.$name;
			$qr = new Ciqrcode();
			$qr->generate($params);
			$file_name = $path.$name;
			chmod($file_name, 775); //added by rsolanki

			//$this->load->library('s3');
			//s3::setAuth(awsAccessKey,awsSecretKey);
		   // if (s3::putObjectFile($file_name, "39c.website",'qrcode/'.$name, s3::ACL_PUBLIC_READ)) {
				//if (file_exists($file_name)) unlink($file_name);
			//}
		}
		return $name;
	}

	/* @Method : To download Job history
	 * @Params :  -
	 * @Response : Download the Job History in the excel file
	 * @Author : Kratika Jain
	 * */

	public function download_job_history($outlet_id,$job_id,$applicant_user_accounts_id){
		$data = array();
		if($outlet_id != "" && $job_id != "" && $applicant_user_accounts_id !=""){
			$data[0] = get_job_details_data($outlet_id,$job_id,$applicant_user_accounts_id);
			/* If Clock in clock out details is adjusted by the SA then a new details get append in the excel sheet */
			$data_adjusted_by_SA = get_new_clock_in_out_details($job_id,$applicant_user_accounts_id,$outlet_id);
			if($data_adjusted_by_SA != false){
				$new_adjusted_data = array();
				$new_adjusted_data['date'] =  trim($data_adjusted_by_SA['payment_data']->clock_in_date);
				$new_adjusted_data['clock_out_date'] = trim($data_adjusted_by_SA['payment_data']->clock_out_date);
				$new_adjusted_data['outlet_name'] = $data[0]['outlet_name'];
				$new_adjusted_data['applicant_fullname'] = $data[0]['applicant_fullname'];
				$new_adjusted_data['unique_id'] = $data[0]['unique_id'];
				$new_adjusted_data['clock_in'] =  date("H:i", strtotime($data_adjusted_by_SA['payment_data']->clock_in_time));
				$new_adjusted_data['clock_out'] =  date("H:i", strtotime($data_adjusted_by_SA['payment_data']->clock_out_time));
				$new_adjusted_data['meal_break_time'] =  $data_adjusted_by_SA['payment_data']->meal_break_time;
				$new_adjusted_data['total_hours_worked'] =  convert_seconds_to_time(round(($data_adjusted_by_SA['payment_data']->total_hours_worked*3600))); 
				
				$new_adjusted_data['wages_per_hour'] =  $data_adjusted_by_SA['payment_data']->wages_per_hour;
				$new_adjusted_data['total_jod_credit'] =  $data_adjusted_by_SA['payment_data']->total_jod_credit;
				$new_adjusted_data['credit_before_transacion'] =  sprintf('%0.2f',$data_adjusted_by_SA['transaction_data']->credit_before_transacion);
				$new_adjusted_data['credit_after_transacion'] =  sprintf('%0.2f',$data_adjusted_by_SA['transaction_data']->credit_after_transacion);
				$data[1] = $new_adjusted_data;
			}
			//print_r($data); die;
			$job_details  = $this->jobs_model->get_job_details($job_id);
			$title = $job_details->job_title;
			$filename = "applicant_job_details"."_".strtotime("now").".xlsx";
		
			$this->generate_excel($data,$title,$filename);
		}
	}

	/* @Method : To genearte the  excel file for transaction history
	 * @Params :  transaction_history_data, title, filename
	 * @Response : create the excel sheet and then make it download
	 * @Author : Kratika Jain
	 */

	public function generate_excel($transaction_history_data,$title,$filename){
		$this->excel->setActiveSheetIndex(0);
		//name the worksheet
		//$this->excel->getActiveSheet()->setTitle($title);
		//set cell A1 content with some text

				$this->excel->getActiveSheet()->setCellValue('A1', $title);
				//$this->excel->getActiveSheet()->setCellValue('A3', 'S. No.');
				$this->excel->getActiveSheet()->setCellValue('A3', 'Clock In Date');
				$this->excel->getActiveSheet()->setCellValue('B3', 'Clock Out Date');
				$this->excel->getActiveSheet()->setCellValue('C3', 'Outlet Name');
				$this->excel->getActiveSheet()->setCellValue('D3', 'Full Name');
				$this->excel->getActiveSheet()->setCellValue('E3', 'NRIC/FIN');
				$this->excel->getActiveSheet()->setCellValue('F3', 'Clock In Time');
				$this->excel->getActiveSheet()->setCellValue('G3', 'Clock Out Time');
				$this->excel->getActiveSheet()->setCellValue('H3', 'Meal Break Time');
				$this->excel->getActiveSheet()->setCellValue('I3', 'Total Hours Worked');
				$this->excel->getActiveSheet()->setCellValue('J3', 'Wages Per Hour');
				$this->excel->getActiveSheet()->setCellValue('K3', 'Total JOD Credit');
				$this->excel->getActiveSheet()->setCellValue('L3', 'Before Deduction');
				$this->excel->getActiveSheet()->setCellValue('M3', 'After Deduction');

				//merge cell A1 until C1
				$this->excel->getActiveSheet()->mergeCells('A1:M1');


		//set aligment to center for that merged cell (A1 to C1)
		$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		 //make the font become bold
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(16);
		$this->excel->getActiveSheet()->getStyle('A1')->getFill()->getStartColor()->setARGB('#333');
		$this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(123);

		 for($col = ord('A'); $col <= ord('M'); $col++){
			//set column dimension
			$this->excel->getActiveSheet()->getColumnDimension(chr($col))->setAutoSize(true);
			$this->excel->getActiveSheet()->getRowDimension(1)->setRowHeight(-1);
			//change the font size
			$this->excel->getActiveSheet()->getStyle(chr($col))->getFont()->setSize(10);
			$this->excel->getActiveSheet()->getStyle(chr($col))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		}

		$exceldata = $transaction_history_data;
		//Fill data
		$this->excel->getActiveSheet()->fromArray($exceldata, null, 'A4');


		//$filename='PHPExcelDemo.xls'; //save our workbook as this file name
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache

		//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		//if you want to save it as .XLSX Excel 2007 format
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
		//force user to download the Excel file without writing it to server's HD
		$objWriter->save('php://output');
	}


	/* @Method : To get the clock in and clock out time
	 * @Params :  job_id, applicant_user_account_id
	 * @Response : return data arary
	 * @Author : Kratika Jain
	 */

	public function get_clockin_clokcout_detail(){
			$result_arr = array();
			$jobId 		 				= trim($this->input->post('job_id'));
			$applicant_user_account_id 	= trim($this->input->post('applicant_user_account_id'));
			$cdateTime 	 = date('Y-m-d H:i:s');
		
			if($jobId!='' && $applicant_user_account_id!=''){
				$clockInOutData = $this->jobs_model->checkClockInOut($jobId,$applicant_user_account_id);
				/* added by Kratika to fetch the payment per hour of the applicant for the same job */
				$applicants_payment_and_meal_deatails = $this->jobs_model->fetch_payment_details($jobId,$applicant_user_account_id);
				//print_r($clockInOutData); die;
				if(!empty($clockInOutData)){
					$job_details  = $this->jobs_model->get_job_details($jobId);
					/* Added By Kratika on date 24 nov 2016  start */
					$result_arr["clock_in_date"] 	= date("Y-m-d", strtotime($clockInOutData[0]->applicant_clockIn_dateTime));
					$result_arr["clock_in_hour"] 	= date("H", strtotime($clockInOutData[0]->applicant_clockIn_dateTime));
					$result_arr["clock_in_min"]  	= date("i", strtotime($clockInOutData[0]->applicant_clockIn_dateTime));
					$result_arr["clock_out_date"] 	= date("Y-m-d", strtotime($cdateTime));
					/* Added By Kratika on date 24 nov 2016 end */
					$result_arr["clock_out_hour"] 	= date("H", strtotime($cdateTime));
					$result_arr["clock_out_min"] 	= date("i", strtotime($cdateTime));
					$result_arr["job_start_hour"] 	= date("H", strtotime($job_details->start_date." ".$job_details->start_time));
					$result_arr["job_start_min"] 	= date("i", strtotime($job_details->start_date." ".$job_details->start_time));
					$result_arr["job_end_hour"]  	= date("H", strtotime($job_details->end_date." ".$job_details->end_time));
					$result_arr["job_end_min"]  	= date("i", strtotime($job_details->end_date." ".$job_details->end_time));
					$result_arr["job_id"]  			= $jobId;
					$result_arr["applicant_id"]  	= $applicant_user_account_id;
					$result_arr["wages_per_hour"]  	= $applicants_payment_and_meal_deatails[0]->payment_amount_per_hour;
					$result_arr["meal_hour"]  		= ($applicants_payment_and_meal_deatails[0]->meal_hours !="") ? date("H", strtotime($applicants_payment_and_meal_deatails[0]->meal_hours)) : '00';
					$result_arr["meal_min"]        =  ($applicants_payment_and_meal_deatails[0]->meal_hours !="") ? date("i", strtotime($applicants_payment_and_meal_deatails[0]->meal_hours)) : '00';
					/* fetch applicant feedback data */
					$applicant_feedback_data = $this->jobs_model->check_applicant_feedback_data($applicant_user_account_id,$jobId);
					if(empty($applicant_feedback_data)){
						$result_arr['is_feedback_data'] = false;
					}else{
						$result_arr['is_feedback_data'] = true;
						$result_arr['feedback_data'] = $applicant_feedback_data[0];
					}
				}
				echo json_encode($result_arr);
			}
	}

	public function update_meal_hours(){
		$job_id = 		trim($this->input->post('job_id_input'));
		$applicant_id = 	trim($this->input->post('user_account_id_input'));
		$meal_min 	  =  	trim($this->input->post('meal_min'));
		$meal_hour 	  = 	trim($this->input->post('meal_hour'));
		$payment_amount_per_hour  = 	trim($this->input->post('wages_per_hour'));
		$meal_hour_time   = $meal_hour.":".$meal_min.":"."00";
		$up_data = array("meal_hours"=>$meal_hour_time,"payment_amount_per_hour"=>$payment_amount_per_hour);
		$update  = $this->jobs_model->update_meal_hours($job_id,$applicant_id,$up_data);
		if($update){
			echo 1;
		}else{
			echo 0;
		}
	}

	public function reject_applicant(){
		
		$job_id = 		trim($this->input->post('job_id'));
		$applicant_id = 	trim($this->input->post('applicant_user_account_id'));
		$job_details = $this->jobs_model->get_job_details($job_id);
		$rejected_jobs = $this->jobs_model->reject_otherjobs_applicants($applicant_id,$job_details,1);
		$applicants_detail = $this->jobs_model->fetch_payment_details($job_id,$applicant_id);
		
		$update  = $this->jobs_model->reject_status($job_id,$applicant_id);
		if($update){
			if(!empty($rejected_jobs)){
				foreach($rejected_jobs as $reject_applicants){
					$this->jobs_model->update_system_rejected_applicants($reject_applicants->applicant_user_account_id,$reject_applicants->job_id);
				}
			}
			$insertData2 = array();
			$insertData2['notification_type'] = 'applicant_rejected_after_selection';
			$insertData2['sender_id'] = LoginUserDetails('userid');
			$insertData2['job_id'] = $job_id;
			$insertData2['user_id'] = $applicant_id;
			$insertData2['user_type'] = LoginUserDetails('role_id');
			$insertData2['status'] = '0';
			$insertData2['created_at'] = date('Y-m-d h:i:s');
			$insertData2['updated_at'] = date('Y-m-d h:i:s');
			$insertData2['is_delete'] = '0';
			$this->jobs_model->add_push_notification($insertData2);
				
		if($applicants_detail[0]->acknowledged  == 1 && $applicants_detail[0]->ack_approval != 1){
				
				$emailData = array();
				$emailData['job_id'] = $job_details->job_id;
				$emailData['sender_id'] =$applicant_id;
				$emailData['sender_user_type'] = 4;
				$emailData['receiver_id'] = 1;
				$emailData['receiver_user_type'] = '1';
				$emailData['purpose'] = 'aptus_notification_applicant_rejected_after_selection';
				$emailData['subject'] = 'JobsOnDemand: Applicant Job Cancellation Notification';
				$emailData['created_at'] = date('Y-m-d h:i:s');
				$emailData['updated_at'] = date('Y-m-d h:i:s');
				$emailData['is_mail_sent'] = '0';
				
				$this->jobs_model->add_email_notification($emailData);
			}
				echo 1;
		}else{
			echo 0;
		}
	}
	
	/* Added By Kratika on date 24 nov 2016 start */
	public function checkDateTimeDifference(){
		$res_arr = array();
		$clock_out_date_time 	= trim($this->input->post('clock_out_date_time'));
		$clock_in_date_time 	= trim($this->input->post('clock_in_date_time'));
		$time1 = new DateTime($clock_out_date_time);
		$time2 = new DateTime($clock_in_date_time);
		$interval = $time1->diff($time2);
		$res_array['days'] = $interval->format('%d');
		$res_array['seconds'] = $interval->format('%s');
		$res_array['hours'] = $interval->format('%h');
		$res_array['mins'] = $interval->format('%i');
		echo json_encode($res_array); die;
	}
	/* Added By Kratika on date 24 Nov 2016 end */
	
	public function job_summary(){
		//
		$siteLang = $this->session->userdata('language');
		
		$data = array('siteLang' =>$siteLang);
		$this->admin_template->load('admin_template','job_summary',$data);	
	}
	/** ---------------------------------- Applicants - Job Payment Summary section (08 Dec 2016)------------------------ */
	public function getJobSummaryByAjax(){
		$search = "";
		$search_set = 0;
		$dirs = "";
		$length = 0;
		$start = 0;
		$column_name = "";
		if(isset($_GET['order'][0]['dir']) && !empty($_GET['order'][0]['dir'])) {
			$dirs = $_GET['order'][0]['dir'];

		}
		if(isset($_GET['order'][0]['column']) && !empty($_GET['order'][0]['column'])){
			if($_GET['order'][0]['column'] == 1) {
				$column_name = "jod_applicants.first_name";
			}
			if($_GET['order'][0]['column'] == 2) {
				$column_name = "jod_jobs.job_id";
			}
			if($_GET['order'][0]['column'] == 3) {
				$column_name = "jod_jobs.job_title";
			}
			if($_GET['order'][0]['column'] == 4) {
				$column_name = "jod_outlets.outlet_name";
			}
		}
		
		if(isset($_GET['order'][0]['column']) && $_GET['order'][0]['column']  == 0) {
			$dirs = 'ASC';

		}
		
		
		if(isset($_GET['search']['value']) && !empty($_GET['search']['value'])) {
			$search = $_GET['search']['value'];
		    $search = trim($search);
		    $search_set = 1 ;
		}
		
		if(isset($_GET['date'])){
				$date = $_GET['date'];
		}
		$all_records = getJobSummaryByDate($date); 
		$records = getJobSummaryByDate($date,$search,$_GET['length'],$_GET['start'],$dirs,$column_name); 
		$total_records = count($all_records);
		$records_filtered = $total_records;
		if(!empty($records) || !empty($all_records)) {
			$messages      = array();
				$i=0;
				$s = $_GET['start'] + 1;
				if($search_set == 1){
					$records_filtered	= count($records);
				}
				$content='[';
				foreach($records as $key=>$record) {
					$applicant_status = getApplicantStatus($record);
					$label = (check_payment_is_done($record->applicant_user_account_id,$record->job_id) == true) ? $this->lang->line('paid') : $this->lang->line('view');
					$content .='[';
					$messages[$i][0] = "";
					$messages[$i][1] = "<a class='pull-left text-center action-icon ad-man' href='".base_url('admin/applicant_profile/'.$record->applicant_user_account_id)."'>". ucfirst($record->applicant_fname)." ".ucfirst($record->applicant_lname)."</a>";
					$messages[$i][2] =  $record->job_id;
					$messages[$i][3] =  $record->job_title;
					$messages[$i][4] =  $record->outlet_name;
					$messages[$i][5] =  "<span class='gray'><strong>".$applicant_status['status']."</strong></spa>";
					$messages[$i][6] =  "<a class='pull-left text-center action-icon ad-man' onclick='javascript:show_popup(". $record->job_id.','.$record->applicant_user_account_id.','.$record->outlet_id.','.$applicant_status['status_code'].")'><i class='fa fa-eye'></i><div class='clerfix'></div><lable>". $label."</lable></a>";
					$i++;
					$s++;
				}
				$content .= ']';
				$final_data = json_encode($messages);
			
			
		}else{
			$final_data = json_encode(array());
			$total_records = 0;
			$records_filtered = 0;
			
		}
		if(isset($_GET['draw'])){
		 $draw = $_GET['draw'] ;
		}else{
		 $draw = 1;
		}
		echo '{"draw":'.$draw.',"recordsTotal":'.$total_records.',"recordsFiltered":'.$records_filtered.',"data":'.$final_data.'}'; die;
		
	}
	
	public function applicant_job_summary_details($job_id1="",$applicant_id1="",$outlet_id1="",$for_download=0){
		
		$job_id 		= 	trim($this->input->post('job_id'));
		$applicant_id 	= 	trim($this->input->post('user_account_id'));
		$outlet_id 		= 	trim($this->input->post('outlet_id'));
		if($for_download == 1){
			$job_id 		= 	$job_id1;
			$applicant_id 	= 	$applicant_id1;
			$outlet_id 		= 	$outlet_id1;
		}
		
		$job_applicant_data 	= $this->jobs_model->get_job_applicant_data($job_id,$applicant_id);
		$job_details = $this->jobs_model->get_job_details($job_id);
		$results = array();
		
		// If applicant has done clock out then only super admin can done adjustments
		if($job_applicant_data[0]->clockInOutStatus == 2){
			$payment_data 			= $this->jobs_model->payment_data_already_exist($job_id,$applicant_id);
			$clockInOutData 		= $this->jobs_model->checkClockInOut($job_id,$applicant_id);
			$transaction_data       = get_applicant_transaction_data($job_id,$applicant_id,$outlet_id);
			
			$user_account_data 		= getUserDetailsByID($applicant_id);
			$applicant_detail       = getApplicantDetails($applicant_id);
			
			$res_array = array();
			$res_array['applicant_full_name'] = (!empty($applicant_detail)) ? ucfirst($applicant_detail[0]->first_name)." ".ucfirst($applicant_detail[0]->last_name) : "N/A";
			$res_array['nric_unique_id'] 	= (!empty($user_account_data)) ? $user_account_data->unique_id : "N/A";
			$res_array['old_clock_in_date_time'] = $clockInOutData[0]->applicant_clockIn_dateTime;
			$res_array['old_clock_out_date_time'] = $clockInOutData[0]->applicant_clockOut_dateTime;
			$res_array['clock_in_date_text']=  date('d M Y', strtotime($clockInOutData[0]->applicant_clockIn_dateTime));
			$res_array['clock_in_date']  	=  date('Y-m-d', strtotime($clockInOutData[0]->applicant_clockIn_dateTime));
			$res_array['clock_in_hour']  	=  date('H', strtotime($clockInOutData[0]->applicant_clockIn_dateTime));
			$res_array['clock_in_min']  	=  date('i', strtotime($clockInOutData[0]->applicant_clockIn_dateTime));
			$res_array['clock_in_time']  	=  date('H:i', strtotime($clockInOutData[0]->applicant_clockIn_dateTime));
			$res_array['clock_out_date_text'] =  date('d M Y', strtotime($clockInOutData[0]->applicant_clockOut_dateTime));
			$res_array['clock_out_date'] 	=  date('Y-m-d', strtotime($clockInOutData[0]->applicant_clockOut_dateTime));
			$res_array['clock_out_hour'] 	=  date('H', strtotime($clockInOutData[0]->applicant_clockOut_dateTime));
			$res_array['clock_out_min']   	=  date('i', strtotime($clockInOutData[0]->applicant_clockOut_dateTime));
			$res_array['clock_out_time']  	=  date('H:i', strtotime($clockInOutData[0]->applicant_clockOut_dateTime));
			$res_array['total_meal_break_time'] = $job_applicant_data[0]->meal_hours;
			$res_array['additional_jod_allowance'] = $job_applicant_data[0]->additional_jod_allowance;
			$meal_time = $job_applicant_data[0]->meal_hours;
			//echo $meal_time; die;
			$meal_array = explode(":",$meal_time);
			$res_array['meal_hour']  	=  $meal_array[0];
			$res_array['meal_min']      =  $meal_array[1];
			$meal_time = $job_applicant_data[0]->meal_hours;
			/* check meal break time */
			if($meal_time != "" && $meal_time !="00:00:00"){
				$seconds = strtotime("1970-01-01 $meal_time UTC");
				$meal_break_minutes =  $seconds/60;
			}else{
				$meal_break_minutes = 0;
			}
			/* Total worked hours fetched from Db not having without dedcuted meal break time  */
				$total_hour_without_deduction_meal_break_time  = $transaction_data->total_hour_worked;
			/* Total Minutes after deduction meal break time in minutes */
				$total_minutes_after_deduction_meal_break_time  = ($transaction_data->total_hour_worked*60) - $meal_break_minutes;
			/* Total hours  after deduction meal break time in minutes */
				$total_hours_worked_after_deduction_meal_break_time = $total_minutes_after_deduction_meal_break_time/60.0;
			/* Total hours  worked after deduction meal break time in h:i formate */
			$res_array["total_hours_worked"] =convert_seconds_to_time(round(($total_hours_worked_after_deduction_meal_break_time*3600),0));
		
			$res_array["wages_per_hour"] = ($job_applicant_data[0]->payment_amount_per_hour != 0) ? sprintf('%0.2f',$job_applicant_data[0]->payment_amount_per_hour) : $job_details->payment_amount ;
			$res_array["total_jod_credit"] = sprintf('%0.2f',$transaction_data->total_wages);
			$res_array["bank_name"] = $job_applicant_data[0]->bank_name;
			$res_array["bank_account_name"] = $job_applicant_data[0]->bank_account_name;
			$res_array["bank_account_number"] = $job_applicant_data[0]->bank_account_number;
			$outlet_feedback = get_managers_feedback($job_id,$applicant_id); 
			$res_array["ack_approval"] = $job_applicant_data[0]->ack_approval;
			$res_array["acknowledged"] = $job_applicant_data[0]->acknowledged;
			/* To check transaction is already done or not */
			
			if(!empty($payment_data)){
					
				//print_r($payment_data); die;
				$res_array['transaction_already_done'] =  true;
				$adjusted_data = array();
				$adjusted_data['new_clock_in_date_time'] 	= $payment_data[0]->clock_in_datetime;
				$adjusted_data['new_clock_out_date_time'] 	= $payment_data[0]->clock_out_datetime;
				$adjusted_data['new_meal_break_time'] 	    = $payment_data[0]->meal_break_time;
				
				
				$adjusted_data['clock_in_date'] 	= date('Y-m-d', strtotime($payment_data[0]->clock_in_datetime));
				$adjusted_data['clock_in_hour'] 	= date('H', strtotime($payment_data[0]->clock_in_datetime));
				$adjusted_data['clock_in_min'] 		= date('i', strtotime($payment_data[0]->clock_in_datetime));
				$adjusted_data['clock_out_date'] 	=  date('Y-m-d', strtotime($payment_data[0]->clock_out_datetime));
				$adjusted_data['clock_out_hour'] 	=  date('H', strtotime($payment_data[0]->clock_out_datetime));
				$adjusted_data['clock_out_min']   	=  date('i', strtotime($payment_data[0]->clock_out_datetime));
				$adjusted_data['wages_per_hour_adjusted_SA']   =  $payment_data[0]->wages_per_hour;
				$adjusted_data['comments_SA']   		=  $payment_data[0]->comments;
				$meal_array = explode(":",$payment_data[0]->meal_break_time);
				$adjusted_data['meal_hour']  			=  $meal_array[0];
				$adjusted_data['meal_min']      		=  $meal_array[1];
				$adjusted_data['total_hours_worked']    =  sprintf('%0.2f',$payment_data[0]->total_hours_worked);
				$adjusted_data['total_jod_credit']    	=  sprintf('%0.2f',$payment_data[0]->total_jod_credit);
				$adjusted_data['time_difference']    	=  $payment_data[0]->time_difference_in_minutes;
				$adjusted_data['jod_credit_difference'] =  $payment_data[0]->jod_credit_difference;
				$adjusted_data['created_at'] 			=  $payment_data[0]->created_at;
				$adjusted_data['updated_at']			=  $payment_data[0]->updated_at;
				$res_array['new_adjusted_data']   		=  $adjusted_data;
			}else{
				$res_array['transaction_already_done'] =  false;
			}
			
			if(!empty($outlet_feedback)){
				$outlet_feedback_arr = array();
				$outlet_feedback_arr['rating'] = $outlet_feedback->rating; 
				$outlet_feedback_arr['feedback_text'] = $outlet_feedback->feedback_text; 
				$results['outlet_feedback'] = $outlet_feedback_arr;
			}else{
				$results['outlet_feedback'] = false;
			}
			$applicant_feedback = get_applicants_feedback($applicant_id,$job_id);
			if(!empty($applicant_feedback)){
				$applicant_feedback_data = array();
				$applicant_feedback_data['rating']              = $applicant_feedback->rating; 
				$applicant_feedback_data['additional_comments'] = $applicant_feedback->additional_comments; 
				$results['applicant_feedback'] 					= $applicant_feedback_data;
				
			}else{
				$results['applicant_feedback'] 	= false;
			}
			$results['data'] = $res_array;
			$results['status'] = true;
		}else{
			$results['data'] = false;
			$results['status'] = false;
			
		}
		if($for_download == 0 ){
			echo json_encode($results); die;
		}else{
			return $results; 
		}
		
		
	}
	
	public function applicants_clockin_clockout_details_adjusted_by_super_admin(){
		$job_id 		= 	trim($this->input->post('job_id_input'));
		$applicant_id 	= 	trim($this->input->post('user_account_id_input'));
		$clock_in_date 	= 	trim($this->input->post('clock_in_date'));
		$clock_in_hour 	= 	trim($this->input->post('clock_in_hour'));
		$clock_in_min 	= 	trim($this->input->post('clock_in_min'));
		$clock_out_date = 	trim($this->input->post('clock_out_date'));
		$clock_out_min 	= 	trim($this->input->post('clock_out_min'));
		$clock_out_hour = 	trim($this->input->post('clock_out_hour'));
		$meal_hour      = 	trim($this->input->post('meal_hour'));
		$meal_min       = 	trim($this->input->post('meal_min'));
		$wages_per_hour = 	trim($this->input->post('wages_per_hour'));
		$super_admin_comments  	= 	trim($this->input->post('super_admin_comments'));
		$outlet_id  			= 	trim($this->input->post('outlet_id'));
		$old_meal_break_time  	= 	trim($this->input->post('old_meal_hours'));
		$old_wages_per_hour  	= 	trim($this->input->post('old_wages_per_hour'));
		$comments  				= 	trim($this->input->post('comments'));
		$bank_name				=   trim($this->input->post('bank_name_input'));
		$bank_account_number	=   trim($this->input->post('bank_account_number_input'));
		$bank_account_name		=   trim($this->input->post('bank_account_name_input'));
		$additional_jod_allowance		=   trim($this->input->post('additional_jod_allowance'));
		
		$clock_in_date_time    	= $clock_in_date." ".$clock_in_hour.":".$clock_in_min.":00";
		$clock_out_date_time   	= $clock_out_date." ".$clock_out_hour.":".$clock_out_min.":00";
		$total_meal_hours	   	= $meal_hour.":".$meal_min.":"."00";
		$applicant_transaction_details = get_applicant_transaction_data($job_id,$applicant_id,$outlet_id);
		$old_meal_break_time1 = $old_meal_break_time.":00";
		/* fetch clock in clock out details */
		$clockin_clockout_details = applicant_clockin_clockout_details($job_id,$applicant_id);
		
		/* Its difference between the clock out and clock in time (In minutes) adjusted by SA */
		$total_worked_min_SA = (strtotime($clock_out_date_time) - strtotime($clock_in_date_time) )/60.0;
		$seconds = strtotime("1970-01-01 $total_meal_hours UTC");
		$meal_break_minutes_SA =  $seconds/60;
		
		/* Total worked minutes after deduction of the meal break minutes adjusted by SA*/
		$total_worked_min_after_deduction_meal_break_min_SA = $total_worked_min_SA - $meal_break_minutes_SA;
		
		/* Total worked hours after deduction of meal break time and adjusted by SA */
		$total_worked_hours_after_deduction_meal_break_min_SA = $total_worked_min_after_deduction_meal_break_min_SA/60.0;
		/* Total wages adjusted by SA */
		$total_wages_adjusted_SA =  $wages_per_hour  *  ($total_worked_min_after_deduction_meal_break_min_SA/60.0);
		
		/* old data */
		$total_min_worked_old_data = ($applicant_transaction_details->total_hour_worked)*60.0;
		$seconds = strtotime("1970-01-01 $old_meal_break_time UTC");
		$old_meal_break_min =  $seconds/60;
		/* Total old worked minutes after  deduction meal break time */
		$total_old_worked_min_after_deduction_meal_break_time = $total_min_worked_old_data - $old_meal_break_min;
		
		/* Time difference between old total worked min and new total worked min */
		$time_difference_minutes = $total_worked_min_after_deduction_meal_break_min_SA - $total_old_worked_min_after_deduction_meal_break_time; 
		//echo $time_difference_minutes; die;
		/* Jod credit difference between old total JOD credit  and new total worked min */
		$jod_credit_difference =  $total_wages_adjusted_SA - $applicant_transaction_details->total_wages;
		
		$payment_data = $this->jobs_model->payment_data_already_exist($job_id,$applicant_id);
			$insert_data = array();
			$insert_data['job_id']						= $job_id;
			$insert_data['applicants_user_account_id']  = $applicant_id;
			$insert_data['clock_in_datetime'] 			= $clock_in_date_time;
			$insert_data['clock_out_datetime'] 			= $clock_out_date_time;
			$insert_data['total_hours_worked'] 			= $total_worked_hours_after_deduction_meal_break_min_SA;
			$insert_data['meal_break_time'] 			= $total_meal_hours;
			$insert_data['wages_per_hour'] 				= $wages_per_hour;
			$insert_data['total_jod_credit'] 			= $total_wages_adjusted_SA;
			$insert_data['time_difference_in_minutes']  = $time_difference_minutes; 
			$insert_data['jod_credit_difference']       = round($jod_credit_difference,2);  
			$insert_data['comments']       				= $super_admin_comments; 
			$insert_data['created_at']       			= date('Y-m-d H:i:s'); 
			$insert_data['updated_at']       			= date('Y-m-d H:i:s'); 
		//	print_r($insert_data); die;
		/* If no record exist */
		if(empty($payment_data)){
			
			/* insert data */
			$outlet_details = getOutletDetail($outlet_id);
			//$company_details = pdf_link($outlet_details->company_id);
			
			$transaction_data = array();
			$transaction_data['job_id']						= $job_id;
			$transaction_data['applicants_user_account_id'] = $applicant_id;
			$transaction_data['total_hour_worked'] 			= $total_worked_hours_after_deduction_meal_break_min_SA;
			$transaction_data['total_wages'] 				= $total_wages_adjusted_SA;
			$transaction_data['outlet_id'] 					= $outlet_id;
			$transaction_data['transaction_reference_id']   = $applicant_transaction_details->id;
			//$transaction_data['jod_credit_difference'] 	= $jod_credit_difference;
			//$transaction_data['credit_after_transacion']  = $company_details[0]->current_jod_credit - $total_wages_adjusted_SA;
			$transaction_data['created_at']					= date('Y-m-d H:i:s');
			$transaction_data['updated_at'] 				= date('Y-m-d H:i:s');
			$update_data = array();
			$update_data["bank_name"]  = $bank_name;
			$update_data["bank_account_name"] = $bank_account_name;
			$update_data["bank_account_number"] = $bank_account_number;
			$update_data["additional_jod_allowance"] = $additional_jod_allowance;
			 if($super_admin_comments != ""){
				 /* update bank detail reocrd in the DB */
				$confirm_payment_status = confirm_payment_by_SA($outlet_details->company_id,$transaction_data,$insert_data);
				$this->jobs_model->update_bank_details($update_data,$applicant_id,$job_id);
				if($confirm_payment_status  == true ){
					echo 1; die;
				}else{
					echo 2; die;
				}
			}else{
				
				if($clockin_clockout_details[0]->applicant_clockIn_dateTime != $clock_in_date_time || $clockin_clockout_details[0]->applicant_clockOut_dateTime != $clock_out_date_time || $old_meal_break_time1 != $total_meal_hours || $old_wages_per_hour != $wages_per_hour){
					echo 3; die;
				}else{
					/* update bank detail reocrd in the DB */
					
					$this->jobs_model->update_bank_details($update_data,$applicant_id,$job_id);
					$confirm_payment_status = confirm_payment_by_SA($outlet_details->company_id,$transaction_data,$insert_data);
					if($confirm_payment_status  == true ){
						echo 1; die;
					}else{
						echo 2; die;
					}
				}
			}
			
		}else{
			echo 0; die;
		}
				
	}
	
	public function job_payments(){
		$siteLang = $this->session->userdata('language');
		$data = array('siteLang' =>$siteLang);
		$this->admin_template->load('admin_template','job_payments',$data);	
	}

	public function getPaymentDataByAjax(){
		$search = "";
		$search_set = 0;
		$dirs = "";
		$length = 0;
		$start = 0;
		$column_name = "";
		if(isset($_GET['order'][0]['dir']) && !empty($_GET['order'][0]['dir'])) {
			$dirs = $_GET['order'][0]['dir'];

		}
		if(isset($_GET['order'][0]['column']) && !empty($_GET['order'][0]['column'])){
			if($_GET['order'][0]['column'] == 1) {
				$column_name = "jod_applicants.first_name";
			}
			if($_GET['order'][0]['column'] == 2) {
				$column_name = "jod_jobs.job_title";
			}
			if($_GET['order'][0]['column'] == 3) {
				$column_name = "jod_outlets.outlet_name";
			}
		}
		
		if(isset($_GET['order'][0]['column']) && $_GET['order'][0]['column']  == 0) {
			$dirs = 'ASC';

		}
		
		
		if(isset($_GET['search']['value']) && !empty($_GET['search']['value'])) {
			$search = $_GET['search']['value'];
		    $search = trim($search);
		    $search_set = 1 ;
		}
		
		if(isset($_GET['date'])){
				$date = $_GET['date'];
		}
		
		$all_records = getConfirmPaymentData($date);
		$records     = getConfirmPaymentData($date,$search,$_GET['length'],$_GET['start'],$dirs,$column_name);
		$total_records = count($all_records);
		$records_filtered = $total_records;
		
		if(!empty($records) && !empty($all_records)) {
			$messages      = array();
				$i=0;
				$s = $_GET['start'] + 1;
				if($search_set == 1){
					$records_filtered	= count($records);
				}
				$content='[';
				foreach($records as $key=>$record) {
				//	print_r($record); die;
				//	$applicant_status = getApplicantStatus($record);
					$content .='[';
					$messages[$i][0] = "";
					$messages[$i][1] =  "<a class='pull-left text-center action-icon ad-man' href='".base_url('admin/applicant_profile/'.$record->applicants_user_account_id)."'>". ucfirst($record->applicant_fname)." ".ucfirst($record->applicant_lname)."</a>";
					$messages[$i][2] =  $record->job_title;
					$messages[$i][3] =  $record->outlet_name;
					$messages[$i][4] =  date('d M Y', strtotime($record->clock_in_datetime));
					$messages[$i][5] =  date('d M Y', strtotime($record->clock_out_datetime));
					$messages[$i][6] = 	convert_seconds_to_time(round(($record->total_hours_worked*3600)));
						
					$messages[$i][7] =  sprintf('%0.2f', $record->total_jod_credit);
					$messages[$i][8] =  "<a class='pull-left text-center action-icon ad-man' onclick='javascript:show_popup(". $record->job_id.','.$record->applicants_user_account_id.','.$record->outlet_id.','.$record->id.")'><i class='fa fa-eye'></i><div class='clerfix'></div><lable>". $this->lang->line('view')."</lable></a>";
					$i++;
					$s++;
				}
				$content .= ']';
				$final_data = json_encode($messages);
		}else{
			$final_data = json_encode(array());
			$total_records = 0;
			$records_filtered = 0;
			
		}
		if(isset($_GET['draw'])){
		 $draw = $_GET['draw'] ;
		}else{
		 $draw = 1;
		}
		echo '{"draw":'.$draw.',"recordsTotal":'.$total_records.',"recordsFiltered":'.$records_filtered.',"data":'.$final_data.'}'; die;
		
	}
	
	public function download_payment_summary($date){
		$all_records = getConfirmPaymentData($date);
		if(!empty($all_records)){
			$final_array = array();
			foreach($all_records as $records){
			
				$record_array = array();
				$applicant_data = $this->applicant_job_summary_details($records->job_id,$records->applicants_user_account_id,$records->outlet_id,1);

				$record_array["job_id"] 					= $records->job_id;
				$record_array["user_accounts_id"] 			= $records->applicants_user_account_id;
				
				$record_array["applicant_fname"] 			= $records->applicant_fname;
				$record_array["applicant_lname"] 			= $records->applicant_lname;
				$record_array["nric_number"]     			= $applicant_data["data"]["nric_unique_id"]; 
				$record_array["outlet_name"] 				= $records->outlet_name;
				$record_array["new_clock_in_date"]   		= date('d M Y', strtotime($records->clock_in_datetime));
				$record_array["new_clock_in_time"] 	 		= date('H:i', strtotime($records->clock_in_datetime));
				$record_array["new_clock_out_date"]  		= date('d M Y', strtotime($records->clock_out_datetime));
				$record_array["new_clock_out_time"] 		= date('H:i', strtotime($records->clock_out_datetime));
				$record_array["total_meal_break_time_new"]  = $records->meal_break_time;
				//$record_array["total_hours_worked_new"]  	= sprintf('%0.2f', $records->total_hours_worked);
				$record_array["total_hours_worked_new"] 	= convert_seconds_to_time(round(($records->total_hours_worked*3600)));
				$record_array["wages_per_hour_new"]    		= "$".$records->wages_per_hour;
				$record_array["total_jod_credit_new"]  		= "$".sprintf('%0.2f', $records->total_jod_credit);
				$record_array["insuarance_premium"]  		= "$".($records->total_hours_worked * 0.35);
				$record_array["additional_allowance"]  		= "$".$applicant_data["data"]["additional_jod_allowance"]; 
				$amt = ($records->total_jod_credit - ($records->total_hours_worked * 0.35))  + $applicant_data["data"]["additional_jod_allowance"];
				$record_array["net_payable"]  				= "$".sprintf('%0.2f',$amt); 
				$record_array["bank_name"]  				=  $applicant_data["data"]["bank_name"];
				$record_array["bank_account_name"]  		=  $applicant_data["data"]["bank_account_name"];
				$record_array["bank_account_number"]  		= $applicant_data["data"]["bank_account_number"];
				
				$final_array[]  = $record_array;
			} 
			$filename = "payment_summary_".$date.".xlsx";
			$title = "Payment Summary -".$date;
			$this->generate_payment_excel($final_array,$title,$filename);
		}else{
			set_global_messages($this->lang->line('no_job_payment_summary_exist'),'error');
			header('Location: '.base_url().'jobs/job_payments'); 
		}
		
	}
	
	/* @Method : To genearte the  excel file for Payment Summary
	 * @Params :  transaction_history_data, title, filename
	 * @Response : create the excel sheet and then make it download
	 * @Author : Kratika Jain
	 */

	public function generate_payment_excel($final_array,$title,$filename){

		$this->excel->setActiveSheetIndex(0);

		$this->excel->getActiveSheet()->setCellValue('A1', $title);
		//$this->excel->getActiveSheet()->setCellValue('A3', 'S. No.');
		$this->excel->getActiveSheet()->setCellValue('A2', 'Job ID Number');
		$this->excel->getActiveSheet()->setCellValue('B2', 'Job Applicant User Account ID');
		$this->excel->getActiveSheet()->setCellValue('C2', 'Job Applicant First Name');
		$this->excel->getActiveSheet()->setCellValue('D2', 'Job Applicant Last Name');
		$this->excel->getActiveSheet()->setCellValue('E2', 'Job Applicant NRIC/FIN number');
		$this->excel->getActiveSheet()->setCellValue('F2', 'Outlet Name');
		$this->excel->getActiveSheet()->setCellValue('G2', 'Job Applicant Clock In Date');
		$this->excel->getActiveSheet()->setCellValue('H2', 'Job Applicant Clock In Time');
		$this->excel->getActiveSheet()->setCellValue('I2', 'Job Applicant Clock Out Date');
		$this->excel->getActiveSheet()->setCellValue('J2', 'Job Applicant Clock Out Time');
		$this->excel->getActiveSheet()->setCellValue('K2', 'Total Break Time');
		$this->excel->getActiveSheet()->setCellValue('L2', 'Total Hours Worked ');
		$this->excel->getActiveSheet()->setCellValue('M2', 'Payment Amount (Rate)');
		$this->excel->getActiveSheet()->setCellValue('N2', 'Job Applicant Total Wages Due ');
		$this->excel->getActiveSheet()->setCellValue('O2', 'Insurance Premium Payable ($0.35 x Total Hours Worked)');
		$this->excel->getActiveSheet()->setCellValue('P2', 'Additional Allowance');
		$this->excel->getActiveSheet()->setCellValue('Q2', 'Net Payable to Member (Total Wages - Insurance Premium + Additional Allowance)');
		$this->excel->getActiveSheet()->setCellValue('R2', 'Job Applicant Bank Name');
		
		$this->excel->getActiveSheet()->setCellValue('S2', 'Job Applicant Name as per Bank Account');
		$this->excel->getActiveSheet()->setCellValue('T2', 'Job Applicant Bank Account No.');
	
	

		//merge cell A1 until C1
		$this->excel->getActiveSheet()->mergeCells('A1:T1');
		


		//set aligment to center for that merged cell (A1 to C1)
		$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		 //make the font become bold
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(16);
		$this->excel->getActiveSheet()->getStyle('A1')->getFill()->getStartColor()->setARGB('#333');
		$this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(123);
		
		

		 for($col = ord('A'); $col <= ord('T'); $col++){
			//set column dimension
			//$this->excel->getActiveSheet()->getColumnDimension(chr($col))->setAutoSize(true);
			$this->excel->getActiveSheet()->getRowDimension(1)->setRowHeight(-1);
			//change the font size
			$this->excel->getActiveSheet()->getStyle(chr($col))->getFont()->setSize(10);
			$this->excel->getActiveSheet()->getStyle(chr($col))->getAlignment()->setWrapText(true); 
			$this->excel->getActiveSheet()->getStyle(chr($col))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$this->excel->getActiveSheet()->getColumnDimension(chr($col))->setWidth(12);
			$this->excel->getActiveSheet()->getColumnDimension(chr($col))->setWidth(12);
		}
		// fill color
		$this->excel->getActiveSheet()
		->getStyle('A2:T2')
		->getFill()
		->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
		->getStartColor()
		->setARGB('FF6600');
		
		
		$cellIterator = $this->excel->getActiveSheet()->getRowIterator()->current()->getCellIterator();
		$cellIterator->setIterateOnlyExistingCells(true);
		/** @var PHPExcel_Cell $cell */
		foreach ($cellIterator as $cell) {
			$this->excel->getActiveSheet()->getColumnDimension($cell->getColumn())->setAutoSize(true);
		}
		

		$exceldata = $final_array;
		//Fill data
		$this->excel->getActiveSheet()->fromArray($exceldata, null, 'A3');


		//$filename='PHPExcelDemo.xls'; //save our workbook as this file name
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache

		//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		//if you want to save it as .XLSX Excel 2007 format
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
		//force user to download the Excel file without writing it to server's HD
		$objWriter->save('php://output');
	}
	
}

