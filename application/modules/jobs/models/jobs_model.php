<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Jobs_model extends CI_model{
	function __construct(){
		parent::__construct();
	}
	/** Job type section start**/
	function get_job_type($id='') {
		$prefix = $this->db->dbprefix;
		$this->db->select('*');
		$this->db->where('is_delete','0');
		$this->db->where('status','1');
		if($id) {
			$this->db->where('job_role_id',$id);
			$query = $this->db->get($prefix.'job_roles');
			return $query->row();
		}
		$this->db->order_by("created_at", "desc");
		$query = $this->db->get($prefix.'job_roles');

		//echo $this->db->last_query(); die;
		return $query->result();
	}
	/** Job type section end**/

	/** Job templates section start**/
	function get_job_templates($companyId='', $id='') {
		$prefix = $this->db->dbprefix;
		$this->db->select('templates.*, job_roles.name');
		$this->db->from($prefix.'templates as templates');
		$this->db->join($prefix.'job_roles as job_roles', 'job_roles.job_role_id = templates.job_type_id', 'left');
		$this->db->where('templates.is_delete','0');
		$this->db->where('templates.status','1');
		$this->db->where('templates.company_id',$companyId);
		//return $this->db->last_query();
		if($id) {
			$this->db->where('template_id',$id);
			$query = $this->db->get();
			return $query->row();
		}
		$this->db->order_by("created_at", "desc");
		$query = $this->db->get();

		//echo $this->db->last_query(); die;
		return $query->result();
	}
	/** Job templates section end**/

	/** All job section start **/

	/** All job section end **/
		function get_all_jobs($id='',$type,$pageNo='',$limit='',$search="") {
		if ($pageNo!="") {
			$offset = ($pageNo-1)*$limit;
		} else {
			$offset='0';
		}
		//echo $offset; die;
		$prefix = $this->db->dbprefix;
		$this->db->select($prefix.'jobs.*');
		$this->db->select($prefix.'job_roles.name');
		$this->db->join($prefix.'job_roles', $prefix.'job_roles.job_role_id = jobs.job_type_id');
		if(LoginUserDetails('role_id') == 2) { // if loged in user is headquarder manager
			$this->db->join($prefix.'outlets', $prefix.'outlets.outlet_id = jobs.outlet_id');
			$this->db->join($prefix.'headquater_manager', $prefix.'headquater_manager.company_id = outlets.company_id');
			$this->db->where($prefix.'headquater_manager.user_accounts_id',LoginUserDetails('userid'));
			//$this->db->where($prefix.'jobs.is_delete','0');
		} else {
			if(LoginUserDetails('role_id') == 5){
				/* Added by Kratika to change the updated all jobs list */
				$area_manager_account_id = LoginUserDetails('userid');
				$area_manager_id = getAreaManagerId(LoginUserDetails('userid'));
				$data = get_outlets_by_area_manager_id($area_manager_id[0]->area_manager_id);
				$outlet_ids = array();
				foreach($data as $outlet){
					array_push($outlet_ids,$outlet->outlet_id);
				}
				$outlet_id_str = implode(",",$outlet_ids);
				$where = "jod_jobs.created_by = $area_manager_account_id || (jod_jobs.job_created_by  = 1 && jod_jobs.outlet_id IN ($outlet_id_str))";
				$this->db->where($where);
				//$this->db->where($prefix.'jobs.created_by',LoginUserDetails('userid'));
			}
			if(LoginUserDetails('role_id') == 3){
				/* Added by Kratika to change the updated all jobs list */
				$outlet_manger_account_id = LoginUserDetails('userid');
				$outlet_id =  LoginUserDetails('user_outlet_id');
				$where = $prefix."jobs.job_created_by  = 0  AND  jod_jobs.outlet_manager_id = $outlet_manger_account_id  OR  jod_jobs.job_created_by  = 1 AND jod_jobs.outlet_id  = $outlet_id";
				$this->db->where($where);
				//$this->db->where($prefix.'jobs.outlet_manager_id',LoginUserDetails('userid'));
			}
		}
		if(trim($search) !=""){
			$this->db->like($prefix.'jobs.job_title', $search);

		}
		//$this->db->where($prefix.'jobs.is_delete','0');
		$this->db->where($prefix.'jobs.is_approved !=','2');
		if($id) {
			$this->db->where($prefix.'jobs.job_id',$id);
			$query = $this->db->get($prefix.'jobs');
			return $query->row();
		}
		$this->db->order_by($prefix.'jobs.created_at', "desc");
		if ($pageNo!="" && $limit!="") $this->db->limit($limit,$offset);
		$result =0;
		switch($type){
			case 'count':
			$result = $this->db->count_all_results($prefix.'jobs');
			break;

			case 'row':
			$query =$this->db->get($prefix.'jobs');
			$result = $query->row();
			break;

			case 'result':
			$query =$this->db->get($prefix.'jobs');
			$result = $query->result();
			break;
		}

		return $result;
	}


	/** open job section start **/
	function get_open_jobs($id='',$type,$pageNo='',$limit='') {
		if ($pageNo!="") {
			$offset = ($pageNo-1)*$limit;
		} else {
			$offset='0';
		}
		$prefix = $this->db->dbprefix;
		$this->db->select($prefix.'jobs.*');
		$this->db->select($prefix.'job_roles.name');
		$this->db->join($prefix.'job_roles', $prefix.'job_roles.job_role_id = jobs.job_type_id');

		if(LoginUserDetails('role_id') == 2) { // if loged in user is headquarder manager
			$this->db->join($prefix.'outlets', $prefix.'outlets.outlet_id = jobs.outlet_id');
			$this->db->join($prefix.'headquater_manager', $prefix.'headquater_manager.company_id = outlets.company_id');
			$this->db->where($prefix.'headquater_manager.user_accounts_id',LoginUserDetails('userid'));
		}
		else if(LoginUserDetails('role_id') == 3)  {// if logged in user is Outlet Manger
			$outlet_manger_account_id = LoginUserDetails('userid');
			$outlet_id =  LoginUserDetails('user_outlet_id');
			$where = "(".$prefix."jobs.job_created_by  = 0  AND  jod_jobs.outlet_manager_id = $outlet_manger_account_id  OR  jod_jobs.job_created_by  = 1 AND jod_jobs.outlet_id  = $outlet_id)";
			$this->db->where($where);
			//echo $where; die;
			//$this->db->where($prefix.'jobs.outlet_manager_id',LoginUserDetails('userid'));
			$this->db->where($prefix.'jobs.is_applicants_hired',0);
			
		}
		else {
			/* Modifications by Kratika for the open jobs list for the Area Manager */
			if(LoginUserDetails('role_id') == 5) {
				$area_manager_account_id = LoginUserDetails('userid');
				$area_manager_id = getAreaManagerId(LoginUserDetails('userid'));
				$data = get_outlets_by_area_manager_id($area_manager_id[0]->area_manager_id);
				$outlet_ids = array();
				foreach($data as $outlet){
					array_push($outlet_ids,$outlet->outlet_id);
				}
				$outlet_id_str = implode(",",$outlet_ids);
				$where = "(jod_jobs.created_by = $area_manager_account_id || (jod_jobs.job_created_by  = 1 && jod_jobs.outlet_id IN ($outlet_id_str)))";
				$this->db->where($where);
				$this->db->where($prefix.'jobs.is_applicants_hired',0);
			}
		}
		$this->db->where($prefix.'jobs.is_delete','0');
		$this->db->where($prefix.'jobs.is_completed',0);
		$this->db->order_by($prefix.'jobs.updated_at', "desc");
		$this->db->order_by($prefix.'jobs.created_at', "desc");
		if($id) {
			$this->db->where($prefix.'jobs.job_id',$id);
			$query = $this->db->get($prefix.'jobs');
			return $query->row();
		}

		if ($pageNo!="" && $limit!="") $this->db->limit($limit,$offset);
		$result =0;
		switch($type){
			case 'count':
			$result = $this->db->count_all_results($prefix.'jobs');
			break;

			case 'row':
			$query =$this->db->get($prefix.'jobs');
			$result = $query->row();
			break;

			case 'result':
			$query =$this->db->get($prefix.'jobs');
			$result = $query->result();
			break;
		}
		//echo $this->db->last_query(); die;
		return $result;
	}

	function add_job($data='') {
		$prefix = $this->db->dbprefix;
		$this->db->insert($prefix.'jobs', $data);
		return $this->db->insert_id();
	}

	function update_job($id='',$data='') {
		$prefix = $this->db->dbprefix;
		$this->db->where('job_id', $id);
		return $this->db->update($prefix.'jobs' ,$data);
	}

	//rsolanki
	function add_clockInOut($data='') {
		$prefix = $this->db->dbprefix;
		$this->db->insert($prefix.'clockIn_clockOut', $data);
		return $this->db->insert_id();
	}

	function update_clockInOut($job_id='',$applicant_user_account_id='',$data='') {
		$prefix = $this->db->dbprefix;
		$this->db->where('job_id', $job_id);
		$this->db->where('applicant_user_account_id', $applicant_user_account_id);
		return $this->db->update($prefix.'clockIn_clockOut' ,$data);
	}
	function checkClockInOut($job_id,$applicant_user_account_id){
		$prefix = $this->db->dbprefix;
		$this->db->select($prefix.'clockIn_clockOut.*');
		$this->db->where('job_id', $job_id);
		$this->db->where('applicant_user_account_id', $applicant_user_account_id);
		$query = $this->db->get($prefix.'clockIn_clockOut');
		return $data = $query->result();
		//return 	$query->num_rows();
	}

	function checkPendingClockInOutJobs($job_id,$applicant_user_account_id){
		$prefix = $this->db->dbprefix;
		$this->db->select($prefix.'clockIn_clockOut.id');
		$this->db->where('job_id', $job_id);
		$this->db->where('applicant_user_account_id', $applicant_user_account_id);
		$this->db->where('applicant_clockOut_status', '0');
		$query = $this->db->get($prefix.'clockIn_clockOut');
		//return $data = $query->result();
		return 	$query->num_rows();
	}

	function checkPendingClockInOutStatus($job_id,$applicant_user_account_id){
		$prefix = $this->db->dbprefix;
		$this->db->select($prefix.'clockIn_clockOut.*');
		$this->db->where('job_id', $job_id);
		$this->db->where('applicant_user_account_id', $applicant_user_account_id);
		//$this->db->where('applicant_clockIn_status', '0');
		$query = $this->db->get($prefix.'clockIn_clockOut');
		return $data = $query->result();
		//return 	$query->num_rows();
	}

	/* function to get outlet manger Id by job Id */
	function get_outlet_manager_id($jobId){
		$prefix = $this->db->dbprefix;
		$this->db->select($prefix.'jobs.*');
		$this->db->where('job_id', $jobId);
		$query = $this->db->get($prefix.'jobs');
		//echo $this->db->last_query(); die;
		$data = $query->result();
		return 	$data[0];
	}

	function get_outlet_id($outletManagerId){
		$prefix = $this->db->dbprefix;
		$this->db->select('outlet_id');
		$this->db->where('user_accounts_id', $outletManagerId);
		$query = $this->db->get($prefix.'outlet_managers');
		return $query->result();

	}
	/* Function to get email addresses of Applicants on from user_accounts table */

	function get_applicants_email(){
		$prefix = $this->db->dbprefix;
		$this->db->select('email_id,user_accounts_id');
		$this->db->where('role_id', "4");
		$query = $this->db->get($prefix.'user_accounts');
		 return $query->result();
	}

	/** Open job section end **/

	/***************Active Job Section Start******************/

	function get_active_jobs($id='',$type,$pageNo='',$limit='') {
		if ($pageNo!="") {
			$offset = ($pageNo-1)*$limit;
		} else {
			$offset='0';
		}
		$prefix = $this->db->dbprefix;
		$this->db->select($prefix.'jobs.*');
		$this->db->select($prefix.'job_roles.name');
		$this->db->join($prefix.'job_roles', $prefix.'job_roles.job_role_id = jobs.job_type_id');
		// only those jobs for which applicants have applied
		//$this->db->join($prefix.'job_applicants', $prefix.'job_applicants.job_id = jod_jobs.job_id');
		$this->db->where(array('jobs.is_approved'=>'1', 'jobs.is_completed'=>'0', 'jobs.is_delete'=>'0', 'jobs.is_applicants_hired'=>'1'));
		if($id) {
			$this->db->where($prefix.'jobs.job_id',$id);
			$query = $this->db->get($prefix.'jobs');
			return $query->row();
		}
		if(LoginUserDetails('role_id') == 3)  {// if logged in user is Outlet Manger
			//$this->db->where($prefix.'jobs.outlet_manager_id',LoginUserDetails('userid'));
			$outlet_manger_account_id = LoginUserDetails('userid');
			$outlet_id =  LoginUserDetails('user_outlet_id');
			$where = "(".$prefix."jobs.job_created_by  = 0  AND  jod_jobs.outlet_manager_id = $outlet_manger_account_id  OR  jod_jobs.job_created_by  = 1 AND jod_jobs.outlet_id  = $outlet_id)";
			$this->db->where($where);

		}else{
			if(LoginUserDetails('role_id') == 5) {
				/* Modifications by Kratika for the active jobs list for the Area Manager */
				$area_manager_account_id = LoginUserDetails('userid');
				$area_manager_id = getAreaManagerId(LoginUserDetails('userid'));
				$data = get_outlets_by_area_manager_id($area_manager_id[0]->area_manager_id);
				$outlet_ids = array();
				foreach($data as $outlet){
					array_push($outlet_ids,$outlet->outlet_id);
				}
				$outlet_id_str = implode(",",$outlet_ids);
				$where = "(jod_jobs.created_by = $area_manager_account_id || (jod_jobs.job_created_by  = 1 && jod_jobs.outlet_id IN ($outlet_id_str)))";
				$this->db->where($where);
			}
			//$this->db->where($prefix.'jobs.is_applicants_hired',1); // Area Manager
		}

		$this->db->order_by($prefix.'jobs.created_at', "desc");
		if ($pageNo!="" && $limit!="") $this->db->limit($limit,$offset);
		$result =0;
		switch($type){
			case 'count':
			$result = $this->db->count_all_results($prefix.'jobs');
			break;

			case 'row':
			$query =$this->db->get($prefix.'jobs');
			$result = $query->row();
			break;

			case 'result':
			$query =$this->db->get($prefix.'jobs');
			$result = $query->result();
			break;
		}
		//echo $this->db->last_query(); die;
		return $result;
	}

	/***************Active Job Section End******************/

/***************Complete Job (Job History) Section Start******************/

	function get_single_job($id,$iscopy="") {
		$prefix = $this->db->dbprefix;
		$this->db->select($prefix.'jobs.*');
		$this->db->select($prefix.'job_roles.name');
		$this->db->join($prefix.'job_roles', $prefix.'job_roles.job_role_id = jobs.job_type_id');
		if($iscopy ==""){
			if(LoginUserDetails('role_id') == 3)  {// if logged in user is Outlet Manger
				$this->db->where($prefix.'jobs.outlet_manager_id',LoginUserDetails('userid'));
			}else{
				$this->db->where($prefix.'jobs.created_by',LoginUserDetails('userid')); // Area Manager
			}
		}
		if($id) {
			$this->db->where($prefix.'jobs.job_id',$id);
			$query = $this->db->get($prefix.'jobs');
			return $query->row();
		}

	}

	/***************Complete Job (Job History) End******************/



	/***************Complete Job (Job History) Section Start******************/

	function get_complete_jobs($id='',$type,$pageNo='',$limit='') {
		if ($pageNo!="") {
			$offset = ($pageNo-1)*$limit;
		} else {
			$offset='0';
		}
		$prefix = $this->db->dbprefix;
		$this->db->select($prefix.'jobs.*');
		$this->db->select($prefix.'job_roles.name');
		$this->db->join($prefix.'job_roles', $prefix.'job_roles.job_role_id = jobs.job_type_id');

		if(LoginUserDetails('role_id') == 3)  {// if logged in user is Outlet Manger
			$outlet_manger_account_id = LoginUserDetails('userid');
			$outlet_id =  LoginUserDetails('user_outlet_id');
			$where = "(".$prefix."jobs.job_created_by  = 0  AND  jod_jobs.outlet_manager_id = $outlet_manger_account_id  OR  jod_jobs.job_created_by  = 1 AND jod_jobs.outlet_id  = $outlet_id)";
			$this->db->where($where);
		}else{
			/* Modifications by Kratika for the completed jobs list for the Area Manager */
			$area_manager_account_id = LoginUserDetails('userid');
			$area_manager_id = getAreaManagerId(LoginUserDetails('userid'));
			$data = get_outlets_by_area_manager_id($area_manager_id[0]->area_manager_id);
			$outlet_ids = array();
			foreach($data as $outlet){
				array_push($outlet_ids,$outlet->outlet_id);
			}
			$outlet_id_str = implode(",",$outlet_ids);
			$where = "(jod_jobs.created_by = $area_manager_account_id || (jod_jobs.job_created_by  = 1 && jod_jobs.outlet_id IN ($outlet_id_str)))";
			$this->db->where($where);
		}
		if($id) {
			$this->db->where($prefix.'jobs.job_id',$id);
			$query = $this->db->get($prefix.'jobs');
			return $query->row();
		}
		//$this->db->where(array('jobs.is_approved'=>'1', 'jobs.is_completed'=>'1'));
		//$this->db->or_where(array('jobs.is_delete'=>'1'));
		$where = '((jod_jobs.is_approved ="1" and jod_jobs.is_completed = "1") or jod_jobs.is_delete = "1")';
		$this->db->where($where);
		$this->db->order_by($prefix.'jobs.created_at', "desc");
		if ($pageNo!="" && $limit!="") $this->db->limit($limit,$offset);
		$result =0;
		switch($type){
			case 'count':
			$result = $this->db->count_all_results($prefix.'jobs');
			break;

			case 'row':
			$query =$this->db->get($prefix.'jobs');
			$result = $query->row();
			break;

			case 'result':
			$query =$this->db->get($prefix.'jobs');
			$result = $query->result();
			break;
		}
		//echo $this->db->last_query(); die;
		return $result;
	}

	/***************Complete Job (Job History) End******************/


	/** Outlet section start**/

	function get_enable_outlet($id='') {
		$area_manager_detail = getAreaManagerId(LoginUserDetails('userid'));
		$prefix = $this->db->dbprefix;
		$this->db->select($prefix.'outlets.*');
		$this->db->select($prefix.'companies.company_name');
		$this->db->join($prefix.'outlets', $prefix.'outlets.company_id = companies.company_id');
		$this->db->where($prefix.'outlets.is_delete','0');
		$this->db->where($prefix.'outlets.status','1');
		$this->db->where($prefix.'outlets.company_id',LoginUserDetails('company_id'));
		if($id) {
			$this->db->where($prefix.'outlets.outlet_id',$id);
			$query = $this->db->get($prefix.'companies');
			return $query->row();
		}
		if(LoginUserDetails('role_id') == 5) {
			$this->db->where($prefix.'outlets.area_manager_id',$area_manager_detail[0]->area_manager_id);
		}
		$this->db->order_by($prefix.'outlets.created_at', "desc");
		$query = $this->db->get($prefix.'companies');
		//echo $this->db->last_query(); die;
		return $query->result();
	}

	function get_outlet_manager_by_outlet($outletId='') {
		$prefix = $this->db->dbprefix;
		$this->db->select('*');
		$this->db->where('outlet_id',$outletId);
		$this->db->order_by('first_name', "asc");
		$query = $this->db->get($prefix.'outlet_managers');
		return $query->result();
	}
	/** Outlet section end**/

	/********	hq manager job notification section : start	************/

/*----- commented HQ notification part
	function get_notification($id='',$type,$pageNo='',$limit='') {
		if ($pageNo!="") {
			$offset = ($pageNo-1)*$limit;
		} else {
			$offset='0';
		}
		$prefix = $this->db->dbprefix;
		$this->db->select($prefix.'jobs.*');
		$this->db->select($prefix.'job_roles.name');
		$this->db->select($prefix.'outlets.outlet_name');
		$this->db->join($prefix.'job_roles', $prefix.'job_roles.job_role_id = jobs.job_type_id');
		$this->db->join($prefix.'outlet_managers', $prefix.'outlet_managers.user_accounts_id = jobs.outlet_manager_id');
		$this->db->join($prefix.'outlets', $prefix.'outlets.outlet_id = outlet_managers.outlet_id');
		//$this->db->join($prefix.'outlets', $prefix.'outlets.outlet_id = jobs.outlet_id');
		$this->db->where($prefix.'outlets.company_id',LoginUserDetails('company_id'));
		$this->db->where($prefix.'jobs.is_delete','0');
		if($id) {
			$this->db->where($prefix.'jobs.job_id',$id);
			$query = $this->db->get($prefix.'jobs');
			return $query->row();
		}
		$this->db->order_by($prefix.'jobs.created_at', "desc");
		if ($pageNo!="" && $limit!="") $this->db->limit($limit,$offset);
		$result =0;
		switch($type){
			case 'count':
			$this->db->where($prefix.'jobs.is_approved',0);
			$this->db->or_where($prefix.'jobs.is_completed',1);
			$result = $this->db->count_all_results($prefix.'jobs');
			break;

			case 'row':
			$query =$this->db->get($prefix.'jobs');
			$result = $query->row();
			break;

			case 'result':
			$query =$this->db->get($prefix.'jobs');
			$result = $query->result();
			break;


		}
		//echo $this->db->last_query(); die;
		return $result;

	}

	function get_hq_manager_notification(){
		$prefix = $this->db->dbprefix;
		$this->db->select($prefix.'jobs.*');
		$this->db->join($prefix.'outlet_managers', $prefix.'outlet_managers.user_accounts_id = jobs.outlet_manager_id');
		$this->db->join($prefix.'outlets', $prefix.'outlets.outlet_id = outlet_managers.outlet_id');
		$this->db->where($prefix.'outlets.company_id',LoginUserDetails('company_id'));
		$this->db->where($prefix.'jobs.is_delete','0');

		$this->db->order_by($prefix.'jobs.created_at', "desc");
		$result =0;
		$this->db->where($prefix.'jobs.is_approved',0);
		$this->db->or_where($prefix.'jobs.is_completed',1);
		$result = $this->db->count_all_results($prefix.'jobs');

		//echo $this->db->last_query(); die;
		return $result;
	}
	----- commented HQ notification part
	* 	*/

		/********	hq manager job notification section : end	************/

	/******* hq manager job templates section  ******************************/

	/* This function is to add the template in the DB ***/
	function add_templates($data){
		$prefix = $this->db->dbprefix;
		return $this->db->insert($prefix.'templates', $data);
	}

	function update_templates($id,$data){
		$prefix = $this->db->dbprefix;
		$this->db->where('template_id', $id);
		return $this->db->update($prefix.'templates' ,$data);
	}

	function fetch_template_by_id($id){
		$prefix = $this->db->dbprefix;
		$this->db->select('*');
		$this->db->where('template_id',$id);
		$query = $this->db->get($prefix.'templates');
		return $query->result();
		//return $query->result();
	}

	function get_templates($search_str="",$length=0,$start=0,$orderBy="",$column_name=""){
		//echo "sssss". $column_name; die;
		$prefix = $this->db->dbprefix;
		$this->db->select('*');
		$this->db->where('is_delete','0');
		$this->db->where('company_id',LoginUserDetails('company_id'));

		if($column_name != ""){
			$this->db->order_by($column_name, $orderBy);
		}else{
			//$this->db->order_by("created_at", "desc");
		}
		if($length!=0) {
			$this->db->limit($length,$start);
		}
		if($search_str !=""){
			$this->db->where("(template_name LIKE '%".$search_str."%' OR  job_title LIKE '%".$search_str."%' OR job_description LIKE '%".$search_str."%')");
		}
		$query = $this->db->get($prefix.'templates');
		//echo $this->db->last_query(); die;
		return $query->result();
	}


	function get_outlet_manager_by_id($id){
		$prefix = $this->db->dbprefix;
		$this->db->select('*');
		$this->db->where('user_accounts_id',$id);
		$query = $this->db->get($prefix.'outlet_managers');
		$data =  $query->result();
		return $data[0];
	}

	function get_outlet_name_by_jobid($outlet_id){
		$prefix = $this->db->dbprefix;
		$this->db->select('outlet_name');
		$this->db->where('outlet_id',$outlet_id);
		$query = $this->db->get($prefix.'outlets');
		$data =  $query->result();
		return $data[0]->outlet_name;
	}

	function get_outlet_data($outletId){
		$prefix = $this->db->dbprefix;
		$this->db->select('*');
		$this->db->where('outlet_id',$outletId);
		$query = $this->db->get($prefix.'outlets');
		return $query->result();
	}

	function get_HQ_Manager_Data($outletId){
		$prefix = $this->db->dbprefix;
		$this->db->select($prefix.'outlets.company_id');
		$this->db->select($prefix.'headquater_manager.*');
		$this->db->join($prefix.'headquater_manager', $prefix.'outlets.company_id = headquater_manager.company_id');
		$this->db->where($prefix.'outlets.outlet_id',$outletId);
		$query = $this->db->get($prefix.'outlets');
		$data = $query->result();
		return $data[0];
	}

	/*===================function for getting applicants who applied for job=====================*/

	function get_applied_applicants($job_id, $type="",$page=""){
		if($type=='count'){
			if($page="history_page"){
				$this->db->where('job_applicants.is_delete', 0);
			}else{
				$this->db->where('job_applicants.is_delete', 0);
			}
			$this->db->select('count(jod_job_applicant_id) as candidate_count');

		}else{
			if($type!=3 && $type!=4 ){
				$this->db->where('job_applicants.is_delete', 0);
				$this->db->where('job_applicants.system_rejected', 0);
				$this->db->where('job_applicants.status',$type);
			}
			$this->db->select('job_applicants.*, job_applicants.is_delete as cancel_status, job_applicants.status as job_status, applicants.*');
			//$this->db->from('job_applicants');
			$this->db->join('applicants', 'applicants.user_account_id = job_applicants.applicant_user_account_id');
		}
		$this->db->where('job_applicants.job_id',$job_id);
		$query = $this->db->get('job_applicants');
		if($type=='count'){
			return $query->row()->candidate_count;
		}
		//echo $this->db->last_query();die;
		return $query->result();

	}

	function get_all_jobs_applicants($job_id){

		$this->db->select('job_applicants.*');
		$this->db->where('job_applicants.job_id',$job_id);
		$query = $this->db->get('job_applicants');

		return $query->result();
	}


	/*===================function for getting job details=====================*/

	function get_job_details($job_id){
		$this->db->select('jobs.*, job_roles.name as job_type_name');
		$this->db->join('job_roles', 'job_roles.job_role_id = jobs.job_type_id');
		$this->db->where('jobs.job_id',$job_id);
		$query = $this->db->get('jobs');
		return $query->row();
	}

	/*===================function for getting job's count s per status=====================*/

	function get_job_count($status='', $user_type=''){
		$user_id =  LoginUserDetails('userid');
		if($status=='open'){
			$this->db->where(array('is_completed'=>'0'));
		}
		if($status=='completed'){
			$this->db->where(array('is_approved'=>'1', 'is_completed'=>'1'));
		}
		if($user_type == "3" || $user_type == "5"){ // if user type is OM and Area manager
			$this->db->where(array('is_applicants_hired'=>'0'));
		}
		if($user_type == "3") { // Outlet Manager
			$this->db->where(array('outlet_manager_id'=>$user_id));
		}else if($user_type == "5"){
			$this->db->where(array('created_by'=>$user_id));
		}
		$this->db->where(array('status'=>'0', 'is_delete'=>'0'));
		$this->db->select('count(job_id) as job_count');
		$query = $this->db->get('jobs');
		//echo $this->db->last_query();die;
		return $query->row()->job_count;
	}

	/*===================function for hired applicants count=====================*/

	function get_hired_applicants_count($date='', $job_id=''){
		if(!empty($date)){
			$this->db->where(array('date(hired_at)'=>$date, 'status'=>'1'));
		}
		if(!empty($job_id)){
			$this->db->where(array('job_id'=>$job_id, 'status'=>'1'));
		}
		$this->db->select('count(jod_job_applicant_id) as candidate_count');
		$query = $this->db->get('job_applicants');
			//echo $this->db->last_query();die;
		return $query->row()->candidate_count;
	}


	function get_rejected_applicant($job_id){
		$this->db->select('job_applicants.*, job_applicants.status as job_status, applicants.*, job_applicants.is_delete as self_cancel_status');
		$this->db->join('applicants', 'applicants.user_account_id = job_applicants.applicant_user_account_id');
		//$this->db->where('job_applicants.status', 1);
		//$this->db->or_where('job_applicants.status', 2);
		$where1 = "(jod_job_applicants.status = 1 OR jod_job_applicants.status = 2)";
		$this->db->where($where1);
		$this->db->where('job_applicants.job_id',$job_id);
		$query = $this->db->get('job_applicants');
		//echo $this->db->last_query();die;
		return $query->result();
	}

	/*===================function for hired applicants count=====================*/

	function changeCandidateJobStatus($job_id, $applicant_user_id, $date){
		$where1 = "job_id = ".$job_id." AND applicant_user_account_id IN (".$applicant_user_id.")";
		$where2 = "job_id = ".$job_id." AND applicant_user_account_id NOT IN (".$applicant_user_id.")";
		$this->db->where($where1);
		$update_data = array('status'=>1, 'hired_at'=>$date);
		$this->db->update('job_applicants', $update_data);
		$this->db->where($where2);
		$update_data = array('status'=>2, 'rejected_by_manager_at'=>$date);
		$is_update = $this->db->update('job_applicants', $update_data);
		return $is_update;
	}

	/*** Function to update status of the jobs after Outlet Manager hire the applicants */
	function updateJobStatusForHired($job_id){
		$this->db->where(array('job_id'=>$job_id));
		$update_data = array('is_applicants_hired'=>1);
		$is_update = $this->db->update('jobs', $update_data);
		return $is_update;
	}

	function get_enable_outlet_for_HQ_Manager($id='') {
		$prefix = $this->db->dbprefix;
		$this->db->select($prefix.'outlets.*');
		$this->db->select($prefix.'companies.company_name');
		$this->db->join($prefix.'outlets', $prefix.'outlets.company_id = companies.company_id');
		$this->db->where($prefix.'outlets.is_delete','0');
		$this->db->where($prefix.'outlets.status','1');
		$this->db->where($prefix.'outlets.company_id',LoginUserDetails('company_id'));
		if($id) {
			$this->db->where($prefix.'outlets.outlet_id',$id);
			$query = $this->db->get($prefix.'companies');
			return $query->row();
		}
		$this->db->order_by($prefix.'outlets.created_at', "desc");
		$query = $this->db->get($prefix.'companies');
		$this->db->limit(3,0);
//		echo $this->db->last_query(); die;
		return $query->result();
	}

	function get_open_jobs_by_outletId($outletId,$jobtype,$type){
		$prefix = $this->db->dbprefix;
		if($jobtype == "open"){
			$this->db->where(array('outlet_id'=>$outletId, 'is_delete'=>'0','is_completed'=>'0','is_applicants_hired'=>'0'));
		}else{
			$this->db->where(array('outlet_id'=>$outletId, 'is_delete'=>'0'));
		}
		$this->db->order_by('created_at', "desc");
		switch($type){
			case 'count':
			$result = $this->db->count_all_results($prefix.'jobs');
			break;

			case 'result':
			$query =$this->db->get($prefix.'jobs');
			$result = $query->result();
			break;
		}
		//echo $this->db->last_query(); die;
			return $result;
	}

	function get_hired_applicants($job_id,$type="",$page ='',$only_feedback="0"){
		if($type=='count'){
			$this->db->select('count(jod_job_applicant_id) as candidate_count');
			if($page!="" && $page =="cancel"){
				$this->db->where('job_applicants.is_delete',1);
				if($only_feedback == 1){
					$this->db->where('job_applicants.system_rejected',0);
				}
			}else{
				$this->db->where('job_applicants.acknowledged',1);
				$this->db->where('job_applicants.status',1);
			}
		}else{
			$this->db->where('job_applicants.status',1);
			//$this->db->where('job_applicants.acknowledged',1);
			$this->db->select('job_applicants.*, job_applicants.status as job_status, job_applicants.is_delete as cancel_status, applicants.*');
			//$this->db->from('job_applicants');
			$this->db->join('applicants', 'applicants.user_account_id = job_applicants.applicant_user_account_id');
		}

		if($page!="" && $page=="at_feedback"){
			$this->db->where('job_applicants.acknowledged',1);
			//echo "sas"; die;
			$this->db->where('job_applicants.is_delete',0);
			$this->db->where('job_applicants.is_feedback_given',0);
		}
		$this->db->where('job_applicants.job_id',$job_id);
		$query = $this->db->get('job_applicants');
		if($type=='count'){
			return $query->row()->candidate_count;
		}
		//echo $this->db->last_query(); die;
		return $query->result();
	}

	/* ===================================== feedback section ============================== */
	function save_feedback($data){
		$prefix = $this->db->dbprefix;
		$this->db->insert($prefix.'applicants_feedback', $data);
		return $this->db->insert_id();
	}

	function update_applicants_job($job_id,$applicant_id){
		$data = array("completed_by_manager" => 1,"is_feedback_given"=> 1);
		$prefix = $this->db->dbprefix;
		$this->db->where('job_id', $job_id);
		$this->db->where('applicant_user_account_id', $applicant_id);
		return $this->db->update($prefix.'job_applicants' ,$data);
	}

	function compelete_job($job_id){
		$data = array("is_completed" => 1);
		$prefix = $this->db->dbprefix;
		$this->db->where('job_id', $job_id);
		return $this->db->update($prefix.'jobs' ,$data);
	}

	/* =========================== Cancel Job Section start =================================== */
	function cancel_job($job_id,$applicant_count){
		$data = array("is_delete" => 1);
		$prefix = $this->db->dbprefix;
		$this->db->where('job_id', $job_id);
		return $this->db->update($prefix.'jobs' ,$data);
	}

	function delete_notification($table,$label,$val,$status){
		$prefix = $this->db->dbprefix;
		$this->db->delete($prefix.$table, array($label => $val,$status=>'0'));
	}

	function cancel_job_for_applied_candidates($job_id,$applicant_id){
		$data = array("is_delete" => 1);
		$prefix = $this->db->dbprefix;
		$this->db->where('job_id', $job_id);
		$this->db->where('applicant_user_account_id', $applicant_id);
		return $this->db->update($prefix.'job_applicants' ,$data);
	}
	/* =========================== Cancel Job Section  end =================================== */

	/******--------------------------- User profile section start ---------------************/
	function applicant_profile($user_accountid){

		$prefix = $this->db->dbprefix;
		$this->db->select($prefix.'user_accounts.*');
		$this->db->select($prefix.'applicants.*');
		$this->db->join($prefix.'applicants', $prefix.'user_accounts.user_accounts_id = applicants.user_account_id');
		$this->db->where('user_accounts.user_accounts_id',$user_accountid);
		$query = $this->db->get($prefix.'user_accounts');
		$data = $query->result();
		return $data[0];
	}

	function applicants_active_job($user_accountid,$isSuperAdmin=''){
			$prefix = $this->db->dbprefix;
			$this->db->select($prefix.'jobs.*');
			$this->db->select($prefix.'job_applicants.*');
			$this->db->join($prefix.'job_applicants', $prefix.'jobs.job_id = job_applicants.job_id');
			$this->db->order_by($prefix.'jobs.is_completed', 0);
			if($isSuperAdmin==1){
				//
			}else{
				if(LoginUserDetails('role_id') == 3)  {// if logged in user is Outlet Manger
					$this->db->where($prefix.'jobs.outlet_manager_id',LoginUserDetails('userid'));
				}else{
					$this->db->where($prefix.'jobs.created_by',LoginUserDetails('userid')); // Area Manager
				}
			}
			$this->db->where($prefix.'job_applicants.applicant_user_account_id	',$user_accountid);
			$this->db->where($prefix.'job_applicants.completed_by_manager	',0);
			$this->db->where($prefix.'job_applicants.acknowledged 	','1');
			$this->db->where($prefix.'job_applicants.status','1');
			$this->db->order_by($prefix.'job_applicants.created_at', "desc");
			$this->db->where($prefix.'jobs.is_applicants_hired', "1");
			$this->db->where($prefix.'jobs.is_delete', "0");
			//$this->db->limit(3,0);
			$query = $this->db->get($prefix.'jobs');
			//echo $this->db->last_query(); die;
			return $query->result();
	}


	function applicants_open_job($user_accountid,$isSuperAdmin=''){
			$prefix = $this->db->dbprefix;
			$this->db->select($prefix.'jobs.*');
			$this->db->select($prefix.'job_applicants.*');
			$this->db->join($prefix.'job_applicants', $prefix.'jobs.job_id = job_applicants.job_id');
			$this->db->where($prefix.'jobs.is_completed', 0);
			$this->db->where($prefix.'job_applicants.is_delete', "0");

			if($isSuperAdmin==1){

			}else{
				if(LoginUserDetails('role_id') == 3)  {// if logged in user is Outlet Manger
					$this->db->where($prefix.'jobs.outlet_manager_id',LoginUserDetails('userid'));
				}else{
					$this->db->where($prefix.'jobs.created_by',LoginUserDetails('userid')); // Area Manager
				}
			}

			//$this->db->where($prefix.'job_applicants.completed_by_manager',0);
			//$this->db->where($prefix.'job_applicants.acknowledged 	','1');
			//$this->db->where($prefix.'job_applicants.status','1');
			//$this->db->or_where($prefix.'job_applicants.status','0');
			$this->db->order_by($prefix.'job_applicants.created_at', "desc");
			$this->db->where($prefix.'jobs.is_applicants_hired', "0");
			$this->db->where($prefix.'jobs.is_delete', "0");
			$this->db->where($prefix.'job_applicants.applicant_user_account_id	',$user_accountid);
			//$this->db->limit(3,0);
			$query = $this->db->get($prefix.'jobs');
				//echo $this->db->last_query(); die;
			return $query->result();
	}

	function applicants_cancel_or_compeleted_jobs($user_accountid){
			$prefix = $this->db->dbprefix;
			$where1 = $prefix."jobs.is_delete = 1  OR " .$prefix."jobs.is_completed = 1 ";
			$this->db->select($prefix.'jobs.* , jobs.is_delete as cancel_job' );
			$this->db->select($prefix.'job_applicants.*');
			$this->db->join($prefix.'job_applicants', $prefix.'jobs.job_id = job_applicants.job_id');
			$this->db->where($where1);
			/*if(LoginUserDetails('role_id') == 3)  {// if logged in user is Outlet Manger
				$this->db->where($prefix.'jobs.outlet_manager_id',LoginUserDetails('userid'));
			}else{
				$this->db->where($prefix.'jobs.created_by',LoginUserDetails('userid')); // Area Manager
			} */
			$this->db->order_by($prefix.'job_applicants.created_at', "desc");
			$this->db->where($prefix.'job_applicants.applicant_user_account_id	',$user_accountid);
			$this->db->where($prefix.'job_applicants.status	!= ','2');
			//$this->db->limit(3,0);
			$query = $this->db->get($prefix.'jobs');
		//	echo $this->db->last_query(); die;
			return $query->result();
	}

	function get_area_manager_id($outletId){
		$prefix = $this->db->dbprefix;
		$this->db->select('area_manager_id');
		$this->db->where('outlet_id',$outletId);
		$query = $this->db->get($prefix.'outlets');
		$data = $query->result();
		return $data;
	}

	function get_area_manager_details($id){
		$prefix = $this->db->dbprefix;
		$this->db->select('*');
		$this->db->where('area_manager_id',$id);
		$query = $this->db->get($prefix.'area_manager');
		$data =  $query->result();
		return $data[0];
	}

	function add_email_notification($data='') {

		$prefix = $this->db->dbprefix;
		$this->db->insert($prefix.'email_notifications', $data);
		return $this->db->insert_id();
	}

	function add_push_notification($data='') {
		$prefix = $this->db->dbprefix;
		$this->db->insert($prefix.'notification', $data);
		return $this->db->insert_id();
	}

	function get_applicants_feedback($job_id) {
		$prefix = $this->db->dbprefix;
		$this->db->select($prefix.'applicants_feedback.*');
		$this->db->select($prefix.'applicants.*');
		$this->db->join($prefix.'applicants', $prefix.'applicants_feedback.applicant_user_account_id = applicants.user_account_id');
		$this->db->where($prefix.'applicants_feedback.job_id', $job_id);
		$query = $this->db->get($prefix.'applicants_feedback');
		return $query->result();
	}

	function getJobStatusForHired($jobId){
		$prefix = $this->db->dbprefix;
		$this->db->select($prefix.'jobs.is_applicants_hired');
		$this->db->where($prefix.'jobs.is_applicants_hired', 1);
		$this->db->where($prefix.'jobs.job_id', $jobId);
		$query = $this->db->get($prefix.'jobs');
		return $query->result();
	}

	function get_not_acknowleged_applicants($jobId,$type=""){
		$this->db->where('job_applicants.status',1);
		$this->db->select('job_applicants.*, job_applicants.status as job_status, job_applicants.is_delete as cancel_status, applicants.*');
		$this->db->join('applicants', 'applicants.user_account_id = job_applicants.applicant_user_account_id');
		if($type!="" && $type="not_ack"){
			$this->db->where('job_applicants.acknowledged',0);
			$this->db->where('job_applicants.is_delete',0);
		}elseif($type!="" && $type="not_ack_cancel"){
				$where1 = $prefix."job_applicants.is_delete = 1  OR " .$prefix."job_applicants.acknowledged = 0 OR " .$prefix."job_applicants.acknowledged = 1 ";
				$this->db->where($where1);
		}else{
			$this->db->where('job_applicants.acknowledged',0);
			$this->db->or_where('job_applicants.is_delete',1);
		}
		$this->db->where('job_applicants.job_id',$jobId);
		$query = $this->db->get('job_applicants');
		//echo $this->db->last_query(); die;
		return $query->result();
	}

	function reject_candidate($jobId,$applicant_account_id){
		$data = array("status" => 2);
		$prefix = $this->db->dbprefix;
		$this->db->where('job_id', $jobId);
		$this->db->where('applicant_user_account_id', $applicant_account_id);
		return $this->db->update($prefix.'job_applicants' ,$data);
	}

		function update_meal_hours($job_id,$applicant_id,$data){
		$prefix = $this->db->dbprefix;
		$this->db->where('job_id', $job_id);
		$this->db->where('applicant_user_account_id', $applicant_id);
		//var_dump(); die;
		return $this->db->update($prefix.'job_applicants' ,$data);
	}

	function to_get_meal_hours($job_id,$applicant_id){
		$prefix = $this->db->dbprefix;
		$this->db->select('meal_hours');
		$this->db->where('job_id', $job_id);
		$this->db->where('applicant_user_account_id', $applicant_id);
		$query = $this->db->get($prefix.'job_applicants');
		$data =  $query->result();
		return $data[0];
	}

	function reject_status($job_id,$applicant_id){
		$where1 = "job_id = ".$job_id." AND applicant_user_account_id  = ".$applicant_id;
		$this->db->where($where1);
		$date = date('Y-m-d h:i:s');
		$update_data = array('status'=>2,'rejected_by_manager_at'=>$date);
		$is_update = $this->db->update('jod_job_applicants', $update_data);
		return $is_update;
	}

	public function system_cancellation($job_id,$applicant_id){
		$where1 = "job_id = ".$job_id." AND applicant_user_account_id  = ".$applicant_id;
		$this->db->where($where1);
		$update_data = array('system_rejected'=>1,"is_delete" => 1);
		$is_update = $this->db->update('jod_job_applicants', $update_data);
		return $is_update;

	}

	function reject_otherjobs_applicants($applicant_id,$job_details=array(),$for_open_jobs=0){

		$prefix = $this->db->dbprefix;
		$this->db->select($prefix.'job_applicants.*');
		$this->db->join($prefix.'jobs', $prefix.'job_applicants.job_id = '.$prefix.'jobs.job_id');
		$this->db->where("((ADDTIME(".$prefix."jobs.start_date".",".$prefix."jobs.start_time) BETWEEN'".$job_details->start_date." ".$job_details->start_time."' AND '".$job_details->end_date." ".$job_details->end_time."') OR ( (ADDTIME(".$prefix."jobs.end_date".",".$prefix."jobs.end_time) BETWEEN '".$job_details->start_date." ".$job_details->start_time."' AND '".$job_details->end_date." ".$job_details->end_time."')) )");
		$this->db->where($prefix.'job_applicants.status', '0');
		$this->db->where($prefix.'jobs.job_id != ',$job_details->job_id);
		$this->db->where($prefix.'job_applicants.applicant_user_account_id', $applicant_id);
		if($for_open_jobs == 1){
			$this->db->where($prefix.'jobs.is_applicants_hired', 0);
		}
		$query = $this->db->get($prefix.'job_applicants');
		//echo $this->db->last_query(); die;
		return $query->result();
	}

	function rejected_applicants($job_id){
		$this->db->select('job_applicants.applicant_user_account_id');
		$this->db->where('job_applicants.job_id',$job_id);
		$this->db->where("(jod_job_applicants.status = '2' OR jod_job_applicants.system_rejected = '1')");
		$query = $this->db->get('job_applicants');
		//echo $this->db->last_query(); die;
		return $query->result();
	}

	function update_system_rejected_applicants($applicant_id,$job_id){
		$where1 = "job_id = ".$job_id." AND applicant_user_account_id  = ".$applicant_id;
		$this->db->where($where1);
		$update_data = array('system_rejected'=>0,"is_delete" => 0);
		$is_update = $this->db->update('jod_job_applicants', $update_data);
		return $is_update;
	}
	
	function fetch_payment_details($jobId,$applicant_user_account_id){
		$this->db->select('job_applicants.payment_amount_per_hour,job_applicants.meal_hours,job_applicants.acknowledged,job_applicants.ack_approval');
		$this->db->where('job_applicants.job_id',$jobId);
		$this->db->where('job_applicants.applicant_user_account_id',$applicant_user_account_id);
		$query = $this->db->get('job_applicants');
		return $query->result();
	}
	
	function get_job_applicant_data($jobId,$applicant_user_account_id){
		$this->db->select('*');
		$this->db->where('job_applicants.job_id',$jobId);
		$this->db->where('job_applicants.applicant_user_account_id',$applicant_user_account_id);
		$query = $this->db->get('job_applicants');
		return $query->result();
		
	}
	
	function payment_data_already_exist($jobId,$applicant_user_account_id){
		$this->db->select('*');
		$this->db->where('jod_applicants_payment_by_admin.job_id',$jobId);
		$this->db->where('jod_applicants_payment_by_admin.applicants_user_account_id',$applicant_user_account_id);
		$query = $this->db->get('jod_applicants_payment_by_admin');
		return $query->result();
		
	}
	
	function save_appliant_payment_data($data){
		$prefix = $this->db->dbprefix;
		$this->db->insert($prefix.'applicants_payment_by_admin', $data);
		return $this->db->insert_id();	
	}
	
	function save_appliant_transaction_data($data){
		$prefix = $this->db->dbprefix;
		$this->db->insert($prefix.'applicants_transactions', $data);
		return $this->db->insert_id();	
	}
	
	function update_company_data($id='',$data='') {
		$prefix = $this->db->dbprefix;
		$this->db->where('company_id', $id);
		return $this->db->update($prefix.'companies' ,$data);
	}
	
	function check_applicant_feedback_data($applicant_user_accountid,$job_id,$manager_id=0){
		$this->db->select('*');
		$this->db->where('jod_applicants_feedback.job_id',$job_id);
		$this->db->where('jod_applicants_feedback.applicant_user_account_id',$applicant_user_accountid);
		if($manager_id !=0){
			$this->db->where('jod_applicants_feedback.manager_id',$manager_id);
		}
		$query = $this->db->get('jod_applicants_feedback');
		return $query->result();
	}
	
	function update_bank_details($update_data,$applicant_id,$job_id){
		$prefix = $this->db->dbprefix;
		$this->db->where('applicant_user_account_id', $applicant_id);
		$this->db->where('job_id', $job_id);
		return $this->db->update($prefix.'job_applicants' ,$update_data);
		
	}
	
	public function get_clockin_not_clock_out_applicants($job_id){
		$prefix = $this->db->dbprefix;
		$this->db->where('job_id', $job_id);
		$this->db->where('status', 1);
		$this->db->where('clockInOutStatus', 1);
		$this->db->where('is_delete',0);
		$query = $this->db->get('jod_job_applicants');
		return $query->result();
	}
	
}



?>
