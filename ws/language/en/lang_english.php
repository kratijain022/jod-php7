<?php

/********************************* COMMON MESSAGES - START *****************************************************/

$_lang['txt.register.error.required'] = "* Marked are required fields";
$_lang['txt.register.error.invalidemail'] = "Please enter valid email format.";
$_lang['txt.register.error.unique'] = "Unique_id is already registered.";
$_lang['txt.register.success'] = "Thanks for registering with Jobs On Demand.";

$_lang['txt.login.error.required'] = "Username and password are required.";
$_lang['txt.login.error.usernotverfied'] = "User mobile number not verfied.";
$_lang['txt.login.error.notexist'] = "Username and Password not exist.";
$_lang['txt.login.success'] = "Login Successfully.";

$_lang['txt.change_user.error.required'] = "Secret Token is missing.";
$_lang['txt.change_user.success'] = "Profile updated Successfully.";
$_lang['txt.change_user.error.unable'] = "Unable to update profile.";
$_lang['txt.change_user.error.session'] = "Please login with valid credentials.";

$_lang['txt.change_user_password.error.required'] = "Password fields can not be black.";
$_lang['txt.change_user_password.error.current_password'] = "Current password is not match.";
$_lang['txt.change_user_password.success'] = "Password Updated successfully.";

$_lang['txt.reset_user_password.error.required'] = "Unique id and Email id are required.";
$_lang['txt.reset_user_password.error.notexist'] = "Unique id or email  is not available, Please check again.";
$_lang['txt.reset_user_password.success'] = "New password information has been sent to user email id. Please check your email.";

$_lang['txt.refer_friend.error.alreadyexist'] = "Referred user is already registered.";
$_lang['txt.refer_friend.success'] = "Your friend successfully refered.";

$_lang['txt.logout.success'] = "Logout successfully.";

$_lang['txt.get_education_institutes.error.notavailable'] = "There is no approved educational institutes in the list.";

$_lang['txt.get_job_roles.error.notavailable'] = "There is no approved job types in the list.";
$_lang['txt.apply_job.success'] = "You have applied for this job successfully.";
$_lang['txt.apply_job.error'] = "Errors occured while applying for job.";
$_lang['txt.get_dashboard_jobs.success'] = "Jobs listed successfully.";
$_lang['txt.get_dashboard_jobs.norecords.success'] = "There is no jobs associated with this user.";

$_lang['txt.get_job_detail.success'] = "Job details fetched sucessfully.";
$_lang['txt.get_jobs.error.invalid_job_status'] = "The job status you have sending is invalid.";
$_lang['txt.get_jobs.error.no_jobs_found'] = "No jobs found in this category.";
$_lang['txt.acknowledge_job.success'] = "You have acknowledge for this job successfully.";
$_lang['txt.completed_job.success'] = "You have completed this job successfully.";
$_lang['txt.completed_job.error'] = "Errors occured while completed for job.";
$_lang['txt.cancel_job.error'] = "Errors occured while cancel for job.";
$_lang['txt.cancel_job.success'] = "Job is cancelled successfully";
$_lang['txt.cancel_job_already.success'] = "Job is already cancelled successfully";

$_lang['txt.maintainance.success'] = "Services is in the maintainance mode.";
$_lang['txt.already_apply.error.required'] = "You have already apply for this job.";
$_lang['txt.already_acknowledge.error.required'] = "You have already acknowledge for this job.";
$_lang['txt.not_apply.error.required'] = "You are not hired for this job yet.";
$_lang['txt.job_not_approved.error.required'] = "This job is not approved yet.";

// start - added jhtang 20160528
$_lang['txt.job_completed.error.required'] = "The job has already completed. Thank you for the interest.";
// end - added jhtang 20160528

$_lang['txt.job_applicants_hired.error.required'] = "Applicants are already hired for this job.";
$_lang['txt.complete_job.error.rating'] = "Rating should be less then or equal to five.";
$_lang['txt.cancel_job.error.already'] = "The job for which you want to cancel is start already.";
$_lang['txt.cancel_job.error.not_able'] = "Job cancellation is not allowed 10 mins before the start of job.";
$_lang['txt.acknowledged_job.error.complete'] = "The job for which you try to acknowledge is already compeleted by Manager.";

/********************************* COMMON MESSAGES - END *****************************************************/

$_lang['txt.verify_user.error.required'] = "Unique id and verify code are required.";
$_lang['txt.verify_user.error.notexist'] = "Unique id OR verify code is not available, Please check again.";
$_lang['txt.verify_user.success'] 		 = "Unique id and verify code is available";

$_lang['txt.resend_verify_user.success'] = "Verify mobile verification code has been sent successfully.";

//resnd verification
$_lang['txt.resend_verify_user.error.required'] = "Unique id is required.";
$_lang['txt.resend_verify_user.error.notexist'] = "Unique id is not available, Please check again.";
$_lang['txt.resend_verify_time.error'] = "verify notification block for 1 hour.";
$_lang['txt.resend_verify_time_5minute1.error'] = "Too many retries.  Next retry in ";
$_lang['txt.resend_verify_time_5minute2.error'] = " minutes.";

$_lang['txt.start_job.error.required'] = "secret_token, job_id and clock_in_dt are required.";
$_lang['txt.start_job.error.notexist'] = "QR code does not belong to this job. Please check with manager.";
$_lang['txt.applicant.error.notexist'] = "Applicant user id is not available, Please check again.";
$_lang['txt.start_job.success'] 	   = "Job started successfully.";

$_lang['txt.start_job_already.error'] 	  = "You have already started this job.";
$_lang['txt.completed_job_already.error'] = "You have already completed this job.";


$_lang['txt.jobStarted.error'] 	   		 = "This Job can not be cancel because the job already started.";
$_lang['txt.clockinDateNotMatch.error']  = "QR code does not match clock-in date time for this job. Please check with manager.";
$_lang['txt.jobNotStarted.error'] 		 = "No clock in found.  Please check with outlet manager.";
$_lang['txt.clockoutDateNotMatch.error'] = "QR code does not match clock-out date time for this job. Please check with manager.";
$_lang['txt.already_apply.insurance_included.error.required'] = "Please upgrade your JOD mobile app. This version is no longer supported.";
$_lang['txt.already_apply.insurance_included.error.valid'] = "Insurance included field should be valid number or have value 1 or 0.";
$_lang['txt.complete_job.qr_code_belongs.error.required'] = "QR code does not belong to applicant. Please check with manager.";
$_lang['txt.clock_in_Date_timeNotMatch.error'] =    "QR code does not match clock-in date time for this job. Please check with manager.";
$_lang['txt.meal_hours_NotMatch.error'] =    "QR code does not match meal hours for this job. Please check with manager.";
$_lang['txt.start_job.error.rejected'] = "You are not able to start the job because you have rejected for this job";
$_lang['txt.already_apply.error.rejected_due_to_time_overlapping'] = "You have auto rejected by system due to time overlaping of the two jobs.";
$_lang['txt.already_apply.insurance_included.error.already-hired'] = "You are not able to apply for this job because you have hired for another job which is also have same start or end time.";
$_lang['txt.already_completed_feedback.error.required'] = "You have already given feedback for this job.";
$_lang['txt.feedback_job.error.alphanumeric'] = "Bank Account Name must be alphanumeric.  Eg. ABC123";
$_lang['txt.feedback_job.error.account_number'] = "Bank Account Number can only be in numbers.";
$_lang['txt.feedback_job.error.alphanumeric.bank_name'] = "Bank Name can must be alphanumeric.";
$_lang['txt.register_user.error.uniqueid.uppercase'] 	= "All Characters in the unique Id must be in uppercase.";
$_lang['txt.clock_out_job.success'] 	= "You have clock out this job successfully.";
$_lang['txt.register.error.contact_no'] 	= "Contact number is already registered.";
$_lang['txt.change_user.contact_no.already_exist'] 	= "Contact number is already registered with other user.";

?>
