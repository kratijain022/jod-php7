<?php 
	date_default_timezone_set("Asia/Calcutta");
	
	define('CURRENT_TIME',date('Y-m-d H:i:s'));
	define('DB_PREFIX','jod_');

	define('ENVIRONMENT',dirname(__FILE__)."/"."environment.php");
	if(file_exists(ENVIRONMENT)){
		include(ENVIRONMENT);
		
		 /// -- All to catch exception handling --
		try{
		
			/// -- check FIle exists or now --
			if(file_exists(BASEPATH.DS.LIBPATH.DS."common.php")){
				/// -- Include File exists or now --
				include(BASEPATH.DS.LIBPATH.DS."common.php");
				/// -- Log Debug Message --
				log_message("debug","common file included");
				/// -- Create Service Object --
				$resService=loadObject("restService");
				/// -- Init Service Object --
				$resService->init();
				/// -- See if need to set Encoding
				if(ENCODING != 'UTF-8')
				$resService->setEncoding(ENCODING);
				/// -- Set output format --
				$format="JSON";
					
				$result = $resService->processRequest($format);

				if($result==""){
					$result=$resService->execute($format);
					$result=$resService->processResponse($result,$format);
				} 

				ob_clean();
				echo $result;	

			}else{
				die("OOPS something goes wrong");
			}	
		}catch(Exception $e){
			die("OOPS something goes wrong");
		}	
		
	}else{
		die("OOPS something goes wrong");
	}	
  
?>
