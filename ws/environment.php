<?php ini_set("zlib.output_compression", "On");

    error_reporting(1);
    /// -- Get Output in Buffer --
    ob_start();

    /// -- Define base path to check it is a correct way to access --
    define('BASEPATH',dirname(__FILE__));
    /// -- Define Lib path --
    define('LIBPATH',"core");
    /// -- Define Directory seprator --
    define('DS',"/");
    /// -- Define Base file extension type --
    define('EXT',".php");
    /// -- Define Encoding --
    define('ENCODING',"UTF-8");

    /// -- Define URL path to pass in the push notification messages. --
    $base_url = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "https" : "http");
    $base_url .= "://". @$_SERVER['HTTP_HOST'];
    //$base_url .= "/zoneit_staging/";
    $base_url .= "/jobsondemand_qa/";
    define('URL_PATH',$base_url);

    $folder_url = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "https" : "http");
    $folder_url .= "://". @$_SERVER['HTTP_HOST'];
    $folder_url .= str_replace(basename($_SERVER['SCRIPT_NAME']),"",$_SERVER['SCRIPT_NAME']);
    define('FOLDER_URL_PATH',$folder_url);

    define('IMG_URL',URL_PATH."uploads/applicantes/");
   // echo IMG_URL; die;
    define('COMPANY_LOGO_URL',URL_PATH."uploads/company_logo/");
	define('NOIMAGE',URL_PATH.'templates/JOD/img/no_image.png');
    define('SITE_LOGO_URL',URL_PATH.'templates/JOD/img/logo-small.png');
    define('OUTLET_LOGO_URL',URL_PATH."uploads/outlet_logo/");
    define('CONTRACT_OF_SERVICE_PATH',$_SERVER["DOCUMENT_ROOT"]."/jobsondemand_qa/uploads/contract_of_service/");
    define('THUMB_IMG_URL',URL_PATH."uploads/thumb_img/");
    //define('IMG_FOLDER_PATH',$_SERVER["DOCUMENT_ROOT"]."/jobs_on_demand/uploads/");
    //define('IMG_FOLDER_PATH',$_SERVER["DOCUMENT_ROOT"]."/uploads/applicantes/");
    define('IMG_FOLDER_PATH',$_SERVER["DOCUMENT_ROOT"]."/jobsondemand_qa/uploads/applicantes/");
    define('TEST_LOG',$_SERVER["DOCUMENT_ROOT"]."/jobsondemand_qa/uploads/logs/test_api.txt");

    $hostNameCheck = $_SERVER['HTTP_HOST'];
    define('IMAGE_FETCH_PATH',FOLDER_URL_PATH."getImage.php?");

    $minSupportedAppVersion = "1.0";
    define('MIN_SUPPORTED_APP_VERSION',$minSupportedAppVersion);
    $restaurantAppURL       = "https://www.google.co.in/";
    define('RESTAURANT_APP_DOWNLOAD_URL',$restaurantAppURL);
    $userAppURL     = "https://www.google.co.in/";
    define('USER_APP_DOWNLOAD_URL',$userAppURL);
    define('SMS_API_CONSTANT',1); /* 1 for the transmit api ad 2 for the one way sms gateway */
    
?>
