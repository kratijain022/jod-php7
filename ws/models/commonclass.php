<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class commonclass {

    public function __construct() {
        $this->_db = env::getInst();
    }
    public function langText($locale,$string){
        header('Content-Type: text/html; charset=utf-8');
        if($locale=='de') {
			$lang=load_lang($locale,"german");
        } else {
			$lang=load_lang($locale,"english");
        }
        return $lang["$string"];
    }

    public function locale() {
        if(!empty($_REQUEST['locale']) && $_REQUEST['locale']=='de') {
            $locale = $_REQUEST['locale'];
        } else {
            $locale = 'en';
        }
        return $locale;
    }

    // start - added by jhtang 20160227
    public function check_user_registered($uniqueId,$contact_no,$email="") {
		if($email != ""){
			$query = "SELECT `user_accounts_id` FROM `".DB_PREFIX."user_accounts` WHERE `unique_id` = '$uniqueId' AND `email_id`= '$email'";
		}else{
			$query = "SELECT `user_accounts_id` FROM `".DB_PREFIX."user_accounts` WHERE `unique_id` = '$uniqueId'";
		}
		//echo $query; die;
		$getUserUniqueId = $this->_db->my_query($query);
		if ($this->_db->my_num_rows($getUserUniqueId) > 0) {
			$row = $this->_db->my_fetch_object($getUserUniqueId);
            $user_id = $row->user_accounts_id;
            return $user_id;
        }
	}
    // start - added by jhtang 20160227
    
    // Added by kratika to check contact number is already exist in our DB 
    
    public function check_mobile_number_already_exist($contact_no){
		$query = "SELECT `contact_no` FROM `".DB_PREFIX."applicants` WHERE `contact_no` = '$contact_no'";
		$getUserContactNo = $this->_db->my_query($query);
		if ($this->_db->my_num_rows($getUserContactNo) > 0) {
			$row = $this->_db->my_fetch_object($getUserContactNo);
			$user_id = $row->contact_no;
			return $user_id;
		}else{
			return 0;
		}
	}
	
	public function check_mobile_number_already_exist_for_other_users($contact_no,$user_account_id){
		$query = "SELECT `contact_no` FROM `".DB_PREFIX."applicants` WHERE `contact_no` = '$contact_no' AND user_account_id NOT IN  ('$user_account_id')";
		$getUserContactNo = $this->_db->my_query($query);
		if ($this->_db->my_num_rows($getUserContactNo) > 0) {
			$row = $this->_db->my_fetch_object($getUserContactNo);
			$user_id = $row->contact_no;
			return $user_id;
		}else{
			return false;
		}
		
	}

    public function check_user_availability($uniqueId,$email="") {
		if($email != ""){
			$query = "SELECT `user_accounts_id` FROM `".DB_PREFIX."user_accounts` WHERE `unique_id` = '$uniqueId' AND `email_id`= '$email' AND `is_delete` = '0'";
		}else{
			$query = "SELECT `user_accounts_id` FROM `".DB_PREFIX."user_accounts` WHERE `unique_id` = '$uniqueId' AND `is_delete` = '0'";
		}
		//echo $query; die;
		$getUserUniqueId = $this->_db->my_query($query);
		if ($this->_db->my_num_rows($getUserUniqueId) > 0) {
			$row = $this->_db->my_fetch_object($getUserUniqueId);
            $user_id = $row->user_accounts_id;
            return $user_id;
        } else {
			return 0;
		}
	}

	public function test_input($data) {
		$data = trim($data);
		$data = stripslashes($data);
		$data = htmlspecialchars($data);
		return $data;
	}

	public function image_upload($imgurl) {
        $encoded_image      = $imgurl;
        $upload_path        = IMG_FOLDER_PATH;
        $decoded_image = base64_decode($encoded_image);
        $imgname       = md5(uniqid()) . ".png";
        file_put_contents($upload_path . $imgname, $decoded_image);
        return $imgname;
    }

// start - added by jhtang 20151103
public function getSslPage($url) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_REFERER, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
}
// end - added by jhtang 20151103

    public function getFullImgByteCode($imgName){
        $byteCode = "";
                // start - added by jhtang 20151103
//              $byteCode = file_get_contents(IMG_URL.$imgName);
                $byteCode = $this->getSslPage(IMG_URL.$imgName);
                // end - added by jhtang 20151103
        $imageEncoded = base64_encode($byteCode);
        return $imageEncoded;
    }

    public function get_user_account_id_by_email_id($emailId){
		$query = "SELECT `user_accounts_id` FROM `".DB_PREFIX."user_accounts` WHERE `email_id` = '$emailId' AND `is_delete` = '0' AND `status` = '1'";
		$getUserAccountId = $this->_db->my_query($query);
		if ($this->_db->my_num_rows($getUserAccountId) > 0) {
			$row = $this->_db->my_fetch_object($getUserAccountId);
            $userId = $row->user_accounts_id;
            return $userId;
        } else {
			return 0;
		}
	}

	public function check_session($secretToken) {
		$query = "SELECT `user_accounts_id` FROM `".DB_PREFIX."user_accounts` WHERE `secret_token` = '$secretToken' AND `is_delete` = '0' AND `status` = '1'";
		$getUserAccountId = $this->_db->my_query($query);
		if ($this->_db->my_num_rows($getUserAccountId) > 0) {
			$row = $this->_db->my_fetch_object($getUserAccountId);
            $userId = $row->user_accounts_id;
            return $userId;
        } else {
			return 0;
		}
	}


	/* This function is to get the profile details of the applicants on the basis of user account id */
	public function get_profile($accountId){
		$query = "SELECT * FROM `".DB_PREFIX."applicants` WHERE `user_account_id` = '$accountId' AND `is_delete` = '0'";
		$getProfile = $this->_db->my_query($query);
		if ($this->_db->my_num_rows($getProfile) > 0) {
			$row = $this->_db->my_fetch_object($getProfile);
            return $row;
        } else {
			return 0;
		}
	}

	public function get_job_history($accountId){
		$query = "SELECT * FROM `".DB_PREFIX."employment_histories` WHERE `user_account_id` = '$accountId' AND `is_delete` = '0'";
		$getJobHistory = $this->_db->my_query($query);
		if ($this->_db->my_num_rows($getJobHistory) > 0) {
			$job_history = array();
			while($row = $this->_db->my_fetch_object($getJobHistory)) {
				$array = array();
				$array['length_of_service']= $row->length_of_service;
				$array['employer'] = $row->employer;
				$job_types = explode(',',$row->job_types);
				$array['job_roles'] = $job_types;
				$job_history[] = $array;
			}
            return $job_history;
        } else {
			return 0;
		}
	}

	public function my_referral_code() {
		$alphabet = "ABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
		$pass = array();
		$alphaLength = strlen($alphabet) - 1;
		for ($i = 0; $i < 10; $i++) {
			$n = rand(0, $alphaLength);
			$pass[] = $alphabet[$n];
		}
		return implode($pass);
	}

	/* This function is to get number of firends on the basis of the referral code of the user*/
	public function get_number_of_friends($referral_code){
		$query = "SELECT count(*) as count FROM `".DB_PREFIX."applicants` WHERE referral_code = '$referral_code'";
		$numberOfFrnds  = $this->_db->my_query($query);
		$numberOfFrnds = $this->_db->my_fetch_object($numberOfFrnds);
		return   $numberOfFrnds->count;
	}
}
?>
