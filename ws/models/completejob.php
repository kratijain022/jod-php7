<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
    
date_default_timezone_set("Asia/Singapore");

class completejob{
	
	public function __construct() {
        $this->_db      = env::getInst();
        $this->_common  = new commonclass();
        $this->_user    = new user();
		$this->_sendpushandemail = new sendpushandemail();
        
    }
	
	public function auto_complete_job(){
		log_message('debug', '-------- Starting point of the complete job cron ------'. CURRENT_TIME);
		/* fetch those jobs which is not completed, not cancelled ,approved by HQ and applicant is already hired for the same job */
		$active_job_query = "SELECT job_id FROM jod_jobs WHERE is_approved = 1 AND is_delete = 0 AND is_completed = 0 AND is_applicants_hired = 1 AND ADDTIME(".DB_PREFIX."jobs.start_date".",".DB_PREFIX."jobs.start_time) < '".CURRENT_TIME."'" ;
		
		$active_job_result = $this->_db->my_query($active_job_query);
		if($this->_db->my_num_rows($active_job_result)>0){
			while($row = $this->_db->my_fetch_object($active_job_result)){
				log_message('debug', '-------- Cron is running for the job  ------'. $row->job_id);
				/* get applicants of the jobs and also check how many of done ack how many done cancel, rejected , clock in, clock out and job end time is passed from current date time */
				//$this->fetch_job_applicants_status($row->job_id); 
				 if($this->fetch_job_applicants_status($row->job_id) == true ){
					 log_message('debug', '-------- is_completed flag updated by cron for job :   ------'. $row->job_id. ' at '.CURRENT_TIME );
					$current_date 	= CURRENT_TIME;	
					$updateArray = array("updated_at" => $current_date,"is_completed" => 1);
					$whereClause = array("job_id" => $row->job_id);
					$update = $this->_db->UpdateAll("jod_jobs", $updateArray, $whereClause, "");	
				} 
			} 
		}
		/* fetch those jobs which is not completed, not cancelled ,approved by HQ and applicant is not hired and also not applied  OPEN JOBS*/
		$open_job_query = "SELECT job_id FROM jod_jobs WHERE is_approved = 1 AND is_delete = 0 AND is_completed = 0 AND is_applicants_hired = 0 AND ADDTIME(".DB_PREFIX."jobs.start_date".",".DB_PREFIX."jobs.start_time) < '".CURRENT_TIME."'";
		$open_job_result = $this->_db->my_query($open_job_query);
		if($this->_db->my_num_rows($open_job_result)>0){
			while($row = $this->_db->my_fetch_object($open_job_result)){
				/* if any of the applicant has not applied for the job then this job is considered to close */
					$current_date =  CURRENT_TIME;
					if($this->to_check_applicant_applied_for_job($row->job_id) == false){
					log_message('debug', '-------- is_completed flag updated by cron for open job :   ------'. $row->job_id. ' at '.CURRENT_TIME );
					$updateArray = array("updated_at" => $current_date,"is_completed" => 1);
					$whereClause = array("job_id" => $row->job_id);
					$update = $this->_db->UpdateAll("jod_jobs", $updateArray, $whereClause, "");
				}

			}

		}

		
		log_message('debug', '-------- End point of the complete job cron ------'. CURRENT_TIME);
	}
	
	public function fetch_job_applicants_status($job_id){
		/* fetch applicant who has also do clock out successfully */
		$clock_out_applicant_query = "SELECT is_feedback_given,completed_by_manager,job_id,jod_job_applicant_id FROM jod_job_applicants WHERE job_id ='".$job_id."' AND status = 1 AND is_delete = 0 AND acknowledged = 1 AND clockInOutStatus = 2 ";
	 	
		$clocked_out_applicant_res = $this->_db->my_query($clock_out_applicant_query);
		$clock_out_applicants = $this->_db->my_num_rows($clocked_out_applicant_res); 
		if($this->_db->my_num_rows($clocked_out_applicant_res) > 0){	
			while($data = $this->_db->my_fetch_object($clocked_out_applicant_res)){
				/* function to check the manager has done with the feedback and job completed by manager is zero , if so function update the flas completed by manager to 1 */
				if($data->is_feedback_given == 1 && $data->completed_by_manager == 0){
					/* if feedback data is exist  and completed by manager flag is 0 then update  the completed by manager flag to 1 */
					$current_date 	= CURRENT_TIME;	
					$updateArray = array("updated_at" => $current_date,"completed_by_manager" => 1);
					$whereClause = array("jod_job_applicant_id" => $data->jod_job_applicant_id);
					$update = $this->_db->UpdateAll("jod_job_applicants", $updateArray, $whereClause, "");		
				}
			}
		}
		
		
		/* fetch any of the applicants of the jobs who has acknowledged and also do clock in but remaining for the clock out */
		$clockedin_applicant_query = "SELECT job_id FROM jod_job_applicants WHERE job_id ='".$job_id."' AND status = 1 AND is_delete = 0 AND acknowledged = 1 AND clockInOutStatus = 1";

		$clockedin_applicant_res = $this->_db->my_query($clockedin_applicant_query);
		//$clocked_in_res = $this->_db->my_fetch_object($clockedin_applicant_res);
		if($this->_db->my_num_rows($clockedin_applicant_res) > 0){	
			return false;
		}
		
		/* fetch applicants who has done ack but not do clock in */
		$ack_but_not_clock_in_applicants_q = "SELECT job_id FROM jod_job_applicants WHERE job_id ='".$job_id."' AND status = 1 AND is_delete = 0 AND acknowledged = 1 AND clockInOutStatus = 0";
		$ack_applicants = $this->_db->my_query($ack_but_not_clock_in_applicants_q);
		if($this->_db->my_num_rows($ack_applicants) > 0){
				
				return false;
		}

		/* fetch any applicants who has selected and not done acknowledgment */
		$not_ack_applicants_query = "SELECT jod_job_applicant_id FROM jod_job_applicants WHERE job_id ='".$job_id."' AND status = 1 AND is_delete = 0 AND acknowledged = 0 AND clockInOutStatus = 0";
		$not_ack_applicants_res = $this->_db->my_query($not_ack_applicants_query);
		if($this->_db->my_num_rows($not_ack_applicants_res) > 0){
			/* those applicants who have not done with ack get auto rejected */
			while($data = $this->_db->my_fetch_object($not_ack_applicants_res)){
				$this->change_not_ack_applicants_to_rejected($data->jod_job_applicant_id);
			}
		}
		
			$rejected_applicants_count = 0;
			$self_cancelled_applicants_count = 0;
			$clock_out_feedback_applicants = 0;
		/*  fetch how many applicant is applied for the job */
		$applied_applicant_query = "SELECT jod_job_applicant_id, job_id, status, system_rejected, acknowledged,  is_delete, clockInOutStatus, is_feedback_given FROM jod_job_applicants WHERE job_id ='".$job_id."'" ;
			$applied_applicants_res = $this->_db->my_query($applied_applicant_query);
			$applied_applicants_count  = $this->_db->my_num_rows($applied_applicants_res);
			
		while($applicant_data_all = $this->_db->my_fetch_object($applied_applicants_res)){
			/* calculate how many applicant is rejected for the job */
			if(($applicant_data_all->status == 2 OR $applicant_data_all->system_rejected  == 1) && $applicant_data_all->is_delete == 0 ){
				$rejected_applicants_count = $rejected_applicants_count +1; 
			}elseif($applicant_data_all->is_delete == 1){ /*Calculate how many applicant is self cancelled for the job */
				$self_cancelled_applicants_count ++; 

			}elseif($applicant_data_all->status == 1 && $applicant_data_all->is_delete == 0 &&  $applicant_data_all->acknowledged == 1 && $applicant_data_all->clockInOutStatus == 2  && $applicant_data_all->is_feedback_given == 1){
				/*  Calculate applicants has successfully clock out and manager has done with the feedback */ 
				$clock_out_feedback_applicants = $clock_out_feedback_applicants + 1;
			}
		}
		
		//	echo "Applied applicant : ". $applied_applicants_count . "Clock out and Feedback Applicant : ".$clock_out_feedback_applicants. " Self Cancelled applicant : ".$self_cancelled_applicants_count. " Rejected applicant : ".$rejected_applicants_count; die;
		
		/* If all the applicants has self cancelled */
		if($self_cancelled_applicants_count == $applied_applicants_count){
			return true;
		}
		/* If all the applicants has rejected for the job */
		if($rejected_applicants_count == $applied_applicants_count){
			return true;
		}
		
		/* If all applicants are either cancel or rejected for the job */
		if(($rejected_applicants_count + $self_cancelled_applicants_count) == $applied_applicants_count){
			return true;
		}
		/* If all applicants are either cancel or rejected for the job or clock out successfully */
		if(($rejected_applicants_count + $self_cancelled_applicants_count + $clock_out_feedback_applicants ) == $applied_applicants_count){
			return true;
		}
	}

	public function change_not_ack_applicants_to_rejected($job_applicant_id){
			$current_date 	= CURRENT_TIME;	
			$updateArray = array("updated_at" => $current_date,"status" => 2);
			$whereClause = array("jod_job_applicant_id" => $job_applicant_id);
			$update = $this->_db->UpdateAll("jod_job_applicants", $updateArray, $whereClause, "");		
			return true;
	}

	public function to_check_applicant_applied_for_job($job_id){
		$applicant_query = "SELECT jod_job_applicant_id FROM jod_job_applicants WHERE job_id = '".$job_id."' AND is_delete = '0'";
		$applicant_res = $this->_db->my_query($applicant_query);
		$applied_applicants_count  = $this->_db->my_num_rows($applicant_res);
		if($applied_applicants_count > 0){
			return true;
		}else{
			return false;

		}
	}
	
	
}


?>
