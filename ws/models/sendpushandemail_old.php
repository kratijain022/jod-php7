<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class sendpushandemail {
	public function __construct() {
        $this->_db      = env::getInst();
        $this->_common  = new commonclass();
        $this->_user    = new user();

        $base_url = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "https" : "http");
		$base_url .= "://". @$_SERVER['HTTP_HOST'];
		$base_url .= str_replace(basename($_SERVER['SCRIPT_NAME']),"",$_SERVER['SCRIPT_NAME']);
        $pathinfo = array_filter( explode('/', $base_url) );
		array_pop($pathinfo);
		$this->baseUrl =  implode('/',$pathinfo);
		$this->baseUrl_new = str_replace("http:/","http://",$this->baseUrl);
		$this->siteLogo = SITE_LOGO_URL;
    }

    /************************ ADDED BY NEERAJ SHARMA FOR ENHANCEMENT FEATURE - START ***********************/

    /**
	 * @method : runCompleteJobNotificationToManagersCron
	 * @param : no input required
	 * @return : Send email to managers (Will execute once in a day)
	*/
    public function testEmail()
    {
		$query = "SELECT * FROM `".DB_PREFIX."email_notifications` WHERE `purpose` = 'sendcompletejobnotification'";
		$result = $this->_db->my_query($query);
		$ids = array();
		if($this->_db->my_num_rows($result)>0){

			while($row = $this->_db->my_fetch_object($result))
			{
				$this->sendActivationMailAction($row,$row,'sendcompletejobnotification');
			}
		}
	}

    /**
	 * @method : runCompleteJobNotificationToManagersCron
	 * @param : no input required
	 * @return : Send email to managers (Will execute once in a day)
	*/
    public function runCompleteJobNotificationToManagersCron()
    {
		return "runCompleteJobNotificationToManagersCron";  //not in used now
	}

	/**
	 * @method : runCloseJobNotificationToApplicantsCron
	 * @param : no input required
	 * @return : Send email to Applicants (Will execute once in a day)
	*/
    public function runCloseJobNotificationToApplicantsCron()
    {
		return "runCloseJobNotificationToApplicantsCron"; //not in used now
	}

	/**
	 * @method : runCloseJobNotificationToApplicantsCron
	 * @param : no input required
	 * @return : Send email to Applicants (Will execute in each 1 hour interval)
	*/
    public function runAcknowledgeNotificationToApplicantsCron()
    {
		return "runAcknowledgeNotificationToApplicantsCron"; //not in used now
	}
	/************************ ADDED BY NEERAJ SHARMA FOR ENHANCEMENT FEATURE - END ***********************/

	public function runPushCron()
	{
		echo "runPushCron"; die;
		$this->logSetup("runPushCron 1st phase");
		$this->_push = pushnotifications::getInst();
		$androidPushDetails = array();
		$iphonePushDetails = array();
		$androidPushSend = array();
		$iphonePushSend = array();
		$check_query = "SELECT * FROM `".DB_PREFIX."notification_master` WHERE `is_cron_running` = '0'";
		$result_master = $this->_db->my_query($check_query);
		if($this->_db->my_num_rows($result_master) > 0 ){
			$query = "SELECT * FROM `".DB_PREFIX."notification` WHERE `status` = '0'";
			$result = $this->_db->my_query($query);
			if($this->_db->my_num_rows($result)>0){
				$query = "UPDATE `".DB_PREFIX."notification_master` SET `is_cron_running`='1'";
				$this->_db->my_query($query);
				while($row = $this->_db->my_fetch_object($result)){
					//echo "androidPushSend"."           ";
					$jobdetails = $this->getData('jobs','job_id',$row->job_id);
					$outletData  = $this->getData('outlets','outlet_id',$jobdetails->outlet_id);
					$all_receiver = explode(',',$row->user_id);
					print_r($all_receiver); die;
					$all_count = count($all_receiver);
					for($i=0;$i<$all_count;$i++){
						$receiver_data = $this->getData('user_accounts','user_accounts_id',$all_receiver[$i]);
						$device_type = strtolower($receiver_data->device_type);
						if($device_type =='android') {
							//for android
							if(!empty($receiver_data->device_id)){
								$androidPushDetails['device_id'] = $receiver_data->device_id;
								$androidPushDetails['job_id'] = $jobdetails->job_id;

								$servicesarray = $this->_user->get_job_detail_return_status($receiver_data->secret_token,$jobdetails->job_id);
								if($servicesarray != ''){
									$androidPushDetails['job_status'] = $servicesarray;
								}else{
									$androidPushDetails['job_status'] = 2;
								}


								if($row->notification_type=='job_opening'){
									$androidPushDetails['message'] = "There is new job ".utf8_encode($jobdetails->job_title)." from ".utf8_encode($outletData->outlet_name).".";
								}else if($row->notification_type=='applicant_hired'){
									//$androidPushDetails['message'] = "You are selected for ".utf8_encode($jobdetails->job_title).".Please acknowledge job.";
									//$androidPushDetails['message'] = "Dear JOD Member, you have been selected for the job at ".utf8_encode($outletData->outlet_name)." on ".utf8_encode($jobdetails->start_date).". Please kindly secure and confirm your job selection by clicking on “I Acknowledge” right away. Thank you.";
									// start - added by jhtang 20160528
									unset($androidPushDetails);
									// end - added by jhtang 20160528
								}else if($row->notification_type=='applicant_rejected'){
									$androidPushDetails['message'] =  "Your ".utf8_encode($jobdetails->job_title)." application was not successful.Please apply for others.";
								}else if($row->notification_type=='job_rejection'){
									//$androidPushDetails['message'] = "The ".utf8_encode($jobdetails->job_title)." job have cancelled for which you have applied.";
									$androidPushDetails['message'] = "Dear JOD Member, we regret to inform you that you are unsuccessful in your application at ".utf8_encode($outletData->outlet_name)." on ".utf8_encode($jobdetails->start_date).". Please do continue to try for the other jobs. Thank you.";
								}else if($row->notification_type=='manager_feedback'){
									// start - added by jhtang 20160528
									$androidPushDetails['message'] = "You have a rating for job ".utf8_encode($jobdetails->job_title);
//									$androidPushDetails['message'] = "You got rating for job ".utf8_encode($jobdetails->job_title);
									// end - added by jhtang 20160528
								}else if($row->notification_type == 'applicant_rejected_after_selection'){
										$androidPushDetails['message'] =  "Your ".utf8_encode($jobdetails->job_title)." application was not successful.Please apply for others.";
								}

								// start - added by jhtang 20160528
								if (!empty($androidPushDetails)) $androidPushSend[] = $androidPushDetails;
//								$androidPushSend[] = $androidPushDetails;
								// end - added by jhtang 20160528
								unset($androidPushDetails);
							}
						} else if ($device_type =='ios') {
							//for iphone
							if(!empty($receiver_data->device_id)){
								$iphonePushDetails['device_id'] = $receiver_data->device_id;
								$iphonePushDetails['job_id'] = $jobdetails->job_id;
								//$iphonePushDetails['job_status'] = $jobdetails->status;

								$servicesarray = $this->_user->get_job_detail_return_status($receiver_data->secret_token,$jobdetails->job_id);
								if($servicesarray != ''){
									$iphonePushDetails['job_status'] = $servicesarray;
								}else{
									$iphonePushDetails['job_status'] = 2;
								}


								if($row->notification_type=='job_opening'){
									$iphonePushDetails['message'] = "There is new job ".$jobdetails->job_title." from ".$outletData->outlet_name." .";
									$iphonePushDetails['welcome_txt'] = 'Job Opening';
								}else if($row->notification_type=='applicant_hired'){
									//$iphonePushDetails['message'] = "You are selected for ".$jobdetails->job_title.". Please acknowledge job .";
									//$iphonePushDetails['message'] = "Dear JOD Member, you have been selected for the job at ".$outletData->outlet_name." on ".$jobdetails->start_date.". Please kindly secure and confirm your job selection by clicking on “I Acknowledge” right away. Thank you.";
									//$iphonePushDetails['welcome_txt'] = 'You are Selected';
									// start - added by jhtang 20160528
									unset($iphonePushDetails);
									// end - added by jhtang 20160528
								}else if($row->notification_type=='applicant_rejected'){
									$iphonePushDetails['message'] = "Your ".$jobdetails->job_title." application was not successful. Please apply for others.";
									$iphonePushDetails['welcome_txt'] = 'You are Rejected';
								}else if($row->notification_type=='job_rejection'){
									//$iphonePushDetails['message'] = "The ".$jobdetails->job_title." job has cancelled for which you have applied .";
									$iphonePushDetails['message'] = "Dear JOD Member, we regret to inform you that you are unsuccessful in your application at ".$outletData->outlet_name." on ".$jobdetails->start_date.". Please do continue to try for the other jobs. Thank you.";
									$iphonePushDetails['welcome_txt'] = 'Job cancellation';
								}else if($row->notification_type=='manager_feedback'){
									// start - added by jhtang 20160528
									$iphonePushDetails['message'] = "You have a rating for job ".utf8_encode($jobdetails->job_title).".";
//									$iphonePushDetails['message'] = "You got rating for job ".$jobdetails->job_title.".";
									// end - added by jhtang 20160528
									//$iphonePushDetails['message'] = "Dear JOD Member, we regret to inform you that your job at <outlet name> on <job start date> at <job start time> has been cancelled by the hiring manager. Please refer to your email for more details. Thank you.";
									$iphonePushDetails['welcome_txt'] = 'Feedback';
								}
								else if($row->notification_type == 'applicant_rejected_after_selection'){
										$iphonePushDetails['message'] =  "Your ".utf8_encode($jobdetails->job_title)." application was not successful.Please apply for others.";
										$iphonePushDetails['welcome_txt'] = 'Applicantion Cancelled';
								}
								// start - added by jhtang 20160528
								if (!empty($iphonePushDetails)) $iphonePushSend[] = $iphonePushDetails;
//								$iphonePushSend[] = $iphonePushDetails;
								// end - added by jhtang 20160528
								unset($iphonePushDetails);
							}
						}
					}
				}
				if(!empty($androidPushSend)) { $this->_push->sendAndroidPushNotification2($androidPushSend); }
				if(!empty($iphonePushSend)) { $this->_push->sendIphonePushNotification2($iphonePushSend); }
				$this->updateNotification('notification');
			}
		}
	}

	//Get Time beween two dates in hrs -  $date1 will big date
	public function getTimeTwoDates($date1,$date2){
		$date1=new DateTime($date1);
		$date2=new DateTime($date2);
		//echo $date1.' - '.$date2; die();
		//if($date1 > $date2){
			$diff = $date2->diff($date1);
			$hours = $diff->h;
			$hours = $hours + ($diff->days*24);
			//final remaining hrs
			return $hours;
		//}else{
			//return 'oldDate';
		//}
	}

	//Get Time beween two dates in minuts -  $date1 will big date
	public function getMinutsTwoDates($date1,$date2){
		if($date1 && $date2){
			$to_time 	= strtotime($date1);  //Big date always
			$from_time 	= strtotime($date2);
			return round(abs($to_time - $from_time) / 60,2);
		}
	}

	public function runCron() {

		$query = "SELECT * FROM `".DB_PREFIX."email_notifications` WHERE `is_mail_sent` = 0";
		$result = $this->_db->my_query($query);

		if($this->_db->my_num_rows($result)>0){

			while($row = $this->_db->my_fetch_object($result)){
				$all_receiver = explode(',',$row->receiver_id);
				$all_count = count($all_receiver);
				for($i=0;$i<$all_count;$i++){
					$this->sendActivationMailAction($row,$all_receiver[$i],$row->purpose);
				}
			}
		}

		// rsolanki - Call cron for Manager Reminder for selection of jobs
		$this->runCronManagerReminder();
		// rsolanki - Call cron for Reminder to the applicants First time
		$this->sentFirstPushNotificationToApplicant();
		// rsolanki - Call cron for Reminder to the applicants second Time
		$this->sentSecondPushNotificationToApplicant();
		// rsolanki - Call cron for Reminder to the applicants second Time
		$this->sentJobEndReminder();

		//Cehck cron url
		require_once("libraries/class.phpmailer.php");
		$mail = new PHPMailer();
		$mail->IsSMTP();                 	// set mailer to use SMTP
		$mail->SMTPAuth = true;     		// turn on SMTP authentication
		$mail->CharSet="windows-1251";
		$mail->CharSet="utf-8";
		$mail->WordWrap = 50;      			// set word wrap to 50 characters
		$mail->IsHTML(true);
		$mail->AddAddress('ramlalsolanki@cdnsol.com','Ram');
		$mail->Subject = 'TEst';
		$mail->Body    = 'This is only for check';
		//$mail->send();
	}

	function getData($table_name,$user_id_field,$user_id) {
		//$query = "SELECT * FROM `".DB_PREFIX.$table_name."` WHERE `".$user_id_field."` = '".$user_id."' AND `is_delete` = '0'";
		$query = "SELECT * FROM `".DB_PREFIX.$table_name."` WHERE `".$user_id_field."` = '".$user_id."'";
		$getProfile = $this->_db->my_query($query);
		if ($this->_db->my_num_rows($getProfile) > 0) {
			$row = $this->_db->my_fetch_object($getProfile);
            return $row;
        } else {
			return 0;
		}
	}

	function getDataCount($table_name,$user_id_field,$user_id) {
		//$query = "SELECT * FROM `".DB_PREFIX.$table_name."` WHERE `".$user_id_field."` = '".$user_id."' AND `is_delete` = '0'";
		$query = "SELECT * FROM `".DB_PREFIX.$table_name."` WHERE `".$user_id_field."` = '".$user_id."'";
		$getProfile = $this->_db->my_query($query);
		return $this->_db->my_num_rows($getProfile);
	}


	function getJobsDetails($jobId) {
		$table_name = "jobs";
		$query = "SELECT * FROM `".DB_PREFIX.$table_name."` WHERE `job_id` = '".$jobId."'";
		$getJobData = $this->_db->my_query($query);
		return $this->_db->my_fetch_object($getJobData);
	}

	function sendJodSMS($msg,$contact_no,$calletId,$emailNotificationsId){
		require_once("libraries/APIClient2.php");
		if($msg!='' && $contact_no!=''){
			//echo $msg .' = > '. $contact_no;

			$contact_no = str_replace("+", "", $contact_no);
			$contact_no = str_replace(" + ", "", $contact_no);
			$contact_no = str_replace("  ", "", $contact_no);

			//$contact_no = "6598637239";
			// start - added by jhtang 20160528
			$sentSms 	= new TransmitsmsApi("8cbc0e61b128f2745f4914e3c7833333",'sms_my_secret');
//			$sentSms 	= new TransmitsmsApi("8cbc0e61b128f2745f4914e3c7833333",'my_secret');
			// end - added by jhtang 20160528
			$updatedMessage = htmlspecialchars($msg);
			$result 	= $sentSms->sendSms($updatedMessage,$contact_no,$calletId);
			if($result->error->code){
				$this->updateSMSResponse('email_notifications',$emailNotificationsId,$result->error->code);
			}
		}
	}

	//rsolanki - 26-04-2016
	public function runCronManagerReminder() {
		$cdate = date('Y-m-d');
		$query 	= "SELECT * FROM `".DB_PREFIX."jobs`
						WHERE `is_completed` = '0'
						AND `status` = '0'
						AND `is_approved` = '1'
						AND `is_applicants_hired` = '0'
						AND `is_delete` = '0'
						AND `start_date` >= '$cdate'";
		$result = $this->_db->my_query($query);
		if($this->_db->my_num_rows($result)>0){
			while($row = $this->_db->my_fetch_object($result)){
				//print_r($row);die('Test');
				$date1 = $row->start_date." ".$row->start_time;
				$date2 = date("Y-m-d H:i:s");
				if($date1 > $date2){
					$remainingTime = $this->getTimeTwoDates($date1,$date2);
					if($remainingTime <= 8){
						//echo $row->job_id.' - '.$remainingTime;die('Ram');
						//get time diff last from last cron run date
						$lastTimeDiff = $this->getTimeTwoDates($date2,$row->updateLastEmailDateTime);
						if($remainingTime != $row->lastEmailUpdateHrs){
							//echo $row->job_id.' - '.$remainingTime;die('Ram');
							if($row->updateLastEmailDateTime == "0000-00-00 00:00:00"){
								$this->sendActivationMailAction($row,$remainingTime,'ReminderForSelectionOfJobApplicants');
							}else if($row->updateLastEmailDateTime != "0000-00-00 00:00:00" && $lastTimeDiff>0){
								$this->sendActivationMailAction($row,$remainingTime,'ReminderForSelectionOfJobApplicants');
							}
							//update to lastEmailUpdateHrs of every time 8,7,6,5,4,3,2,1 (0=>sent last updates mail and UPdate to updateLastEmail=1)
						}else if($remainingTime == 0){
							$this->sendActivationMailAction($row,$remainingTime,'ReminderForSelectionOfJobApplicants');
						}
					}
				}
			}
		}
	}

	//rsolanki - 29-04-2016 - monday
	public function sentFirstPushNotificationToApplicant() {
		$cdate 				= date('Y-m-d');
		$cdateTime 			= date('Y-m-d H:i:s');
		$this->_push 		= pushnotifications::getInst();
		$androidPushDetails = array();
		$iphonePushDetails 	= array();
		$androidPushSend 	= array();
		$iphonePushSend 	= array();
		$query = "SELECT * FROM `".DB_PREFIX."jobs`
								WHERE `is_completed` = '0'
									AND `status` = '0'
									AND `is_approved` = '1'
									AND `is_applicants_hired` = '1'
									AND `start_date` >= '$cdate' ORDER BY job_id DESC";
		$result = $this->_db->my_query($query);
		$ids 	= array();
		if($this->_db->my_num_rows($result)>0){
			while($jobdetails	= $this->_db->my_fetch_object($result)){
				//print_r($jobdetails);die();
				$jobId 			= $jobdetails->job_id;
				$query2 		= "SELECT * FROM `".DB_PREFIX."job_applicants`
										WHERE `job_id` = '".$jobId."'
										AND `status` = '1'
										AND `acknowledged` = '0'";
				$result2 = $this->_db->my_query($query2);
				if($this->_db->my_num_rows($result2)>0)
				{
					//Close fefor 2 hrs
					$getMinutesDiffForTwoHrs = $this->getMinutsTwoDates($cdateTime,$jobdetails->start_date." ".$jobdetails->start_time);
					//if($getMinutesDiffForTwoHrs > 125){
					echo $getMinutesDiffForTwoHrs; die;
					if($getMinutesDiffForTwoHrs > 182){     //60*3
						//update ack_approval
						while($jobApplicantsData = $this->_db->my_fetch_object($result2)){
							$userAccountId = $jobApplicantsData->applicant_user_account_id;
							$this->updateSecondack_approval_1($jobId, $userAccountId);
							//print_r($jobApplicantsData); die('Ram');
							$NotificationSent   = 2;
							if($jobdetails->firstJobAckDateTime == '0000-00-00 00:00:00'){
								$NotificationSent=1;
								$this->updateFirstJobAckDateTimeFirstTime($jobId);
							}else if($jobdetails->firstJobAckDateTime != '0000-00-00 00:00:00'){
								//if($cdateTime > $jobdetails->firstJobAckDateTime){
									//get minuts diff
									$getMinutesDiff = $this->getMinutsTwoDates($cdateTime,$jobdetails->firstJobAckDateTime);
									//Sent Nonify
									if($getMinutesDiff >= 15){
										$NotificationSent=1;
									}
									//Sent mail to aptus
									$getMinutesDiffFromFirstTime = $this->getMinutsTwoDates($cdateTime,$jobdetails->firstJobAckDateTimeFirstTime);
									if($getMinutesDiffFromFirstTime >= 55){
										if($jobdetails->firstJobAckAptusNotify==0){
											//rsolanki - sent aptus notification after 1 hrs applicant select.
											$this->sendActivationMailAction($jobApplicantsData, $jobApplicantsData,'sentFirstEmailNotificationToAptus'); //Send Email
											$this->updateFirstJobAckAptusNotify($jobId);
										}
									}
								//}
								//Notify to Aptus

							}
							if($NotificationSent==1){
								$jobdetails->outlet_id;
								$outletData  		= $this->getData('outlets','outlet_id',$jobdetails->outlet_id);
								/** Prepare data for push notifications - Start **/
								$user_accounts_data = $this->getData('user_accounts','user_accounts_id',$jobApplicantsData->applicant_user_account_id);
								$applicant_data 	= $this->getData('applicants','user_account_id',$jobApplicantsData->applicant_user_account_id);
								$device_id 			= $user_accounts_data->device_id;
								$device_type 		= strtolower($user_accounts_data->device_type);
								$applicant_name 	= ucfirst($applicant_data->first_name)." ".ucfirst($applicant_data->last_name);
								if($device_type =='android') {
									//for android
									if(!empty($user_accounts_data->device_id)){
										$androidPushDetails['device_id'] = $user_accounts_data->device_id;
										$androidPushDetails['job_id'] 	 = $jobdetails->job_id;
										// start - added by jhtang 20160528
										$androidPushDetails['message'] 	 = "Dear ".utf8_encode($applicant_name).", you have been selected for the job at ".utf8_encode($outletData->outlet_name)." on ".utf8_encode($jobdetails->start_date)." Please kindly secure and confirm your job selection by clicking on [Acknowledge] right away.";
//										$androidPushDetails['message'] 	 = "Dear ".utf8_encode($applicant_name).", you have been selected for the job at ".utf8_encode($outletData->outlet_name)." on ".utf8_encode($jobdetails->start_date)." Please kindly secure and confirm your job selection by clicking on “I Acknowledge” right away. Thank you.";
										// end - added by jhtang 20160528
										$androidPushSend[] 				 = $androidPushDetails;
										unset($androidPushDetails);
									}
								}else if ($device_type =='ios') {
									//for iphone
									if(!empty($user_accounts_data->device_id)){
										$iphonePushDetails['device_id']   = $user_accounts_data->device_id;
										$iphonePushDetails['job_id'] 	  = $jobdetails->job_id;
										$iphonePushDetails['job_status']  = $jobdetails->status;
										// start - added by jhtang 20160528
										$iphonePushDetails['message'] 	  = "Dear ".$applicant_name.", you have been selected for the job at ".$outletData->outlet_name." on ".$jobdetails->start_date." Please kindly secure and confirm your job selection by clicking on [Acknowledge] right away.";
//										$iphonePushDetails['message'] 	  = "Dear ".$applicant_name.", you have been selected for the job at ".$outletData->outlet_name." on ".$jobdetails->start_date." Please kindly secure and confirm your job selection by clicking on “I Acknowledge” right away. Thank you.";
										// start - added by jhtang 20160528
										$iphonePushDetails['welcome_txt'] = 'Job Acknowledgement';
										$iphonePushSend[] 				  = $iphonePushDetails;
										unset($iphonePushDetails);
									}
								}
								//update firstJobAckDateTime
								$this->updateFirstJobAckDateTime($jobId);
							}
							/** Prepare data for push notifications - End **/
						}
					}
				}
				/** Send push notifications **/
				if(!empty($androidPushSend)) { $this->_push->sendAndroidPushNotification2($androidPushSend); }
				if(!empty($iphonePushSend)) { $this->_push->sendIphonePushNotification2($iphonePushSend); }
				// start - added by jhtang 20160528
				unset($androidPushSend);
				unset($iphonePushSend);
				// end - added by jhtang 20160528
			}
		}
	}


	//rsolanki - 04-05-2016 Wednesday
	public function sentSecondPushNotificationToApplicant() {
		$cdate 				= date('Y-m-d');
		$cdateTime 			= date('Y-m-d H:i:s');
		$this->_push 		= pushnotifications::getInst();
		$androidPushDetails = array();
		$iphonePushDetails 	= array();
		$androidPushSend 	= array();
		$iphonePushSend 	= array();
		$query = "SELECT * FROM `".DB_PREFIX."jobs`
								WHERE `is_completed` = '0'
									AND `status` = '0'
									AND `is_approved` = '1'
									AND `is_applicants_hired` = '1'
									AND `start_date` >= '$cdate' ORDER BY job_id DESC";
		$result = $this->_db->my_query($query);
		$ids 	= array();
		if($this->_db->my_num_rows($result)>0){
			while($jobdetails	= $this->_db->my_fetch_object($result)){
				$jobId 			= $jobdetails->job_id;
				$query2 		= "SELECT * FROM `".DB_PREFIX."job_applicants`
										WHERE `job_id` = '".$jobId."' AND `clockInOutStatus` = '0' AND `acknowledged` = '0'
										AND `status` = '1' AND ack_approval!=4";
				/*
				$query2 		= "SELECT * FROM `".DB_PREFIX."job_applicants`
									WHERE `job_id` = '".$jobId."'
									AND `status` = '1'
									AND `acknowledged` = '0'";
				*/

				$result2 = $this->_db->my_query($query2);
				if($this->_db->my_num_rows($result2)>0){
					while($jobApplicantsData = $this->_db->my_fetch_object($result2)){
					//print_r($jobApplicantsData);die();
						$NotificationSent   	 = 2;
						$getMinutesDiffForTwoHrs = $this->getMinutsTwoDates($cdateTime,$jobdetails->start_date." ".$jobdetails->start_time);
						//if($getMinutesDiffForTwoHrs < 118){
						if($getMinutesDiffForTwoHrs < 178){   //60*3

							//update ack_approval
							$userAccountId = $jobApplicantsData->applicant_user_account_id;
							$this->updateSecondack_approval_2($jobId,$userAccountId);

							if($jobdetails->secondJobAckDateTime == '0000-00-00 00:00:00'){
								$NotificationSent=1;
								$this->updateSecondJobAckDateTimeFirstTime($jobId);
							}else if($jobdetails->secondJobAckDateTime != '0000-00-00 00:00:00'){
								//get minuts diff
								$getMinutesDiff = $this->getMinutsTwoDates($cdateTime,$jobdetails->secondJobAckDateTime);
								//Sent Nonify
								if($getMinutesDiff >= 5){
									$NotificationSent=1;
								}
								//Sent mail to aptus
								$getMinutesDiffFromFirstTime = $this->getMinutsTwoDates($cdateTime,$jobdetails->secondJobAckDateTimeFirstTime);
								//die();
								if($getMinutesDiffFromFirstTime >= 50){
									if($jobdetails->secondJobAckAptusNotify==0){
										//rsolanki - sent aptus notification after 1 hrs applicant select.
										$this->sendActivationMailAction($jobApplicantsData, $jobApplicantsData,'sentSecondEmailNotificationToAptus'); //Send Email
										$this->updateSecondJobAckAptusNotify($jobId);
									}
								}
								//Notify to Aptus
							}
							if($NotificationSent==1){
								$jobdetails->outlet_id;
								$outletData  		= $this->getData('outlets','outlet_id',$jobdetails->outlet_id);
								/** Prepare data for push notifications - Start **/
								$user_accounts_data = $this->getData('user_accounts','user_accounts_id',$jobApplicantsData->applicant_user_account_id);
								$applicant_data 	= $this->getData('applicants','user_account_id',$jobApplicantsData->applicant_user_account_id);
								$device_id 			= $user_accounts_data->device_id;
								$device_type 		= strtolower($user_accounts_data->device_type);
								$applicant_name 	= ucfirst($applicant_data->first_name)." ".ucfirst($applicant_data->last_name);
								if($device_type =='android') {
									//for android
									if(!empty($user_accounts_data->device_id)){
										$androidPushDetails['device_id'] = $user_accounts_data->device_id;
										$androidPushDetails['job_id'] 	 = $jobdetails->job_id;
										// start - added by jhtang 20160528
										$androidPushDetails['message'] 	 = "Dear ".utf8_encode($applicant_name).", your	job	at ".utf8_encode($outletData->outlet_name)." will be starting today	at ".utf8_encode($jobdetails->start_time).". Please kindly confirm your attendance by clicking on [Acknowledge] right away. Thank you.";
//										$androidPushDetails['message'] 	 = "Dear ".utf8_encode($applicant_name).", your	job	at ".utf8_encode($outletData->outlet_name)." will be starting today	at ".utf8_encode($jobdetails->start_time).". Please kindly confirm your attendance by clicking on “I Acknowledge” right away. Thank you.";
										// end - added by jhtang 20160528
										$androidPushSend[] 				 = $androidPushDetails;
										unset($androidPushDetails);
									}
								}else if ($device_type =='ios') {
									//for iphone
									if(!empty($user_accounts_data->device_id)){
										$iphonePushDetails['device_id']   = $user_accounts_data->device_id;
										$iphonePushDetails['job_id'] 	  = $jobdetails->job_id;
										$iphonePushDetails['job_status']  = $jobdetails->status;
										// start - added by jhtang 20160528
										$iphonePushDetails['message'] 	  = "Dear ".$applicant_name.", your	job	at ".$outletData->outlet_name."	will be	starting today at ".$jobdetails->start_time.". Please kindly confirm your attendance by clicking on [Acknowledge] right away. Thank you.";
//										$iphonePushDetails['message'] 	  = "Dear ".$applicant_name.", your	job	at ".$outletData->outlet_name."	will be	starting today at ".$jobdetails->start_time.". Please kindly confirm your attendance by clicking on “I Acknowledge” right away. Thank you.";
										// end - added by jhtang 20160528
										$iphonePushDetails['welcome_txt'] = 'Job Acknowledgement';
										$iphonePushSend[] 				  = $iphonePushDetails;
										unset($iphonePushDetails);
									}
								}
									//update firstJobAckDateTime
									$this->updateSecondJobAckDateTime($jobId);
							}
						}
						/** Prepare data for push notifications - End **/
					}
				}
				/** Send push notifications **/
				if(!empty($androidPushSend)) { $this->_push->sendAndroidPushNotification2($androidPushSend);  }
				if(!empty($iphonePushSend)) { $this->_push->sendIphonePushNotification2($iphonePushSend); }
				// start - added by jhtang 20160528
				unset($androidPushSend);
				unset($iphonePushSend);
				// end - added by jhtang 20160528
			}
		}
	}

	//rsolanki - 05-05-2016 Thuirday
	public function sentJobEndReminder() {
		$cdate 				= date('Y-m-d');
		$cdateTime 			= date('Y-m-d H:i:s');
		$this->_push 		= pushnotifications::getInst();
		$androidPushDetails = array();
		$iphonePushDetails 	= array();
		$androidPushSend 	= array();
		$iphonePushSend 	= array();
		$query 				= "SELECT ja.*,j.start_date,j.start_time,j.end_date,j.end_time,j.outlet_id,j.status as Jstatus,j.created_by,j.job_created_by,j.is_completed
				FROM `".DB_PREFIX."job_applicants` ja
				LEFT JOIN `".DB_PREFIX."jobs` j ON ja.job_id=j.job_id
				WHERE (`completed_by_manager` = '0' OR `completed_by_applicant` = '0') AND `j`.`is_delete` = '0' AND  ja.status = '1'
				GROUP BY ja.jod_job_applicant_id";
		$result = $this->_db->my_query($query);
		if($this->_db->my_num_rows($result)>0){
			while($jobApplicantsData = $this->_db->my_fetch_object($result))
			{
				$jobDetails = $this->getJobsDetails($jobApplicantsData->job_id);
				$cdateTime 	= date('Y-m-d H:i:s');
				$getMinutesDiffEndJob = $this->getMinutsTwoDates($cdateTime,$jobDetails->end_date." ".$jobDetails->end_time);
				//echo $cdateTime." ====== ".$jobdetails->end_date." ".$jobdetails->end_time." Time remaining = ".$getMinutesDiffEndJob;
				if($getMinutesDiffEndJob < 59)
				{
					$jobDeleted = $jobApplicantsData->is_delete;
					$jobStartTime  = $jobApplicantsData->hired_at;
					if($jobStartTime != '0000-00-00 00:00:00')
					{
						if($jobDeleted == 0)
						{
							$outletData  		= $this->getData('outlets','outlet_id',$jobApplicantsData->outlet_id);
							$NotificationSent   = 2;
							$sentEmail 		  	= 2;
							$getMinutesDiffEndJob = $this->getMinutsTwoDates($cdateTime,$jobApplicantsData->end_date." ".$jobApplicantsData->end_time);
							if(($jobApplicantsData->is_completed==0) && ($cdateTime > ($jobApplicantsData->end_date." ".$jobApplicantsData->end_time))){

								if($getMinutesDiffEndJob > 0){
									if($jobApplicantsData->dateTimeJobEndReminder == '0000-00-00 00:00:00'){
										$NotificationSent = 1;
										$sentEmail 		  = 1;
										$this->updateDateTimeJobEndReminder($jobApplicantsData->jod_job_applicant_id);
									}else if($jobApplicantsData->dateTimeJobEndReminder != '0000-00-00 00:00:00'){
										//get minuts diff
										if($cdateTime > $jobApplicantsData->dateTimeJobEndReminder){
											$getMinutesDiff = $this->getMinutsTwoDates($cdateTime,$jobApplicantsData->dateTimeJobEndReminder);
											//Sent Nonify
											if($getMinutesDiff >= 14){
												$NotificationSent = 1;
												$sentEmail 		  = 1;
												$this->updateDateTimeJobEndReminder($jobApplicantsData->jod_job_applicant_id);
											}
										}
									}
									//sent sms and mail to manager
									if($sentEmail==1 && $jobApplicantsData->completed_by_manager == 0){
										$this->sendActivationMailAction($jobApplicantsData, $jobApplicantsData,'sentJobEndReminderEmailTemp'); //Send Email
									}

									//sent native notification to the applicant
									if($NotificationSent==1 && $jobApplicantsData->completed_by_applicant == 0){
										/** Prepare data for push notifications - Start **/
										$user_accounts_data = $this->getData('user_accounts','user_accounts_id',$jobApplicantsData->applicant_user_account_id);
										$applicant_data 	= $this->getData('applicants','user_account_id',$jobApplicantsData->applicant_user_account_id);
										$device_id 			= $user_accounts_data->device_id;
										$device_type 		= strtolower($user_accounts_data->device_type);
										$applicant_name 	= ucfirst($applicant_data->first_name)." ".ucfirst($applicant_data->last_name);
										if($device_type =='android') {
											//for android
											if(!empty($user_accounts_data->device_id)){
												$androidPushDetails['device_id'] = $user_accounts_data->device_id;
												$androidPushDetails['job_id'] 	 = $jobApplicantsData->job_id;
												$androidPushDetails['message'] 	 = "Dear ".utf8_encode($applicant_name).", your job at ".utf8_encode($outletData->outlet_name)." was due for completion today at ".utf8_encode($jobApplicantsData->end_time).". Please kindly close the job and rate the hiring manager as soon as possible. Thank you.";
												$androidPushSend[] 				 = $androidPushDetails;
												unset($androidPushDetails);
											}
										}else if ($device_type =='ios') {
											//for iphone
											if(!empty($user_accounts_data->device_id)){
												$iphonePushDetails['device_id']   = $user_accounts_data->device_id;
												$iphonePushDetails['job_id'] 	  = $jobApplicantsData->job_id;
												$iphonePushDetails['job_status']  = $jobApplicantsData->Jstatus;
												$iphonePushDetails['message'] 	  = "Dear ".$applicant_name.", your job at ".$outletData->outlet_name." was due for completion today at ".$jobApplicantsData->end_time.". Please kindly close the job and rate the hiring manager as soon as possible. Thank you.";
												$iphonePushDetails['welcome_txt'] = 'Job Acknowledgement';
												$iphonePushSend[] 				  = $iphonePushDetails;
												unset($iphonePushDetails);
											}
										}
										/** Send push notifications **/
										if(!empty($androidPushSend)) {  $this->_push->sendAndroidPushNotification2($androidPushSend); }
										if(!empty($iphonePushSend)) { $this->_push->sendIphonePushNotification2($iphonePushSend); }
										// start - added by jhtang 20160528
										unset($androidPushSend);
										unset($iphonePushSend);
										// end - added by jhtang 20160528
									}
								}
							}
						}
					}
				}
				else
				{
					//echo "<pre>"; print_r($jobApplicantsData); die();
				}
				/** Prepare data for push notifications - End **/
			}
		}
	}

	function updateData($table_name,$whereId) {
		$query = "UPDATE `".DB_PREFIX.$table_name."` SET `is_mail_sent`='1' WHERE `id` = '".$whereId."'";
		$getProfile = $this->_db->my_query($query);
		return true;
	}

	function updateDataFlagSelectionJobApplicants($lastEmailUpdateHrs,$job_id) {
		$cdateTime 	= date('Y-m-d H:i:s');
		$query = "UPDATE `".DB_PREFIX."jobs` SET `lastEmailUpdateHrs`='$lastEmailUpdateHrs',`updateLastEmailDateTime`='$cdateTime' WHERE `job_id` = '".$job_id."'";
		$getProfile = $this->_db->my_query($query);
		return true;
	}

	function updateLastEmailRecords($job_id) {
		$query = "UPDATE `".DB_PREFIX."jobs` SET `updateLastEmail`='1' WHERE `job_id` = '".$job_id."'";
		$getProfile = $this->_db->my_query($query);
		return true;
	}

	function updateSentReminderLastToapplicant($job_id) {
		$query = "UPDATE `".DB_PREFIX."jobs` SET `sentReminderLastToapplicant`='1' WHERE `job_id` = '".$job_id."'";
		$getProfile = $this->_db->my_query($query);
		return true;
	}



	function updateFirstJobAckDateTime($job_id) {
		$cdateTime 	= date('Y-m-d H:i:s');
		$query = "UPDATE `".DB_PREFIX."jobs` SET `firstJobAckDateTime`='$cdateTime' WHERE `job_id` = '".$job_id."'";
		$getProfile = $this->_db->my_query($query);
		return true;
	}

	function updateSecondJobAckDateTime($job_id) {
		$cdateTime 	= date('Y-m-d H:i:s');
		$query = "UPDATE `".DB_PREFIX."jobs` SET `secondJobAckDateTime`='$cdateTime' WHERE `job_id` = '".$job_id."'";
		$getProfile = $this->_db->my_query($query);
		return true;
	}

	function updateFirstJobAckAptusNotify($job_id) {
		$query = "UPDATE `".DB_PREFIX."jobs` SET `firstJobAckAptusNotify`='1' WHERE `job_id` = '".$job_id."'";
		$getProfile = $this->_db->my_query($query);
		return true;
	}

	function updateSecondJobAckAptusNotify($job_id) {
		$query = "UPDATE `".DB_PREFIX."jobs` SET `secondJobAckAptusNotify`='1' WHERE `job_id` = '".$job_id."'";
		$getProfile = $this->_db->my_query($query);
		return true;
	}

	function updateSecondack_approval_1($job_id, $userAccountId) {
		$query = "UPDATE `".DB_PREFIX."job_applicants` SET `ack_approval`='1' WHERE `job_id` = '".$job_id."' AND `applicant_user_account_id` = '".$userAccountId."'";
		$getProfile = $this->_db->my_query($query);
		return true;
	}

	function updateSecondack_approval_2($job_id, $userAccountId) {
		$query = "UPDATE `".DB_PREFIX."job_applicants` SET `ack_approval`='2' WHERE `job_id` = '".$job_id."' AND `applicant_user_account_id` = '".$userAccountId."'";
		$getProfile = $this->_db->my_query($query);
		return true;
	}

	function updateFirstJobAckDateTimeFirstTime($job_id) {
		$cdateTime 	= date('Y-m-d H:i:s');
		$query = "UPDATE `".DB_PREFIX."jobs` SET `firstJobAckDateTimeFirstTime`='$cdateTime' WHERE `job_id` = '".$job_id."'";
		$getProfile = $this->_db->my_query($query);
		return true;
	}

	function updateSecondJobAckDateTimeFirstTime($job_id) {
		$cdateTime 	= date('Y-m-d H:i:s');
		$query = "UPDATE `".DB_PREFIX."jobs` SET `secondJobAckDateTimeFirstTime`='$cdateTime' WHERE `job_id` = '".$job_id."'";
		$getProfile = $this->_db->my_query($query);
		return true;
	}

	function updateDateTimeJobEndReminder($jod_job_applicant_id) {
		$cdateTime 	= date('Y-m-d H:i:s');
		$query = "UPDATE `".DB_PREFIX."job_applicants` SET `dateTimeJobEndReminder`='$cdateTime' WHERE `jod_job_applicant_id` = '".$jod_job_applicant_id."'";
		$getProfile = $this->_db->my_query($query);
		return true;
	}

	function updateSMSResponse($table_name,$whereId,$smsResponse) {
		$query 		= "UPDATE `".DB_PREFIX.$table_name."` SET `smsResponse`='$smsResponse' WHERE `id` = '".$whereId."'";
		$getProfile = $this->_db->my_query($query);
		return true;
	}

	function updateNotification($table_name) {
		$query_n = "UPDATE `".DB_PREFIX.$table_name."` SET `status`='1' WHERE `status` = '0'";
		$getProfile = $this->_db->my_query($query_n);
		$query = "UPDATE `".DB_PREFIX."notification_master` SET `is_cron_running`='0'";
		$this->_db->my_query($query);
		return true;
	}

	function getEmailTemplate($purpose) {
		$query = "SELECT * FROM `".DB_PREFIX."email_template` WHERE `purpose` = '".$purpose."'";
		$getProfile = $this->_db->my_query($query);
		if ($this->_db->my_num_rows($getProfile) > 0) {
			$row = $this->_db->my_fetch_object($getProfile);
            return $row;
        } else {
			return 0;
		}
	}

	function getSMSTemplate($purpose) {
		$query 		= "SELECT * FROM `".DB_PREFIX."sms_template` WHERE `purpose` = '".$purpose."'";
		$getProfile = $this->_db->my_query($query);
		if ($this->_db->my_num_rows($getProfile) > 0) {
			$row = $this->_db->my_fetch_object($getProfile);
            return $row;
        } else {
			return 0;
		}
	}

	function sendActivationMailAction($notificationData,$receiver_id,$purpose)
	{
		require_once("libraries/class.phpmailer.php");
		$mail = new PHPMailer();
		$mail->IsSMTP();                 	// set mailer to use SMTP
		$mail->SMTPAuth = true;     		// turn on SMTP authentication
		$mail->CharSet="windows-1251";
		$mail->CharSet="utf-8";
		$mail->WordWrap = 50;      			// set word wrap to 50 characters
		$mail->IsHTML(true);

		$jobdetails 	= $this->getData('jobs','job_id',$notificationData->job_id);
		$outletData  	= $this->getData('outlets','outlet_id',$jobdetails->outlet_id);
		$aptusEmailId 	= 'notification@aptus.com.sg'; //notification@aptus.com.sg,ramlalsolanki@cdnsol.com
		//$addBCC 		= 'ramlalsolanki@cdnsol.com';

		// start - added by jhtang 20150915
        // workaround for ssl certificate verification
        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );
        // end - added by jhtang 20150915

		switch ($purpose) {
			case "manager_notification":
					if($notificationData->sender_user_type=='5' || $notificationData->sender_user_type=='3'){
						if($notificationData->receiver_user_type=='2'){
							/*** When email is sent to the HQ manager ***/
							$receiver_data = $this->getData('headquater_manager','user_accounts_id',$receiver_id);
							$emailid = $receiver_data->email_id;
							$user_name = $senderData->email_id;
							$receiver_contact_no = $receiver_data->contact_no;

							$mail->AddAddress($emailid,$user_name);	//sender user email id
							//$mail->AddBCC($addBCC,'Aptus Email Address');
							if($notificationData->sender_user_type=='5'){
								$sender_data = $this->getData('area_manager','user_accounts_id',$notificationData->sender_id);
							}else {
								$sender_data = $this->getData('outlet_managers','user_accounts_id',$notificationData->sender_id);
							}
							$manager_name = ucfirst($receiver_data->first_name)." ".ucfirst($receiver_data->last_name);
							if($jobdetails->is_delete=='1'){
								$receiver_email_data = $this->getEmailTemplate("manager_notification_job_cancellation");
								//SMS body - rsolanki
								$receiver_sms_data = $this->getSMSTemplate("manager_notification_job_cancellation");

								/*** The email is sent to HQ Manager when any job get cancelled by the Area/OM manager ***/
								$deleted_by_name = $sender_data->first_name." ".$sender_data->last_name;
								// start - added by jhtang - 20151221
								$searchArray = array("{site_logo}","{manager_name}","{job_title}","{outlet_name}","{outlet_location}","{deleted_by_name}","{job_start_time}","{job_end_time}");
								$replaceArray = array($this->siteLogo,$manager_name, $jobdetails->job_title,$outletData->outlet_name,$outletData->outlet_address,$deleted_by_name,$jobdetails->start_date.' '.$jobdetails->start_time,$jobdetails->end_date.' '.$jobdetails->end_time);
//								$searchArray = array("{site_logo}","{manager_name}","{job_title}","{outlet_name}","{outlet_location}","{deleted_by_name}","{job_start_time}","{job_end_time}");
//								$replaceArray = array($this->siteLogo,$manager_name, $jobdetails->job_title,$outletData->outlet_name,$outletData->outlet_address,$deleted_by_name,$jobdetails->start_time,$jobdetails->end_time);
								// end - added by jhtang - 20151221
							}else {
								/*** The email is sent to HQ Manager for the need of the job approval ***/
								$receiver_email_data = $this->getEmailTemplate("need_job_approval");
								//SMS body - rsolanki
								$receiver_sms_data = $this->getSMSTemplate("need_job_approval");

								$created_by_name = $sender_data->first_name." ".$sender_data->last_name;
								$click_here_link = $this->baseUrl_new."/jobs/notification";
								$searchArray = array("{site_logo}","{manager_name}","{job_title}","{outlet_name}","{created_by_name}","{click_here}");
								$replaceArray = array($this->siteLogo,$manager_name, $jobdetails->job_title,$outletData->outlet_name,$created_by_name,$click_here_link);
							}
							$templateBody = $receiver_email_data->body;
							$smsBodyTemplate= $receiver_sms_data->body;

							$subject = $receiver_email_data->subject;

							$mainBody 	= str_replace($searchArray, $replaceArray, $templateBody);
							$smsBody 	= str_replace($searchArray, $replaceArray, $smsBodyTemplate);

							$mail->Subject = $subject;
							$mail->Body    = $mainBody;
							$mail->send();
							//SMS send - rsolanki
							if($jobdetails->is_delete!='1') {
								if($smsBody && $receiver_contact_no){
									$this->sendJodSMS($smsBody,$receiver_contact_no,'JOD',$notificationData->id);
								}
							}
							$this->updateData('email_notifications',$notificationData->id);
						}
					} else if($notificationData->sender_user_type=='2'){
						/** When email is sent to the Area/Outlet manager when job is approved by HQ manager ****/
						$sender_data = $this->getData('headquater_manager','user_accounts_id',$notificationData->sender_id);
						if($notificationData->receiver_user_type=='5'){
							$receiver_data = $this->getData('area_manager','user_accounts_id',$receiver_id);
						}else if($notificationData->receiver_user_type=='3'){
							$receiver_data = $this->getData('outlet_managers','user_accounts_id',$receiver_id);
						}
						$emailid = $receiver_data->email_id;
						$receiver_contact_no = $receiver_data->contact_no;

						$user_name = $senderData->email_id;

						$mail->AddAddress($emailid,$user_name);	//sender user email id
						//$mail->AddBCC($addBCC,'Aptus Email Address');

						$manager_name = ucfirst($receiver_data->first_name)." ".ucfirst($receiver_data->last_name);
						if($jobdetails->is_approved=='1'){
							$receiver_email_data = $this->getEmailTemplate("manager_notification_job_approval");
							//SMS body - rsolanki
							$receiver_sms_data = $this->getSMSTemplate("manager_notification_job_approval");

							$searchArray = array("{site_logo}","{manager_name}","{body}","{job_title}","{job_id}");
							$replaceArray = array($this->siteLogo,$manager_name,$body,$jobdetails->job_title,$jobdetails->job_id);
						} else{
							$receiver_email_data = $this->getEmailTemplate("manager_notification_job_disapproval");
							//SMS body - rsolanki
							$receiver_sms_data = $this->getSMSTemplate("manager_notification_job_disapproval");

							$searchArray = array("{site_logo}","{manager_name}","{job_title}","{job_id}","{outlet_name}");
							$replaceArray = array($this->siteLogo,$manager_name,$jobdetails->job_title,$jobdetails->job_id,$outletData->outlet_name);
						}

						$templateBody = $receiver_email_data->body;
						$smsBodyTemplate= $receiver_sms_data->body;

						$mainBody = str_replace($searchArray, $replaceArray, $templateBody);
						$smsBody  = str_replace($searchArray, $replaceArray, $smsBodyTemplate);

						$mail->Subject = $receiver_email_data->subject;
						$mail->Body    = $mainBody;
						$mail->send();
						//SMS send - rsolanki
						if($smsBody && $receiver_contact_no){
							$this->sendJodSMS($smsBody,$receiver_contact_no,'JOD',$notificationData->id);
						}
						$this->updateData('email_notifications',$notificationData->id);

					}
					break;
				case "applicant_notification":
					//$receiver_email_data = $this->getEmailTemplate("applicant_notification");
					if($notificationData->sender_user_type=='2'){
						$sender_data = $this->getData('headquater_manager','user_accounts_id',$notificationData->sender_id);
					} else if($notificationData->sender_user_type=='3'){
						$sender_data = $this->getData('outlet_managers','user_accounts_id',$notificationData->sender_id);
					} else if($notificationData->sender_user_type=='5'){
						$sender_data = $this->getData('area_manager','user_accounts_id',$notificationData->sender_id);
					}
					if($notificationData->receiver_user_type=='4'){
						$receiver_data = $this->getData('applicants','user_account_id',$receiver_id);
					}

					$emailid = $receiver_data->email_id;
					$receiver_contact_no = $receiver_data->contact_no;

					$user_name = $senderData->email_id;
					$mail->AddAddress($emailid,$user_name);	//sender user email id
					$mail->AddBCC($aptusEmailId,'Aptus Email Address');
					//$mail->AddBCC($addBCC,'Aptus Email Address');
					$candidate_name = ucfirst($receiver_data->first_name)." ".ucfirst($receiver_data->last_name);
					if($jobdetails->is_delete=='1'){
						$receiver_email_data = $this->getEmailTemplate("applicants_notification_job_cancellation");
						//SMS body - rsolanki
						$receiver_sms_data = $this->getSMSTemplate("applicants_notification_job_cancellation");

						// start - added by jhtang - 20151221
						$searchArray = array("{site_logo}","{candidate_name}","{body}","{job_title}","{outlet_name}","{outlet_location}","{job_start_time}","{job_end_time}");
						$replaceArray = array($this->siteLogo,$candidate_name, $body, $jobdetails->job_title,$outletData->outlet_name,$outletData->outlet_address, $jobdetails->start_date.' '.$jobdetails->start_time, $jobdetails->end_date.' '.$jobdetails->end_time);
//						$searchArray = array("{site_logo}","{candidate_name}","{body}","{job_title}","{outlet_name}");
//						$replaceArray = array($this->siteLogo,$candidate_name, $body, $jobdetails->job_title,$outletData->outlet_name);
						// end - added by jhtang - 20151221
					}else if($jobdetails->is_approved=='1'){
						$body = "Jobs on Demand ogranization is looking for the candidates whose profile will match for the following requirements.";
						$searchArray = array("{site_logo}","{candidate_name}","{body}","{job_role}","{job_description}");
						$replaceArray = array($this->siteLogo,$candidate_name, $body, $jobdetails->job_title, $jobdetails->description);
					}
					if($receiver_email_data->body){
						$templateBody 	= $receiver_email_data->body;
						$mainBody 		= str_replace($searchArray, $replaceArray, $templateBody);
						$mail->Subject 	= $receiver_email_data->subject;
						$mail->Body    	= $mainBody;
					}else{
						$templateBody 	= $body;
						$mainBody 		= str_replace($searchArray, $replaceArray, $templateBody);
						$mail->Subject 	= "Jobs on Demand";
						$mail->Body    	= $mainBody;
					}
					if($receiver_sms_data->body){
						$smsBodyTemplate= $receiver_sms_data->body;
						$smsBody 		= str_replace($searchArray, $replaceArray, $smsBodyTemplate);
					}else{
						$smsBody="Jobs on Demand ogranization is looking for the candidates whose profile will match for the following requirements.";
					}
					$mail->send();
					//SMS send - rsolanki
					/*
					if($smsBody && $receiver_contact_no){
						$this->sendJodSMS($smsBody,$receiver_contact_no,'JOD',$notificationData->id);
					}
					*/
					//Notification Needed

					$this->updateData('email_notifications',$notificationData->id);
					break;
				case "applicant_hired":
					$receiver_email_data = $this->getEmailTemplate("applicant_hired");
					//SMS body - rsolanki
					$receiver_sms_data = $this->getSMSTemplate("applicant_hired");
					if($notificationData->sender_user_type=='3'){
						$sender_data = $this->getData('outlet_managers','user_accounts_id',$notificationData->sender_id);
					}
					if($notificationData->receiver_user_type=='4'){
						$receiver_data = $this->getData('applicants','user_account_id',$receiver_id);
					}
					$emailid 	= $receiver_data->email_id;
					$receiver_contact_no = $receiver_data->contact_no;

					$user_name = $senderData->email_id;
					$mail->AddAddress($emailid,$user_name);	//sender user email id
					//$mail->AddBCC($addBCC,'Aptus Email Address');

					$candidate_name = ucfirst($receiver_data->first_name)." ".ucfirst($receiver_data->last_name);
					$PDFpath = $this->get_contract_of_services($notificationData->job_id);

					// start - added by jhtang - 20151221
					$first_name = ucfirst($receiver_data->first_name);
					$searchArray = array("{site_logo}",    "{applicant_name}", "{job_title}",          "{outlet_name}",          "{outlet_manager_number}", "{company_name}");
					$replaceArray = array($this->siteLogo, $candidate_name,    $jobdetails->job_title, $outletData->outlet_name, $sender_data->contact_no,  $sender_company->company_name);
//					$searchArray = array("{site_logo}","{applicant_name}","{job_title}","{outlet_name}");
//					$replaceArray = array($this->siteLogo,$candidate_name, $jobdetails->job_title,$outletData->outlet_name );
					// end - added by jhtang - 20151221

					$templateBody 	= $receiver_email_data->body;
					$smsBodyTemplate= $receiver_sms_data->body;

					$mainBody 		= str_replace($searchArray, $replaceArray, $templateBody);
					$smsBody 		= str_replace($searchArray, $replaceArray, $smsBodyTemplate);
					if($PDFpath != false) {
						$res = $mail->AddAttachment($PDFpath, 'contract_for_service.pdf', $encoding = 'base64', $type = 'application/pdf');
					}
					$mail->Subject = $receiver_email_data->subject;

					$mail->Body    = $mainBody;
					$mail->send();

					//SMS send - rsolanki
					/*
					if($smsBody && $receiver_contact_no){
						$this->sendJodSMS($smsBody,$receiver_contact_no,'JOD',$notificationData->id);
					}
					*/
					//Notification Needed

					$this->updateData('email_notifications',$notificationData->id);
					break;
				case "acknowledge":
					$receiver_email_data = $this->getEmailTemplate("manager_notification_acknowledge");
					//SMS body - rsolanki
					$receiver_sms_data = $this->getSMSTemplate("manager_notification_acknowledge");

					$sender_data = $this->getData('applicants','user_account_id',$notificationData->sender_id);

					if($notificationData->receiver_user_type=='3'){
						$receiver_data = $this->getData('outlet_managers','user_accounts_id',$receiver_id);
					}else if($notificationData->receiver_user_type=='5'){
						$receiver_data = $this->getData('area_manager','user_accounts_id',$receiver_id);
					}

					$emailid = $receiver_data->email_id;
					$receiver_contact_no = $receiver_data->contact_no;


					$user_name = $senderData->email_id;
					$mail->AddAddress($emailid,$user_name);	//sender user email id
					//$mail->AddBCC($addBCC,'Aptus Email Address');

					$candidate_name = ucfirst($receiver_data->first_name)." ".ucfirst($receiver_data->last_name);
					$sender_name = ucfirst($sender_data->first_name)." ".ucfirst($sender_data->last_name);
					$searchArray = array("{site_logo}","{manager_name}","{job_title}","{job_id}","{job_start_date_time}","{outlet_name}","{outlet_location}","{sender_name}","{sender_emailid}","{sender_phone_number}");
					$replaceArray = array($this->siteLogo,$candidate_name,$jobdetails->job_title,$jobdetails->job_id,$jobdetails->start_date." ".$jobdetails->start_time,$outletData->outlet_name,$outletData->outlet_address, $sender_name,$sender_data->email_id,$sender_data->contact_no);

					$templateBody = $receiver_email_data->body;
					$smsBodyTemplate= $receiver_sms_data->body;

					$mainBody   = str_replace($searchArray, $replaceArray, $templateBody);
					$smsBody 	= str_replace($searchArray, $replaceArray, $smsBodyTemplate);

					$mail->Subject = $receiver_email_data->subject ;
					$mail->Body    = $mainBody;
					$mail->send();
					//SMS send - rsolanki
					if($smsBody && $receiver_contact_no){
						$this->sendJodSMS($smsBody,$receiver_contact_no,'JOD',$notificationData->id);
					}
					$this->updateData('email_notifications',$notificationData->id);
					break;
				case "feedback":
					$receiver_email_data = $this->getEmailTemplate("outlet_feedback");
					//SMS body - rsolanki
					$receiver_sms_data = $this->getSMSTemplate("outlet_feedback");

					$sender_data = $this->getData('applicants','user_account_id',$notificationData->sender_id);

					if($notificationData->receiver_user_type=='3'){
						$receiver_data = $this->getData('outlet_managers','user_accounts_id',$receiver_id);
					}else if($notificationData->receiver_user_type=='5'){
						$receiver_data = $this->getData('area_manager','user_accounts_id',$receiver_id);
					}
					$emailid = $receiver_data->email_id;
					$receiver_contact_no = $receiver_data->contact_no;

					$user_name = $senderData->email_id;
					$mail->AddAddress($emailid,$user_name);	//sender user email id
					//$mail->AddBCC($addBCC,'Aptus Email Address');

					$candidate_name = ucfirst($receiver_data->first_name)." ".ucfirst($receiver_data->last_name);
					$sender_name = ucfirst($sender_data->first_name)." ".ucfirst($sender_data->last_name);

					$feedback_details = $this->get_feedback_details_outlet($sender_data->user_account_id,$jobdetails->job_id);
					$rating 		= $feedback_details->rating;
					$comment 		= $feedback_details->feedback_text;
					$searchArray 	= array("{site_logo}","{user_name}","{applicant_name}","{job_title}","{outlet_name}","{rating}","{comment_text}");
					$replaceArray 	= array($this->siteLogo,$candidate_name, $sender_name, $jobdetails->job_title,$outletData->outlet_name,$rating,$comment);

					$templateBody    = $receiver_email_data->body;
					$smsBodyTemplate = $receiver_sms_data->body;


					$mainBody 	= str_replace($searchArray, $replaceArray, $templateBody);
					$smsBody 	= str_replace($searchArray, $replaceArray, $smsBodyTemplate);

					// start - added by jhtang - 20151221
					$mail->Subject = str_replace(array("{job_title}"), array($jobdetails->job_title), $receiver_email_data->subject);
//					$mail->Subject = 'Feedback for '.$jobdetails->job_title ;
					// end - added by jhtang - 20151221
					$mail->Body    = $mainBody;
					$mail->send();

					//SMS send - rsolanki
					/*
					if($smsBody && $receiver_contact_no){
						$this->sendJodSMS($smsBody,$receiver_contact_no,'JOD',$notificationData->id);
					}
					*/

					$this->updateData('email_notifications',$notificationData->id);
					break;
				case "manager_feedback":

					$receiver_email_data = $this->getEmailTemplate("applicant_feedback");
					if($notificationData->sender_user_type=='3'){
						$sender_data = $this->getData('outlet_managers','user_accounts_id',$notificationData->sender_id);
					}else if($notificationData->sender_user_type=='5'){
						$sender_data = $this->getData('area_manager','user_accounts_id',$notificationData->sender_id);
					}

					$receiver_data = $this->getData('applicants','user_account_id',$receiver_id);
					$emailid = $receiver_data->email_id;
					$user_name = $senderData->email_id;
					$mail->AddAddress($emailid,$user_name);	//sender user email id
					//$mail->AddBCC($addBCC,'Aptus Email Address');

					$candidate_name = ucfirst($receiver_data->first_name)." ".ucfirst($receiver_data->last_name);
					$sender_name = ucfirst($sender_data->first_name)." ".ucfirst($sender_data->last_name);
					$feedback_details = $this->get_feedback_details_applicants($receiver_data->user_account_id,$jobdetails->job_id);

					// start - added by jhtang - 20151221
					$first_name = ucfirst($receiver_data->first_name);
					$searchArray =  array("{site_logo}",   "{first_name}", "{job_id}",          "{job_title}",          "{outlet_name}",          "{rating}",                "{comment_text}");
					$replaceArray = array($this->siteLogo, $first_name,    $jobdetails->job_id, $jobdetails->job_title, $outletData->outlet_name, $feedback_details->rating, $feedback_details->additional_comments);
//					$searchArray = array("{site_logo}","{user_name}","{job_title}","{outlet_name}","{rating}","{comment_text}");
//					$replaceArray = array($this->siteLogo,$candidate_name, $jobdetails->job_title, $outletData->outlet_name,$feedback_details->rating,$feedback_details->additional_comments);
					// end - added by jhtang - 20151221

					$templateBody = $receiver_email_data->body;
					$mainBody=str_replace($searchArray, $replaceArray, $templateBody);

					// start - added by jhtang - 20151221
					$mail->Subject = str_replace(array("{job_title}"), array($jobdetails->job_title), $receiver_email_data->subject);
//					$mail->Subject = 'Feedback for '.$jobdetails->job_title ;
					// end - added by jhtang - 20151221

					$mail->Body    = $mainBody;
					$mail->send();

					$this->updateData('email_notifications',$notificationData->id);
					break;
				case "applicant_rejected":
					$receiver_email_data = $this->getEmailTemplate("applicant_rejected");
					//SMS body - rsolanki
					$receiver_sms_data = $this->getSMSTemplate("applicant_rejected");

					if($notificationData->sender_user_type=='3'){
						$sender_data = $this->getData('outlet_managers','user_accounts_id',$notificationData->sender_id);
					}
					if($notificationData->receiver_user_type=='4'){
						$receiver_data = $this->getData('applicants','user_account_id',$receiver_id);
					}
					$emailid = $receiver_data->email_id;
					$receiver_contact_no = $receiver_data->contact_no;

					$user_name = $senderData->email_id;

					$mail->AddAddress($emailid,$user_name);	//sender user email id
					$mail->AddBCC($aptusEmailId,'Aptus Email Address');
					//$mail->AddBCC($addBCC,'Aptus Email Address');
					$candidate_name = ucfirst($receiver_data->first_name)." ".ucfirst($receiver_data->last_name);

					// start - added by jhtang 20160528
					$searchArray = array("{site_logo}","{applicant_name}","{job_title}","{outlet_name}","{outlet_location}","{job_start_time}","{job_end_time}");
					$replaceArray = array($this->siteLogo,$candidate_name, $jobdetails->job_title,$outletData->outlet_name,$outletData->outlet_location, $jobdetails->start_date.' '.$jobdetails->start_time, $jobdetails->end_date.' '.$jobdetails->end_time);
//					$searchArray = array("{site_logo}","{applicant_name}","{job_title}","{outlet_name}");
//					$replaceArray = array($this->siteLogo,$candidate_name, $jobdetails->job_title,$outletData->outlet_name );
					// end - added by jhtang 20160528

					$templateBody = $receiver_email_data->body;
					$smsBodyTemplate= $receiver_sms_data->body;

					$mainBody= str_replace($searchArray, $replaceArray, $templateBody);
					$smsBody = str_replace($searchArray, $replaceArray, $smsBodyTemplate);

					$mail->Subject = $receiver_email_data->subject;
					$mail->Body    = $mainBody;
					$mail->send();
					//SMS send - rsolanki
					/*
					if($smsBody && $receiver_contact_no){
						$this->sendJodSMS($smsBody,$receiver_contact_no,'JOD',$notificationData->id);
					}
					*/
					$this->updateData('email_notifications',$notificationData->id);
					break;
				case "contract_of_service":
					$receiver_email_data = $this->getEmailTemplate("contract_of_service");
					$path = $this->get_contract_of_services($notificationData->job_id);
					if($path != false) {
						if($notificationData->receiver_user_type=='4'){
							$receiver_data = $this->getData('applicants','user_account_id',$receiver_id);
						}

						$emailid = $receiver_data->email_id;
						$user_name = $senderData->email_id;
						$mail->AddAddress($emailid,$user_name);	//sender user email id
						//$mail->AddBCC($addBCC,'Aptus Email Address');
						$candidate_name = ucfirst($receiver_data->first_name)." ".ucfirst($receiver_data->last_name);
						$body = "You are selected for the ".$jobdetails->job_title." job for which you have applied. Before you are moving forward to start the job, Please have a look in the attached PDF document which contains all the details of the contract for services.";
						$searchArray = array("{site_logo}","{applicant_name}","{body}");
						$replaceArray = array($this->siteLogo,$candidate_name, $body);
						$templateBody = $receiver_email_data->body;
						$mainBody=str_replace($searchArray, $replaceArray, $templateBody);
						$mail->Subject = $notificationData->subject;
						$mail->Body    = $mainBody;
						$res = $mail->AddAttachment($path, 'contract_for_service.pdf', $encoding = 'base64', $type = 'application/pdf');
						$mail->send();
						global $message;
						if(!$mail->Send()) {
						  $message =  "Invoice could not be send. Mailer Error: " . $mail->ErrorInfo;
						} else {
						  $message = "Invoice sent!";
						}

						$this->updateData('email_notifications',$notificationData->id);

					}
					break;
				case "appliedforjob":

					/*echo "<pre>";
					print_r($notificationData);
					print_r($receiver_id);
					print_r($purpose);
					die;*/

					$receiver_email_data = $this->getEmailTemplate("manager_notification_appliedforjob");
					$receiver_sms_data 	 = $this->getSMSTemplate("manager_notification_appliedforjob");

					$sender_data = $this->getData('applicants','user_account_id',$notificationData->sender_id);

					if($notificationData->receiver_user_type=='3'){
						$receiver_data = $this->getData('outlet_managers','user_accounts_id',$receiver_id);
					}else if($notificationData->receiver_user_type=='5'){
						$receiver_data = $this->getData('area_manager','user_accounts_id',$receiver_id);
					}

					$emailid = $receiver_data->email_id;
					$receiver_contact_no = $receiver_data->contact_no;

					$user_name = $senderData->email_id;
					$mail->AddAddress($emailid,$user_name);	//sender user email id
					$mail->AddBCC($aptusEmailId,'Aptus Email Address');

					$manager_name = ucfirst($receiver_data->first_name)." ".ucfirst($receiver_data->last_name);
					$sender_name = ucfirst($sender_data->first_name)." ".ucfirst($sender_data->last_name);
					$sender_address = ucfirst($sender_data->address);
					$sender_is_NEA_certified = ($sender_data->is_NEA_certified==1) ? 'Yes' : 'No';

					$portrait = ($sender_data->portrait) != "" ?  IMG_URL.$sender_data->portrait :  NOIMAGE;
					//$portrait = $this->siteLogo.'applicantes/'.$feedback->portrait;

					$averageRating = round($this->getAverageRating($notificationData->sender_id), 2);
					$userAge = $this->getAgeOfUser($sender_data->date_of_birth);

					$EmploymentData = 'No Existing Employer';
					$EmploymentExperience = 'No Job Experience';
					if(!empty($exprience_details)) {
						$EmploymentData = $EmploymentHistory->employer;
						$EmploymentExperience = $EmploymentHistory->length_of_service. ($EmploymentHistory->length_of_service>1)?'yrs':'yr';
					}

					$click_here_link = $this->baseUrl_new."/jobs/job_detail/".$jobdetails->job_id;

					$searchArray = array("{site_logo}","{portrait}","{manager_name}","{job_title}","{job_id}","{job_start_date}","{outlet_name}","{sender_name}","{sender_address}","{average_rating}","{user_age}","{sender_is_NEA_certified}","{employment_experience}","{employment_data}","{click_here_link}");
					$replaceArray = array($this->siteLogo,$portrait,$manager_name,$jobdetails->job_title,$jobdetails->job_id,$jobdetails->start_date,$outletData->outlet_name, $sender_name, $sender_address, $averageRating, $userAge, $sender_is_NEA_certified, $EmploymentExperience, $EmploymentData, $click_here_link);

					$templateBody = $receiver_email_data->body;
					$smsBodyTemplate= $receiver_sms_data->body;

					$mainBody=str_replace($searchArray, $replaceArray, $templateBody);
					$smsBody = str_replace($searchArray, $replaceArray, $smsBodyTemplate);

					$mailSubject = $receiver_email_data->subject;
					$mailSubjectBody = str_replace($searchArray, $replaceArray, $mailSubject);

					$mail->Subject =  $mailSubjectBody;
					$mail->Body    = $mainBody;

					$mail->send();
					//SMS send - rsolanki
					if($smsBody && $receiver_contact_no){
						$this->sendJodSMS($smsBody,$receiver_contact_no,'JOD',$notificationData->id);
					}
					$this->updateData('email_notifications',$notificationData->id);
					break;
				case "re_appliedforjob":
					$receiver_email_data = $this->getEmailTemplate("re_appliedforjob");
					$receiver_sms_data = $this->getSMSTemplate("re_appliedforjob");

					$sender_data = $this->getData('applicants','user_account_id',$notificationData->sender_id);

					if($notificationData->receiver_user_type=='3'){
						$receiver_data = $this->getData('outlet_managers','user_accounts_id',$receiver_id);
					}else if($notificationData->receiver_user_type=='5'){
						$receiver_data = $this->getData('area_manager','user_accounts_id',$receiver_id);
					}

					$emailid 	= $receiver_data->email_id;
					$receiver_contact_no = $receiver_data->contact_no;

					$user_name 	= $senderData->email_id;
					$mail->AddAddress($emailid,$user_name);	//sender user email id
					//$mail->AddBCC($addBCC,'Aptus Email Address');

					$manager_name 	= ucfirst($receiver_data->first_name)." ".ucfirst($receiver_data->last_name);
					$sender_name 	= ucfirst($sender_data->first_name)." ".ucfirst($sender_data->last_name);
					$sender_address = ucfirst($sender_data->address);


					$searchArray = array("{site_logo}","{manager_name}","{job_title}","{job_id}","{outlet_name}","{outlet_location}","{sender_name}","{job_start_date}","{job_start_time}");
					$replaceArray = array($this->siteLogo,$manager_name,$jobdetails->job_title,$jobdetails->job_id,$outletData->outlet_name,$outletData->outlet_address, $sender_name,$jobdetails->start_date,$jobdetails->start_time);

					$templateBody = $receiver_email_data->body;
					$smsBodyTemplate= $receiver_sms_data->body;

					$mainBody=str_replace($searchArray, $replaceArray, $templateBody);
					$smsBody = str_replace($searchArray, $replaceArray, $smsBodyTemplate);

					$mail->Subject = $receiver_email_data->subject ;
					$mail->Body    = $mainBody;
					$mail->send();
					//SMS send - rsolanki
					if($smsBody && $receiver_contact_no){
						$this->sendJodSMS($smsBody,$receiver_contact_no,'JOD',$notificationData->id);
					}
					$this->updateData('email_notifications',$notificationData->id);
					break;
				case "cancel_job":  //rsolanki - cancel job by applicant
					$receiver_email_data = $this->getEmailTemplate("cancel_job");
					//SMS body - rsolanki
					$receiver_sms_data = $this->getSMSTemplate("cancel_job");

					$sender_data 		 = $this->getData('applicants','user_account_id',$notificationData->sender_id);

					if($notificationData->receiver_user_type=='3'){
						$receiver_data = $this->getData('outlet_managers','user_accounts_id',$receiver_id);
					}else if($notificationData->receiver_user_type=='5'){
						$receiver_data = $this->getData('area_manager','user_accounts_id',$receiver_id);
					}

					$emailid 	= $receiver_data->email_id;
					$receiver_contact_no = $receiver_data->contact_no;

					$user_name 	= $senderData->email_id;
					$mail->AddAddress($emailid,$user_name);	//sender user email id
					$mail->AddBCC($aptusEmailId,'Aptus Email Address');
					//$mail->AddBCC($addBCC,'Aptus Email Address');

					$manager_name 	= ucfirst($receiver_data->first_name)." ".ucfirst($receiver_data->last_name);
					$sender_name 	= ucfirst($sender_data->first_name)." ".ucfirst($sender_data->last_name);
					$sender_display_name = $sender_data->display_name;
					$sender_id 			= $sender_data->user_account_id;
					$sender_address 	= ucfirst($sender_data->address);
					$sender_contact_no 	= $sender_data->contact_no;
					$sender_email 		= $sender_data->email_id;
					//get reason from job_applicants
					$job_applicants 	= $this->getData('job_applicants','job_id',$jobdetails->job_id);


					$searchArray 	= array("{site_logo}","{hiring_manager}","{job_title}","{job_id}","{outlet_name}","{outlet_location}","{job_start_date_time}","{candidate_name}","{applicant_user_name}","{applicant_mobile}","{applicant_email}","{applicant_reason}");
					$replaceArray 	= array($this->siteLogo,$manager_name,$jobdetails->job_title,$jobdetails->job_id,$outletData->outlet_name,$outletData->outlet_address, $jobdetails->start_date.' '.$jobdetails->start_time,$sender_name,$sender_id , $sender_contact_no,$sender_email,$job_applicants ->applicant_cancel_feedback);

					$templateBody 	= $receiver_email_data->body;
					$smsBodyTemplate= $receiver_sms_data->body;

					$mainBody  		= str_replace($searchArray, $replaceArray, $templateBody);
					$smsBody 		= str_replace($searchArray, $replaceArray, $smsBodyTemplate);

					$mail->Subject 	= $receiver_email_data->subject ;
					$mail->Body= $mainBody;
					$mail->send();
					//SMS send - rsolanki
					if($smsBody && $receiver_contact_no){
						$this->sendJodSMS($smsBody,$receiver_contact_no,'JOD',$notificationData->id);
					}
					$this->updateData('email_notifications',$notificationData->id);
					break;
				case "sendcompletejobnotification": //Might be this is used for CR-1 features when notification send to hiring manager for complete job notification.
					$cdateTime 	= date('Y-m-d H:i:s');
					$getMinutesDiffEndJob = $this->getMinutsTwoDates($cdateTime,$jobdetails->end_date." ".$jobdetails->end_time);
					//echo $cdateTime." ====== ".$jobdetails->end_date." ".$jobdetails->end_time." Time remaining = ".$getMinutesDiffEndJob;
					if($getMinutesDiffEndJob < 59) {
						$receiver_email_data = $this->getEmailTemplate("send_complete_job_notification");
						//SMS body - rsolanki
						$receiver_sms_data = $this->getSMSTemplate("send_complete_job_notification");

						$click_here_link = $this->baseUrl_new."/jobs/job_detail/".$jobdetails->job_id;

						if($notificationData->is_job_created_by_area_manager=='1') {
							$receiver_data = $this->getData('area_manager','user_accounts_id',$notificationData->outlet_manager_id);
						} else {
							$receiver_data = $this->getData('outlet_managers','user_accounts_id',$notificationData->outlet_manager_id);
						}

						$HQMdata = $this->getHQManager($receiver_data->company_id);
						$hqmName = ucfirst($HQMdata->first_name)." ".ucfirst($HQMdata->last_name);
						$hqmEmailId = $HQMdata->email_id;

						$manager_name = ucfirst($receiver_data->first_name)." ".ucfirst($receiver_data->last_name);

						$emailid = $receiver_data->email_id;
						$receiver_contact_no = $receiver_data->contact_no;

						$user_name = ucfirst($receiver_data->first_name)." ".ucfirst($receiver_data->last_name);
						$mail->AddAddress($emailid,$user_name);	//receiver user email id
						$mail->AddBCC($hqmEmailId,$hqmName);	//HQ Manager user email id
						//$mail->AddBCC($addBCC,'Aptus Email Address');
						$searchArray = array("{site_logo}","{manager_name}","{job_title}","{job_id}","{outlet_name}","{job_description}","{click_here_link}");
						$replaceArray = array($this->siteLogo, $manager_name, $jobdetails->job_title, $jobdetails->job_id, $outletData->outlet_name, $jobdetails->description, $click_here_link);

						$templateBody = $receiver_email_data->body;
						$smsBodyTemplate= $receiver_sms_data->body;

						$mainBody   = str_replace($searchArray, $replaceArray, $templateBody);
						$smsBody 	= str_replace($searchArray, $replaceArray, $smsBodyTemplate);

						$mail->Subject = $receiver_email_data->subject ;
						$mail->Body    = $mainBody;
						$mail->send();

						//SMS send - rsolanki
						if($smsBody && $receiver_contact_no){
							$this->sendJodSMS($smsBody,$receiver_contact_no,'JOD',$notificationData->id);
						}
					}
					break;
				case "sendclosejobnotificationtoapplicant":
					$receiver_email_data = $this->getEmailTemplate("send_close_job_notification_to_applicant");
					$click_here_link = $this->baseUrl_new."/jobs/job_detail/".$jobdetails->job_id;

					$receiver_data = $this->getData('applicants','user_account_id',$notificationData->applicant_user_account_id);
					$emailid = $receiver_data->email_id;
					$user_name = ucfirst($receiver_data->first_name)." ".ucfirst($receiver_data->last_name);
					$mail->AddAddress($emailid,$user_name);	//receiver user email id
					$mail->AddBCC($aptusEmailId,'Aptus Email Address');
					//$mail->AddBCC($addBCC,'Aptus Email Address');
					$searchArray = array("{site_logo}","{sender_name}","{job_title}","{job_id}","{outlet_name}","{job_description}","{click_here_link}");
					$replaceArray = array($this->siteLogo, $user_name, $jobdetails->job_title, $jobdetails->job_id, $outletData->outlet_name, $jobdetails->description, $click_here_link);
					$templateBody = $receiver_email_data->body;
					$mainBody=str_replace($searchArray, $replaceArray, $templateBody);
					$mail->Subject = $receiver_email_data->subject ;
					$mail->Body    = $mainBody;
					$mail->send();
					break;
				case "ReminderForSelectionOfJobApplicants":  //rsolanki - ReminderForSelectionOfJobApplicants
					if($notificationData->updateLastEmail==0)
					{
						$lastEmailUpdateHrs = $receiver_id;
						if($lastEmailUpdateHrs == 0  && $notificationData->updateLastEmail==0){
							$receiver_email_data = $this->getEmailTemplate("ReminderForSelectionOfJobApplicantsFinal");
						}else if($lastEmailUpdateHrs <= 8 && $lastEmailUpdateHrs > 0) {
							$receiver_email_data = $this->getEmailTemplate("ReminderForSelectionOfJobApplicants");
						}else if($lastEmailUpdateHrs == 0  && $notificationData->updateLastEmail==1) {
							$receiver_email_data = $this->getEmailTemplate("ReminderForSelectionOfJobApplicants");
						}

						//SMS body - rsolanki
						$receiver_sms_data = $this->getSMSTemplate("ReminderForSelectionOfJobApplicants");

						$getJobapplicantsData 	= $this->getData('job_applicants','job_id',$notificationData->job_id);
						$totalApplicants 	= $this->getDataCount('job_applicants','job_id',$notificationData->job_id);
						if($totalApplicants>0){
							$sender_data	= $this->getData('applicants','user_account_id',$getJobapplicantsData->applicant_user_account_id);

							if($notificationData->job_created_by=='1'){
								$receiver_data = $this->getData('outlet_managers','user_accounts_id',$notificationData->created_by);
							}else if($notificationData->job_created_by=='0'){
								$receiver_data = $this->getData('area_manager','user_accounts_id',$notificationData->created_by);
							}

							$emailid 	= $receiver_data->email_id;
							$receiver_contact_no = $receiver_data->contact_no;

							$user_name 	= $senderData->email_id;
							$mail->AddAddress($emailid,$user_name);	//Hiring manager email id

							//$mail->AddBCC($addBCC,'Aptus Email Address');

							$manager_name 		= ucfirst($receiver_data->first_name)." ".ucfirst($receiver_data->last_name);
							$sender_name 		= ucfirst($sender_data->first_name)." ".ucfirst($sender_data->last_name);
							$sender_display_name = $sender_data->display_name;
							$sender_id 			= $sender_data->user_account_id;
							$sender_address 	= ucfirst($sender_data->address);
							$sender_contact_no 	= $sender_data->contact_no;
							$sender_email 		= $sender_data->email_id;
							//get reason from job_applicants
							$job_applicants 	= $this->getData('job_applicants','job_id',$jobdetails->job_id);


							$searchArray 	= array("{site_logo}","{hiring_manager}","{outlet_name}","{outlet_location}","{job_start_date}","{job_start_time}","{number_of_applicant}");
							$replaceArray 	= array($this->siteLogo,$manager_name,$outletData->outlet_name,$outletData->outlet_address, $notificationData->start_date, $notificationData->start_time,$totalApplicants);

							$templateBody 	= $receiver_email_data->body;
							$smsBodyTemplate= $receiver_sms_data->body;

							$mainBody  		= str_replace($searchArray, $replaceArray, $templateBody);
							$smsBody 		= str_replace($searchArray, $replaceArray, $smsBodyTemplate);

							$mail->Subject 	= $receiver_email_data->subject ;
							$mail->Body= $mainBody;
							if($lastEmailUpdateHrs==0 && $notificationData->updateLastEmail==0){
								$HQMdata 	= $this->getHQManager($receiver_data->company_id);
								$hqmName 	= ucfirst($HQMdata->first_name)." ".ucfirst($HQMdata->last_name);
								$hqmEmailId = $HQMdata->email_id;

								//echo "<pre>"; print_r($hqmEmailId); die;
								$mail->AddAddress($hqmEmailId,$hqmName);
								$mail->AddAddress($aptusEmailId,"Aptus Email Address");



								//SMS send for final reminder to hiring manager - Neeraj Sharma
								if($smsBody && $receiver_contact_no){
									//echo "<pre>"; print_r($smsBody); die;
									$this->sendJodSMS($smsBody,$receiver_contact_no,'JOD',$notificationData->id);
								}

								//$mail->AddBCC($addBCC,'Aptus Email Address');
								$mail->send();

								//Update flag for sent last updates mail
								$this->updateLastEmailRecords($notificationData->job_id);
							}else if($lastEmailUpdateHrs <= 8 && $lastEmailUpdateHrs > 0) {
								$mail->send();
								//update sending hrs remaning
								$this->updateDataFlagSelectionJobApplicants($lastEmailUpdateHrs,$notificationData->job_id);

								//SMS send - rsolanki
								if($smsBody && $receiver_contact_no){
									$this->sendJodSMS($smsBody,$receiver_contact_no,'JOD',$notificationData->id);
								}
							}else if($lastEmailUpdateHrs==0 && $notificationData->updateLastEmail==1 && $notificationData->sentReminderLastToapplicant==0){
								$mail->send();
								//update sending hrs remaning
								$this->updateSentReminderLastToapplicant($notificationData->job_id);

								//SMS send - rsolanki
								if($smsBody && $receiver_contact_no){
									$this->sendJodSMS($smsBody,$receiver_contact_no,'JOD',$notificationData->id);
								}
							}
						}
					}
					break;
				case "sentFirstEmailNotificationToAptus":
					$receiver_email_data = $this->getEmailTemplate("sentFirstEmailNotificationToAptus");
					//Get user information
					$user_accounts_data = $this->getData('user_accounts','user_accounts_id',$notificationData->applicant_user_account_id);
					$applicant_data 	= $this->getData('applicants','user_account_id',$notificationData->applicant_user_account_id);
					$jobdetails 		= $this->getData('jobs','job_id',$notificationData->job_id);
					$outletDetails 		= $this->getData('outlets','outlet_id',$jobdetails->outlet_id);

					$user_name 			= ucfirst($applicant_data->first_name)." ".ucfirst($applicant_data->last_name);
					$job_id 			= $jobdetails->job_id;
					$outlet_name 		= $outletDetails->outlet_name;
					$outlet_location 	= $outletDetails->outlet_address;
					$job_start_date 	= $jobdetails->start_date;
					$job_start_time 	= $jobdetails->start_time;
					$job_applicant_name = $user_name;
					$job_applicant_user_account_id = $notificationData->applicant_user_account_id;
					$job_applicant_mobile_number = $applicant_data->contact_no;
					$job_applicant_email = $applicant_data->email_id;

					$mail->AddAddress($aptusEmailId,'Aptus Email Address');	//receiver user email id
					//$mail->AddBCC($addBCC,'Aptus Email Address');
					//$mail->AddBCC($aptusEmailId,'First Job Acknowledgement	by selected	Job	Applicant');

					$searchArray 	= array("{site_logo}","{job_id}","{outlet_name}","{outlet_location}","{job_start_date}","{job_start_time}","{job_applicant_name}","{job_applicant_user_account_id}","{job_applicant_mobile_number}","{job_applicant_email}");
					$replaceArray 	= array($this->siteLogo, $job_id,$outlet_name,$outlet_location,$job_start_date,$job_start_time,$job_applicant_name,$job_applicant_user_account_id,$job_applicant_mobile_number,$job_applicant_email);
					$templateBody 	= $receiver_email_data->body;
					$mainBody 		= str_replace($searchArray, $replaceArray, $templateBody);
					$mail->Subject 	= $receiver_email_data->subject ;
					$mail->Body    	= $mainBody;
					$mail->send();
					break;
				case "sentSecondEmailNotificationToAptus":
					$receiver_email_data = $this->getEmailTemplate("sentSecondEmailNotificationToAptus");
					//Get user information
					$user_accounts_data = $this->getData('user_accounts','user_accounts_id',$notificationData->applicant_user_account_id);
					$applicant_data 	= $this->getData('applicants','user_account_id',$notificationData->applicant_user_account_id);
					$jobdetails 		= $this->getData('jobs','job_id',$notificationData->job_id);
					$outletDetails 		= $this->getData('outlets','outlet_id',$jobdetails->outlet_id);

					$user_name 			= ucfirst($applicant_data->first_name)." ".ucfirst($applicant_data->last_name);
					$job_id 			= $jobdetails->job_id;
					$outlet_name 		= $outletDetails->outlet_name;
					$outlet_location 	= $outletDetails->outlet_address;
					$job_start_date 	= $jobdetails->start_date;
					$job_start_time 	= $jobdetails->start_time;
					$job_applicant_name = $user_name;
					$job_applicant_user_account_id = $notificationData->applicant_user_account_id;
					$job_applicant_mobile_number = $applicant_data->contact_no;
					$job_applicant_email = $applicant_data->email_id;

					$mail->AddAddress($aptusEmailId,'Aptus Email Address');	//receiver user email id
					//$mail->AddBCC($addBCC,'Aptus Email Address');
					//$mail->AddBCC($aptusEmailId,'Second	Job	Acknowledgement	by selected	Job	Applicant');

					$searchArray 	= array("{site_logo}","{job_id}","{outlet_name}","{outlet_location}","{job_start_date}","{job_start_time}","{job_applicant_name}","{job_applicant_user_account_id}","{job_applicant_mobile_number}","{job_applicant_email}");
					$replaceArray 	= array($this->siteLogo, $job_id,$outlet_name,$outlet_location,$job_start_date,$job_start_time,$job_applicant_name,$job_applicant_user_account_id,$job_applicant_mobile_number,$job_applicant_email);
					$templateBody 	= $receiver_email_data->body;
					$mainBody 		= str_replace($searchArray, $replaceArray, $templateBody);
					$mail->Subject 	= $receiver_email_data->subject ;
					$mail->Body    	= $mainBody;
					$mail->send();
					break;
				case "sentJobEndReminderEmailTemp":
					$receiver_email_data = $this->getEmailTemplate("sentJobEndReminderEmailTemp");
					$receiver_sms_data = $this->getSMSTemplate("sentJobEndReminderEmailTemp");

					$sender_data = $this->getData('applicants','user_account_id',$notificationData->applicant_user_account_id);

					if($notificationData->job_created_by=='1'){
						$receiver_data = $this->getData('outlet_managers','user_accounts_id',$notificationData->created_by);
					}else if($notificationData->job_created_by=='0'){
						$receiver_data = $this->getData('area_manager','user_accounts_id',$notificationData->created_by);
					}

					$emailid 	= $receiver_data->email_id;
					$receiver_contact_no = $receiver_data->contact_no;

					$user_name 	= $senderData->email_id;
					$mail->AddAddress($emailid,$user_name);	//sender user email id
					$mail->AddBCC($aptusEmailId,'Aptus Email Address');
					//$mail->AddBCC($addBCC,'Aptus Email Address');

					$manager_name 	= ucfirst($receiver_data->first_name)." ".ucfirst($receiver_data->last_name);
					$sender_name 	= ucfirst($sender_data->first_name)." ".ucfirst($sender_data->last_name);
					$sender_address = ucfirst($sender_data->address);

					// start - added by jhtang 20160627
					$searchArray = array("{site_logo}","{manager}","{outlet_name}","{outlet_location}","{applicant_name}","{job_end_date}","{job_end_time}","{job_id}","{job_title}");
					$replaceArray = array($this->siteLogo,$manager_name,$outletData->outlet_name,$outletData->outlet_address, $sender_name, $notificationData->end_date, $notificationData->end_time,$jobdetails->job_id,$jobdetails->job_title);
//					$searchArray = array("{site_logo}","{manager}","{outlet_name}","{outlet_location}","{applicant_name}","{job_end_time}");
//					$replaceArray = array($this->siteLogo,$manager_name,$outletData->outlet_name,$outletData->outlet_address, $sender_name,$notificationData->end_time);
					// end - added by jhtang 20160627

					$templateBody = $receiver_email_data->body;
					$smsBodyTemplate= $receiver_sms_data->body;

					$mainBody=str_replace($searchArray, $replaceArray, $templateBody);
					$smsBody = str_replace($searchArray, $replaceArray, $smsBodyTemplate);

					$mail->Subject = $receiver_email_data->subject ;
					$mail->Body    = $mainBody;
					$mail->send();
					//SMS send - rsolanki
					if($smsBody && $receiver_contact_no){
						$this->sendJodSMS($smsBody,$receiver_contact_no,'JOD',$notificationData->id);
					}
					$this->updateData('email_notifications',$notificationData->id);
					break;
				case "sendacknowledgenotificationtoapplicant":
					$receiver_email_data 	= $this->getEmailTemplate("send_acknowledge_notification_to_applicant");
					$click_here_link= $this->baseUrl_new."/jobs/job_detail/".$jobdetails->job_id;
					$receiver_data 	= $this->getData('applicants','user_account_id',$notificationData->applicant_user_account_id);
					$emailid 		= $receiver_data->email_id;
					$user_name 		= ucfirst($receiver_data->first_name)." ".ucfirst($receiver_data->last_name);
					$mail->AddAddress($emailid,$user_name);	//receiver user email id
					//$mail->AddBCC($addBCC,'Aptus Email Address');
					$searchArray 	= array("{site_logo}","{sender_name}","{job_title}","{job_id}","{outlet_name}","{job_description}","{click_here_link}");
					$replaceArray 	= array($this->siteLogo, $user_name, $jobdetails->job_title, $jobdetails->job_id, $outletData->outlet_name, $jobdetails->description, $click_here_link);
					$templateBody 	= $receiver_email_data->body;
					$mainBody=str_replace($searchArray, $replaceArray, $templateBody);
					$mail->Subject 	= $receiver_email_data->subject ;
					$mail->Body    	= $mainBody;
					$mail->send();
					die;
					break;
		}
	}

	function get_contract_of_services($job_id){
		$query = "SELECT outlet_id FROM `".DB_PREFIX."jobs` WHERE `job_id` = $job_id";
		$result = $this->_db->my_query($query);
		if($this->_db->my_num_rows($result)>0){
			$row = $this->_db->my_fetch_object($result);
			$query_outlet = "SELECT company_id FROM `".DB_PREFIX."outlets` WHERE `outlet_id` = $row->outlet_id";
			$outlet_result = $this->_db->my_query($query_outlet);
			$row_outlet = $this->_db->my_fetch_object($outlet_result);
			$query_company = "SELECT contract_of_service FROM `".DB_PREFIX."companies` WHERE `company_id` = $row_outlet->company_id";
			$company_result = $this->_db->my_query($query_company);
			$company = $this->_db->my_fetch_object($company_result);
			//print_r($company); die;
			if($company->contract_of_service!= ""){

				$path =  CONTRACT_OF_SERVICE_PATH.$company->contract_of_service;

			}else{
				$path = false;
			}
			return $path;
		}
	}

		public function logSetup($webserviceName)
		{
			//Start:maintain log file for testing purpose only.
				$file_name = "logs/log.txt";
				$base_path = realpath(dirname(__FILE__)).'/';
				$base_path = str_replace("/models","",$base_path);
				$file_path = $base_path.$file_name;

				if(file_exists($file_path) == false){
					$file_pointer = fopen($file_path, "a+");
				}
				$file_pointer = fopen($file_path, "a+");
				//"ERR". print_r($file_pointer); die("asdadadad");
				$file_pointer = fopen($file_path, "a+");
				fwrite($file_pointer, "##########################################################################\n");
				fwrite($file_pointer, "Date:".date('d-m-Y h:i:s'));
				fwrite($file_pointer, "\nName of Webservice: ".$webserviceName."\n");
				//fwrite($file_pointer, "\nRequested Parameters\n");
				//fwrite($file_pointer, "inputParameters=>".print_r($inputParameters, TRUE))."\n";
				//fwrite($file_pointer, "Response\n");
				//fwrite($file_pointer, print_r($records_array, TRUE));
				fclose($file_pointer);
			//End:maintain log file for testing purpose only.
		}

		function get_feedback_details_applicants($applicant_id,$job_id){
			$query = "SELECT additional_comments,rating FROM `".DB_PREFIX."applicants_feedback` WHERE `applicant_user_account_id` = '$applicant_id' AND `job_id` = '$job_id'";
			$feedback_result = $this->_db->my_query($query);
			$feedback = $this->_db->my_fetch_object($feedback_result);
			return $feedback;
		}

		function get_feedback_details_outlet($applicant_id,$job_id){
			$query = "SELECT feedback_text,rating FROM `".DB_PREFIX."outlet_feedback` WHERE `applicant_user_account_id` = '$applicant_id' AND `job_id` = '$job_id'";
			$feedback_result = $this->_db->my_query($query);
			$feedback = $this->_db->my_fetch_object($feedback_result);
			return $feedback;
		}


		/************************ ADDED BY NEERAJ SHARMA FOR ENHANCEMENT FEATURE - START ***********************/
		/**
		 * @method : getHQManager
		 * @param : userId
		 * @return : get HQ Manager Details
		*/
		function getHQManager($companyId) {
			$query = "SELECT * FROM `jod_headquater_manager` WHERE `company_id` = '$companyId'";
			$result = $this->_db->my_query($query);
			if($this->_db->my_num_rows($result)>0){
				$data = $this->_db->my_fetch_object($result);
				return $data;
			}else{
				return 0;
			}
		}

		/**
		 * @method : getEmploymentHistory
		 * @param : userId
		 * @return : Employment History List
		*/
		function getEmploymentHistory($cid='') {
			$query = "SELECT * FROM `jod_employment_histories` WHERE `user_account_id` = '$cid'";
			$result = $this->_db->my_query($query);
			if($this->_db->my_num_rows($result) > 0){
				return $data = $this->_db->my_fetch_object($result);
			} else {
				return false;
			}
		}

		/**
		 * @method : getTotalvoteCounts
		 * @param : userId
		 * @return : get Total vote Counts of user
		*/
		function getTotalvoteCounts($user_account_id) {
			$query = "SELECT count(feedback_id) as count FROM `jod_applicants_feedback` WHERE `applicant_user_account_id` = '$user_account_id'";
			$result = $this->_db->my_query($query);
			if($this->_db->my_num_rows($result)>0){
				$data = $this->_db->my_fetch_object($result);
				return $data->count;
			}else{
				return 0;
			}
		}

		/**
		 * @method : getAverageRating
		 * @param : userId
		 * @return : get Average Rating of user
		*/
		function getAverageRating($user_account_id){
			$total_completed_jobs = $this->getTotalvoteCounts($user_account_id);
			$query = "SELECT SUM(rating) as total_rating FROM `jod_applicants_feedback` WHERE `applicant_user_account_id` = '$user_account_id'";
			$result = $this->_db->my_query($query);
			if($this->_db->my_num_rows($result)>0){
				$data = $this->_db->my_fetch_object($result);
				if($total_completed_jobs > 0){
					$avg_rating =  $data->total_rating/$total_completed_jobs;
				}else{
						$avg_rating = 0;
				}
				return $avg_rating ;
			}else{
				return 0;
			}
		}


		/**
		 * @method : getAgeOfUser
		 * @param : userDob
		 * @return : get Age of user
		*/
		function getAgeOfUser($user_dob){
			$dob = date_create($user_dob);
			$today = date_create(date('Y-m-d'));
			$age = date_diff($dob,$today, true);
			return $age->y;
		}

		/************************ ADDED BY NEERAJ SHARMA FOR ENHANCEMENT FEATURE - END ***********************/
}
?>
