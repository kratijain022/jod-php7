<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class user {
	public function __construct() {
        $this->_db      = env::getInst();
        $this->_common  = new commonclass();
    }

	public function register_user() {
		$locale 		= $this->_common->locale();
        $result_array  	= array();
        $current_date 	= CURRENT_TIME;
		$unique_id 		= $this->_common->test_input($_REQUEST['unique_id']);
		$password 		= $this->_common->test_input($_REQUEST['password']);
		$first_name 	= $this->_common->test_input($_REQUEST['first_name']);
		$last_name 		= $this->_common->test_input($_REQUEST['last_name']);
		$display_name 	= $this->_common->test_input($_REQUEST['display_name']);
		$date_of_birth 	= $this->_common->test_input($_REQUEST['date_of_birth']);
		$email 			= $this->_common->test_input($_REQUEST['email']);
		$gender 		= $this->_common->test_input($_REQUEST['gender']);
		$school_attend 	= $this->_common->test_input($_REQUEST['school_attend']);
		$portrait 		= $this->_common->test_input($_REQUEST['portrait']);
	//	$portrait = 'iVBORw0KGgoAAAANSUhEUgAAA9QAAAIwCAMAAABQqNOWAAAAvVBMVEUAAAACAgIKCgoLCwsNDQ0PDgAODg4QDwAPDw8YGBgfHAAeHh4gHQAgICAvKgAvLy8wKwAwMDA8PDw/OAA/Pz9AOQA/Pz9PRgBPT09QRwBQUFBfVABeXl5gVQBfX19vYgBvb29wYwBwcHB/cQB+fn+AcQCAgICPgACPj4+QgQCQkJCfjgCfn5+gjwCgoKCvnACvr6+wnQCvr7C/qgC/v7/AqwC/wMDPuADOzs/fxgDe39/v1ADv7+//4wD///9stINcAAAmXElEQVR42u2d61oTSbRAOZcBjojCERFH1KiIqIxkRkYg5PD+j3UAuXSSXemurr1Tuypr/Zvvk56ku1eqal+qVv4PAKpiJfcHAABdkBqgMpAaoDKQGqAykBqgMpAaoDKQGqAykBqgMpAaoDKQGqAykBqgMpAaoDKQGqAykBqgMpAaoDKQGqAykBqgMpAaoDKQGqAykBqgMpAaoDKQGqAykBqgMpAaoDKQGqAykBqgMpAaoDKQGqAykBqgMpAaoDKQGqAykBqgMpAaoDKQGqAykBqgMpAaoDKQGqAykBqgMpAaoDKQGqAykBqgMpAaoDKQGqAykBqgMpAaoDKQGqAykBqgMpC6Ri7+vebvz/d8vf6v3B8JFgdSV8TZv18/v335ckVm7eXrz3/9e5n7Q4I5SF0FZ39/fr210omnrz//jdlVg9Sl8+/Xrjo32PqA2PWC1AVz8feHl9E+P4r99SL3FwATkLpQLv56/bS/0A8DNl5XCFIXyMVfb9OFvvf6L+bhtYHUpfHvh/gl9FzW3p7l/k6gClKXxOVfr9d0jf7Ny5+5vxkogtTFcL2KthD6fhae++uBGkhdBhdflSfds1ozWtcCUheAvdG3vGRtXQdI7Z1Ly1n3FG/JcNUAUvvm78UZfcPa19xfGNJBasecfTCJdc9lizl48SC1Vy4Xs5Ce5QPVKIWD1D75+TaP0Tc8ZbAuG6R2yOVXtSrQfnzOfQcgBaR2R85B+p6XhMELBqmd8ddWbqFvWTvJfSOgN0jtiYvPiw93h/iQ+2ZAX5DaD5rz7tWdBv0u8ZIoeKEgtRdU5t0bO4OD4en51Qynw4P9WLmJghcKUrsgPd69ujM4Pr1q4fx48Dzimmvfc98X6ANSO+AisXLs2udRm88PjI73VztfmarREkHq7FwkLaV3DlrH51mGnb1+m/vuQDxInZkUpZ8Pegh9x3HHFTbhsvJA6qz87L3F7+re8bi30b/n4d2Gazo8igOpM9Jb6Y2EIbrB+KCL1mtYXRhInY2Lnq3SG4PzdJ9jtCYIXhhInYnLD/2MPuge5+6odYf/K1YXBVLn4WufJJbmGP3IaK/9//w29/2CCJA6Byc9Sk1W91XW0RKnG+1WEwQvB6RePJc9FtPJse7UOfgWVhcDUi+c+Jm3+kJ6lvPW8tEtWqxLAakXzFl0Gstu2j0xWA/aPgeprVJA6sXyOXaQPrKcdk8wbMturXGGRxkg9SK5iBym9xYySN8zap2Ck9oqAqReICdRq+lV+5X0FOPW5BZWlwBSL4zLqNaNneMFG31LaxScfUYLAKkXxdlWhNKLCY4JtC6s3+a+j9AKUi+IiKn34ufdDc6xuniQejF0j3pvZJl3PzJuC5e9pgzFOUi9CLrXkO0M8yp91SFcRnGZc5B6AVxudVQ621J6kn2sLhqktqdriGw/41J6kmOsLhmkNuesW4jMj9I3VreEy9jjyDNIbU0np1cPFlYN2o22IDiF4I5BamM6OT1wpvSN1RtYXSpIbUsXp11NvB9oS21htVuQ2pQOTu+4VPrG6h2sLhOktqTd6Q0fSSyZfawuEqQ25KLN6dWD3N5idYUgtR2tNSd7XmfeDxxjdYEgtR0tOyKs5q8ITbf6JPdNhlmQ2oyW9uk9f2ks0eqWhDXbJvgDqa34Xv4wfUtbGQpWuwOpjTiba8JOGcM0VhcJUttwOfcMDudBb6wuG6S2Yd6CetVzblqibZtRrPYFUpswb0H93OKUO1vaSkax2hVIbcG8qpPnBS2nsbpIkNqCObsX7ef2E6urB6kNOKnOaawuCaTWZ07kO8rp8a/Dw93dzZu/29zdPfz2K6/V+1hdCEitz3sNp/95tz3797uHOYNsWF0ISK3ORbrT//wZvMbmx3xdIFhdBkitzpvQS/+8mzqjj+vz5dn94dTqD7lvPdyC1NoE60O75bJG71ba2cyldYvVb3PffLgBqbUJNVyudlkOd1L6VutMDSEtVr9kQ3AHILUyP0Pve4cjssYfOyp9w26emFmL1U/ZNiE/SK1MaEU9aPfldDPC6WsOPVq98jX3EwCk1iUU+t5oXVCP/1yJZTvLYN1mNVPw3CC1LqGBurUx6zxymL5lPcuxt21Wr/3M/RSWHKRW5TLQydGaof7WQ+kb3tl4e1PM9vHPw8N/xKR4m9XktvKC1KoEWi5X2wpGuga9Z9nVb/oaf9t9vP7mR2GS0Wo1B+jlBKlVCeSzWnY6Ge+2STKHbWWrx4fTtS+7s1q3Wr3yOfejWGKQWpNAmGx1vnfj7VZF5rGpGi77JpWzzU4HDlo/1suL3E9jaUFqTT71Gahbnd65Zt4ZlOt6Vo8D64D1mcG67Vz6lZU1ButMILUmW/LrPXegnuf06v7Rg03j04M9a6vnfJaZOHu71aysM4HUigRm3/v9PFrdn13LDuXF7KbOunrunKGP1StvyVlnAKkV+SK/2XPH0ZBHG0eyqOMjaSauEy2bH6+b+RpdrGYOngGkVuSVrOc8jwJr2LnHYR4LWv+p4HRL8Gt95oej7UieW55+ZrReMEitiPxWH83x6Ej+k0HLwHswa1P6+QCjljZu4X/Rts3//WiN1gsFqfUINGjNKTwZin/QYWPw8WzMLLkXsz33PPtNulm9svL679wPZ5lAaj3khNac/U7G4tjYoZ/r5vdgWqf1xGX1qF1NIeLXtsno43D99i8S1wsCqfWQl9RzHJX6sjofhzlzFk7isrpD2Gtd+mHaa/+7e56+/vpv7oe0DCC1HnIzR1jSofCvY87v2O/8f+pCFznFXrP24rIJtj4wFTcGqdW4lF/ioEXS5Hs/ag49mBpIkybgXYSUo3GnHRfWD6y9ZipuCVKrIcfJwktqYYSLPb/jOO3Pm3RYUgf/BzFT8Hte/kVI3AqkVkMuPdmPsCheyimrE8pFT7uYuBP666Mufz3NW+bhNiC1GvLBHMEs9WwGqc9AO2n1rrHU28E/P+8aBZ/gKcO1BUitxgvxvQ3tYzQ7UAeHwQir+x9nnzZSX0/BB32sXln7zOpaHaRWQ5Y6FLyaGaj7nls9YXX/oTpVaiFz3pEPjNbKILUackYrNLDNhL57r4cnhsj+Q3UX/+bXxfSJl91A04cySK1G1Ng2U+qRULu907hM/wB4l3H2qOUafQfrpye5H15VILUWcpo6JPV2x3/XhYlSzd656vbS77ll7HcfpedgTeO1JkithZymDoycM2Gy/tPma84bF+q9E/iw3bwux3aebrRfR4JdUvRAai1kqQOz6unZd0rZyNVEHcte74u0T53bZt+3jCPLRu9ZYwquBVJrESX19Cw19SD5xgS89zVaOzo2Ol5o1HMOzqH1SiC1FlFST8W+EwfqiYRU9ER+/G333eEN/9MiXfeGkdOdlT5gtQ5IrYUstSzCdFI4fTfQxzBXdBi9S4TshqhY3rDX0hqrVUBqLU7E11QeN6dmul3ntXN4jLx122OhQUfhYotj+ozWa0TLNEBqLeR9T2Spp2JJ0R4KPCgUHSnrlltejc+VnXadAzzylMyWAkithdykJUs9NYhp7MX/0CcVPf3ustNvl33TBEYHsbPw17kfYw0gtRbymlqWeipOpuD01fh+wI0PpI+ODvZ2du4C6P8hfo24zRuanA/irCaxlQ5SayFLLdeCTP6blGqyR47n/Q8jfhyEOfNG0kZJcWtrJuDpILUWMSmtLv8mmpvI1PPkjYKvB+4prTcSfycii1E+5X6Q5YPUWmSXWo/xcP9+LbxzlLzgP1+J4mnuB1k+SK1Ff6mT6r7tOD1NrXO7IzJYRrI6FaTWIkLqqXYOp1KrEZnZepP7SRYPUmtxJr6hcgp6uaQeMv9eLEithviGypHt5ZK6Y3nLA7kfZPFwB9VA6hCDOKl/5n6SpYPUajyRXtB18S2fHLqqlzoy/o3UiSC1GvJuouJbPlmPUb3UkfFvpE4EqdWI2CJ4Mh7sLk+tTqfy8ge+5H6SpYPUakS0aU0WWSVvkeCecVSojJqyRJBaDVlqscZycpOE7VhHyiMqVY3UiSC1GnLvpTi3HneYoldFpzM176FRKxGkVkMuKZP3LJg8Tk6hC8M7MXsREihLBKnVuBDfUDlRPZj4N/Uvqrsd1YXUOiC1HvIrKr7jw4l/splbuQUQ0VWN1IkgtR7PxFdUXjBPhoM19jNyTsRQjdSJILUeMQdUT4aDl2D+HTFUs/dJIkith5zTkktLJssx1uuPf0cM1bmfY/FwB/WQc1qBUXhy/p26s1gJdB2qn+R+jsWD1HrIOa3AtoKTRWXLECrrOlS/yP0ciwep9ZBPqA5sADxVjsFQ/cCr3M+xeJBakT/ElzQQ2p4MlS3DqrpjWRlVoqkgtSJy+HvY6RXXOHrHO90qwMlopYLUisSEv2decaWtOz3TrVnrIvdjLB6kVuS7+JKGDuAYTb7iu7mVWwBd9vX/I/dTLB+kVkTeUHS94yt+lFu5BdBhCxSC38kgtSbyazrq+IpTLHoDcbJkkFoTufp72PEV31yCCHh7CyZxsmSQWpM34msa3oNsMPkP3+VWzp5RW6yMJXU6SK2JXCgaPqp2/HzprD5qkZrSk3SQWhO5UHQ9/IqfTw1cS1BY1lJXxvF46SC1KvKLOicFPb15bv1Wt0zAyVKng9SqREbKrmaPpKnf6rkT8Ge5n2ANILUqcqRsbgno/tJZPW8Czkb+CiC1KrGRshv2ls3q0X+HpWbXEwWQWhU5UrYy9x2fDoFXHwM//c+g0xw4rwFS6yK/q/NrxcbT89FtT80do2+Hh6pH+H2cM/smTKYBUusiR8raZtTT6+p1P9v7/1ZwV62EdbQ9x2nqvlVAal16RMokq90srN/df6CPOjWsx+tznKZEVAek1qVPpOz2ZZ8eq33UgTeq0zd/pF9u/G5lHlST6YDUusjdlyvt7/v5VMuWj6F60PxIu6lL6/PNuU6zolYCqZWRX9cOS9LxZGrLx0n0UyG8tKX14XylabrUAqmVkfcp6zTuDjdi/2LRUq+svOsdmJ8bIbvhCTlqJZBamffiC9ttW8Hx414oqz7W1EJJZ0+tv82NkN1AlEwLpFYmbp+ymeHsPgzuY6CWtwp8F7+2Hu22Kc3kWw+kVqZ3pKyp9YabPPWx+G12/4m7ymHrME2KWhGk1kZ+ZyPmrONT1QKuRE7lvQI3f3RfH5y3raZXWFCrgtTaxBxoWwLjwL6+6x+7/VCN55WF3vPHWe7HVhNIrU3sPmX+GYV6JXd/tP/xj/aZN04rg9TayDVle7nNTOI0tF/3+sf5mevTDjNvnNYGqbWRuy+3c3uZyHFwG/7Nb8Fp+Gh+Veg9z3BaF6RWR35zc1uZzEF4b7Ft0etxN6VXXhEjUwap1XkivrrlH78xnqP1yvb0PHzcIY11CxsYqYPU6kQdaFsUc7Ve2Xz342HAHnVVmqm3AUitTtyBtmUxPp5/xN1vsTuupa95w9TbAKRW50R8fcsOfzc4btmNf+W//qer0n+c5H5WdYLU6siFol2rvwvgdL+rtfN5Rv+0DUitj/wK51ZRk9FB2zl3HaCDwwqk1kcOf/vopVSjdRbewhM6Lc1Aan1qq/4OMBpsRIrcgAiZIUitj7xPgpMOaVWG+/2m4UTITEFqfeTq7zpyWjMc/2+809SQ2YLU+sjV39XktBr8Omzf0YRheuEgtT4X4rtcekvHtM/f3nXqwGKYXjxIbYD8NufWUInRr2+Hu/18vuYJw7Q9SG2AfKBWyTmt8a9fvw4PD3f72/yb9wzTCwCpDSg+p3Xj8I9riT/uXpOmcZMXdG8sBKQ2QG7pcJ7Tuhb5281YvNuxvyqaJ99zP5dlAakNKCunNf51PSInTqs78ImZ96JAagPknNZ+bnsFfh2aDcyTvKF5Y3EgtQFl9GmNvv25EJ+veYHSiwSpLRDf7M3cFjcZ/1AMgLUpTe/GYkFqC/4QX+7cIj8yereYSTdKZwGpLZBzWr1PgVVXemFGo3QOkNoC+ZQOJ4nqrnsCKvCGxHQOkNoCx4nqLqfV6fDHJ8JjeUBqC+RDqj0kqo8XNUw/o9QkG0htgZyodiD14YIGaebdOUFqC7wmqgcLUfoVg3RekNoEn1IfL8DoZ18oB80NUpsgvu/rmZ0+N19Pv/hCbMwBSG2CnKjO6/TY2OlX3xmjfYDUJnisPlE6V0Meoj9RZOIHpDZB3iU4a/XJqZXQz96fMES7AqlNkKtPskpt0cDx7M0XRmh/ILUJ/rZJUI58//HizaefDNA+QWoT/FWfbCrZ/OTa5hOGZ9cgtQnupE5YUT95cc2nT5++//xJwqoIkNoEeT//jNUnezEz61uHP/28Jvd9hD4gtQ3OpB61u3xj8slParYrAKltEL3Jd/LOYK7O778Q86oJpLZBPqQjm9TBMNmT94zN1YHUNviqEz0PLZ+/5L5PYABS2yBLnes4rYOA04zSVYLUNvgqKQtsYcQJlHWC1Da4kjoQ+36R+yaBDUhtgyupAyWiZKErBaltOBE1ylRSJjddPsl9j8AIpLbBVZ2ovKR+n/segRFIbYMrqQmTLRdIbcOZI6kDzRy5bxFYwaM1QvQoT/H3EbHv5QKpjXAktdyh9Sn3HQIrkNoIR1LLhd8sqasFqY3wI/VYXlKz4UG1ILURYvF3lu385TgZWep6QWoj/LRpyd0cr3LfIDADqY3wI7VcT0acrF6Q2gg/Uu+In4TC73pBaiPeuJFaPkKL/YvqBamNkNu0cpymRZxs2UBqI9z0XspbGVFPVjFIbYQbqeWMFi1aFYPURriRWs5oEfyuGKQ2wrnUFIlWDFIb8d2L1GS0lg6kNuKnb6lz3x4whKdrhCz10eKlXkfqZYOna4Sb/YzEz0FGq2aQ2gikhlwgtRFIDblAaiO8SC3XnpCmrhmkNgKpIRdIbQRSQy6Q2ohLJ1LL52h9z317wBCktsKJ1HKVKAVlNYPUViA1ZAKprRBl2kdqMAeprRBlWvzG30i9fCC1FUgNmUBqK5AaMoHUViA1ZAKprXAtNQdp1QxSW+Fa6tw3Byzh8VrhWmpG6ppBaitcS82aumaQ2gqkhkwgtRVIDZlAaiuQGjKB1FYgNWQCqa1AasgEUluB1JAJpLYCqSETSG0FUkMmkNoKJ1IPxc/BHmU1g9RWOJGa3USXD6S2AqkhE0htBVJDJpDaCqSGTCC1FU6kPhc/x6vcdwcMQWornEjNqZfLB1JbgdSQCaS2AqkhE0hthSgTh86DPUhthRepn4sfJPfdAUN4ulZ4kXoHqZcNnq4VSA2Z4Oka8ROpIRM8XSPcSL0nfpCz3PcH7EBqI9xITUP10oHURiA15AKpjXAuNbskVAxSG+FG6qH4QWjTqhikNsKN1PReLh1IbYRzqd/kvj9gB1IbIUt9tHipKf5eOpDaCFnqUy9SP819f8AOpDbiixupN8RPkvv+gB08XCM+uZFarhO9yH2DwAykNsKP1HKdKNUn9YLURviRmuqTZQOpjfAj9ZH4SUhU1wtSG+FHajlRTU6rXpDaCD9Sj8RPQk6rXpDaCFnqDE4HEtU8+Hrh2Rrxyo/Uck6L8He1ILURL/xILee0CH9XC1Ib4UhqOaf1PvcdAiuQ2ghHUsvh763cdwisQGojHEkth7958tXCozXCkdRXq0TKlgqkNkKU+nkeqeXwNzVltYLURqxJHmU49PIGOVJGTVmtILURK46kHoofZi33LQIjkNoIT1IHImWc0lEpSG2EJ6kDm5+wqK4UpDbCldT74qchU10pSG2EqNF+JqmP5fn3Ze6bBCYgtQ1nokUZtv2+JbCopvy7TpDaBjd7+f9GXlSzo3+dILUNzqSWF9UkteoEqW1wJnVgUX2S+zaBBUhtgzOpx7LUzL+rBKltOBElGuaS+uo58+/lAalt8LPv4G/k8m/m31WC1DZ4k/pclvpV7vsEBiC1Dd6kDiS1qD+pEaS2wZ3UclKL+pMaQWobZKnP80k9ZP69NCC1DZ52M/qNvKcRR9pWCFLb4E/qwPybnYLrA6lt8Cd1YP5Nqro+kNoGf1KH5t+EyqoDqW0QpV7NKvVAlpqtEqoDqW0Q/cm18clvAvUnbFVWHUhtg0OpQ/UndHXUBlLb4FHqo8BQTVVZZSC1DR6lDmxqxK6itYHUJlyK9uTad/Ae+aDqlae57xbogtQmONsj4Y5hYKgmq1UXSG2CT6lDoTKG6rpAahOcSh3YKoFTbesCqU1wKnVgq7KVl7nvF2iC1CZ8Ed3Jt0XZPYGuDobqqkBqE9ztkXBHqKqMtuqaQGoTvEp9tROwmrbqikBqE9xKPQxITa1oRSC1CW9Ec0a5lb4KZrUYqisCqU1w2E59R+AAHmpFKwKpTfArdWivhDXaOqoBqU1wLHWoAIWhuhqQ2oQtv1KPGaprB6lNELXJ3Hl5zyAwVNPWUQtIbYJnqUNt1bR11AJSm+BZ6mCtKEN1JSC1BZeupQ4N1ewrWglIbYHTJq17Ajug0NZRCUhtgXOpTwNS04FZB0htgXOpg20dbAFeBUhtgXephwGpaeuoAqS2wG2T1j20ddQMUlvgXupQWwdDdQ0gtQXupQ4O1dSKVgBSW/DevdShI3ho66gApLbAcZPWHbR1VAxSW+Bf6mAHJrWi5YPUFhQgdWiopq2jfJDagqf+paato16Q2gLRFi/9HHfQ1lEtSG1BCVJzWke1ILUFRUgdOq2Dto7SQWoDzkRZ9nJbPA2ndVQKUhvgvZ/jjlAHJrWihYPUBhQi9dVzhuoqQWoDSpGa0zrqBKkNkPs5jnM7PEugrYNa0bJBagP8N2ndEWrroAClaJDagGKkpla0SpDaAP+dl/fQ1lEjSG1AAf0cd4RqRSlAKRmkNqAcqakVrRGkNqAgqUNDNQUoBYPUBqxJmqzm9leGWtH6QGoDREu89XPcEaoVfZ/7JkJvkNqAkqQO1YpSgFIuSK2P7zMvpwnVin7JfRuhL0itj1z6Pchtb4hArSgFKMWC1PqU0s9xBwUotYHU+hQmdahWlAKUUkFqfb6IjhzlljfIIDBUc7BtoSC1PsX0c9xBAUplILU+pUl9tRewmqxWmSC1Pm9KkzpUgMIOKGWC1PrIpd/j3OrOIVSAkvtOQi+QWp+C+jnuCBWgkNUqEqTWpzypQwUoHMFTJEitj+jHRm5v5xIqQKGtukSQWh9RD6+l378JFaCQ1SoRpNanQKmDO6DQVl0gSK3OWYlShwpQyGoVCFKrU1jp9x2BHVDIahUIUqtTptTDwFBNVqs8kFqd70VKTVarHpBaHbn0e6gt4Y93u+80S09DBSj0ahUHUquzkH6O8+3bq/6pV3xKVqsakFqdRfRzjNfvLrurd81BYKimV6s0kFqdRfRzPKaV9ab1oawWOxCWBlKrs4DS74Z/+3pXDbRVswNhaSC1Ok/tpW7UfykWtYTaqk9y31GIA6nVEcV4rul0c6KsWakWyGqxA2FhILU6ohiqVaLNQm3N/QxDWS0KwMsCqbW5MJe6OVCvagbgQlktztUqC6TWxv58juZArVuoFujVogC8LJBaG/PSb7OBOpzVogC8KJBam0/WUtsN1MFeLUJlRYHU2lhXiT4Uk+kP1BSA1wFSa2NdJdrcTkz/JM1AVosC8JJAam3kgrKRlnXNgVrvquJPRjNURgF4QSC1NsZVok3rFEtEH34zCJWVD1Jrs2Yrte1AHcxqsVdCQSC1NqITarUnzUiWwUAdLgAnVFYOSK3Mpa3Um8YDNaGyCkBqZeTakz0l44bWA/XV1RGhstJBamVsC8p2zQfqYAE4obJiQGplTAvKrHouJyBUVjpIrYxpQdm+/iVnOSdUVjhIrcwLQ6mbhSequy5MEjiCngbMUkBqZSy3HWzms47tpA4UgNOAWQpIrYxl7cn24wVX7ZwmVFY6SK2MqIPOifPNxa7pKT6BUBkNmIWA1LpYbmbUdM0onzX769GEvcrKAKl1saw9Wde+YJBAqIzDqssAqXUxPPKyGb9SP24v/L9qwLb+ZYDUushpapV9fBsHaPReo//4c327w1mZoVAZ2/oXAVLrYrfvSbPTuefIP76rMm1PhwVCZXR1FAFS6yKnqc8VpG5OiXuGyd51nr2HGjDp6igBpNbF7iCtxuy7ZzD9Mai92fpvAw2YpKpLAKl1EVXQqBRpzr57VpM1dkJqnToEGjBJVZcAUqtil6Zuzr57Fp02pG5d5Ie29SdVXQBIrYpdmnov/XIxUocOqyZVXQBIrYpZmlph9h0n9VCWmlR1ASC1KmZp6qZjfVu+oqS+CqSqf+a+xdAKUqvyShRBIU3dSBz3nszHST2QpSZV7R+kVsUsTd3YRbT3uB8ndaCrg65q/yC1KrII6U43g9G9G7TipA51dZCqdg9SayJv+q3QTd1IG/ffxyhS6kCq+lXumwxtILUmckZLIU3dSDD1P+lyJ07q0LFalIp6B6k1kTNaCgfOrsf52Cp1p1V+IFXN/Ns7SK2J1abfzaBV/6vsRF5kyPy7TJBaE6uMVmN5mzCXX4/9ZQikqikVdQ5Sa2KV0WpkqROG/ceLdOwwGchSf8l9m2E+SK2JLEGy0829gROG/ejhPpCq5gAe5yC1ImYZrcbV+h8LcBotdairmvm3b5BaEauMVkPHhNN2GlfpGo9n/l0kSK3IF1GB9IxWo2ok4VDqRrSt68I80FXN/Ns3SK3Ie1GB9B6tgcrFDnpcJVAqygGYrkFqRaxOvIwsBQuw3+MqgVJRtkpwDVIrIu86mH5CTnSCue2noXOSjfl3iSC1IrIByU7HJ5hFNvt8pECpKPFvzyC1HgsIfqdcrNdPQ+AAHuLfnkFqPeR2jvRdB4ePF0uIpI96/TQEWrWYf3sGqfWwaufoE7aepTHex/zOMP8uD6TWQw5+px9QGbm5QYAeaeobmH+XB1LrIQe/09s59lSkbvw0xOwxHJh/03/pGKTWQ379k52O7oNuvUrUT0Ng/s3+J35BajXk4HdCsfY92ypSN64SlTkPzL/Z/8QvSK2GXPmtcOTO48VSGr76Th6YfxcHUqshV34rHLnzeLGENHUjoxU5eWD+XRpIrYZV8FtH6v4VLIH590nu+w0hkFqNNfHdVzidQ0XqRvA7cvIQmH9z/o5bkFoLedsTheC3jtSDx6vEnpopz785/9ItSK2FWfC7sRpOqBJNaN8MzL9pqvYKUmshF4km7FRyT2M1nBB1a/RoxW5zFph/U1TmFaTWQt7zWyH4rSN1yopA3v/kRe47DgGQWost8c1XOJpaReqk9s3A/icktZyC1FrIL37/HX11pW6si+NXBIH9T0hqOQWplZDjZCk7lahK3T+jdYO8/zdJLacgtRJykajCKbbNcbK/1Gl7Fw7EL7eW+56DDFIr8UZ87xXiZDp56kbwu8dGiKfilyOp5RSkVmJLfO0VikR1pG58pj5/Lp9/SVLLJ0ithDyWpW8PrCN16t6F++K3I6nlE6TWwS5O1hwmt/teohH87tULGigqy33XQYTnooNdnExl55O04HewqOxn7tsOEkitgxwnSz8bT0nqxiX6LfPlojKO33EJUuuwJb70sf1QMoPHC/YtUGsEv/v1gh6I349FtUuQWgd5eqrQTH2lskVw4zP1u0AgqZX7toMEj0UFOU6m0Ux9NRGl6pn3Vji4R05qsaj2CFKrYBknayrZs5MzqfL7N/JOCSyqPYLUKsh9lzpxsmbouefPRGNV3rfGTe7UYlHtEaRWQT6cQydONjH17XcBhVPrA51auW88CPBUNAjsT6YTJ+t3XPwEjVPre9e4saguBqTWwDRONlGk2Wvwb8zf+9e4yYtqyr8dgtQayPuTKcXJJtazvZbpKqfWy4tqeqodgtQamMbJrq7OH6/Zq/o79Udh+kM0YKNghyC1BvI+/ip9l7c0Ltpnf6TG9D3h1Hp5Uc1GZf5AagXO5CW1St/lLYml2wrB76vQopqNyvyB1Ap8F193nb7LWwaPV+1TPJI40N8hl39TfuIPpFZAbtFSOMT2nuHjVTfj/7qxGk45C1cu/+ZIW38gtQJb4uuusj/ZHY3LxmeqGz8JSQF58VsSKfMHUqcTKD1R2Mf/gcaqOD5+nbpDwj1yTzWRMncgdTonstSKTje9jJ9/NyJcSQF5eaMyasrcgdTpyKUnCuddPtJMEkfPv7dT5u4N5I3KqClzB1Kn80J82dVKT27ZSLiw1uxBjpRRU+YOpE5Hnn1rtWj9ZtB//t3or0qcPYjfk+5LdyB1MoFuDr3Skxua8+/IhbFK5fctYqSMw3fcgdTJyLueKJae3NKYf0cmwHXOt75Brikj/O0NpE5G7uZQLD25pTH/jpwEaGW0QjVlhL+9gdTJyN0cmqUnN4x6X7uhYuI6X46Ufc/9AGAKpE4l0M2hWXpyS2NBux5Vwa2ww/Adcvcl1d/eQOpU5CW1aunJLc0scVQDpZ7UcvibnJY3kDoVeUmttuvJA+NGP3NUVkun8fKWDemrktPyBlKnspgl9dVklWbM4lhR6h3pq9LS4Q2kTmRRS+rJUFnMUK0o9UD8rrkfAUzBE0lkUUvqq8mhOqIARVHqA6QuAZ5IIotaUl9NZpR2e0mdWuUm7yhKotoZSJ3IwpbUk3pGDLqKI/UpUpcAUqexuCX1Vd+hGqmXDaROY4FL6qvJlFLn1mhFqeUDteiodgZSpyEvqVU3SGjQLEDpvK3oQHECIX5bSsqcgdRpyEtq3Q0SGjSH6q61oooVZUhdBEidRGBJrXc2R1jQzsE4pF42kDqJwJI6Ycv8+fSpFdXr0kLqMkDqJOTtyayW1JOGdlVUr58aqcsAqZOQB2qbLPUtzfhzx6zWEKmXDKROIbDjt02W+jfNWtFuWS297YyQugyQOoX3stSGTk8M1d2yWg2pk/dYQuoSQOoUtsSX3KTw+4FGLUm3HVBGep9sjNQlgNQJXMgDdcLB7h2I3wHl8d9vJ/6/T5G6BJA6Aflc6rSzbdpZjZVUb2GA1EWA1AnINaLaO35PM4j9/XiuJrXcT812os5A6gTkGlHtHb+naYbKOtWj7kT+CITh3MsiQOr+BGpEdQ/REmgclLEeqWJism1b/MIXuR8ETILU/ZGPsFU+REtgGPkL0pg0J8bw5C+c+znAFDyR/sg1ohvWTkefq9WIl6dVn8hxsme5nwNM8f/IukmoNohIrQAAAABJRU5ErkJggg==';
		$region_number 	= $this->_common->test_input($_REQUEST['region_number']);
		$contact_no 	= $this->_common->test_input($_REQUEST['contact_number']);
		$is_NEA_certified = $this->_common->test_input($_REQUEST['is_NEA_certified']);
		$referral_code 	= $this->_common->test_input($_REQUEST['referral_code']); //optional
		$device_type 	= $this->_common->test_input($_REQUEST['device_type']); //optional
		$device_id 		= $this->_common->test_input($_REQUEST['device_id']); //optional
		/* if($referral_code == "") {
			$referral_code = rand(10000,100000);
		} */

		// start - added by jhtang 20160227
		//Sent verify code for mobile number verification - rsolanki
		//$randomNum = rand(10000,100000);
		//$newContactNo = $region_number.$contact_no;
		//$this->sendJodSMS($newContactNo,$randomNum);
		// end - added by jhtang 20160227

		$my_referral_code 	= $this->_common->my_referral_code();
		$emp_histories 		= json_decode($_REQUEST['employement_history']);//optional
		if($unique_id != "" && $password != "" && $first_name != "" && $last_name != "" && $display_name != "" && $date_of_birth != "" && $email != ""  && $school_attend != "" && $portrait != "" && $region_number != "" && $contact_no != ""&& $is_NEA_certified != "") {
			if (!filter_var($email, FILTER_VALIDATE_EMAIL) && $email != "") {
				$result_array['result_code']= 1;
				$result_array['error_msg'] 	= $this->_common->langText($locale,'txt.register.error.invalidemail');
			} else {
				// start - added by jhtang 20160227
				if($this->_common->check_user_registered($unique_id,$contact_no) > 0) {
//				if($this->_common->check_user_availability($unique_id) > 0) {
				// end - added by jhtang 20160227
					$result_array['result_code']= 1;
					$result_array['error_msg'] 	= $this->_common->langText($locale,'txt.register.error.unique');
				} else {
					// start - added by jhtang 20160227
					//Sent verify code for mobile number verification - rsolanki
					$randomNum = rand(10000,100000);
					$newContactNo = $region_number.$contact_no;
					$this->sendJodSMS($newContactNo,$randomNum);
					// end - added by jhtang 20160227

					$image = 'avatar.png';
					if($portrait != ""){
						$image = $this->_common->image_upload($portrait);
					}
					$secret_token 	= uniqid();
					$insertUser 	= "INSERT INTO `".DB_PREFIX."user_accounts` (`email_id`, `unique_id` ,`password` ,`role_id` ,`last_login_at` ,`created_at` ,`updated_at` ,`secret_token`, `status`,`device_id`,`device_type`,`verify_code`) VALUES ('$email','$unique_id' ,'$password' ,'4' ,'$current_date' ,'$current_date' ,'$current_date' ,'$secret_token' ,'1','$device_id','$device_type','$randomNum')";
					$this->_db->my_query($insertUser);
					$user_account_id = mysql_insert_id();
					if($user_account_id > 0) {
						$insertApplicant = "INSERT INTO `".DB_PREFIX."applicants` (`user_account_id`, `first_name`, `last_name`, `display_name`, `date_of_birth`, `email_id`, `gender`, `school_attend`, `portrait`, `region_number`, `contact_no`, `is_NEA_certified`, `referral_code`, `my_referral_code`, `created_at`, `updated_at`) VALUES ('$user_account_id', '$first_name', '$last_name', '$display_name', '$date_of_birth', '$email', '$gender', '$school_attend', '$image', '$region_number', '$contact_no', '$is_NEA_certified', '$referral_code', '$my_referral_code', '$current_date', '$current_date');";
						$this->_db->my_query($insertApplicant);
						foreach($emp_histories as $emp_history) {
							$employer 			= $emp_history->employer;
							$length_of_service 	= $emp_history->length_of_service;
							$job_type_array 	= $emp_history->job_roles;
							$job_types 			= implode(",",$job_type_array);
							$insertEmpHistory 	= "INSERT INTO `".DB_PREFIX."employment_histories` (`user_account_id`, `employer`, `length_of_service`, `job_types`, `created_at`, `updated_at`) VALUES ('$user_account_id', '$employer', '$length_of_service', '$job_types', '$current_date', '$current_date');";
							$this->_db->my_query($insertEmpHistory);
						}
						$result_array['result_code'] = 0;
						$result['secret_token'] 	 = $secret_token;
						$result['my_referral_code']  = $my_referral_code;
						$result_array['result'] 	 = $result;
						$result_array['success_msg'] = $this->_common->langText($locale,'txt.register.success');
					}
				}
			}
		} else {
			$result_array['result_code'] = 1;
			$result_array['error_msg']   = $this->_common->langText($locale,'txt.register.error.required');
		}
		return $result_array;
	}

	//Send sms - rsolanki
	function sendJodSMS($contact_no,$verify_code){
		require_once("libraries/APIClient2.php");
		if($contact_no && $verify_code){

			$contact_no = str_replace("+", "", $contact_no);
			$contact_no = str_replace(" + ", "", $contact_no);
			$contact_no = str_replace("  ", "", $contact_no);

			//$contact_no = "6598637239";
			// start - added by jhtang 20160528
			$msg 		= "Your JobsOnDemand verification code: ".$verify_code;
			$sentSms 	= new TransmitsmsApi("8cbc0e61b128f2745f4914e3c7833333",'sms_my_secret');
//			$msg 		= "<b>".$verify_code."</b> is your Jobs on Demand mobile verification code.";
//			$sentSms 	= new TransmitsmsApi("8cbc0e61b128f2745f4914e3c7833333",'my_secret');
			// end - added by jhtang 20160528
			$updatedMessage = htmlspecialchars($msg);
			$result 	= $sentSms->sendSms($updatedMessage,$contact_no,"JOD");
				if($result->error->code){
			}
		}
	}

	//sms varification  - rsolanki
	public function verify_user() {
		// start - added by jhtang 20150915
        require_once("libraries/sendMailCron.php");
        // end - added by jhtang 20150915
		$locale 	   = $this->_common->locale();
        $result_array  = array();
        $current_date  = CURRENT_TIME;

		if($this->_common->test_input($_REQUEST['unique_id']) != '' && $this->_common->test_input($_REQUEST['verify_code']) != '') {
			$unique_id 			= $this->_common->test_input($_REQUEST['unique_id']);
			$verify_code 		= $this->_common->test_input($_REQUEST['verify_code']);
			$selectApplicant  	= "SELECT * FROM `".DB_PREFIX."user_accounts` WHERE `unique_id`  = '$unique_id' AND `verify_code` = '$verify_code'";
			$resultApplicant 	= $this->_db->my_query($selectApplicant);
			if ($this->_db->my_num_rows($resultApplicant) > 0)
			{
				// start - added by jhtang 20150915
        		$device_type 	= $this->_common->test_input($_REQUEST['device_type']); //optional
				$device_id 		= $this->_common->test_input($_REQUEST['device_id']); //optional
				$row = $this->_db->my_fetch_object($resultApplicant);
				$user_accounts_id = $row->user_accounts_id;
				$secret_token = $row->secret_token;
				$selectApplicant  	= "SELECT * FROM `".DB_PREFIX."applicants` WHERE `user_account_id`  = '$user_accounts_id'";
				$resultApplicant 	= $this->_db->my_query($selectApplicant);
				$row = $this->_db->my_fetch_object($resultApplicant);

					$profileDetail 	= $this->_common->get_profile($user_accounts_id);
					$jobHistory 			= $this->_common->get_job_history($user_accounts_id);
					$result['unique_id'] 	= $unique_id;
					$result['secret_token'] = $secret_token;
					$result['first_name'] 	= $profileDetail->first_name;
					$result['last_name'] 	= $profileDetail->last_name;
					$result['display_name'] = $profileDetail->display_name;
					$result['date_of_birth']= $profileDetail->date_of_birth;
					$result['email'] 		= $profileDetail->email_id;
					$result['gender'] 		= $profileDetail->gender;
					$result['school_attend']= $profileDetail->school_attend;
					$result['portrait'] 	= $this->_common->getFullImgByteCode($profileDetail->portrait);
					$result['region_number']= $profileDetail->region_number;
					$result['contact_number'] 	= $profileDetail->contact_no;
					$result['is_NEA_certified'] = $profileDetail->is_NEA_certified;
					$result['referral_code'] 	= $profileDetail->referral_code;
					$result['my_referral_code'] = $profileDetail->my_referral_code;
					$result['employment_history'] = $jobHistory;

				$email = $row->email_id;
				$first_name = $row->first_name;
                sendActivationMailAction('Jobs On Demands', $email ,'newUser','en','newUser',$first_name);
                // end - added by jhtang 20150915
				$updateApplicant  	= "UPDATE `".DB_PREFIX."user_accounts` SET is_user_verified='1'	WHERE `unique_id`  = '$unique_id'";
				$this->_db->my_query($updateApplicant);

				// start - added by jhtang 20150915
				$updateArray = array("last_login_at" => $current_date);
				$whereClause = array("user_accounts_id" => $user_accounts_id);
				$update = $this->_db->UpdateAll(DB_PREFIX."user_accounts", $updateArray, $whereClause, "");

				if( $this->_common->test_input($_REQUEST['device_type']) != "" && $this->_common->test_input($_REQUEST['device_id']) != "") {
					$this->update_applicants_device_details($device_id,$device_type,$user_accounts_id);
				}
                // end - added by jhtang 20150915

				$result_array['result_code'] = 0;
				$result_array['success_msg'] = $this->_common->langText($locale,'txt.verify_user.success');
				// start - added by jhtang 20150915
				$result['secret_token'] = $secret_token;
				$result_array['result'] = $result;
				// end - added by jhtang 20150915
			} else {
				$result_array['result_code'] = 1;
				$result_array['error_msg']   = $this->_common->langText($locale,'txt.verify_user.error.notexist');
			}
		}else{
			$result_array['result_code'] = 1;
			$result_array['error_msg']   = $this->_common->langText($locale,'txt.verify_user.error.required');
		}
		return $result_array;
	}

	//sms varification  - rsolanki
	public function resend_verify_code() {
		$locale 	   = $this->_common->locale();
        $result_array  = array();
        $current_date  = CURRENT_TIME;
		$cdateTime 	   = date('Y-m-d H:i:s');

		if($this->_common->test_input($_REQUEST['unique_id']) != '') {
			$unique_id 			= $this->_common->test_input($_REQUEST['unique_id']);
			$selectApplicant  	= "SELECT * FROM `".DB_PREFIX."user_accounts` WHERE `unique_id`  = '$unique_id'";
			$resultApplicant 	= $this->_db->my_query($selectApplicant);
			if ($this->_db->my_num_rows($resultApplicant) > 0) {
				$row = $this->_db->my_fetch_object($resultApplicant);
				$user_accounts_id = $row->user_accounts_id;
				if($row->resend_verify_code_first=="0000-00-00 00:00:00" && $row->resend_verify_total==0){
					//get applicant contact number
					$contact_no = $this->get_contact_no_by_userAcId($user_accounts_id);
					$this->sendJodSMS($contact_no,$row->verify_code);
					//update date and time and counter
					$resend_verify_total = $row->resend_verify_total+1;
					$updateArray = array("resend_verify_code_first" => $cdateTime,"resend_verify_total" => $resend_verify_total);
					$whereClause = array("user_accounts_id" => $user_accounts_id);
					$update 	 = $this->_db->UpdateAll(DB_PREFIX."user_accounts", $updateArray, $whereClause, "");

					$result_array['result_code'] = 0;
					$result_array['success_msg'] = $this->_common->langText($locale,'txt.resend_verify_user.success');
				} else if($row->resend_verify_code_first != "0000-00-00 00:00:00"){
					//Get time diff
					$lastTimeSendReminer = $this->getMinutsTwoDates($cdateTime,$row->resend_verify_code_first);
					if($row->resend_verify_total>=5){
						if($lastTimeSendReminer < 59){
							$result_array['result_code'] = 1;
							$result_array['error_msg']   = $this->_common->langText($locale,'txt.resend_verify_time.error');
						}else{
							$contact_no = $this->get_contact_no_by_userAcId($user_accounts_id);
							$this->sendJodSMS($contact_no,$row->verify_code);
							//update date and time and counter
							//$resend_verify_total = 0;
							//$updateArray = array("resend_verify_code_first" => $cdateTime,"resend_verify_total" => $resend_verify_total);
							$resend_verify_total = $row->resend_verify_total+1;
							$updateArray = array("resend_verify_code_first" => $cdateTime,"resend_verify_total" => $resend_verify_total, "last_verify_sms_sent_at" => $cdateTime);
							$whereClause = array("user_accounts_id" => $user_accounts_id);
							$update 	 = $this->_db->UpdateAll(DB_PREFIX."user_accounts", $updateArray, $whereClause, "");
							$result_array['result_code'] = 0;
							$result_array['success_msg'] = $this->_common->langText($locale,'txt.resend_verify_user.success');
						}
					} else {
						$lastTimeVerifyReminder = $this->getMinutsTwoDates($cdateTime,$row->last_verify_sms_sent_at);
						$remaningTime = 5-round($lastTimeVerifyReminder);
						$remaningTime = ceil(5-$lastTimeVerifyReminder);

						if($lastTimeVerifyReminder > 5 && $row->resend_verify_total<5) {
							$contact_no = $this->get_contact_no_by_userAcId($user_accounts_id);
							$this->sendJodSMS($contact_no,$row->verify_code);
							//update date and time and counter
							$resend_verify_total = $row->resend_verify_total+1;
							$updateArray = array("resend_verify_total" => $resend_verify_total, "last_verify_sms_sent_at" => $cdateTime);
							$whereClause = array("user_accounts_id" => $user_accounts_id);
							$update 	 = $this->_db->UpdateAll(DB_PREFIX."user_accounts", $updateArray, $whereClause, "");
							$result_array['result_code'] = 0;
							$result_array['success_msg'] = $this->_common->langText($locale,'txt.resend_verify_user.success');
						} else
						{
							$result_array['result_code'] = 1;
							$result_array['error_msg']   = $this->_common->langText($locale,'txt.resend_verify_time_5minute1.error').$remaningTime.$this->_common->langText($locale,'txt.resend_verify_time_5minute2.error');
						}
					}
				}
			} else {
				$result_array['result_code'] = 1;
				$result_array['error_msg']   = $this->_common->langText($locale,'txt.resend_verify_user.error.notexist');
			}
		}else{
			$result_array['result_code'] = 1;
			$result_array['error_msg']   = $this->_common->langText($locale,'txt.resend_verify_user.error.required');
		}
		return $result_array;
	}

	//QR start job  - rsolanki
	public function start_job() {
		$locale 	   = $this->_common->locale();
        $result_array  = array();
        $current_date  = CURRENT_TIME;

		if($this->_common->test_input($_REQUEST['secret_token']) != '' && $this->_common->test_input($_REQUEST['job_id']) != '' && $this->_common->test_input($_REQUEST['clock_in_dt']) != '' &&  $this->_common->test_input($_REQUEST['applicant_id'])) {
			$applicant_id = $_REQUEST['applicant_id'];
				if($this->_common->check_session($_REQUEST['secret_token']) ==$applicant_id ){
					$secret_token	= $this->_common->test_input($_REQUEST['secret_token']);
					$job_id 		= $this->_common->test_input($_REQUEST['job_id']);
					$clock_in_dt	= $this->_common->test_input($_REQUEST['clock_in_dt']);
					$applicant_id 	= $this->_common->test_input($_REQUEST['applicant_id']);
					$selectApplicantId = "SELECT ua.user_accounts_id
										FROM `".DB_PREFIX."user_accounts` as ua
										LEFT JOIN `".DB_PREFIX."applicants` as ap ON ua.user_accounts_id=ap.user_account_id
										WHERE ua.`secret_token` = '$secret_token'";
					$selectApplicantId  = $this->_db->my_query($selectApplicantId);
					$row 			 	= $this->_db->my_fetch_object($selectApplicantId);
					$user_accounts_id	= $row->user_accounts_id;
					if($user_accounts_id){
						//Get start date info
						$selectApplicant  	= "SELECT * FROM `".DB_PREFIX."clockIn_clockOut`
												WHERE `job_id`  = '$job_id'
												AND `applicant_user_account_id` = '$user_accounts_id'";
						$resultApplicant 	= $this->_db->my_query($selectApplicant);
						if ($this->_db->my_num_rows($resultApplicant) > 0) {
							$cloclInOutrow = $this->_db->my_fetch_object($resultApplicant);

							//check clock in date time first
							//echo $cloclInOutrow->clockIn_current_dateTime." - ".$clock_in_dt;
							if($cloclInOutrow->clockIn_current_dateTime != $clock_in_dt){
								$result_array['result_code'] = 1;
								$result_array['error_msg'] = $this->_common->langText($locale,'txt.clockinDateNotMatch.error');
							}else{
								//Update job statrt date and time
								if($cloclInOutrow->applicant_clockIn_status==0){
									$updateArrayClockIn = array("applicant_clockIn_dateTime" => $clock_in_dt, "applicant_clockIn_status" => '1');
									$whereClauseClockIn = array("id" => $cloclInOutrow->id);
									$this->_db->UpdateAll(DB_PREFIX."clockIn_clockOut", $updateArrayClockIn, $whereClauseClockIn, "");


									//Also update job_applicants
									$updateApplicant  	= "UPDATE `".DB_PREFIX."job_applicants`
															SET clockInOutStatus='1'
															WHERE `job_id`  = '$job_id'
															AND `applicant_user_account_id` = '$user_accounts_id'";
									$this->_db->my_query($updateApplicant);
									//Success msg
									$result_array['result_code'] = 0;
									$result_array['success_msg'] = $this->_common->langText($locale,'txt.start_job.success');
								}else{
									//already clock in
									$result_array['result_code'] = 1;
									$result_array['error_msg'] = $this->_common->langText($locale,'txt.start_job_already.error');
								}
							}
					} else {
						$result_array['result_code'] = 1;
						$result_array['error_msg']   = $this->_common->langText($locale,'txt.start_job.error.notexist');
					}
				}else{
						$result_array['result_code'] = 1;
						$result_array['error_msg']   = $this->_common->langText($locale,'txt.applicant.error.notexist');
				}
			}else{
				$result_array['result_code'] = 1;
				$result_array['error_msg']   = $this->_common->langText($locale,'txt.complete_job.qr_code_belongs.error.required');

			}
		}else{
			$result_array['result_code'] = 1;
			$result_array['error_msg']   = $this->_common->langText($locale,'txt.start_job.error.required');
		}
		return $result_array;
	}


	//Get Time beween two dates in minuts -  $date1 will big date
	public function getMinutsTwoDates($date1,$date2){
		if($date1 && $date2){
			$to_time 	= strtotime($date1);  //Big date always
			$from_time 	= strtotime($date2);
			return round(abs($to_time - $from_time) / 60,2);
		}
	}

	function get_contact_no_by_userAcId($user_account_id){
		$userAcData = "SELECT region_number, contact_no FROM `".DB_PREFIX."applicants` WHERE `user_account_id` = '$user_account_id'";
		$userAcData = $this->_db->my_query($userAcData);
		$row 	= $this->_db->my_fetch_object($userAcData);
		return $row->region_number.$row->contact_no;
	}


	public function login_user() {
		$locale = $this->_common->locale();
        $result_array  	= array();
        $current_date 	= CURRENT_TIME;
        $device_type 	= $this->_common->test_input($_REQUEST['device_type']); //optional
		$device_id 		= $this->_common->test_input($_REQUEST['device_id']); //optional
        if($this->_common->test_input($_REQUEST['unique_id']) != "" && $this->_common->test_input($_REQUEST['password']) != "") {
			$unique_id 	= $this->_common->test_input($_REQUEST['unique_id']);
			$password 	= $this->_common->test_input($_REQUEST['password']);

			// start - added by jhtang 20151103
            $paas_no_md5 = $password;
			$paas = md5($password);
//			$selectApplicant = "SELECT * FROM `".DB_PREFIX."user_accounts` WHERE `unique_id` = '$unique_id' AND `password` = '$paas' AND `is_delete` = '0' ";
            $selectApplicant = "SELECT * FROM `".DB_PREFIX."user_accounts` WHERE `unique_id` = '$unique_id' AND (`password` = '$paas' OR `password` = '$paas_no_md5') AND `is_delete` = '0' ";
            // end - added by jhtang 20151103

			$resultApplicant = $this->_db->my_query($selectApplicant);
			$result = array();
			if ($this->_db->my_num_rows($resultApplicant) > 0) {
				$row 			= $this->_db->my_fetch_object($resultApplicant);
				$is_user_verified	= $row->is_user_verified;
				if($is_user_verified == 1)
				{
					$secret_token 	= uniqid();
					$id 			= $row->user_accounts_id;
					$profileDetail 	= $this->_common->get_profile($id);
					$jobHistory 			= $this->_common->get_job_history($id);
					$result['unique_id'] 	= $unique_id;
					$result['secret_token'] = $secret_token;
					$result['first_name'] 	= $profileDetail->first_name;
					$result['last_name'] 	= $profileDetail->last_name;
					$result['display_name'] = $profileDetail->display_name;
					$result['date_of_birth']= $profileDetail->date_of_birth;
					$result['email'] 		= $profileDetail->email_id;
					$result['gender'] 		= $profileDetail->gender;
					$result['school_attend']= $profileDetail->school_attend;
					$result['portrait'] 	= $this->_common->getFullImgByteCode($profileDetail->portrait);
					$result['region_number']= $profileDetail->region_number;
					$result['contact_number'] 	= $profileDetail->contact_no;
					$result['is_NEA_certified'] = $profileDetail->is_NEA_certified;
					$result['referral_code'] 	= $profileDetail->referral_code;
					$result['my_referral_code'] = $profileDetail->my_referral_code;
					$result['employment_history'] = $jobHistory;

					$updateArray = array("secret_token" => $secret_token,"last_login_at" => $current_date);
					$whereClause = array("user_accounts_id" => $id);
					$update = $this->_db->UpdateAll(DB_PREFIX."user_accounts", $updateArray, $whereClause, "");

					if( $this->_common->test_input($_REQUEST['device_type']) != "" && $this->_common->test_input($_REQUEST['device_id']) != "") {
						$this->update_applicants_device_details($device_id,$device_type,$id);
					}
					$result_array['result_code'] = 0;
					$result_array['success_msg'] = $this->_common->langText($locale,'txt.login.success');
					$result_array['result'] = $result;
				} else
				{
					$result_array['result_code'] = 2;
					$result_array['error_msg'] = $this->_common->langText($locale,'txt.login.error.usernotverfied');
				}

			} else {
				$result_array['result_code'] = 1;
				$result_array['error_msg'] = $this->_common->langText($locale,'txt.login.error.notexist');
			}
		} else {
			$result_array['result_code'] = 1;
			$result_array['error_msg'] = $this->_common->langText($locale,'txt.login.error.required');
		}
		return $result_array;
	}

	public function change_user() {
		$locale = $this->_common->locale();
        $result_array  	= array();
        $current_date 	= CURRENT_TIME;

		if($this->_common->test_input($_REQUEST['secret_token']) != '' && $this->_common->check_session($_REQUEST['secret_token'])) {
			$secretToken 	= $this->_common->test_input($_REQUEST['secret_token']);
			$first_name 	= $this->_common->test_input($_REQUEST['first_name']);
			$last_name 		= $this->_common->test_input($_REQUEST['last_name']);
			$display_name 	= $this->_common->test_input($_REQUEST['display_name']);
			$date_of_birth 	= $this->_common->test_input($_REQUEST['date_of_birth']);
			$email 			= $this->_common->test_input($_REQUEST['email']);
			$school_attend  = $this->_common->test_input($_REQUEST['school_attend']);
			$portrait 		= $this->_common->test_input($_REQUEST['portrait']);
			//$portrait = 'iVBORw0KGgoAAAANSUhEUgAAA9QAAAIwCAMAAABQqNOWAAAAvVBMVEUAAAACAgIKCgoLCwsNDQ0PDgAODg4QDwAPDw8YGBgfHAAeHh4gHQAgICAvKgAvLy8wKwAwMDA8PDw/OAA/Pz9AOQA/Pz9PRgBPT09QRwBQUFBfVABeXl5gVQBfX19vYgBvb29wYwBwcHB/cQB+fn+AcQCAgICPgACPj4+QgQCQkJCfjgCfn5+gjwCgoKCvnACvr6+wnQCvr7C/qgC/v7/AqwC/wMDPuADOzs/fxgDe39/v1ADv7+//4wD///9stINcAAAmXElEQVR42u2d61oTSbRAOZcBjojCERFH1KiIqIxkRkYg5PD+j3UAuXSSXemurr1Tuypr/Zvvk56ku1eqal+qVv4PAKpiJfcHAABdkBqgMpAaoDKQGqAykBqgMpAaoDKQGqAykBqgMpAaoDKQGqAykBqgMpAaoDKQGqAykBqgMpAaoDKQGqAykBqgMpAaoDKQGqAykBqgMpAaoDKQGqAykBqgMpAaoDKQGqAykBqgMpAaoDKQGqAykBqgMpAaoDKQGqAykBqgMpAaoDKQGqAykBqgMpAaoDKQGqAykBqgMpAaoDKQGqAykBqgMpAaoDKQGqAykBqgMpAaoDKQGqAykBqgMpC6Ri7+vebvz/d8vf6v3B8JFgdSV8TZv18/v335ckVm7eXrz3/9e5n7Q4I5SF0FZ39/fr210omnrz//jdlVg9Sl8+/Xrjo32PqA2PWC1AVz8feHl9E+P4r99SL3FwATkLpQLv56/bS/0A8DNl5XCFIXyMVfb9OFvvf6L+bhtYHUpfHvh/gl9FzW3p7l/k6gClKXxOVfr9d0jf7Ny5+5vxkogtTFcL2KthD6fhae++uBGkhdBhdflSfds1ozWtcCUheAvdG3vGRtXQdI7Z1Ly1n3FG/JcNUAUvvm78UZfcPa19xfGNJBasecfTCJdc9lizl48SC1Vy4Xs5Ce5QPVKIWD1D75+TaP0Tc8ZbAuG6R2yOVXtSrQfnzOfQcgBaR2R85B+p6XhMELBqmd8ddWbqFvWTvJfSOgN0jtiYvPiw93h/iQ+2ZAX5DaD5rz7tWdBv0u8ZIoeKEgtRdU5t0bO4OD4en51Qynw4P9WLmJghcKUrsgPd69ujM4Pr1q4fx48Dzimmvfc98X6ANSO+AisXLs2udRm88PjI73VztfmarREkHq7FwkLaV3DlrH51mGnb1+m/vuQDxInZkUpZ8Pegh9x3HHFTbhsvJA6qz87L3F7+re8bi30b/n4d2Gazo8igOpM9Jb6Y2EIbrB+KCL1mtYXRhInY2Lnq3SG4PzdJ9jtCYIXhhInYnLD/2MPuge5+6odYf/K1YXBVLn4WufJJbmGP3IaK/9//w29/2CCJA6Byc9Sk1W91XW0RKnG+1WEwQvB6RePJc9FtPJse7UOfgWVhcDUi+c+Jm3+kJ6lvPW8tEtWqxLAakXzFl0Gstu2j0xWA/aPgeprVJA6sXyOXaQPrKcdk8wbMturXGGRxkg9SK5iBym9xYySN8zap2Ck9oqAqReICdRq+lV+5X0FOPW5BZWlwBSL4zLqNaNneMFG31LaxScfUYLAKkXxdlWhNKLCY4JtC6s3+a+j9AKUi+IiKn34ufdDc6xuniQejF0j3pvZJl3PzJuC5e9pgzFOUi9CLrXkO0M8yp91SFcRnGZc5B6AVxudVQ621J6kn2sLhqktqdriGw/41J6kmOsLhmkNuesW4jMj9I3VreEy9jjyDNIbU0np1cPFlYN2o22IDiF4I5BamM6OT1wpvSN1RtYXSpIbUsXp11NvB9oS21htVuQ2pQOTu+4VPrG6h2sLhOktqTd6Q0fSSyZfawuEqQ25KLN6dWD3N5idYUgtR2tNSd7XmfeDxxjdYEgtR0tOyKs5q8ITbf6JPdNhlmQ2oyW9uk9f2ks0eqWhDXbJvgDqa34Xv4wfUtbGQpWuwOpjTiba8JOGcM0VhcJUttwOfcMDudBb6wuG6S2Yd6CetVzblqibZtRrPYFUpswb0H93OKUO1vaSkax2hVIbcG8qpPnBS2nsbpIkNqCObsX7ef2E6urB6kNOKnOaawuCaTWZ07kO8rp8a/Dw93dzZu/29zdPfz2K6/V+1hdCEitz3sNp/95tz3797uHOYNsWF0ISK3ORbrT//wZvMbmx3xdIFhdBkitzpvQS/+8mzqjj+vz5dn94dTqD7lvPdyC1NoE60O75bJG71ba2cyldYvVb3PffLgBqbUJNVyudlkOd1L6VutMDSEtVr9kQ3AHILUyP0Pve4cjssYfOyp9w26emFmL1U/ZNiE/SK1MaEU9aPfldDPC6WsOPVq98jX3EwCk1iUU+t5oXVCP/1yJZTvLYN1mNVPw3CC1LqGBurUx6zxymL5lPcuxt21Wr/3M/RSWHKRW5TLQydGaof7WQ+kb3tl4e1PM9vHPw8N/xKR4m9XktvKC1KoEWi5X2wpGuga9Z9nVb/oaf9t9vP7mR2GS0Wo1B+jlBKlVCeSzWnY6Ge+2STKHbWWrx4fTtS+7s1q3Wr3yOfejWGKQWpNAmGx1vnfj7VZF5rGpGi77JpWzzU4HDlo/1suL3E9jaUFqTT71Gahbnd65Zt4ZlOt6Vo8D64D1mcG67Vz6lZU1ButMILUmW/LrPXegnuf06v7Rg03j04M9a6vnfJaZOHu71aysM4HUigRm3/v9PFrdn13LDuXF7KbOunrunKGP1StvyVlnAKkV+SK/2XPH0ZBHG0eyqOMjaSauEy2bH6+b+RpdrGYOngGkVuSVrOc8jwJr2LnHYR4LWv+p4HRL8Gt95oej7UieW55+ZrReMEitiPxWH83x6Ej+k0HLwHswa1P6+QCjljZu4X/Rts3//WiN1gsFqfUINGjNKTwZin/QYWPw8WzMLLkXsz33PPtNulm9svL679wPZ5lAaj3khNac/U7G4tjYoZ/r5vdgWqf1xGX1qF1NIeLXtsno43D99i8S1wsCqfWQl9RzHJX6sjofhzlzFk7isrpD2Gtd+mHaa/+7e56+/vpv7oe0DCC1HnIzR1jSofCvY87v2O/8f+pCFznFXrP24rIJtj4wFTcGqdW4lF/ioEXS5Hs/ag49mBpIkybgXYSUo3GnHRfWD6y9ZipuCVKrIcfJwktqYYSLPb/jOO3Pm3RYUgf/BzFT8Hte/kVI3AqkVkMuPdmPsCheyimrE8pFT7uYuBP666Mufz3NW+bhNiC1GvLBHMEs9WwGqc9AO2n1rrHU28E/P+8aBZ/gKcO1BUitxgvxvQ3tYzQ7UAeHwQir+x9nnzZSX0/BB32sXln7zOpaHaRWQ5Y6FLyaGaj7nls9YXX/oTpVaiFz3pEPjNbKILUackYrNLDNhL57r4cnhsj+Q3UX/+bXxfSJl91A04cySK1G1Ng2U+qRULu907hM/wB4l3H2qOUafQfrpye5H15VILUWcpo6JPV2x3/XhYlSzd656vbS77ll7HcfpedgTeO1JkithZymDoycM2Gy/tPma84bF+q9E/iw3bwux3aebrRfR4JdUvRAai1kqQOz6unZd0rZyNVEHcte74u0T53bZt+3jCPLRu9ZYwquBVJrESX19Cw19SD5xgS89zVaOzo2Ol5o1HMOzqH1SiC1FlFST8W+EwfqiYRU9ER+/G333eEN/9MiXfeGkdOdlT5gtQ5IrYUstSzCdFI4fTfQxzBXdBi9S4TshqhY3rDX0hqrVUBqLU7E11QeN6dmul3ntXN4jLx122OhQUfhYotj+ozWa0TLNEBqLeR9T2Spp2JJ0R4KPCgUHSnrlltejc+VnXadAzzylMyWAkithdykJUs9NYhp7MX/0CcVPf3ustNvl33TBEYHsbPw17kfYw0gtRbymlqWeipOpuD01fh+wI0PpI+ODvZ2du4C6P8hfo24zRuanA/irCaxlQ5SayFLLdeCTP6blGqyR47n/Q8jfhyEOfNG0kZJcWtrJuDpILUWMSmtLv8mmpvI1PPkjYKvB+4prTcSfycii1E+5X6Q5YPUWmSXWo/xcP9+LbxzlLzgP1+J4mnuB1k+SK1Ff6mT6r7tOD1NrXO7IzJYRrI6FaTWIkLqqXYOp1KrEZnZepP7SRYPUmtxJr6hcgp6uaQeMv9eLEithviGypHt5ZK6Y3nLA7kfZPFwB9VA6hCDOKl/5n6SpYPUajyRXtB18S2fHLqqlzoy/o3UiSC1GvJuouJbPlmPUb3UkfFvpE4EqdWI2CJ4Mh7sLk+tTqfy8ge+5H6SpYPUakS0aU0WWSVvkeCecVSojJqyRJBaDVlqscZycpOE7VhHyiMqVY3UiSC1GnLvpTi3HneYoldFpzM176FRKxGkVkMuKZP3LJg8Tk6hC8M7MXsREihLBKnVuBDfUDlRPZj4N/Uvqrsd1YXUOiC1HvIrKr7jw4l/splbuQUQ0VWN1IkgtR7PxFdUXjBPhoM19jNyTsRQjdSJILUeMQdUT4aDl2D+HTFUs/dJIkith5zTkktLJssx1uuPf0cM1bmfY/FwB/WQc1qBUXhy/p26s1gJdB2qn+R+jsWD1HrIOa3AtoKTRWXLECrrOlS/yP0ciwep9ZBPqA5sADxVjsFQ/cCr3M+xeJBakT/ElzQQ2p4MlS3DqrpjWRlVoqkgtSJy+HvY6RXXOHrHO90qwMlopYLUisSEv2decaWtOz3TrVnrIvdjLB6kVuS7+JKGDuAYTb7iu7mVWwBd9vX/I/dTLB+kVkTeUHS94yt+lFu5BdBhCxSC38kgtSbyazrq+IpTLHoDcbJkkFoTufp72PEV31yCCHh7CyZxsmSQWpM34msa3oNsMPkP3+VWzp5RW6yMJXU6SK2JXCgaPqp2/HzprD5qkZrSk3SQWhO5UHQ9/IqfTw1cS1BY1lJXxvF46SC1KvKLOicFPb15bv1Wt0zAyVKng9SqREbKrmaPpKnf6rkT8Ge5n2ANILUqcqRsbgno/tJZPW8Czkb+CiC1KrGRshv2ls3q0X+HpWbXEwWQWhU5UrYy9x2fDoFXHwM//c+g0xw4rwFS6yK/q/NrxcbT89FtT80do2+Hh6pH+H2cM/smTKYBUusiR8raZtTT6+p1P9v7/1ZwV62EdbQ9x2nqvlVAal16RMokq90srN/df6CPOjWsx+tznKZEVAek1qVPpOz2ZZ8eq33UgTeq0zd/pF9u/G5lHlST6YDUusjdlyvt7/v5VMuWj6F60PxIu6lL6/PNuU6zolYCqZWRX9cOS9LxZGrLx0n0UyG8tKX14XylabrUAqmVkfcp6zTuDjdi/2LRUq+svOsdmJ8bIbvhCTlqJZBamffiC9ttW8Hx414oqz7W1EJJZ0+tv82NkN1AlEwLpFYmbp+ymeHsPgzuY6CWtwp8F7+2Hu22Kc3kWw+kVqZ3pKyp9YabPPWx+G12/4m7ymHrME2KWhGk1kZ+ZyPmrONT1QKuRE7lvQI3f3RfH5y3raZXWFCrgtTaxBxoWwLjwL6+6x+7/VCN55WF3vPHWe7HVhNIrU3sPmX+GYV6JXd/tP/xj/aZN04rg9TayDVle7nNTOI0tF/3+sf5mevTDjNvnNYGqbWRuy+3c3uZyHFwG/7Nb8Fp+Gh+Veg9z3BaF6RWR35zc1uZzEF4b7Ft0etxN6VXXhEjUwap1XkivrrlH78xnqP1yvb0PHzcIY11CxsYqYPU6kQdaFsUc7Ve2Xz342HAHnVVmqm3AUitTtyBtmUxPp5/xN1vsTuupa95w9TbAKRW50R8fcsOfzc4btmNf+W//qer0n+c5H5WdYLU6siFol2rvwvgdL+rtfN5Rv+0DUitj/wK51ZRk9FB2zl3HaCDwwqk1kcOf/vopVSjdRbewhM6Lc1Aan1qq/4OMBpsRIrcgAiZIUitj7xPgpMOaVWG+/2m4UTITEFqfeTq7zpyWjMc/2+809SQ2YLU+sjV39XktBr8Omzf0YRheuEgtT4X4rtcekvHtM/f3nXqwGKYXjxIbYD8NufWUInRr2+Hu/18vuYJw7Q9SG2AfKBWyTmt8a9fvw4PD3f72/yb9wzTCwCpDSg+p3Xj8I9riT/uXpOmcZMXdG8sBKQ2QG7pcJ7Tuhb5281YvNuxvyqaJ99zP5dlAakNKCunNf51PSInTqs78ImZ96JAagPknNZ+bnsFfh2aDcyTvKF5Y3EgtQFl9GmNvv25EJ+veYHSiwSpLRDf7M3cFjcZ/1AMgLUpTe/GYkFqC/4QX+7cIj8yereYSTdKZwGpLZBzWr1PgVVXemFGo3QOkNoC+ZQOJ4nqrnsCKvCGxHQOkNoCx4nqLqfV6fDHJ8JjeUBqC+RDqj0kqo8XNUw/o9QkG0htgZyodiD14YIGaebdOUFqC7wmqgcLUfoVg3RekNoEn1IfL8DoZ18oB80NUpsgvu/rmZ0+N19Pv/hCbMwBSG2CnKjO6/TY2OlX3xmjfYDUJnisPlE6V0Meoj9RZOIHpDZB3iU4a/XJqZXQz96fMES7AqlNkKtPskpt0cDx7M0XRmh/ILUJ/rZJUI58//HizaefDNA+QWoT/FWfbCrZ/OTa5hOGZ9cgtQnupE5YUT95cc2nT5++//xJwqoIkNoEeT//jNUnezEz61uHP/28Jvd9hD4gtQ3OpB61u3xj8slParYrAKltEL3Jd/LOYK7O778Q86oJpLZBPqQjm9TBMNmT94zN1YHUNviqEz0PLZ+/5L5PYABS2yBLnes4rYOA04zSVYLUNvgqKQtsYcQJlHWC1Da4kjoQ+36R+yaBDUhtgyupAyWiZKErBaltOBE1ylRSJjddPsl9j8AIpLbBVZ2ovKR+n/segRFIbYMrqQmTLRdIbcOZI6kDzRy5bxFYwaM1QvQoT/H3EbHv5QKpjXAktdyh9Sn3HQIrkNoIR1LLhd8sqasFqY3wI/VYXlKz4UG1ILURYvF3lu385TgZWep6QWoj/LRpyd0cr3LfIDADqY3wI7VcT0acrF6Q2gg/Uu+In4TC73pBaiPeuJFaPkKL/YvqBamNkNu0cpymRZxs2UBqI9z0XspbGVFPVjFIbYQbqeWMFi1aFYPURriRWs5oEfyuGKQ2wrnUFIlWDFIb8d2L1GS0lg6kNuKnb6lz3x4whKdrhCz10eKlXkfqZYOna4Sb/YzEz0FGq2aQ2gikhlwgtRFIDblAaiO8SC3XnpCmrhmkNgKpIRdIbQRSQy6Q2ohLJ1LL52h9z317wBCktsKJ1HKVKAVlNYPUViA1ZAKprRBl2kdqMAeprRBlWvzG30i9fCC1FUgNmUBqK5AaMoHUViA1ZAKprXAtNQdp1QxSW+Fa6tw3Byzh8VrhWmpG6ppBaitcS82aumaQ2gqkhkwgtRVIDZlAaiuQGjKB1FYgNWQCqa1AasgEUluB1JAJpLYCqSETSG0FUkMmkNoKJ1IPxc/BHmU1g9RWOJGa3USXD6S2AqkhE0htBVJDJpDaCqSGTCC1FU6kPhc/x6vcdwcMQWornEjNqZfLB1JbgdSQCaS2AqkhE0hthSgTh86DPUhthRepn4sfJPfdAUN4ulZ4kXoHqZcNnq4VSA2Z4Oka8ROpIRM8XSPcSL0nfpCz3PcH7EBqI9xITUP10oHURiA15AKpjXAuNbskVAxSG+FG6qH4QWjTqhikNsKN1PReLh1IbYRzqd/kvj9gB1IbIUt9tHipKf5eOpDaCFnqUy9SP819f8AOpDbiixupN8RPkvv+gB08XCM+uZFarhO9yH2DwAykNsKP1HKdKNUn9YLURviRmuqTZQOpjfAj9ZH4SUhU1wtSG+FHajlRTU6rXpDaCD9Sj8RPQk6rXpDaCFnqDE4HEtU8+Hrh2Rrxyo/Uck6L8He1ILURL/xILee0CH9XC1Ib4UhqOaf1PvcdAiuQ2ghHUsvh763cdwisQGojHEkth7958tXCozXCkdRXq0TKlgqkNkKU+nkeqeXwNzVltYLURqxJHmU49PIGOVJGTVmtILURK46kHoofZi33LQIjkNoIT1IHImWc0lEpSG2EJ6kDm5+wqK4UpDbCldT74qchU10pSG2EqNF+JqmP5fn3Ze6bBCYgtQ1nokUZtv2+JbCopvy7TpDaBjd7+f9GXlSzo3+dILUNzqSWF9UkteoEqW1wJnVgUX2S+zaBBUhtgzOpx7LUzL+rBKltOBElGuaS+uo58+/lAalt8LPv4G/k8m/m31WC1DZ4k/pclvpV7vsEBiC1Dd6kDiS1qD+pEaS2wZ3UclKL+pMaQWobZKnP80k9ZP69NCC1DZ52M/qNvKcRR9pWCFLb4E/qwPybnYLrA6lt8Cd1YP5Nqro+kNoGf1KH5t+EyqoDqW0QpV7NKvVAlpqtEqoDqW0Q/cm18clvAvUnbFVWHUhtg0OpQ/UndHXUBlLb4FHqo8BQTVVZZSC1DR6lDmxqxK6itYHUJlyK9uTad/Ae+aDqlae57xbogtQmONsj4Y5hYKgmq1UXSG2CT6lDoTKG6rpAahOcSh3YKoFTbesCqU1wKnVgq7KVl7nvF2iC1CZ8Ed3Jt0XZPYGuDobqqkBqE9ztkXBHqKqMtuqaQGoTvEp9tROwmrbqikBqE9xKPQxITa1oRSC1CW9Ec0a5lb4KZrUYqisCqU1w2E59R+AAHmpFKwKpTfArdWivhDXaOqoBqU1wLHWoAIWhuhqQ2oQtv1KPGaprB6lNELXJ3Hl5zyAwVNPWUQtIbYJnqUNt1bR11AJSm+BZ6mCtKEN1JSC1BZeupQ4N1ewrWglIbYHTJq17Ajug0NZRCUhtgXOpTwNS04FZB0htgXOpg20dbAFeBUhtgXephwGpaeuoAqS2wG2T1j20ddQMUlvgXupQWwdDdQ0gtQXupQ4O1dSKVgBSW/DevdShI3ho66gApLbAcZPWHbR1VAxSW+Bf6mAHJrWi5YPUFhQgdWiopq2jfJDagqf+paato16Q2gLRFi/9HHfQ1lEtSG1BCVJzWke1ILUFRUgdOq2Dto7SQWoDzkRZ9nJbPA2ndVQKUhvgvZ/jjlAHJrWihYPUBhQi9dVzhuoqQWoDSpGa0zrqBKkNkPs5jnM7PEugrYNa0bJBagP8N2ndEWrroAClaJDagGKkpla0SpDaAP+dl/fQ1lEjSG1AAf0cd4RqRSlAKRmkNqAcqakVrRGkNqAgqUNDNQUoBYPUBqxJmqzm9leGWtH6QGoDREu89XPcEaoVfZ/7JkJvkNqAkqQO1YpSgFIuSK2P7zMvpwnVin7JfRuhL0itj1z6Pchtb4hArSgFKMWC1PqU0s9xBwUotYHU+hQmdahWlAKUUkFqfb6IjhzlljfIIDBUc7BtoSC1PsX0c9xBAUplILU+pUl9tRewmqxWmSC1Pm9KkzpUgMIOKGWC1PrIpd/j3OrOIVSAkvtOQi+QWp+C+jnuCBWgkNUqEqTWpzypQwUoHMFTJEitj+jHRm5v5xIqQKGtukSQWh9RD6+l378JFaCQ1SoRpNanQKmDO6DQVl0gSK3OWYlShwpQyGoVCFKrU1jp9x2BHVDIahUIUqtTptTDwFBNVqs8kFqd70VKTVarHpBaHbn0e6gt4Y93u+80S09DBSj0ahUHUquzkH6O8+3bq/6pV3xKVqsakFqdRfRzjNfvLrurd81BYKimV6s0kFqdRfRzPKaV9ab1oawWOxCWBlKrs4DS74Z/+3pXDbRVswNhaSC1Ok/tpW7UfykWtYTaqk9y31GIA6nVEcV4rul0c6KsWakWyGqxA2FhILU6ohiqVaLNQm3N/QxDWS0KwMsCqbW5MJe6OVCvagbgQlktztUqC6TWxv58juZArVuoFujVogC8LJBaG/PSb7OBOpzVogC8KJBam0/WUtsN1MFeLUJlRYHU2lhXiT4Uk+kP1BSA1wFSa2NdJdrcTkz/JM1AVosC8JJAam3kgrKRlnXNgVrvquJPRjNURgF4QSC1NsZVok3rFEtEH34zCJWVD1Jrs2Yrte1AHcxqsVdCQSC1NqITarUnzUiWwUAdLgAnVFYOSK3Mpa3Um8YDNaGyCkBqZeTakz0l44bWA/XV1RGhstJBamVsC8p2zQfqYAE4obJiQGplTAvKrHouJyBUVjpIrYxpQdm+/iVnOSdUVjhIrcwLQ6mbhSequy5MEjiCngbMUkBqZSy3HWzms47tpA4UgNOAWQpIrYxl7cn24wVX7ZwmVFY6SK2MqIPOifPNxa7pKT6BUBkNmIWA1LpYbmbUdM0onzX769GEvcrKAKl1saw9Wde+YJBAqIzDqssAqXUxPPKyGb9SP24v/L9qwLb+ZYDUushpapV9fBsHaPReo//4c327w1mZoVAZ2/oXAVLrYrfvSbPTuefIP76rMm1PhwVCZXR1FAFS6yKnqc8VpG5OiXuGyd51nr2HGjDp6igBpNbF7iCtxuy7ZzD9Mai92fpvAw2YpKpLAKl1EVXQqBRpzr57VpM1dkJqnToEGjBJVZcAUqtil6Zuzr57Fp02pG5d5Ie29SdVXQBIrYpdmnov/XIxUocOqyZVXQBIrYpZmlph9h0n9VCWmlR1ASC1KmZp6qZjfVu+oqS+CqSqf+a+xdAKUqvyShRBIU3dSBz3nszHST2QpSZV7R+kVsUsTd3YRbT3uB8ndaCrg65q/yC1KrII6U43g9G9G7TipA51dZCqdg9SayJv+q3QTd1IG/ffxyhS6kCq+lXumwxtILUmckZLIU3dSDD1P+lyJ07q0LFalIp6B6k1kTNaCgfOrsf52Cp1p1V+IFXN/Ns7SK2J1abfzaBV/6vsRF5kyPy7TJBaE6uMVmN5mzCXX4/9ZQikqikVdQ5Sa2KV0WpkqROG/ceLdOwwGchSf8l9m2E+SK2JLEGy0829gROG/ejhPpCq5gAe5yC1ImYZrcbV+h8LcBotdairmvm3b5BaEauMVkPHhNN2GlfpGo9n/l0kSK3IF1GB9IxWo2ok4VDqRrSt68I80FXN/Ns3SK3Ie1GB9B6tgcrFDnpcJVAqygGYrkFqRaxOvIwsBQuw3+MqgVJRtkpwDVIrIu86mH5CTnSCue2noXOSjfl3iSC1IrIByU7HJ5hFNvt8pECpKPFvzyC1HgsIfqdcrNdPQ+AAHuLfnkFqPeR2jvRdB4ePF0uIpI96/TQEWrWYf3sGqfWwaufoE7aepTHex/zOMP8uD6TWQw5+px9QGbm5QYAeaeobmH+XB1LrIQe/09s59lSkbvw0xOwxHJh/03/pGKTWQ379k52O7oNuvUrUT0Ng/s3+J35BajXk4HdCsfY92ypSN64SlTkPzL/Z/8QvSK2GXPmtcOTO48VSGr76Th6YfxcHUqshV34rHLnzeLGENHUjoxU5eWD+XRpIrYZV8FtH6v4VLIH590nu+w0hkFqNNfHdVzidQ0XqRvA7cvIQmH9z/o5bkFoLedsTheC3jtSDx6vEnpopz785/9ItSK2FWfC7sRpOqBJNaN8MzL9pqvYKUmshF4km7FRyT2M1nBB1a/RoxW5zFph/U1TmFaTWQt7zWyH4rSN1yopA3v/kRe47DgGQWost8c1XOJpaReqk9s3A/icktZyC1FrIL37/HX11pW6si+NXBIH9T0hqOQWplZDjZCk7lahK3T+jdYO8/zdJLacgtRJykajCKbbNcbK/1Gl7Fw7EL7eW+56DDFIr8UZ87xXiZDp56kbwu8dGiKfilyOp5RSkVmJLfO0VikR1pG58pj5/Lp9/SVLLJ0ithDyWpW8PrCN16t6F++K3I6nlE6TWwS5O1hwmt/teohH87tULGigqy33XQYTnooNdnExl55O04HewqOxn7tsOEkitgxwnSz8bT0nqxiX6LfPlojKO33EJUuuwJb70sf1QMoPHC/YtUGsEv/v1gh6I349FtUuQWgd5eqrQTH2lskVw4zP1u0AgqZX7toMEj0UFOU6m0Ux9NRGl6pn3Vji4R05qsaj2CFKrYBknayrZs5MzqfL7N/JOCSyqPYLUKsh9lzpxsmbouefPRGNV3rfGTe7UYlHtEaRWQT6cQydONjH17XcBhVPrA51auW88CPBUNAjsT6YTJ+t3XPwEjVPre9e4saguBqTWwDRONlGk2Wvwb8zf+9e4yYtqyr8dgtQayPuTKcXJJtazvZbpKqfWy4tqeqodgtQamMbJrq7OH6/Zq/o79Udh+kM0YKNghyC1BvI+/ip9l7c0Ltpnf6TG9D3h1Hp5Uc1GZf5AagXO5CW1St/lLYml2wrB76vQopqNyvyB1Ap8F193nb7LWwaPV+1TPJI40N8hl39TfuIPpFZAbtFSOMT2nuHjVTfj/7qxGk45C1cu/+ZIW38gtQJb4uuusj/ZHY3LxmeqGz8JSQF58VsSKfMHUqcTKD1R2Mf/gcaqOD5+nbpDwj1yTzWRMncgdTonstSKTje9jJ9/NyJcSQF5eaMyasrcgdTpyKUnCuddPtJMEkfPv7dT5u4N5I3KqClzB1Kn80J82dVKT27ZSLiw1uxBjpRRU+YOpE5Hnn1rtWj9ZtB//t3or0qcPYjfk+5LdyB1MoFuDr3Skxua8+/IhbFK5fctYqSMw3fcgdTJyLueKJae3NKYf0cmwHXOt75Brikj/O0NpE5G7uZQLD25pTH/jpwEaGW0QjVlhL+9gdTJyN0cmqUnN4x6X7uhYuI6X46Ufc/9AGAKpE4l0M2hWXpyS2NBux5Vwa2ww/Adcvcl1d/eQOpU5CW1aunJLc0scVQDpZ7UcvibnJY3kDoVeUmttuvJA+NGP3NUVkun8fKWDemrktPyBlKnspgl9dVklWbM4lhR6h3pq9LS4Q2kTmRRS+rJUFnMUK0o9UD8rrkfAUzBE0lkUUvqq8mhOqIARVHqA6QuAZ5IIotaUl9NZpR2e0mdWuUm7yhKotoZSJ3IwpbUk3pGDLqKI/UpUpcAUqexuCX1Vd+hGqmXDaROY4FL6qvJlFLn1mhFqeUDteiodgZSpyEvqVU3SGjQLEDpvK3oQHECIX5bSsqcgdRpyEtq3Q0SGjSH6q61oooVZUhdBEidRGBJrXc2R1jQzsE4pF42kDqJwJI6Ycv8+fSpFdXr0kLqMkDqJOTtyayW1JOGdlVUr58aqcsAqZOQB2qbLPUtzfhzx6zWEKmXDKROIbDjt02W+jfNWtFuWS297YyQugyQOoX3stSGTk8M1d2yWg2pk/dYQuoSQOoUtsSX3KTw+4FGLUm3HVBGep9sjNQlgNQJXMgDdcLB7h2I3wHl8d9vJ/6/T5G6BJA6Aflc6rSzbdpZjZVUb2GA1EWA1AnINaLaO35PM4j9/XiuJrXcT812os5A6gTkGlHtHb+naYbKOtWj7kT+CITh3MsiQOr+BGpEdQ/REmgclLEeqWJism1b/MIXuR8ETILU/ZGPsFU+REtgGPkL0pg0J8bw5C+c+znAFDyR/sg1ohvWTkefq9WIl6dVn8hxsme5nwNM8f/IukmoNohIrQAAAABJRU5ErkJggg==';
			$region_number 		= $this->_common->test_input($_REQUEST['region_number']);
			$contact_no 		= $this->_common->test_input($_REQUEST['contact_number']);
			$is_NEA_certified 	= $this->_common->test_input($_REQUEST['is_NEA_certified']);
			//$referral_code = $this->_common->test_input($_REQUEST['referral_code']);
			$emp_histories = json_decode($_REQUEST['employement_history']);
			if (!filter_var($email, FILTER_VALIDATE_EMAIL) && $email != "") {
				$result_array['result_code'] = 1;
				$result_array['error_msg']   = $this->_common->langText($locale,'txt.register.error.invalidemail');
			} else {
				$userAccountId = $this->_common->check_session($secretToken);
				$profileDetail = $this->_common->get_profile($userAccountId);
				$this->_db->my_query("DELETE FROM `".DB_PREFIX."employment_histories` WHERE `user_account_id` = '$userAccountId'");
				//$updateArray = array("first_name" => $first_name,"last_name" => $last_name,"display_name" => $display_name,"email" => $email,"school_attend" => $school_attend,"portrait" => $image,"region_number" => $region_number,"contact_no" => $contact_no,"is_NEA_certified" => $is_NEA_certified,"referral_code" => $referral_code,"employer" => $employer,"job_types" => $jobTypes,"updated_at" => $current_date,"updated_by" => $userAccountId);
				$whereClause = array("user_account_id" => $userAccountId);

				if($portrait != ""){
					$_REQUEST['portrait'] = $this->_common->image_upload($portrait);
				}
				if($email != ''){
					$_REQUEST['email_id'] = $email;
					$updateArrayAccount = array("email_id" => $email);
					$whereClauseAccount = array("user_accounts_id" => $userAccountId);
					$this->_db->UpdateAll(DB_PREFIX."user_accounts", $updateArrayAccount, $whereClauseAccount, "");
				}
				if($contact_no != ''){
					$_REQUEST['contact_no'] = $contact_no;
				}
				if(count($_REQUEST) >= 4) {
					$update = $this->_db->UpdateAll(DB_PREFIX."applicants", $_REQUEST, $whereClause, "");
					foreach($emp_histories as $emp_history) {
						$employer 			= $emp_history->employer;
						$length_of_service 	= $emp_history->length_of_service;
						$job_type_array 	= $emp_history->job_roles;
						$job_types 			= implode(",",$job_type_array);
						$insertEmpHistory 	= "INSERT INTO `".DB_PREFIX."employment_histories` (`user_account_id`, `employer`, `length_of_service`, `job_types`, `created_at`, `updated_at`) VALUES ('$userAccountId', '$employer', '$length_of_service', '$job_types', '$current_date', '$current_date');";
						$this->_db->my_query($insertEmpHistory);
					}
					if($update) {
						$result_array['result_code'] = 0;
						$result_array['success_msg'] = $this->_common->langText($locale,'txt.change_user.success');
					} else {
						$result_array['result_code'] = 1;
						$result_array['error_msg'] = $this->_common->langText($locale,'txt.change_user.error.unable');
					}
				} else {
					$result_array['result_code'] = 1;
					$result_array['error_msg'] = $this->_common->langText($locale,'txt.change_user.error.unable');
				}
			}
		} else {
			$result_array['result_code'] = 1;
			$result_array['error_msg'] = $this->_common->langText($locale,'txt.change_user.error.session');
		}
		return $result_array;
	}

	public function change_user_password() {
		$locale 	   = $this->_common->locale();
        $result_array  = array();
        $current_date  = CURRENT_TIME;

		if($this->_common->test_input($_REQUEST['secret_token']) != '' && $this->_common->check_session($_REQUEST['secret_token'])) {
			if($this->_common->test_input($_REQUEST['password']) != '' && $this->_common->test_input($_REQUEST['new_password']) != ''){
				$secretToken 		= $this->_common->test_input($_REQUEST['secret_token']);
				$password 			= $this->_common->test_input($_REQUEST['password']);
				$newPassword 		= $this->_common->test_input($_REQUEST['new_password']);
				$userAccountId 		= $this->_common->check_session($secretToken);
				$selectApplicant  	= "SELECT * FROM `".DB_PREFIX."user_accounts` WHERE `user_accounts_id`  = '$userAccountId' AND `password` = '$password'";
				$resultApplicant 	= $this->_db->my_query($selectApplicant);
				if ($this->_db->my_num_rows($resultApplicant) > 0) {
					$updateArray = array("password" => $newPassword,"updated_at" => $current_date,"updated_by" => $userAccountId);
					$whereClause = array("user_accounts_id" => $userAccountId);
					$update = $this->_db->UpdateAll(DB_PREFIX."user_accounts", $updateArray, $whereClause, "");
					if($update) {
						$result_array['result_code'] = 0;
						$result_array['success_msg'] = $this->_common->langText($locale,'txt.change_user_password.success');
					} else {
						$result_array['result_code'] = 1;
						$result_array['error_msg']   = $this->_common->langText($locale,'txt.change_user.error.unable');
					}
				} else {
					$result_array['result_code'] = 1;
					$result_array['error_msg']   = $this->_common->langText($locale,'txt.change_user_password.error.current_password');
				}
			} else {
				$result_array['result_code'] = 1;
				$result_array['error_msg']   = $this->_common->langText($locale,'txt.change_user_password.error.required');
			}
		} else {
			$result_array['result_code'] = 1;
			$result_array['error_msg']   = $this->_common->langText($locale,'txt.change_user.error.session');
		}
		return $result_array;
	}

	public function reset_user_password() {
		require_once("libraries/sendMailCron.php");
		$locale = $this->_common->locale();
        $result_array   = array();
        $current_date   = CURRENT_TIME;
        if($this->_common->test_input($_REQUEST['unique_id']) != '' && $this->_common->test_input($_REQUEST['email']) != ''){
			$unique_id 	= $this->_common->test_input($_REQUEST['unique_id']);
			$email 		= $this->_common->test_input($_REQUEST['email']);
			if (!filter_var($email, FILTER_VALIDATE_EMAIL) && $email != "") {
				$result_array['result_code'] = 1;
				$result_array['error_msg'] = $this->_common->langText($locale,'txt.register.error.invalidemail');
			} else {
				$user_account_id = $this->_common->check_user_availability($unique_id,$email);
				if($user_account_id > 0) {
					$newPassword 	= rand(10000,100000);
					$encodeNewPass 	= md5($newPassword);
					$updateArray 	= array("password" => $encodeNewPass,"updated_at" => $current_date,"updated_by" => $user_account_id);
					$whereClause 	= array("user_accounts_id" => $user_account_id);
					$update 		= $this->_db->UpdateAll(DB_PREFIX."user_accounts", $updateArray, $whereClause, "");
					if($update) {
						sendActivationMailAction('Jobs On Demands', $email ,'forgotPassword','en','forgotPassword',$newPassword);
						$result_array['result_code'] = 0;
						$result_array['success_msg'] = $this->_common->langText($locale,'txt.reset_user_password.success');
					} else {
						$result_array['result_code'] = 1;
						$result_array['error_msg']   = $this->_common->langText($locale,'txt.change_user.error.unable');
					}
				} else {
					$result_array['result_code'] = 1;
					$result_array['error_msg']   = $this->_common->langText($locale,'txt.reset_user_password.error.notexist');
				}
			}
		} else {
			$result_array['result_code'] = 1;
			$result_array['error_msg']   = $this->_common->langText($locale,'txt.reset_user_password.error.required');
		}
		return $result_array;
    }
    public function logout_user() {
		$locale = $this->_common->locale();
        $result_array  = array();
        $current_date = CURRENT_TIME;
		if($this->_common->test_input($_REQUEST['secret_token']) != '' && $this->_common->check_session($_REQUEST['secret_token'])) {
			$secretToken = $_REQUEST['secret_token'];
			$updateArray = array("secret_token" => '');
			$whereClause = array("secret_token" => $secretToken);
			$update = $this->_db->UpdateAll(DB_PREFIX."user_accounts", $updateArray, $whereClause, "");
			if($update) {
				$result_array['result_code'] = 0;
				$result_array['success_msg'] = $this->_common->langText($locale,'txt.logout.success');
			} else {
				$result_array['result_code'] = 1;
				$result_array['error_msg'] = $this->_common->langText($locale,'txt.change_user.error.unable');
			}
		} else {
			$result_array['result_code'] = 1;
			$result_array['error_msg'] = $this->_common->langText($locale,'txt.change_user.error.session');
		}
		return $result_array;

	}

	public function refer_friend() {
		require_once("libraries/sendMailCron.php");
		$locale 		= $this->_common->locale();
        $result_array   = array();
        $current_date   = CURRENT_TIME;
		if($this->_common->test_input($_REQUEST['secret_token']) != '' && $this->_common->check_session($_REQUEST['secret_token'])) {
			if($this->_common->test_input($_REQUEST['first_name']) != '' && $this->_common->test_input($_REQUEST['last_name']) != '' && $this->_common->test_input($_REQUEST['email']) != '') {
				$secretToken	= $_REQUEST['secret_token'];
				$firstName 		= $_REQUEST['first_name'];
				$lastName 		= $_REQUEST['last_name'];
				$email 			= $_REQUEST['email'];
				if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
					if(!$this->_common->get_user_account_id_by_email_id($email)) {
						$userAccountId = $this->_common->check_session($secretToken);
						$profileDetail = $this->_common->get_profile($userAccountId);
						$myReferralCode = $profileDetail->my_referral_code;
						$body = 'Hello '.$firstName.' '.$lastName.',<br/> Your friend '.$profileDetail->first_name.' '.$profileDetail->last_name.' has invited you to join Jobs on Demand. The invitation code is : '.$profileDetail->email_id.'.<br/>';
						$body .= '<br/>Thanks and Regards<br/>JOD Team';
						sendActivationMailAction('Jobs On Demands', $email ,'referfriend','en','referfriend',$body);
						$result_array['result_code'] = 0;
						$result_array['success_msg'] = $this->_common->langText($locale,'txt.refer_friend.success');
					} else {
						$result_array['result_code'] = 1;
						$result_array['error_msg']   = $this->_common->langText($locale,'txt.refer_friend.error.alreadyexist');
					}
				} else {
					$result_array['result_code'] = 1;
					$result_array['error_msg']   = $this->_common->langText($locale,'txt.register.error.invalidemail');
				}

			} else {
				$result_array['result_code'] = 1;
				$result_array['error_msg']   = $this->_common->langText($locale,'txt.register.error.required');
			}
		} else {
			$result_array['result_code'] = 1;
			$result_array['error_msg']   = $this->_common->langText($locale,'txt.change_user.error.session');
		}
		return $result_array;
	}

	public function test() {

		$json_array = json_decode($_REQUEST['employment_history']);
		foreach($json_array as $r) {
			$job_roles = $r->job_roles;
			$job_roles1 = implode(",",$job_roles);
			echo $job_roles1.'<br/>';
		}
		//die;
		// employment_history=[{"length_of_service":"3","employer":"Starbucks","job_roles":["Waiter\/Waitress","Cleaner"]},{"length_of_service":"3","employer":"Starbucks","job_roles":["Waiter\/Waitress","Cleaner"]},{"length_of_service":"3","employer":"Starbucks","job_roles":["Waiter\/Waitress","Cleaner"]}];
		//$portrait = 'iVBORw0KGgoAAAANSUhEUgAAA9QAAAIwCAMAAABQqNOWAAAAvVBMVEUAAAACAgIKCgoLCwsNDQ0PDgAODg4QDwAPDw8YGBgfHAAeHh4gHQAgICAvKgAvLy8wKwAwMDA8PDw/OAA/Pz9AOQA/Pz9PRgBPT09QRwBQUFBfVABeXl5gVQBfX19vYgBvb29wYwBwcHB/cQB+fn+AcQCAgICPgACPj4+QgQCQkJCfjgCfn5+gjwCgoKCvnACvr6+wnQCvr7C/qgC/v7/AqwC/wMDPuADOzs/fxgDe39/v1ADv7+//4wD///9stINcAAAmXElEQVR42u2d61oTSbRAOZcBjojCERFH1KiIqIxkRkYg5PD+j3UAuXSSXemurr1Tuypr/Zvvk56ku1eqal+qVv4PAKpiJfcHAABdkBqgMpAaoDKQGqAykBqgMpAaoDKQGqAykBqgMpAaoDKQGqAykBqgMpAaoDKQGqAykBqgMpAaoDKQGqAykBqgMpAaoDKQGqAykBqgMpAaoDKQGqAykBqgMpAaoDKQGqAykBqgMpAaoDKQGqAykBqgMpAaoDKQGqAykBqgMpAaoDKQGqAykBqgMpAaoDKQGqAykBqgMpAaoDKQGqAykBqgMpAaoDKQGqAykBqgMpAaoDKQGqAykBqgMpC6Ri7+vebvz/d8vf6v3B8JFgdSV8TZv18/v335ckVm7eXrz3/9e5n7Q4I5SF0FZ39/fr210omnrz//jdlVg9Sl8+/Xrjo32PqA2PWC1AVz8feHl9E+P4r99SL3FwATkLpQLv56/bS/0A8DNl5XCFIXyMVfb9OFvvf6L+bhtYHUpfHvh/gl9FzW3p7l/k6gClKXxOVfr9d0jf7Ny5+5vxkogtTFcL2KthD6fhae++uBGkhdBhdflSfds1ozWtcCUheAvdG3vGRtXQdI7Z1Ly1n3FG/JcNUAUvvm78UZfcPa19xfGNJBasecfTCJdc9lizl48SC1Vy4Xs5Ce5QPVKIWD1D75+TaP0Tc8ZbAuG6R2yOVXtSrQfnzOfQcgBaR2R85B+p6XhMELBqmd8ddWbqFvWTvJfSOgN0jtiYvPiw93h/iQ+2ZAX5DaD5rz7tWdBv0u8ZIoeKEgtRdU5t0bO4OD4en51Qynw4P9WLmJghcKUrsgPd69ujM4Pr1q4fx48Dzimmvfc98X6ANSO+AisXLs2udRm88PjI73VztfmarREkHq7FwkLaV3DlrH51mGnb1+m/vuQDxInZkUpZ8Pegh9x3HHFTbhsvJA6qz87L3F7+re8bi30b/n4d2Gazo8igOpM9Jb6Y2EIbrB+KCL1mtYXRhInY2Lnq3SG4PzdJ9jtCYIXhhInYnLD/2MPuge5+6odYf/K1YXBVLn4WufJJbmGP3IaK/9//w29/2CCJA6Byc9Sk1W91XW0RKnG+1WEwQvB6RePJc9FtPJse7UOfgWVhcDUi+c+Jm3+kJ6lvPW8tEtWqxLAakXzFl0Gstu2j0xWA/aPgeprVJA6sXyOXaQPrKcdk8wbMturXGGRxkg9SK5iBym9xYySN8zap2Ck9oqAqReICdRq+lV+5X0FOPW5BZWlwBSL4zLqNaNneMFG31LaxScfUYLAKkXxdlWhNKLCY4JtC6s3+a+j9AKUi+IiKn34ufdDc6xuniQejF0j3pvZJl3PzJuC5e9pgzFOUi9CLrXkO0M8yp91SFcRnGZc5B6AVxudVQ621J6kn2sLhqktqdriGw/41J6kmOsLhmkNuesW4jMj9I3VreEy9jjyDNIbU0np1cPFlYN2o22IDiF4I5BamM6OT1wpvSN1RtYXSpIbUsXp11NvB9oS21htVuQ2pQOTu+4VPrG6h2sLhOktqTd6Q0fSSyZfawuEqQ25KLN6dWD3N5idYUgtR2tNSd7XmfeDxxjdYEgtR0tOyKs5q8ITbf6JPdNhlmQ2oyW9uk9f2ks0eqWhDXbJvgDqa34Xv4wfUtbGQpWuwOpjTiba8JOGcM0VhcJUttwOfcMDudBb6wuG6S2Yd6CetVzblqibZtRrPYFUpswb0H93OKUO1vaSkax2hVIbcG8qpPnBS2nsbpIkNqCObsX7ef2E6urB6kNOKnOaawuCaTWZ07kO8rp8a/Dw93dzZu/29zdPfz2K6/V+1hdCEitz3sNp/95tz3797uHOYNsWF0ISK3ORbrT//wZvMbmx3xdIFhdBkitzpvQS/+8mzqjj+vz5dn94dTqD7lvPdyC1NoE60O75bJG71ba2cyldYvVb3PffLgBqbUJNVyudlkOd1L6VutMDSEtVr9kQ3AHILUyP0Pve4cjssYfOyp9w26emFmL1U/ZNiE/SK1MaEU9aPfldDPC6WsOPVq98jX3EwCk1iUU+t5oXVCP/1yJZTvLYN1mNVPw3CC1LqGBurUx6zxymL5lPcuxt21Wr/3M/RSWHKRW5TLQydGaof7WQ+kb3tl4e1PM9vHPw8N/xKR4m9XktvKC1KoEWi5X2wpGuga9Z9nVb/oaf9t9vP7mR2GS0Wo1B+jlBKlVCeSzWnY6Ge+2STKHbWWrx4fTtS+7s1q3Wr3yOfejWGKQWpNAmGx1vnfj7VZF5rGpGi77JpWzzU4HDlo/1suL3E9jaUFqTT71Gahbnd65Zt4ZlOt6Vo8D64D1mcG67Vz6lZU1ButMILUmW/LrPXegnuf06v7Rg03j04M9a6vnfJaZOHu71aysM4HUigRm3/v9PFrdn13LDuXF7KbOunrunKGP1StvyVlnAKkV+SK/2XPH0ZBHG0eyqOMjaSauEy2bH6+b+RpdrGYOngGkVuSVrOc8jwJr2LnHYR4LWv+p4HRL8Gt95oej7UieW55+ZrReMEitiPxWH83x6Ej+k0HLwHswa1P6+QCjljZu4X/Rts3//WiN1gsFqfUINGjNKTwZin/QYWPw8WzMLLkXsz33PPtNulm9svL679wPZ5lAaj3khNac/U7G4tjYoZ/r5vdgWqf1xGX1qF1NIeLXtsno43D99i8S1wsCqfWQl9RzHJX6sjofhzlzFk7isrpD2Gtd+mHaa/+7e56+/vpv7oe0DCC1HnIzR1jSofCvY87v2O/8f+pCFznFXrP24rIJtj4wFTcGqdW4lF/ioEXS5Hs/ag49mBpIkybgXYSUo3GnHRfWD6y9ZipuCVKrIcfJwktqYYSLPb/jOO3Pm3RYUgf/BzFT8Hte/kVI3AqkVkMuPdmPsCheyimrE8pFT7uYuBP666Mufz3NW+bhNiC1GvLBHMEs9WwGqc9AO2n1rrHU28E/P+8aBZ/gKcO1BUitxgvxvQ3tYzQ7UAeHwQir+x9nnzZSX0/BB32sXln7zOpaHaRWQ5Y6FLyaGaj7nls9YXX/oTpVaiFz3pEPjNbKILUackYrNLDNhL57r4cnhsj+Q3UX/+bXxfSJl91A04cySK1G1Ng2U+qRULu907hM/wB4l3H2qOUafQfrpye5H15VILUWcpo6JPV2x3/XhYlSzd656vbS77ll7HcfpedgTeO1JkithZymDoycM2Gy/tPma84bF+q9E/iw3bwux3aebrRfR4JdUvRAai1kqQOz6unZd0rZyNVEHcte74u0T53bZt+3jCPLRu9ZYwquBVJrESX19Cw19SD5xgS89zVaOzo2Ol5o1HMOzqH1SiC1FlFST8W+EwfqiYRU9ER+/G333eEN/9MiXfeGkdOdlT5gtQ5IrYUstSzCdFI4fTfQxzBXdBi9S4TshqhY3rDX0hqrVUBqLU7E11QeN6dmul3ntXN4jLx122OhQUfhYotj+ozWa0TLNEBqLeR9T2Spp2JJ0R4KPCgUHSnrlltejc+VnXadAzzylMyWAkithdykJUs9NYhp7MX/0CcVPf3ustNvl33TBEYHsbPw17kfYw0gtRbymlqWeipOpuD01fh+wI0PpI+ODvZ2du4C6P8hfo24zRuanA/irCaxlQ5SayFLLdeCTP6blGqyR47n/Q8jfhyEOfNG0kZJcWtrJuDpILUWMSmtLv8mmpvI1PPkjYKvB+4prTcSfycii1E+5X6Q5YPUWmSXWo/xcP9+LbxzlLzgP1+J4mnuB1k+SK1Ff6mT6r7tOD1NrXO7IzJYRrI6FaTWIkLqqXYOp1KrEZnZepP7SRYPUmtxJr6hcgp6uaQeMv9eLEithviGypHt5ZK6Y3nLA7kfZPFwB9VA6hCDOKl/5n6SpYPUajyRXtB18S2fHLqqlzoy/o3UiSC1GvJuouJbPlmPUb3UkfFvpE4EqdWI2CJ4Mh7sLk+tTqfy8ge+5H6SpYPUakS0aU0WWSVvkeCecVSojJqyRJBaDVlqscZycpOE7VhHyiMqVY3UiSC1GnLvpTi3HneYoldFpzM176FRKxGkVkMuKZP3LJg8Tk6hC8M7MXsREihLBKnVuBDfUDlRPZj4N/Uvqrsd1YXUOiC1HvIrKr7jw4l/splbuQUQ0VWN1IkgtR7PxFdUXjBPhoM19jNyTsRQjdSJILUeMQdUT4aDl2D+HTFUs/dJIkith5zTkktLJssx1uuPf0cM1bmfY/FwB/WQc1qBUXhy/p26s1gJdB2qn+R+jsWD1HrIOa3AtoKTRWXLECrrOlS/yP0ciwep9ZBPqA5sADxVjsFQ/cCr3M+xeJBakT/ElzQQ2p4MlS3DqrpjWRlVoqkgtSJy+HvY6RXXOHrHO90qwMlopYLUisSEv2decaWtOz3TrVnrIvdjLB6kVuS7+JKGDuAYTb7iu7mVWwBd9vX/I/dTLB+kVkTeUHS94yt+lFu5BdBhCxSC38kgtSbyazrq+IpTLHoDcbJkkFoTufp72PEV31yCCHh7CyZxsmSQWpM34msa3oNsMPkP3+VWzp5RW6yMJXU6SK2JXCgaPqp2/HzprD5qkZrSk3SQWhO5UHQ9/IqfTw1cS1BY1lJXxvF46SC1KvKLOicFPb15bv1Wt0zAyVKng9SqREbKrmaPpKnf6rkT8Ge5n2ANILUqcqRsbgno/tJZPW8Czkb+CiC1KrGRshv2ls3q0X+HpWbXEwWQWhU5UrYy9x2fDoFXHwM//c+g0xw4rwFS6yK/q/NrxcbT89FtT80do2+Hh6pH+H2cM/smTKYBUusiR8raZtTT6+p1P9v7/1ZwV62EdbQ9x2nqvlVAal16RMokq90srN/df6CPOjWsx+tznKZEVAek1qVPpOz2ZZ8eq33UgTeq0zd/pF9u/G5lHlST6YDUusjdlyvt7/v5VMuWj6F60PxIu6lL6/PNuU6zolYCqZWRX9cOS9LxZGrLx0n0UyG8tKX14XylabrUAqmVkfcp6zTuDjdi/2LRUq+svOsdmJ8bIbvhCTlqJZBamffiC9ttW8Hx414oqz7W1EJJZ0+tv82NkN1AlEwLpFYmbp+ymeHsPgzuY6CWtwp8F7+2Hu22Kc3kWw+kVqZ3pKyp9YabPPWx+G12/4m7ymHrME2KWhGk1kZ+ZyPmrONT1QKuRE7lvQI3f3RfH5y3raZXWFCrgtTaxBxoWwLjwL6+6x+7/VCN55WF3vPHWe7HVhNIrU3sPmX+GYV6JXd/tP/xj/aZN04rg9TayDVle7nNTOI0tF/3+sf5mevTDjNvnNYGqbWRuy+3c3uZyHFwG/7Nb8Fp+Gh+Veg9z3BaF6RWR35zc1uZzEF4b7Ft0etxN6VXXhEjUwap1XkivrrlH78xnqP1yvb0PHzcIY11CxsYqYPU6kQdaFsUc7Ve2Xz342HAHnVVmqm3AUitTtyBtmUxPp5/xN1vsTuupa95w9TbAKRW50R8fcsOfzc4btmNf+W//qer0n+c5H5WdYLU6siFol2rvwvgdL+rtfN5Rv+0DUitj/wK51ZRk9FB2zl3HaCDwwqk1kcOf/vopVSjdRbewhM6Lc1Aan1qq/4OMBpsRIrcgAiZIUitj7xPgpMOaVWG+/2m4UTITEFqfeTq7zpyWjMc/2+809SQ2YLU+sjV39XktBr8Omzf0YRheuEgtT4X4rtcekvHtM/f3nXqwGKYXjxIbYD8NufWUInRr2+Hu/18vuYJw7Q9SG2AfKBWyTmt8a9fvw4PD3f72/yb9wzTCwCpDSg+p3Xj8I9riT/uXpOmcZMXdG8sBKQ2QG7pcJ7Tuhb5281YvNuxvyqaJ99zP5dlAakNKCunNf51PSInTqs78ImZ96JAagPknNZ+bnsFfh2aDcyTvKF5Y3EgtQFl9GmNvv25EJ+veYHSiwSpLRDf7M3cFjcZ/1AMgLUpTe/GYkFqC/4QX+7cIj8yereYSTdKZwGpLZBzWr1PgVVXemFGo3QOkNoC+ZQOJ4nqrnsCKvCGxHQOkNoCx4nqLqfV6fDHJ8JjeUBqC+RDqj0kqo8XNUw/o9QkG0htgZyodiD14YIGaebdOUFqC7wmqgcLUfoVg3RekNoEn1IfL8DoZ18oB80NUpsgvu/rmZ0+N19Pv/hCbMwBSG2CnKjO6/TY2OlX3xmjfYDUJnisPlE6V0Meoj9RZOIHpDZB3iU4a/XJqZXQz96fMES7AqlNkKtPskpt0cDx7M0XRmh/ILUJ/rZJUI58//HizaefDNA+QWoT/FWfbCrZ/OTa5hOGZ9cgtQnupE5YUT95cc2nT5++//xJwqoIkNoEeT//jNUnezEz61uHP/28Jvd9hD4gtQ3OpB61u3xj8slParYrAKltEL3Jd/LOYK7O778Q86oJpLZBPqQjm9TBMNmT94zN1YHUNviqEz0PLZ+/5L5PYABS2yBLnes4rYOA04zSVYLUNvgqKQtsYcQJlHWC1Da4kjoQ+36R+yaBDUhtgyupAyWiZKErBaltOBE1ylRSJjddPsl9j8AIpLbBVZ2ovKR+n/segRFIbYMrqQmTLRdIbcOZI6kDzRy5bxFYwaM1QvQoT/H3EbHv5QKpjXAktdyh9Sn3HQIrkNoIR1LLhd8sqasFqY3wI/VYXlKz4UG1ILURYvF3lu385TgZWep6QWoj/LRpyd0cr3LfIDADqY3wI7VcT0acrF6Q2gg/Uu+In4TC73pBaiPeuJFaPkKL/YvqBamNkNu0cpymRZxs2UBqI9z0XspbGVFPVjFIbYQbqeWMFi1aFYPURriRWs5oEfyuGKQ2wrnUFIlWDFIb8d2L1GS0lg6kNuKnb6lz3x4whKdrhCz10eKlXkfqZYOna4Sb/YzEz0FGq2aQ2gikhlwgtRFIDblAaiO8SC3XnpCmrhmkNgKpIRdIbQRSQy6Q2ohLJ1LL52h9z317wBCktsKJ1HKVKAVlNYPUViA1ZAKprRBl2kdqMAeprRBlWvzG30i9fCC1FUgNmUBqK5AaMoHUViA1ZAKprXAtNQdp1QxSW+Fa6tw3Byzh8VrhWmpG6ppBaitcS82aumaQ2gqkhkwgtRVIDZlAaiuQGjKB1FYgNWQCqa1AasgEUluB1JAJpLYCqSETSG0FUkMmkNoKJ1IPxc/BHmU1g9RWOJGa3USXD6S2AqkhE0htBVJDJpDaCqSGTCC1FU6kPhc/x6vcdwcMQWornEjNqZfLB1JbgdSQCaS2AqkhE0hthSgTh86DPUhthRepn4sfJPfdAUN4ulZ4kXoHqZcNnq4VSA2Z4Oka8ROpIRM8XSPcSL0nfpCz3PcH7EBqI9xITUP10oHURiA15AKpjXAuNbskVAxSG+FG6qH4QWjTqhikNsKN1PReLh1IbYRzqd/kvj9gB1IbIUt9tHipKf5eOpDaCFnqUy9SP819f8AOpDbiixupN8RPkvv+gB08XCM+uZFarhO9yH2DwAykNsKP1HKdKNUn9YLURviRmuqTZQOpjfAj9ZH4SUhU1wtSG+FHajlRTU6rXpDaCD9Sj8RPQk6rXpDaCFnqDE4HEtU8+Hrh2Rrxyo/Uck6L8He1ILURL/xILee0CH9XC1Ib4UhqOaf1PvcdAiuQ2ghHUsvh763cdwisQGojHEkth7958tXCozXCkdRXq0TKlgqkNkKU+nkeqeXwNzVltYLURqxJHmU49PIGOVJGTVmtILURK46kHoofZi33LQIjkNoIT1IHImWc0lEpSG2EJ6kDm5+wqK4UpDbCldT74qchU10pSG2EqNF+JqmP5fn3Ze6bBCYgtQ1nokUZtv2+JbCopvy7TpDaBjd7+f9GXlSzo3+dILUNzqSWF9UkteoEqW1wJnVgUX2S+zaBBUhtgzOpx7LUzL+rBKltOBElGuaS+uo58+/lAalt8LPv4G/k8m/m31WC1DZ4k/pclvpV7vsEBiC1Dd6kDiS1qD+pEaS2wZ3UclKL+pMaQWobZKnP80k9ZP69NCC1DZ52M/qNvKcRR9pWCFLb4E/qwPybnYLrA6lt8Cd1YP5Nqro+kNoGf1KH5t+EyqoDqW0QpV7NKvVAlpqtEqoDqW0Q/cm18clvAvUnbFVWHUhtg0OpQ/UndHXUBlLb4FHqo8BQTVVZZSC1DR6lDmxqxK6itYHUJlyK9uTad/Ae+aDqlae57xbogtQmONsj4Y5hYKgmq1UXSG2CT6lDoTKG6rpAahOcSh3YKoFTbesCqU1wKnVgq7KVl7nvF2iC1CZ8Ed3Jt0XZPYGuDobqqkBqE9ztkXBHqKqMtuqaQGoTvEp9tROwmrbqikBqE9xKPQxITa1oRSC1CW9Ec0a5lb4KZrUYqisCqU1w2E59R+AAHmpFKwKpTfArdWivhDXaOqoBqU1wLHWoAIWhuhqQ2oQtv1KPGaprB6lNELXJ3Hl5zyAwVNPWUQtIbYJnqUNt1bR11AJSm+BZ6mCtKEN1JSC1BZeupQ4N1ewrWglIbYHTJq17Ajug0NZRCUhtgXOpTwNS04FZB0htgXOpg20dbAFeBUhtgXephwGpaeuoAqS2wG2T1j20ddQMUlvgXupQWwdDdQ0gtQXupQ4O1dSKVgBSW/DevdShI3ho66gApLbAcZPWHbR1VAxSW+Bf6mAHJrWi5YPUFhQgdWiopq2jfJDagqf+paato16Q2gLRFi/9HHfQ1lEtSG1BCVJzWke1ILUFRUgdOq2Dto7SQWoDzkRZ9nJbPA2ndVQKUhvgvZ/jjlAHJrWihYPUBhQi9dVzhuoqQWoDSpGa0zrqBKkNkPs5jnM7PEugrYNa0bJBagP8N2ndEWrroAClaJDagGKkpla0SpDaAP+dl/fQ1lEjSG1AAf0cd4RqRSlAKRmkNqAcqakVrRGkNqAgqUNDNQUoBYPUBqxJmqzm9leGWtH6QGoDREu89XPcEaoVfZ/7JkJvkNqAkqQO1YpSgFIuSK2P7zMvpwnVin7JfRuhL0itj1z6Pchtb4hArSgFKMWC1PqU0s9xBwUotYHU+hQmdahWlAKUUkFqfb6IjhzlljfIIDBUc7BtoSC1PsX0c9xBAUplILU+pUl9tRewmqxWmSC1Pm9KkzpUgMIOKGWC1PrIpd/j3OrOIVSAkvtOQi+QWp+C+jnuCBWgkNUqEqTWpzypQwUoHMFTJEitj+jHRm5v5xIqQKGtukSQWh9RD6+l378JFaCQ1SoRpNanQKmDO6DQVl0gSK3OWYlShwpQyGoVCFKrU1jp9x2BHVDIahUIUqtTptTDwFBNVqs8kFqd70VKTVarHpBaHbn0e6gt4Y93u+80S09DBSj0ahUHUquzkH6O8+3bq/6pV3xKVqsakFqdRfRzjNfvLrurd81BYKimV6s0kFqdRfRzPKaV9ab1oawWOxCWBlKrs4DS74Z/+3pXDbRVswNhaSC1Ok/tpW7UfykWtYTaqk9y31GIA6nVEcV4rul0c6KsWakWyGqxA2FhILU6ohiqVaLNQm3N/QxDWS0KwMsCqbW5MJe6OVCvagbgQlktztUqC6TWxv58juZArVuoFujVogC8LJBaG/PSb7OBOpzVogC8KJBam0/WUtsN1MFeLUJlRYHU2lhXiT4Uk+kP1BSA1wFSa2NdJdrcTkz/JM1AVosC8JJAam3kgrKRlnXNgVrvquJPRjNURgF4QSC1NsZVok3rFEtEH34zCJWVD1Jrs2Yrte1AHcxqsVdCQSC1NqITarUnzUiWwUAdLgAnVFYOSK3Mpa3Um8YDNaGyCkBqZeTakz0l44bWA/XV1RGhstJBamVsC8p2zQfqYAE4obJiQGplTAvKrHouJyBUVjpIrYxpQdm+/iVnOSdUVjhIrcwLQ6mbhSequy5MEjiCngbMUkBqZSy3HWzms47tpA4UgNOAWQpIrYxl7cn24wVX7ZwmVFY6SK2MqIPOifPNxa7pKT6BUBkNmIWA1LpYbmbUdM0onzX769GEvcrKAKl1saw9Wde+YJBAqIzDqssAqXUxPPKyGb9SP24v/L9qwLb+ZYDUushpapV9fBsHaPReo//4c327w1mZoVAZ2/oXAVLrYrfvSbPTuefIP76rMm1PhwVCZXR1FAFS6yKnqc8VpG5OiXuGyd51nr2HGjDp6igBpNbF7iCtxuy7ZzD9Mai92fpvAw2YpKpLAKl1EVXQqBRpzr57VpM1dkJqnToEGjBJVZcAUqtil6Zuzr57Fp02pG5d5Ie29SdVXQBIrYpdmnov/XIxUocOqyZVXQBIrYpZmlph9h0n9VCWmlR1ASC1KmZp6qZjfVu+oqS+CqSqf+a+xdAKUqvyShRBIU3dSBz3nszHST2QpSZV7R+kVsUsTd3YRbT3uB8ndaCrg65q/yC1KrII6U43g9G9G7TipA51dZCqdg9SayJv+q3QTd1IG/ffxyhS6kCq+lXumwxtILUmckZLIU3dSDD1P+lyJ07q0LFalIp6B6k1kTNaCgfOrsf52Cp1p1V+IFXN/Ns7SK2J1abfzaBV/6vsRF5kyPy7TJBaE6uMVmN5mzCXX4/9ZQikqikVdQ5Sa2KV0WpkqROG/ceLdOwwGchSf8l9m2E+SK2JLEGy0829gROG/ejhPpCq5gAe5yC1ImYZrcbV+h8LcBotdairmvm3b5BaEauMVkPHhNN2GlfpGo9n/l0kSK3IF1GB9IxWo2ok4VDqRrSt68I80FXN/Ns3SK3Ie1GB9B6tgcrFDnpcJVAqygGYrkFqRaxOvIwsBQuw3+MqgVJRtkpwDVIrIu86mH5CTnSCue2noXOSjfl3iSC1IrIByU7HJ5hFNvt8pECpKPFvzyC1HgsIfqdcrNdPQ+AAHuLfnkFqPeR2jvRdB4ePF0uIpI96/TQEWrWYf3sGqfWwaufoE7aepTHex/zOMP8uD6TWQw5+px9QGbm5QYAeaeobmH+XB1LrIQe/09s59lSkbvw0xOwxHJh/03/pGKTWQ379k52O7oNuvUrUT0Ng/s3+J35BajXk4HdCsfY92ypSN64SlTkPzL/Z/8QvSK2GXPmtcOTO48VSGr76Th6YfxcHUqshV34rHLnzeLGENHUjoxU5eWD+XRpIrYZV8FtH6v4VLIH590nu+w0hkFqNNfHdVzidQ0XqRvA7cvIQmH9z/o5bkFoLedsTheC3jtSDx6vEnpopz785/9ItSK2FWfC7sRpOqBJNaN8MzL9pqvYKUmshF4km7FRyT2M1nBB1a/RoxW5zFph/U1TmFaTWQt7zWyH4rSN1yopA3v/kRe47DgGQWost8c1XOJpaReqk9s3A/icktZyC1FrIL37/HX11pW6si+NXBIH9T0hqOQWplZDjZCk7lahK3T+jdYO8/zdJLacgtRJykajCKbbNcbK/1Gl7Fw7EL7eW+56DDFIr8UZ87xXiZDp56kbwu8dGiKfilyOp5RSkVmJLfO0VikR1pG58pj5/Lp9/SVLLJ0ithDyWpW8PrCN16t6F++K3I6nlE6TWwS5O1hwmt/teohH87tULGigqy33XQYTnooNdnExl55O04HewqOxn7tsOEkitgxwnSz8bT0nqxiX6LfPlojKO33EJUuuwJb70sf1QMoPHC/YtUGsEv/v1gh6I349FtUuQWgd5eqrQTH2lskVw4zP1u0AgqZX7toMEj0UFOU6m0Ux9NRGl6pn3Vji4R05qsaj2CFKrYBknayrZs5MzqfL7N/JOCSyqPYLUKsh9lzpxsmbouefPRGNV3rfGTe7UYlHtEaRWQT6cQydONjH17XcBhVPrA51auW88CPBUNAjsT6YTJ+t3XPwEjVPre9e4saguBqTWwDRONlGk2Wvwb8zf+9e4yYtqyr8dgtQayPuTKcXJJtazvZbpKqfWy4tqeqodgtQamMbJrq7OH6/Zq/o79Udh+kM0YKNghyC1BvI+/ip9l7c0Ltpnf6TG9D3h1Hp5Uc1GZf5AagXO5CW1St/lLYml2wrB76vQopqNyvyB1Ap8F193nb7LWwaPV+1TPJI40N8hl39TfuIPpFZAbtFSOMT2nuHjVTfj/7qxGk45C1cu/+ZIW38gtQJb4uuusj/ZHY3LxmeqGz8JSQF58VsSKfMHUqcTKD1R2Mf/gcaqOD5+nbpDwj1yTzWRMncgdTonstSKTje9jJ9/NyJcSQF5eaMyasrcgdTpyKUnCuddPtJMEkfPv7dT5u4N5I3KqClzB1Kn80J82dVKT27ZSLiw1uxBjpRRU+YOpE5Hnn1rtWj9ZtB//t3or0qcPYjfk+5LdyB1MoFuDr3Skxua8+/IhbFK5fctYqSMw3fcgdTJyLueKJae3NKYf0cmwHXOt75Brikj/O0NpE5G7uZQLD25pTH/jpwEaGW0QjVlhL+9gdTJyN0cmqUnN4x6X7uhYuI6X46Ufc/9AGAKpE4l0M2hWXpyS2NBux5Vwa2ww/Adcvcl1d/eQOpU5CW1aunJLc0scVQDpZ7UcvibnJY3kDoVeUmttuvJA+NGP3NUVkun8fKWDemrktPyBlKnspgl9dVklWbM4lhR6h3pq9LS4Q2kTmRRS+rJUFnMUK0o9UD8rrkfAUzBE0lkUUvqq8mhOqIARVHqA6QuAZ5IIotaUl9NZpR2e0mdWuUm7yhKotoZSJ3IwpbUk3pGDLqKI/UpUpcAUqexuCX1Vd+hGqmXDaROY4FL6qvJlFLn1mhFqeUDteiodgZSpyEvqVU3SGjQLEDpvK3oQHECIX5bSsqcgdRpyEtq3Q0SGjSH6q61oooVZUhdBEidRGBJrXc2R1jQzsE4pF42kDqJwJI6Ycv8+fSpFdXr0kLqMkDqJOTtyayW1JOGdlVUr58aqcsAqZOQB2qbLPUtzfhzx6zWEKmXDKROIbDjt02W+jfNWtFuWS297YyQugyQOoX3stSGTk8M1d2yWg2pk/dYQuoSQOoUtsSX3KTw+4FGLUm3HVBGep9sjNQlgNQJXMgDdcLB7h2I3wHl8d9vJ/6/T5G6BJA6Aflc6rSzbdpZjZVUb2GA1EWA1AnINaLaO35PM4j9/XiuJrXcT812os5A6gTkGlHtHb+naYbKOtWj7kT+CITh3MsiQOr+BGpEdQ/REmgclLEeqWJism1b/MIXuR8ETILU/ZGPsFU+REtgGPkL0pg0J8bw5C+c+znAFDyR/sg1ohvWTkefq9WIl6dVn8hxsme5nwNM8f/IukmoNohIrQAAAABJRU5ErkJggg==';
		//return 					$image = $this->_common->image_upload($portrait);

	}

	/**** At the Mobile end
	 * 1 - cash
	 * 2 - cheque
	 * 3 - bank transfer
	 * So by putting check changes done in the mobile end
	 */

	function get_dashboard_jobs(){

		$locale 		= $this->_common->locale();
		$result_array   = array();
		if($this->_common->test_input($_REQUEST['secret_token']) != '' && $this->_common->check_session($_REQUEST['secret_token'])) {
			$secretToken 	= $_REQUEST['secret_token'];
			$userAccountId  = $this->_common->check_session($secretToken);
			// get number of friends
			$applicant_data = $this->_common->get_profile($userAccountId);
			$noOfFriends 	= $this->_common->get_number_of_friends($applicant_data->my_referral_code);
			$result_array["no_friends"]    = $noOfFriends ;
			$no_completed_jobs  = $this->get_applied_jobIds_by_applicantId($userAccountId,6);
			$no_active_jobs 	= $this->get_applied_jobIds_by_applicantId($userAccountId,4);
			$rating  			= $this->get_rating_appliants($userAccountId);
			/* for now below mentioned three feilds are hard coded START*/
			$result_array["no_completed_jobs"]  = count($no_completed_jobs);
			$result_array["no_active_jobs"] 	= count($no_active_jobs) ;
			$result_array["rating"]    			= $this->round_up($rating,2);  ;
			/* for now above mentioned three feilds are hard coded END*/

			$UserjobIdsData = $this->get_applied_jobIds_by_applicantId($userAccountId);
			//print_r($UserjobIdsData); die;
			if($UserjobIdsData != false ){
				// get all jobs
				foreach ($UserjobIdsData as $key => $jobData){
						//print_r($jobData);
						$jobs = array();
						$jobDetails = $this->get_job_details_by_jobId($jobData['jobId']);

						$jobs['job_id'] 		= $jobDetails->job_id;
						$jobs['title'] 		 	= $jobDetails->job_title;
						$jobs['description'] 	= utf8_encode($jobDetails->description);
						$jobs['job_role'] 		= $jobDetails->job_type_id;
						$jobs['rate_per_hour'] = $jobDetails->payment_amount;
						$jobs['special_instructions']  = utf8_encode($jobDetails->special_instructions);
						$jobs['require_NEA_certified'] = $jobDetails->required_NEA_certificate;
						// start - added jhtang 20151204
	//					$jobs['company'] 		= utf8_encode($jobDetails->company_name);
						$jobs['company']		= utf8_encode($jobDetails->outlet_name);
						// end - added jhtang 20151204
						$jobs['company_logo'] 	= OUTLET_LOGO_URL.$jobDetails->outlet_logo;
						$jobs['outlet_logo'] 	= OUTLET_LOGO_URL.$jobDetails->outlet_logo;
						$jobs['outlet_name'] 	= utf8_encode($jobDetails->outlet_name);
						$jobs['outlet_address'] = $jobDetails->outlet_address;
						$jobs['start_date'] 	= $jobDetails->start_date;
						$jobs['end_date'] 		= $jobDetails->end_date;
						$jobs['start_time'] 	= $jobDetails->start_time;
						$jobs['end_time'] 		= $jobDetails->end_time;
						$jobs['payment_method'] = $jobDetails->payment_mode;
						if( $jobDetails->payment_mode == 2){ // bank transfer
							$jobs['payment_method'] = 3 ;
						}
						if( $jobDetails->payment_mode == 3){ // cheque
							$jobs['payment_method'] = 2 ;
						}
						$clockInOutData	  = $this->getJobClockInOutData($jobData['jobId'],$userAccountId);
						$jobs["insurance_included"]	 = (!empty($clockInOutData) &&  $clockInOutData != false) ?  $clockInOutData->insurance_included : "N/A" ;
						$jobs["clock_in_dt"]	 = (!empty($clockInOutData) &&  $clockInOutData != false) ?  $clockInOutData->applicant_clockIn_dateTime : "N/A" ;
						$jobs["clock_out_dt"]	 = (!empty($clockInOutData) &&  $clockInOutData != false) ?  $clockInOutData->applicant_clockOut_dateTime : "N/A" ;
						$jobs["meal_hours"]	 = (!empty($clockInOutData) &&  $clockInOutData != false) ?  $clockInOutData->meal_hours : "N/A" ;
						$jobs['job_status'] = $jobData['status'];
						$result_array['record'][]    = $jobs;
					}

				$result_array['result_code'] = 0;
				$result_array['success_msg'] = $this->_common->langText($locale,'txt.get_dashboard_jobs.success');
			}else{
				$result_array['record']    = array();
				$result_array['result_code'] = 0;
				$result_array['success_msg'] = $this->_common->langText($locale,'txt.get_dashboard_jobs.norecords.success');
			}
		}else {
			$result_array['result_code'] = 1;
			$result_array['error_msg'] = $this->_common->langText($locale,'txt.change_user.error.session');
		}
		//echo "<pre>"; print_r($result_array); die;
		return $result_array;
	}

	function getJobClockInOutStatus($job_id,$user_accounts_id){
			$query = "SELECT clockInOutStatus
							FROM `jod_job_applicants`
							WHERE `job_id` = '$job_id' AND `applicant_user_account_id` = '$user_accounts_id'";
			$result =  $this->_db->my_query($query);
			if($this->_db->my_num_rows($result)>0){
				$data = $this->_db->my_fetch_object($result);
				return $data->clockInOutStatus;
			}else{
				return 0;
			}
	}

	function getJobClockInOutData($job_id,$user_accounts_id){
		$query = "SELECT jod_clockIn_clockOut.applicant_clockIn_dateTime,jod_clockIn_clockOut.applicant_clockOut_dateTime, jod_job_applicants.meal_hours, jod_job_applicants.insurance_included FROM `jod_clockIn_clockOut` JOIN jod_job_applicants ON jod_clockIn_clockOut.job_id = jod_job_applicants.job_id AND jod_clockIn_clockOut.applicant_user_account_id = jod_job_applicants.applicant_user_account_id
							WHERE jod_clockIn_clockOut.job_id = '$job_id' AND jod_clockIn_clockOut.applicant_user_account_id = '$user_accounts_id'";
			$result =  $this->_db->my_query($query);
			if($this->_db->my_num_rows($result)>0){
				$data = $this->_db->my_fetch_object($result);
				return $data;
			}else{
				return false;
			}
	}


	function getJobACKStatus($job_id, $user_accounts_id){
		$query = "SELECT ack_approval FROM `jod_job_applicants` WHERE `job_id` = '$job_id' AND `applicant_user_account_id` = '$user_accounts_id'";
		$result =  $this->_db->my_query($query);
		if($this->_db->my_num_rows($result)>0){
			$data = $this->_db->my_fetch_object($result);
			return $data->ack_approval;
		}else{
			return 0;
		}
	}

	function get_jobs(){
		$current_date = CURRENT_TIME;
		$locale = $this->_common->locale();
		$result_array  = array();
		if($this->_common->test_input($_REQUEST['secret_token']) != '' && $this->_common->check_session($_REQUEST['secret_token'])) {
			$secretToken  = $_REQUEST['secret_token'];
			if($this->_common->test_input($_REQUEST['job_status']) != '' ) {
				$userAccountId = $this->_common->check_session($secretToken);
				$applicant_data = $this->_common->get_profile($userAccountId);
				$jobStatus = $_REQUEST['job_status'];
				if($jobStatus <= 10) {
					$UserjobIdsData = $this->get_applied_jobIds_by_applicantId($userAccountId,$jobStatus);
					if(!empty($UserjobIdsData)){
						foreach ($UserjobIdsData as $key => $jobData){
							$jobs = array();
							$jobDetails = $this->get_job_details_by_jobId($jobData['jobId']);
							if(!empty($jobDetails)){
								$jobs['job_id'] 		= $jobDetails->job_id;
								$jobs['title'] 		 	= $jobDetails->job_title;
								$jobs['description'] 	= utf8_encode($jobDetails->description);
								$jobs['job_role'] 		= $jobDetails->job_type_id;
								$jobs['rate_per_hour'] 	= $jobDetails->payment_amount;
								$jobs['special_instructions']  = utf8_encode($jobDetails->special_instructions);
								$jobs['require_NEA_certified'] = $jobDetails->required_NEA_certificate;
								// start - added jhtang 20151204
//								$jobs['company'] 		= utf8_encode($jobDetails->company_name);
								$jobs['company']		= utf8_encode($jobDetails->outlet_name);
								// end - added jhtang 20151204
								$jobs['company_logo'] 	= OUTLET_LOGO_URL.$jobDetails->outlet_logo;
								$jobs['outlet_logo'] 	= OUTLET_LOGO_URL.$jobDetails->outlet_logo;
								$jobs['outlet_name'] 	= utf8_encode($jobDetails->outlet_name);
								$jobs['outlet_address'] = $jobDetails->outlet_address;
								$jobs['start_date'] 	= $jobDetails->start_date;
								$jobs['end_date'] 		= $jobDetails->end_date;
								$jobs['start_time'] 	= $jobDetails->start_time;
								$jobs['end_time'] 		= $jobDetails->end_time;
								$jobs['payment_method'] = $jobDetails->payment_mode;
								if( $jobDetails->payment_mode == 2){ // bank transfer
									$jobs['payment_method'] = 3 ;
								}
								if( $jobDetails->payment_mode == 3){ // cheque
									$jobs['payment_method'] = 2 ;
								}
								$clockInOutData	  = $this->getJobClockInOutData($jobData['jobId'],$userAccountId);
								$jobs["insurance_included"]	 = (!empty($clockInOutData) &&  $clockInOutData != false) ?  $clockInOutData->insurance_included : "N/A" ;
								$jobs["clock_in_dt"]	 = (!empty($clockInOutData) &&  $clockInOutData != false) ?  $clockInOutData->applicant_clockIn_dateTime : "N/A" ;
								$jobs["clock_out_dt"]	 = (!empty($clockInOutData) &&  $clockInOutData != false) ?  $clockInOutData->applicant_clockOut_dateTime : "N/A" ;
								$jobs["meal_hours"]	 = (!empty($clockInOutData) &&  $clockInOutData != false) ?  $clockInOutData->meal_hours : "N/A" ;
								$jobs['job_status'] = $jobData['status'];
								$result_array['record'][]    = $jobs;
							}
						}
						$result_array['result_code'] = 0;
						$result_array['success_msg'] = $this->_common->langText($locale,'txt.get_dashboard_jobs.success');
					}else{
						$result_array['result_code'] = 0;
						$result_array['success_msg'] = $this->_common->langText($locale,'txt.get_dashboard_jobs.success');
						$result_array['record'] = array();
					}
				}else{
					$result_array['result_code'] = 1;
					$result_array['error_msg'] = $this->_common->langText($locale,'txt.get_jobs.error.invalid_job_status');
				}
			}else{ /*  job status should not be blank */
				$result_array['result_code'] = 1;
				$result_array['error_msg'] = $this->_common->langText($locale,'txt.register.error.required');
			}
		}else{
			$result_array['result_code'] = 1;
			$result_array['error_msg'] = $this->_common->langText($locale,'txt.change_user.error.session');
		}
	//	echo "<pre>"; print_r($result_array); die;
		return $result_array;
	}


	function apply_job(){
		$current_date 	= CURRENT_TIME;
		$locale 		= $this->_common->locale();
		$result_array  	= array();
		if($this->_common->test_input($_REQUEST['secret_token']) != '' && $this->_common->check_session($_REQUEST['secret_token'])) {
			$secretToken  = $_REQUEST['secret_token'];
			if($this->_common->test_input($_REQUEST['job_id']) != '' && $this->_common->test_input($_REQUEST['latitude']) && $this->_common->test_input($_REQUEST['longitude']) && $this->_common->test_input($_REQUEST['address'])) {
				if(isset($_REQUEST["insurance_included"])) {
					$insurance_included  = $_REQUEST["insurance_included"];
					/* CR 3 changes by Kratika */
					if($insurance_included == 1 || $insurance_included == 0) {
						$jobId 				= $_REQUEST['job_id'];
						$check_job_query 	= "SELECT * FROM `jod_jobs` WHERE `job_id` = '$jobId' AND `is_approved` = 1 AND `is_applicants_hired` = 0";
						$job_data 			= $this->_db->my_query($check_job_query);
						$job_arr 			= $this->_db->my_fetch_object($job_data);
						// if job is approved
						if(!empty($job_arr)){
							$lattitude = $_REQUEST['latitude'];
							$longitude = $_REQUEST['longitude'];
							$address   = $_REQUEST['address'];
							$userAccountId  = $this->_common->check_session($secretToken);
							$profileDetail  = $this->_common->get_profile($userAccountId);
							$update 		= $this->update_applicants_location($lattitude,$longitude,$address,$userAccountId);
							$check_query    = "SELECT * FROM `".DB_PREFIX."job_applicants` WHERE `job_id` = '$jobId' AND `applicant_user_account_id` = '$userAccountId' ";
							$resultdata     = $this->_db->my_query($check_query);
							$data = $this->_db->my_fetch_object($resultdata);
							if(empty($data) == true) {
								$applyJobQuery = "INSERT INTO `".DB_PREFIX."job_applicants` (`job_id`, `applicant_user_account_id`, `applied_at`, `status`, `created_at`, `updated_at`, `ack_approval`,`insurance_included`) VALUES ('$jobId','$userAccountId' , '$current_date', '0', '$current_date', '$current_date', '1','$insurance_included')";
								if($this->_db->my_query($applyJobQuery) == true && $update == true){
									$jobAppliedId = mysql_insert_id();
									$this->sendEmailNotification($jobId, $userAccountId,'appliedforjob'); //Added by NS 25-02-2016
									$result_array['result_code'] = 0;
									$result_array['success_msg'] = $this->_common->langText($locale,'txt.apply_job.success');
								}else{
									$result_array['result_code'] = 1;
									$result_array['error_msg'] = $this->_common->langText($locale,'txt.apply_job.error');
								}
							}else{
								if($data->is_delete == 1){
									$update_query = "UPDATE `".DB_PREFIX."job_applicants` SET is_delete = 0 WHERE applicant_user_account_id= '$userAccountId' AND job_id = '$jobId'";
									if($this->_db->my_query($update_query) == true){
										//rsolanki - Reapply job
										$this->sendEmailNotification($jobId, $userAccountId,'re_appliedforjob'); //Added by rsolanki 28-04-2016
										$result_array['result_code'] = 0;
										$result_array['success_msg'] = $this->_common->langText($locale,'txt.apply_job.success');
									}
								}else{
									$result_array['result_code'] = 1;
									$result_array['error_msg'] = $this->_common->langText($locale,'txt.already_apply.error.required');
								}
							}
						}else{
							$check_j_query = "SELECT * FROM `jod_jobs` WHERE `job_id` = '$jobId'";
							$job_data = $this->_db->my_query($check_j_query);
							$job_arr  = $this->_db->my_fetch_object($job_data);
							if($job_arr->is_applicants_hired == 1) {
								$result_array['result_code'] = 1;
								$result_array['error_msg'] = $this->_common->langText($locale,'txt.job_applicants_hired.error.required');
							}else{
								$result_array['result_code'] = 1;
								$result_array['error_msg'] = $this->_common->langText($locale,'txt.job_not_approved.error.required');
							}
						}
					}else{
						/* insurance included is valid else part */
						$result_array['result_code'] = 1;
						$result_array['error_msg'] = $this->_common->langText($locale,'txt.already_apply.insurance_included.error.valid');
					}
				}else{ /* insurance included else part */
					$result_array['result_code'] = 1;
					$result_array['error_msg'] = $this->_common->langText($locale,'txt.already_apply.insurance_included.error.required');
				}
			}else{
				$result_array['result_code'] = 1;
				$result_array['error_msg'] = $this->_common->langText($locale,'txt.register.error.required');
			}
		}
		else {
			$result_array['result_code'] = 1;
			$result_array['error_msg'] = $this->_common->langText($locale,'txt.change_user.error.session');
		}

		return $result_array;
	}

	function get_job_detail(){
		$locale = $this->_common->locale();
		$result_array  = array();
		if($this->_common->test_input($_REQUEST['secret_token']) != '' && $this->_common->check_session($_REQUEST['secret_token'])) {
			$secretToken  = $_REQUEST['secret_token'];
			if($this->_common->test_input($_REQUEST['job_id']) != '' ) {
				$jobs 	= array();
				$jobId 	= $_REQUEST['job_id'];
				$jobDetails 	= 	$this->get_job_details_by_jobId($jobId);
				$jobs['job_id'] = $jobDetails->job_id;
				$jobs['title'] 	= $jobDetails->job_title;
				$jobs['description'] 	= utf8_encode($jobDetails->description);
				$jobs['job_role'] 	 	= $jobDetails->job_type_id;
				$jobs['rate_per_hour'] 	= $jobDetails->payment_amount;
				//For now hard code already put query to the client
				$jobs['special_instructions']  = utf8_encode($jobDetails->special_instructions);
				$jobs['require_NEA_certified'] = $jobDetails->required_NEA_certificate;
				// start - added jhtang 20151204
//				$jobs['company'] 		= utf8_encode($jobDetails->company_name);
				$jobs['company']		= utf8_encode($jobDetails->outlet_name);
				// end - added jhtang 20151204
				$jobs['company_logo'] 	= OUTLET_LOGO_URL.$jobDetails->outlet_logo;
				$jobs['outlet_logo'] 	= OUTLET_LOGO_URL.$jobDetails->outlet_logo;
				$jobs['outlet_name'] 	= utf8_encode($jobDetails->outlet_name);
				$jobs['outlet_address'] = $jobDetails->outlet_address;
				$jobs['start_date'] 	= $jobDetails->start_date;
				$jobs['end_date'] 		= $jobDetails->end_date;
				$jobs['start_time'] 	= $jobDetails->start_time;
				$jobs['end_time'] 		= $jobDetails->end_time;
				$jobs['payment_method'] = $jobDetails->payment_mode;
				if( $jobDetails->payment_mode == 2){ // bank transfer
					$jobs['payment_method'] = 3 ;
				}

				if( $jobDetails->payment_mode == 3){ // cheque
					$jobs['payment_method'] = 2 ;
				}
				// start - added by jhtang 20160220
				$userAccountId = $this->_common->check_session($secretToken);
				$jobs['job_status'] = $this->get_applicant_job_status($jobId, $userAccountId);
				// end - added by jhtang 20160220
				$clockInOutData	  = $this->getJobClockInOutData($jobId,$userAccountId);
				$jobs["insurance_included"]	 = (!empty($clockInOutData) &&  $clockInOutData != false) ?  $clockInOutData->insurance_included : "N/A" ;
				$jobs["clock_in_dt"]	 = (!empty($clockInOutData) &&  $clockInOutData != false) ?  $clockInOutData->applicant_clockIn_dateTime : "N/A" ;
				$jobs["clock_out_dt"]	 = (!empty($clockInOutData) &&  $clockInOutData != false) ?  $clockInOutData->applicant_clockOut_dateTime : "N/A" ;
				$jobs["meal_hours"]	 = (!empty($clockInOutData) &&  $clockInOutData != false) ?  $clockInOutData->meal_hours : "N/A" ;


				$result_array["result"] = $jobs;
				$result_array['result_code'] = 0;
				$result_array['success_msg'] = $this->_common->langText($locale,'txt.get_job_detail.success');
			}else{
				$result_array['result_code'] = 1;
				$result_array['error_msg'] = $this->_common->langText($locale,'txt.register.error.required');
			}
		}else{
			$result_array['result_code'] = 1;
			$result_array['error_msg'] = $this->_common->langText($locale,'txt.change_user.error.session');
		}
		return $result_array;
	}

	// start - added by jhtang 20160220
	function get_applicant_job_status($job_id, $userAccountId) {
		$jod_jobs_data = $this->get_job_details_by_jobId($job_id);

		// if manager has cancelled job ... job_status = 11;  // needs to change to another number
		if ($jod_jobs_data->is_delete == 1) {
			return 11;
		}

		// if applicant has applied for the job
		$jod_applicants_query = "SELECT `is_delete`, `acknowledged`, `completed_by_applicant`, `completed_by_manager`, `status` FROM `".DB_PREFIX."job_applicants` WHERE applicant_user_account_id= '$userAccountId' AND job_id = '$job_id'";
		$jod_applicants_result  = $this->_db->my_query($jod_applicants_query);
		if($this->_db->my_num_rows($jod_applicants_result) > 0) {
			$jod_applicants_data = $this->_db->my_fetch_object($jod_applicants_result);

			// if applicant has cancelled the job ... job_status = 2;
			if ($jod_applicants_data->is_delete == 1) {
				if ($jod_jobs_data->is_applicants_hired == 1) return 12;  // job has started
				else return 2;  // job has not started, so can reapply
			}

			// if manager has not approve ... job_status = 3;
			if ($jod_applicants_data->status == 0) {
				return 3;
			}

			// if applicant has been rejected .. job_status = 7;
			if ($jod_applicants_data->status == 2) {
				return 7;
			}

			// assumption for below is applicant is selected by manager - is_applicants_hired

			$clockInOutStatus = $this->getJobClockInOutStatus($job_id,$userAccountId);
			// if applicant has completed the clock out .. job_status = 6;
			if ($clockInOutStatus == 2) {
				return 6;
			}

			// if applicant has completed the clock in ... job_status = 10;
			if ($clockInOutStatus == 1) {
				return 10;
			}

			// if manager has completed the job for applicant and yet no clock in or clock out
			if ($jod_applicants_data->completed_by_manager) {
				return 7;  // no show ... should distinguish a new status between rejected
			}

			$jobACKStatus = $this->getJobACKStatus($job_id, $userAccountId);
			// if applicant has not done 1st or 2nd ack
			if ($jod_applicants_data->acknowledged == 0 || $jobACKStatus == 2 || $jobACKStatus == 1) {
				return 4;
			}
			// if applicant has completed 1st or 2nd ack
			if ($jod_applicants_data->acknowledged == 1 || $jobACKStatus == 3 || $jobACKStatus == 4) {
				return 5;
			}

			// should not reach here
			// applicant is hired and yet we don't know if he has acknowledge or has clock in or out
			return 0;
		}

		// applicant didn't apply for job ... but we need to check if job already started
		if ($jod_jobs_data->is_applicants_hired == 1) return 12;  // job has started
		return 2; // brand new job
	}
	// end - added by jhtang 20160220


	function get_job_detail_return_status($secret_token,$jobId){
		$locale = $this->_common->locale();
		$result_array  = array();
		if($this->_common->test_input($secret_token) != '' && $this->_common->check_session($secret_token)) {
			$secretToken  = $secret_token;
			if($this->_common->test_input($jobId) != '' ) {
				$jobs 	= array();
				$jobId 	= $jobId;
				$jobDetails 	= 	$this->get_job_details_by_jobId($jobId);
				$jobs['job_id'] = $jobDetails->job_id;
				if( $jobDetails->payment_mode == 2){ // bank transfer
					$jobs['payment_method'] = 3 ;
				}

				if( $jobDetails->payment_mode == 3){ // cheque
					$jobs['payment_method'] = 2 ;
				}

				// start - added by jhtang 20160220
				$userAccountId = $this->_common->check_session($secretToken);
				$UserjobIdsData = $this->get_applied_jobIds_by_applicantId($userAccountId, 1);  // all jobs of user

				//rsolanki - check clock in and clock out time status
				$clockInOutStatus = $this->getJobClockInOutStatus($jobId,$userAccountId);
				if($clockInOutStatus==1){
					$jobs['job_status'] = 10;
				}else if($clockInOutStatus==2){
					$jobs['job_status'] = 6;
				}else{
					//rsolanki - Check ACK status
					$jobACKStatus = $this->getJobACKStatus($jobId,$userAccountId);
					if($jobACKStatus==2  || $jobACKStatus==1){
						$jobs['job_status'] = 4;
					}else if($jobACKStatus==3 || $jobACKStatus==4){
						$jobs['job_status'] = 5;
					}else{
						if($UserjobIdsData == false) $jobs['job_status'] = 2;  // open job
						else {
							// get all jobs
							foreach ($UserjobIdsData as $key => $jobData){
								if($jobDetails->is_completed==1){
									$jobs['job_status'] = 6;
								}else{
									if ($jobData['jobId'] == $jobDetails->job_id) {
										$jobs['job_status'] = $jobData['status'];
										break;
									}
								}
							}
						}
					}
				}
			}
		}
		if($jobs['job_status'] != ''){
			return $jobs['job_status'];
		}else{
			return '';
		}
	}

	function get_applied_jobIds_by_applicantId($applicantUserAccountId,$status=""){
		$current_date = CURRENT_TIME;
		$resData = array();
		if($status == ""){
			$jobIdsQuery  = "SELECT * FROM `".DB_PREFIX."job_applicants`
									WHERE `applicant_user_account_id` = '$applicantUserAccountId' AND is_delete = 0 AND status != 2";
			$resultdata = $this->_db->my_query($jobIdsQuery);

			if($this->_db->my_num_rows($resultdata) > 0){
				while($row = $this->_db->my_fetch_object($resultdata)){
					//echo "<pre>"; print_r($row);
					$jodData 			= array();
					$jodData['jobId'] 	= $row->job_id;
					if($row->status == 0 && $row->acknowledged == 0 && $row->is_delete == 0 && $row->clockInOutStatus == 0 && $row->completed_by_applicant == 0 ){
						$jodData['status'] 	= 3;
					}
					if($row->status == 1 && $row->acknowledged == 0 && $row->is_delete == 0 && $row->clockInOutStatus == 0 && $row->completed_by_applicant == 0){
						$jodData['status'] 	= 4;
					}
					if($row->status == 1 && $row->acknowledged == 1 && $row->is_delete == 0 && $row->clockInOutStatus == 0 && $row->completed_by_applicant == 0) {
						$jodData['status'] 	= 5;
					}
					if($row->status == 1 && $row->acknowledged == 1 && $row->is_delete == 0 && ($row->clockInOutStatus == 1 || $row->clockInOutStatus == 2)){
						$jodData['status'] 	= 10;
					}
					$resData[] = $jodData;
				}
				//print_r($resData); die;
			}
		}else{
			if($status == 1){  // This is for all jobs
				$jobIdsQuery  = "SELECT * FROM `".DB_PREFIX."job_applicants`
										WHERE `applicant_user_account_id` = '$applicantUserAccountId'
										";
				$resultdata = $this->_db->my_query($jobIdsQuery);
				if($this->_db->my_num_rows($resultdata) > 0){
					$jobIDs = array();

					while($row = $this->_db->my_fetch_object($resultdata)){
						$jodData = array();
						//array_push($jobIDs,$row->job_id);
						$jodData['jobId'] = $row->job_id;
						if($row->status == 0 && $row->acknowledged == 0 && $row->completed_by_applicant == 0 && $row->clockInOutStatus == 0 ) {  // pending approval
							$jodData['status'] = 3;
						}
						if($row->status == 1 && $row->acknowledged == 0 && $row->completed_by_applicant == 0  && $row->clockInOutStatus == 0) { // approved
							$jodData['status'] = 4;
						}
						if($row->status == 1 && $row->acknowledged == 1 && $row->completed_by_applicant == 0 && $row->clockInOutStatus == 0) { // acknowledged
							$jodData['status'] = 5;
						}
						if($row->status == 2) { // rejected
							$jodData['status'] = 7;
						}
						if($row->status == 1 && $row->acknowledged == 1 && $row->completed_by_applicant == 1 && $row->clockInOutStatus == 2){  //completed
							$jodData['status'] = 6;
						}
						if($row->is_delete == 1 ){  //user cacelled
							$jodData['status'] = 8;
						}
						/*	if($row->is_delete == 0 || $row->completed_by_applicant == 1 ||  $row->status == 2 ){  //user cacelled, compelted , rejected
							$jodData['status'] = 9;
						} */
						if($row->status == 1 && $row->acknowledged == 1 && $row->completed_by_applicant == 0 && $row->clockInOutStatus == 1){  // clocked in
							$jodData['status'] = 10;
						}
						$resData[] = $jodData;
						array_push($jobIDs,$row->job_id);
					}

				}
				if(!empty($jobIDs)){
					$jobIDsStr = implode(",",$jobIDs);
					$openJobQuery  = "SELECT job_id FROM `".DB_PREFIX."jobs`
											WHERE `job_id` NOT IN ($jobIDsStr)
											AND `is_completed` = '0'
											AND `is_approved` = '1'
											AND `is_delete` = '0'
											AND CONCAT(start_date, ' ', start_time) >= '$current_date' ";
				}else{
					$openJobQuery  = "SELECT job_id FROM `".DB_PREFIX."jobs`
											WHERE `is_completed` ='0'
											AND `is_approved` = '1'
											AND `is_delete` = '0'
					 						AND CONCAT(start_date, ' ', start_time) >= '$current_date'";
				}
				//echo $openJobQuery; die;
				$openJobdata = $this->_db->my_query($openJobQuery);

				if($this->_db->my_num_rows($openJobdata) > 0){
					$jobIDs = array();
					while($row = $this->_db->my_fetch_object($openJobdata)){
						$jodData = array();
						$jodData['jobId'] = $row->job_id;
						$jodData['status'] = 2;   // job status 2 for open job
						array_push($resData,$jodData);
					}
				}
			}
			if($status == 2) { // open jobs are the jobs for which user can apply
				$jobIdsQuery  = "SELECT * FROM `".DB_PREFIX."job_applicants`
										WHERE `applicant_user_account_id` = '$applicantUserAccountId'
										AND `is_delete` = 0 ";
				$resultdata = $this->_db->my_query($jobIdsQuery);
				if($this->_db->my_num_rows($resultdata) > 0){
					$jobIDs = array();
					while($row = $this->_db->my_fetch_object($resultdata)){
						$jodData = array();
						array_push($jobIDs,$row->job_id);
					}
				}
				if(!empty($jobIDs)){
					$jobIDsStr = implode(",",$jobIDs);
					$jobIdsQuery  = "SELECT job_id FROM `".DB_PREFIX."jobs`
									WHERE `job_id` NOT IN ($jobIDsStr)
									AND `is_completed` = '0'
									AND `is_approved` = '1'
									AND `is_delete` = '0'
									AND `is_applicants_hired` = '0'
									AND CONCAT(start_date, ' ', start_time) >= '$current_date' ";
				}else{
					$jobIdsQuery  = "SELECT job_id FROM `".DB_PREFIX."jobs`
									WHERE `is_completed` ='0'
									AND `is_approved` = '1'
									AND `is_delete` = '0'
									AND `is_applicants_hired` = '0'
									AND CONCAT(start_date, ' ', start_time) >= '$current_date'";
				}
				$resultdata = $this->_db->my_query($jobIdsQuery);
				if($this->_db->my_num_rows($resultdata) > 0){
					$jobIDs = array();
					while($row = $this->_db->my_fetch_object($resultdata)){
						$jodData = array();
						$jodData['jobId'] = $row->job_id;
						$jodData['status'] = 2;   // job status 10 for open job
						array_push($resData,$jodData);
					}
				}
			}
			if($status == 3) { // pending approval
				$jobIdsQuery  = "SELECT * FROM `".DB_PREFIX."job_applicants`
								WHERE `applicant_user_account_id` = '$applicantUserAccountId'
								AND status = 0
								AND `is_delete` = '0' ";
				$resultdata = $this->_db->my_query($jobIdsQuery);
				if($this->_db->my_num_rows($resultdata) > 0){
					$jobIDs = array();
					while($row = $this->_db->my_fetch_object($resultdata)){
						$jodData = array();
						$jodData['jobId'] = $row->job_id;
						$jodData['status'] = 3;   // job status 10 for open job
						array_push($resData,$jodData);
					}
				}
			}
			if($status == 4){ // approved
				$jobIdsQuery  = "SELECT * FROM `".DB_PREFIX."job_applicants`
									WHERE `applicant_user_account_id` = '$applicantUserAccountId'
									AND status = 1
									AND `is_delete` = '0'
									AND `completed_by_applicant` = '0 '   ";
				$resultdata = $this->_db->my_query($jobIdsQuery);
				if($this->_db->my_num_rows($resultdata) > 0){
					$jobIDs = array();
					while($row = $this->_db->my_fetch_object($resultdata)){
						$jodData = array();
						$jodData['jobId'] = $row->job_id;
						$jodData['status'] = 4;   // job status 10 for open job
						array_push($resData,$jodData);
					}
				}
			}
			if($status == 5){ // Acknowledged
				$jobIdsQuery  = "SELECT * FROM `".DB_PREFIX."job_applicants`
								WHERE `applicant_user_account_id` = '$applicantUserAccountId'
								AND acknowledged = 1
								AND is_delete = '0'";
				$resultdata = $this->_db->my_query($jobIdsQuery);
				if($this->_db->my_num_rows($resultdata) > 0){
					$jobIDs = array();
					while($row = $this->_db->my_fetch_object($resultdata)){
						$jodData = array();
						$jodData['jobId'] = $row->job_id;
						$jodData['status'] = 5;   // job status 10 for open job
						array_push($resData,$jodData);
					}
				}
			}
			if($status == 6){ // completed jobs
				$jobIdsQuery  = "SELECT * FROM `".DB_PREFIX."job_applicants`
								WHERE `applicant_user_account_id` = '$applicantUserAccountId'
								AND completed_by_applicant = 1
								AND is_delete = '0'";
				$resultdata = $this->_db->my_query($jobIdsQuery);
				if($this->_db->my_num_rows($resultdata) > 0){
					$jobIDs = array();
					while($row = $this->_db->my_fetch_object($resultdata)){
						$jodData = array();
						$jodData['jobId'] = $row->job_id;
						$jodData['status'] = 6;   // job status 10 for open job
						array_push($resData,$jodData);
					}
				}
			}
			if($status == 7){ // Rejected
				$jobIdsQuery  = "SELECT * FROM `".DB_PREFIX."job_applicants`
							WHERE `applicant_user_account_id` = '$applicantUserAccountId'
							AND status = 2
							AND is_delete = '0'";
				$resultdata = $this->_db->my_query($jobIdsQuery);
				if($this->_db->my_num_rows($resultdata) > 0){
					$jobIDs = array();
					while($row = $this->_db->my_fetch_object($resultdata)){
						$jodData = array();
						$jodData['jobId'] = $row->job_id;
						$jodData['status'] = 7;   // job status 10 for open job
						array_push($resData,$jodData);
					}
				}
			}
			/* Added by Kratika Jain on date 30 July START */
			if($status == 8){ // User Cancelled
				$jobIdsQuery  = "SELECT * FROM `".DB_PREFIX."job_applicants`
							WHERE `applicant_user_account_id` = '$applicantUserAccountId'
							AND status = '1'
							AND is_delete = '1'";
				$resultdata = $this->_db->my_query($jobIdsQuery);
				if($this->_db->my_num_rows($resultdata) > 0){
					$jobIDs = array();
					while($row = $this->_db->my_fetch_object($resultdata)){
						$jodData = array();
						$jodData['jobId'] = $row->job_id;
						$jodData['status'] = 8;   // job status 10 for open job
						array_push($resData,$jodData);
					}
				}
			}
			if($status == 9){ // completed, Rejected, User Cancelled
				$jobIdsQuery  = "SELECT * FROM `".DB_PREFIX."job_applicants`
							WHERE `applicant_user_account_id` = '$applicantUserAccountId'
							AND status = '2'
							OR is_delete = '1' OR completed_by_applicant = 1 ";
				$resultdata = $this->_db->my_query($jobIdsQuery);
				if($this->_db->my_num_rows($resultdata) > 0){
					$jobIDs = array();
					while($row = $this->_db->my_fetch_object($resultdata)){
						$jodData = array();
						$jodData['jobId'] = $row->job_id;
						$jodData['status'] = 9;   // job status 9 for open job
						array_push($resData,$jodData);
					}
				}
			}
			if($status == 10) {
				$jobIdsQuery = "SELECT *
							FROM `jod_job_applicants` WHERE
							`applicant_user_account_id` = '$applicantUserAccountId' AND `clockInOutStatus` = '1' AND `is_delete` = '0' AND status = '1'";
				$resultdata = $this->_db->my_query($jobIdsQuery);
				if($this->_db->my_num_rows($resultdata) > 0){
					$jobIDs = array();
					while($row = $this->_db->my_fetch_object($resultdata)){
						$jodData = array();
						$jodData['jobId'] = $row->job_id;
						$jodData['status'] = 10;   // job status 9 for open job
						array_push($resData,$jodData);
					}
				}
			}
			/* Added by Kratika Jain on date 30 July END */
		}
		//print_r($resData); die;
		return $resData;
	}

	function get_job_details_by_jobId($jobId){
		$resData = array();
		 $jobIdsQuery  	= "SELECT ".DB_PREFIX."jobs.* , ".DB_PREFIX."outlets.outlet_name, ".DB_PREFIX."outlets.outlet_logo, ".DB_PREFIX."outlets.outlet_address , ".DB_PREFIX."companies.company_name , ".DB_PREFIX."companies.company_logo FROM `".DB_PREFIX."jobs` INNER JOIN ".DB_PREFIX."outlets ON ".DB_PREFIX."jobs.outlet_id= ".DB_PREFIX."outlets.outlet_id JOIN `".DB_PREFIX."companies` ON ".DB_PREFIX."outlets.company_id = ".DB_PREFIX."companies.company_id WHERE job_id = '$jobId'" ;
		$resultdata 	= $this->_db->my_query($jobIdsQuery);
		$jobData 		= $this->_db->my_fetch_object($resultdata);
		//print_r($jobData); die;
		return $jobData;
	}

	function update_applicants_location($lattitude,$longitude,$address,$applicantsUserAccountId){
			$current_date = CURRENT_TIME;
			$updateArray  = array("latitude" => $lattitude,"longitude" => $longitude, "address"=>$address,"updated_at"=>$current_date);
			$whereClause  = array("user_account_id" => $applicantsUserAccountId);
			$update 	  = $this->_db->UpdateAll(DB_PREFIX."applicants", $updateArray, $whereClause, "");
			return $update;
	}


	function update_applicants_device_details($device_id,$device_type,$applicantsUserAccountId){
			$current_date = CURRENT_TIME;
			$updateArray  = array("device_id" => $device_id,"device_type" => $device_type,"updated_at"=>$current_date);
			$whereClause  = array("user_accounts_id" => $applicantsUserAccountId);
			$update 	  = $this->_db->UpdateAll(DB_PREFIX."user_accounts", $updateArray, $whereClause, "");
			return $update;
	}

	function sendEmailNotification($jobId, $userAccountId ,$purpose){
		$jobDetails = 	$this->get_job_details_by_jobId($jobId);
		//print_r($jobDetails); die;
		$type = ($jobDetails->job_created_by  == 0 )?'5':'3';
		$date = date('Y-m-d H:i:s');
		if($purpose=='acknowledge'){
			$subject = 'User acknowledge';
		}else if($purpose=='feedback'){
			$subject = 'User feedback';
		}else if($purpose=='appliedforjob'){
			$subject = 'Job Approval Notification';
		}
		$completeJobQuery = "INSERT INTO `".DB_PREFIX."email_notifications` (`job_id`, `sender_id`, `sender_user_type`, `receiver_id`,
							`receiver_user_type`,`purpose`,`subject`,`created_at`,`is_mail_sent`)
							VALUES ('$jobDetails->job_id','$userAccountId' , '4', '$jobDetails->created_by', '$type', '$purpose','$subject','$date','0')";
							//echo $completeJobQuery; die;
		$inserted = $this->_db->my_query($completeJobQuery) ;
	}

	function acknowledge_job(){
		$current_date 	= CURRENT_TIME;
		$locale 		= $this->_common->locale();
		$result_array  	= array();
		$cdateTime 		= date('Y-m-d H:i:s');
		if($this->_common->test_input($_REQUEST['secret_token']) != '' && $this->_common->check_session($_REQUEST['secret_token'])) {
			$secretToken  = $_REQUEST['secret_token'];
			if($this->_common->test_input($_REQUEST['job_id']) != '' && $this->_common->test_input($_REQUEST['latitude']) && $this->_common->test_input($_REQUEST['longitude']) && $this->_common->test_input($_REQUEST['address'])) {
				$jobId 		= $_REQUEST['job_id'];
				$lattitude 	= $_REQUEST['latitude'];
				$longitude 	= $_REQUEST['longitude'];
				$address 	= $_REQUEST['address'];
				$expect_arrive_time = '';
				if($_REQUEST['expect_arrive_time'] != ''){
					$expect_arrive_time = $_REQUEST['expect_arrive_time'];
				}

				$jobDetails = 	$this->get_job_details_by_jobId($jobId);
				if($jobDetails->is_completed == 0){
					$userAccountId 	= $this->_common->check_session($secretToken);
					$profileDetail 	= $this->_common->get_profile($userAccountId);
					$chk_query 		= "SELECT * FROM `".DB_PREFIX."job_applicants`
													WHERE `job_id` = '$jobId'
													AND `applicant_user_account_id` = '$userAccountId'
													AND `status` = '1'";
					$chk_data = $this->_db->my_query($chk_query);
					if($this->_db->my_num_rows($chk_data) > 0) {
						$update_location = $this->update_applicants_location($lattitude,$longitude,$address,$userAccountId);
						$check_query  	= "SELECT * FROM `".DB_PREFIX."job_applicants` WHERE `job_id` = '$jobId' AND `applicant_user_account_id` = '$userAccountId' AND `acknowledged` = '1'";
						$resultdata 	= $this->_db->my_query($check_query);
						$data 			= $this->_db->my_fetch_object($resultdata);
						if(empty($data)){
							//Update clock in Date time image path - rsolanki
							//$clockInImg 	= '';
							//$clockInImgText = 'Clock In Date Time: '.$cdateTime;
							//$clockInImg 	= $this->createQrImg($clockInImgText);

							if(!empty($expect_arrive_time)){
								$update_query = "UPDATE `".DB_PREFIX."job_applicants`
															SET acknowledged = 1, expect_arrive_time = '$expect_arrive_time'
															WHERE applicant_user_account_id= '$userAccountId'
															AND job_id = '$jobId'";
							}else{
								$update_query = "UPDATE `".DB_PREFIX."job_applicants`
														SET acknowledged = 1
														WHERE applicant_user_account_id= '$userAccountId'
														AND job_id = '$jobId'";
							}
							if($update_location == true && $this->_db->my_query($update_query) == true ){
								$this->sendEmailNotification($jobId, $userAccountId,'acknowledge');
								$result_array['result_code'] = 0;
								$result_array['success_msg'] = $this->_common->langText($locale,'txt.acknowledge_job.success');
							}else{
								$result_array['result_code'] = 1;
								$result_array['error_msg'] = $this->_common->langText($locale,'txt.apply_job.error');
							}
							//update ack_approval
							$checkAckApproval = "SELECT ack_approval
													FROM`".DB_PREFIX."job_applicants`
														WHERE `job_id` = '$jobId' AND applicant_user_account_id= '$userAccountId'";
							$checkAckApprovalobj= $this->_db->my_query($checkAckApproval);
							$rowACK 			= $this->_db->my_fetch_object($checkAckApprovalobj);
							$ack_approval		= $rowACK->ack_approval;
							if($ack_approval==1){
								$update_query = "UPDATE `".DB_PREFIX."job_applicants` SET ack_approval = 3 WHERE job_id = '$jobId' AND applicant_user_account_id= '$userAccountId'";
								$this->_db->my_query($update_query);
							}else if($ack_approval==2){
								$update_query = "UPDATE `".DB_PREFIX."job_applicants` SET ack_approval = 4 WHERE job_id = '$jobId' AND applicant_user_account_id= '$userAccountId'";
								$this->_db->my_query($update_query);
							}
						}else{
							//ACK with 2 job ACK
							$checkAckApproval = "SELECT ack_approval
													FROM`".DB_PREFIX."job_applicants`
														WHERE `job_id` = '$jobId' AND applicant_user_account_id= '$userAccountId'";
							$checkAckApprovalobj= $this->_db->my_query($checkAckApproval);
							$rowACK 			= $this->_db->my_fetch_object($checkAckApprovalobj);
							$ack_approval		= $rowACK->ack_approval;
							if($ack_approval==2){
								$update_query = "UPDATE `".DB_PREFIX."job_applicants` SET ack_approval = 4 WHERE job_id = '$jobId' AND applicant_user_account_id= '$userAccountId'";
								$this->_db->my_query($update_query);

								$result_array['result_code'] = 0;
								$result_array['success_msg'] = $this->_common->langText($locale,'txt.acknowledge_job.success');
							}else{

								$result_array['result_code'] = 1;
								$result_array['error_msg'] = $this->_common->langText($locale,'txt.already_acknowledge.error.required');
							}
						}
					}else{
						$result_array['result_code'] = 1;
						$result_array['error_msg'] = $this->_common->langText($locale,'txt.not_apply.error.required');
					}
				}else{
					$result_array['result_code'] = 1;
					$result_array['error_msg'] = $this->_common->langText($locale,'txt.acknowledged_job.error.complete');
				}
			}else{
				$result_array['result_code'] = 1;
				$result_array['error_msg'] = $this->_common->langText($locale,'txt.register.error.required');
			}
		}
		else {
			$result_array['result_code'] = 1;
			$result_array['error_msg'] = $this->_common->langText($locale,'txt.change_user.error.session');
		}
		return $result_array;
	}

	function complete_job(){
		$current_date 	= CURRENT_TIME;
		$locale 		= $this->_common->locale();
		$result_array  	= array();
		$cdateTime 		= date('Y-m-d H:i:s');
		$secretToken  = $_REQUEST['secret_token'];
		if($this->_common->test_input($_REQUEST['secret_token']) != '' && $this->_common->check_session($_REQUEST['secret_token'])){ // session error if
			if($this->_common->test_input($_REQUEST['job_id']) != '' && $this->_common->test_input($_REQUEST['feedback']) && $this->_common->test_input($_REQUEST['rating']) && $this->_common->test_input($_REQUEST['clock_out_dt']) && $this->_common->test_input($_REQUEST['bank_name']) && $this->_common->test_input($_REQUEST['bank_account_name']) && $this->_common->test_input($_REQUEST['bank_account_number']) && $this->_common->test_input($_REQUEST["applicant_id"]) && $this->_common->test_input($_REQUEST["clock_in_dt"]) && $this->_common->test_input($_REQUEST["meal_hours"])) { // required field if
				$jobId 				= $_REQUEST['job_id'];
				$feedback 			= $_REQUEST['feedback'];
				$rating 			= $_REQUEST['rating'];
				$applicant_id 		= $_REQUEST["applicant_id"];
				$clock_out_dt 		= $_REQUEST['clock_out_dt'];
				$bank_name 			= $_REQUEST['bank_name'];
				$bank_account_name 	= $_REQUEST['bank_account_name'];
				$bank_account_number= $_REQUEST['bank_account_number'];
				$clock_in_dt		= $_REQUEST["clock_in_dt"];
				$meal_hours			= $_REQUEST["meal_hours"];
				if($this->_common->check_session($_REQUEST['secret_token']) == $applicant_id) { //to check applicant id in the Qr code is same */
					if($rating <= 5) {
						//rsolanki
						$selectApplicantId = "SELECT ua.user_accounts_id
								FROM `".DB_PREFIX."user_accounts` as ua
								LEFT JOIN `".DB_PREFIX."applicants` as ap ON ua.user_accounts_id=ap.user_account_id
								WHERE ua.`secret_token` = '$secretToken'";
						$selectApplicantId  = $this->_db->my_query($selectApplicantId);
						$row 			 	= $this->_db->my_fetch_object($selectApplicantId);
						$user_accounts_id	= $row->user_accounts_id;
						if($user_accounts_id){
							//Get start date info
							$selectApplicant  	= "SELECT * FROM `".DB_PREFIX."clockIn_clockOut`
													WHERE `job_id`  = '$jobId'
													AND `applicant_user_account_id` = '$user_accounts_id'";
							$resultApplicant 	= $this->_db->my_query($selectApplicant);
							if ($this->_db->my_num_rows($resultApplicant) > 0) {
								$cloclInOutrow = $this->_db->my_fetch_object($resultApplicant);
								//Check clock out status
								if($cloclInOutrow->applicant_clockOut_status==0){
									//check clock in status
									if($cloclInOutrow->applicant_clockIn_status==0){
										$result_array['result_code'] = 1;
										$result_array['error_msg'] = $this->_common->langText($locale,'txt.jobNotStarted.error');
									}else{
										//check clock out in date time
										if($clock_out_dt != $cloclInOutrow->clockout_current_dateTime){
											$result_array['result_code'] = 1;
											$result_array['error_msg'] = $this->_common->langText($locale,'txt.clockoutDateNotMatch.error');
										}
										elseif($cloclInOutrow->applicant_clockIn_dateTime != $clock_in_dt ){
											$result_array['result_code'] = 1;
											$result_array['error_msg'] = $this->_common->langText($locale,'txt.clock_in_Date_timeNotMatch.error');
										}
										else{
											//Update job statrt date and time
											$updateArrayClockIn = array("applicant_clockOut_dateTime" => $clock_out_dt, "applicant_clockOut_status" => 1);
											$whereClauseClockIn = array("id" => $cloclInOutrow->id);
											$this->_db->UpdateAll(DB_PREFIX."clockIn_clockOut", $updateArrayClockIn, $whereClauseClockIn, "");
											//Also update job_applicants
											$updateApplicant  = "UPDATE `".DB_PREFIX."job_applicants`
															SET clockInOutStatus='2', meal_hours = '$meal_hours'
															WHERE `job_id`  = '$jobId'
															AND `applicant_user_account_id` = '$user_accounts_id'";
											$this->_db->my_query($updateApplicant);
											//Success msg
											//$result_array['result_code'] = 0;
											//$result_array['success_msg'] = $this->_common->langText($locale,'txt.start_job.success');
											$result_array['result_code'] = 0;
											$result_array['success_msg'] = $this->_common->langText($locale,'txt.completed_job.success');
										}
									}
								}else{
								//already clock out
								$result_array['result_code'] = 1;
								$result_array['error_msg'] = $this->_common->langText($locale,'txt.completed_job_already.error');
								}
							}else{
								$result_array['result_code'] = 1;
								$result_array['error_msg'] = $this->_common->langText($locale,'txt.start_job.error.notexist');
							}
						}
						//--------------------------------XXX----------------------
						if($cloclInOutrow->applicant_clockOut_status==1){
								//already clock out
								$result_array['result_code'] = 1;
								$result_array['error_msg'] = $this->_common->langText($locale,'txt.completed_job_already.error');
						}else{
							//Check clock out status
							if($cloclInOutrow->applicant_clockOut_status==0){
								//check clock in status
								if($cloclInOutrow->applicant_clockIn_status==0){
									$result_array['result_code'] = 1;
									$result_array['error_msg'] = $this->_common->langText($locale,'txt.jobNotStarted.error');
								}else{
									//check clock out in date time
									if($clock_out_dt != $cloclInOutrow->clockout_current_dateTime){
										$result_array['result_code'] = 1;
										$result_array['error_msg'] = $this->_common->langText($locale,'txt.clockoutDateNotMatch.error');
									}elseif($cloclInOutrow->applicant_clockIn_dateTime != $clock_in_dt ){
											$result_array['result_code'] = 1;
											$result_array['error_msg'] = $this->_common->langText($locale,'txt.clock_in_Date_timeNotMatch.error');
									}
									else{
										$userAccountId 	= $this->_common->check_session($secretToken);
										$outletIDQuery  = "SELECT outlet_id FROM `".DB_PREFIX."jobs` WHERE `job_id` = '$jobId'";
										$resultdata 	= $this->_db->my_query($outletIDQuery);
										$outletData 	= $this->_db->my_fetch_object($resultdata);
										$outletId 		= $outletData->outlet_id;

										$completeJobQuery = "INSERT INTO `".DB_PREFIX."outlet_feedback` (`job_id`, `applicant_user_account_id`, `outlet_id`, `rating`,  `feedback_text`,`created_at`) VALUES ('$jobId','$userAccountId' , '$outletId', '$rating', '$feedback', '$current_date')";
										$inserted 		= $this->_db->my_query($completeJobQuery) ;

										$update_query 	= "UPDATE `".DB_PREFIX."job_applicants` SET
																	completed_by_applicant = 1,
																	bank_name = '$bank_name',
																	bank_account_name = '$bank_account_name',
																	bank_account_number = '$bank_account_number'
																	WHERE
																	applicant_user_account_id= '$userAccountId' AND job_id = '$jobId'";
										if($inserted && $this->_db->my_query($update_query) == true ){
											$this->sendEmailNotification($jobId, $userAccountId,'feedback');

										}else{
											$result_array['result_code'] = 1;
											$result_array['error_msg'] = $this->_common->langText($locale,'txt.completed_job.error');
										}
										//Update job statrt date and time
										$updateArrayClockIn = array("applicant_clockOut_dateTime" => $clock_out_dt, "applicant_clockOut_status" => 1);
										$whereClauseClockIn = array("id" => $cloclInOutrow->id);
										$this->_db->UpdateAll(DB_PREFIX."clockIn_clockOut", $updateArrayClockIn, $whereClauseClockIn, "");
										//Also update job_applicants
										$updateApplicant  	= "UPDATE `".DB_PREFIX."job_applicants`
														SET clockInOutStatus='2'
														WHERE `job_id`  = '$jobId'
														AND `applicant_user_account_id` = '$user_accounts_id'";
										$this->_db->my_query($updateApplicant);
										//Success msg
										//$result_array['result_code'] = 0;
										//$result_array['success_msg'] = $this->_common->langText($locale,'txt.start_job.success');
										$result_array['result_code'] = 0;
										$result_array['success_msg'] = $this->_common->langText($locale,'txt.completed_job.success');
									}
								}
							}else{
								$result_array['result_code'] = 1;
								$result_array['error_msg'] = $this->_common->langText($locale,'txt.start_job.error.notexist');
							}
						}
					}else{
						$result_array['result_code'] = 1;
						$result_array['error_msg'] = $this->_common->langText($locale,'txt.complete_job.error.rating');
					}
				}else{
					$result_array['result_code'] = 1;
					$result_array['error_msg'] = $this->_common->langText($locale,'txt.complete_job.qr_code_belongs.error.required');
				}
			}else{ // // required field else
				$result_array['result_code'] = 1;
				$result_array['error_msg'] = $this->_common->langText($locale,'txt.register.error.required');
			}
		}else{ 		// session error else
			$result_array['result_code'] = 1;
			$result_array['error_msg'] = $this->_common->langText($locale,'txt.change_user.error.session');
		}
		return $result_array;
	}

	function cancel_job(){
		$current_date = CURRENT_TIME;
		$locale = $this->_common->locale();
		$current_date = CURRENT_TIME;
		echo $current_date."\n";
		$result_array  = array();
		if($this->_common->test_input($_REQUEST['secret_token']) != '' && $this->_common->check_session($_REQUEST['secret_token'])){
			$secretToken  = $_REQUEST['secret_token'];
			if($this->_common->test_input($_REQUEST['job_id']) != '' && $this->_common->test_input($_REQUEST['reason'])) {
				$userAccountId = $this->_common->check_session($secretToken);
				$jobId 		= $_REQUEST['job_id'];
				$reason 	= $_REQUEST['reason'];
				$JobQuery  	= "SELECT * FROM `".DB_PREFIX."jobs` WHERE `job_id` = '$jobId'";
				$resultdata = $this->_db->my_query($JobQuery);
				$jobData 	= $this->_db->my_fetch_object($resultdata);
				//$current_date_time = date_create($current_date);
				$current_date_time 	= strtotime($current_date);
				$start_date  		= $jobData->start_date." ".$jobData->start_time;
				$start_date_time 	= strtotime($start_date);

				//if($current_date_time > $start_date_time)
				$cDateTime = date("Y-m-d H:i:s");
				if($cDateTime > $start_date)
				{
					$result_array['result_code'] = 1;
					$result_array['error_msg'] = $this->_common->langText($locale,'txt.cancel_job.error.already');
				}else{
					$diff = $this->dateDiff($start_date);
					if(isset($diff["Minutes"])){
						if($diff["remaining"] < 10){
							$result_array['result_code'] = 1;
							$result_array['error_msg'] = $this->_common->langText($locale,'txt.cancel_job.error.not_able');
						}else{
							//rsolanki check for already cancel this job
							$chk_query_cancel = "SELECT `is_delete` FROM `".DB_PREFIX."job_applicants` WHERE applicant_user_account_id= '$userAccountId' AND job_id = '$jobId' AND is_delete='1'";
							$chk_data_cancel  = $this->_db->my_query($chk_query_cancel);
							if($this->_db->my_num_rows($chk_data_cancel) > 0) {
								$result_array['result_code'] = 0;
								$result_array['success_msg'] = $this->_common->langText($locale,'txt.cancel_job_already.success');
							}else{
								//Check job started
								$checkClockInOutStatus = "SELECT clockInOutStatus
														FROM`".DB_PREFIX."job_applicants`
														WHERE applicant_user_account_id= '$userAccountId' AND job_id = '$jobId'";
								$checkClockInOutStatusboj 	= $this->_db->my_query($checkClockInOutStatus);
								$rowclockInOutStatus 		= $this->_db->my_fetch_object($checkClockInOutStatusboj);
								$clockInOutStatus			= $rowclockInOutStatus->clockInOutStatus;
								if($clockInOutStatus!=0){
									$result_array['result_code'] = 1;
									$result_array['error_msg'] = $this->_common->langText($locale,'txt.jobStarted.error');
								}else{
									$update_query = "UPDATE `".DB_PREFIX."job_applicants` SET `is_delete` = '1', `applicant_cancel_feedback` = '$reason' WHERE applicant_user_account_id= '$userAccountId' AND job_id = '$jobId'";
									if($this->_db->my_query($update_query) == true ){
										$result_array['result_code'] = 0;
										$result_array['success_msg'] = $this->_common->langText($locale,'txt.cancel_job.success');
										//send email notification - rsolanki
										// start - added by jhtang 20160528
										if ($jobData->is_applicants_hired == 1) $this->sendEmailNotification($jobId, $userAccountId,'cancel_job');
//										$this->sendEmailNotification($jobId, $userAccountId,'cancel_job');
										// end - added by jhtang 20160528
									}else{
										$result_array['result_code'] = 1;
										$result_array['error_msg'] = $this->_common->langText($locale,'txt.cancel_job.error');
									}
								}
							}
						}
					}else{
							//rsolanki check for already cancel this job
							$chk_query_cancel = "SELECT `is_delete` FROM `".DB_PREFIX."job_applicants` WHERE applicant_user_account_id= '$userAccountId' AND job_id = '$jobId' AND is_delete='1'";
							$chk_data_cancel  = $this->_db->my_query($chk_query_cancel);
							if($this->_db->my_num_rows($chk_data_cancel) > 0) {
								$result_array['result_code'] = 0;
								$result_array['success_msg'] = $this->_common->langText($locale,'txt.cancel_job_already.success');
							}else{
								//Check job started
								$checkClockInOutStatus = "SELECT clockInOutStatus
														FROM`".DB_PREFIX."job_applicants`
														WHERE applicant_user_account_id= '$userAccountId' AND job_id = '$jobId'";
								$checkClockInOutStatusboj 	= $this->_db->my_query($checkClockInOutStatus);
								$rowclockInOutStatus 		= $this->_db->my_fetch_object($checkClockInOutStatusboj);
								$clockInOutStatus			= $rowclockInOutStatus->clockInOutStatus;
								if($clockInOutStatus!=0){
									$result_array['result_code'] = 1;
									$result_array['error_msg'] = $this->_common->langText($locale,'txt.jobStarted.error');
								}else{
									$update_query = "UPDATE `".DB_PREFIX."job_applicants` SET `is_delete` = '1', `applicant_cancel_feedback` = '$reason' WHERE applicant_user_account_id= '$userAccountId' AND job_id = '$jobId'";
									if($this->_db->my_query($update_query) == true ){
										$result_array['result_code'] = 0;
										$result_array['success_msg'] = $this->_common->langText($locale,'txt.cancel_job.success');
										//send email notification - rsolanki
										// start - added by jhtang 20160528
										if ($jobData->is_applicants_hired == 1) $this->sendEmailNotification($jobId, $userAccountId,'cancel_job');
//										$this->sendEmailNotification($jobId, $userAccountId,'cancel_job');
										// end - added by jhtang 20160528
									}else{
										$result_array['result_code'] = 1;
										$result_array['error_msg'] = $this->_common->langText($locale,'txt.cancel_job.error');
									}
								}

							}
					}
				}
			}else{
				$result_array['result_code'] = 1;
				$result_array['error_msg'] = $this->_common->langText($locale,'txt.register.error.required');
			}
		}else{
			$result_array['result_code'] = 1;
			$result_array['error_msg'] = $this->_common->langText($locale,'txt.change_user.error.session');
		}
		return $result_array;
	}

	function maintainance(){
		$result_array  = array();
		$locale = $this->_common->locale();
		$result_array['result_code'] = 1;
		$result_array['error_msg'] = $this->_common->langText($locale,'txt.maintainance.success');
		return $result_array;
	}

	function get_rating_appliants($user_account_id){
		$total_completed_jobs = $this->getTotalvoteCounts($user_account_id);
		$query  = "SELECT SUM(rating) as total_rating FROM `jod_applicants_feedback` WHERE `applicant_user_account_id` = '$user_account_id'";
		$result =  $this->_db->my_query($query);
		if($this->_db->my_num_rows($result)>0){
			$data =  $this->_db->my_fetch_object($result);

			if($total_completed_jobs > 0){
				$avg_rating =  $data->total_rating/$total_completed_jobs;
			}else{
				$avg_rating = 0;
			}
			return $avg_rating ;
		}else{
			return 0;
		}
	}

	function getTotalvoteCounts($user_account_id){
			$query = "SELECT count(feedback_id) as count FROM `jod_applicants_feedback` WHERE `applicant_user_account_id` = '$user_account_id'";
			$result =  $this->_db->my_query($query);
			if($this->_db->my_num_rows($result)>0){
				$data = $this->_db->my_fetch_object($result);
				return $data->count;
			}else{
				return 0;
			}
	}

	function dateDiff($date)
	{
		  $my_data = array();
		  $mydate= date("Y-m-d H:i:s");
		  $theDiff="";
		  //echo $mydate;//2014-06-06 21:35:55
		  $datetime1 = date_create($date);
		  $datetime2 = date_create($mydate);
		  $interval = date_diff($datetime1, $datetime2);
		  //echo $interval;die;
		 // echo $interval->format('%s Seconds %i Minutes %h Hours %d days %m Months %y Year    Ago')."<br>"; die;
		  $min=$interval->format('%i');
		  $sec=$interval->format('%s');
		  $hour=$interval->format('%h');
		  $mon=$interval->format('%m');
		  $day=$interval->format('%d');
		  $year=$interval->format('%y');
		  if($interval->format('%i%h%d%m%y')=="00000")
		  {
			  $my_data["Seconds"] = true;
			  $my_data["remaining"] = $sec;
			//echo $interval->format('%i%h%d%m%y')."<br>";
			//return $sec." Seconds";

		  }

		else if($interval->format('%h%d%m%y')=="0000"){
		   //return $min." Minutes";
			$my_data["Minutes"] = true;
			$my_data["remaining"] = $min;
		   }


		else if($interval->format('%d%m%y')=="000"){
		   //return $hour." Hours";
			$my_data["Hours"] = true;
			$my_data["remaining"] = $hour;
		   }


		else if($interval->format('%m%y')=="00"){
				//return $day." Days";
			$my_data["Days"] = true;
			$my_data["remaining"] = $day;
		   }

		else if($interval->format('%y')=="0"){
		   //return $mon." Months";
			$my_data["Months"] = true;
			$my_data["remaining"] = $mon;
			}

		else{
			$my_data["Years"] = true;
			$my_data["remaining"] = $year;
		  // return $year." Years";
		   }
			return $my_data;
	}

	function round_up($value, $places)
	{
		$mult = pow(10, abs($places));
		 return $places < 0 ?
		ceil($value / $mult) * $mult :
			ceil($value * $mult) / $mult;
	}

	//Create QR code - rsolanki
	function createQrImg($dateTime){
		//error_reporting(0);
		require_once("libraries/Ciqrcode.php");
		$path = dirname(__FILE__).'/../../qr_images/';
		//$path = dirname(__FILE__).'/qr_images/';;
		//die();
		$name ="";
		if (!empty($dateTime)) {
			$name = md5(date('Y-m-d H:i:s')).uniqid().'.png';
			$params['data'] 	= $dateTime;
			$params['level'] 	= 'H';
			$params['size'] 	= 5;
			$params['savename'] = $path.$name;
			$qr = new Ciqrcode();
			$qr->generate($params);
			$file_name = $path.$name;
			chmod($file_name, 775); //added by rsolanki

			//$this->load->library('s3');
			//s3::setAuth(awsAccessKey,awsSecretKey);
		   // if (s3::putObjectFile($file_name, "39c.website",'qrcode/'.$name, s3::ACL_PUBLIC_READ)) {
				//if (file_exists($file_name)) unlink($file_name);
			//}
		}
		return $name;
	}

}
?>
