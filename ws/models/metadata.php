<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class metadata {
	public function __construct() {
        $this->_db      = env::getInst();
        $this->_common  = new commonclass();
    }
	
	public function get_education_institutes() {
		$locale = $this->_common->locale();
        $result_array  = array();
        $current_date = CURRENT_TIME;
        $query = "SELECT * FROM `".DB_PREFIX."education_institutes` WHERE `status` = '1' AND `is_delete` = '0' ORDER BY `name`";
		$getEducationInstitutes = $this->_db->my_query($query);
		$result =array();
		if ($this->_db->my_num_rows($getEducationInstitutes) > 0) { 
			while($row = $this->_db->my_fetch_object($getEducationInstitutes)) {
				$result[$row->education_institutes_id] = $row->name;
			}
			$result_array['result_code'] = 0;
			$result_array['result'] = $result;
		} else {
			$result_array['result_code'] = 1;
			$result_array['error_msg'] = $this->_common->langText($locale,'txt.get_education_institutes.error.notavailable');	
		}
		//print_r($result_array);
		//die;
		return $result_array;
	} 
	
	public function get_job_roles() {
		$locale = $this->_common->locale();
        $result_array  = array();
        $current_date = CURRENT_TIME;
		if($this->_common->test_input($_REQUEST['secret_token']) != '' && $this->_common->check_session($_REQUEST['secret_token'])) {
			$query = "SELECT * FROM `".DB_PREFIX."job_roles` WHERE `status` = '1' AND `is_delete` = '0' ORDER BY `name`";
			$getEducationInstitutes = $this->_db->my_query($query);
			$result =array();
			if ($this->_db->my_num_rows($getEducationInstitutes) > 0) { 
				while($row = $this->_db->my_fetch_object($getEducationInstitutes)) {
					$result[$row->job_role_id] = $row->name;
				}
				$result_array['result_code'] = 0;
				$result_array['result'] = $result;
			} else {
				$result_array['result_code'] = 1;
				$result_array['error_msg'] = $this->_common->langText($locale,'txt.get_education_institutes.error.notavailable');	
			}
		} else {
			$result_array['result_code'] = 1;
			$result_array['error_msg'] = $this->_common->langText($locale,'txt.change_user.error.session');	
		}
		return $result_array;
	} 
	
}
?>
