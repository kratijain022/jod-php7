<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

spl_autoload_register(function ($class) {

    $directory = array(
            dirname(__FILE__)."/",
            dirname(dirname(__FILE__))."/core/",
            dirname(dirname(__FILE__))."/controllers/",
            dirname(dirname(__FILE__))."/models/",
            dirname(dirname(__FILE__))."/libraries/",
            dirname(dirname(__FILE__))."/helpers/",
            dirname(dirname(__FILE__))."/modules/",
            dirname(dirname(__FILE__))."/language/",
            dirname(dirname(__FILE__))."/include/",
            dirname(dirname(__FILE__))."/classes/",
            dirname(dirname(__FILE__))."/inc/"
    );

    foreach ($directory as $base_dir) {
        $file = $base_dir . str_replace('\\', '/', strtolower($class)) . '.php';
        if (file_exists($file)) 
            require_once $file;
    }
});

?>