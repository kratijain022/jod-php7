<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Author : CDN SOL
 * Email  : admin@cdnsol.com
 * Timestamp : Aug-29 06:11PM
 * Copyright : CDNSOL team
 *
 */
	/** Function for log the message - Start **/
    function commonLogMessages($PostParameters,$logfrom)
    {
		$PostParameters = "";
		foreach ($_REQUEST as $key => $value) { 
			if($key==='password'||$key==='oldPassword'||$key==='newPasword')
			{   $password='';
				for($i=1;$i<=strlen($value);++$i)
				{
					$password.='*';
				}
				$PostParameters = $PostParameters."$key = $password, ";
			}else{
			     $PostParameters = $PostParameters."$key = $value, "; 
		     }
		}
				
		$Ip = "";
		if (isset($_SERVER['HTTP_CLIENT_IP']) && $_SERVER['HTTP_CLIENT_IP'] != '')
			$Ip = $_SERVER['HTTP_CLIENT_IP'];
		elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR']) && $_SERVER['HTTP_X_FORWARDED_FOR'] != '')
			$Ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		elseif (isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] != '')
			$Ip = $_SERVER['REMOTE_ADDR'];
		
		//$pageURL = $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
		if ($logfrom == "website")
		{
			$CI = & get_instance();
			$action = $CI->uri->segment('1');
			$actionMethod = $CI->uri->segment('2');
			ws_log("info","ProcessID :=> ".getmypid());
			ws_log("info","Request Details :=> ".$PostParameters);
			ws_log("info","Action Set :=> ".$action);
			ws_log("info","Action Method Set :=> ".$actionMethod);
			ws_log("info","Request Type :=> ".$_SERVER['REQUEST_METHOD']);
			ws_log("info","Client IP :=> ".$Ip);
			ws_log("info","Request Agent :=> ".$_SERVER['HTTP_USER_AGENT']."\n");
		} 
		else
		{
			$action = $_REQUEST["action"];
			$actionMethod = $_REQUEST["actionMethod"];
			log_message("info","ProcessID :=> ".getmypid());
			log_message("info","Request Details :=> ".$PostParameters);
			log_message("info","Action Set :=> ".$action);
			log_message("info","Action Method Set :=> ".$actionMethod);
			log_message("info","Request Type :=> ".$_SERVER['REQUEST_METHOD']);
			log_message("info","Client IP :=> ".$Ip);
			log_message("info","Request Agent :=> ".$_SERVER['HTTP_USER_AGENT']);
		}
	}
?>
