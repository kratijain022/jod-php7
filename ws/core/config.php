<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

    final class config
    {
        /// -- Hold Static class object --
        private static $ref;
        /// -- Hold Static private class members variables --
        private $_config=array();
        /**
        * @method __construct
        * @see private constructor to protect beign inherited
        * @access private
        * @return void
        */
        private function __construct()
        {
            /*
            |--------------------------------------------------------------------------
            | Web Site URL
            |--------------------------------------------------------------------------
            | Get url of web site with domain name and folder address
            */
            $this->_config['siteUrl'] = $_SERVER["REQUEST_URI"];
            /*
            |--------------------------------------------------------------------------
            | Web Site Language
            |--------------------------------------------------------------------------
            | Get Default language of web sservice with domain name and folder address
            */
            $this->_config['lang'] = "en";
            /*
            |--------------------------------------------------------------------------
            | Web Service Name
            |--------------------------------------------------------------------------
            | Display Web Service Name on Service Interface
            */
            $this->_config['webServiceTitle'] = "JOD_WEB_PORTAL";
            /*
            |--------------------------------------------------------------------------
            | Web soap server Namespace
            |--------------------------------------------------------------------------
            | Display Web Service - create a soap server Namespace
            */
            $this->_config['namespace'] = "JOD";
            /*
            |--------------------------------------------------------------------------
            | Sql Host Server Name
            |--------------------------------------------------------------------------
            |
            | Name of server which you are trying to connect Ex. (www.chatsupport.co.uk)
            | Please do not allow this use access to "%" to mantain security
            |
            */

            if($_SERVER["SERVER_ADDR"] == '10.10.10.2') {
				$this->_config["SQL_HOST_NAME"] ="10.10.10.2";
				$this->_config['SQL_USER_NAME'] ="jobsondemand_qa";
				$this->_config['SQL_PASSWORD']="SaPd6GAbEP4cLNfM";
				$this->_config['SQL_DB']="jobsondemand_qa";

			} else {
				//~ $this->_config["SQL_HOST_NAME"] ="localhost";
				//~ $this->_config['SQL_USER_NAME'] ="root";
				//~ $this->_config['SQL_PASSWORD']="cdn123";
				//~ $this->_config['SQL_DB']="jobsondemand_qa";

				$this->_config["SQL_HOST_NAME"] = "localhost";
				$this->_config['SQL_USER_NAME'] = "root";
				$this->_config['SQL_PASSWORD']  = "cdn123";
				$this->_config['SQL_DB'] 		= "jod_12_jan_2017";

				/*$this->_config["SQL_HOST_NAME"] ="10.10.10.2";
				$this->_config['SQL_USER_NAME'] ="jobsondemand_qa";
				$this->_config['SQL_PASSWORD']="SaPd6GAbEP4cLNfM";
				$this->_config['SQL_DB']="jobsondemand_qa"; */

			}

            /*
            |--------------------------------------------------------------------------
            | Error Logging Directory Path
            |--------------------------------------------------------------------------
            |
            | Leave this BLANK unless you would like to set something other than the default
            | system/logs/ folder.  Use a full server path with trailing slash.
            |
            */
            $this->_config['log_path'] = '';
            /*
            |--------------------------------------------------------------------------
            | Error Logging Threshold
            |--------------------------------------------------------------------------
            | If you have enabled error logging, you can set an error threshold to
            | determine what gets logged. Threshold options are:
            | You can enable error logging by setting a threshold over zero. The
            | threshold determines what gets logged. Threshold options are:
            |
            |   0 = Disables logging, Error logging TURNED OFF
            |   1 = Error Messages (including PHP errors)
            |   2 = Debug Messages
            |   3 = Informational Messages
            |   4 = All Messages
            | For a live site you'll usually only enable Errors (1) to be logged otherwise
            | your log files will fill up very fast.
            */
            $this->_config['log_threshold'] = 4;
            /*
            |--------------------------------------------------------------------------
            | Date Format for Logs
            |--------------------------------------------------------------------------
            | Each item that is logged has an associated date. You can use PHP date
            | codes to set your own date formatting
            */
            $this->_config['log_date_format']= 'D M j G:i:s T Y';
        }

        /**
        *Config::getInst()
        *@Param Void
        *@RETURN Static Object of Class
        */
        final public static function getInst()
        {
          if(!is_object(config::$ref)){
              config::$ref=new config();
          }
          return config::$ref;
        }

        /**
        *Config::getConfig Array()
        *@Param Void
        *@RETURN Array of Settings
        */
        final public function getSettings()
        {
            return $this->_config;
        }

        /**
        *config::getKeyValue Array()
        *@Param key STRING
        *@RETURN Array of Settings
        */
        final public function getKeyValue($key)
        {
            if(array_key_exists($key,$this->_config))
            {
              return $this->_config[$key];
            }
        }

        /**
        *config::getKeyValue Array()
        *@Param key STRING
        *@RETURN Array of Settings
        */
        public function setLang($idiom)
        {
            if(array_key_exists("lang",$this->_config))
            {
              $this->_config["lang"]=$idiom;
            }
        }
    }///  -- Class:config ENDS --
?>
