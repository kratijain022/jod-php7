<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

    /**
     * Author : Neeraj sharma
     * Email  : neerajsharma@cdnsol.com
     * Timestamp : 01-Oct-2013 01:22 PM
     * Copyright : www.cdnsol.com
     */
    final class pushnotifications {

        public $_parents = array();
        public $_data = array();

        /// -- Hold Static Connection Object --
        private static $ref;
        /// -- Hold Connection Object --

        public function __construct() {
            /// -- Create Database Connection instance --
        }

        /**
         * final static env::getInst
         * @access Public
         * @return Object
         */
        final public static function getInst(){
            if(!is_object(pushnotifications::$ref))
            {
                pushnotifications::$ref=new pushnotifications(config::getInst());
            }
            return pushnotifications::$ref;
        }

        /**
         *  This function is for send push notifications on user iPhone device - Start.
        **/
        public function sendIphonePushNotification($deviceId) {

            // Adjust to your timezone
            //date_default_timezone_set('Europe/Rome');

            // Report all PHP errors
            error_reporting(1);

            require_once 'pushNotifications/ApnsPHP/Autoload.php';

            // Instanciate a new ApnsPHP_Push object
            $push = new ApnsPHP_Push( ApnsPHP_Abstract::ENVIRONMENT_PRODUCTION, dirname(__DIR__).'/core/pushNotifications/apns-dev.pem' );

            // Set the Root Certificate Autority to verify the Apple remote peer
            //$push->setRootCertificationAuthority('entrust_root_certification_authority.pem');

            // Connect to the Apple Push Notification Service
            $push->connect();

            foreach ($deviceId as $key => $value)
            {
                $deviceId = $value['deviceId'];
                $pushMessage = $value['pushMessage'];
                $checkId = 1;

                $message = new ApnsPHP_Message($deviceId);
                // Set a custom identifier. To get back this identifier use the getCustomIdentifier() method
                // over a ApnsPHP_Message object retrieved with the getErrors() message.
                $message->setCustomIdentifier("Message-Badge-3");
                $badgeCount = intval($checkId);
                $message->setBadge($badgeCount);
                // Set a simple welcome text
                $message->setText($pushMessage);
                // Play the default sound
                $message->setSound();
                // Set a custom property
                $message->setCustomProperty('post_data', array());
                // Set another custom property
                //$message->setCustomProperty('acme3', array('bing', 'bong'));
                // Set the expiry value to 30 seconds
                //$message->setExpiry(30);
                // Add the message to the message queue
                $push->add($message);
            }

            // Send all messages in the message queue
            $push->send();

            // Disconnect from the Apple Push Notification Service
            $push->disconnect();

            // Examine the error message container
            $aErrorQueue = $push->getErrors();
            if (!empty($aErrorQueue)) {
                //var_dump($aErrorQueue);
            }
        }
        /** This function is for send push notifications on user iPhone device - End **/

        /**
         *  This function is for send push notifications on user android device - Start.
        **/
        public function sendAndroidPushNotification($details)
        {
			if(!empty($details))
			{
				$result=array();
				$activityMessage = "You got a private message.";
				$apiKey="AIzaSyCnyBOlo5pgFE0fuwIRYPiBQqaBE2JLNzE";
				$url = 'https://android.googleapis.com/gcm/send';
				// Open connection
				$ch = curl_init();
				foreach ($details as $value)
				{
					$deviceId = $value['deviceId'];
					$pushMessage = $value['pushMessage'];

					$registation_ids = array($deviceId);
					$message= array('pushMessage'=>$pushMessage,'deviceId'=>$deviceId);
					$finalMessage = json_encode($message);

					$fields = array('registration_ids' => $registation_ids,'data' => array('message'=>$finalMessage));
					$headers = array('Authorization: key=' . $apiKey,'Content-Type: application/json');

					curl_setopt($ch, CURLOPT_URL, $url);
					curl_setopt($ch, CURLOPT_POST, true);
					curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
					curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
					$result[] = curl_exec($ch);
				}

				if ($result === FALSE) {
					//die('Curl failed: ' . curl_error($ch));
				}
				curl_close($ch); // Close connection
				return $result;
			}
		}

        public function sendAndroidPushNotification2($androidDetails){

			//$apiKey  ;
			$apiKey = "AIzaSyDDddUe6kO2GfSTkRSJcBJ8NfF3eMo8Az0";
			foreach ($androidDetails as $key => $value)
			{
				try{
					$deviceId = $value['device_id'];
					$pushMessage = $value['message'];

					$registrationIDs =  array($deviceId); // Replace with real client registration IDs ('Device ID')
					$message = $pushMessage; // Message to be sent
					$url = 'https://android.googleapis.com/gcm/send'; // Set POST variables // $url =  'https://android.apis.google.com/c2dm/send';
					$fields = array(
									'registration_ids'  => $registrationIDs,
									'data'   => array("message" => $message),
									//'post_data'  => array("push_type" => $pushType),
									);
					$headers = array(
										'Authorization: key=' . $apiKey,
										'Content-Type: application/json'
									);

					$ch = curl_init(); // Open connection
					// Set the url, number of POST vars, POST data
					curl_setopt( $ch, CURLOPT_URL, $url);
					curl_setopt( $ch, CURLOPT_POST, true);
					curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
					curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
					// Disabling SSL Certificate support temporarly
					//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
					curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode($fields));
					// Execute post
					$result = curl_exec($ch);
					// Close connection
					curl_close($ch);
					//echo $result;
				}catch ( Exception $e )
				{
					echo "Exception at $key ";
				}
			}
			if ($result === FALSE) {
				//die('Curl failed: ' . curl_error($ch));
			} else {
				//return 'success';
			}
		}


		public function sendIphonePushNotification2($iphoneDetails) {

			// Report all PHP errors
			error_reporting(1);
			require_once 'pushNotifications/ApnsPHP/Autoload.php';
			// Instanciate a new ApnsPHP_Push object
			//$push = new ApnsPHP_Push( ApnsPHP_Abstract::ENVIRONMENT_PRODUCTION, dirname(__DIR__).'/core/pushNotifications/nk.pem' );
			$push = new ApnsPHP_Push( ApnsPHP_Abstract::ENVIRONMENT_SANDBOX, dirname(__DIR__).'/core/pushNotifications/nk.pem' );
			// Set the Root Certificate Autority to verify the Apple remote peer
			//$push->setRootCertificationAuthority(dirname(__DIR__).'/core/pushNotifications/entrust_root_certification_authority.pem');
			// Connect to the Apple Push Notification Service
			$push->connect();
			//try
			//{
				foreach ($iphoneDetails as $key => $value)
				{
					try{
						log_message('debug', '--------  IOS function called ------ '.$value['device_id']);
						$deviceId = $value['device_id'];
						$pushMessage = $value['message'];
						$welcome_txt = $value['welcome_txt'];
						$pushMessage = utf8_encode($pushMessage);
						//echo $pushMessage;
						$job_id = $value['job_id'];
						$job_status = $value['job_status'];

						$message = new ApnsPHP_Message($deviceId);
						// Set a custom identifier. To get back this identifier use the getCustomIdentifier() method
						// over a ApnsPHP_Message object retrieved with the getErrors() message.
						$message->setCustomIdentifier("Message-Badge-3");
						$message->setBadge(0);
						// Set a simple welcome text
						$message->setText($pushMessage);
						// Play the default sound
						$message->setSound();
						// Set a custom property  rsolanki - no need acme3
						//$message->setCustomProperty('acme3', array("device_id"=>$deviceId,"job_id"=>$job_id,"job_status"=>$job_status));
						$message->setCustomProperty('data', array("job_id"=>$job_id,"job_status"=>$job_status));
						// Set another custom property
						//$message->setCustomProperty('acme3', array('bing', 'bong'));
						// Set the expiry value to 30 seconds
						//$message->setExpiry(30);
						// Add the message to the message queue
						$push->add($message);
						//var_dump($push->send());
						/*if ($push->send()) {
							echo "done";
						} else {
							echo " throw new Exception";
						} */
					} catch ( Exception $e )
					  {
						echo "Exception at $key ";
					  }
				}

				// Send all messages in the message queue
				$push->send();

				//print_r($push);
				// Disconnect from the Apple Push Notification Service
				$push->disconnect();

				// Examine the error message container
				$aErrorQueue = $push->getErrors();
				if (!empty($aErrorQueue)) {
					//return var_dump($aErrorQueue);
				} else {
					//return 'success';
				}
			//}
			//catch ( Exception $e )
			//{
				//continue;
				// echo "Exception at $key";
			//}
		}

        /** This function is for send push notifications on user Android device - End **/
    }
