var UIAlertDialogApi = function () {
 var handleDialogs = function() {

        $('#demo_1').click(function(){
                bootbox.alert("Hello world!");    
            });
            //end #demo_1

            $('#demo_2').click(function(){
                bootbox.alert("Hello world!", function() {
                    alert("Hello world callback");
                });  
            });
            //end #demo_2
        
            
            //end #demo_3

            $('#demo_4').click(function(){
                bootbox.prompt("What is your name?", function(result) {
                    if (result === null) {
                        alert("Prompt dismissed");
                    } else {
                        alert("Hi <b>"+result+"</b>");
                    }
                });
            });
            //end #demo_6

            $('#demo_5').click(function(){
                bootbox.dialog({
                    message: "I am a custom dialog",
                    title: "Custom title",
                    buttons: {
                      success: {
                        label: "Success!",
                        className: "green",
                        callback: function() {
                          alert("great success");
                        }
                      },
                      danger: {
                        label: "Danger!",
                        className: "red",
                        callback: function() {
                          alert("uh oh, look out!");
                        }
                      },
                      main: {
                        label: "Click ME!",
                        className: "blue",
                        callback: function() {
                          alert("Primary button");
                        }
                      }
                    }
                });
            });
            //end #demo_7

    }

    
    var handleAlerts = function() {
        
        $('.table-group-action-submit').click(function(){
			var selected = $('tbody > tr > td:nth-child(1) input[type="checkbox"]:checked').size();
			var select_val =  $('.table-group-action-input').val();
			
			if(select_val=='')
			{
				var messages = 'Please select any action';
				
			}
			else
			{
				var messages = 'Please select any check box';
			}
		if(selected==0 || select_val=='')
		{
            Metronic.alert({
                container: $('#alert_container').val(), // alerts parent container(by default placed after the page breadcrumbs)
                place: $('#alert_place').val(), // append or prepent in container 
                type: 'danger',  // alert's type
                message: messages,  // alert's message
                close:'true', // make alert closable
                reset: 'true', // close all previouse alerts first
                focus: $('#alert_focus').is(":checked"), // auto scroll to the alert after shown
                closeInSeconds: $('#alert_close_in_seconds').val(), // auto close after defined seconds
                icon: 'warning' // put icon before the message
            });
            return false;
		}
		
			bootbox.confirm("Are you sure to perform this action ?", function(result) {
				if(result==true)
				{
					$('#staff_section').submit();
				}
			}); 
			return false;
        });

    }

    var handleAlerts1 = function() {
        
        $('.classic').click(function(){
			
			var selected = $('tbody > tr > td:nth-child(1) input[type="checkbox"]:checked').size();
			var select_val =  $('.table-group-action-input').val();
			
			if(select_val=='')
			{
				var messages = 'Please select any action';
				
			}
			else
			{
				var messages = 'Please select any check box';
			}
			
		if(selected==0 || select_val=='')
		{
            Metronic.alert({
                container: $('#alert_container').val(), // alerts parent container(by default placed after the page breadcrumbs)
                place: $('#alert_place').val(), // append or prepent in container 
                type: 'danger',  // alert's type
                message: messages,  // alert's message
                close:'true', // make alert closable
                reset: 'true', // close all previouse alerts first
                focus: $('#alert_focus').is(":checked"), // auto scroll to the alert after shown
                closeInSeconds: $('#alert_close_in_seconds').val(), // auto close after defined seconds
                icon: 'warning' // put icon before the message
            });
            return false;
		}
		
			bootbox.confirm("Are you sure to perform this action ?", function(result) {
				if(result==true)
				{
					$('#staff_section').submit();
				}
			}); 
			return false;
        });

    }

    return {

        //main function to initiate the module
        init: function () {
            handleDialogs();
            handleAlerts();
            handleAlerts1();
        }
    };

}();
